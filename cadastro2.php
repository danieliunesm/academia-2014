<?php
include "include/security2.php";
include "include/defines.php";
include "include/dbconnection.php";
include "include/genericfunctions.php";
include "include/codefunctions.php";

$comunidade = 'Colaborae';
$cd_empresa = isset($_SESSION['cd_empresa'])?$_SESSION['cd_empresa']:0;
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $cd_empresa";
$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

if($oRs = mysql_fetch_row($query)){
	$comunidade = $oRs[0];
}
mysql_free_result($query);
?>
<?php writetopcode(); ?>
<script language="JavaScript">
function ValidaData(DtVal){
	DtVal = Exclui_inval(DtVal, 8);
	var msg = "";
	if(DtVal.length != 8){
		msg = "Preencha a Data de Nascimento no formato DDMMAAAA!     \n";
        return msg;
	}
	var nChar = null;
	for (i=0;i<8;i++){
		nChar = DtVal.charAt(i);
		if(isNaN(nChar)){
			msg = "Caracter \ " + nChar + " \ n�o � v�lido.     \n";
			return msg;
		}
	}
	var DtAcerto = null;
	DtAcerto = DtVal.substring(2,4) + "/" + DtVal.substring(0,2) + "/" + DtVal.substring(4,8);
	var bError 		= false;
	var aChar 		= null;
	var holder 		= null;
	var Short 		= null;
	var FebCheck 	= null;
	var NovCheck 	= null;
	var MonthCheck	= null;
	
	if(parseInt(DtVal.substring(2,4)) == 0 || parseInt(DtVal.substring(0,2)) == 0 || parseInt(DtVal.substring(4,8)) == 0) bError = true;
	
    for(i=0;i<10;i++){
		aChar = DtAcerto.charAt(i);
		if(i == 0 && aChar > 1) bError = true;
		if(i == 0 && aChar < 0) bError = true;
		if(i == 0 && aChar == 1) MonthCheck = 1;
		if(i == 0 && aChar == 1) NovCheck = 1;
		if(i == 1 && aChar < "0" || aChar > "9") bError = true;
		if(i == 1 && aChar == 2) FebCheck=1;
		if(i == 1 && MonthCheck == 1 && aChar >2) bError = true;
		if(i == 1 && aChar == "4" || aChar =="6" || aChar =="9") Short = 1;
		if(i == 1 && NovCheck == 1 && aChar == 1) Short = 1;
		if(i == 2 && aChar != "/") bError = true;
        if(FebCheck == 1 && NovCheck != 1){
			if(i==3 && aChar > 2) bError = true;
		}
		if (i == 3 && aChar > 3) bError = true;
		if (i == 3 && aChar ==3) holder=1;
		if (i == 4 && aChar >0 && Short==1 && holder==1) bError = true;
		if (i == 4 && aChar >1 && holder==1) bError = true;
		if (i == 4 && aChar < "0" || aChar > "9") bError = true;
		if (i == 5 && aChar != "/")  bError = true;
		if (i == 8 && aChar < "0" || aChar > "9") bError = true;
		if (i == 9 && aChar < "0" || aChar > "9") bError = true;
	}
	if((DtAcerto.substring(0,5) == "02/29") && DtAcerto.substring(6,10) % 4 != 0){
		msg =  "Data de Nascimento inv�lida. N�o � ano bissexto!     \n";
		return msg;
	}
    if(bError){
		msg =  "Data de Nascimento inv�lida!     \n";
		return msg;
	}
	else{
		return "";
	}
}

function validaEmail(emailStr){
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")	;
	var matchArray=emailStr.match(emailPat);

	if(matchArray==null)return false;

	var user=matchArray[1];
	var domain=matchArray[2];

	if(user.match(userPat)==null)return false;

	var IPArray=domain.match(ipDomainPat);
	if(IPArray!=null){
		for(var i=1;i<=4;i++){
			if(IPArray[i]>255)return false;
		}
		return true;
	}	
	var domainArray=domain.match(domainPat);

	if(domainArray==null)return false;
	
	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;

	if(domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>4)return false;

	if(len<2)return false;

	return true;
}

function checkFormFields(){
	var msg 	 = "";
	var msgdata	 = "";
	var f   	 = document.cadastroForm;
	var objfocus = null;
	if(f.userlogin.value == ""){
		msg+="O campo Login precisa ser preenchido!       \n";
		f.userlogin.style.backgroundColor = '#ddd';
		objfocus = f.userlogin;
	}
	if((f.userpass1.value == '')||(f.userpass1.value.length < 6)){
		msg+="A senha atual est� vazia ou com menos de 6 caracteres!       \n";
		f.userpass1.style.backgroundColor = '#ddd';
		if(objfocus == null)objfocus = f.userpass1;
	}
	if(f.userpass1.value != f.userpass2.value){
		msg+="A confirma��o de senha n�o foi digitada corretamente!       \n";
		f.userpass2.style.backgroundColor = '#ddd';
		if(objfocus == null)objfocus = f.userpass2;
	}
	if(msg != ""){
		alert(msg);
		return false;
	}
	else{
		f.submit();
	}
}
</script>
</head>
<body>
<div id="global">
<div id="globalpadding">
<?php writeheadercode();?>
<?php writemenucode();?>
	<div id="conteudo">
<?php
$titulo = "Cadastro";
?>
		<h1><?php echo $titulo; writenavcode(1);?></h1>
		
		<table border="0" cellpadding="0" cellspacing="5" width="400">
		<form name="cadastroForm" enctype="multipart/form-data" method="post" action="gravarcadastro2.php">
		<tr><td class="textblk2" colspan="2">Agora que seus dados foram cadastrados, escolha um login e senha para ter acessao � �rea exclusiva de participantes da Comunidade <strong><?php echo $comunidade; ?></strong>.<br /><br /></td></tr>
		<tr>
		<td class="title" align="right" width="1%">Login:&nbsp;</td>
		<td class="textblk2"><input name="userlogin" type="text" class="textbox2" maxlength="10" tabIndex="39"> (min 6 caracteres, max 10 caracteres)</td>
		</tr>
		<tr>
		<td class="title" align="right" width="1%">Senha:&nbsp;</td>
		<td class="textblk2"><input name="userpass1" type="password" class="textbox2" maxlength="10" tabIndex="40"> (min 6 caracteres, max 10 caracteres)</td>
		</tr>
		<tr>
		<td class="title" align="right" width="1%">Confirmar&nbsp;Senha:&nbsp;</td>
		<td><input name="userpass2" type="password" class="textbox2" maxlength="10" tabIndex="41"></td>
		</tr>
		
		<tr><td><br /></td></tr>
		
		<tr>
		<td align="center" colspan="2"><input class="buttonsty8" type="button" value="    Enviar    " onclick="checkFormFields()" tabIndex="43">&nbsp;&nbsp;<input class="buttonsty8" type="reset" name="Reset" value="   Limpar   " onblur="document.cadastroForm.nome.focus()" tabIndex="44"><br /><br /></td>
		</tr>
		</form>
		</table>
	</div>
<?php writebottomcode(); ?>
</div>
</div>
</body>
</html>
<?php
mysql_close();
?>