<?php
include "include/security2.php";
include "include/defines.php";
include "include/dbconnection.php";
include "include/genericfunctions.php";
include "include/codefunctions.php";
?>
<?php writetopcode(); ?>

<script type="text/javascript">
function validaEmail(emailStr){
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")	;
	var matchArray=emailStr.match(emailPat);

	if(matchArray==null)return false;

	var user=matchArray[1];
	var domain=matchArray[2];

	if(user.match(userPat)==null)return false;

	var IPArray=domain.match(ipDomainPat);
	if(IPArray!=null){
		for(var i=1;i<=4;i++){
			if(IPArray[i]>255)return false;
		}
		return true;
	}	
	var domainArray=domain.match(domainPat);

	if(domainArray==null)return false;
	
	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;

	if(domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>4)return false;

	if(len<2)return false;

	return true;
}

function checkFormFields(){
	msg = "";
	if(document.mailForm.email.value == "") msg+="Preencha o campo E-mail!       \n";
	else{
		if(document.mailForm.email.value != "" && (!validaEmail(document.mailForm.email.value)))msg+="O e-mail fornecido n�o � v�lido! Verifique os dados digitados.       \n";
	}
	if(msg != ""){
		alert(msg);
		document.mailForm.email.focus();
		return false;
	}
	else{
		document.mailForm.submit();
	}
}

window.onload = function(){document.mailForm.email.focus();}
</script>
<script type="text/javascript" src="/include/js/browser.js"></script>
<script type="text/javascript" src="/include/js/colaborae.js"></script>
</head>
<body>
<div id="global">
<div id="globalpadding"> <br/><br/><br/><br/><br/>
<?php //writeheadercode(2);?>
<?php //writemenucode();?>
	<div id="conteudo">
<?php
$titulo   = "Esqueci meus dados";
$conteudo = "Esqueceu seus dados de login?<br /><br /><br /><br />";
?>
		<h1><?php echo $titulo; //writenavcode(1);?></h1>
<?php
echo $conteudo;
?>
		<form name="mailForm" method="post" action="requestdataprogramatic.php">
		<table border="0">
		<tr>
			<td class="textblk" nowrap>Seu e-mail:</td>
			<td><input type="text" class="textbox" name="email" size="52" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'"></td>
		</tr>

		<tr>
			<td></td>
			<td align="center"><input class="buttonsty" type="button" value="    Enviar    " onclick="checkFormFields()" onfocus="if(this.blur())this.blur()">&nbsp;&nbsp;<input class="buttonsty" type="reset" name="Reset" value="   Limpar   " onfocus="if(this.blur())this.blur();document.mailForm.email.focus()"><br /><br /></td>
		</tr>
		</table>
		</form>
	</div>
<?php //writebottomcode(); ?>
</div>
</div>
</body>
</html>
<?php
mysql_close();
?>