<?php

function pagePreRender(){

	page::$validador = "javascript:return validarPergunta();";

	page::$script = '
	
	function validarPergunta(){
	
		var validacao = VerificarPreenchimentoObrigatorioTag();
		
		if (!validacao){
			return false;
		}

		for (i=0; i < document.forms[0].rdoCerta.length; i++){
			if(document.forms[0].rdoCerta[i].checked){
				return true;
			}
		}
		
		alert("Selecionar uma resposta correta");
		
		return false;
		
	
	}';
	
}

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_perguntas();
}

function carregarRN(){
	
	$rn = new col_perguntas();
	
	page::$rn->DS_PERGUNTAS = $_POST["txtPergunta"];
	page::$rn->IN_PESO = $_POST["txtPeso"];
	page::$rn->CD_PERGUNTAS = $_REQUEST["id"];
	page::$rn->CD_DISCIPLINA = $_POST["cboDisciplina"];
	
	page::$rn->TX_OBSERVACAO = $_POST["txtObservacao"];
	page::$rn->DS_LINK_SUMARIO = $_POST["txtLinkSumario"];
	
	$respostas = array();
	
	for ($i = 0; $i < 4; $i++){
        
		$resposta = new col_respostas();
		$resposta->CD_RESPOSTAS = $i + 1;
		$resposta->DS_RESPOSTAS = $_POST["txtResposta$i"];
		
		
		if ($_POST["rdoCerta"] == $i){
			$resposta->IN_CERTA = 1;
		}
		
		$respostas[] = $resposta;
	}
	
	page::$rn->respostas = $respostas;
	
}

function pageRenderEspecifico(){
	
?>				<TR>
					<TD class="textblk" colspan="2">Disciplina:</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<?php comboboxDisciplina("cboDisciplina",  page::$rn->CD_DISCIPLINA, "Disciplina"); ?>
					</TD>
				</TR>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<TD class=textblk>Pergunta: *</TD>
					<TD class=textblk>N�vel: *</TD>
				</TR>
				<TR>
					<TD><?php textarea("txtPergunta", "300", page::$rn->DS_PERGUNTAS, "100%", "68px", "textbox3", "Pergunta"); ?></TD>
					<TD valign="top"><?php comboboxPeso(txtPeso,page::$rn->IN_PESO,"Peso"); //textbox("txtPeso", "3", page::$rn->IN_PESO, "30px", "textbox3", "Peso", Mascaras::Numero) ?></TD>
				</TR>
				<TR><TD>&nbsp;</TD></TR>
				<TR>
				<TR>
					<TD class="textblk" colspan="2">Observa��o:</TD>
				</TR>
					<TD colspan="2">
						<?php textarea("txtObservacao", "500", page::$rn->TX_OBSERVACAO, "100%", "68px", "textbox3"); ?>
					</TD>
				</TR>
				<TR><TD>&nbsp;</TD></TR>
				<TR>
					<TD class="textblk" colspan="2">Link para Sum�rio:</TD>
				</TR>
				<TR><TD>&nbsp;</TD></TR>
				<TR>
					<TD colspan="2">
						<?php textbox("txtLinkSumario", "200", page::$rn->DS_LINK_SUMARIO, "100%", "textbox3"); ?>
					</TD>
				</TR>

<?php
	pageRespostas(page::$rn->respostas);
}


function pageRespostas($respostas){
	
?>

				<TR>
					<TD class="textblk">Respostas:</TD>
					<TD class="textblk" align="center">Correta:</TD>
				</TR>

<?php

	$linhas = 4;

	if (is_array($respostas)){
		$linhas = count($respostas);
	}
	

	for ($i = 0; $i < $linhas; $i++){
		
		$texto = "";
		$certa = 0;
		
		if (is_array($respostas)){
			$resposta = $respostas[$i];
			$texto = $resposta->DS_RESPOSTAS;
			$certa = $resposta->IN_CERTA;
		}
		
?>

				<TR>
					<TD>
						<?php textarea("txtResposta$i", "500", $texto,  "100%", "68px", "textbox3", "Resposta"); ?>
					</TD>
					<TD align="center">
						<INPUT type="radio" name="rdoCerta" <?php echo $certa == "1"?"checked":""; echo " value='$i'";  ?>  />
					</TD>
				</TR>

<?php
		
	}


?>
				

<?php
	
}

?>