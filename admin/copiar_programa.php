<?php

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_copia_programa();
}

function carregarRN(){
	
	page::$rn->empresaOrigem = $_POST["cboProgramaOrigem"];
	page::$rn->empresaDestino = $_POST["cboProgramaDestino"];
	
	page::$rn->copiarProva = isset($_POST["chkProva"]);
	page::$rn->copiarSimulado = isset($_POST["chkSimulado"]);
	page::$rn->copiarForum = isset($_POST["chkForum"]);
	page::$rn->copiarPesquisa = isset($_POST["chkPesquisa"]);
	page::$rn->copiarComentario = isset($_POST["chkComentario"]);
	page::$rn->copiarRadar = isset($_POST["chkRadar"]);
	page::$rn->copiarAssessment = isset($_POST["chkAssessment"]);
	
	
}

function pageRenderEspecifico(){
	
?>					<TR>

						<TD class="textblk">Programa Origem (a ser copiado): *</TD>
					</TR>
					<TR>
						<TD><?php comboboxEmpresa("cboProgramaOrigem", "", "Programa Origem") ?></TD>
					</TR>
					<TR>
						<TD colspan="2">&nbsp;</TD>
					</TR>
					<TR>

						<TD class="textblk">Programa Destino: (que copiará do outro) *</TD>
					</TR>
					<TR>
						<TD><?php comboboxEmpresa("cboProgramaDestino", "", "Programa Origem") ?></TD>
					</TR>
					<TR>
						<TD colspan="2">&nbsp;</TD>
					</TR>
					<tr>
						<TD class="textblk">Itens a copiar:</TD>
					</tr>
					<TR>
						<TD class="textblk">
							<input type="checkbox" name="chkProva" />Provas
							&nbsp;
							<input type="checkbox" name="chkSimulado" />Simulados
							&nbsp;
							<input type="checkbox" name="chkForum" />Fóruns
							&nbsp;
							<input type="checkbox" name="chkPesquisa" />Pesquisas
							&nbsp;
							<input type="checkbox" name="chkComentario" />Comentários
							<br/>
							<input type="checkbox" name="chkRadar" />Configuração do Radar
							<input type="checkbox" name="chkAssessment" />Configuração do Assessment
							&nbsp;
						</TD>
					</TR>
<?php

}

?>