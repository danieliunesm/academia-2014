<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

include('paginaBase.php');

function instanciarRN(){
    page::$rn = new col_curso_empresa();
}


function carregarRN(){

    if (isset($_REQUEST["id"])) {
        page::$rn->CD_EMPRESA = $_REQUEST["id"];
    }

    page::$rn->cursos = array();
    page::$rn->areasConhecimento = array();

    reset($_POST);
    while (list($key, $val) = each($_POST)){
        if(substr($key, 0, 3) == "chk"){
            $idcurso = substr($key, 3, strlen($key));
            $cursoEmpresa = new curso_empresa();
            $cursoEmpresa->curso = $idcurso;
            $cursoEmpresa->ordem = $_POST["txto".$idcurso];
            $cursoEmpresa->cor = $_POST["txtc".$idcurso];
            page::$rn->cursos[] = $cursoEmpresa;
        }elseif (substr($key, 0, 3) == "cha"){
            $idarea = substr($key, 3, strlen($key));
            $areaEmpresa = new area_conhecimento_empresa();
            $areaEmpresa->area = $idarea;
            $areaEmpresa->ordem = $_POST["txta".$idarea];
            $areaEmpresa->cor = $_POST["txtx".$idarea];
            page::$rn->areasConhecimento[] = $areaEmpresa;
        }
    }


}

function pageRenderEspecifico(){
    ?>
<script type="text/javascript" src="include/js/jscolor/jscolor.js"></script>
<TR>

    <TD class="textblk">Programa: *</TD>
</TR>
<TR>
    <TD><?php comboboxEmpresa("cboEmpresa", page::$rn->CD_EMPRESA, "Programa", true, false,"textbox3", !isset($_REQUEST["id"])); ?></TD>
</TR>
<tr>
    <td class="textblk">&nbsp;</td>
</tr>

<?php

    $sql = "SELECT
				ac.CD_AREA_CONHECIMENTO,
				ac.NM_AREA_CONHECIMENTO,
				ae.NR_ORDEM,
				ae.CD_EMPRESA,
				ae.TX_COR
            FROM
                            col_area_conhecimento ac
                            LEFT JOIN col_area_conhecimento_empresa ae ON ac.CD_AREA_CONHECIMENTO = ae.CD_AREA_CONHECIMENTO AND ae.CD_EMPRESA = {$_GET["id"]}
            ORDER BY
                            ae.CD_EMPRESA DESC,
                            COALESCE(ae.NR_ORDEM ,999999),
                            ac.NM_AREA_CONHECIMENTO";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

?>

<tr>
    <td class="textblk">�reas de Conhecimento</td>
</tr>
<tr>
    <td>
        <table cellpadding="1" cellspacing="1" border="1" width="100%">

            <?php
            while($linha = mysql_fetch_array($resultado)){

                $checked = verificarCheckArea($linha["CD_AREA_CONHECIMENTO"]);

                $ordem = $linha["NR_ORDEM"];
                $cor = $linha["TX_COR"];

                echo "<tr>
                            <td><input type='checkbox' $checked id='cha{$linha["CD_AREA_CONHECIMENTO"]}' name='cha{$linha["CD_AREA_CONHECIMENTO"]}'></td>
                            <td><input type='text' id='txta{$linha["CD_AREA_CONHECIMENTO"]}' name='txta{$linha["CD_AREA_CONHECIMENTO"]}' maxlength='3' size='3' value='$ordem' /></td>
                            <td>{$linha["NM_AREA_CONHECIMENTO"]}</td>
                            <td><input type='textbox' class='color {required:false,hash:true}' id='txtx{$linha["CD_AREA_CONHECIMENTO"]}' name='txtx{$linha["CD_AREA_CONHECIMENTO"]}' size='6' value='$cor'/>  </td>
                          </tr>";

            }

            ?>

        </table>

    </td>
</tr>

<?php

    $sql = "SELECT
                    pi.ID,
                    pi.Titulo,
                    COALESCE(ce.NR_ORDEM, pi.NR_ORDEM) AS NR_ORDEM,
                    ce.CD_EMPRESA,
                    ce.TX_COR
            FROM
                    tb_pastasindividuais pi
                    LEFT JOIN col_curso_empresa ce ON pi.ID = ce.ID_PASTA_INDIVIDUAL AND CD_EMPRESA = {$_GET["id"]}
            WHERE
                    pi.Status = 1
            ORDER BY
                    ce.CD_EMPRESA DESC,
                    COALESCE(COALESCE(ce.NR_ORDEM, pi.NR_ORDEM),999999),
                    pi.Titulo";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

?>
<tr>
    <td class="textblk">&nbsp;</td>
</tr>
<tr>
    <td class="textblk">Cursos</td>
</tr>
<tr>
    <td>
        <table cellpadding="1" cellspacing="1" border="1" width="100%">

            <?php
                while($linha = mysql_fetch_array($resultado)){

                    $checked = verificarCheck($linha["ID"]);

                    $ordem = $linha["NR_ORDEM"];
                    $cor = $linha["TX_COR"];

                    echo "<tr>
                            <td><input type='checkbox' $checked id='chk{$linha["ID"]}' name='chk{$linha["ID"]}'></td>
                            <td><input type='text' id='txto{$linha["ID"]}' name='txto{$linha["ID"]}' maxlength='3' size='3' value='$ordem' /></td>
                            <td>{$linha["Titulo"]}</td>
                            <td><input type='textbox' readonly='readonly' class='color {required:false,hash:true}' id='txtc{$linha["ID"]}' name='txtc{$linha["ID"]}' size='6' value='$cor'/>  </td>
                          </tr>";

                }

            ?>

        </table>

    </td>
</tr>

<?php

}

function verificarCheck($id){

    if(in_array($id, page::$rn->cursos, true)){
        return "checked";

    }

    return "";

}

function verificarCheckArea($id){

    if(in_array($id, page::$rn->areasConhecimento, true)){
        return "checked";

    }

    return "";

}

?>