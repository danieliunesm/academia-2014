<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";

$searchString = $_SERVER["QUERY_STRING"];

$vemp = isset($_GET["vemp"])?$_GET["vemp"]:1;

function getSTATUScheck($oView){
	switch($oView){
		case 1:
			return "IN_ATIVO = 1";
		case 5:
			return "(IN_ATIVO = 1 OR IN_ATIVO = 3)";
	}
}

function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}

function obterServidor() {
	
	$url = curPageURL();
	
	$posicaoBarra = strpos($url, '/', 7);
	$servidor = substr($url, 0, $posicaoBarra);
	
	return $servidor;
	
}

function escreveEmpresas(){
	global $vemp;
	$iCont   = 0;
	$iTotal  = 0;
	$bgcolor = "#ffffff";
	$usuario = '';
	
	$sql = "SELECT CD_EMPRESA, DS_EMPRESA, IN_ATIVO FROM col_empresa WHERE ".getSTATUScheck($vemp)." ORDER BY DS_EMPRESA";
	$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
	
	$httpServidor = obterServidor();
	
	while($oRs = mysql_fetch_row($query)){
		$strlink = "empresa_id=$oRs[0]";
		echo "<tr bgcolor=\"$bgcolor\">";
		echo "<td class=\"textblk\" style=\"padding-left:40px\">$oRs[1]</td>";
		echo "<td class=\"textblk\" style=\"padding-left:20px;text-align:center\">$oRs[0]</td>";
		//echo "<td class=\"textblk\" style=\"padding-left:40px\">http://www.colaborae.com.br/cadastro1_empresa.php?b=" . md5($strlink) . "</td>";
		echo "<td class=\"textblk\" style=\"padding-left:40px\"><a target=\"_blank\" href=\"$httpServidor/boas_vindas_login.php?b=" . md5($strlink) . "\">$httpServidor/boas_vindas_login.php?b=" . md5($strlink) . "</a></td>";
?>
	<td class="item"><input type="radio" name="EMP<? echo $oRs[0]; ?>" value="1" <? if($oRs[2] == 1) echo "checked"; ?> onfocus="noFocus(this)"></td>
	<td class="item"><input type="radio" name="EMP<? echo $oRs[0]; ?>" value="3" <? if($oRs[2] == 3) echo "checked"; ?> onfocus="noFocus(this)"></td>
	<td class="item"><a href="#" onclick="getURL(<? echo $oRs[0]; ?>,2);return false" onfocus="noFocus(this)"><img src="/admin/images/bt_edit.gif" width="20" height="20" alt="   Editar   "></a></td>
</tr>
<?
		$iCont++;
		$iTotal++;
		if($iCont % 2 == 0) $bgcolor = "#ffffff"; else $bgcolor = "#f1f1f1";
	}
	echo "<input type=\"hidden\" name=\"Nemp\" value=\"" . $iTotal . "\">";
	mysql_free_result($query);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>Colabor&aelig; - Consultoria e Educa��o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/admin/include/css/adminstyles.css">
<script type="text/javascript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script type="text/javascript" src="/admin/include/js/dyntable.js"></script>
<script language="JavaScript">
function init(){
<?
if($_SESSION["msg"] != "")
{
	echo "	alert(\"" . $_SESSION["msg"]. "\");";
	$_SESSION["msg"] = "";
}
?>
}

function ShowDialog(pagePath, args, width, height, left, top){
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:yes;status:no");
}

oVemp = <? echo $vemp; ?>;
function viewControl(vSession){
	oVsession = vSession;
	var html = ShowDialog("/admin/htmleditor/dialog/viewempresacontrol.html", window, 420, 180);
	if(html)document.location.href=html;	
}

function openWindow(url,w,h,l,t){
	newWin=null;
	if(!l)l=(screen.width-w)/2;
	if(!t)t=(screen.height-h)/2;
	newWin=window.open(url,'htmleditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
	if(newWin!=null)setTimeout('newWin.focus()',100);
}

function getURL(i,a){
	//openWindow('/admin/htmleditor/input_empresas.php?id='+i+'&action='+a,(700),(300),10,20); //screen.width-40, screen.height-80
	openWindow('cadastro_empresa.php?id='+i,(700),(screen.height-80),10,20); //screen.width-40, screen.height-80
}

function submitPage(url,tgt){
	queryorder = oVemp>1?('?vemp='+oVemp):'';
	document.homeForm.action=url+queryorder;
	document.homeForm.target=tgt;
	document.homeForm.submit();
}

function checkChecked(){
	f   = document.homeForm;
	chk = false;
	for(var i=0; i < f.length; i++){
		if(f[i].type == 'radio' && f[i].name.indexOf('EMP')!=-1){
			if(f[f[i].name][1].checked){
				chk = true;
				break;
			}
		}
	}
	return chk;
}

function delRegisters(url){
	queryorder = oVemp>1?('?vemp='+oVemp):'';
	if(!checkChecked(document.homeForm)){
		alert('N�o h� Programas exclu�dos!     \nPara verificar se h� Programas exclu�dos, clique em "Exibir Programas" e assinale a op��o "Removidas".     ');
		return false;
	}
	else{
		if(confirm('Confirma a dele��o dos Programas assinalados? (Esta opera��o n�o poder� ser desfeita.)     ')){
			document.homeForm.action='/admin/delempresaregisters.php?'+queryorder;
			document.homeForm.target='';
			document.homeForm.submit();
		}
		else return false;
	}
}

function abrirImportacao(){
	newWin=null;
	var w=520;
	var h=350;
	var l=(screen.width-w)/2;
	var t=(screen.height-h)/2;
	newWin=window.open('cadastro_hierarquia_importacao.php','importacaoEditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
	if(newWin!=null)setTimeout('newWin.focus()',100);
}

window.onload = init;
</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><strong>USU�RIO:&nbsp;</strong><? echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="4"></td></tr>
<tr><td class="textblk" colspan="3"><span class="title">PROGRAMAS</span></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="1"></td></tr>
</table>
<br>
<form name="homeForm" action="" method="post">
<table class="home" id="pgiTable">
<tr><td class="tarjaTitulo" colspan="6">PROGRAMAS - Listagem por Ordem Alfab�tica Crescente de Nome</td></tr>
<tr>
	<td><a href="#" onclick="viewControl('vemp');return false" onfocus="noFocus(this)"><img src="images/bt_all.gif" width="22" height="26" align="absmiddle"><span>&nbsp;Exibir Programas</span></a></td>
	<td></td>
	<td colspan="4" style="padding-left:20px"><a href="#" onclick="getURL('',1);return false" onfocus="noFocus(this)"><img src="images/bt_novo.gif" width="20" height="20" vspace="10" align="absmiddle"><span>&nbsp;Incluir Programa</span></a>
	<span style="width:180px;">&nbsp;</span> <a href="#" onclick="abrirImportacao();return false" onfocus="noFocus(this)"><img src="/images/layout/excel2.jpg" width="20" height="20" border="0" vspace="10" align="absmiddle"><span>Importar&nbsp;Hierarquia</span></a>
	</td>
	
</tr>
<tr class="tarjaItens">
	<td class="title2" rowspan="2" style="text-align:left;padding-left:40px;width:10%"><span>&nbsp;PROGRAMA</span></td>
	<td class="title2" rowspan="2" style="text-align:left;padding-left:20px;width:10%;text-align:center"><span>&nbsp;ID</span></td>
	<td class="title2" rowspan="2" style="text-align:left;padding-left:40px;width:70%"><span>&nbsp;LINK</span></td>
	<td class="title" colspan="2">STATUS</td>
 	<td></td>
</tr>
<tr class="tarjaItens">
	<td align="center"><img class="headerimg" src="images/bt_publicar.gif" width="32" height="32" alt="   publicar   "></td>
	<td align="center"><img class="headerimg" src="images/bt_remover.gif" width="32" height="32" alt="   excluir provisoriamente  "></td>
	<td align="center"><img class="headerimg" src="images/bt_editar.gif" width="32" height="32" alt="   editar   "></td>
</tr>
<tr class="fioItens"><td width="1%"><img src="images/blank.gif" width="200" height="10"></td><td colspan="4"></td></tr>
<?
escreveEmpresas();
?>
<tr class="fioTitulo"><td colspan="6"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="1" height="40"></td></tr>
<tr><td align="center" style="font-family:tahoma,arial,helvetica,sans-serif;font-size:11px;color:#000;white-space:nowrap"><input type="button" class="buttonsty" value="Atualizar Status" onclick="submitPage('/admin/updateempresastatus.php','')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttondelsty" value="" onclick="delRegisters()" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'">Excluir definitivamente<img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"><br><br><br></td></tr>
</table>
</form>
</body>
</html>
<?
mysql_close();
?>