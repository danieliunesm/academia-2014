<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="JavaScript">
<?
$SUBDIR = isset($_GET["subdir"])?$_GET["subdir"]:(isset($_POST["subdir"])?$_POST["subdir"]:'');

$urlsubdir = $SUBDIR;

if($SUBDIR != '') $SUBDIR = $SUBDIR . '/';

$IMAGES_BASE_URL = '/images/';
$IMAGES_BASE_DIR = getDocumentRoot().$IMAGES_BASE_URL.$SUBDIR;


$fdel = isset($_POST["fdel"])?$_POST["fdel"]:false;

if($fdel){
	reset($_POST); 
	while(list($key, $val) = each($_POST))
	{
		if(substr($key, 0, 3) == "chk")
		{
			unlink($IMAGES_BASE_DIR.$val);
//			echo $IMAGES_BASE_DIR.$val\n;
		}
	}
}

function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/admin/htmleditor/filemanager/browse/browse4.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

function walk_dir($path)
{
	$retval = array();
	if ($dir = opendir($path))
	{
		while(false !== ($file = readdir($dir)))
		{
			if($file[0]==".") continue;
			if(is_file($path."/".$file)) $retval[]=$path."/".$file;
		}
		closedir($dir);
	}
	return $retval;
}

function CheckImgExt($filename)
{
	$img_exts = array("gif","jpg", "jpeg","png");
	foreach($img_exts as $this_ext)
	{
		if(preg_match("/\.$this_ext$/", $filename))	return TRUE;
	}
	return FALSE;
}

$files = array();
foreach(walk_dir($IMAGES_BASE_DIR) as $file)
{
	$file = preg_replace("#//+#", '/', $file);
	$IMAGES_BASE_DIR = preg_replace("#//+#", '/', $IMAGES_BASE_DIR);
	$file = preg_replace("#$IMAGES_BASE_DIR#", '', $file);
	if(CheckImgExt($file)) $files[] = $file;	//adicionando arquivo ao array de arquivos
}

if($files) sort($files);	//ordenando array

// gerando lista $html_img_lst
$html_img_lst = '';
$i			  = 0;
foreach($files as $file)
{
	$i++;
	$html_img_lst .= "<input type=\"checkbox\" name=\"chk$i\" value=\"$file\"><a href=\"javascript:getImage('$file');\">$file</a><br>\n";
}
?>

var sImagesPath  = "/images/<? echo $SUBDIR; ?>";
var sActiveImage = "" ;

function getImage(imageName)
{
	sActiveImage = sImagesPath + imageName;
	document.getElementById('imgPreview').src = sActiveImage;
	if(checkBorder.checked&&document.getElementById('imgPreview').src!='')document.getElementById('imgPreview').style.border="1px solid #999999";
	document.getElementById('nameLayer').innerHTML = "<span class='textbld'>" + imageName + "</span>";
}

function setBorder(b)
{
	if(document.getElementById('nameLayer').innerHTML!="")
	{
		if(b)document.getElementById('imgPreview').style.border="1px solid #999999";
		else document.getElementById('imgPreview').style.border="none"
	}
}

function ok()
{	
	window.setImage(sActiveImage) ;
	window.close() ;
}

function delFiles(){
	var f   = document.fileForm;
	var chk = false;
	for(var i=0; i < f.length; i++){
		if(f[i].type == 'checkbox' && f[i].checked == true){
			chk = true;
			break;
		}
	}
	if(chk){
		if(confirm('Confirma dele��o de arquivos assinalados? (Esta opera��o n�o poder� ser desfeita.)     ')){
			document.fileForm.fdel.value = true;
			document.fileForm.submit();
		}
		else return false;
	}
	else{
		alert('� preciso haver ao menos 1 arquivo assinalado!     ');
		return false;
	}
}

function setChecked(obj){
	document.location.href = 'browse4.php?subdir='+obj.value;
}
</script>
</head>
<body style="margin:0px" scroll="no" bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="10" width="100%" height="100%">
<tr>
<td>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><input type="radio" name="subdir" value="" onfocus="noFocus(this)" onmousedown="if(!this.checked)setChecked(this)" <?php if($SUBDIR == '') echo "checked"; ?>></td>
	<td class="textblk">Geral&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td><input type="radio" name="subdir" value="banners" onfocus="noFocus(this)" onmousedown="if(!this.checked)setChecked(this)" <?php if($SUBDIR == 'banners/') echo "checked"; ?>></td>
	<td class="textblk">Banners&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td><input type="radio" name="subdir" value="colaboradores" onfocus="noFocus(this)" onmousedown="if(!this.checked)setChecked(this)" <?php if($SUBDIR == 'colaboradores/') echo "checked"; ?>></td>
	<td class="textblk">Fotos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td><input type="radio" name="subdir" value="colaboradores/miniaturas" onfocus="noFocus(this)" onmousedown="if(!this.checked)setChecked(this)" <?php if($SUBDIR == 'colaboradores/miniaturas/') echo "checked"; ?>></td>
	<td class="textblk">Miniaturas</td>
	</tr>
	</table>
</td>
</tr>
<tr>
	<td width="70%"><div id="nameLayer" style="float:left"></div></td>
	<td width="30%"><table border="0" cellpadding="0" cellspacing="0"><tr><td><input name="checkBorder" type="checkbox" onclick="if(this.checked)setBorder(1);else setBorder(0)" onfocus="noFocus(this)"></td><td class="textblk">&nbsp;visualizar&nbsp;borda</td></tr></table></td>
</tr>
<form name="fileForm" action="browse4.php?subdir=<? echo $urlsubdir; ?>" method="post">
<tr>
	<td>
		<div id="imgLayer" style="float:center;width:100%;height:400px;overflow:auto;border:1px solid #666666"><table border="0" cellpadding="10" cellspacing="0" width="100%" height="100%"><tr><td align="center"><img src="/images/layout/blank.gif" id="imgPreview"></td></tr></table></div>
	</td>
	<td>
		<div id="listLayer" style="float:center;width:100%;height:400px;overflow:auto"><? echo $html_img_lst ?></div>
	</td>
</tr>
<tr>
	<td align="center">
		<!-- <input type="button" class="buttonsty" value="OK" onclick="ok()" onfocus="noFocus(this)">&nbsp;&nbsp;&nbsp;&nbsp; -->
		<input type="button" class="buttonsty" value="Fechar" onclick="window.close()" onfocus="noFocus(this)">
	</td>
	<td style="width:210px;font-family:tahoma,arial,helvetica,sans-serif;font-size:11px;white-space:nowrap"><input type="button" class="buttondelsty" value="" onclick="delFiles()" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'">Deletar arquivos assinalados<!-- <img src="/images/layout/blank.gif" width="210" height="1"> --></td>
</tr>
<input type="hidden" name="fdel" value="false">
</form>
</table>

</body>
</html>
