<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="JavaScript">
function delFiles(){
	var f   = document.fileForm;
	var chk = false;
	for(var i=0; i < f.length; i++){
		if(f[i].type == 'checkbox' && f[i].checked == true){
			chk = true;
			break;
		}
	}
	if(chk){
		if(confirm('Confirma dele��o de arquivos assinalados? (Esta opera��o n�o poder� ser desfeita.)     ')){
			document.fileForm.fdel.value = true;
			document.fileForm.submit();
		}
		else return false;
	}
	else{
		alert('� preciso haver ao menos 1 arquivo assinalado!     ');
		return false;
	}
}
</script>
</head>
<body scroll="no" bgcolor="#ffffff">
<?
$IMAGES_BASE_URL = '/dbquestions/';
$IMAGES_BASE_DIR = getDocumentRoot().$IMAGES_BASE_URL;

$fdel = isset($_POST["fdel"])?$_POST["fdel"]:false;

if($fdel){
	reset($_POST); 
	while(list($key, $val) = each($_POST))
	{
		if(substr($key, 0, 3) == "chk")
		{
			unlink($IMAGES_BASE_DIR.$val);
//			echo $IMAGES_BASE_DIR.$val\n;
		}
	}
}

function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/admin/htmleditor/filemanager/browse/browse_dbquestions.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

function walk_dir($path)
{
	$retval = array();
	if ($dir = opendir($path))
	{
		while(false !== ($file = readdir($dir)))
		{
			if($file[0]==".") continue;
			if(is_file($path."/".$file)) $retval[]=$path."/".$file;
		}
		closedir($dir);
	}
	return $retval;
}

function CheckImgExt($filename)
{
	$img_exts = array("docx"); 
	foreach($img_exts as $this_ext)
	{
		if(preg_match("/\.$this_ext$/", $filename))	return TRUE;
	}
	return FALSE;
}

$files = array();
foreach(walk_dir($IMAGES_BASE_DIR) as $file)
{
	$file = preg_replace("#//+#", '/', $file);
	$IMAGES_BASE_DIR = preg_replace("#//+#", '/', $IMAGES_BASE_DIR);
	$file = preg_replace("#$IMAGES_BASE_DIR#", '', $file);
	if(CheckImgExt($file)) $files[] = $file;	//adicionando arquivo ao array de arquivos
}

if($files) sort($files);	//ordenando array

// gerando lista $html_img_lst
$html_img_lst = '';
$i			  = 0;
foreach($files as $file)
{
	$i++;
//	$html_img_lst .= "<span class=\"text\" style=\"white-space:nowrap\">$file</span><br>\n";
	$html_img_lst .= "<input style=\"float:left;margin-top:2px\" type=\"checkbox\" name=\"chk$i\" value=\"$file\" title=\"   Selecione para excluir este arquivo   \"><a style=\"white-space:nowrap\" href=\"javascript:void(0)\" onclick=\"self.opener.setQuestionsFile('$file');self.close()\" title=\"   Clique aqui para usar este arquivo   \"><strong>Usar:</strong>&nbsp;&nbsp;</a><a style=\"white-space:nowrap\" href=\"$IMAGES_BASE_URL$file\" target=\"_blank\" title=\"   Clique aqui para visualizar este arquivo   \">$file</a><br />\n";
}
?>
<table border="0" cellpadding="0" cellspacing="20" width="100%" height="100%">
<form name="fileForm" action="browse_dbquestions.php" method="post">
<tr>
<td valign="top">
	<div id="listLayer" style="float:center;width:758px;height:270px;overflow:auto;line-height:1.4em"><? echo $html_img_lst ?></div>
</td>
</tr>
<tr>
<td style="font-family:tahoma,arial,helvetica,sans-serif;font-size:11px;white-space:nowrap"><input type="button" class="buttondelsty" value="" onclick="delFiles()" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'">Deletar arquivos assinalados</td>
</tr>
<tr>
<td align="center">
	<input type="button" class="buttonsty" value="Fechar" onclick="window.close()" onfocus="noFocus(this)">
</td>
</tr>
<input type="hidden" name="fdel" value="false">
</form>
</table>
</body>
</html>
