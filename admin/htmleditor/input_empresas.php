<?
include "../../include/security.php";
include "../../include/defines.php";
include "../../include/dbconnection.php";
include "../../include/genericfunctions.php";
include "htmleditor.php";
include "../controles.php";
include "../page.php";


$id 	= $_GET["id"];
$action	= $_GET["action"];
global $ciclos;

function formataData($valor)
{
	
	$valor = strftime("%d/%m/%Y", strtotime($valor));
	return $valor;
}

function EscreveDados($oID,$oAction)
{
	global $ohtmleditor, $titulo, $id, $dataVigenciaInicio, $dataVigenciaTermino;

	if($oAction == 2)
	{
		$sql = "SELECT DS_EMPRESA, CD_EMPRESA, DT_INICIO_VIGENCIA, DT_TERMINO_VIGENCIA FROM col_empresa WHERE CD_EMPRESA = " . $oID;
		$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

		if($oRs = mysql_fetch_row($RS_query))
		{
			$titulo 			= $oRs[0];
			//$ohtmleditor->Value = $oRs[1];
			$id				    = $oRs[1];
			$dataVigenciaInicio = formataData($oRs[2]); 
			$dataVigenciaTermino= formataData($oRs[3]);
		}
		mysql_free_result($RS_query);
	}
}

$ohtmleditor = New htmleditor;
$ohtmleditor->ToolbarSet = 'thisHtmlEditorToolbarSet';

EscreveDados($id,$action);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<style type="text/css">
.title{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}
.text{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.textblk{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.data{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:8pt;font-weight:bold;color:#ffffff}
.legenda{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#cc0000}
.buttonsty{width:150px;height:19px;background-color:#dddddd;border:2px solid #000099;font-family:"verdana","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000;cursor:hand;line-height:11px}
</style>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script type="text/javascript" src="../include/js/functions.js"></script>
<script language="javascript" src="js/dhtmled.js"></script>
<script language="JavaScript">
loaded=false;
function checkValues(frm)
{
	var msg="";
	if(frm.titulo.value=="")msg+="Necess�rio digitar o nome da Empresa!   \n";
	if(msg!="")
	{
		alert(msg);
		return false;
	}
	else return true;
}

function submitPage(url,tgt)
{
	if(!checkValues(f))return false;
	f.action=url;
	f.target=tgt;
	f.submit();
}

function init()
{
	document.onkeypress=mascara;
	document.onmousedown=checkSrc;
	document.onmouseup=releaseSrc;
	document.onselectstart=unselectElement;
	f=document.inputForm;
	loaded=true;
	setTimeout('document.inputForm.titulo.focus()',300);
}


						
function adicionarCiclo(){

	var no = document.getElementById("dvCiclo").cloneNode(true);
	var dv = document.getElementById("dvCiclos");
	
	//alert(no.all.length)
	
	var itens = no.all.length;
	
	for (i=0; i<itens; i++){
		if (no.all[i].type == "text"){
			no.all[i].value = "";
		}else if (no.all[i].type == "select-one"){
			no.all[i].selectedIndex = 0;
		}
	}
	
	dv.appendChild(no);
	
}

function removerCiclo(obj){
	if (document.getElementById("dvCiclos").children.length==1){
		alert('Deve existir ao menos um ciclo.')
		return;
	}
	
	 document.getElementById("dvCiclos").removeChild(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
	
	//alert(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
		
}
						
						
		

</script>
</head>
<body style="margin:0px;border:none" scroll="no" onload="init()">

<form name="inputForm" enctype="multipart/form-data" action="" method="post">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr height="1%">
<td>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
</table>

</td></tr>
<tr><td>
<table border="0" cellpadding="0" cellspacing="15" style="background-color:buttonface;border:none;width:100%;height:100%" align="center">
<tr>
<td height="1%" colspan="2">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr><td class="title" colspan="2" nowrap>EMPRESAS</td></tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>
	</table>
</td>
</tr>
<tr>
	<td class="textblk" width="1%" nowrap>Nome da Empresa*:&nbsp;</td>
	<td ><input type="text" name="titulo" size="120" maxlength="120" style="width:350px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="5" value='<? echo $titulo; ?>'></td>
</tr>


<tr>
	<td height="1%" class="textblk" style="font-weight:normal;" nowrap>
		Logotipo da Empresa:
	</td>
	<td>
		<input type="file" class="textbox8e" name="flLogo" size="26" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="100" tabIndex="35">
		<?php
			if ($id != "" && file_exists(getDocumentRoot()."/canal/images/empresas/logo$id.png"))
			{
				echo "<a href='/canal/images/empresas/logo$id.png' target='foto'>Visualizar Logotipo</a>";
			};
		?>
	</td>
</tr>
<tr>
	<td class="textblk" nowrap>
		Per�odo de Vig�ncia:
	</td>
	<td>
		<?php textbox("txtDataInicioVigencia", "10", $dataVigenciaInicio, "110px", "textbox3", "In�cio Realiza��o", Mascaras::Data) ?>
		a
		<?php textbox("txtDataTerminoVigencia", "10", $dataVigenciaTermino, "110px", "textbox3", "In�cio Realiza��o", Mascaras::Data) ?>
	</td>
</tr>
<?php /*
<tr>
	<td colspan="2" class="textblk">
		Ciclos: <a href="javascript:adicionarCiclo()">Adicionar Ciclos</a>
	</td>

</tr>

<tr>
	<td colspan="2" class="textblk">
		<div id="dvCiclos">
							<?php
							
								$ciclos = null;
							
								$linhas = 1;
								
								if ($action == 2 )
								{
									$sql = "SELECT * FROM col_ciclo WHERE CD_EMPRESA = $id";
									$RS_ciclo = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
									
									$ciclos = array();
									
									while ($linha = mysql_fetch_array($RS_ciclo)) {
										
										
										
									}
									
								}
							
								
								
								
								
								$ciclos = page::$rn->ciclos;
								
								if (is_array($ciclos))
								{
									if (count($ciclos) > 1)
										$linhas = count($ciclos);
								}
								
								for ($i=0; $i < $linhas; $i++)
								{
									$nomeCiclo = "";
									$dataInicio = "";
									$dataTermino = "";
									$codigoCiclo = "";
									
									if (is_array($ciclos))
									{
										$ciclo = $ciclos[$i];
										$nomeCiclo = $ciclo->NM_CICLO;
										$dataInicio = $ciclo->DT_INICIO;
										$dataTermino = $ciclo->DT_TERMINO;
										$codigoCiclo = $ciclo->CD_CICLO;
										
									}
									
								
							
							?>
	
	
			<div id="dvCiclo" style="border: 1px solid black; margin-top: 2px;width:70%" >
				<table cellpadding="2" cellspacing="0" width="98%">
					<tr>
						<td class="textblk" >Ciclo:</td>
						<td rowspan="4" class="textblk" align="center"><input type="image" src="/images/layout/bt_delsession.gif" value="" onclick="javascript:removerCiclo(this)" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'"></td>
					</tr>												
					<tr>
						<td class="textblk" ><?php textbox("txtCiclo[]", "100", $nomeCiclo, "100%", "textbox3", "Ciclo"); ?></td>
					</tr>
					<tr>
						<td class="textblk" >Per�odo:</td>
					</tr>
					<tr>
						<td class="textblk" >
							De <?php textbox("txtDataInicio[]", "10", $dataInicio, "100px", "textbox3", "Data de In�cio", Mascaras::Data) ?>
							At�	<?php textbox("txtDataTermino[]", "10", $dataTermino, "100px", "textbox3", "Data de T�rmino", Mascaras::Data) ?>
							<input type="hidden" id="hdnCodigoCiclo[]" name="hdnCodigoCiclo[]" value="<?php echo $codigoCiclo; ?>" />
						</td>
					</tr>
				</table>
			</div>
			
			<?php
				}
			?>
			
		</div>
			
	</td>
</tr>
*/?>
<tr><td height="1%" colspan="2">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td align="center" ><input type="button" class="buttonsty" value="Cancelar" onclick="self.close()" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Submeter dados" onclick="<?php echo $funcaoJs;?>submitPage('/admin/updateempresa.php','')" onfocus="noFocus(this)">
</td></tr>
</table>
</td></tr>
</table>
</td></tr></table>
<input type="hidden" name="id" value="<? echo $id; ?>">
</form>
</body>
</html>
<?

function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/admin/htmleditor/input_empresas.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

mysql_close();
?>