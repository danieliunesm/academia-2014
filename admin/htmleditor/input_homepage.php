<?
include "../../include/security.php";
include "../../include/defines.php";
include "../../include/dbconnection.php";
include "../../include/genericfunctions.php";
include "htmleditor.php";

$id 		= $_GET["id"];
$action 	= $_GET["action"];
$coluna		= 0;
$tipo   	= 1;
$tipoLink 	= 2;

function EscreveDados($oID,$oAction)
{
	global $ohtmleditor, $ochamadaeditor, $titulo, $data, $coluna, $tipoLink, $Link, $chamada;
	if($oAction == 2)
	{
		$sql = "SELECT TITULO, CHAMADA, COLUNA, TIPO_LINK, LINK, DATA, CONTEUDO, MAX(COLUNA) FROM tb_homepage WHERE ID = " . $oID;
		$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
		if($oRs = mysql_fetch_row($RS_query))
		{
			$titulo	 		  	 	= $oRs[0];
			$ochamadaeditor->Value	= $oRs[1];
			$coluna			  	 	= $oRs[2];
			$tipoLink		  	 	= $oRs[3];
			$Link				  	= $oRs[4];
			$data 			  	 	= $oRs[5];
			$ohtmleditor->Value 	= $oRs[6];
			
			if($coluna == 0)
				$coluna = $maxCol;
		}
		mysql_free_result($RS_query);
	}
	else{
		$sql = "SELECT MAX(COLUNA) FROM tb_homepage";
		$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
		if($oRs = mysql_fetch_row($RS_query))
		{
			$coluna	= $oRs[0] + 1;
		}
		mysql_free_result($RS_query);
	}
}

$ohtmleditor = New htmleditor;
$ohtmleditor->ToolbarSet = 'thisHtmlEditorToolbarSet';

$ochamadaeditor = New htmleditor;
$ochamadaeditor->ToolbarSet = 'HPEditorToolbarSet';

EscreveDados($id,$action);
if(empty($data)) $data = date("d/m/Y");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<style type="text/css">
.title{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}
.text{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.textblk{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.data{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:8pt;font-weight:bold;color:#ffffff}
.legenda{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#cc0000}
.buttonsty{width:150px;height:19px;background-color:#dddddd;border:2px solid #000099;font-family:"verdana","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000;cursor:hand;line-height:11px}
.textbox5{BORDER-RIGHT: #003 1px solid; BORDER-TOP: #003 1px solid; BORDER-LEFT: #003 1px solid; WIDTH: 32px; COLOR: #333; BORDER-BOTTOM: #003 1px solid; HEIGHT: 19px; BACKGROUND-COLOR: #fff; TEXT-ALIGN: right}
</style>
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="javascript" src="js/dhtmled.js"></script>
<script language="JavaScript">
loaded = false;
coluna = <? echo $coluna; ?>;
tpLink = <? echo $tipoLink; ?>;

function ShowDialog(pagePath, args, width, height, left, top)
{
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
}

function checkValues(frm)
{
	var msg="";
	chamadavalue = document.frames['chamadaFrame'].document.getElementById('objContent').DOM.body.innerText;
	if((tpLink==1)&&frm.titulo.value=="")msg+="� necess�rio haver um t�tulo para a coluna!   \n";
	if(chamadavalue=='')msg+="Necess�rio haver uma chamada para a coluna!   \n";	
	if((tpLink==2||tpLink==0)&&frm.Link.value=="")msg+="Necess�rio haver um link para a coluna!   \n";
	if(msg!="")
	{
		alert(msg);
		return false;
	}
	else return true;
}

function setTipoLink(tL)
{
	tpLink=tL;
	if(tpLink==1) // editor
	{
		document.getElementById('inputTable').style.height='100%';
		document.getElementById('inputTD').style.display='inline';
		f.Link.disabled=true;
		f.Link.style.backgroundColor='buttonface';
		setViewFileButton(0);
	}
	else // externo ou paginas site ou sem link
	{
		document.getElementById('inputTable').style.height='';
		document.getElementById('inputTD').style.display='none';
		
		if(tL==2)setViewFileButton(1); // 2 paginas site
		else setViewFileButton(0);
		
		if(tL==3)
		{
			f.Link.disabled=true;
			f.Link.style.backgroundColor='buttonface';
		}
		else
		{
			f.Link.disabled=false;
			f.Link.style.backgroundColor='#ffffff';
		}
	}
	f.titulo.focus();
}

function setViewFileButton(st)
{
	var obj = document.getElementById('viewfile');
	if(st)
	{
		obj.style.filter='alpha(opacity=100)';
		obj.style.cursor='hand';
		obj.onmouseover=function(){this.style.backgroundColor='#99ccff';this.style.borderColor='#666666'};
		obj.onmouseout=function(){this.style.backgroundColor='buttonface';this.style.borderColor='#dddddd'};
		obj.onclick=function(){selectURL()};
		
		f.regstatus[0].disabled=false;
		f.regstatus[1].disabled=false;
		f.regstatus[2].disabled=false;
	}
	else
	{
		obj.style.filter='gray() alpha(opacity=30)';
		obj.style.cursor='default';
		obj.style.backgroundColor='buttonface';
		obj.style.borderColor='#dddddd';
		obj.onmouseover=function(){return false};
		obj.onmouseout=function(){return false};
		obj.onclick=function(){return false};
		
		f.regstatus[0].disabled=true;
		f.regstatus[1].disabled=true;
		f.regstatus[2].disabled=true;
	}
}

function selectURL()
{
	var sURL = selectLink();
	if(sURL)f.Link.value = sURL ;
}

function selectLink()
{
	var st=f.regstatus[0].checked?1:(f.regstatus[1].checked?2:3);
	return showModalDialog("dialog/treeview2.php?st="+st, window, "dialogWidth:720px;dialogHeight:600px;help:no;scroll:auto;status:no");
}

function submitPage(url,tgt)
{
	if(!checkValues(f))return false;
	f.action=url;
	f.target=tgt;
	
	if(url.indexOf('preview')!=-1)
	{
		if(tpLink==3)return false;
		switch(tpLink)
		{
			case 0: // 0->link externo
				if(f.Link.value.indexOf('http://')==-1)f.Link.value="http://"+f.Link.value;
				window.open(f.Link.value,'','');
				break;
			case 2: // 2->pg site
				window.open(f.Link.value,'','');
				break;
			case 1: // 1->editor
				f.submit();
				break;
		}
	}
	else f.submit();
}

function init()
{
	document.onkeypress=mascara;
	document.onmousedown=checkSrc;
	document.onmouseup=releaseSrc;
	document.onselectstart=unselectElement;
	f=document.inputForm;
	loaded=true;
<?
	if($tipoLink==1 || $tipoLink==3) echo "	setTimeout('f.titulo.focus()',300);"; else echo "	setTimeout('f.Link.focus()',300);";
?>
}
</script>
</head>
<body style="background-color:buttonface;margin:0px;border:none" scroll="no" onload="init()">

<form name="inputForm" action="" method="post">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr height="1%">
<td>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
</table>

</td></tr>
<tr><td valign="top">
<table id="inputTable" border="0" cellpadding="0" cellspacing="15" style="background-color:buttonface;border:none;width:100%<? if($tipoLink==1) echo ";height:100%"; ?>" align="center">
<tr>
<td height="1%">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr><td class="title" colspan="2">HOME PAGE: COLUNAS</td></tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr valign="top">
	<td>
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td class="textblk" align="right">Coluna:&nbsp;</td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<!-- <td><input type="radio" name="col" value="0" <? //if($coluna==0) echo "checked"?> onfocus="noFocus(this)" style="margin-left:-3px"></td>
			<td class="textblk">&nbsp;1&nbsp;&nbsp;&nbsp;</td>
			<td><input type="radio" name="col" value="1" <? //if($coluna==1) echo "checked"?> onfocus="noFocus(this)"></td>
			<td class="textblk">&nbsp;2&nbsp;&nbsp;&nbsp;</td>
			<td><input type="radio" name="col" value="2" <? //if($coluna==2) echo "checked"?> onfocus="noFocus(this)"></td>
			<td class="textblk">&nbsp;3&nbsp;&nbsp;&nbsp;</td>
			<td><input type="radio" name="col" value="3" <? //if($coluna==3) echo "checked"?> onfocus="noFocus(this)"></td>
			<td class="textblk">&nbsp;4</td> -->
			
			<td><input type="text" name="col" value="<?php echo $coluna; ?>" class="textbox5"></td>
			
			</tr>
			</table>
		</td>
		<td><img src="/images/layout/blank.gif" width="20" height="1"></td>
		</tr>
		<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
		<tr>
		<td class="textblk" align="right" width="1%" nowrap>Link:&nbsp;</td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><input type="radio" name="tipoLink" value="1" onfocus="noFocus(this)" <? if($tipoLink==1) echo "checked"; ?> onclick="setTipoLink(1)" style="margin-left:-3px" disabled></td>
			<td class="textblk">Editor&nbsp;&nbsp;&nbsp;</td>
			<td><input type="radio" name="tipoLink" value="0" onfocus="noFocus(this)" <? if($tipoLink==0) echo "checked"; ?> onclick="setTipoLink(0)" style="margin-left:-3px"></td>
			<td class="textblk">URL&nbsp;ext.&nbsp;&nbsp;</td>
			<td><input type="radio" name="tipoLink" value="3" onfocus="noFocus(this)" <? if($tipoLink==3) echo "checked"; ?> onclick="setTipoLink(3)" style="margin-left:-3px"></td>
			<td class="textblk">Sem&nbsp;link&nbsp;&nbsp;&nbsp;</td>
			<td><input type="radio" name="tipoLink" value="2" onfocus="noFocus(this)" <? if($tipoLink==2) echo "checked"; ?> onclick="setTipoLink(2)" style="margin-left:-3px"></td>
			<td class="textblk">Link&nbsp;para&nbsp;p�ginas&nbsp;do&nbsp;site</td>
			<td align="right"><img src="/images/layout/blank.gif" width="4" height="23" align="absmiddle"><img id="viewfile" src="/images/layout/viewfile2.gif" width="21" height="21" style="border:1px solid #E6E5D7" align="absmiddle" alt="  Pesquisar no site  " <? if($tipoLink!=2) echo "style=\"cursor:default;filter:gray() alpha(opacity=30)\""; else echo "onmouseover=\"this.style.backgroundColor='#99ccff';this.style.borderColor='#666666'\" onmouseout=\"this.style.backgroundColor='buttonface';this.style.borderColor='#dddddd'\" onclick=\"selectURL()\" style=\"cursor:hand\""; ?>></td>
			</tr>
			</table>
		</td>
		</tr>		
		<tr>
		<td></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><input type="text" name="Link" maxlength="255" style="width:190px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="4" onfocus="srcflag=true;this.select()" onblur="srcflag=false" value="<? if($action==2 && $tipoLink!=1) echo $Link; ?>" <? if($tipoLink==1 || $tipoLink==3) echo "style=\"background-color:buttonface\" disabled"; ?>></td>
			<td>&nbsp;&nbsp;&nbsp;<img src="/images/layout/setacorner.gif" width="9" height="23"></td>
			<td><input type="radio" name="regstatus" value="1" onfocus="noFocus(this)" checked <?if($tipoLink!=2) echo "disabled"; ?>></td>
			<td class="textblk">publicados&nbsp;</td>
			<td><input type="radio" name="regstatus" value="2" onfocus="noFocus(this)" <?if($tipoLink!=2) echo "disabled"; ?> style="display:none"></td>
			<td><input type="radio" name="regstatus" value="3" onfocus="noFocus(this)" <?if($tipoLink!=2) echo "disabled"; ?>></td>
			<td class="textblk">exclu�dos</td>
			</tr>
			</table>
		</td>
		</tr>
		<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
		<tr>
		<td class="textblk" align="right" width="1%" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T�tulo*:&nbsp;</td>
		<td><input type="text" name="titulo" size="67" maxlength="120" style="width:366px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="5" value='<? echo $titulo; ?>'></td>
		<td></td>
		</tr>
 		<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
		<tr>
		<td class="textblk" align="right"><br><br>Chamada:&nbsp;</td>
		<td>
<?
$ochamadaeditor->Createhtmleditor('chamada', '655px', '200px');
?>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	</table>
</td>
</tr>
<tr><td align="center" id="inputTD"<? if($tipoLink!=1) echo " style=\"display:none\""; ?>>
<?
$ohtmleditor->Createhtmleditor('htmleditor', '100%', "100%");
?>
</td></tr>
<tr><td height="1%">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td align="center"><input type="button" class="buttonsty" value="Cancelar" onclick="self.close()" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Preview" onclick="submitPage('preview_homepage.php','_blank')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Submeter dados" onclick="submitPage('/admin/updatehp.php','')" onfocus="noFocus(this)">
</td></tr>
</table>
</td></tr>
</table>
</td></tr></table>
<input type="hidden" name="id" value="<? echo $id; ?>">
<input type="hidden" name="data" value="<? echo $data; ?>">
</form>
</body>
</html>
<?php
mysql_close();
?>
