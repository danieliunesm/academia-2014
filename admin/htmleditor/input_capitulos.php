<?
include "../../include/security.php";
include "../../include/defines.php";
include "../../include/dbconnection.php";
include "../../include/genericfunctions.php";
include "htmleditor.php";

$id 	= $_GET["id"];
$action	= $_GET["action"];

function EscreveDados($oID,$oAction)
{
	global $ohtmleditor, $titulo, $id, $pastaid, $ordenacao;

	if($oAction == 2)
	{
		$sql = "SELECT NM_CAPITULO, Conteudo, CD_CAPITULO, CD_PASTA, NR_ORDEM_CAPITULO FROM col_capitulo WHERE CD_CAPITULO = " . $oID;
		$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

		if($oRs = mysql_fetch_row($RS_query))
		{
			$titulo 			= $oRs[0];
			$ohtmleditor->Value = $oRs[1];
			$id				    = $oRs[2];
			$pastaid			= $oRs[3];
            $ordenacao          = $oRs[4];
		}
		mysql_free_result($RS_query);
	}
}

$ohtmleditor = New htmleditor;
$ohtmleditor->ToolbarSet = 'thisHtmlEditorToolbarSet';

EscreveDados($id,$action);
if(empty($data)) $data = date("d/m/Y");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<style type="text/css">
.title{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}
.text{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.textblk{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.data{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:8pt;font-weight:bold;color:#ffffff}
.legenda{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#cc0000}
.buttonsty{width:150px;height:19px;background-color:#dddddd;border:2px solid #000099;font-family:"verdana","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000;cursor:hand;line-height:11px}
</style>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="javascript" src="js/dhtmled.js"></script>
<script language="JavaScript">
loaded=false;
function checkValues(frm)
{
	var msg="";

    if(frm.pastaid.selectedIndex==0)msg+="Necess�rio haver uma pasta para o cap�tulo!   \n";
    if(msg!="")
    {
        alert(msg);
        return false;
    }


	if(frm.titulo.value=="")msg+="Necess�rio haver um t�tulo para o cap�tulo!   \n";
	if(msg!="")
	{
		alert(msg);
		return false;
	}

    return true;
}

function submitPage(url,tgt)
{
	if(!checkValues(f))return false;
	f.action=url;
	f.target=tgt;
	f.submit();
}

function init()
{
	document.onkeypress=mascara;
	document.onmousedown=checkSrc;
	document.onmouseup=releaseSrc;
	document.onselectstart=unselectElement;
	f=document.inputForm;
	loaded=true;
	setTimeout('document.inputForm.titulo.focus()',300);
}

function openVideoDialog(){
	newWin = null;
	var w  = 460;
	var h  = 500;
	var l  = (screen.width-w)/2;
	var t  = (screen.height-h)/2;
	newWin = window.open('dialog/fck_media3.html','videowindow','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=0,resizable=0');
	if(newWin!=null)
		setTimeout('newWin.focus()',100);
}

function defineValorPagina(obj){

    var dialog = obj.getDialog(),
        textboxUrl = dialog.getContentElement( 'info', 'url' ), // 'general' is the id of the dialog panel.
        selectedSet = parseInt(obj.getValue()),
        selectProtocol = dialog.getContentElement( 'info', 'protocol' );

    textboxUrl.setValue("paginaindividualprivada.php?id=" + selectedSet);
    setSelectedIndex(selectProtocol, 4);

    //var dialog = CKEDITOR.dialog.getCurrent();
    //dialog.setValueof('info','txtUrl',"http://google.com");
    //textboxUrl['default'] = 'www.example.com';
}

function selecionaPaginas(obj){

    var dialog = obj.getDialog(),
        values = dialog.getContentElement( 'info', 'cboPagina' ), // 'general' is the id of the dialog panel.
        selectedSet = parseInt(obj.getValue());

    removeAllOptions( values );

    comboFrom = document.getElementById("cboPaginaall");

    for(var i = 0; i < comboFrom.options.length; i++){

        var option = comboFrom.options[i];

        if(option.pasta == selectedSet || option.pasta == 0){
            var oOption = addOption( values, option.text,
                option.value, dialog.getParentEditor().document );
            if ( i == 0 )
            {
                oOption.setAttribute( 'selected', 'selected' );
                oOption.selected = true;
            }
        }
    }

}

function removeAllOptions( combo )
{
    combo = getSelect( combo );
    while ( combo.getChild( 0 ) && combo.getChild( 0 ).remove() )
    { /*jsl:pass*/ }
}

function getSelect( obj )
{
    if ( obj && obj.domId && obj.getInputElement().$ )				// Dialog element.
        return  obj.getInputElement();
    else if ( obj && obj.$ )
        return obj;
    return false;
}

function addOption( combo, optionText, optionValue, documentObject, index )
{
    combo = getSelect( combo );
    var oOption;
    if ( documentObject )
        oOption = documentObject.createElement( "OPTION" );
    else
        oOption = document.createElement( "OPTION" );

    if ( combo && oOption && oOption.getName() == 'option' )
    {
        if ( CKEDITOR.env.ie ) {
            if ( !isNaN( parseInt( index, 10) ) )
                combo.$.options.add( oOption.$, index );
            else
                combo.$.options.add( oOption.$ );

            oOption.$.innerHTML = optionText.length > 0 ? optionText : '';
            oOption.$.value     = optionValue;
        }
        else
        {
            if ( index !== null && index < combo.getChildCount() )
                combo.getChild( index < 0 ? 0 : index ).insertBeforeMe( oOption );
            else
                combo.append( oOption );

            oOption.setText( optionText.length > 0 ? optionText : '' );
            oOption.setValue( optionValue );
        }
    }
    else
        return false;

    return oOption;
}

function setSelectedIndex( combo, index )
{
    combo = getSelect( combo );
    if ( index < 0 )
        return null;
    var count = combo.getChildren().count();
    combo.$.selectedIndex = ( index >= count ) ? ( count - 1 ) : index;
    return combo;
}

</script>
</head>
<body style="background-color:white;margin:0px;border:none" onload="init()">

<form name="inputForm" action="" method="post">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr height="1%">
<td>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
</table>

</td></tr>
<tr><td>
<table border="0" cellpadding="0" cellspacing="15" style="background-color:buttonface;border:none;width:100%;height:100%" align="center">
<tr>
<td height="1%">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr><td class="title" colspan="2" nowrap>CAP�TULOS</td></tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>
	

    <tr>
        <td class="textblk" align="right" width="1%" nowrap>N� Ordem:&nbsp;</td>
        <td width="99%"><input type="text" name="ordenacao" size="120" maxlength="120" style="width:350px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="5" value='<? echo $ordenacao; ?>'></td>
    </tr>
    <tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
        <td class="textblk" align="right" width="1%" nowrap>Pasta*:&nbsp;</td>
    <td width="99%"><select id="pastaid" name="pastaid" class="textbox8" style="width:350px" tabIndex="4">
    <option id="" value="0">Selecione a pasta onde vai ficar o cap�tulo</option>
    <?
    $sql2 = "SELECT ID, TITULO FROM tb_pastasindividuais WHERE STATUS = 1 ORDER BY TITULO";
    $query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());

while($oRs2 = mysql_fetch_row($query2)){
	if((int)($pastaid) == (int)($oRs2[0])) $selected = "selected";else $selected = "";
	echo "<option value=\"" . $oRs2[0] . "\" " . $selected . ">" . $oRs2[1] . "</option>";
}
mysql_free_result($query2);
?></select>	
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>

	<tr>
	<td class="textblk" align="right" width="1%" nowrap>T�tulo*:&nbsp;</td>
	<td width="99%"><input type="text" name="titulo" size="120" maxlength="120" style="width:350px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="6" value='<? echo $titulo; ?>'></td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>

	<tr><td></td><td><input type="button" value="v�deo" style="border:2px solid #009;background-color:#ddd;width:100px;height:21px" onclick="openVideoDialog()"></td></tr>

	</table>
</td>
</tr>
<tr><td align="center" valign="top">
<?
$ohtmleditor->Createhtmleditor('htmleditor', '100%', "100%");
?>
</td></tr>
<tr><td height="1%">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td align="center"><input type="button" class="buttonsty" value="Cancelar" onclick="self.close()" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Preview" onclick="submitPage('preview_paginaindividual.php','_blank')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Submeter dados" onclick="submitPage('/admin/updatecapitulo.php','')" onfocus="noFocus(this)">
</td></tr>
</table>
</td></tr>
</table>
</td></tr></table>
<input type="hidden" name="id" value="<? echo $id; ?>">
<input type="hidden" name="data" value="<? echo $data; ?>">
</form>

<?

$sql = "SELECT ID, Titulo FROM tb_pastasindividuais WHERE Status = 1 ORDER BY Titulo";
$resultado = mysql_query($sql);
$pastas = "[ 'Selecione', -1 ]";
while($linha=mysql_fetch_row($resultado)){

    if($pastas != ""){

        $pastas = "$pastas,";
    }

    $pastas = "$pastas [ '{$linha[1]}', {$linha[0]} ]";

}

$sql = "SELECT ID, Titulo, PastaID FROM tb_paginasindividuais WHERE Status = 1 ORDER BY Titulo";
$resultado = mysql_query($sql);
echo "<select id='cboPaginaall' style='display: none;'><option value='0'>Selecione<option/>";
    
while($linha=mysql_fetch_row($resultado)){
    echo "<option value='{$linha[0]}' pasta='{$linha[2]}'>{$linha[1]}</option>";
}
echo "</select>";
?>

<script type="text/javascript">
    //<![CDATA[

    // When opening a dialog, its "definition" is created for it, for
    // each editor instance. The "dialogDefinition" event is then
    // fired. We should use this event to make customizations to the
    // definition of existing dialogs.
    CKEDITOR.on( 'dialogDefinition', function( ev )
    {
        // Take the dialog name and its definition from the event
        // data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        // Check if the definition is from the dialog we're
        // interested on (the "Link" dialog).
        if ( dialogName == 'link' )
        {
            // Get a reference to the "Link Info" tab.
            var infoTab = dialogDefinition.getContents( 'info' );

            // Add a text field to the "info" tab.
            infoTab.add( {
                type : 'select',
                label : 'Pasta',
                id : 'cboPasta',
                items: [ <?php echo $pastas; ?> ],
                onChange : function( api ) {
                    // this = CKEDITOR.ui.dialog.select
                    selecionaPaginas(this);
                }
            });
            infoTab.add( {
                type : 'select',
                label : 'P�gina Individual',
                id : 'cboPagina',
                items: [ [ 'Selecione', -1 ] ],
                onChange : function( api ) {
                // this = CKEDITOR.ui.dialog.select
                defineValorPagina( this );
            }
            });
        }
    });

    //]]>


</script>


</body>
</html>
<?
mysql_close();
?>