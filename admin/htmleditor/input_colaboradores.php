<?
include "../../include/security.php";
include "../../include/defines.php";
include "../../include/dbconnection.php";
include "../../include/genericfunctions.php";
include "htmleditor.php";

$id 	= $_GET["id"];
$action	= $_GET["action"];
$idUSU  = 0;

function EscreveDados($oID,$oAction){
	global $ohtmleditor, $foto, $nome, $snome, $ddd1, $ddd2, $tel, $cel, $email, $posX, $posY, $data, $id, $idUSU;

	$foto 	= '';
	$nome 	= '';
	$snome 	= '';
	$email	= '';
	$posX	= 0;
	$posY	= 0;
	
	if($oAction == 2){
//		$sql = "SELECT FOTO, NOME, SNOME, TEL_COM, CEL, EMAIL_COM, X, Y, CV, ID FROM tb_usuariostmp WHERE ID = " . $oID;
		
		$sql = "SELECT u.FOTOFILE, u.NM_USUARIO, u.NM_SOBRENOME, u.EMAIL, c.X, c.Y, u.CV, c.ID, u.CD_USUARIO FROM col_usuario u, tb_colaboradores c WHERE c.IDusuario = u.CD_USUARIO AND c.ID = $oID";
		
		$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

		if($oRs = mysql_fetch_row($RS_query)){
			$foto				= $oRs[0];
			$nome 				= $oRs[1];
			$snome 				= $oRs[2];
			$email 				= $oRs[3];
			$posX				= $oRs[4];
			$posY				= $oRs[5];
			$ohtmleditor->Value = $oRs[6];
			$id				    = $oRs[7];
			$idUSU			    = $oRs[8];
		}
		mysql_free_result($RS_query);
	}
}

$ohtmleditor = New htmleditor;
$ohtmleditor->ToolbarSet = 'thisHtmlEditorToolbarSet';

EscreveDados($id,$action);
if(empty($data)) $data = date("d/m/Y");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<style type="text/css">
.title{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}
.text{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.textblk{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.data{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:8pt;font-weight:bold;color:#ffffff}
.legenda{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#cc0000}
.buttonsty{width:150px;height:19px;background-color:#dddddd;border:2px solid #000099;font-family:"verdana","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000;cursor:hand;line-height:11px}
.textbox3{width:290px;height:19px;border:1px solid #003300;background-color:#ffffff;color:#333333}
.ImagePreviewArea{position:absolute;left:340px;top:-54px;border:1px solid #000;padding:0px;overflow:auto;width:116px;height:100px;background-color:#fff}
</style>
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="JavaScript" src="/include/js/formfunctions.js"></script>
<script language="javascript" src="js/dhtmled.js"></script>
<script language="JavaScript">
loaded=false;
posArray = [
<?
	if($id!='')
		$sql = "SELECT X, Y FROM tb_colaboradores WHERE STATUS = 1 AND ID <> ".$id;
	else
		$sql = "SELECT X, Y FROM tb_colaboradores WHERE STATUS = 1";
	$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
	$iCount = 0;
	while($oRs = mysql_fetch_row($RS_query)){
		if($iCount > 0) echo ",";
		echo "[" . $oRs[0] . "," . $oRs[1] . "]";
		$iCount++;
	}
	mysql_free_result($RS_query);
?>
];

function setEditor(st){
	if(st){
		document.getElementById('colTable').style.visibility = 'visible';
		document.getElementById('editorCel').style.display   = 'inline';
		document.getElementById('uploadLayer').style.visibility = 'visible';
	}
	else{
		document.getElementById('colTable').style.visibility = 'hidden';
		document.getElementById('editorCel').style.display   = 'none';
		document.getElementById('uploadLayer').style.visibility = 'hidden';
	}
	document.getElementById('Xlabel').style.visibility  = 'visible';
	document.getElementById('XYTable').style.visibility = 'visible';
}

var BaseUrl = document.location.protocol + "//" + document.location.host + "/";
var bCanBrowse = true;
var sBrowseURL = BaseUrl + "admin/htmleditor/filemanager/browse/browse5.php";
var iBrowseWindowWidth  = 620;
var iBrowseWindowHeight = 520;
var bCanUpload = true;
var sUploadURL = BaseUrl + "admin/htmleditor/filemanager/upload/upload.php";
var iUploadWindowWidth  = 300;
var iUploadWindowHeight = 150;
var sUploadAllowedExtensions = ".gif .jpg .jpeg .png";

var	imgOriginal = new Image();

function updateImage(imgTarget){
	var fotoObj    = document.getElementById('foto_');
	var txtWidthObj  = document.getElementById('txtWidth');
	var txtHeightObj = document.getElementById('txtHeight'); 
	if (fotoObj.value == ""){
		imgTarget.style.display = "none" ;
		txtWidthObj.value = "";
		txtHeightObj.value = "";	
	}
	else{
		imgTarget.style.removeAttribute("display") ;
		imgTarget.src = fotoObj.value ;
		imgOriginal.src = fotoObj.value ;
		txtWidthObj.value = imgOriginal.width;
		txtHeightObj.value = imgOriginal.height;
	}
}

function updatePreview(){
	updateImage(imgPreview) ;
}

function uploadFile(){
	var sFile = frmUpload.UploadFile.value;
	
	var imageTMP 	= new Image();
	imageTMP.onload = function(){
		w = imageTMP.width;
		h = imageTMP.height;
	

		if(document.frmUpload.subdir[0].checked){
			var wImg = 114;var hImg = 98;var dirImg = 'fotos';
		}
		else{
			var wImg = 54;var hImg = 48;;var dirImg = 'miniaturas';
		}
	
		if(sFile == ""){
			alert("Nenhum arquivo selecionado.   ") ;
			return ;
		}
	
		var sExt = sFile.match( /\.[^\.]*$/ ) ;
		sExt = sExt ? sExt[0].toLowerCase() : "." ;

		if(sUploadAllowedExtensions.indexOf( sExt ) < 0 ){
			alert("Apenas os seguintes tipos de arquivo s�o permitidos para upload:\n\n" + self.opener.config.ImageUploadAllowedExtensions + "\n\nOpera��o cancelada.   ") ; 
			return ;
		}
	
		if(w!=wImg || h!=hImg){
			alert('As dimens�es da imagem s�o diferentes dos valores para arquivos de '+dirImg+'!     \nAs '+dirImg+' de Colaboradores devem ter '+wImg+' pixels de largura por '+hImg+' pixels de altura.     \nRedimensione a imagem e tente novamente.     ');
			return;
		}
	
		var oWindow = openNewWindow("", "UploadWindow", iUploadWindowWidth, iUploadWindowHeight) ;

		frmUpload.action = sUploadURL;
		frmUpload.submit() ;
		oWindow.setImage = null ;
	}
	imageTMP.src 	= sFile;
}

function browserCV(){
	CVWin=null;
	var w = 760;
	var h = 480;
	var l = (screen.width - w) / 2;
	var t = (screen.height - h) / 2;
	CVWin=window.open("/admin/htmleditor/filemanager/browse/browse2cv.php",'viewserverwindow','left='+l+',top='+t+',width='+w+',height='+h);
	if(CVWin!=null)setTimeout('CVWin.focus()',100);
}

function browserServer(){
	var oWindow = openNewWindow((sBrowseURL + getSubDir()), "BrowseWindow", iBrowseWindowWidth, iBrowseWindowHeight) ;
	oWindow.setImage = setImage ;
}

function getSubDir(){
	for(var i=0;i<document.frmUpload.subdir.length;i++){
		if(document.frmUpload.subdir[i].checked)return ('?subdir='+document.frmUpload.subdir[i].value);
	}
}

oWindow = null;
function openNewWindow(sURL, sName, iWidth, iHeight, bResizable, bScrollbars){
	var iTop  = (screen.height - iHeight) / 2;
	var iLeft = (screen.width  - iWidth) / 2;
	
	var sOptions = "toolbar=no" ;
	sOptions += ",width=" + iWidth ; 
	sOptions += ",height=" + iHeight ;
	sOptions += ",resizable="  + (bResizable  ? "yes" : "no") ;
	sOptions += ",scrollbars=" + (bScrollbars ? "yes" : "no") ;
	sOptions += ",left=" + iLeft ;
	sOptions += ",top=" + iTop ;
	
	oWindow = window.open(sURL, sName, sOptions)
	oWindow.focus();
	
	return oWindow ;
}

function setImage(sImageURL){
	document.getElementById('foto_').value = sImageURL ;
	updatePreview() ;
}

function setDefaults(){
	if(document.getElementById('imgPreview').style.display!="none"){
		imgOriginal.src = document.getElementById('foto_').value ;
		document.getElementById('txtWidth').value = document.getElementById('imgPreview').width;
		document.getElementById('txtHeight').value = document.getElementById('imgPreview').height;
	}
}

function checkPosition(xCol,yCol){
	var msg = '';
	if(!(((xCol % 2 == 0) && (yCol % 2 == 0))||((xCol % 2 == 1) && (yCol % 2 == 1))))
		msg += 'Os pares de coordenadas devem ser ambos pares ou �mpares!     \n';
	if(xCol>7)msg += 'O valor de X n�o pode ser superior a 7!     \n';
	if(xCol<0 || yCol<0)msg += 'Os valores de X e Y n�o podem ser negativos!     \n';
	posflag = false;
	for(var i=0;i<posArray.length;i++){
		if((xCol==posArray[i][0])&&(yCol==posArray[i][1])){
			msg += 'Esta posi��o na colm�ia j� est� em uso! Utilize outra posi��o ou altere a c�lula que est� nesta posi��o.     \n';
			break;
		}
	}
	return msg;
}

function checkValues(){
	var msg = '';
//	if(Trim(f.nome.value)=="")	msg += "� necess�rio digitar o Nome do colaborador!   \n";
//	if(Trim(f.snome.value)=="")	msg += "� necess�rio digitar o Sobrenome do colaborador!   \n";
<?php
if($nome == '')
{
	echo 'if(f.usuario.selectedIndex == 0)	msg += "� necess�rio selecionar um Usu�rio!   \n";';
}
?>
	if(Trim(f.foto_.value)=="")	msg += "� necess�rio incluir uma foto do Colaborador!   \n";
	msg += checkPosition(f.posX.value,f.posY.value);
	if(msg!=""){
		alert(msg);
		return false;
	}
	else return true;
}

function submitPage(url,tgt){
	var msg = '';
	if(document.getElementById('colselect2').checked&&url=='preview_colaborador.php'){
		alert("Para visualizar a posi��o da c�lula vazia, volte para a p�gina de administra��o de Colaboradores, ajuste a nova posi��o da c�lula e clique em 'Preview'.     ");
		return false;
	}
	if(document.getElementById('colselect1').checked){
		if(!checkValues())return false;
		var lastslash   = f.foto_.value.lastIndexOf('/');
		f.foto.value	= f.foto_.value.substring((lastslash+1),f.foto_.value.length);
	}
	else{
		msg = checkPosition(f.posX.value,f.posY.value);
		if(msg!=''){
			alert(msg);
			return false;
		}
		f.nome.value='';
		f.snome.value='';
		f.email.value='';
		f.foto_.value='';
		f.foto.value='';
	}
	f.action=url;
	f.target=tgt;
	f.submit();
}

function setUsuario(userdata){
	var dataarray = userdata.lang.split('|');
	f.email.value = dataarray[0];
	f.nome.value  = dataarray[1];
	f.snome.value = dataarray[2];
	f.idUSU.value = userdata.value;
}

function init(){
	document.onkeypress=mascara;
	document.onmousedown=checkSrc;
	document.onmouseup=releaseSrc;
	document.onselectstart=unselectElement;
	f=document.inputForm;
	loaded=true;
	document.getElementById('Xlabel').style.visibility  = 'visible';
	document.getElementById('XYTable').style.visibility = 'visible';
}

</script>
</head>
<body style="background-color:white;margin:0px;border:none" onload="init();setDefaults()">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr height="1%">
<td>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
</table>

</td></tr>
<tr><td>
<table border="0" cellpadding="0" cellspacing="15" style="background-color:buttonface;border:none;width:100%;height:100%" align="center">
<tr>
<td height="1%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr><td class="title" colspan="4">COLABORADORES</td></tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td style="padding-left:68px"><input name="colselect" id="colselect1" type="radio" onclick="setEditor(1)" <? echo (($nome != '' || $action == 1)?'checked':''); ?>></td>
	<td class="textblk" align="right" nowrap style="padding-right:20px">&nbsp;Colaborador</td>
	<td><input name="colselect" id="colselect2" type="radio" onclick="setEditor(0)" <? echo (($nome == '' && $action != 1)?'checked':''); ?> <? if($action == 2 && $nome != '')echo 'disabled'; ?>></td>
	<td class="textblk" align="right" nowrap>&nbsp;C�lula vazia</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
	</table>
	<form name="inputForm" action="" method="post" style="margin:0px;padding:0px">
	<table id="colTable" border="0" cellpadding="0" cellspacing="0" width="1%" style="visibility:<? echo (($nome != '' || $action == 1)?'visible':'hidden'); ?>">
	<tr>
<?php
if($nome != '')
{
?>
	<td class="textblk" align="right" nowrap>Nome:&nbsp;</td>
	<td><input type="text" name="nome" maxlength="120" style="width:90px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="1" value='<? echo $nome; ?>' readonly></td>
	<td><img src="/images/layout/blank.gif" width="12" height="1"></td>
	<td class="textblk" align="right" nowrap>Sobrenome:&nbsp;</td>
	<td><input type="text" name="snome" maxlength="120" style="width:160px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="2" value='<? echo $snome; ?>' readonly></td>
<?php
}
else
{
?>
	<td class="textblk" align="right" nowrap>Usu�rio*:&nbsp;</td>
	<td colspan="4">
	<select name="usuario" style="width:322px" onchange="setUsuario(this[this.selectedIndex])">
	<option lang="" value="0">Selecione o usu�rio do cadastro de usu�rios</option>
<?php
$sql = "SELECT CD_USUARIO, NM_USUARIO, NM_SOBRENOME, email FROM col_usuario WHERE tipo >= 3 OR tipo < 0 ORDER BY NM_USUARIO";
$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
while($oRs = mysql_fetch_row($RS_query))
{
	echo "<option lang=\"$oRs[3]|$oRs[1]|$oRs[2]\" value=\"$oRs[0]\">$oRs[1] $oRs[2]</option>";
}
mysql_free_result($RS_query);
?>
	</select>
	<input type="hidden" name="nome">
	<input type="hidden" name="snome">
	</td>
<?php
}
?>
	<td><img src="/images/layout/blank.gif" width="32" height="1"></td>
	<td class="textblk" align="right" nowrap>Imagem*:&nbsp;</td>
	<td><input type="text" name="foto_" id="foto_" maxlength="120" style="width:290px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="2" value='<? if($foto!='')echo '/images/colaboradores/'.$foto; ?>' onblur="updatePreview()"></td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
	<tr>
	<td class="textblk" align="right" nowrap>E-mail:&nbsp;</td>
	<td colspan="4"><input type="text" name="email" maxlength="255" style="width:321px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="6" value='<? echo $email; ?>' readonly></td>
	<td></td>
	<td></td>
	<td align="right"><input type="button" class="buttonsty" value="Procurar no Servidor" onclick="browserServer()" onfocus="noFocus(this)"></td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>	
	<tr>
	<td class="textblk" align="right" nowrap></td>
	<td colspan="4"><input type="button" class="buttonsty" style="width:321px" value="Abrir arquivo recebido de curr�culo" onclick="browserCV()" onfocus="noFocus(this)"></td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
	<tr>
	<td id="Xlabel" class="textblk" align="right" nowrap>Pos X (0 a 7) *:&nbsp;</td>
	<td colspan="4">
		<table id="XYTable" border="0" cellpadding="0" cellspacing="0" width="1%">
		<tr>
		<td><input type="text" name="posX" size="10" maxlength="1" style="width:20px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="3" value='<? echo $posX; ?>'></td>
		<td><img src="/images/layout/blank.gif" width="9" height="1"></td>
		<td class="textblk" align="right" nowrap>Pos Y *:&nbsp;</td>
		<td><input type="text" name="posY" size="10" maxlength="2" style="width:20px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="4" value='<? echo $posY; ?>' onblur="checkPosition(document.inputForm.posX.value,this.value)"></td>
		</tr>
		</table>
	</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	</table>
</td>
</tr>
<tr><td align="center" valign="top" id="editorCel" style="display:<? echo (($nome != '' || $action == 1)?'inline':'none'); ?>">
<?
$ohtmleditor->Createhtmleditor('htmleditor', '100%', "100%");
?>
</td></tr>
<tr><td height="1%">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td align="center"><input type="button" class="buttonsty" value="Cancelar" onclick="self.close()" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Preview" onclick="submitPage('preview_colaborador.php','_blank')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Submeter dados" onclick="submitPage('/admin/updatecolaborador.php','')" onfocus="noFocus(this)">
</td></tr>
</table>
</td></tr>
</table>
</td></tr></table>
<input type="hidden" name="id" value="<? echo $id; ?>">
<input type="hidden" name="idUSU" value="<? echo $idUSU; ?>">
<input type="hidden" name="data" value="<? echo $data; ?>">
<input type="hidden" name="foto" value="">
</form>

<div id="uploadLayer" style="position:absolute;width:390px;height:100px;left:451px;top:155px;visibility:<? echo (($nome != '' || $action == 1)?'visible':'hidden'); ?>">
<form name="frmUpload" id="frmUpload" enctype="multipart/form-data" method="post" target="UploadWindow">
<table border="0" cellpadding="0" cellspacing="0" width="1%">
<tr>
<td class="textblk" align="right" nowrap>Upload:&nbsp;</td>
<td colspan="2"><input class="textbox3" type="file" name="UploadFile" id="UploadFile"></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
<td></td>
<td>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><input type="radio" name="subdir" value="colaboradores" onfocus="noFocus(this)" checked></td>
	<td class="textblk">Fotos&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td><input type="radio" name="subdir" value="colaboradores/miniaturas" onfocus="noFocus(this)"></td>
	<td class="textblk">Miniaturas</td>
	</tr>
	</table>
</td>
<td align="right"><input type="button" class="buttonsty" value="Enviar para o Servidor" onclick="uploadFile()" onfocus="noFocus(this)"></td>
</tr>
</table>
<input type="hidden" name="UploadBaseDir" value="/images/">
</form>
<div class="ImagePreviewArea"><table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%"><tr><td align="center"><img src="<? if($foto!='')echo '/images/colaboradores/'.$foto; ?>" id="imgPreview" <? if($foto == "") echo "style=\"display:none\""; ?>></td></tr></table></div>
<div style="position:absolute;left:470px;top:-54px">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="text" id="txtWidth" style="width:30px;height:20px;border:1px solid #000" readonly="true"></td>
<td class="textblk">&nbsp;Largura</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
<td><input type="text" id="txtHeight" style="width:30px;height:20px;border:1px solid #000" readonly="true"></td>
<td class="textblk">&nbsp;Altura</td>
</tr>
</table>
</div>
</div>
</body>
</html>
<?
mysql_close();
?>