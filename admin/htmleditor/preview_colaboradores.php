<?
include "../../include/defines.php";
include "../../include/dbconnection.php";
include "../../include/genericfunctions.php";
include "../../include/previewcodefunctions.php";

$titulo   = "Colaboradores";

$istCol	  = 1;
$iNmc	  = 1;
$iImc	  = 1;
$iPxc	  = 1;
$iPyc	  = 1;
$i		  = 1;

reset($_POST); 
while (list($key, $val) = each($_POST)){
	if(substr($key, 0, 3) == "col"){
		$stCol[$istCol] = $val;
		$istCol++;
	}

	if(substr($key, 0, 3) == "nmc"){
		$Nmc[$iNmc] = $val;
		$iNmc++;
	}

	if(substr($key, 0, 3) == "imc"){
		$Imc[$iImc] = $val;
		$iImc++;
	}

	if(substr($key, 0, 3) == "pxc"){
		$Pxc[$iPxc] = $val;
		$iPxc++;
	}

	if(substr($key, 0, 3) == "pyc"){
		$Pyc[$iPyc] = $val;
		$iPyc++;
	}

	$i++;
}

$Ncol = $_POST["Ncol"];

function doColmeia($status,$Name,$Image,$PosX,$PosY,$Ncolaboradores){
	global $maxtop;
	$maxtop = 0;
	for($i = 1;$i <= $Ncolaboradores;$i++){
		$zindex = 1000 - $PosX[$i];
		$left	= $PosX[$i]*53;
		$top	= $PosY[$i]*30;
		
		$maxtop = max($top,$maxtop);
		
		if($status[$i] == 1){
			if($Name[$i] != ''){
				echo "<div class=\"modulo\" style=\"left:".$left."px;top:".$top."px;z-index:".$zindex."\">\n";
				echo "<div class=\"maparea\"><a href=\"#\"><img src=\"/images/layout/blank.gif\" width=\"47\" height=\"47\" alt=\"   ".$Name[$i]."   \" title=\"   ".$Name[$i]."   \"></a></div>\n";
				echo "<div class=\"hexagon\"><img src=\"/images/layout/modulo_colmeia.gif\"></div>\n";
				echo "<div class=\"foto\"><img src=\"".$Image[$i]."\"></div>\n";
				echo "</div>\n\n";
			}
			else{
				echo "<div class=\"modulo\" style=\"left:".$left."px;top:".$top."px;z-index:".$zindex."\">\n";
				echo "<div class=\"hexagon\"><img src=\"/images/layout/modulo_colmeia.gif\"></div>\n";
				echo "</div>\n\n";
			}
		}
	}
}

writetopcode();
?>
</head>
<body>
<div id="global">
<div id="globalpadding">
<?php writeheadercode();?>
<?php writemenucode();?>
	<div id="conteudo">
		<h1><?php echo $titulo; writenavcode(1);?></h1>

		<?php echo "<div id=\"colmeia\">";
		doColmeia($stCol,$Nmc,$Imc,$Pxc,$Pyc,$Ncol);
		echo "</div>";
		?>
	</div>
<?php writebottomcode(); ?>
</div>
</div>
<script type="text/javascript">
document.getElementById('colmeia').style.height = '<? echo ($maxtop + 59);?>px';
</script>
</body>
</html>
<?
mysql_close();
?>
