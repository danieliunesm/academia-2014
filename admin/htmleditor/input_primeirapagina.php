<?
include "../../include/security.php";
include "../../include/defines.php";
include "../../include/dbconnection.php";
include "../../include/genericfunctions.php";
include "htmleditor.php";

$id 		= $_GET["id"];
$action 	= $_GET["action"];
$sessid		= $_GET["sessid"];
$tipo   	= 0;
$tipoLink 	= 1;
$dd     	= date("d");
$mm     	= date("m");
$yyyy   	= date("Y");
		
function EscreveDados($oID,$oAction)
{
	global $ohtmleditor, $tipo, $titulo, $chamada, $data, $fonte, $foto_dir, $foto_w, $foto_h, $foto_legenda, $autor, $foto_autoria, $dd, $mm, $yyyy, $tipoLink, $extLink;

	if($oAction == 2)
	{
		$sql = "SELECT TIPO, TITULO, CHAMADA, DATA, FONTE, FOTO_DIR, FOTO_W, FOTO_H, FOTO_LEGENDA, AUTOR, FOTO_AUTORIA, CONTEUDO, TIPOLINK, EXTLINK FROM TB_PRIMEIRAPAGINA WHERE ID=" . $oID;
		$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

		if($oRs = mysql_fetch_row($RS_query))
		{
			$ohtmleditor->Value	= $oRs[11];
			$tipo 			   	= $oRs[0];
			$titulo 		 	= $oRs[1];
			$chamada 		 	= $oRs[2];
			$data 			 	= $oRs[3];
			$fonte 			 	= $oRs[4];
			$foto_dir 		 	= $oRs[5];
			$foto_w 			= $oRs[6];
			$foto_h 			= $oRs[7];
			$foto_legenda 	 	= $oRs[8];
			$autor 			 	= $oRs[9];
			$foto_autoria	 	= $oRs[10];
			$tipoLink		 	= $oRs[12];
			$extLink			= $oRs[13];

			$dd			     	= strftime("%d", strtotime($data));
			$mm				 	= strftime("%m", strtotime($data));
			$yyyy			 	= strftime("%Y", strtotime($data));
		}
		mysql_free_result($RS_query);
		mysql_close();
	}
}

$ohtmleditor = New htmleditor;
$ohtmleditor->ToolbarSet = 'thisHtmlEditorToolbarSet';

EscreveDados($id,$action);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<style type="text/css">
.title{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}
.text{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.textblk{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.data{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:8pt;font-weight:bold;color:#ffffff}
.legenda{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#cc0000}
.buttonsty{width:150px;height:19px;background-color:#dddddd;border:2px solid #000099;font-family:"verdana","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000;cursor:hand;line-height:11px}
</style>
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="javascript" src="js/dhtmled.js"></script>
<script language="JavaScript">
loaded=false;
theimage = new Image();
tipo = <? echo $tipo; ?>;
function openImage()
{
	theimage.src = f.pathImage.value;
	theimage.width = f.wImage.value;
	theimage.height = f.hImage.value;
	
	var html = ShowDialog("dialog/fck_image2.html", window, 620, 460);
	if(html)
	{
		f.pathImage.value = html.substring((html.indexOf('src=')+5),(html.indexOf('width')-2));
		f.tagImage.value = html;
		wpos = html.indexOf('width=');
		hpos = html.indexOf('height=');
		f.wImage.value = parseInt(html.substring(wpos+7,hpos-2));
		f.hImage.value = parseInt(html.substring(hpos+8,html.length-3));
		
		if(tipo)document.getElementById('TDchamadaImage').innerHTML = html;
	}
}

function ShowDialog(pagePath, args, width, height, left, top)
{
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
}

function checkValues(frm)
{
	var msg="";
	if((frm.tipoLink[0].checked&&frm.titulo.value=="")||(tipo==0&&!frm.tipoLink[0].checked&&frm.titulo.value==""))msg+="� necess�rio haver um t�tulo para a mat�ria!   \n";
	msg+=ValidaData(frm.DD.value,frm.MM.value,frm.YYYY.value,'<? echo date("d/m/Y"); ?>');
	//if(tipo==0&&frm.chamada.value=="")msg+="� necess�rio haver uma chamada textual para a mat�ria!   \n";
	if(tipo==1&&frm.pathImage.value=="")msg+="Necess�rio haver um banner como chamada para a mat�ria!   \n";
	if(!frm.tipoLink[0].checked&&frm.extLink.value=="")msg+="Necess�rio haver um link para a mat�ria!   \n";
	if(msg!="")
	{
		alert(msg);
		return false;
	}
	else return true;
}

function setTipo(t)
{
	tipo = t;
	if(tipo) // 1 - banner
	{
		document.getElementById('TRtituloText').style.display='none';
		document.getElementById('TRchamadaText').style.display='none';
		document.getElementById('TRchamadaImage').style.display='inline';
		document.getElementById('TDimagem').innerHTML='<span class="textbold">Imagem*:&nbsp;</span>';
		if(document.inputForm.pathImage.value!="")document.getElementById('TDchamadaImage').innerHTML = '<img src="'+f.pathImage.value+'">';
		f.legendaImage.style.backgroundColor = "buttonface";
		f.legendaImage.disabled = true;
		f.foto_autoria.style.backgroundColor = "buttonface";
		f.foto_autoria.disabled = true;
		if(!f.tipoLink[0].checked)
		{
			f.titulo.disabled=true;
			f.titulo.style.backgroundColor='buttonface';
			f.fonte.disabled=true;
			f.fonte.style.backgroundColor='buttonface';
			f.autor.disabled=true;
			f.autor.style.backgroundColor='buttonface';
		}
		f.pathImage.focus();
	}
	else // 0 - textual
	{
		document.getElementById('TRtituloText').style.display='inline';
		document.getElementById('TRchamadaImage').style.display='none';
		document.getElementById('TRchamadaText').style.display='inline';
		document.getElementById('TDimagem').innerHTML='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Imagem:&nbsp;';
		f.legendaImage.style.backgroundColor = "#ffffff";
		f.legendaImage.disabled = false;
		f.foto_autoria.style.backgroundColor = "#ffffff";
		f.foto_autoria.disabled = false;
		f.titulo.disabled=false;
		f.titulo.style.backgroundColor='#ffffff';
		f.fonte.disabled=false;
		f.fonte.style.backgroundColor='#ffffff';
		f.autor.disabled=false;
		f.autor.style.backgroundColor='#ffffff';
		f.chamada.focus();
	}
}

function setTipoLink(tL)
{
	if(tL==1) // editor
	{
		document.getElementById('inputTable').style.height='100%';
		document.getElementById('ppTD').style.display='inline';
		f.extLink.disabled=true;
		f.extLink.style.backgroundColor='buttonface';
		setViewFileButton(0);
		f.titulo.disabled=false;
		f.titulo.style.backgroundColor='#ffffff';
		f.fonte.disabled=false;
		f.fonte.style.backgroundColor='#ffffff';
		f.autor.disabled=false;
		f.autor.style.backgroundColor='#ffffff';	
	}
	else // externo
	{
		document.getElementById('inputTable').style.height='';
		document.getElementById('ppTD').style.display='none';
		f.extLink.disabled=false;
		f.extLink.style.backgroundColor='#ffffff';
		if(tL==2)setViewFileButton(1); // 2 paginas site
		else setViewFileButton(0);
		if(f.tipochamada[1].checked)
		{
			f.titulo.disabled=true;
			f.titulo.style.backgroundColor='buttonface';
			f.fonte.disabled=true;
			f.fonte.style.backgroundColor='buttonface';
			f.autor.disabled=true;
			f.autor.style.backgroundColor='buttonface';		
		}
		f.extLink.focus();
	}
}

function setViewFileButton(st)
{
	if(st)
	{
		document.getElementById('viewfile').style.filter='alpha(opacity=100)';
		document.getElementById('viewfile').style.cursor='hand';
		document.getElementById('viewfile').onmouseover=function(){this.style.backgroundColor='#99ccff';this.style.borderColor='#666666'};
		document.getElementById('viewfile').onmouseout=function(){this.style.backgroundColor='buttonface';this.style.borderColor='#dddddd'};
		document.getElementById('viewfile').onclick=function(){selectURL()};
		
		f.regstatus[0].disabled=false;
		f.regstatus[1].disabled=false;
		f.regstatus[2].disabled=false;
	}
	else
	{
		document.getElementById('viewfile').style.filter='gray() alpha(opacity=30)';
		document.getElementById('viewfile').style.cursor='default';
		document.getElementById('viewfile').style.backgroundColor='buttonface';
		document.getElementById('viewfile').style.borderColor='#dddddd';
		document.getElementById('viewfile').onmouseover=function(){return false};
		document.getElementById('viewfile').onmouseout=function(){return false};
		document.getElementById('viewfile').onclick=function(){return false};
		
		f.regstatus[0].disabled=true;
		f.regstatus[1].disabled=true;
		f.regstatus[2].disabled=true;
	}
}

function selectURL()
{
	var sURL = selectLink();
	if(sURL)f.extLink.value = sURL ;
}

function selectLink()
{
	var st=f.regstatus[0].checked?1:(f.regstatus[1].checked?2:3);
	return showModalDialog("dialog/treeview.php?st="+st, window, "dialogWidth:720px;dialogHeight:420px;help:no;scroll:auto;status:no");
}

function updateImage()
{
	if(f.pathImage.value=="")
	{
		document.getElementById('TDchamadaImage').innerHTML = "";
		f.legendaImage.value = "";
		f.foto_autoria.value = "";
	}
}

function submitPage(url,tgt)
{
	if(!checkValues(f))return false;
	f.action=url;
	f.target=tgt;
	
	if(url.indexOf('preview')!=-1)
	{
		if(f.tipoLink[1].checked) // link externo
		{
			if(f.extLink.value.indexOf('http://')==-1)f.extLink.value="http://"+f.extLink.value;
			window.open(f.extLink.value,'','');
		}
		if(f.tipoLink[2].checked) window.open(f.extLink.value,'',''); // pg site
		if(f.tipoLink[0].checked) f.submit(); // editor
	}
	else f.submit();
}

function init()
{
	document.onkeypress=mascara;
	document.onmousedown=checkSrc;
	document.onmouseup=releaseSrc;
	document.onselectstart=unselectElement;
	f=document.inputForm;
	loaded=true;

	//setTimeout('f.titulo.focus()',300);
}
</script>
</head>
<body style="background-color:buttonface;margin:0px;border:none" scroll="no" onload="init()">
<form name="inputForm" action="" method="post">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr height="1%">
<td>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td bgcolor="#000066" width="1%"><img src="/images/layout/pirai_admin.gif" width="373" height="21" hspace="18"></td>
<td class="data" width="99%" bgcolor="#000066"><? echo getServerDate(); ?></td>
</tr>
</table>

</td></tr>
<tr><td valign="top">
<table id="inputTable" border="0" cellpadding="0" cellspacing="15" style="background-color:buttonface;border:none;width:100%<? if($tipoLink==1) echo ";height:100%"; ?>" align="center">
<tr>
<td height="1%">
	<table border="0" cellpadding="0" cellspacing="0" width="1%">
	<tr><td class="title" colspan="2">HOME PAGE: PRIMEIRA P�GINA</td></tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr valign="top">
	<td width="90%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td class="textblk" align="right">Data*:&nbsp;</td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><input size="2" name="DD" value="<? echo $dd; ?>" maxlength="2" style="width:20px;font-family:arial,helvetica,sans-serif;font-size:11px" onkeyup="if(event.keyCode!=9)if(this.value.length==2)document.inputForm.MM.focus()" onfocus="srcflag=true;this.select()" onblur="srcflag=false" tabIndex="1"></td>
			<td class="textblk">&nbsp;/&nbsp;</td><td><input size="2" name="MM" value="<? echo $mm; ?>" maxlength="2" style="width:20px;font-family:arial,helvetica,sans-serif;font-size:11px" onkeyup="if(event.keyCode!=9)if(this.value.length==2)document.inputForm.YYYY.focus()" onfocus="srcflag=true;this.select()" onblur="srcflag=false" tabIndex="2"></td>
			<td class="textblk">&nbsp;/&nbsp;</td><td><input size="4" name="YYYY" value="<? echo $yyyy; ?>" maxlength="4" style="width:30px;font-family:arial,helvetica,sans-serif;font-size:11px" onkeyup="if(event.keyCode!=9)if(this.value.length==4)document.inputForm.chamada.focus()" onfocus="srcflag=true;this.select()" onblur="srcflag=false" tabIndex="3"></td>
			<td><img src="/images/layout/blank.gif" width="60" height="1"></td>
			<td class="textblk" align="right">Tipo chamada:&nbsp;</td>
			<td><input type="radio" name="tipochamada" value="0" <? if($tipo==0) echo "checked"; ?> onclick="setTipo(0)" onfocus="noFocus(this)"></td>
			<td class="textblk">&nbsp;Textual</td>
			<td><img src="/images/layout/blank.gif" width="10" height="1"></td>
			<td><input type="radio" name="tipochamada" value="1" <? if($action==2 && $tipo==1) echo "checked"; ?> onclick="setTipo(1)" onfocus="noFocus(this)"></td>
			<td class="textblk">&nbsp;Banner</td>
			</tr>
			</table>
		</td>
		</tr>
		<tr><td><img src="/images/layout/blank.gif" width="56" height="5"></td></tr>
		<tr>
		<td class="textblk" align="right" width="1%" nowrap>Link:&nbsp;</td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
			<td width="1%"><input type="radio" name="tipoLink" value="1" onfocus="noFocus(this)" <? if($tipoLink==1) echo "checked"; ?> onclick="setTipoLink(1);" style="margin-left:-3px"></td>
			<td class="textblk" width="1%">Editor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td width="1%"><input type="radio" name="tipoLink" value="0" onfocus="noFocus(this)" <? if($action==2 && $tipoLink==0) echo "checked"; ?> onclick="setTipoLink(0)" style="margin-left:-3px"></td>
			<td class="textblk" width="1%">URL&nbsp;externa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td width="1%"><input type="radio" name="tipoLink" value="2" onfocus="noFocus(this)" <? if($action==2 && $tipoLink==2) echo "checked"; ?> onclick="setTipoLink(2)" style="margin-left:-3px"></td>
			<td class="textblk" width="1%">Link&nbsp;para&nbsp;p�ginas&nbsp;do&nbsp;site</td>
			<td width="94%" align="right"><img src="/images/layout/blank.gif" width="4" height="23" align="absmiddle"><img id="viewfile" src="/images/layout/viewfile2.gif" width="21" height="21" style="border:1px solid #E6E5D7" align="absmiddle" alt="  Pesquisar no site  " <? if($tipoLink!=2) echo "style=\"cursor:default;filter:gray() alpha(opacity=30)\""; else echo "onmouseover=\"this.style.backgroundColor='#99ccff';this.style.borderColor='#666666'\" onmouseout=\"this.style.backgroundColor='buttonface';this.style.borderColor='#dddddd'\" onclick=\"selectURL()\" style=\"cursor:hand\""; ?>></td>
			</tr>
			</table>
		</td>
		</tr>
		<tr>
		<td></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
			<td><input type="text" name="extLink" maxlength="255" style="width:180px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="4" onfocus="srcflag=true;this.select()" onblur="srcflag=false" value="<? if($action==2 && $tipoLink!=1) echo $extLink; ?>" <? if($tipoLink==1) echo "style=\"background-color:buttonface\" disabled"; ?>></td>
			<td>&nbsp;<img src="/images/layout/setacorner.gif" width="9" height="23" hspace="5"></td>
			<td><input type="radio" name="regstatus" value="1" onfocus="noFocus(this)" checked <?if($tipoLink!=2) echo "disabled"; ?>></td>
			<td class="textblk">pub.&nbsp;</td>
			<td><input type="radio" name="regstatus" value="2" onfocus="noFocus(this)" <?if($tipoLink!=2) echo "disabled"; ?>></td>
			<td class="textblk">leia&nbsp;mais&nbsp;</td>
			<td><input type="radio" name="regstatus" value="3" onfocus="noFocus(this)" <?if($tipoLink!=2) echo "disabled"; ?>></td>
			<td class="textblk">excl.</td>
			</tr>
			</table>
		</td>
		</tr>
		<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
		<tr id="TRtituloText" style="display:<? if($tipo==0) echo "inline"; else echo "none"; ?>">
		<td class="textblk" align="right" width="1%" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T�tulo*:&nbsp;</td>
		<td width="99%"><input type="text" name="titulo" maxlength="120" style="width:366px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="5" value='<? echo $titulo; ?>'></td>
		</tr>
 		<tr><td></td><td><img src="/images/layout/blank.gif" width="366" height="5"></td></tr>
		<tr id="TRchamadaText" style="display:<? if($tipo==0) echo "inline"; else echo "none"; ?>">
		<td class="textblk" align="right">Chamada:&nbsp;</td>
		<td><textarea name="chamada" cols="69" rows="3" wrap:"physycal" style="width:366px;height:63px;font-family:arial,helvetica,sans-serif;font-size:11px;overflow-y:auto" tabIndex="6"><? if($tipo==0) echo $chamada; ?></textarea></td>
		</tr>
		<tr id="TRchamadaImage" style="display:<? if($tipo==1) echo "inline"; else echo "none"; ?>">
		<td class="textblk" align="right">Chamada:&nbsp;</td>
		<td id="TDchamadaImage" valign="top" height="65"><? if($tipo==1) echo $chamada; ?></td>
		</tr>
		</table>
	</td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="9%">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
		<td class="textblk" align="right" id="TDimagem" nowrap><? if($tipo==0) echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Imagem:&nbsp;"; else echo "<span class=\"textbold\">Imagem*:&nbsp;</span>"; ?></td>
		<td><input type="text" name="pathImage" size="36" style="font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="7" value="<? echo $foto_dir; ?>" onfocus="updateImage();srcflag=true;this.select()" onblur="updateImage();srcflag=false" onkeypress="alert('Para garantir a exist�ncia da imagem no servidor,\nutilize o bot�o ao lado do campo para escolher a imagem     \nou fazer o upload de uma nova!   ');return false" onfocus="srcflag=true;this.select()" onblur="srcflag=false"><img src="/images/layout/blank.gif" width="5" height="23" align="absmiddle"><a href="javascript:void(openImage())" onmouseover="document.getElementById('ppImg').style.backgroundColor='#99ccff';document.getElementById('ppImg').style.border='1px solid #666666'" onmouseout="document.getElementById('ppImg').style.backgroundColor='transparent';document.getElementById('ppImg').style.border='1px solid #E6E5D7'" onfocus="noFocus(this)"><img id="ppImg" src="images/toolbar/button.image.gif" style="border:1px solid #E6E5D7" align="absmiddle" alt="  Inserir imagem  " width="21" height="21"></a></td>
		</tr>
 		<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
		<tr>
		<td class="textblk" align="right">Legenda:&nbsp;</td>
		<td><input type="text" name="legendaImage" size="36" style="font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="8" onfocus="srcflag=true;this.select()" onblur="srcflag=false" value="<? echo $foto_legenda; ?>" <? if($action==2 && $tipo==1) echo "style=\"background-color:buttonface\" disabled"; ?>></td>
		</tr>
 		<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
		<tr>
		<td class="textblk" align="right">Cr�dito&nbsp;foto:&nbsp;</td>
		<td><input type="text" name="foto_autoria" size="36" style="font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="9" value="<? echo $foto_autoria; ?>" onfocus="srcflag=true;this.select()" onblur="srcflag=false" <? if($action==2 && $tipo==1) echo "style=\"background-color:buttonface\" disabled"; ?>></td>
		</tr>
		<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>		
		<tr>
		<td class="textblk" align="right" width="1%">Fonte:&nbsp;</td>
		<td><input type="text" name="fonte" size="36" style="font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="10" value="<? echo $fonte; ?>" onfocus="srcflag=true;this.select()" onblur="srcflag=false" <? if($action==2 && $tipo==1) echo "style=\"background-color:buttonface\" disabled"; ?>></td>
		</tr>
		<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
		<tr>
		<td class="textblk" align="right">Autor:&nbsp;</td>
		<td><input type="text" name="autor" size="36" style="font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="11" value="<? echo $autor; ?>" onfocus="srcflag=true;this.select()" onblur="srcflag=false;document.inputForm.titulo.focus()" <? if($action==2 && $tipo==1) echo "style=\"background-color:buttonface\" disabled"; ?>></td>
		</tr>
		</table>
	</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	</table>
</td>
</tr>
<tr><td align="center" valign="top" id="ppTD"<? if($action==2 && $tipoLink!=1) echo " style=\"display:none\""; ?>>
<?
$ohtmleditor->Createhtmleditor('htmleditor', '100%', "100%");
?>
</td></tr>
<tr><td height="1%">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td align="center"><input type="button" class="buttonsty" value="Cancelar" onclick="self.close()" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Preview" onclick="submitPage('preview_primeirapagina.php','_blank')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Submeter dados" onclick="submitPage('/admin/updateHPSessions.php','')" onfocus="noFocus(this)">
</td></tr>
</table>
</td></tr>
</table>
</td></tr></table>
<input type="hidden" name="tagImage" value='<? if($foto_dir!="") echo "<img src=\"" . $foto_dir . "\" width=\"" . $foto_w . "\" height=\"" . $foto_h . "\" border=\"0\"/>"; ?>'>
<input type="hidden" name="wImage" value="<? echo $foto_w; ?>">
<input type="hidden" name="hImage" value="<? echo $foto_h; ?>">
<input type="hidden" name="id" value="<? echo $id; ?>">
<input type="hidden" name="sessid" value="<? echo $sessid; ?>">
</form>
</body>
</html>
