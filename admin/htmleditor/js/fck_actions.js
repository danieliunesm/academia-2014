function SetFocus()
{
	if (BrowserInfo.IsIE55OrMore)
		objContent.DOM.focus() ;
	else
		objContent.focus() ;
}

function decCommand(cmdId, cmdExecOpt, url)
{
	var status = objContent.QueryStatus(cmdId) ;
	
	if ( status != DECMDF_DISABLED && status != DECMDF_NOTSUPPORTED )
	{
		if (cmdExecOpt == null) cmdExecOpt = OLECMDEXECOPT_DODEFAULT ;
		objContent.ExecCommand(cmdId, cmdExecOpt, url) ;
	}
	SetFocus() ;
}

function docCommand(command)
{
	objContent.DOM.execCommand(command) ;
	SetFocus();
}

function doStyle(command)
{
	var oSelection = objContent.DOM.selection ;
	var oTextRange = oSelection.createRange() ;
	
	if (oSelection.type == "Text")
	{
		decCommand(DECMD_REMOVEFORMAT);
		if (!FCKFormatBlockNames) loadFormatBlockNames() ;
		doFormatBlock( FCKFormatBlockNames[0] );
 
		var oFont = document.createElement("FONT") ;
		oFont.innerHTML = oTextRange.htmlText ;
		
		var oParent = oTextRange.parentElement() ;
		var oFirstChild = oFont.firstChild ;
		
		if (oFirstChild.nodeType == 1 && oFirstChild.outerHTML == oFont.innerHTML && 
				(oFirstChild.tagName == "SPAN"
				|| oFirstChild.tagName == "FONT"
				|| oFirstChild.tagName == "P"
				|| oFirstChild.tagName == "DIV"))
		{
			oParent.className = command.value ;
		}
		else
		{
			oFont.className = command.value ;
			oTextRange.pasteHTML( oFont.outerHTML ) ;
		}
	}
	else if (oSelection.type == "Control" && oTextRange.length == 1)
	{
		var oControl = oTextRange.item(0) ;
		oControl.className = command.value ;
	}
	
	command.selectedIndex = 0 ;
	
	SetFocus();
}

function doFormatBlock(combo)
{
	if (combo.value == null || combo.value == "")
	{
		if (!FCKFormatBlockNames) loadFormatBlockNames() ;
		objContent.ExecCommand(DECMD_SETBLOCKFMT, OLECMDEXECOPT_DODEFAULT, FCKFormatBlockNames[0]);
	}
	else
		objContent.ExecCommand(DECMD_SETBLOCKFMT, OLECMDEXECOPT_DODEFAULT, combo.value);
	
	SetFocus();
}

function doFontName(combo)
{
	if (combo.value == null || combo.value == "")
	{
		// Remove font name attribute.
	}
	else
		objContent.ExecCommand(DECMD_SETFONTNAME, OLECMDEXECOPT_DODEFAULT, combo.value);
	
	SetFocus();
}

function doFontSize(combo)
{
	if (combo.value == null || combo.value == "")
	{
		objContent.ExecCommand(DECMD_SETFONTSIZE, OLECMDEXECOPT_DODEFAULT, 3);
	}
	else
		objContent.ExecCommand(DECMD_SETFONTSIZE, OLECMDEXECOPT_DODEFAULT, parseInt(combo.value));
	
	SetFocus();
}

function dialogImage()
{
	var html = FCKShowDialog("dialog/fck_image.html", window, 620, 480);
	if (html) insertHtml(html) ;
	SetFocus();
}

function dialogTable(searchParentTable)
{
	if (searchParentTable)
	{
		var oRange  = objContent.DOM.selection.createRange() ;
		var oParent = oRange.parentElement() ;
		
		while (oParent && oParent.nodeName != "TABLE")
		{
			oParent = oParent.parentNode ;
		}
		
		if (oParent && oParent.nodeName == "TABLE")
		{
			var oControlRange = objContent.DOM.body.createControlRange();
			oControlRange.add( oParent ) ;
			oControlRange.select() ;
		}
		else
			return ;
	}

	FCKShowDialog("dialog/fck_table.html", window, 520, 255);
	SetFocus();
}

function dialogTableCell()
{
	FCKShowDialog("dialog/fck_tablecell.html", window, 630, 230);
	SetFocus();
}

function dialogLink()
{
	if (checkDecCommand(DECMD_HYPERLINK) != OLE_TRISTATE_GRAY)
	{
		FCKShowDialog("dialog/fck_link.html", window, 460, 260, 20, 20);
		SetFocus();
	}
}

function insertHtml(html)
{
	if (objContent.DOM.selection.type.toLowerCase() != "none")
		objContent.DOM.selection.clear() ;
	objContent.DOM.selection.createRange().pasteHTML(html) ;
	SetFocus();
}

function foreColor()
{
	var color = FCKShowDialog("dialog/fck_selcolor.html", window, 410, 310);
	if (color) objContent.ExecCommand(DECMD_SETFORECOLOR,OLECMDEXECOPT_DODEFAULT, color) ;
	SetFocus();
}

function backColor()
{
	var color = FCKShowDialog("dialog/fck_selcolor.html", window, 370, 240);
	if (color) objContent.ExecCommand(DECMD_SETBACKCOLOR,OLECMDEXECOPT_DODEFAULT, color) ;
	SetFocus();
}

function insertSpecialChar()
{
	var html = FCKShowDialog("dialog/fck_specialchar.html", window, 400, 250);
	if (html) insertHtml(html) ;
	SetFocus();
}

function insertSmiley()
{
	var html = FCKShowDialog("dialog/fck_smiley.html", window, config.SmileyWindowWidth, config.SmileyWindowHeight) ;
	if (html) insertHtml(html) ;
	SetFocus();
}

function FCKShowDialog(pagePath, args, width, height, left, top)
{
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
}

function about()
{
	FCKShowDialog("dialog/fck_about.html", window, 460, 290);
}

function pastePlainText()
{
	var sText = HTMLEncode( clipboardData.getData("Text") ) ;
	sText = sText.replace(/\n/g,'<BR>') ;
	insertHtml(sText) ;
}

function pasteFromWord()
{
	if (BrowserInfo.IsIE55OrMore)
		cleanAndPaste( GetClipboardHTML() ) ;
	else if ( confirm( lang["NotCompatiblePaste"] ) )
		decCommand(DECMD_PASTE) ;
}

function cleanAndPaste( html )
{
	html = html.replace(/<\/?SPAN[^>]*>/gi, "" );
	html = html.replace(/<(\w[^>]*) class=([^ |>]*)([^>]*)/gi, "<$1$3") ;
	html = html.replace(/<(\w[^>]*) style="([^"]*)"([^>]*)/gi, "<$1$3") ;
	html = html.replace(/<(\w[^>]*) lang=([^ |>]*)([^>]*)/gi, "<$1$3") ;
	html = html.replace(/<\\?\?xml[^>]*>/gi, "") ;
	html = html.replace(/<\/?\w+:[^>]*>/gi, "") ;
	html = html.replace(/&nbsp;/, " " );
	var re = new RegExp("(<P)([^>]*>.*?)(<\/P>)","gi") ;
	html = html.replace( re, "<div$2</div>" ) ;
	
	insertHtml( html ) ;
}

function GetClipboardHTML()
{
	var oDiv = document.getElementById("divTemp")
	oDiv.innerHTML = "" ;
	
	var oTextRange = document.body.createTextRange() ;
	oTextRange.moveToElementText(oDiv) ;
	oTextRange.execCommand("Paste") ;
	
	var sData = oDiv.innerHTML ;
	oDiv.innerHTML = "" ;
	
	return sData ;
}

function HTMLEncode(text)
{
	text = text.replace(/&/g, "&amp;") ;
	text = text.replace(/"/g, "&quot;") ;
	text = text.replace(/</g, "&lt;") ;
	text = text.replace(/>/g, "&gt;") ;
	text = text.replace(/'/g, "&#146;") ;

	return text ;
}

function showTableBorders()
{
	objContent.ShowBorders = !objContent.ShowBorders ;
	SetFocus();
}

function showDetails()
{
	objContent.ShowDetails = !objContent.ShowDetails ;
	SetFocus();
}

var FCKFormatBlockNames ;

function loadFormatBlockNames()
{
	var oNamesParm = new ActiveXObject("DEGetBlockFmtNamesParam.DEGetBlockFmtNamesParam") ;
	objContent.ExecCommand(DECMD_GETBLOCKFMTNAMES, OLECMDEXECOPT_DODEFAULT, oNamesParm);
	var vbNamesArray = new VBArray(oNamesParm.Names) ;

	FCKFormatBlockNames = vbNamesArray.toArray() ;
}

function doZoom( sizeCombo ) 
{
	if (sizeCombo.value != null || sizeCombo.value != "")
		objContent.DOM.body.runtimeStyle.zoom = sizeCombo.value + "%" ;
}

function dialogFind()
{
	if(objContent.DOM.body.createTextRange().htmlText !="")
	{
		var html = FCKShowDialog("dialog/fck_find.html", window, 620, 220, 0, 10);
		objContent.focus() ;
	}
	else alert('O Editor est� vazio! N�o h� palavras para procurar ou substituir.   '); 
}

function insertList( type )
{
	var oDoc = objContent.DOM ;
	if ( !config.UseBROnCarriageReturn || oDoc.queryCommandState( 'InsertOrderedList' ) || oDoc.queryCommandState( 'InsertUnorderedList' ) )
	{
		if ( type == 'ul' )
			decCommand( DECMD_UNORDERLIST ) ;
		else
			decCommand( DECMD_ORDERLIST ) ;
	}
	else
	{
		insertHtml('<' + type + '><li id="____tempLI">.</li></' + type + '>') ;
		
		var oLI = oDoc.getElementById( '____tempLI' ) ;
		oLI.removeAttribute("id") ;
		
		var oRange = oDoc.selection.createRange() ;
		oRange.moveToElementText( oLI ) ;
		oRange.findText( '.' ) ;
		oRange.select() ;
		oDoc.selection.clear() ;
	}
}

function SpellCheck() 
{
	try {
		var tmpis = new ActiveXObject( "ieSpell.ieSpellExtension" ) ;
		tmpis.CheckAllLinkedDocuments( objContent );
	}
	catch(exception) {
		if( exception.number == -2146827859 )
		{
			if ( confirm( "Spell checker n�o est� instalado. Voc� deseja fazer o seu download agora?" ) )
				window.open( config.SpellCheckerDownloadUrl , "SpellCheckerDownload" );
		}
		else
			alert("Error durante a carga do ieSpell: Exce��o: " + exception.number);
	}
}

function InsertAnchor()
{
	var html = FCKShowDialog("dialog/fck_anchor.html", window, 455, 130) ;
	if (html) insertHtml(html) ;
	SetFocus();
}

function InsertLinkVideo()
{
	if (checkDecCommand(DECMD_HYPERLINK) != OLE_TRISTATE_GRAY)
	{
		FCKShowDialog("dialog/fck_media2.html", window, 620, 220, 20, 20);
		SetFocus();
	}
}

function InsertVideo()
{
	var html = FCKShowDialog("dialog/fck_media3.html", window, 460, 290, 20, 20) ;
	if (html) insertHtml(html) ;
	SetFocus();
}

function selectDialogLink()
{
	if (checkDecCommand(DECMD_HYPERLINK) != OLE_TRISTATE_GRAY)
	{
		var oParent ;
		var oRange ;
		
		var oDOM = this.objContent.DOM ;
	
		if (oDOM.selection.type == "Control")
		{
			oRange = oDOM.selection.createRange() ;
			for ( i = 0 ; i < oRange.length ; i++ )
			{
				if (oRange(i).parentNode)
				{
					oParent = oRange(i).parentNode ;
					break ;
				}
			}
		}
		else
		{
			oRange  = oDOM.selection.createRange() ;
			oParent = oRange.parentElement() ;
		}

		while (oParent && oParent.nodeName != "A")
		{
			oParent = oParent.parentNode ;
		}

		if (oParent && oParent.nodeName == "A")
		{
			oDOM.selection.empty() ;
			oRange = oDOM.selection.createRange() ;
			oRange.moveToElementText( oParent ) ;
			oRange.select() ;
		
//			if(oRange.htmlText.indexOf('.asx') != -1)
//				InsertLinkVideo();
//			else 
				dialogLink();
		}
		else
			return null ;
	}
}