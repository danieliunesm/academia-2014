
var config = new Object() ;

config.BasePath = document.location.pathname.substring(0,document.location.pathname.lastIndexOf('/')+1) ;
config.EditorAreaCSS = config.BasePath + 'css/fck_editorarea.css' ;
config.BaseUrl = document.location.protocol + '//' + document.location.host + '/' ;
config.EnableXHTML = false ;
config.StartupShowBorders = false ;
config.StartupShowDetails = false ;
config.ForcePasteAsPlainText = false ;
config.AutoDetectPasteFromWord = true ;
config.UseBROnCarriageReturn	= true ;
config.TabSpaces = 0;
config.AutoDetectLanguage = true ;
config.DefaultLanguage    = "pt" ;
config.SpellCheckerDownloadUrl = "" ;
config.ToolbarImagesPath = config.BasePath + "images/toolbar/" ;
config.ToolbarSets = new Object() ;
config.ToolbarSets["Source"] = [
	['EditSource']
] ;

//config.ToolbarSets["thisHtmlEditorToolbarSet"] = [
//	['EditSource','-','Cut','Copy','Paste','PasteText','PasteWord','-','Find','-','Undo','Redo','-','SelectAll','RemoveFormat','-','Link','RemoveLink','-','Image','Table','SpecialChar','-','Anchor','-','LinkVideo','RemoveVLink','Video'] ,
//	['FontStyle','-','Font','-','FontSize','-','TextColor','Bold','Italic','-','Subscript','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','-','InsertOrderedList','InsertUnorderedList','-','Outdent','Indent']
//] ;

config.ToolbarSets["thisHtmlEditorToolbarSet"] = [
	['EditSource','-','Cut','Copy','Paste','PasteText','PasteWord','-','Find','-','Undo','Redo','-','SelectAll','RemoveFormat','-','Link','RemoveLink','-','Image','Table','SpecialChar','-','Anchor','-','Video'] ,
	['FontStyle','-','Font','-','FontSize','-','TextColor','Bold','Italic','-','Subscript','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','-','InsertOrderedList','InsertUnorderedList','-','Outdent','Indent']
] ;

config.ToolbarSets["HPEditorToolbarSet"] = [
	['EditSource','-','Cut','Copy','Paste','PasteText','PasteWord','-','Find','-','Undo','Redo','-','RemoveFormat','-','Link','RemoveLink','-','Image','Table','SpecialChar'] ,
	['FontStyle','-','Font','-','FontSize','-','TextColor','Bold','Italic','-','Subscript','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','-','InsertOrderedList','InsertUnorderedList','-','Outdent','Indent']
] ;

config.ToolbarSets["chamadaeditorToolbarSet"] = [
	['EditSource'],
	['FontStyle','Bold','Italic','-','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','-','InsertOrderedList','InsertUnorderedList','-','Link','RemoveLink']
] ;

config.ToolbarSets["IMG"] = [
	['Image']
] ;

config.StyleNames  = ';Sub-t�tulo;Texto normal;Texto bold;Texto preto;Rodap�;Legenda;Assinatura';
config.StyleValues = ';title;text;textbold;textblk;rodape;legendablk;assinatura';
config.ToolbarFontNames = 'verdana;arial;tahoma;courier new;sans-serif' ;
config.LinkShowTargets = true ;
config.LinkTargets = '_blank;_parent;_self;_top' ;
config.LinkDefaultTarget = '' ;
config.ImageBrowser = true ;
config.ImageBrowserURL = config.BasePath + "filemanager/browse/browse.php" ;
config.ImageBrowserWindowWidth  = 760 ;
config.ImageBrowserWindowHeight = 480 ;
config.ImageUpload = true ;
config.ImageUploadURL = config.BasePath + "filemanager/upload/upload.php" ;
config.ImageUploadWindowWidth	= 300 ;
config.ImageUploadWindowHeight	= 150 ;
config.ImageUploadAllowedExtensions = ".gif .jpg .jpeg .png" ;
config.LinkBrowser = true ;
config.LinkBrowserURL = config.BasePath + "filemanager/browse/docs/browsefile.html" ;
config.LinkBrowserWindowWidth	= 400 ;
config.LinkBrowserWindowHeight	= 250 ;
config.LinkUpload = true ;
config.LinkUploadURL = config.BasePath + "filemanager/upload/aspx/upload.phpx" ;
config.LinkUploadWindowWidth	= 300 ;
config.LinkUploadWindowHeight	= 150 ;
config.LinkUploadAllowedExtensions	= "*" ;
config.LinkUploadDeniedExtensions	= ".exe .php .php .phpx .js .cfm .dll" ;

config.SmileyPath	= '' ;
config.SmileyImages	= [] ;
config.SmileyColumns = 0 ;
config.SmileyWindowWidth	= 0 ;
config.SmileyWindowHeight	= 0 ;