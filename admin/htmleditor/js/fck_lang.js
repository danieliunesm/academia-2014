
var lang = new Object() ;

var AvailableLangs = new Object() ;

AvailableLangs["bs"]	= true ;
AvailableLangs["cs"]	= true ;
AvailableLangs["da"]	= true ;
AvailableLangs["de"]	= true ;
AvailableLangs["en"]	= true ;
AvailableLangs["es"]	= true ;
AvailableLangs["fi"]	= true ;
AvailableLangs["fr"]	= true ;
AvailableLangs["gr"]	= true ;
AvailableLangs["hu"]	= true ;
AvailableLangs["is"]	= true ;
AvailableLangs["it"]	= true ;
AvailableLangs["jp"]	= true ;
AvailableLangs["ko"]	= true ;
AvailableLangs["nl"]	= true ;
AvailableLangs["no"]	= true ;
AvailableLangs["pl"]	= true ;
AvailableLangs["pt-br"]	= true ;
AvailableLangs["ro"]	= true ;
AvailableLangs["ru"]	= true ;
AvailableLangs["sk"]	= true ;
AvailableLangs["sv"]	= true ;
AvailableLangs["tr"]	= true ;
AvailableLangs["zh-cn"]	= true ;

AvailableLangs.GetActiveLanguage = function()
{
	if ( config.AutoDetectLanguage )
	{
		var sUserLang = navigator.userLanguage.toLowerCase() ;
		
		if ( this[sUserLang] ) 
			return sUserLang ;
		else if ( sUserLang.length > 2 )
		{
			sUserLang = sUserLang.substr(0,2) ;
			if ( this[sUserLang] ) 
				return sUserLang ;
		}
	}
	
	return config.DefaultLanguage ;
}

document.write('<script src="lang/' + AvailableLangs.GetActiveLanguage() + '.js" type="text/javascript"><\/script>') ;

AvailableLangs.TranslatePage = function( targetDocument )
{
	var aInputs = targetDocument.getElementsByTagName("INPUT") ;
	for ( i = 0 ; i < aInputs.length ; i++ )
	{
		if ( aInputs[i].fckLang )
			aInputs[i].value = lang[ aInputs[i].fckLang ] ;
	}

	var aSpans = targetDocument.getElementsByTagName("SPAN") ;
	for ( i = 0 ; i < aSpans.length ; i++ )
	{
		if ( aSpans[i].fckLang )
			aSpans[i].innerText = lang[ aSpans[i].fckLang ] ;
	}
	
	var aOptions = targetDocument.getElementsByTagName("OPTION") ;
	for ( i = 0 ; i < aOptions.length ; i++ )
	{
		if ( aOptions[i].fckLang )
			aOptions[i].innerText = lang[ aOptions[i].fckLang ] ;
	}
}