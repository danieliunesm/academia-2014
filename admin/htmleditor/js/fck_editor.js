
var bInitialized = false ;
var bDataLoaded  = false ;

function initEditor()
{
	if (! bInitialized) 
	{
		bInitialized = true ;	
		
		loadToolbarSet() ;
		loadToolbarSourceSet() ;

		objContent.BaseURL = config.BaseUrl ;
	}

	if (! bDataLoaded && ! objContent.Busy)
	{
		bDataLoaded = true ;

		objContent.DOM.body.onpaste		= onPaste ;
		objContent.DOM.body.ondrop		= onDrop ;
		objContent.DOM.body.onkeydown	= onKeyDown ;
		objContent.ShowBorders			= config.StartupShowBorders ;
		objContent.ShowDetails			= config.StartupShowDetails ;
		objContent.DOM.createStyleSheet(config.EditorAreaCSS) ;
		setLinkedField() ;
	}
}

function loadToolbarSet()
{
	var sToolBarSet = URLParams["Toolbar"] == null ? "Default" : URLParams["Toolbar"] ;
	var oToolbarHolder = document.getElementById("divToolbar") ;
	var oToolbar = new TBToolbar() ;
	oToolbar.LoadButtonsSet( sToolBarSet ) ;
	oToolbarHolder.innerHTML = oToolbar.GetHTML() ;
}

function loadToolbarSourceSet()
{
	var oToolbarHolder = document.getElementById("divToolbarSource") ;

	var oToolbar = new TBToolbar() ;
	oToolbar.LoadButtonsSet( "Source" ) ;
	oToolbarHolder.innerHTML = oToolbar.GetHTML() ;
}

function switchEditMode()
{
	var bSource = (trSource.style.display == "none") ;
	
	if (bSource) 
		txtSource.value = objContent.DOM.body.innerHTML ;
	else
	{
		objContent.DOM.body.innerHTML = "<div id=__tmpFCKRemove__>&nbsp;</div>" + txtSource.value ;
		objContent.DOM.getElementById('__tmpFCKRemove__').removeNode(true) ;
	}
		
	trEditor.style.display = bSource ? "none" : "inline" ;
	trSource.style.display = bSource ? "inline" : "none" ;
	
	events.fireEvent('onViewMode', bSource) ;
}

function selValue(el, str, text)
{
	for (var i = 0; i < el.length; i++) 
	{
		if (((text || !el[i].value) && el[i].text == str) || ((!text || el[i].value) && el[i].value == str)) 
		{
			el.selectedIndex = i;
			return;
		}
	}
	el.selectedIndex = 0;
}

var oLinkedField = null ;
function setLinkedField()
{
	if (! URLParams['FieldName']) return ;
	
	oLinkedField = parent.document.getElementsByName(URLParams['FieldName'])[0] ;
	
	if (! oLinkedField) return ;

	objContent.DOM.body.innerHTML = "<div id=__tmpFCKRemove__>&nbsp;</div>" + oLinkedField.value ;
	objContent.DOM.getElementById('__tmpFCKRemove__').removeNode(true) ;
	
	var oForm = oLinkedField.form ;
	
	if (!oForm) return ;

	oForm.attachEvent("onsubmit", setFieldValue) ;
	
	if (! oForm.updatehtmleditor) oForm.updatehtmleditor = new Array() ;
	oForm.updatehtmleditor[oForm.updatehtmleditor.length] = setFieldValue ;
	if (! oForm.originalSubmit)
	{
		oForm.originalSubmit = oForm.submit ;
		oForm.submit = function()
		{
			if (this.updatehtmleditor)
			{
				for (var i = 0 ; i < this.updatehtmleditor.length ; i++)
				{
					this.updatehtmleditor[i]() ;
				}
			}
			this.originalSubmit() ;
		}
	}
}

function setFieldValue()
{
	if (trSource.style.display != "none")
	{
		switchEditMode() ;
	}

	if (config.EnableXHTML)
	{
		window.status = lang["ProcessingXHTML"] ;
		oLinkedField.value = getXhtml( objContent.DOM.body ) ;
		window.status = 'Done' ;
	}
	else
		oLinkedField.value = objContent.DOM.body.innerHTML ;
}

function onPaste()
{
	if (config.ForcePasteAsPlainText)
	{
		pastePlainText() ;	
		return false ;
	}
	else if (config.AutoDetectPasteFromWord && BrowserInfo.IsIE55OrMore)
	{
		var sHTML = GetClipboardHTML() ;
		var re = /<\w[^>]* class="?MsoNormal"?/gi ;
		if ( re.test( sHTML ) )
		{
			if ( confirm( lang["PasteWordConfirm"] ) )
			{
				cleanAndPaste( sHTML ) ;
				return false ;
			}
		}
	}
	else
		return true ;
}

function onDrop()
{
	if (config.ForcePasteAsPlainText)
	{
		var sText = HTMLEncode( objContent.DOM.parentWindow.event.dataTransfer.getData("Text") ) ;
		sText = sText.replace(/\n/g,'<BR>') ;
		insertHtml(sText) ;
		return false ;
	}
	else if (config.AutoDetectPasteFromWord && BrowserInfo.IsIE55OrMore)
	{
		return true ;
	}
	else
		return true ;
}

function onKeyDown()
{
	var oWindow = objContent.DOM.parentWindow ;
	
	if ( oWindow.event.ctrlKey || oWindow.event.altKey || oWindow.event.shiftKey ) 
	{
		oWindow.event.returnValue = true ;
		return ;
	}

	if ( oWindow.event.keyCode == 9 && config.TabSpaces > 0 )
	{
		var sSpaces = "" ;
		for ( i = 0 ; i < config.TabSpaces ; i++ )
			sSpaces += "&nbsp;" ;
		insertHtml( sSpaces ) ;
	}
	else if (  oWindow.event.keyCode == 13 && config.UseBROnCarriageReturn )
	{
		if ( objContent.DOM.queryCommandState( 'InsertOrderedList' ) || objContent.DOM.queryCommandState( 'InsertUnorderedList' ) )
		{
			oWindow.event.returnValue = true ;
			return ;
		}

		insertHtml("<br>&nbsp;") ;
			
		var oRange = objContent.DOM.selection.createRange() ;
		oRange.moveStart('character',-1) ;	
		oRange.select() ;
		objContent.DOM.selection.clear() ;
			
		oWindow.event.returnValue = false ;
	}
}