
function StringBuilder(sString) {
	
	this.length = 0;

	this.append = function (sString) {
		this.length += (this._parts[this._current++] = String(sString)).length;
		this._string = null;
		return this;
	};

	this.toString = function () {
		if (this._string != null)
			return this._string;
		
		var s = this._parts.join("");
		this._parts = [s];
		this._current = 1;
		this.length = s.length;
		
		return this._string = s;
	};

	this._current	= 0;
	this._parts		= [];
	this._string	= null;
	if (sString != null)
		this.append(sString);
}

function getXhtml(oNode) {
	var sb = new StringBuilder;
	var cs = oNode.childNodes;
	var l = cs.length;
	for (var i = 0; i < l; i++)
		_appendNodeXHTML(cs[i], sb);

	return sb.toString();
}

function _fixAttribute(s) {
	return String(s).replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/\"/g, "&quot;");
}

function _fixText(s) {
	return String(s).replace(/\&/g, "&amp;").replace(/</g, "&lt;");
}

function _getAttributeValue(oAttrNode, oElementNode, sb) {
	if (!oAttrNode.specified)
		return;

	var name = oAttrNode.expando ? oAttrNode.nodeName : oAttrNode.nodeName.toLowerCase() ;
	var value = oAttrNode.nodeValue;

	if (name != "style" ) {
		if (!isNaN(value) || name == "src" || name == "href")
			value = oElementNode.getAttribute(name, 2);
		sb.append(" " + name + "=\"" + _fixAttribute(value) + "\"");
	}
	else
		sb.append(" style=\"" + _fixAttribute(oElementNode.style.cssText) + "\"");
}

function _appendNodeXHTML(node, sb) {

	switch (node.nodeType) {
		case 1:
		
			if (node.nodeName == "!") {	
				sb.append(node.text);
				break;
			}

			var name = node.nodeName;
			if (node.scopeName == "HTML")
				name = name.toLowerCase();

			sb.append("<" + name);
			
			var attrs = node.attributes;
			var l = attrs.length;
			for (var i = 0; i < l; i++)
			{
				_getAttributeValue(attrs[i], node, sb);
			}
				
			if (name == "input" && node.value)
				sb.append(" value=\"" + _fixAttribute(node.value) + "\"");
			
			if (node.canHaveChildren || node.hasChildNodes()) {
				sb.append(">");
				
				var cs = node.childNodes;
				l = cs.length;
				for (var i = 0; i < l; i++)
					_appendNodeXHTML(cs[i], sb);
				
				sb.append("</" + name + ">");
			}
			else if (name == "script")
				sb.append(">" + node.text + "</" + name + ">");
			else if (name == "title" || name == "style" || name == "comment")
				sb.append(">" + node.innerHTML + "</" + name + ">");
			else 
				sb.append(" />");
				
			break;

		case 3:
			sb.append( _fixText(node.nodeValue) );
			break;
				
		case 4:
			sb.append("<![CDA" + "TA[\n" + node.nodeValue + "\n]" + "]>");
			break;

		case 8:
			//sb.append("<!--" + node.nodeValue + "-->");
			sb.append(node.text);
			if (/(^<\?xml)|(^<\!DOCTYPE)/.test(node.text) )
				sb.append("\n");
			break;

		case 9:
			var cs = node.childNodes;
			l = cs.length;
			for (var i = 0; i < l; i++)
				_appendNodeXHTML(cs[i], sb);
			break;

		default:
			sb.append("<!--\nNot Supported:\n\n" + "nodeType: " + node.nodeType + "\nnodeName: " + node.nodeName + "\n-->");
	}
}