<?php
include "../../include/security.php";
include "../../include/defines.php";
include "../../include/dbconnection.php";
include "../../include/genericfunctions.php";

$id				= $_GET["id"];
$action			= $_GET["action"];

$programa		= '';
$curso			= '';
$programaDy		= '';
$cursoDy		= '';
$logo1			= '';
$logo1Dy		= '';
$logo1Dx		= '';
$logo1W			= '';
$logo1H			= '';
$logo2			= '';
$logo2Dy		= '';
$logo2Dx		= '';
$logo2W			= '';
$logo2H			= '';
$marcadagua		= '';
$marcadaguaDy	= '';
$marcadaguaDx	= '';
$marcadaguaW	= '';
$marcadaguaH	= '';
$nome			= '';
$linhaDy		= '';
$txt1			= '';
$txt1Dy			= '';
$txt2			= '';
$txt2Dy			= '';
$txt3			= '';
$txt3Dy			= '';
$assinatura1	= '';
$assinatura1Dy	= '';
$assinatura1Dx	= '';
$assinatura1W	= '';
$assinatura1H	= '';
$nome1			= '';
$cargo1			= '';
$conjunto1Dy	= '';
$conjunto1Dx	= '';
$assinatura2	= '';
$assinatura2Dy	= '';
$assinatura2Dx	= '';
$assinatura2W	= '';
$assinatura2H	= '';
$nome2			= '';
$cargo2			= '';
$conjunto2Dy	= '';
$conjunto2Dx	= '';
$data			= '';
$dataDy			= '';
$medalhaStatus	= '';
$medalhaDy		= '';
$medalhaDx		= '';
$status			= '';
$nu_certificado = '';

if($action == 2 || $action == 3)
{
	$sql = "SELECT * FROM col_certificado WHERE CD_CERTIFICADO = " . $id;
	$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
	
	while($row = mysql_fetch_assoc($query))
	{
		$id				= ($action == 2) ? $row['CD_CERTIFICADO'] : '';
		
		$programa		= $row['DS_PROGRAMA'];
		$curso			= $row['DS_CURSO'];
		$programaDy		= $row['programaDy'];
		$cursoDy		= $row['cursoDy'];
		$logo1			= $row['logo1'];
		$logo1Dy		= $row['logo1Dy'];
		$logo1Dx		= $row['logo1Dx'];
		$logo1W			= $row['logo1W'];
		$logo1H			= $row['logo1H'];
		$logo2			= $row['logo2'];
		$logo2Dy		= $row['logo2Dy'];
		$logo2Dx		= $row['logo2Dx'];
		$logo2W			= $row['logo2W'];
		$logo2H			= $row['logo2H'];
		$marcadagua		= $row['marcadagua'];
		$marcadaguaDy	= $row['marcadaguaDy'];
		$marcadaguaDx	= $row['marcadaguaDx'];
		$marcadaguaW	= $row['marcadaguaW'];
		$marcadaguaH	= $row['marcadaguaH'];
		$nome			= $row['nome'];
		$linhaDy		= $row['linhaDy'];
		$txt1			= $row['txt1'];
		$txt1Dy			= $row['txt1Dy'];
		$txt2			= $row['txt2'];
		$txt2Dy			= $row['txt2Dy'];
		$txt3			= $row['txt3'];
		$txt3Dy			= $row['txt3Dy'];
		$assinatura1	= $row['assinatura1'];
		$assinatura1Dy	= $row['assinatura1Dy'];
		$assinatura1Dx	= $row['assinatura1Dx'];
		$assinatura1W	= $row['assinatura1W'];
		$assinatura1H	= $row['assinatura1H'];
		$nome1			= $row['nome1'];
		$cargo1			= $row['cargo1'];
		$conjunto1Dy	= $row['conjunto1Dy'];
		$conjunto1Dx	= $row['conjunto1Dx'];
		$assinatura2	= $row['assinatura2'];
		$assinatura2Dy	= $row['assinatura2Dy'];
		$assinatura2Dx	= $row['assinatura2Dx'];
		$assinatura2W	= $row['assinatura2W'];
		$assinatura2H	= $row['assinatura2H'];
		$nome2			= $row['nome2'];
		$cargo2			= $row['cargo2'];
		$conjunto2Dy	= $row['conjunto2Dy'];
		$conjunto2Dx	= $row['conjunto2Dx'];
		$data			= $row['data'];
		$dataDy			= $row['dataDy'];
		$medalhaStatus	= $row['medalhaStatus'];
		$medalhaDy		= $row['medalhaDy'];
		$medalhaDx		= $row['medalhaDx'];
		$status			= $row['IN_ATIVO'];
		$nu_certificado = $row['nu_certificado'];
	}
	mysql_free_result($query);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<style type="text/css">
.title{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}
.text{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.textblk{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.data{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:8pt;font-weight:bold;color:#ffffff}
.legenda{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#cc0000}
.buttonsty{width:150px;height:19px;background-color:#dddddd;border:2px solid #000099;font-family:verdana,arial,helvetica,sans-serif;font-size:11px;font-weight:normal;color:#000000;cursor:hand;line-height:11px}
.textbox,.textbox2,.textbox4,.textbox4b,.textbox4c{width:600px;height:19px;border:1px solid #003300;background-color:#ffffff;color:#333333;margin:2px 0 2px 0;font-family:tahoma,arial,helvetica,sans-serif;font-size:11px}
.textbox2{width:445px}
.textbox4,.textbox4b,.textbox4c{width:40px;margin:0 5px 0 15px}
.textbox4b{margin:0 5px 0 0}
.textbox4c{margin:0 5px 0 40px}
.textbox3{width:290px;height:19px;border:1px solid #003300;background-color:#ffffff;color:#333333;font-family:tahoma,arial,helvetica,sans-serif;font-size:11px}
.ImagePreviewArea{position:absolute;left:500px;top:-45px;border:1px solid #000;padding:0px;overflow:auto;width:116px;height:100px;background-color:#fff}
</style>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="JavaScript">
loaded=false;
var BaseUrl = document.location.protocol + "//" + document.location.host + "/";
var bCanBrowse = true;
var sBrowseURL = BaseUrl + "admin/htmleditor/filemanager/browse/browse6.php";
var iBrowseWindowWidth  = 720;
var iBrowseWindowHeight = 520;
var bCanUpload = true;
var sUploadURL = BaseUrl + "admin/htmleditor/filemanager/upload/upload.php";
var iUploadWindowWidth  = 300;
var iUploadWindowHeight = 150;
var sUploadAllowedExtensions = ".gif .jpg .jpeg .png";

function checkValues(frm)
{
	var msg="";
	if(frm.programa.value=="")msg+="Necess�rio digitar o Programa a que se refere o Certificado!   \n";
	if(frm.curso.value=="")msg+="Necess�rio digitar o Curso a que se refere o Certificado!   \n";
	if(msg!="")
	{
		alert(msg);
		return false;
	}
	else return true;
}

function submitPage(url,tgt)
{
	if(!checkValues(f))return false;
	f.action=url;
	f.target=tgt;
	f.submit();
}

function copiarDados(){
	if(document.getElementById('certificados').selectedIndex == 0){
		alert('� preciso selecionar um Certificado para c�pia!     ');
		document.getElementById('certificados').focus();
		return false;
	}
	else{
		id=document.getElementById('certificados')[document.getElementById('certificados').selectedIndex].value;
		document.location.href = 'input_certificados.php?id='+id+'&action=3';
	}
}

function uploadFile(){
	var sFile = frmUpload.UploadFile.value;
	if(sFile == ""){
		alert("Nenhum arquivo selecionado.   ") ;
		return ;
	}	

	var sExt = sFile.match( /\.[^\.]*$/ ) ;
	sExt = sExt ? sExt[0].toLowerCase() : "." ;

	if(sUploadAllowedExtensions.indexOf( sExt ) < 0 ){
		alert("Desculpe, apenas os seguintes tipos de arquivo s�o permitidos para upload:\n\n" + sUploadAllowedExtensions + "\n\nOpera��o cancelada.   ") ; 
		return ;
	}
	var oWindow = openNewWindow("", "UploadWindow", iUploadWindowWidth, iUploadWindowHeight) ;

	frmUpload.action = sUploadURL;
	frmUpload.submit() ;
	oWindow.setImage = null ;
}

oWindow = null;
function openNewWindow(sURL, sName, iWidth, iHeight, bResizable, bScrollbars){
	var iTop  = (screen.height - iHeight) / 2;
	var iLeft = (screen.width  - iWidth) / 2;
	
	var sOptions = "toolbar=no" ;
	sOptions += ",width=" + iWidth ; 
	sOptions += ",height=" + iHeight ;
	sOptions += ",resizable="  + (bResizable  ? "yes" : "no") ;
	sOptions += ",scrollbars=" + (bScrollbars ? "yes" : "no") ;
	sOptions += ",left=" + iLeft ;
	sOptions += ",top=" + iTop ;
	
	oWindow = window.open(sURL, sName, sOptions);
	oWindow.focus();
	
	return oWindow ;
}

var currentField = '';
function browserServer(f){
	currentField = f;
	var oWindow = openNewWindow(sBrowseURL, "BrowseWindow", iBrowseWindowWidth, iBrowseWindowHeight) ;
	oWindow.setImage = setImage ;
}

function setImage(sImageURL){
	var imageTMP = new Image();
	var wTMP, hTMP;
	var subdir = '/canal/certificacao/certificado/';
	var imagename = sImageURL.split(subdir)[1];
	if(currentField != ''){
		document.getElementById(currentField).value = imagename;
		imageTMP.onload = function(){
			wTMP = imageTMP.width;
			hTMP = imageTMP.height;
			document.getElementById(currentField+'W').value = Math.round(wTMP / 300 * 2540) / 100;
			document.getElementById(currentField+'H').value = Math.round(hTMP / 300 * 2540) / 100;
		}
		imageTMP.src = sImageURL;
	}
}

function init()
{
	f=document.inputForm;
	loaded=true;
	setTimeout('document.inputForm.programa.focus()',300);
}
</script>
</head>
<body style="background-color:white;margin:0px;border:none" scroll="no" onload="init()">

<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%" style="background-color:buttonface;border:none">
<tr>
<td style="height:1%">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
	<tr>
	<td background="/images/layout/bg_logo_admin.png">
		<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
		<tr>
		<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
		<td align="right" class="data"><?php echo getServerDate(); ?></td>
		</tr>
		</table>	
	</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
	</table>
</td>
</tr>
<tr>
<td style="height:98%;vertical-align:top;padding:15px 15px 20px 15px">
	<table border="0" cellpadding="0" cellspacing="0">
<!-- 	<tr><td class="title" colspan="2" nowrap>CERTIFICADOS</td></tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr> -->
	<tr>
	<td class="textblk" align="right" nowrap valign="top">Carregar dados:&nbsp;</td>
	<td><select id="cerificados" name="certificados" style="width:600px;margin-bottom:20px"><option>Se deseja copiar os dados de certificado existente, selecione-o da lista e clique em "copiar"</option>
<?php
$sql = "SELECT CD_CERTIFICADO, DS_PROGRAMA, DS_CURSO, DT_CRIACAO, IN_ATIVO FROM col_certificado WHERE IN_ATIVO = 1 ORDER BY DS_PROGRAMA, DS_CURSO, DT_CRIACAO";
$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

while($oRs = mysql_fetch_row($query)){
	$programaTMP = str_replace("\\n"," ",$oRs[1]);
	$cursoTMP	 = str_replace("\\n"," ",$oRs[2]);
	$selected	 = ($oRs[0] == $id) ? 'selected' : '';
	echo "<option value=\"$oRs[0]\" $selected title=\"$programaTMP | $cursoTMP - $oRs[3]\">$programaTMP | $cursoTMP - $oRs[3]</otion>";
}
mysql_free_result($query);
?>
</select></td><td valign="top"><input type="button" class="buttonsty" style="width:100px;margin:1px 0 0 5px" value="Copiar" onclick="copiarDados();return false"></td>
	</tr>
	<form name="inputForm" action="" method="post">
	<tr>
	<td class="textblk" align="right" nowrap>Programa*:&nbsp;</td>
	<td><input type="text" name="programa" size="120" maxlength="255" class="textbox" value="<?php echo $programa; ?>"></td>
	<td class="textblk"><input type="text" name="programaDy" maxlength="3" class="textbox4" value="<?php echo $programaDy; ?>">DY</td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Curso*:&nbsp;</td>
	<td><input type="text" name="curso" size="120" maxlength="255" class="textbox" tabIndex="5" value="<?php echo $curso; ?>"></td>
	<td class="textblk"><input type="text" name="cursoDy" maxlength="3" class="textbox4" value="<?php echo $cursoDy; ?>">DY</td>
	</tr>
	
	<tr>
	<td class="textblk" align="right" nowrap>Logo 1:&nbsp;</td>
	<td><input type="text" name="logo1" size="120" maxlength="255" class="textbox2" value="<?php echo $logo1; ?>"><input type="button" class="buttonsty" style="margin:-21px 0 0 5px" value="Procurar no Servidor" onclick="browserServer('logo1')" onfocus="noFocus(this)"></td>
	<td class="textblk"><input type="text" name="logo1Dy" maxlength="3" class="textbox4" value="<?php echo $logo1Dy; ?>">DY</td>
	<td class="textblk"><input type="text" name="logo1Dx" maxlength="3" class="textbox4b" value="<?php echo $logo1Dx; ?>">DX</td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Logo 2:&nbsp;</td>
	<td><input type="text" name="logo2" size="120" maxlength="255" class="textbox2" value="<?php echo $logo2; ?>"><input type="button" class="buttonsty" style="margin:-21px 0 0 5px" value="Procurar no Servidor" onclick="browserServer('logo2')" onfocus="noFocus(this)"></td>
	<td class="textblk"><input type="text" name="logo2Dy" maxlength="3" class="textbox4" value="<?php echo $logo2Dy; ?>">DY</td>
	<td class="textblk"><input type="text" name="logo2Dx" maxlength="3" class="textbox4b" value="<?php echo $logo2Dx; ?>">DX</td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Marca d'�gua:&nbsp;</td>
	<td><input type="text" name="marcadagua" size="120" maxlength="255" class="textbox2" value="<?php echo $marcadagua; ?>"><input type="button" class="buttonsty" style="margin:-21px 0 0 5px" value="Procurar no Servidor" onclick="browserServer('marcadagua')" onfocus="noFocus(this)"></td>
	<td class="textblk"><input type="text" name="marcadaguaDy" maxlength="3" class="textbox4" value="<?php echo $marcadaguaDy; ?>">DY</td>
	<td class="textblk"><input type="text" name="marcadaguaDx" maxlength="3" class="textbox4b" value="<?php echo $marcadaguaDx; ?>">DX</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td class="textblk" align="right" nowrap>Nome*:&nbsp;</td>
	<td><input type="text" name="nome" size="120" maxlength="64" class="textbox" value="<?php echo $nome; ?>"></td>
	<td class="textblk"><input type="text" name="linhaDy" maxlength="3" class="textbox4" value="<?php echo $linhaDy; ?>">DY</td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Texto 1:&nbsp;</td>
	<td><input type="text" name="txt1" size="120" maxlength="255" class="textbox" value="<?php echo $txt1; ?>"></td>
	<td class="textblk"><input type="text" name="txt1Dy" maxlength="3" class="textbox4" value="<?php echo $txt1Dy; ?>">DY</td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Texto 2:&nbsp;</td>
	<td><input type="text" name="txt2" size="120" maxlength="255" class="textbox" value="<?php echo $txt2; ?>"></td>
	<td class="textblk"><input type="text" name="txt2Dy" maxlength="3" class="textbox4" value="<?php echo $txt2Dy; ?>">DY</td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Texto 3:&nbsp;</td>
	<td><input type="text" name="txt3" size="120" maxlength="255" class="textbox" value="<?php echo $txt3; ?>"></td>
	<td class="textblk"><input type="text" name="txt3Dy" maxlength="3" class="textbox4" value="<?php echo $txt3Dy; ?>">DY</td>
	</tr>	
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td class="textblk" align="right" nowrap>Assinatura 1:&nbsp;</td>
	<td><input type="text" name="assinatura1" size="120" maxlength="255" class="textbox2" value="<?php echo $assinatura1; ?>"><input type="button" class="buttonsty" style="margin:-21px 0 0 5px" value="Procurar no Servidor" onclick="browserServer('assinatura1')" onfocus="noFocus(this)"></td>
	<td class="textblk"><input type="text" name="assinatura1Dy" maxlength="3" class="textbox4" value="<?php echo $assinatura1Dy; ?>">DY</td>
	<td class="textblk"><input type="text" name="assinatura1Dx" maxlength="3" class="textbox4b" value="<?php echo $assinatura1Dx; ?>">DX</td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Nome 1:&nbsp;</td>
	<td class="textblk"><input type="text" name="nome1" size="120" maxlength="64" class="textbox3" value="<?php echo $nome1; ?>"></td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Cargo 1:&nbsp;</td>
	<td class="textblk"><input type="text" name="cargo1" size="120" maxlength="64" class="textbox3" value="<?php echo $cargo1; ?>"></td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Conjunto 1:&nbsp;</td>
	<td class="textblk"><input type="text" name="conjunto1Dy" maxlength="3" class="textbox4b" value="<?php echo $conjunto1Dy; ?>">DY<input type="text" name="conjunto1Dx" class="textbox4c" value="<?php echo $conjunto1Dx; ?>">DX</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td class="textblk" align="right" nowrap>Assinatura 2:&nbsp;</td>
	<td><input type="text" name="assinatura2" size="120" maxlength="255" class="textbox2" value="<?php echo $assinatura2; ?>"><input type="button" class="buttonsty" style="margin:-21px 0 0 5px" value="Procurar no Servidor" onclick="browserServer('assinatura2')" onfocus="noFocus(this)"></td>
	<td class="textblk"><input type="text" name="assinatura2Dy" maxlength="3" class="textbox4" value="<?php echo $assinatura2Dy; ?>">DY</td>
	<td class="textblk"><input type="text" name="assinatura2Dx" maxlength="3" class="textbox4b" value="<?php echo $assinatura2Dx; ?>">DX</td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Nome 2:&nbsp;</td>
	<td class="textblk"><input type="text" name="nome2" size="120" maxlength="64" class="textbox3" value="<?php echo $nome2; ?>"></td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Cargo 2:&nbsp;</td>
	<td class="textblk"><input type="text" name="cargo2" size="120" maxlength="64" class="textbox3" value="<?php echo $cargo2; ?>"></td>
	</tr>
	<tr>
	<td class="textblk" align="right" nowrap>Conjunto 2:&nbsp;</td>
	<td class="textblk"><input type="text" name="conjunto2Dy" maxlength="3" class="textbox4b" value="<?php echo $conjunto2Dy; ?>">DY<input type="text" name="conjunto2Dx" class="textbox4c" value="<?php echo $conjunto2Dx; ?>">DX</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td class="textblk" align="right" nowrap>Local e Data:&nbsp;</td>
	<td class="textblk"><input type="text" name="data" size="120" maxlength="128" class="textbox3" value="<?php echo $data; ?>"><input type="text" name="dataDy" maxlength="3" class="textbox4" value="<?php echo $dataDy; ?>">DY</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td class="textblk" align="right" nowrap>Com louvor:&nbsp;</td>
	<td class="textblk"><input type="radio" name="medalhaStatus" value="1" <?php echo ($medalhaStatus == 1)?'checked':''; ?>>Sim&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="medalhaStatus" value="0" <?php echo ($medalhaStatus != 1)?'checked':''; ?>>N�o&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="medalhaDy" maxlength="3" class="textbox4c" value="<?php echo $medalhaDy; ?>">DY<input type="text" name="medalhaDx" maxlength="3" class="textbox4c" value="<?php echo $medalhaDx; ?>">DX</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td class="textblk" align="right" nowrap>No. Certificado:&nbsp;</td>
	<td class="textblk"><input type="text" name="nu_certificado" size="60" maxlength="16" class="textbox3" style="width:145px" value="<?php echo $nu_certificado; ?>"></td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	</table>
</td>
</tr>
<tr>
<td style="height:1%;vertical-align:top;padding:15px 15px 15px 15px">
	<input type="hidden" name="id" value="<?php echo $id; ?>">
	<input type="hidden" name="logo1W" value="<?php echo $logo1W; ?>">
	<input type="hidden" name="logo1H" value="<?php echo $logo1H; ?>">
	<input type="hidden" name="logo2W" value="<?php echo $logo2W; ?>">
	<input type="hidden" name="logo2H" value="<?php echo $logo2H; ?>">
	<input type="hidden" name="marcadaguaW" value="<?php echo $marcadaguaW; ?>">
	<input type="hidden" name="marcadaguaH" value="<?php echo $marcadaguaH; ?>">
	<input type="hidden" name="assinatura1W" value="<?php echo $assinatura1W; ?>">
	<input type="hidden" name="assinatura1H" value="<?php echo $assinatura1H; ?>">
	<input type="hidden" name="assinatura2W" value="<?php echo $assinatura2W; ?>">
	<input type="hidden" name="assinatura2H" value="<?php echo $assinatura2H; ?>">
	<!-- <input type="hidden" name="numerocertificado" value="C0000/2008"> -->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
	<td align="center"><input type="button" class="buttonsty" value="Cancelar" onclick="self.close()" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Preview" onclick="submitPage('/canal/certificacao/certificado/preview_certificado.php','_blank')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Submeter dados" onclick="submitPage('/admin/updatecertificado.php','')" onfocus="noFocus(this)">
	</td>
	</tr>
	</table>
</td>
</tr>
</table>
</form>

<div id="uploadLayer" style="position:absolute;width:390px;height:100px;left:53px;top:630px">
	<form name="frmUpload" id="frmUpload" enctype="multipart/form-data" method="post" target="UploadWindow">
	<table border="0" cellpadding="0" cellspacing="0" width="1%">
	<tr>
	<td class="textblk" align="right" nowrap>Upload:&nbsp;</td>
	<td><input type="file" class="textbox3" name="UploadFile" id="UploadFile"></td>
	<td style="padding-left:5px"><input type="button" class="buttonsty" value="Enviar para o Servidor" onclick="uploadFile()" onfocus="noFocus(this)"></td>
	</tr>
	</table>
	<input type="hidden" name="subdir" value="">
	<input type="hidden" name="UploadBaseDir" value="/canal/certificacao/certificado/images/">
	</form>
</div>

</body>
</html>
<?php
mysql_close();
?>