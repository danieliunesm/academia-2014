<?php

$htmleditorBasePath = "/admin/htmleditor/" ;

class htmleditor
{
	var $ToolbarSet ;
	var $Value ;
	var $CanUpload ;
	var $CanBrowse ;

	function htmleditor()
	{
		$this->ToolbarSet = '' ;
		$this->Value = '' ;
		$this->CanUpload = 'none' ;
		$this->CanBrowse = 'none' ;
	}
	
	function Createhtmleditor($instanceName, $width, $height)
	{
		echo $this->Returnhtmleditor($instanceName, $width, $height) ;
	}
	
	function Returnhtmleditor($instanceName, $width, $height)
	{
		$grstr = htmlspecialchars( $this->Value ) ;
        $grstr = $this->Value;
		$strEditor = "" ;
//		$grstr = htmlentities( $this->Value ) ;
//		if ( $this->IsCompatible() )
//		{
//			global $htmleditorBasePath ;
//			$sLink = $htmleditorBasePath . "htmleditor.html?FieldName=$instanceName" ;
//
//			if ( $this->ToolbarSet != '' )
//				$sLink = $sLink . "&Toolbar=$this->ToolbarSet" ;
//
//			if ( $this->CanUpload != 'none' )
//			{
//				if ($this->CanUpload == true)
//					$sLink = $sLink . "&Upload=true" ;
//				else
//					$sLink = $sLink . "&Upload=false" ;
//			}
//
//			if ( $this->CanBrowse != 'none' )
//			{
//				if ($this->CanBrowse == true)
//					$sLink = $sLink . "&Browse=true" ;
//				else
//					$sLink = $sLink . "&Browse=false" ;
//			}
//			$strEditor .= "<IFRAME name=\"" . $instanceName . "Frame\" src=\"$sLink\" width=\"$width\" height=\"$height\" frameborder=\"no\" scrolling=\"no\"></IFRAME>" ;
//			$strEditor .= "<INPUT type=\"hidden\" id=\"$instanceName\" name=\"$instanceName\" value=\"$grstr\">" ;
//		}
//		else
//		{
//			$strEditor .= "<TEXTAREA id=\"$instanceName\" name=\"$instanceName\" rows=\"4\" cols=\"40\" style=\"WIDTH: $width; HEIGHT: $height\" wrap=\"virtual\">$grstr</TEXTAREA>" ;
//		}
		$strEditor .= " <script type='text/javascript' src='/admin/ckeditor/ckeditor.js'></script>";
		$strEditor .= " <TEXTAREA id=\"$instanceName\" name=\"$instanceName\" rows=\"4\" cols=\"40\" style=\"WIDTH:100%; HEIGHT: $height\" wrap=\"virtual\">$grstr</TEXTAREA>" ;
		$strEditor .= " <script type='text/javascript'>
				//<![CDATA[
				CKEDITOR.replace( '$instanceName', {skin:'office2003', enterMode:CKEDITOR.ENTER_BR/*, toolbar:'$this->ToolbarSet'*/});
				//]]>
						</script>";
		
		return $strEditor;
	}
	
//	function IsCompatible()
//	{
//		$sAgent = $_SERVER['HTTP_USER_AGENT'] ;
//
//		if ( is_integer( strpos($sAgent, 'MSIE') ) && is_integer( strpos($sAgent, 'Windows') ) && !is_integer( strpos($sAgent, 'Opera') ) )
//		{
//			$iVersion = (int)substr($sAgent, strpos($sAgent, 'MSIE') + 5, 1) ;
//			return ($iVersion >= 5) ;
//		} else {
//			return FALSE ;
//		}
//	}
}
?>