<?
include "../../include/security.php";
include "../../include/defines.php";
include "../../include/dbconnection.php";
include "../../include/genericfunctions.php";
include "htmleditor.php";
include "../controles.php";
include "../page.php";

function formataData($valor)
{
	
	$valor = strftime("%d/%m/%Y", strtotime($valor));
	return $valor;
}

$id 	 	 = $_GET["id"];
$action	 	 = $_GET["action"];

$titulo  	 = '';
$pastaid 	 = '';
$usu	 	 = 0;
$emailpadrao = 'colaborae@colaborae.com.br';
$email 		 = $emailpadrao;
$vigenciaInicio = "";
$vigenciaTermino = "";

function EscreveDados($oID,$oAction)
{
	global  $ohtmleditor, $titulo, $id, $pastaid, $usu, $email, $emailpadrao, $vigenciaInicio, $vigenciaTermino;

	if($oAction == 2)
	{
		$sql = "SELECT CD_FORUM, DS_FORUM, CD_EMPRESA, CD_USUARIO_MEDIADOR, TEXTAREA, EMAIL, DT_INICIO_VIGENCIA, DT_TERMINO_VIGENCIA FROM col_foruns WHERE CD_FORUM = " . $oID;
		$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

		if($oRs = mysql_fetch_row($RS_query))
		{
//			$sql2 = "SELECT login FROM col_usuario WHERE CD_USUARIO = $oRs[3]";
//			$RS_query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());
//			if($oRs2 = mysql_fetch_row($RS_query2))
//			{
//				$usu			= $oRs2[0];
//			}
//			mysql_free_result($RS_query2);
			
			$id				    = $oRs[0];
			$titulo 			= $oRs[1];
			$pastaid			= $oRs[2];
			$usu				= $oRs[3];
			$ohtmleditor->Value = $oRs[4];
			$email				= $oRs[5] != '' ? $oRs[5] : $emailpadrao;
			$vigenciaInicio		= formataData($oRs[6]);
			$vigenciaTermino	= formataData($oRs[7]);
			
		}
		mysql_free_result($RS_query);
	}
}

$ohtmleditor = New htmleditor;
$ohtmleditor->ToolbarSet = 'thisHtmlEditorToolbarSet';

EscreveDados($id,$action);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<style type="text/css">
.title{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}
.text{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.textblk{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.data{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:8pt;font-weight:bold;color:#ffffff}
.legenda{font-family:"Microsoft Sans Serif","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#cc0000}
.buttonsty{width:150px;height:19px;background-color:#dddddd;border:2px solid #000099;font-family:"verdana","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000;cursor:hand;line-height:11px}
</style>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script type="text/javascript" src="../include/js/functions.js"></script>
<script language="javascript" src="js/dhtmled.js"></script>
<script language="JavaScript">
loaded=false;
function validaEmail(emailStr){
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")	;
	var matchArray=emailStr.match(emailPat);

	if(matchArray==null)return false;

	var user=matchArray[1];
	var domain=matchArray[2];

	if(user.match(userPat)==null)return false;

	var IPArray=domain.match(ipDomainPat);
	if(IPArray!=null){
		for(var i=1;i<=4;i++){
			if(IPArray[i]>255)return false;
		}
		return true;
	}	
	var domainArray=domain.match(domainPat);

	if(domainArray==null)return false;
	
	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;

	if(domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>4)return false;

	if(len<2)return false;

	return true;
}

function checkValues(frm)
{
	var msg="";
	if(frm.pastaid.selectedIndex == 0)msg+="Necess�rio definir a Empresa � qual ser� associada a Lota��o!   \n";
	if(frm.titulo.value=="")msg+="Necess�rio definir o nome da Lota��o!   \n";
	if(frm.email.value == "") msg+="O campo E-mail de Suporte precisa ser preenchido!       \n";
	if(frm.txtDataInicioVigencia.value == "") msg+="O campo In�cio Per�odo de Vig�ncia precisa ser preenchido!       \n";
	if(frm.txtDataTerminoVigencia.value == "") msg+="O campo T�rmino Per�odo de Vig�ncia precisa ser preenchido!       \n";
	else{
		if(!validaEmail(frm.email.value))msg+="O e-mail fornecido n�o � v�lido! Verifique os dados digitados.       \n";
	}
	if(msg!="")
	{
		alert(msg);
		return false;
	}
	else return true;
}

function submitPage(url,tgt)
{
	if(!checkValues(f))return false;
	f.action=url;
	f.target=tgt;
	f.submit();
}

function init()
{
	document.onkeypress=mascara;
	document.onmousedown=checkSrc;
	document.onmouseup=releaseSrc;
	document.onselectstart=unselectElement;
	f=document.inputForm;
	loaded=true;
	setTimeout('document.inputForm.titulo.focus()',300);
}
</script>
</head>
<body style="background-color:white;margin:0px;border:none" scroll="no" onload="init()">

<form name="inputForm" action="" method="post">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr><td>
<table border="0" cellpadding="0" cellspacing="15" style="background-color:buttonface;border:none;width:100%;height:100%" align="center">
<tr>
<td height="1%">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr><td class="title" colspan="2" nowrap>F�RUNS</td></tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>
	<tr>
	<td class="textblk" align="right" width="1%" nowrap>Nome&nbsp;do&nbsp;F�rum*:&nbsp;</td>
	<td width="99%"><input type="text" name="titulo" size="120" maxlength="120" style="width:370px;font-family:arial,helvetica,sans-serif;font-size:11px" tabIndex="5" value='<? echo $titulo; ?>'></td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td class="textblk" align="right" width="1%" nowrap>Programa*:&nbsp;</td>
	<td width="99%"><select name="pastaid" class="textbox8" style="width:370px" tabIndex="4">
			   <option id="" value="0">Selecione o Programa ao qual ser� associado o F�rum</option>
<?
$sql2 = "SELECT CD_EMPRESA, DS_EMPRESA FROM col_empresa WHERE IN_ATIVO = 1 ORDER BY DS_EMPRESA";
$query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());

while($oRs2 = mysql_fetch_row($query2)){
	if((int)($pastaid) == (int)($oRs2[0])) $selected = "selected";else $selected = "";
	echo "<option value=\"" . $oRs2[0] . "\" " . $selected . ">" . $oRs2[1] . "</option>";
}
mysql_free_result($query2);
?></select>
	</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td class="textblk" align="right" width="1%" nowrap>Mediador*:&nbsp;</td>
	<td width="99%"><select name="mediador" class="textbox8" style="width:370px" tabIndex="4">
			   <option id="" value="0">Selecione o Mediador do F�rum</option>
<?
$sql2 = "SELECT CD_USUARIO, login FROM col_usuario WHERE (STATUS = 1 OR STATUS = 5) AND tipo >=3 AND login <> '' ORDER BY login";
$query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());

while($oRs2 = mysql_fetch_row($query2)){
	if((int)($usu) == (int)($oRs2[0])) $selected = "selected";else $selected = "";
	echo "<option value=\"" . $oRs2[0] . "\" " . $selected . ">" . $oRs2[1] . "</option>";
}
mysql_free_result($query2);
?></select>
	</td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	
	<tr>
	<td class="textblk" align="right" width="1%" nowrap>E-mail suporte*:&nbsp;</td>
	<td width="99%"><input type="text" name="email" class="textbox8" style="width:370px" tabIndex="5" maxlength="255" value="<?php echo $email; ?>">
	</td>
	</tr>
	
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr>
	<td class="textblk" align="right" width="1%" nowrap>Per�odo de Vigencia*:&nbsp;</td>
	
	
	<td width="99%">
	<?php textbox("txtDataInicioVigencia", "10", $vigenciaInicio, "110px", "textbox3", "In�cio Per�odo de Vig�ncia", Mascaras::Data) ?>
		a
		<?php textbox("txtDataTerminoVigencia", "10", $vigenciaTermino, "110px", "textbox3", "T�rmino Per�odo de Vig�ncia", Mascaras::Data) ?>
	
	</td>
	</tr>
	
	
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	
	</table>
</td>
</tr>
<tr><td align="center" valign="top">
<?
$ohtmleditor->Createhtmleditor('htmleditor', '100%', "100%");
?>
</td></tr>
<tr><td height="1%">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td align="center"><input type="button" class="buttonsty" value="Cancelar" onclick="self.close()" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Preview" onclick="submitPage('preview_forum.php','_blank')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Submeter dados" onclick="submitPage('/admin/updateforum.php','')" onfocus="noFocus(this)">
</td></tr>
</table>
</td></tr>
</table>
</td></tr></table>
<input type="hidden" name="id" value="<? echo $id; ?>">
</form>
</body>
</html>
<?
mysql_close();
?>