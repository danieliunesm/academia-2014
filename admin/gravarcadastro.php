<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";
include "../include/cripto.php";
//require('../blog/wp-blog-header.php');

function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/admin/gravarcadastro.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

reset($_POST);

$filtro					= $_POST["filtro"];
$tipo					= $_POST['tipo'];
$grupo					= 1;
$permissao				= "";
$nome					= "";
$cd_usuario 			= 0;
$inAlteraSenha = $_POST["chkAlteraSenha"];

$UPLOAD_BASE_DOC_DIR	= "/usuarios/";
$UPLOAD_BASE_DOC 		= getDocumentRoot().$UPLOAD_BASE_DOC_DIR;

$UPLOAD_BASE_IMG_DIR	= "/images/colaboradores/";
$UPLOAD_BASE_IMG 		= getDocumentRoot().$UPLOAD_BASE_IMG_DIR;

$id						= $_POST["id"];
$nome					= $_POST["nome"];
$sobrenome				= $_POST["sobrenome"];
$datanasc				= $_POST["datanasc"];
$naturalidade			= $_POST["naturalidade"];
$uf						= $_POST["uf"];
$nacionalidade			= $_POST["nacionalidade"];
$enderecores			= $_POST["enderecores"];
$cepres					= $_POST["cepres"];
$dddtelres				= $_POST["dddtelres"];
$telres					= $_POST["telres"];
$dddcel					= $_POST["dddcel"];
$cel					= $_POST["cel"];
$email					= $_POST["email"];
$cd_empresa				= $_POST["cd_empresa"];
//$nomeempresa			= $_POST["empresa"];
$cd_lotacao				= $_POST["cd_lotacao"];
$cd_localidade			= ($_POST["cd_localidade"]>0?$_POST["cd_localidade"]:"NULL");
//$lotacao				= $_POST["lotacao"];
$cargofuncao			= $_POST["cargofuncao"];
$enderecocom			= $_POST["enderecocom"];
$cepcom					= $_POST["cepcom"];
$dddtelcom				= $_POST["dddtelcom"];
$telcom					= $_POST["telcom"];
$ramal					= $_POST["ramal"];
$experiencia			= $_POST["experiencia"];
$outrosexperiencia		= $_POST["outrosexperiencia"];
$especializacao			= $_POST["especializacao"];
$outrosespecializacao	= $_POST["outrosespecializacao"];
$fotofile				= $_POST["fotofileuploaded"];
$cvfile					= $_POST["cvfileuploaded"];
$newsletteragreement	= isset($_POST["newsletteragreement"]) ? $_POST["newsletteragreement"] : "0";
$policyagreement		= isset($_POST["policyagreement"]) ? $_POST["policyagreement"] : "0";

$login 					= $_POST["userlogin"];
$senha 					= $_POST["userpass1"];

$experiencia			= $experiencia != '' ? implode(",", $experiencia) : '';
$especializacao			= $especializacao != '' ? implode(",", $especializacao) : '';
$newsletteragreement	= $newsletteragreement == "on" ? "1" : "0";
$policyagreement		= $policyagreement == "on" ? "1" : "0";

$uploadfotoerror = 0;
$uploadcverror   = 0;


if(is_uploaded_file($_FILES['foto']['tmp_name'])){
	$savefile = $UPLOAD_BASE_IMG.$_FILES['foto']['name'];
	if(move_uploaded_file($_FILES['foto']['tmp_name'], $savefile)){
		chmod($savefile, 0666);
		$fotofile = $_FILES['foto']['name'];
	}
}
else{
	$uploadfotoerror = (Integer)$_FILES['foto']['error'] + 1;
}

if(is_uploaded_file($_FILES['cv']['tmp_name'])){
	$savefile = $UPLOAD_BASE_DOC.$_FILES['cv']['name'];
	if(move_uploaded_file($_FILES['cv']['tmp_name'], $savefile)){
		chmod($savefile, 0666);
		$cvfile = $_FILES['cv']['name'];
	}
}
else{
	$uploadcverror = (Integer)$_FILES['cv']['error'] + 1;
}

/*
foreach ($cd_empresa as $programa)
{
	
	//Verifica se j� est� cadastrado no programa
	$sql = "SELECT CD_USUARIO FROM col_usuario WHERE empresa = $programa";
	
}
*/

if($id != ""){
	$sql  = "UPDATE col_usuario SET ";
	$sql .= "tipo = '" . $tipo . "', ";
	$sql .= "grupo = '" . $grupo . "', ";
	$sql .= "permissao = '" . $permissao . "', ";
	$sql .= "NM_USUARIO = '" . $nome . "', ";
	$sql .= "NM_SOBRENOME = '" . $sobrenome . "', ";
	$sql .= "datanasc = '" . $datanasc . "', ";
	$sql .= "naturalidade = '" . $naturalidade . "', ";
	$sql .= "uf = '" . $uf . "', ";
	$sql .= "nacionalidade = '" . $nacionalidade . "', ";
	$sql .= "enderecores = '" . $enderecores . "', ";
	$sql .= "cepres = '" . $cepres . "', ";
	$sql .= "dddtelres = '" . $dddtelres . "', ";
	$sql .= "telres = '" . $telres . "', ";
	$sql .= "dddcel = '" . $dddcel . "', ";
	$sql .= "cel = '" . $cel . "', ";
	$sql .= "email = '" . $email . "', ";
	$sql .= "empresa = '" . $cd_empresa . "', ";
	$sql .= "lotacao = '" . $cd_lotacao . "', ";
	$sql .= "CD_LOCALIDADE = $cd_localidade, ";
	$sql .= "cargofuncao = '" . $cargofuncao . "', ";
	$sql .= "enderecocom = '" . $enderecocom . "', ";
	$sql .= "cepcom = '" . $cepcom . "', ";
	$sql .= "dddtelcom = '" . $dddtelcom . "', ";
	$sql .= "telcom = '" . $telcom . "', ";
	$sql .= "ramal = '" . $ramal . "', ";
	$sql .= "experiencia = '" . $experiencia . "', ";
	$sql .= "outrosexperiencia = '" . $outrosexperiencia . "', ";
	$sql .= "especializacao = '" . $especializacao . "', ";
	$sql .= "outrosespecializacao = '" . $outrosespecializacao . "', ";
	$sql .= "fotofile = '" . $fotofile . "', ";
	$sql .= "cvfile = '" . $cvfile . "', ";
//	$sql .= "cv = '" . $cv . "', ";
	$sql .= "newsletteragreement = '" . $newsletteragreement . "', ";
	$sql .= "policyagreement = '" . $policyagreement . "', ";
	
	$sql .= "login = '" . $login . "', ";
	$sql .= "senha = '" . str_replace("'","''",Cripto($senha,"C")) . "', ";
	
	$sql .= "CD_FILTRO = $filtro ";
	
	//$sql .= "datacadastro = datacadastro ";
	$sql .= "WHERE CD_USUARIO = " . $id;
}
else{
	$sql  = "INSERT INTO col_usuario(";
	$sql .= "tipo,";
	$sql .= "grupo,";
	$sql .= "permissao,";
	$sql .= "NM_USUARIO,";
	$sql .= "NM_SOBRENOME,";
	$sql .= "datanasc,";
	$sql .= "naturalidade,";
	$sql .= "uf,";
	$sql .= "nacionalidade,";
	$sql .= "enderecores,";
	$sql .= "cepres,";
	$sql .= "dddtelres,";
	$sql .= "telres,";
	$sql .= "dddcel,";
	$sql .= "cel,";
	$sql .= "email,";
	$sql .= "empresa,";
	$sql .= "lotacao,";
	$sql .= "CD_LOCALIDADE,";
	$sql .= "cargofuncao,";
	$sql .= "enderecocom,";
	$sql .= "cepcom,";
	$sql .= "dddtelcom,";
	$sql .= "telcom,";
	$sql .= "ramal,";
	$sql .= "experiencia,";
	$sql .= "outrosexperiencia,";
	$sql .= "especializacao,";
	$sql .= "outrosespecializacao,";
	$sql .= "fotofile,";
	$sql .= "cvfile,";
//	$sql .= "cv,";
	$sql .= "newsletteragreement,";
	$sql .= "policyagreement,";
	
	$sql .= "login,";
	$sql .= "senha,";
	
	$sql .= "CD_FILTRO,";
	
	$sql .= "datacadastro,";
	$sql .= "Status";
	$sql .= ") VALUES (";
	$sql .= "'"  . $tipo . "'";
	$sql .= ",'" . $grupo . "'";
	$sql .= ",'" . $permissao . "'";
	$sql .= ",'" . $nome . "'";
	$sql .= ",'" . $sobrenome . "'";
	$sql .= ",'" . $datanasc . "'";
	$sql .= ",'" . $naturalidade . "'";
	$sql .= ",'" . $uf . "'";
	$sql .= ",'" . $nacionalidade . "'";
	$sql .= ",'" . $enderecores . "'";
	$sql .= ",'" . $cepres . "'";
	$sql .= ",'" . $dddtelres . "'";
	$sql .= ",'" . $telres . "'";
	$sql .= ",'" . $dddcel . "'";
	$sql .= ",'" . $cel . "'";
	$sql .= ",'" . $email . "'";
	$sql .= ",'" . $cd_empresa . "'";
	$sql .= ",'" . $cd_lotacao . "'";
	$sql .= ", $cd_localidade";
	$sql .= ",'" . $cargofuncao . "'";
	$sql .= ",'" . $enderecocom . "'";
	$sql .= ",'" . $cepcom . "'";
	$sql .= ",'" . $dddtelcom . "'";
	$sql .= ",'" . $telcom . "'";
	$sql .= ",'" . $ramal . "'";
	$sql .= ",'" . $experiencia . "'";
	$sql .= ",'" . $outrosexperiencia . "'";
	$sql .= ",'" . $especializacao . "'";
	$sql .= ",'" . $outrosespecializacao . "'";
	$sql .= ",'" . $fotofile . "'";
	$sql .= ",'" . $cvfile . "'";
//	$sql .= ",''";								// CV
	$sql .= ",'" . $newsletteragreement . "'";
	$sql .= ",'" . $policyagreement . "'";
	
	$sql .= ",'" . $login . "'";
	$sql .= ",'" . str_replace("'","''",Cripto($senha,"C")) . "'";
	
	$sql .= ", $filtro";
	
	$sql .= ",NOW()";							// NOW()
	$sql .= ",'1'";								// STATUS
	$sql .= ")";
}

if($RS_query = mysql_query($sql)){

	$codigoUsuario = 0;
	
	if ($id == "")
	{
		$RS_query = mysql_query('SELECT MAX(CD_USUARIO) FROM col_usuario');
		if($oRs = mysql_fetch_row($RS_query)){
			$codigoUsuario = $oRs[0];
		}
	}
	else 
	{
		$codigoUsuario = $id;
	}
	
//	Codigo para inserir o usuario automaticamente no blog.
//	if ($tipo > 2 && $tipo < 6){
//		$user_data = array();
//		$user_data["user_pass"] = $senha;
//	    $user_data["user_login"] = $login;
//	    $user_data["user_nicename"] = $login;
//	    $user_data["user_email"] = $email;
//	    $user_data["display_name"] = $login;
//	    $user_data["nickname"] = $login;
//	    $user_data["first_name"] = $nome;
//	    $user_data["last_name"] = $sobrenome;
//	    $user_data["imgusuario"] = $fotofile;
//	    $user_data["idcolaborae"] = $codigoUsuario;
//	    $user_data["role"] = "author";
//		$result = wp_insert_user($user_data);
//	}
	
	if ($inAlteraSenha == 1)
	{
		$sql = "UPDATE col_usuario SET IN_SENHA_ALTERADA = 0 WHERE CD_USUARIO = $codigoUsuario";
		mysql_query($sql);
		
	}

	$resultado = mysql_query("SELECT CD_PROVA FROM col_prova_vinculo WHERE (CD_VINCULO = '$cd_empresa' AND IN_TIPO_VINCULO = 1)
							OR (CD_VINCULO = '$cd_lotacao' AND IN_TIPO_VINCULO = 2)");
	
	while($linhaProva = mysql_fetch_array($resultado)) {
	
		$sql = "INSERT INTO col_prova_aplicada (CD_PROVA, CD_USUARIO) VALUES ({$linhaProva["CD_PROVA"]}, $codigoUsuario)";
		mysql_query($sql);
	
	}

	header('Location:retornocadastro.php?erro=0&up1='.$uploadfotoerror.'&up2='.$uploadcverror);
}
else{
	header('Location:retornocadastro.php?erro=1&up1='.$uploadfotoerror.'&up2='.$uploadcverror);
}
mysql_close();
?>