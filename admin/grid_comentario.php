<?php
include("../include/defines.php");
include('framework/crud.php');
include('controles.php');
include('datagrid.php');

$paginaEdicao = "cadastro_comentario.php";
$paginaEdicaoAltura = "(screen.height-80)";
$paginaEdicaoLargura = "(screen.width-40)";

function instanciarRN(){
	return new col_comentario();
}


define(TITULO_PAGINA, "Comentários");
define(BOTAO_ADICIONAR, "Incluir Comentário");
define(BOTAO_MODO_EXIBICAO, "Visualizar Comentários");

include('grid_cabecalho.php');

?>

<table cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td class="textblk">
			Programa:
		</td>
	</tr>
	<tr>
		<td>
			<?php
			comboboxEmpresa("cboEmpresa", $_POST["cboEmpresa"]);
			?>		
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="center">
			<input type='submit' name='btnPesquisa' class='buttonsty' value='Filtrar'>
		</td>
	</tr>

</table>


<?php 


$rn = new col_comentario();
$resultado = $rn->listarTodos($_POST["cboEmpresa"],"DS_EMPRESA, DS_PAGINA");

dataGridColaborae(array("EMPRESA", "PÁGINA"), array("DS_EMPRESA", "DS_PAGINA"), array("50%","50%"), $resultado, "CD_COMENTARIO", true);

include('grid_rodape.php');

?>