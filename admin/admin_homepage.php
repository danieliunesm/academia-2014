<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";

$searchString = $_SERVER["QUERY_STRING"];
//session_register("searchString");

$vpp = isset($_GET["vpp"])?$_GET["vpp"]:1;

function getSTATUScheck($oView,$tb)
{
	switch($oView)
	{
		case 1:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 5)";
		case 3:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 2 OR " . $tb . "STATUS = 5)";
		case 5:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 3 OR " . $tb . "STATUS = 5)";
		case 7:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 2 OR " . $tb . "STATUS = 3 OR " . $tb . "STATUS = 5)";
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Educa��o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<!-- <script language="JavaScript" src="htmleditor/js/dyntable.js"></script> -->
<script language="JavaScript">
function init()
{
<?
if($_SESSION["msg"] != "")
{
	echo "	alert(\"" . $_SESSION["msg"]. "\");";
	$_SESSION["msg"] = "";
}
?>
}

function openWindow(url)
{
	newWin=null;
	var w=screen.width-40;
	var h=screen.height-80;
	var l=10;
	var t=20;
	newWin=window.open(url,'htmleditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=0,resizable=1');
	if(newWin!=null)setTimeout('newWin.focus()',100);
}

function getURL(pg,s,i,a)
{
	openWindow('/admin/htmleditor/input_'+pg+'.php?sessid='+s+'&id='+i+'&action='+a);
}

function ShowDialog(pagePath, args, width, height, left, top)
{
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
}

oVpp = <? echo $vpp;?>;

function viewControl(vSession)
{
	oVsession = vSession;
	var html = ShowDialog("/admin/htmleditor/dialog/viewhpcontrol.html", window, 420, 210);
	if(html)document.location.href=html;
}
/*
function checkDuplicate(){
	var msg 	= ''; 
	tbObj   	= 'tb1';
	dupflag 	= [];
	dupflag[0] 	= 0;
	dupflag[1] 	= 0;
	dupflag[2] 	= 0;
	dupflag[3] 	= 0;
	FR=document.getElementById(tbObj+'FR');
	LR=document.getElementById(tbObj+'LR');
	tableObj=document.getElementById(tbObj);
	FRind=getRowIndex(tableObj,FR);
	LRind=getRowIndex(tableObj,LR);
	for(var i=FRind+1;i<LRind;i++){
		for(var j=0; j<4; j++){
			if(document.getElementById(tableObj.rows[i].id+'Col'+j).firstChild.checked && document.getElementById(tableObj.rows[i].id+'PUB').firstChild.checked){
				dupflag[j]+=1;
			}
			if(dupflag[j]>1)msg+='H� mais de uma chamada para a coluna ' + (j+1) +'! Voc� deve manter apenas uma com o status de publicada.     \n';
		}
	}
	return msg;
}
*/
function checkValues(f)
{
	var msg="";
//	msg = checkDuplicate();
	if(msg!="")
	{
		alert(msg);
		return false;
	}
	else return true;
}

function submitPage(url,tgt)
{
	if(!checkValues(document.homeForm))return false;
	document.homeForm.action=url;
	document.homeForm.target=tgt;
	document.homeForm.submit();
}

function checkChecked(){
	f   = document.homeForm;
	chk = false;
	for(var i=0; i < f.length; i++){
		if(f[i].type == 'radio' && f[i].name.indexOf('Sta')!=-1){
			if(f[f[i].name][1].checked){
				chk = true;
				break;
			}
		}
	}
	return chk;
}

function delRegisters(url)
{
	if(!checkChecked(document.homeForm)){
		alert('N�o h� chamadas exclu�das!     \nPara verificar se h� chamadas exclu�das, clique em "Exibir Chamadas" e assinale a op��o "Removidos"     ');
		return false;
	}
	else{
		if(confirm('Confirma a dele��o das chamadas assinaladas? (Esta opera��o n�o poder� ser desfeita.)     ')){
			document.homeForm.action='/admin/delhpregisters.php';
			document.homeForm.target='';
			document.homeForm.submit();
		}
		else return false;
	}
}
</script>
</head>
<body onload="init()">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><span class="title">USU�RIO:&nbsp;</span><? echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="4"></td></tr>
<tr><td class="textblk" colspan="3"><span class="title">HOME PAGE</span></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="1"></td></tr>
</table>
<br>

<form name="homeForm" action="" method="post">

<!-- COLUNAS -->
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tbody id="tb1">
<tr class="tarjaTitulo">
	<td height="20" colspan="9" align="center">COLUNAS</td>
</tr>
<tr><td colspan="9" class="title"><a href="#" onclick="viewControl('vpp');return false" onfocus="noFocus(this)"><img src="/images/layout/bt_all.gif" width="22" height="26" border="0" align="absmiddle"><span class="title">&nbsp;Exibir&nbsp;Chamadas</span></a><a href="#" onclick="getURL('homepage',0,'',1);return false" onfocus="noFocus(this)" style="margin-left:110px"><img src="/images/layout/bt_novo.gif" width="20" height="20" border="0" vspace="10" align="absmiddle"><span class="title">&nbsp;Incluir Chamada</span></a></td></tr>
<tr>
	<!-- <td width="1%"></td>
	<td width="1%"></td>
	<td width="1%"></td>
	<td width="1%"></td> -->
	
	<td width="1%"></td>
	
	<td width="1%"></td>
	<td width="85%"><img src="/images/layout/blank.gif" width="197" height="1"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="1%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="1%"></td>
	<td width="1%"></td>
	<td width="1%"></td>
</tr>
<tr class="tarjaItens">
	<td align="center" class="title" style="padding-left:5px">ORDEM</td>
	<td><img src="/images/layout/blank.gif" width="10" height="25"></td>
	<td align="center" class="title" rowspan="2">T�TULO / CHAMADA</td>
	<td></td>
	<td align="center" class="title" rowspan="2">DATA</td>
	<td></td>
	<td colspan="3" align="center" class="title">STATUS</td>
</tr>
<tr class="tarjaItens">
	<!-- <td align="center" style="padding-left:5px"><img src="/images/layout/bt_col1.gif" width="25" height="32" alt="   coluna 1   "></td>
	<td align="center"><img src="/images/layout/bt_col1.gif" width="25" height="32" alt="   coluna 2   "></td>
	<td align="center"><img src="/images/layout/bt_col1.gif" width="25" height="32" alt="   coluna 3   "></td>
	<td align="center"><img src="/images/layout/bt_col2.gif" width="26" height="32" alt="   coluna 4   "></td> -->
	

	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td align="center"><img src="/images/layout/bt_publicar.gif" width="32" height="32" hspace="2" alt="   publicar   "></td>
	<td align="center"><img src="/images/layout/bt_remover.gif" width="32" height="32" hspace="2" alt="   excluir provisoriamente  "></td>
	<td align="center"><img src="/images/layout/bt_editar.gif" width="32" height="32" hspace="2" alt="   editar   "></td>
</tr>
<tr class="tarjaItens"><td colspan="9"><img src="/images/layout/blank.gif" width="1" height="7"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>

<tr id="tb1FR"><td><img src="/images/layout/blank.gif" width="1" height="1"></td></tr>
<?
function escrevePrincipal($oRs,$oRsLtb,$nRows,$rowId,$bgcolor,$iCont)
{
?>
<tr bgcolor="<? echo $bgcolor; ?>" id="<? echo $rowId; ?>" style="padding-top:5px;padding-bottom:5px">
	<!-- <td align="center" id="<? echo ($rowId . "Col0"); ?>" style="padding-left:5px"><input type="radio" name="Col<? echo $oRs[5]; ?>" value="0" <? if($oRs[3]==0) echo "checked defaultChecked"; ?> onmousedown="if(!this.checked)checkColuna('tb1',0)" onclick="getElementById('<? echo ($rowId . "Content"); ?>').style.paddingLeft=0" onfocus="noFocus(this)"></td>
	<td align="center" id="<? echo ($rowId . "Col1"); ?>"><input type="radio" name="Col<? echo $oRs[5]; ?>" value="1" <? if($oRs[3]==1) echo "checked defaultChecked"; ?> onmousedown="if(!this.checked)checkColuna('tb1',1)" onclick="getElementById('<? echo ($rowId . "Content"); ?>').style.paddingLeft=0" onfocus="noFocus(this)"></td>
	<td align="center" id="<? echo ($rowId . "Col2"); ?>"><input type="radio" name="Col<? echo $oRs[5]; ?>" value="2" <? if($oRs[3]==2) echo "checked defaultChecked"; ?> onmousedown="if(!this.checked)checkColuna('tb1',2)" onclick="getElementById('<? echo ($rowId . "Content"); ?>').style.paddingLeft=0" onfocus="noFocus(this)"></td>
	<td align="center" id="<? echo ($rowId . "Col3"); ?>"><input type="radio" name="Col<? echo $oRs[5]; ?>" value="3" <? if($oRs[3]==3) echo "checked defaultChecked"; ?> onmousedown="if(!this.checked)checkColuna('tb1',3)" onclick="getElementById('<? echo ($rowId . "Content"); ?>').style.paddingLeft=0" onfocus="noFocus(this)"></td> -->

<td align="center"><input type="text" class="textbox5" name="Col<? echo $oRs[5]; ?>" value="<? echo $oRs[3]; ?>"></td>

<td><img src="/images/layout/blank.gif" width="10" height="1"></td>
<?
echo "<td id=\"" . ($rowId . "Content") . "\" class=\"textblk\" valign=\"top\"><span class=\"textbld\">" . $oRs[0] . "</span><br>" . $oRs[2] . "</td>";
?>
	<td><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td class="dataarquivos"><? echo FmtDataTempo($oRs[1]); ?></td>
	<td><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td align="center" id="<? echo ($rowId . "PUB"); ?>"><input type="radio" name="Sta<? echo $oRs[5]; ?>" value="1" <?if($oRs[4]==1 || $oRs[4]==5) echo "checked"; ?> onfocus="noFocus(this)" <?if($oRs[4]==5) echo "style=\"background-image:url(/images/layout/bg_radio.gif)\""; ?> onclick="<? if($oRs[4]==5) echo "document.homeForm.Sta" . $oRs[5] . "[0].style.backgroundImage='url(/images/layout/bg_radio.gif)'"; ?>"></td>
	<td align="center"><input type="radio" name="Sta<? echo $oRs[5]; ?>" value="3" <?if($oRs[4]==3) echo "checked"; ?> onfocus="noFocus(this)" onclick="document.homeForm.Sta<? echo $oRs[5]; ?>[0].style.backgroundImage=''"></td>
	<td align="center"><a href="#" onclick="getURL('homepage',0,<? echo $oRs[5]; ?>,2);return false" onfocus="noFocus(this)"><img src="/images/layout/<?
if($oRs[7]==1) echo "bt_edit.gif";
if($oRs[7]==0 || $oRs[7]==2) echo "bt_link.gif";
if($oRs[7]==3) echo "bt_nolink.gif"; ?>" width="20" height="20" border="0"></a></td>
</tr>
<?
}

$sql = "SELECT TITULO, DATA, CHAMADA, COLUNA, STATUS, ID, TIPO, TIPO_LINK FROM tb_homepage WHERE " . getSTATUScheck($vpp,"") . " ORDER BY STATUS, COLUNA, DATA DESC";
$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

$nRows = mysql_num_rows($RS_query);

$iCount = 1;
$bgcolor = "#ffffff";
$rowId = "";
while($oRs = mysql_fetch_row($RS_query))
{
	$rowId = "tb1r" . ($iCount + 1);
	escrevePrincipal($oRs,$oRs,$nRows,$rowId,$bgcolor,$iCount);
	$iCount++;
	if($iCount % 2 == 0) $bgcolor="#ffffff"; else $bgcolor="#f1f1f1";
}
echo "<input type=\"hidden\" name=\"NHP\" value=\"" . $iCount . "\">";
mysql_free_result($RS_query);
?>
<tr id="tb1LR"><td><img src="/images/layout/blank.gif" width="1" height="1"></td></tr>
<tr class="tarjaTitulo"><td colspan="9"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="1" height="40"></td></tr>
<tr><td align="center" style="font-family:tahoma,arial,helvetica,sans-serif;font-size:11px;white-space:nowrap"><input type="button" class="buttonsty" value="Submeter dados" onclick="submitPage('/admin/updatehpstatus.php','')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttondelsty" value="" onclick="delRegisters()" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'">Excluir definitivamente<img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"><!-- <img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Preview" onclick="submitPage('/admin/htmleditor/previewhomepage.php','previewWin')" onfocus="noFocus(this)"> --><br><br><br></td></tr></table>

</body>
</html>

<?
mysql_close();
?>