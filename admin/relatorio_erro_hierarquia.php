<?php
//Includes
include('page.php');
include("../include/defines.php");
include('framework/crud.php');
include('relatorio_util.php');

$codigoImportacao = $_REQUEST["id"];
$importacao = new col_hierarquia_importacao();
$resultado = $importacao->obterRelatorioErrosImportacao($codigoImportacao);

$linha = mysql_fetch_array($resultado);
$nomeEmpresa = $linha["DS_EMPRESA"];
$dataImportacao = $linha["DT_IMPORTACAO"];

mysql_data_seek($resultado, 0);

?>
<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	
	
	<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">		
		
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
	</style>
	
</head>

<body>

<br>

<table cellpadding="1" cellspacing="0" align="center" width="95%">

	<tr style="height: 30px; width: 20%;" >
		<td class='textblk'>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td class='textblk' style="width: 60%; text-align:center;">
				<b>Relat�rio Erros de Importa��o</b>
		</td>
		<td style="text-align: right; width: 20%;" class='textblk'>
			<p><?php echo date("d/m/Y") ?></p>
		</td>
	</tr>
	<tr>
		<td class="textblk" colspan="3">
			Data de Importa��o: <?php echo date("d/m/Y", $dataImportacao);?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="1">

	<tr class="textblk">
		<td>Identificador da Unidade</td>
		<td>Nome da Unidade</td>
		<td>Identificador da Unidade Pai</td>
		<td>Importado</td>
		<td>Erro</td>
	</tr>
	
<?php 
while($linha = mysql_fetch_array($resultado)) {
	
	echo "<tr class=\"textblk\">";
	
	$importado = ($linha["IN_IMPORTADO"]==1?"Sim":"N�o");
	
	echo "	<td>{$linha["CD_LOTACAO"]}</td>
			<td>{$linha["NM_LOTACAO"]}</td>
			<td>{$linha["CD_LOTACAO_PAI"]}</td>
			<td>$importado</td>
			<td>{$linha["DS_ERRO"]}</td>";
	
	echo "</tr>";
}

?>
	
	
</table>


