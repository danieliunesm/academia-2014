<?php

include('filtrocomponente.php');
include("../include/defines.php");
include('framework/crud.php');
include('controles.php');
include('datagrid.php');
include('farol_util.php');

function instanciarRN(){
	return new col_disciplina();
}

$paginaEdicao = "relatorio_farol.php";
$paginaEdicaoAltura = 600;
$paginaEdicaoLargura = 900;

$obterParametros = "'cboEmpresa=' + document.getElementById('cboEmpresa').value + '&nomeEmpresa=' + document.getElementById('cboEmpresa').options[document.getElementById('cboEmpresa').selectedIndex].text + 
					'&cboLotacao=' + document.getElementById('cboLotacao').value + '&nomeLotacao=' + escape(document.getElementById('cboLotacao').options[document.getElementById('cboLotacao').selectedIndex].text) + 
					'&cboCargo=' + escape(document.getElementById('cboCargo').value) +
					'&cboUsuario=' + document.getElementById('cboUsuario').value + '&nomeUsuario=' + document.getElementById('cboUsuario').options[document.getElementById('cboUsuario').selectedIndex].text + 
					'&cboProva=' + document.getElementById('cboProva').value + '&nomeProva=' + escape(document.getElementById('cboProva').options[document.getElementById('cboProva').selectedIndex].text) +
					'&txtDe=' + document.getElementById('txtDe').value + '&txtAte=' + document.getElementById('txtAte').value +
					'&rdoTipoRelatorio=' + (document.getElementById('rdoTipoRelatorio').checked?1:0)" ;

define(TITULO_PAGINA, "Radar");

include('relatorio_cabecalho.php');

exibirParametros();

include('relatorio_rodape.php');


function exibirParametros(){
	
	?>
	
	<table cellpadding="0" cellspacing="0" border="0" align="center">
		
		<?php exibirParametrosFarolConsulta(); ?>
		
	</table>
	
	<?php
	
}


?>