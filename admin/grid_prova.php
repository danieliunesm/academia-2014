<?php
include("../include/defines.php");
include('framework/crud.php');
include('controles.php');
include('datagrid.php');

$paginaEdicao = "cadastro_prova.php";
$paginaEdicaoAltura = 600;
$paginaEdicaoLargura = 555;

function instanciarRN(){
	return new col_prova();
}


define(TITULO_PAGINA, "Avalia��es/Certifica��es");
define(BOTAO_ADICIONAR, "Incluir Avalia��o/Certifica��o");
define(BOTAO_MODO_EXIBICAO, "Visualizar Avalia��es/Certifica��es");

include('grid_cabecalho.php');

?>

<table cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td class="textblk">
			Programa:
		</td>
	</tr>
	<tr>
		<td>
			<?php
			comboboxEmpresa("cboEmpresa", $_POST["cboEmpresa"]);
			?>		
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td class="textblk">
			Tipo de Avalia��o:
		</td>
	</tr>
	<tr>
		<td class="textblk">
			<?php 
				$tipoSelecionado = 2;
				
				if (count($_POST) > 0)
				{
					$tipoSelecionado = $_POST["rdoTipoAvaliacao"];
				}
				
				$tipoTodos = ($tipoSelecionado=="2") ? "checked=\"true\"" : "";
				$tipoAprendizado = ($tipoSelecionado=="1") ? "checked=\"true\"" : "";
				$tipoConhecimento = ($tipoSelecionado=="0") ? "checked=\"true\"" : "";			
			
			?>
		
		
			<input type="radio" value="2" name="rdoTipoAvaliacao" <?php echo $tipoTodos; ?>>Todos</input>
			<input type="radio" value="1" name="rdoTipoAvaliacao" <?php echo $tipoAprendizado; ?>>Avalia��es</input>
			<input type="radio" value="0" name="rdoTipoAvaliacao" <?php echo $tipoConhecimento; ?>>Simulados</input>	
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	
	<tr>
		<td align="center">
			<input type='submit' name='btnPesquisa' class='buttonsty' value='Filtrar'>
		</td>
	</tr>

    <tr>
        <td align="center">
            <a href="javascript:selecionar(true);">Marcar todos</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="javascript:selecionar(false);">Desmarcar todos</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="javascript:definirDatas();">Definir datas</a>
            <div id="divDatas" style="display: none;">
                <fieldset title="Alterar Datas" >
                    Defina as datas:<br /><br />
                    1� chamada -
                    in�cio: <input type="text" maxlength="10" name="txtDataInicio" id="txtDataInicio" onKeyPress="DinamicMasks(this,'##/##/####','N');" onBlur="ValidarData(this);" />&nbsp;&nbsp;&nbsp;
                    t�rmino: <input type="text" maxlength="10" name="txtDataTermino" id="txtDataTermino" onKeyPress="DinamicMasks(this,'##/##/####','N');" onBlur="ValidarData(this);" />
                    <br /><br/>
                    2� chamada -
                    in�cio: <input type="text" maxlength="10" name="txtDataInicio2" id="txtDataInicio2" onKeyPress="DinamicMasks(this,'##/##/####','N');" onBlur="ValidarData(this);" />&nbsp;&nbsp;&nbsp;
                    t�rmino: <input type="text" maxlength="10" name="txtDataTermino2" id="txtDataTermino2" onKeyPress="DinamicMasks(this,'##/##/####','N');" onBlur="ValidarData(this);" />
                    <br /><br />

                    <input type="button" onclick="gravar()" value="Gravar" class='buttonsty'/>
                </fieldset>
            </div>
        </td>

    </tr>

</table>

    <iframe id="frmDataProva" name="frmDataProva" style="display: none" > </iframe>

    <script type="text/javascript">

        function selecionar(marcar){

            var itens = document.getElementsByTagName("input");
            var t = itens.length;

            for(var i=0;i<t;i++){

                if (itens[i].type == "checkbox"){

                    itens[i].checked = marcar;

                }

            }
        }

        function definirDatas(){

            document.getElementById("divDatas").style.display = "block";

        }

        function gravar(){

            if (document.getElementById("txtDataInicio").value == ""){
                alert("O campo Data de In�cio � de preenchimento obrigat�rio.");
                return;
            }

            if (document.getElementById("txtDataTermino").value == ""){
                alert("O campo Data de T�rmino � de preenchimento obrigat�rio.");
                return;
            }

            if (document.getElementById("txtDataInicio2").value != "" || document.getElementById("txtDataTermino2").value != ""){

                if (document.getElementById("txtDataInicio2").value == ""){
                    alert("O campo Data de In�cio da 2� chamada � de preenchimento obrigat�rio.");
                    return;
                }

                if (document.getElementById("txtDataTermino2").value == ""){
                    alert("O campo Data de T�rmino da 2� chamada � de preenchimento obrigat�rio.");
                    return;
                }
            }

            var actionOld =  document.forms[0].action;
            var targetOld = document.forms[0].target;

            document.forms[0].action = "gravarmudancadataprova.php";
            document.forms[0].target = "frmDataProva";
            document.forms[0].submit();

            document.forms[0].target = targetOld;
            document.forms[0].action = actionOld;

        }


    </script>


<?php 
$rn = new col_prova();
$resultado = $rn->listarPorPrograma($_POST["cboEmpresa"],$_POST["rdoTipoAvaliacao"]);

dataGridColaborae(array("Avalia��o"), array("DS_PROVA"), array("100%"), $resultado, "CD_PROVA", false, true, true);

include('grid_rodape.php');

?>