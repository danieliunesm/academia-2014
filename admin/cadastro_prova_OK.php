<?php

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_prova();
}

function carregarRN(){
	
	page::$rn->CD_PROVA = $_REQUEST["id"];
	page::$rn->DS_PROVA = $_POST["txtProva"];
	page::$rn->DT_REALIZACAO = $_POST["txtDataRealizacao"];
	page::$rn->DT_FIM_REALIZACAO = $_POST["txtDataRealizacaoFim"];
	page::$rn->DURACAO_PROVA = $_POST["txtDuracao"];
	page::$rn->HORARIO_FIM_DISPONIVEL = "{$_POST['txtDataRealizacao']} {$_POST['txtHorarioTermino']}";
	page::$rn->HORARIO_INICIO_DISPONIVEL = "{$_POST['txtDataRealizacao']} {$_POST['txtHorarioInicio']}";
	page::$rn->IN_DIFICULDADE = $_POST["cboDificuldade"];
	page::$rn->CD_DISCIPLINA = $_POST["cboDisciplina"];
	page::$rn->NR_QTD_PERGUNTAS_PAGINA = $_POST["txtPaginaPergunta"];
	page::$rn->NR_QTD_PERGUNTAS_TOTAL = $_POST["txtTotalPergunta"];
	page::$rn->VL_MEDIA_APROVACAO = $_POST["txtMedia"];
    page::$rn->IN_PROVA = $_POST["rdoSimulado"];   
    
	
	$codigoUsuarios = $_POST["listUsuario"];
	
	$codigoUsuarios = split(";", $codigoUsuarios);
	
	$codigoUsuariosProva = array();
	
	if (is_array($codigoUsuarios)){
		
		for ($i = 0; $i < count($codigoUsuarios); $i++){
			
			if (strlen($codigoUsuarios[$i]) > 0){
				if (substr($codigoUsuarios[$i],0,1) == "L"){
					$lotacao = substr($codigoUsuarios[$i],1,strlen($codigoUsuarios[$i]));
					
					$sql = "SELECT CD_USUARIO FROM col_usuario WHERE lotacao = '$lotacao'";
					$lotacao_usuarios = DaoEngine::getInstance()->executeQuery($sql);
					
					while ($linha_lotacao = mysql_fetch_array($lotacao_usuarios)){
						//$codigoUsuariosLotacao[] = $linha_lotacao["CD_USUARIO"];
						$usuario = new col_prova_usuario();
						$usuario->CD_USUARIO = $linha_lotacao["CD_USUARIO"];
						$codigoUsuariosProva[] = $usuario;
					}
	
				}else{
					$usuario = new col_prova_usuario();
					$usuario->CD_USUARIO = $codigoUsuarios[$i];
					$codigoUsuariosProva[] = $usuario;
				}
			}

		}
		
	}
	
	
	if (count($codigoUsuariosProva) == 0){
		$codigoUsuariosProva[] = new col_prova_usuario();
	}
	
	page::$rn->usuarios = $codigoUsuariosProva;
	
}

function pageRenderEspecifico(){
	
?>				<TR>
				    <TD>
				        <table cellpadding="0" cellspacing="0" border="0">
					        <TR>
								<TD class="textblk" colspan="3">Disciplina:</TD>
							</TR>
							<TR>
								<TD colspan="3">
									<?php comboboxDisciplina("cboDisciplina",  page::$rn->CD_DISCIPLINA, "Disciplina"); ?>
								</TD>
							</TR>
				        	<TR><TD colspan="3">&nbsp;</TD></TR>
				            <TR>
        					    <TD class="textblk" colspan="3">Prova:</TD>
		        		    </TR>
				            <TR>
					            <TD colspan="3">
						            <?php textbox("txtProva", "50", page::$rn->DS_PROVA, "100%", "textbox3", "Disciplina") ?>
					            </TD>
	        			    </TR>
    		    		    <TR><TD colspan="3">&nbsp;</TD></TR>
				            <TR><TD colspan="3"></TD></TR>
					            <TD class=textblk>Data de In�cio</TD>
					            <TD class=textblk>Data de T�rmino</TD>
					            <TD class=textblk>Hora de In�cio</TD>
        				    </TR>
		        		    <TR>
					            <TD><?php textbox("txtDataRealizacao", "10", page::$rn->DT_REALIZACAO, "110px", "textbox3", "In�cio Realiza��o", Mascaras::Data) ?></TD>
					            <TD><?php textbox("txtDataRealizacaoFim", "10", page::$rn->DT_FIM_REALIZACAO, "110px", "textbox3", "T�rmino Realiza��o", Mascaras::Data) ?></TD>
					            <TD><?php textbox("txtHorarioInicio", "5", page::$rn->HORARIO_INICIO_DISPONIVEL, "110px", "textbox3", "Hor�rio de In�cio", Mascaras::Hora) ?></TD>
        				    </TR>
        				    <TR><TD colspan="3">&nbsp;</TD></TR>
        				    <TR>
        				    	<TD class=textblk>Hora de T�rmino</TD>
					            <TD class=textblk>Dura��o Prova (min)</TD>
					            <TD class=textblk>Total de perguntas</TD>
        				    </TR>
		        		    <TR>
		        		    	<TD><?php textbox("txtHorarioTermino", "5", page::$rn->HORARIO_FIM_DISPONIVEL, "110px", "textbox3", "Hor�rio de T�rmino", Mascaras::Hora) ?></TD>
					            <TD><?php textbox("txtDuracao", "3", page::$rn->DURACAO_PROVA, "110px", "textbox3", "Dura��o", Mascaras::Numero) ?></TD>
					            <TD><?php textbox("txtTotalPergunta", "50", page::$rn->NR_QTD_PERGUNTAS_TOTAL, "110px", "textbox3", "Total de Perguntas", Mascaras::Numero) ?></TD>
        				    </TR>
        				    <TR><TD colspan="3">&nbsp;</TD></TR>
        				    <TR>
        				    	<TD class=textblk>Perguntas por p�gina</TD>
					            <TD class=textblk>M�dia para Aprova��o</TD>
					            <TD class=textblk>Dificuldade</TD>
        				    </TR>
		        		    <TR>
		        		    	<TD><?php textbox("txtPaginaPergunta", "50", page::$rn->NR_QTD_PERGUNTAS_PAGINA, "110px", "textbox3", "Perguntas por P�gina", Mascaras::Numero) ?></TD>
					            <TD><?php textbox("txtMedia", "4", page::$rn->VL_MEDIA_APROVACAO, "110px", "textbox3", "M�dia para Aprova��o", Mascaras::Numero) ?></TD>
					            <TD><?php comboboxPeso("cboDificuldade",page::$rn->IN_DIFICULDADE,"Dificuldade"); //<?php textbox("txtDificuldade", "3", page::$rn->IN_DIFICULDADE, "110px", "textbox3", "Dificuldade", Mascaras::Numero) ?></TD>
        				    </TR>
        				    <TR><TD colspan="3">&nbsp;</TD></TR>
        				    <TR>
        				    	<TD colspan="3" class=textblk>
	                                <?php
	                                    $simuladoSim = "";
	                                    $simuladoNao = "";
	                                    
	                                    if (page::$rn->IN_PROVA == 1){
	                                         $simuladoNao = "checked";
	                                    }else{
	                                         $simuladoSim = "checked";
	                                    }
	                                    
	                                ?>
	                                    <INPUT name="rdoSimulado" type="radio" value="0" <?php echo $simuladoSim ?>/>Simulado <INPUT name="rdoSimulado" type="radio" value="1" <?php echo $simuladoNao ?>/>Prova
        				    	</TD>
        				    </TR>
        				    <TR><TD colspan="3">&nbsp;</TR>
        				    <TR>
        				    	<TD colspan="3">
        				    		<?php $usuario = new col_usuario();
        				    		 dualListBox("listUsuario",10, $usuario->listarUsuarioLotacao(),page::$rn->usuarios, "CD_USUARIO", "NM_USUARIO", "Usu�rios n�o selecionados", "Usu�rios Selecionados"); ?>        				    	
        				    	</TD>
        				    </TR>
        				    

        				    
                        </table>
				    </TD> 
				</TR>

<?php

}


?>