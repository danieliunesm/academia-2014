<?php

//include('page.php');
include("../include/defines.php");
include('framework/crud.php');
include('relatorio_util.php');

$pesoParticipanteAvaliacao = ZeraVazio("txtPesoParticipanteAvaliacao");
$PesoMediaAvaliacao = ZeraVazio("txtPesoMediaAvaliacao");
$PesoParticipanteSimulado = ZeraVazio("txtPesoParticipanteSimulado");
$PesoMediaSimulado = ZeraVazio("txtPesoMediaSimulado");
$PesoContribuicaoForum = ZeraVazio("txtPesoContribuicaoForum");
$PesoVelocidadeProva = ZeraVazio("txtPesoVelocidadeProva");
$PesoQuantidadeDownload = ZeraVazio("txtPesoQuantidadeDownload");
$PesoQuantidadeHits = ZeraVazio("txtPesoQuantidadeHits");
$PesoQuantidadeSessao = ZeraVazio("txtPesoQuantidadeSessao");
$PesoQuantidadeUsuario = ZeraVazio("txtPesoQuantidadeUsuario");

$codigoEmpresa = $_POST['cboEmpresa'];
$codigoUsuario = joinArray($_POST['cboUsuario']);
$codigoProva = joinArray($_POST['cboProva']);
$cargo = joinArray($_POST['cboCargo'],"','");
$cargoCorrigido = corrigePlic($cargo);
$codigoLotacao = joinArray($_POST['cboLotacao']);
$codigoForum = joinArray($_POST['cboForum']);

$dataInicio = $_POST["txtDe"];
$dataTermino = $_POST["txtAte"];

if ($dataInicio == "" && $dataTermino == "")
{
	$dataTermino = date("d/m/Y");
}

$dataInicioTexto = $dataInicio;
$dataTerminoTexto = $dataTermino;

$dataTerminoAnterior = "";
$dataTerminoAnteriorTexto = "";

$sqlDataProva = "";
$sqlDataForum = "";
$sqlDataAcesso = "";
$sqlDataAnteriorProva = "";
$sqlDataAnteriorForum = "";
$sqlDataAnteriorAcesso = "";

if ($dataInicio != ""){
	$dataInicio = substr($dataInicio, 6, 4) . substr($dataInicio, 3, 2) . substr($dataInicio, 0, 2);
	
	$sqlDataProva = " $sqlData AND  DATE_FORMAT(DT_INICIO, '%Y%m%d') >= '$dataInicio' ";
	$sqlDataForum = " $sqlData AND  DATE_FORMAT(data, '%Y%m%d') >= '$dataInicio' ";
	$sqlDataAcesso = " $sqlData AND  DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicio' ";
}

if ($dataTermino != ""){
	$ano = substr($dataTermino, 6, 4);
	$mes = substr($dataTermino, 3, 2);
	$dia = substr($dataTermino, 0, 2);
	$dataTermino = $ano . $mes . $dia;
	
	$dataTerminoAnteriorTexto = strftime("%d/%m/%Y" , mktime(0, 0, 0, $mes, $dia - 1, $ano));
	$dataTerminoAnterior = substr($dataTerminoAnteriorTexto, 6, 4) . substr($dataTerminoAnteriorTexto, 3, 2) . substr($dataTerminoAnteriorTexto, 0, 2);
		
	$sqlDataProva = " $sqlData AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= '$dataTermino' ";
	$sqlDataForum = " $sqlData AND DATE_FORMAT(data, '%Y%m%d') <= '$dataTermino' ";
	$sqlDataAcesso = " $sqlData AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataTermino' ";
	
	$sqlDataAnteriorProva = " $sqlData AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= '$dataTerminoAnterior' ";
	$sqlDataAnteriorForum = " $sqlData AND DATE_FORMAT(data, '%Y%m%d') <= '$dataTerminoAnterior' ";
	$sqlDataAnteriorAcesso = " $sqlData AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataTerminoAnterior' ";
}

$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];

//Queryes
$sqlMediaProva = "
SELECT
  ROUND(AVG(pa.VL_MEDIA),2) AS TOTAL
FROM
 	col_prova_aplicada pa
 	INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA
 	INNER JOIN col_usuario u ON pa.CD_USUARIO = u.CD_USUARIO
 	INNER JOIN col_empresa e ON u.empresa = e.CD_EMPRESA
	INNER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
WHERE
  	pa.VL_MEDIA IS NOT NULL
AND	p.IN_PROVA = 1
AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
$sqlDataProva";


$sqlTotalSimulado = "
SELECT
  COUNT(1) AS TOTAL
FROM
 	col_prova_aplicada pa
 	INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA
 	INNER JOIN col_usuario u ON pa.CD_USUARIO = u.CD_USUARIO
 	INNER JOIN col_empresa e ON u.empresa = e.CD_EMPRESA
	INNER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
WHERE
  	pa.VL_MEDIA IS NOT NULL
AND	p.IN_PROVA = 0
AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
$sqlDataProva";

$sqlMediaSimulado = "
SELECT
  ROUND(AVG(pa.VL_MEDIA),2) AS TOTAL
FROM
 	col_prova_aplicada pa
 	INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA
 	INNER JOIN col_usuario u ON pa.CD_USUARIO = u.CD_USUARIO
 	INNER JOIN col_empresa e ON u.empresa = e.CD_EMPRESA
	INNER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
WHERE
  	pa.VL_MEDIA IS NOT NULL
AND	p.IN_PROVA = 0
AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
$sqlDataProva";

$sqlTempoProva = "
SELECT
  ROUND(AVG(p.DURACAO_PROVA * 60 / TIME_TO_SEC(TIMEDIFF(pa.DT_TERMINO, pa.DT_INICIO))),2) AS TOTAL
FROM
  col_prova_aplicada pa
  INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA
  INNER JOIN col_usuario u ON pa.CD_USUARIO = u.CD_USUARIO
  INNER JOIN col_empresa e ON u.empresa = e.CD_EMPRESA
  INNER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
WHERE
  	pa.VL_MEDIA IS NOT NULL
AND	p.IN_PROVA = 0
AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
$sqlDataProva";

$sqlTotalForum = "
SELECT
	COUNT(tf.id) AS TOTAL
FROM
	col_foruns f
	LEFT OUTER JOIN tb_forum tf ON tf.CD_FORUM = f.CD_FORUM
	LEFT OUTER JOIN col_usuario u ON u.login = tf.name
	LEFT OUTER JOIN col_empresa e ON u.empresa = e.CD_EMPRESA
	LEFT OUTER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
WHERE 1=1 
AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
AND	(f.CD_FORUM in ($codigoForum) OR '$codigoForum' = '-1')
$sqlDataForum
";


$sqlTotalDownload = "
SELECT
	count(1) AS TOTAL
FROM
	col_pagina_acesso pa
	LEFT JOIN col_acesso a ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
	LEFT JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
	LEFT JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
	LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
WHERE
	NOT pa.IN_ORDENAR = -1
AND	DS_PAGINA_ACESSO LIKE 'Download %'
AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
$sqlDataAcesso
";

$sqlTotalHits = "
SELECT
	count(1) as TOTAL
FROM
	col_pagina_acesso pa
	LEFT JOIN col_acesso a ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
	LEFT JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
	LEFT JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
	LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
WHERE
	NOT pa.IN_ORDENAR = -1
AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
$sqlDataAcesso
";

$sqlTotalSessao = "
SELECT
	COUNT(DISTINCT(DS_ID_SESSAO)) AS TOTAL
FROM
	col_pagina_acesso pa
	LEFT JOIN col_acesso a ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
	LEFT JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
	LEFT JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
	LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
WHERE
	NOT pa.IN_ORDENAR = -1
AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
$sqlDataAcesso
";

$sqlTotalUsuario = "
SELECT
	COUNT(1) AS TOTAL
FROM
(
	SELECT
		DISTINCT a.CD_USUARIO, DATE_FORMAT(a.DT_ACESSO, '%Y%m%d')
	FROM
		col_pagina_acesso pa
		LEFT JOIN col_acesso a ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
		LEFT JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
		LEFT JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
		LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
	WHERE
		NOT pa.IN_ORDENAR = -1
	AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
	AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
	$sqlDataAcesso
) AS TABELA
";

$resultadoDownload = DaoEngine::getInstance()->executeQuery($sqlTotalDownload,true);
$resultadoForum = DaoEngine::getInstance()->executeQuery($sqlTotalForum,true);
$resultadoHits = DaoEngine::getInstance()->executeQuery($sqlTotalHits,true);

$resultadoSessao = DaoEngine::getInstance()->executeQuery($sqlTotalSessao,true);
$resultadoTotalSimulado = DaoEngine::getInstance()->executeQuery($sqlTotalSimulado,true);
$resultadoUsuario = DaoEngine::getInstance()->executeQuery($sqlTotalUsuario,true);
$resultadoMediaProva = DaoEngine::getInstance()->executeQuery($sqlMediaProva,true);
$resultadoMediaSimulado = DaoEngine::getInstance()->executeQuery($sqlMediaSimulado,true);
$resultadoTempoProva = DaoEngine::getInstance()->executeQuery($sqlTempoProva,true);

$resultadoTotalProva = executarQueryTotalAvaliacao($sqlDataProva, 1);
$resultadoAnteriorProva = executarQueryTotalAvaliacao($sqlDataAnteriorProva, 0);



?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	
	<?php
	
	if ($_POST["excel"] != 1) {

	?>
	<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">		
	<?php
	}
	?>
	
	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
	</style>
	
</head>

<body>

<table cellpadding="0" cellspacing="0" border="0" width="95%" align="center">
	<tr style="height: 30px">
			<td class='textblk'>
				<p><b><?php echo $nomeEmpresa; ?></b></p>
			</td>
			<td align="center" class="textblk" colspan="2">
				<b>
					Relat�rio Farol
				</b>
			</td>
			<td style="text-align: right" class='textblk'>
				<p><?php echo date("d/m/Y") ?></p>
			</td>
	</tr>

	<tr align="center" class="textblk" style="font-weight: bold">
		<td class="titRelatEsq" align="left">
			Itens Analisados
		</td>
		<td class="titRelat" >
			At� <?php echo $dataTerminoAnteriorTexto; ?>
		</td>
		<td class="titRelat" >
			At� <?php echo $dataTerminoTexto; ?>
		</td>
		<td class="titRelat" >
			<?php echo $dataTerminoTexto; ?>
		</td>
	</tr>
	
	<?php
		$somatorioDataAnterior = 0;
	
		$itensRelatorio = array(
									array('Participantes nas Avalia��es', $pesoParticipanteAvaliacao, $resultadoTotalProva),
									array('M�dia nas Avalia��es', $PesoMediaAvaliacao, $resultadoMediaProva),
									array('Participantes nos Simulados', $PesoParticipanteSimulado, $resultadoTotalSimulado),
									array('M�dia nos Simulados', $PesoMediaSimulado, $resultadoMediaSimulado),
									array('Velocidade na Prova', $PesoVelocidadeProva, $resultadoTempoProva),
									array('Contribui��es no F�rum', $PesoContribuicaoForum, $resultadoForum),
									array('Quantidade de Download', $PesoQuantidadeDownload, $resultadoDownload),
									array('Quantidade de Hits', $PesoQuantidadeHits, $resultadoHits),
									array('Quantidade de Sess�es', $PesoQuantidadeSessao, $resultadoSessao),
									array('Quantidade de Acessos de Usu�rios', $PesoQuantidadeUsuario, $resultadoUsuario),
								);
	
		foreach($itensRelatorio as $item)
		{
		
			$titulo = $item[0];
			$peso = $item[1];
			$resultadoItemDataAnterior = $item[2];
			$linha = mysql_fetch_row($resultadoItemDataAnterior);
			$valorDataAnterior = $linha[0];
			
			$somatorioDataAnterior = $somatorioDataAnterior + ($peso * $valorDataAnterior);
			
			echo "<tr class=\"textblk\" align=\"center\">
					<td class=\"titRelatEsq\" align=\"left\">
						$titulo
					</td>
					<td class=\"titRelat\">$valorDataAnterior</td>
					<td class=\"titRelat\">3</td>
					<td class=\"titRelat\">4</td>
				  </tr>";
		}
		
		
		echo "<tr class=\"textblk\" align=\"center\">
				<td class=\"titRelatEsqRod\" align=\"left\">
					Pontua��o Farol
				</td>
				<td class=\"titRelatRod\">$somatorioDataAnterior</td>
				<td class=\"titRelatRod\">3</td>
				<td class=\"titRelatRod\">4</td>
		  	  </tr>";
	?>
	
	</table>
<?php

	function ZeraVazio($nomeCampo)
	{
		
		if (is_numeric($_POST[$nomeCampo]))
		{
			return $_POST[$nomeCampo];
		}
		else
		{
			return 0;
		}
		
	}

	
	function executarQueryTotalAvaliacao($sqlDataProva, $inProva, $inTotal = true)
	{
		global 	$codigoEmpresa, $codigoUsuario, $codigoProva, $cargo, $cargoCorrigido,
				$codigoLotacao, $codigoForum;
		
		$count = "";
		if ($inTotal)
		{
			$count = "COUNT(1) AS TOTAL";
		}
		else
		{
			$count = "ROUND(AVG(pa.VL_MEDIA),2) AS TOTAL";
		}
		
		$sqlTotalAvaliacao = "
			SELECT
			  $count
			FROM
			 	col_prova_aplicada pa
			 	INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA
			 	INNER JOIN col_usuario u ON pa.CD_USUARIO = u.CD_USUARIO
			 	INNER JOIN col_empresa e ON u.empresa = e.CD_EMPRESA
				INNER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
			WHERE
			  	pa.VL_MEDIA IS NOT NULL
			AND	p.IN_PROVA = $inProva
			AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
			AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
			AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
			AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
			AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
			$sqlDataProva";
		
		return DaoEngine::getInstance()->executeQuery($sqlTotalAvaliacao, true);
				
	}
	
?>