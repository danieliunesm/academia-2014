<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once '../Classes/PHPExcel.php';
if (!(isset($definesIncluido) && $definesIncluido)){
    include_once("../include/defines.php");
}

include('framework/crud.php');
include('controles.php');

$inputFileType = PHPExcel_IOFactory::identify('hierarquia.xlsx');

$objReader = PHPExcel_IOFactory::createReader($inputFileType);

$objReader->setReadDataOnly(true);

$objPHPExcel = $objReader->load('hierarquia.xlsx');

$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);

$highestRow = $objWorksheet->getHighestRow();

$codigoEmpresa = 5;

//Abre a conex�o
DaoEngine::getInstance()->obterConexao();

for ($row = 2; $row <= $highestRow;++$row)
{

    $codigo = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue()));
    $nome = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue()));

    if($codigo == "" || $nome == "")
        continue;

    //Verifica se a unidade existe. Se n�o, insere.
    //$query = "SELECT 1 FROM col_lotacao WHERE CD_EMPRESA = $codigoEmpresa AND CD_IDENTIFICADOR_LOTACAO = '$codigo'";
    //$resultado = DaoEngine::getInstance()->executeQuery($query, false);

    //if(mysql_num_rows($resultado) == 0){

        //$query = "INSERT INTO col_lotacao (DS_LOTACAO, CD_EMPRESA, CD_IDENTIFICADOR_LOTACAO) VALUES ('$nome', $codigoEmpresa, '$codigo')";

    $query = "INSERT INTO col_lotacao (DS_LOTACAO, CD_EMPRESA, CD_IDENTIFICADOR_LOTACAO)
              SELECT '$nome', $codigoEmpresa, '$codigo' FROM DUAL WHERE NOT EXISTS(SELECT 1 FROM col_lotacao WHERE CD_IDENTIFICADOR_LOTACAO = '$codigo')";

    DaoEngine::getInstance()->executeQuery($query, false);

        //echo "Inserida Lota��o: $nome <br />";

    //}

    //mysql_free_result($resultado);

}

//Obter todas as lotacoes
$query = "SELECT CD_LOTACAO, CD_IDENTIFICADOR_LOTACAO FROM col_lotacao WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($query, false);

$lotacoes = array();

while($linha = mysql_fetch_array($resultado))
{

    $lotacoes[$linha["CD_IDENTIFICADOR_LOTACAO"]] = $linha["CD_LOTACAO"];

}

mysql_free_result($resultado);

$query = "UPDATE col_lotacao SET IN_ATIVO = 0 WHERE CD_EMPRESA = $codigoEmpresa";
DaoEngine::getInstance()->executeQuery($query, false);
//Update em todas as lota��es
for ($row = 2; $row <= $highestRow;++$row)
{

    $codigo = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue()));
    $nome = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue()));
    $codigoPai = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue()));

    if($codigo == "" || $nome == "")
        continue;

    //$query = "SELECT CD_LOTACAO FROM col_lotacao WHERE CD_IDENTIFICADOR_LOTACAO = '$codigoPai' AND CD_EMPRESA = $codigoEmpresa";
    //$resultado = DaoEngine::getInstance()->executeQuery($query, false);

    //if($linha = mysql_fetch_row($resultado))
    //    $codigoPai = $linha[0];

    if(array_key_exists($codigoPai,$lotacoes) && $codigoPai != $codigo){
        $codigoPai = $lotacoes[$codigoPai];
    }else{
        $codigoPai = "NULL";
    }

    //mysql_free_result($resultado);

    $query = "UPDATE col_lotacao SET DS_LOTACAO = '$nome', CD_LOTACAO_PAI = $codigoPai, IN_ATIVO = 1
                WHERE CD_IDENTIFICADOR_LOTACAO = '$codigo' AND CD_EMPRESA = $codigoEmpresa";


    //echo $query;

    //break;

    DaoEngine::getInstance()->executeQuery($query, false);
}

$query = "SELECT
                CONCAT('UPDATE col_lotacao SET DS_LOTACAO_HIERARQUIA = ''',
                CONCAT(IFNULL(CONCAT(l5.CD_LOTACAO, '.'),''), IFNULL(CONCAT(l4.CD_LOTACAO, '.'),''), IFNULL(CONCAT(l3.CD_LOTACAO, '.'),''), IFNULL(CONCAT(l2.CD_LOTACAO, '.'),''), l1.CD_LOTACAO),
                ''' WHERE CD_LOTACAO = ', l1.CD_LOTACAO, ';')
            FROM
                col_lotacao l1
                LEFT JOIN col_lotacao l2 ON l1.CD_LOTACAO_PAI = l2.CD_LOTACAO
                LEFT JOIN col_lotacao l3 ON l2.CD_LOTACAO_PAI = l3.CD_LOTACAO
                LEFT JOIN col_lotacao l4 ON l3.CD_LOTACAO_PAI = l4.CD_LOTACAO
                LEFT JOIN col_lotacao l5 ON l4.CD_LOTACAO_PAI = l5.CD_LOTACAO
            WHERE
                l1.CD_EMPRESA = $codigoEmpresa
            AND l1.IN_ATIVO = 1";

$resultado = DaoEngine::getInstance()->executeQuery($query, false);

while($linha = mysql_fetch_row($resultado))
{

    DaoEngine::getInstance()->executeQuery($linha[0], false);

}

DaoEngine::getInstance()->fecharConexao();

?>