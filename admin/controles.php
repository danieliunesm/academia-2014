<?php

class Mascaras{

    const Data = 1;

    const Numero = 2;

    const Hora = 3;

}

function textbox($nome, $tamanho, $valor = "", $width = "100%", $cssClass = null, $obrigatorio = false, $mascara = ""){

    if ($obrigatorio){

        $obrigatorio = "TagMensagemObrigatorio='$obrigatorio'";

    }

    $aux = "";

    switch ($mascara){

        case Mascaras::Data:

            $aux = "onKeyPress=\"DinamicMasks(this,'##/##/####','N');\" onBlur=\"ValidarData(this);\"";

            $tamanho = "10";

            break;

        case Mascaras::Numero:

            $qtdNumeros = str_repeat("#",$tamanho);

            $aux = "onblur=\"MascaraDinamica(this,'$qtdNumeros');\" onKeyPress=\"MascaraDinamica(this,'$qtdNumeros');\"";

            break;

        case Mascaras::Hora:

            $aux = "onblur=\"MascaraDinamica(this,'##:##');\" onKeyPress=\"MascaraDinamica(this,'##:##');\"";

            $tamanho = "5";

            break;

    }

    echo "<INPUT type='text' id=\"$nome\" name='$nome' maxlength='$tamanho' value='$valor' style=\"width: $width\" class='$cssClass' $aux $obrigatorio/>";

}

function labelTextBox($label, $nome, $tamanho, $valor = "", $valorCheck = 2, $width = "100%", $cssClass = null, $obrigatorio = false, $mascara = ""){

    $checked = "";

    if ($valorCheck == 1)
        $checked = "checked ";

    echo "<p>

			<input type=\"checkbox\" id=\"chk$nome\" name=\"chk$nome\"  value=\"1\" $checked/>

			<label for='chk$nome' style='width: 180px;'>

			$label: </label>";

    textbox($nome, $tamanho, $valor, $width, $cssClass, $obrigatorio, $mascara);

    echo "</p>";

}

function textarea($nome, $tamanho, $valor = "", $width = "100%", $height = "", $cssClass = "textbox3", $obrigatorio = false){

    if ($obrigatorio){

        $obrigatorio = "TagMensagemObrigatorio = '$obrigatorio'";

    }

    echo "<TEXTAREA class=\"$cssClass\" style=\"height: $height\" id=\"$nome\" name=\"$nome\" style=\"width: $width\" $obrigatorio>$valor</TEXTAREA>";

}

function datagrid($colunas, $campos, $query = "", $objetoListar = null){


}

function comboboxCicloEmpresa($nome, $valor, $obrigatorio) {

    $sql = "SELECT CD_CICLO, CONCAT(DS_EMPRESA, ' - ', NM_CICLO) as NM_CICLO from col_empresa e INNER JOIN col_ciclo c ON c.CD_EMPRESA = e.CD_EMPRESA WHERE e.IN_ATIVO = 1 ORDER BY DS_EMPRESA, c.DT_INICIO, c.NM_CICLO";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "NM_CICLO", "CD_CICLO", true, "100%", "textbox3", $obrigatorio, false, false, false);

}

function comboboxCicloPorEmpresa($nome, $valor, $empresa){

    if ($empresa == "")
        $empresa = -1;

    $sql = "SELECT * FROM col_ciclo WHERE CD_EMPRESA = $empresa AND IN_ATIVO = 1 ORDER BY DT_INICIO";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    $itemInicial = array();
    $itemInicial["NM_CICLO"] = "Todos os Ciclos";
    $itemInicial["CD_CICLO"] = -1;

    combobox($nome, $valor, $resultado, "NM_CICLO", "CD_CICLO", true, "100%", "textbox3", false, false, false, false, "", $itemInicial);

}

function comboboxCiclo($nome, $valor, $empresa){

    $sql = "SELECT * FROM col_ciclo WHERE CD_EMPRESA = $empresa AND IN_ATIVO = 1 ORDER BY DT_INICIO";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    $primeiroItem = true;

    $campoValor = "CD_CICLO";

    $campoTexto = "NM_CICLO";

    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"textbox3\">";

    $ultimaData = "";

    $dataAtual = date("Ymd");

    $dataAtualFormatada = date("d/m/Y");

    //$dataAtual = "20100730";

    while ($linha = mysql_fetch_array($resultado)) {

        $ultimaData = formataDataBancodmY($linha["DT_TERMINO"]);

    }

    if (formataDataStringYmd($ultimaData) > $dataAtual){

        $ultimaData = $dataAtualFormatada;

    }

    mysql_data_seek($resultado, 0);

    while ($linha = mysql_fetch_array($resultado)) {

        $dataInicial = formataDataBancodmY($linha["DT_INICIO"]);

        $dataFinal = formataDataBancodmY($linha["DT_TERMINO"]);

        if (formataDataStringYmd($dataInicial) > $dataAtual){

            break;

        }

        if($primeiroItem){

            echo "<OPTION value='-1' dataInicial='$dataInicial' dataFinal='$ultimaData'>Todos os Ciclos - $dataInicial a $ultimaData</OPTION>";

            $primeiroItem = false;

        }

        echo "<OPTION value='$linha[$campoValor]' ";

        if ($dataAtual >= formataDataStringYmd($dataInicial) && $dataAtual <= formataDataStringYmd($dataFinal)) {

            echo "selected";

        }

        echo " dataInicial='$dataInicial' dataFinal='$dataFinal' >$linha[$campoTexto] - $dataInicial a $dataFinal</OPTION>";

    }

    echo "</SELECT>";

}


function comboboxTipoRelatorio($nome, $valor, $somenteAssessment=1){

    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"textbox3\">";

    echo "<OPTION value='relatorio_acesso_consolidado.php?hdnTipoRelatorio=D'>Evolu��o do Programa</OPTION>";

    echo "<OPTION value='relatorio_acesso.php?hdnTipoRelatorio=S'>Lista de Participantes</OPTION>";

    echo "<OPTION value='relatorio_nao_acesso.php?hdnTipoRelatorio=S'>Lista de N�o Participantes</OPTION>";

    //	echo "<OPTION value='relatorio_prova.php?tipo=0'>Participantes em Simulados</OPTION>";

    //	echo "<OPTION value='relatorio_prova.php?tipo=1'>Participantes em Avalia��es/Certifica��es</OPTION>";

    //	echo "<OPTION value='relatorio_nao_acesso_avaliacao.php?tipo=1'>N�o Participantes em Avalia��es/Certifica��es</OPTION>";

    //	echo "<OPTION value='relatorio_ranking_grupo.php?hdnTipoRelatorio=L'>Gest�o de Desempenho de Grupos Gerenciais</OPTION>";

    if ($somenteAssessment) {

        echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=U'>Desempenho de Participantes</OPTION>";

        echo "<OPTION value='relatorio_assessment_grupo.php?hdnTipoRelatorio=G&tipo=C'>Relat�rio Skill Assessment</OPTION>";

    }else{

        echo "<OPTION value='relatorio_pergunta.php?hdnTipoRelatorio=G&tipo=C'>Relat�rio de Perguntas</OPTION>";
    }

    //if ($_SESSION["empresaID"] != 37)

    //	echo "<OPTION value='relatorio_segunda_chamada.php?hdnTipoRelatorio=2'>Eleg�veis para Segunda Chamada</OPTION>";

    //	echo "<OPTION value='relatorio_segunda_chamada.php?hdnTipoRelatorio=B'>Avalia��es e Segundas Chamadas</OPTION>";

    if (!$somenteAssessment) {

        echo "<OPTION value='relatorio_ranking_avaliacao.php?hdnTipoRelatorio=U'>Desempenho de Participantes</OPTION>";

        //if ($_SESSION["empresaID"] != 37)
        //    echo "<OPTION value='relatorio_ranking_avaliacao.php?hdnTipoRelatorio=U&boletim=1'>Radar Boletim</OPTION>";

        echo "<OPTION value='relatorio_ranking_avaliacao2.php?hdnTipoRelatorio=U&boletim=1'>Radar Boletim</OPTION>";

        echo "<OPTION value='relatorio_assessment.php?hdnTipoRelatorio=G&tipo=C'>Plano de Aprendizado</OPTION>";

        echo "<OPTION value='relatorio_assessment.php?hdnTipoRelatorio=G'>Evolu��o de Aprendizado</OPTION>";

        //	echo "<OPTION value='relatorio_pesquisa.php?cboPesquisa=5'>Resultados de Pesquisa - Participantes</OPTION>";

        //	echo "<OPTION value='relatorio_boletim.php?hdnTipoRelatorio=G&tipo=C'>Radar Boletim</OPTION>";

    }

    //	echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=L'>Desempenho de Lota��es</OPTION>";

    //	echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=G'>Desempenho de Grupos Gerenciais</OPTION>";

    //if ($_SESSION["empresaID"] == 37)
    echo "<OPTION value='relatorio_pesquisa.php?cboPesquisa=72'>Resultados de Pesquisa</OPTION>";

    //if ($_SESSION["empresaID"] == 44)
    //    echo "<OPTION value='relatorio_pesquisa.php?cboPesquisa=35'>Resultados de Pesquisa - Participantes</OPTION>";

    echo "</SELECT>";

}

function comboboxTipoRelatorioGerente($nome, $valor){

    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"textbox3\">";

    echo "<OPTION value='relatorio_acesso_consolidado.php?hdnTipoRelatorio=D'>Consolidado da Empresa</OPTION>";

    echo "<OPTION value='relatorio_acesso_consolidado.php?hdnTipoRelatorio=D'>Consolidado do Grupo Gerencial</OPTION>";

    echo "<OPTION value='relatorio_acesso.php?hdnTipoRelatorio=S'>Lista de Participantes</OPTION>";

    echo "<OPTION value='relatorio_nao_acesso.php?hdnTipoRelatorio=S'>Lista de N�o Participantes</OPTION>";

    //	echo "<OPTION value='relatorio_prova.php?tipo=0'>Participantes em Simulados</OPTION>";

    echo "<OPTION value='relatorio_prova.php?tipo=1'>Participantes em Avalia��es/Certifica��es</OPTION>";

    echo "<OPTION value='relatorio_ranking_avaliacao.php?hdnTipoRelatorio=U'>Desempenho de Participantes</OPTION>";

    echo "<OPTION value='relatorio_ranking_avaliacao.php?hdnTipoRelatorio=U'>Desempenho de Participantes 2</OPTION>";

    echo "<OPTION value='relatorio_ranking_avaliacao2.php?hdnTipoRelatorio=U&boletim=1'>Radar Boletim</OPTION>";

    echo "<OPTION value='relatorio_segunda_chamada.php?hdnTipoRelatorio=B'>Avalia��es e Segundas Chamadas</OPTION>";

    //	echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=L'>Desempenho de Lota��es</OPTION>";

    //	echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=G'>Desempenho de Grupos Gerenciais</OPTION>";

    echo "</SELECT>";

}


function comboboxTipoRelatorioGerenteLotacaoTim($nome, $valor){

    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"textbox3\">";

    echo "<OPTION value='relatorio_acesso_consolidado.php?hdnTipoRelatorio=D'>Consolidado do Grupo de Gest�o</OPTION>";

    echo "<OPTION value='relatorio_acesso.php?hdnTipoRelatorio=S'>Lista de Participantes</OPTION>";

    echo "<OPTION value='relatorio_nao_acesso.php?hdnTipoRelatorio=S'>Lista de N�o Participantes</OPTION>";

    //	echo "<OPTION value='relatorio_prova.php?tipo=0'>Participantes em Simulados</OPTION>";

    echo "<OPTION value='relatorio_prova.php?tipo=1'>Participantes em Avalia��es/Certifica��es</OPTION>";

    //	echo "<OPTION value='relatorio_ranking_grupo.php?hdnTipoRelatorio=L'>Gest�o de Desempenho de Grupos Gerenciais</OPTION>";

    echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=U'>Desempenho de Participantes</OPTION>";

    echo "<OPTION value='relatorio_assessment.php?hdnTipoRelatorio=G'>Assessment</OPTION>";

    //	echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=L'>Desempenho de Lota��es</OPTION>";

    echo "</SELECT>";

}


function comboboxTipoRelatorioGerenteLotacao($nome, $valor, $somenteAssessment=1){

    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"textbox3\">";

    echo "<OPTION value='relatorio_acesso_consolidado.php?hdnTipoRelatorio=D'>Evolu��o do Programa</OPTION>";

    //	echo "<OPTION value='relatorio_acesso_consolidado.php?hdnTipoRelatorio=D'>Consolidado da Lota��o</OPTION>";

    echo "<OPTION value='relatorio_acesso.php?hdnTipoRelatorio=S'>Lista de Participantes</OPTION>";

    echo "<OPTION value='relatorio_nao_acesso.php?hdnTipoRelatorio=S'>Lista de N�o Participantes</OPTION>";

    //	echo "<OPTION value='relatorio_prova.php?tipo=0'>Participantes em Simulados</OPTION>";

    //	echo "<OPTION value='relatorio_prova.php?tipo=1'>Participantes em Avalia��es/Certifica��es</OPTION>";

    //	echo "<OPTION value='relatorio_ranking_grupo.php?hdnTipoRelatorio=L'>Gest�o de Desempenho de Grupos Gerenciais</OPTION>";

    //if ($somenteAssessment) {

    echo "<OPTION value='relatorio_ranking_avaliacao.php?hdnTipoRelatorio=U'>Desempenho de Participantes</OPTION>";

    echo "<OPTION value='relatorio_ranking_avaliacao2.php?hdnTipoRelatorio=U&boletim=1'>Radar Boletim</OPTION>";

    //}

    //	echo "<OPTION value='relatorio_segunda_chamada.php?hdnTipoRelatorio=B'>Avalia��es e Segundas Chamadas</OPTION>";

    //	echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=L'>Desempenho de Lota��es</OPTION>";

    echo "<OPTION value='relatorio_assessment.php?hdnTipoRelatorio=G&tipo=C'>Plano de Aprendizado</OPTION>";

    if (!$somenteAssessment) {

        echo "<OPTION value='relatorio_assessment.php?hdnTipoRelatorio=G'>Evolu��o de Aprendizado</OPTION>";

        //if ($_SESSION["empresaID"] != 37)
        //    echo "<OPTION value='relatorio_boletim.php?hdnTipoRelatorio=G&tipo=C'>Radar Boletim</OPTION>";

    }

    //if ($_SESSION["empresaID"] == 37)
    echo "<OPTION value='relatorio_pesquisa.php?cboPesquisa=72'>Resultados de Pesquisa</OPTION>";

    //if ($_SESSION["empresaID"] == 44)
    //    echo "<OPTION value='relatorio_pesquisa.php?cboPesquisa=35'>Resultados de Pesquisa - Participantes</OPTION>";

    echo "</SELECT>";

}



function comboboxTipoRelatorioIndividualTim($nome, $valor){

    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"textbox3\">";

    //	echo "<OPTION value=''>Nenhum Relat�rio Dispon�vel no Momento</OPTION>";

    echo "<OPTION value='relatorio_ranking_individual.php?hdnTipoRelatorio=G'>Radar do Participante</OPTION>";

    echo "<OPTION value='relatorio_prova_individual.php?tipo=0'>Simulados</OPTION>";

    echo "<OPTION value='relatorio_prova_individual.php?tipo=1'>Avalia��es/Certifica��es</OPTION>";

    echo "<OPTION value='relatorio_pesquisa.php?cboPesquisa=8'>Resultados de Pesquisa</OPTION>";

    echo "</SELECT>";

}



function comboboxTipoRelatorioIndividual($nome, $valor, $somenteAssessment=1){

    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"textbox3\">";

    //	echo "<OPTION value=''>Nenhum Relat�rio Dispon�vel no Momento</OPTION>";

    if ($somenteAssessment)
        echo "<OPTION value='relatorio_assessment_individual.php?hdnTipoRelatorio=G&tipo=C'>Assessment do Participante</OPTION>";
    else
        echo "<OPTION value='relatorio_assessment_individual.php?hdnTipoRelatorio=G&tipo=C'>Plano de Aprendizado</OPTION>";

    if (!$somenteAssessment) {

        //		echo "<OPTION value='relatorio_ranking_individual.php?hdnTipoRelatorio=G'>Radar de Desempenho</OPTION>";

        echo "<OPTION value='relatorio_prova_individual.php?tipo=0'>Lista de Simulados</OPTION>";

        echo "<OPTION value='relatorio_prova_individual.php?tipo=1'>Lista de Avalia��es</OPTION>";

    }else{

        if ($_SESSION["tipo"] >=4 || $_SESSION["tipo"] == -1){

            //		echo "<OPTION value='relatorio_prova_individual.php?tipo=1'>Lista de Avalia��es/Certifica��es</OPTION>";

            echo "<OPTION value='relatorio_prova_individual.php?tipo=1'>Lista de Avalia��es</OPTION>";
        }
    }

    if (!$somenteAssessment) {

        //		echo "<OPTION value='relatorio_assessment.php?hdnTipoRelatorio=G&tipo=C'>Radar de Conhecimento</OPTION>";

        echo "<OPTION value='relatorio_assessment.php?hdnTipoRelatorio=G&hdnLotacao=-1&cboLocalidade=-1&cboCargo=-1'>Evolu��o de Aprendizado</OPTION>";

        //		echo "<OPTION value='relatorio_pesquisa.php?cboPesquisa=8'>Resultados de Pesquisa</OPTION>";

    }

    //if ($_SESSION["empresaID"] == 37)
    echo "<OPTION value='relatorio_pesquisa.php?cboPesquisa=72'>Resultados de Pesquisa</OPTION>";


    //if ($_SESSION["empresaID"] == 44)
    //    echo "<OPTION value='relatorio_pesquisa.php?cboPesquisa=35'>Resultados de Pesquisa - Participantes</OPTION>";

    echo "</SELECT>";

}


function comboboxTipoUsuario($nome, $valor, $autoPostBack = false, $cssClass = "textbox3", $linhaExtra=false, $multiple = false, $campoTag = "", $itemInicial = null){

    $sql = "SELECT ID, NM_TIPO_USU FROM tb_tipo_usuario";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "NM_TIPO_USU", "ID", $linhaExtra, "100%", $cssClass, false, false, $autoPostBack, $multiple, $campoTag, $itemInicial);

}


function comboboxCliente($nome, $valor, $obrigatorio = false, $multiple = false){

    $rn = new col_cliente();

    $resultado = $rn->listar(null,"NM_CLIENTE");

    combobox($nome, $valor, $resultado, "NM_CLIENTE", "CD_CLIENTE", true, "100%", "textbox3", $obrigatorio, true, false, $multiple);

}


function comboboxBlog($nome, $valor, $obrigatorio = false, $multiple = false){

    //	$rn = new wp_blogs();

    //	$resultado = $rn->listar(null,"PATH");

    $sql = " SELECT BLOG_ID, SUBSTR(PATH,7) as PATH FROM wp_blogs WHERE PUBLIC <> 1 AND DELETED = 0 AND SPAM = 0 ORDER BY PATH ";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "PATH", "PATH", true, "110", "textbox", $obrigatorio, false, false, $multiple);

}


function comboboxDisciplina($nome, $valor, $obrigatorio = false, $multiple = false){

    $rn = new col_disciplina();

    $resultado = $rn->listar(null,"DS_DISCIPLINA");

    combobox($nome, $valor, $resultado, "DS_DISCIPLINA", "CD_DISCIPLINA", true, "100%", "textbox3", $obrigatorio, true, false, $multiple);

}



function comboboxEmpresa($nome, $valor, $obrigatorio = false, $autoPostBack = false, $multiple = false, $cssClass = "textbox3", $habilitado = true){

    $rn = new col_empresa();

    $resultado = $rn->listar(null,"DS_EMPRESA");

    combobox($nome, $valor, $resultado, "DS_EMPRESA", "CD_EMPRESA", true, "100%", $cssClass, $obrigatorio, true, $autoPostBack, $multiple, "", null, $habilitado);

}


function comboboxPagina($nome, $valor, $obrigatorio = false, $autoPostBack = false, $multiple = false){

    $rn = new col_pagina_comentario();

    $resultado = $rn->listarTodos(null,"");

    combobox($nome, $valor, $resultado, "DS_PAGINA", "NM_PAGINA", true, "100%", "textbox3", $obrigatorio, false, $autoPostBack, $multiple, "DS_URL_PAGINA");

}



function comboboxFiltro($nome, $valor, $empresa = 0, $obrigatorio = false, $multiple = false, $autoPostBack = false, $itemInicial = null, $removerInativos = false){

    $rn = new col_filtro();

    $parametro = null;

    //if ($empresa != "" && $empresa > 0)

    $parametro = array(new ParametroDB("CD_EMPRESA", $empresa));

    $resultado = $rn->listar($parametro,"NM_FILTRO");

    combobox($nome, $valor, $resultado, "NM_FILTRO", "CD_FILTRO", true, "100%", "textbox3", $obrigatorio, $removerInativos, $autoPostBack, $multiple, "", $itemInicial);

}



function obterLotacaoUsuario($usuario){

    $sql = "select DS_LOTACAO_HIERARQUIA, tipo from col_usuario u INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao where CD_USUARIO = $usuario";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    $linha = mysql_fetch_array($resultado);

    return $linha;

}


function comboboxTipoRelatorioTim($nome, $valor, $visao){

    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"textbox3\">";

    echo "<OPTION value='relatorio_acesso_consolidado.php?hdnTipoRelatorio=D'>Consolidado</OPTION>";

    echo "<OPTION value='relatorio_acesso.php?hdnTipoRelatorio=S'>Lista de Participantes</OPTION>";

    echo "<OPTION value='relatorio_nao_acesso.php?hdnTipoRelatorio=S'>Lista de N�o Participantes</OPTION>";

    //	echo "<OPTION value='relatorio_prova.php?tipo=0'>Participantes em Simulados</OPTION>";

    echo "<OPTION value='relatorio_prova.php?tipo=1'>Participantes em Avalia��es/Certifica��es</OPTION>";

    echo "<OPTION value='relatorio_nao_acesso_avaliacao.php?tipo=1'>Participantes Eleg�veis sem Certifica��es</OPTION>";

    echo "<OPTION value='relatorio_ranking_grupo.php?hdnTipoRelatorio=L'>Radar de Desempenho de Grupos Gerenciais</OPTION>";

    echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=U'>Radar de Desempenho de Participantes</OPTION>";

    echo "<OPTION value='relatorio_assessment.php?hdnTipoRelatorio=G'>Assessment</OPTION>";

    //	echo "<OPTION value='relatorio_segunda_chamada.php?hdnTipoRelatorio=G'>Eleg�veis para Segunda Chamada</OPTION>";

    //	echo "<OPTION value='relatorio_ranking.php?hdnTipoRelatorio=L&hdnHierarquico=1'>Desempenho</OPTION>";

    echo "</SELECT>";

}



function comboboxLotacaoHierarquia($nome, $valor, $empresa, $usuario, $obrigatorio = false, $multiple = false, $itemInicialSelect = false, $cssClass = "textbox3"){

    $filtro = " WHERE l.CD_EMPRESA = $empresa ";

    $itemInicial = array("CD_LOTACAO" => "-1", "DS_LOTACAO" => "Empresa");

    if ($itemInicialSelect)
        $itemInicial = null;

    $dadosUsuario = obterLotacaoUsuario($usuario);

    if (!($dadosUsuario["tipo"] == -1 || $dadosUsuario["tipo"] >= 4)){

        $lotacaoHierarquia = $dadosUsuario["DS_LOTACAO_HIERARQUIA"];

        $filtro = "$filtro AND (DS_LOTACAO_HIERARQUIA LIKE '$lotacaoHierarquia.%' OR DS_LOTACAO_HIERARQUIA = '$lotacaoHierarquia') ";

        $itemInicial = null;

    }

    if ($dadosUsuario["tipo"] >= 4 && $_SESSION["tipo"] != -1 && !($_SESSION["tipo"] >= 4)){ //Se for administrador mas estiver simulando outro perfil que n�o super RH

        $lotacao = $_SESSION["lotacaoID"];

        $filtro = "$filtro AND (DS_LOTACAO_HIERARQUIA LIKE '%$lotacao.%' OR CD_LOTACAO = '$lotacao') ";

        $itemInicial = null;

    }

    $sql = "SELECT l.CD_LOTACAO, CONCAT(COALESCE(l.CD_IDENTIFICADOR_LOTACAO,''), ' - ', l.DS_LOTACAO) AS DS_LOTACAO, l.IN_ATIVO FROM col_lotacao l LEFT OUTER JOIN col_hierarquia h ON l.CD_NIVEL_HIERARQUICO = h.CD_NIVEL_HIERARQUICO

			$filtro

			ORDER BY l.CD_IDENTIFICADOR_LOTACAO, l.CD_NIVEL_HIERARQUICO, l.DS_LOTACAO";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "DS_LOTACAO", "CD_LOTACAO", true, "100%", $cssClass, $obrigatorio, true, false, $multiple, "", $itemInicial);

}



function comboboxLotacao($nome, $valor, $empresa = 0, $obrigatorio = false, $multiple = false){

    $filtro = "";

    //if ($empresa != "" && $empresa > 0)
    //{

    $filtro = " WHERE CD_EMPRESA = $empresa ";

    //}

    $sql = "SELECT CD_LOTACAO, DS_LOTACAO, IN_ATIVO FROM col_lotacao
			$filtro
			ORDER BY DS_LOTACAO";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "DS_LOTACAO", "CD_LOTACAO", true, "100%", "textbox3", $obrigatorio, false, false, $multiple);

}

function comboboxPastaEmpresa($nome, $valor, $empresa, $obrigatorio = false){

    $sql = "SELECT DISTINCT pa.ID, pa.Titulo, pa.Status as IN_ATIVO FROM tb_pastasindividuais pa
      INNER JOIN tb_paginasindividuais pi ON pa.ID = pi.PastaID
      WHERE
                pi. STATUS = 1
            AND pi.CD_EMPRESA = $empresa
      ORDER BY  pa.NR_ORDEM";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "Titulo", "ID", true, "100%", "textbox3", $obrigatorio, true, false);

}



function comboboxLocalidade($nome, $valor, $empresa = -1, $obrigatorio = false, $multiple = false){

    $sql = "SELECT CD_LOCALIDADE, NM_LOCALIDADE FROM col_localidade WHERE CD_EMPRESA = $empresa AND IN_ATIVO =1 ORDER BY NM_LOCALIDADE";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "NM_LOCALIDADE", "CD_LOCALIDADE", true, "100%", "textbox3", $obrigatorio, false, false, $multiple, "", null, true, "20px");

}


function comboboxCargoFuncaoPrograma($nome, $valor, $empresa = -1, $obrigatorio = false, $multiple = false){

    $filtro = "";

    //if ($empresa != "" && $empresa > 0)

    //{

    $filtro = " AND (empresa = $empresa or $empresa = -1 )";

    //}

    $sql = "SELECT DISTINCT CONCAT(DS_EMPRESA, ' - ', `cargofuncao`) as cargofuncaoempresa, cargofuncao FROM `col_usuario` INNER JOIN col_empresa ON col_usuario.empresa = col_empresa.CD_EMPRESA
			WHERE Status = 1 AND NOT cargofuncao = '' $filtro
			ORDER BY DS_EMPRESA, cargofuncao";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "cargofuncaoempresa", "cargofuncao", true, "100%", "textbox3", $obrigatorio, false, false, $multiple);

}



function comboboxCargoFuncao($nome, $valor, $empresa = 0, $obrigatorio = false, $multiple = false){

    $filtro = "";

    //if ($empresa != "" && $empresa > 0)

    //{

    $filtro = " AND empresa = $empresa ";

    //}

    $sql = "SELECT DISTINCT `cargofuncao` FROM `col_usuario`
			WHERE Status = 1 AND tipo = 1 AND NOT cargofuncao = ''  $filtro
			ORDER BY cargofuncao";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "cargofuncao", "cargofuncao", true, "100%", "textbox3", $obrigatorio, false, false, $multiple);

}



function comboboxUsuario($nome, $valor, $obrigatorio = false){

    $rn = new col_usuario();

    $resultado = $rn->listar(null,"NM_USUARIO");

    combobox($nome, $valor, $resultado, "NM_USUARIO", "CD_USUARIO", true, "100%", "textbox3", $obrigatorio);

}



function comboboxUsuarioEmpresa($nome, $valor, $empresa, $obrigatorio = false, $multiple = false, $tipo = null){

    $rn = new col_usuario();

    $parametro=array();

    $parametro[] = new ParametroDB("empresa", $empresa);

    if ($tipo != null){

        $parametro[] = new ParametroDB("tipo", $tipo);

    }

    $resultado = $rn->listar($parametro,"NM_USUARIO");

    combobox($nome, $valor, $resultado, "NM_USUARIO", "CD_USUARIO", true, "100%", "textbox3", $obrigatorio, false, false, $multiple);

}



function comboboxUsuarioModerador($nome, $valor, $empresa, $obrigatorio = false){

    $filtro = "";

    //if ($empresa != "" && $empresa > 0)

    //{

    $filtro = " AND f.CD_EMPRESA = $empresa ";

    //}

    $sql = "SELECT CD_USUARIO, NM_USUARIO FROM col_usuario u INNER JOIN col_foruns f ON u.CD_USUARIO = f.CD_USUARIO_MEDIADOR WHERE f.IN_ATIVO = 1 $filtro";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "NM_USUARIO", "CD_USUARIO", true, "100%", "textbox3", $obrigatorio);

}


function comboboxForum($nome, $valor, $empresa, $obrigatorio = false, $multiple = false, $exibirInativos = false){

    $filtro = "";

    //if ($empresa != "" && $empresa > 0)

    //{

    $filtro = " AND CD_EMPRESA = $empresa ";

    //}

    if (!$exibirInativos){

        $filtro = " $filtro AND IN_ATIVO = 1 ";

    }

    $sql = "SELECT CD_FORUM, DS_FORUM FROM col_foruns WHERE 1=1 $filtro";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "DS_FORUM", "CD_FORUM", true, "100%", "textbox3", $obrigatorio, false, false, $multiple);

}




function comboboxProva($nome, $valor, $obrigatorio = false, $removerInativos=true){







    $rn = new col_prova();



    $resultado = $rn->listar(null,"DS_PROVA");







    combobox($nome, $valor, $resultado, "DS_PROVA", "CD_PROVA", true, "100%", "textbox3", $obrigatorio, $removerInativos);







}







function comboboxProvaEmpresa($nome, $valor, $empresa, $obrigatorio = false, $removerInativos = true, $multiple = false){







    $sql = "SELECT p.CD_PROVA, p.DS_PROVA, p.IN_ATIVO FROM col_prova p



						INNER JOIN col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA



						INNER JOIN col_usuario u ON u.CD_USUARIO = pa.CD_USUARIO



					WHERE u.empresa = $empresa GROUP BY p.CD_PROVA, p.DS_PROVA, p.IN_ATIVO ORDER BY p.DS_PROVA";







    //echo $sql;







    $dao = new DaoEngine();







    $resultado = $dao->executeQuery($sql);







    combobox($nome, $valor, $resultado, "DS_PROVA", "CD_PROVA", true, "100%", "textbox3", $obrigatorio, $removerInativos, false, $multiple);







}







function dualListBoxPaginas(){







    $paginaAcesso = new col_pagina_acesso();



    $resultado = $paginaAcesso->listar();



    dualListBox("listPaginas", "100%", $resultado, "", "CD_PAGINA_ACESSO", "DS_PAGINA_ACESSO", "Dispon�veis", "Selecionadas", "300px", "300px", "150px", true);







}







function combobox($nome, $valor, $resultado, $campoTexto, $campoValor, $linhaExtra = true,$width = "100%", $cssClass = "textbox3", $obrigatorio = false, $removerInativos = false, $autoPostBack = false, $multiple = false, $campoTag = "", $itemInicial = null, $habilitado = true, $altura = "")
{

    if ($obrigatorio){
        $obrigatorio = "TagMensagemObrigatorio = '$obrigatorio'";
    }

    if ($autoPostBack)
    {



        $autoPostBack = "onchange=document.forms[0].submit();";



    }else



    {



        $autoPostBack = "";



    }







    if ($multiple)
    {



        $multiple = "multiple";



        $cssClass = "";



        $linhaExtra = false;



    }else{



        $multiple = "";



    }



    if (!$habilitado) {

        $habilitado = "disabled";

        echo "<input type=\"hidden\" id=\"$nome\" name=\"$nome\" value=\"$valor\" />";

    }

    else {

        $habilitado = "";

    }


    if($altura != ""){

        $altura = "style=\"height: $altura;\"";

    }


    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"$cssClass\" $autoPostBack $obrigatorio $multiple $habilitado $altura>";







    if($linhaExtra){







        if ($itemInicial == null)



            echo "<OPTION value='-1'>-- Selecione --</OPTION>";



        else



            echo "<OPTION value='{$itemInicial[$campoValor]}'>{$itemInicial[$campoTexto]}</OPTION>";



    }







    while ($linha = mysql_fetch_array($resultado)) {







        if ($removerInativos){



            if ($linha["IN_ATIVO"] != 1 && !compararValorCombo($linha[$campoValor], $valor)){



                continue;



            }



        }







        echo "<OPTION value='$linha[$campoValor]' ";







        if (compararValorCombo($linha[$campoValor], $valor)){



            echo "selected";



        }





        $tag="";

        if ($campoTag != "")



            $tag = " tag=\"$linha[$campoTag]\"";







        echo "$tag>$linha[$campoTexto]</OPTION>";







    }











    echo "</SELECT>";







}







function compararValorCombo($valorLinha, $valorSelecionado)



{







    if (is_array($valorSelecionado))



    {







        foreach ($valorSelecionado as $valor)



        {







            if ($valor == $valorLinha)



                return true;



        }



    }



    else



    {



        return $valorLinha == $valorSelecionado;



    }











    return false;



}







function comboboxAgrupamento($nome, $valor)



{







    $selectLotacao = "";



    $selectCargoFuncao = "";



    $selectUsuario = "";







    if (strpos($valor, "1") !== false)



    {



        $selectLotacao = "selected";



    }







    if (strpos($valor, "2") !== false)



    {



        $selectCargoFuncao = "selected";



    }







    if (strpos($valor, "3") !== false)



    {



        $selectUsuario = "selected";



    }







    echo	"<select class=\"select3\" id=\"$nome\" name=\"$nome\" multiple>



				<option value=\"1\" $selectLotacao>Lota��o</option>



				<option value=\"2\" $selectCargoFuncao>Cargo/Fun��o</option>



				<option value=\"3\" $selectUsuario>Usu�rio</option>



			 </select>";







}







function comboboxPeso($nome, $valor, $obrigatorio = false){







    if ($obrigatorio){



        $obrigatorio = "TagMensagemObrigatorio = '$obrigatorio'";



    }







    echo "<SELECT id=\"$nome\" name=\"$nome\" class=\"textbox3\" $obrigatorio style='width:40px'>";



    echo "<OPTION value=\"-1\"> </OPTION>";



    for ($i = 1; $i < 21; $i++){



        $selected = "";



        if ($i == $valor){



            $selected = " selected";



        }



        echo "<OPTION$selected value=\"$i\">$i</OPTION>";



    }







    echo  "</SELECT>";







}







function label()

{







}







function dualListBox($nome, $tamanho, $listagem, $selecionados = "", $campoValor, $campoTexto, $tituloEsquerda, $tituloDireita, $larguraEsquerda = "170px", $larguraDireita = "200px", $altura = "", $ordenacao = false){







    $itensDireita = array();



    $itensEsquerda = array();



    $itensSelecionados = null;







    if (is_array($selecionados)){



        foreach ($selecionados as $selecionado){



            $propriedade = new ReflectionProperty(get_class($selecionado), $campoValor);



            $valor = $propriedade->getValue($selecionado);



            $itensSelecionados = "$itensSelecionados $valor;";



        }



    }else{



        $itensSelecionados = $selecionados;



    }







    $itensSelecionados = split(";",$itensSelecionados);







    while ($linha = mysql_fetch_array($listagem)){







        $itemSelecionado = false;







        for ($i = 0; $i < count($itensSelecionados); $i++){







            if ($linha[$campoValor] == $itensSelecionados[$i]){



                $itemSelecionado = true;



                break;



            }







        }







        if ($itemSelecionado){



            $itensDireita[] = $linha;



        }else {



            $itensEsquerda[] = $linha;



        }











    }











    echo "<INPUT type='hidden' id=\"$nome\" name='$nome' value='$itensSelecionados' />";







    echo "<TABLE cellpadding='0' cellspacing='2' border='0'>



			<TR><TD class='textblk'>";







    echo "$tituloEsquerda <BR />";



    listBox("{$nome}Esquerda", $itensEsquerda, $campoValor, $campoTexto, $larguraEsquerda, $altura);







    echo "</TD><TD>";







    echo "<BUTTON onClick=\"javascript:MovingItems('{$nome}Esquerda','{$nome}Direita', false)\">>></BUTTON ><br>



	<BUTTON onClick=\"javascript:MovingItems('{$nome}Direita','{$nome}Esquerda', false)\"><<</BUTTON>";







    echo "</TD><TD class='textblk'>";







    echo "$tituloDireita <BR />";



    listBox("{$nome}Direita", $itensDireita,  $campoValor, $campoTexto, $larguraDireita, $altura);







    echo "</TD>";







    if ($ordenacao){



        echo "<td><BUTTON onClick=\"javascript:SwapItem('{$nome}Direita', -1)\" style='font-family: wingdings'>�</BUTTON ><br>



		<BUTTON onClick=\"javascript:SwapItem('{$nome}Direita', 1)\" style='font-family: wingdings'>�</BUTTON></td>";



    }











    echo "</TR></TABLE>";







    $nomeFuncaoJavascript = "selecionar$nome";







    page::$validador = $nomeFuncaoJavascript . "();" . page::$validador . ";";







    ?>







<script language="javascript">







    function MovingItems(From, To, All)



    {



        From = document.getElementById(From);



        To = document.getElementById(To);







        for (i = 0; i < From.options.length; i++)



        {



            if (All)



                From.options[i].selected = true;



            if (From.options[i].selected)



            {



                To.options[To.options.length] = new Option(From.options[i].text, From.options[i].value);



                From.options[i--] = null;



            }



        }



    }







    function <?php echo "$nomeFuncaoJavascript()"; ?>
    {

        var campoHidden = document.getElementById("<?php echo $nome; ?>");

        campoHidden.value = "";

        var objCamposDireita = document.getElementById("<?php echo "{$nome}Direita"; ?>");

        for (var i = 0; i < objCamposDireita.options.length; i++)

        {	if (i > 0) campoHidden.value += ";";

            campoHidden.value += objCamposDireita.options[i].value;

        }

        return campoHidden.value;
    }

    function SwapItem(s, indice) {

        s = document.getElementById(s);

        var itemPos = s.selectedIndex;

        var swapPos = s.selectedIndex + indice;

        if (swapPos < 0 || swapPos > s.options.length - 1){

            return;

        }

        var tempOption = new Array(s.options[swapPos].text, s.options[swapPos].value);

        s.options[swapPos].text = s.options[itemPos].text;

        s.options[swapPos].value = s.options[itemPos].value;

        s.options[itemPos].text = tempOption[0];

        s.options[itemPos].value = tempOption[1];

        s.selectedIndex = swapPos;

    }


</script>

<?php

}

function listBox($nome, $itens, $campoValor, $campoTexto, $largura = "", $altura = ""){

    if ($altura != ""){

        $altura = "height: $altura";

    }

    echo "<SELECT multiple id=\"$nome\" name='$nome' style='width: $largura; $altura'>";

    foreach ($itens as $item){

        echo "<OPTION value='$item[$campoValor]' title='$item[$campoTexto]'>$item[$campoTexto]</OPTION>";

    }

    echo "</SELECT>";

}

function obterTextoComentario($pagina, $controlaConexao = true, $codigoCiclo = 0){

    if (isset($_GET["operacao"]) && $_GET["operacao"] == 765){

        if ($pagina == str_ireplace(".php", "", $_POST["cboPagina"])){

            echo $_POST["htmleditor"];

            return;

        }

    }

    $pagina = $pagina . ".php";

    $idEmpresa = $_SESSION["empresaID"];

    $sql = "SELECT TX_COMENTARIO FROM col_comentario WHERE NM_PAGINA = '$pagina' AND CD_EMPRESA = $idEmpresa AND IN_ATIVO=1 AND (CD_CICLO = $codigoCiclo OR ($codigoCiclo = 0 AND (CD_CICLO IS NULL OR CD_CICLO < 0)))";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,$controlaConexao);

    while ($linha = mysql_fetch_array($resultado)) {

        echo $linha["TX_COMENTARIO"];

    }

}


function obterSessaoEmpresa(){

    if ($_GET["operacao"] == 765){

        $_SESSION["empresaIDBackup"] = $_SESSION["empresaID"];

        $_SESSION["empresaID"] = $_POST["cboEmpresa"];

    }

}







function acertarSessaoEmpresa()

{







    if ($_GET["operacao"] == 765)

    {







        $_SESSION["empresaID"] = $_SESSION["empresaIDBackup"];



        $_SESSION["empresaIDBackup"] = "";







    }







}











function obterValoresLotacao($valores)

{







    if (!is_array($valores))



        return array();







    $codigos = array();







    foreach ($valores as $valor)

    {







        $codigos[] = $valor->CD_LOTACAO;







    }







    return  $codigos;







}







function obterValoresCargo($valores)

{







    if (!is_array($valores))



        return array();







    $codigos = array();







    foreach ($valores as $valor)

    {







        $codigos[] = $valor->NM_CARGO_FUNCAO;







    }







    return  $codigos;







}







function obterValoresUsuario($valores)

{







    if (!is_array($valores))



        return array();







    $codigos = array();







    foreach ($valores as $valor)

    {







        $codigos[] = $valor->CD_USUARIO;







    }







    return  $codigos;







}







function obterValoresProva($valores)

{







    if (!is_array($valores))



        return array();







    $codigos = array();







    foreach ($valores as $valor)

    {







        $codigos[] = $valor->CD_PROVA;







    }







    return  $codigos;







}







function obterValoresForum($valores)

{







    if (!is_array($valores))



        return array();







    $codigos = array();







    foreach ($valores as $valor)

    {







        $codigos[] = $valor->CD_FORUM;







    }







    return  $codigos;







}







function TratarCheckFarol($valor)

{



    if ($valor != 1)



        return 2;







    return $valor;



}







function conveterDataParaYMD($data)

{



    $aData = split("/", $data);



    $data = "{$aData[2]}{$aData[1]}{$aData[0]}";







    return $data;







}







function somarUmDia($data){



    $aData = split("/", $data);



    $data = date("d/m/Y", strtotime("{$aData[1]}/{$aData[0]}/{$aData[2]}") + 90000);







    return  $data;



}







function formataDataBancoYmd($data)

{







    $data = split("-", $data);



    $data = "{$data[0]}{$data[1]}{$data[2]}";







    return $data;







}







function formataDataBancodmY($data)

{







    $data = split("-", $data);



    $data = "{$data[2]}/{$data[1]}/{$data[0]}";







    return $data;







}



function formataDataStringYmd($data)

{



    $data = split("/", $data);



    $data = "{$data[2]}{$data[1]}{$data[0]}";



    return $data;



}



function formataNotaAvaliacao($nota)

{



    global $notaCentesimal;

    if (!$notaCentesimal) {

        return $nota;

    }



    return $nota * 10;

}

function obterCicloAtual($empresa){

    $sql = "SELECT
                *
            FROM
                col_ciclo
            WHERE
                CD_EMPRESA = $empresa
            AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= DATE_FORMAT(ADDDATE(SYSDATE(),1), '%Y%m%d')
            AND DATE_FORMAT(DT_TERMINO, '%Y%m%d') >= DATE_FORMAT(ADDDATE(SYSDATE(),1), '%Y%m%d')
            ORDER BY
                DT_INICIO DESC";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    if($linha = mysql_fetch_array($resultado)){
        return $linha["CD_CICLO"];
    }

    return 0;

}


function obterCiclos($empresa){

    $sql = "SELECT * FROM col_ciclo WHERE CD_EMPRESA = $empresa AND IN_ATIVO = 1 AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= DATE_FORMAT(ADDDATE(SYSDATE(),1), '%Y%m%d') ORDER BY DT_INICIO";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    $ciclos = array();

    while ($linha = mysql_fetch_array($resultado)) {

        $ciclos[] = $linha;

    }

    return $ciclos;

}

function obterCiclosLiberados($usuario){

    $sql = "SELECT
                a.CD_PROVA, p.CD_CICLO
            FROM
                col_assessment a
            INNER JOIN col_prova_aplicada pa ON pa.CD_PROVA = a.CD_PROVA
            INNER JOIN col_prova p ON p.CD_PROVA = a.CD_PROVA
            WHERE
                pa.CD_USUARIO = $usuario
            AND pa.VL_MEDIA IS NOT NULL";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    $ciclosLiberados = array();

    while ($linha = mysql_fetch_array($resultado)) {

        $ciclosLiberados[$linha["CD_CICLO"]] = $linha["CD_CICLO"];

    }

    return $ciclosLiberados;

}

function obterPAs($codigoEmpresa, $controlaConexao = true){

    $sql = "SELECT
                CD_PROVA
            FROM
                col_assessment a
            WHERE
                CD_EMPRESA = $codigoEmpresa";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,$controlaConexao);

    $ciclosLiberados = array();

    while ($linha = mysql_fetch_array($resultado)) {

        $ciclosLiberados[] = $linha["CD_PROVA"];

    }

    return join(",", $ciclosLiberados);

}


function comboboxTipoConsulta($nome, $valor, $obrigatorio){

    echo "<SELECT onchange=\"javascript:preparaFiltroConsulta(this)\" id=\"$nome\" name=\"$nome\" style=\"width: 700px\" class=\"textbox3\">";

    echo "<option value=\"1\" tipo=\"0\">Todos os participantes cadastrados</option>";
    echo "<option value=\"2\" tipo=\"0\">Todos os gerentes cadastrados</option>";
    echo "<option value=\"3\" tipo=\"0\">Todos os participantes que acessaram e n�o fizeram o PA</option>";
    echo "<option value=\"4\" tipo=\"0\">Todos os participantes que fizeram o PA</option>";
    echo "<option value=\"5\" tipo=\"0\">Todos os participantes que fizeram o PA, mas n�o fizeram Simulados</option>";
    echo "<option value=\"6\" tipo=\"0\">Todos os participantes que fizeram o PA e n�o fizeram o AA</option>";
    echo "<option value=\"7\" tipo=\"N\">Todos os participantes que fizeram o AA e obtiveram nota acima de (vari�vel nota)</option>";
    echo "<option value=\"8\" tipo=\"0\">Todos os participantes que participaram da pesquisa corrente</option>";
    echo "<option value=\"9\" tipo=\"0\">Todos os participantes que n�o participaram da pesquisa corrente</option>";
    echo "<option value=\"10\" tipo=\"Q\">Todo os participantes que fizeram o PA  e fizeram (vari�vel quantidade) simulados </option>";
    echo "<option value=\"11\" tipo=\"Q\">Todos os participantes que acessaram menos que (vari�vel quantidade) vezes o programa </option>";
    echo "<option value=\"12\" tipo=\"N\">Todos os participantes que fizeram simulados mas n�o tiraram notas acima de (vari�vel nota)</option>";
    echo "<option value=\"13\" tipo=\"QN\">Todos os participantes que fizeram mais de (vari�vel quantidade) simulados com nota acima de (vari�vel nota)</option>";

    echo "</SELECT>";


}


function comboboxPasta($nome, $valor, $obrigatorio = false, $autoPostBack = false){

    $sql = "SELECT ID, Titulo, Status as IN_ATIVO FROM tb_pastasindividuais ORDER BY Titulo";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    combobox($nome, $valor, $resultado, "Titulo", "ID", true, "100%", "textbox3", $obrigatorio, true, $autoPostBack);

}


function graficoCadastro($empresa, $codigoLotacao = -1){

    $sql = "SELECT
                COUNT(DISTINCT u.CD_USUARIO) AS TOTAL,
                COUNT(DISTINCT IF(u.IN_ELEGIVEL = 1,  u.CD_USUARIO, null)) AS ELEGIVEL,
                COUNT(DISTINCT a.CD_USUARIO) AS PARTICIPANTE
            FROM
                col_usuario u
                LEFT JOIN col_lotacao l ON u.lotacao = l.CD_LOTACAO
                LEFT JOIN col_acesso_diario a ON a.CD_USUARIO = u.CD_USUARIO
            WHERE
                u.empresa = $empresa
            AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
            AND u.`Status` = 1";

    $resultado = DaoEngine::getInstance()->executeQuery($sql);

    //echo "<!--$sql-->";

    $linha = mysql_fetch_row($resultado);

    $maxValue = $linha[0] + 500;
    $textos = urlencode("Cadastrados;Eleg�veis;Participantes");
    $querystring = str_replace(";","%3B", "parametro=;;{$linha[0]};{$linha[1]};{$linha[2]}&texto=$textos&title=Ades�o&ytitle=&xtitle&ymax=$maxValue&tickincrement=500&width=550");

    echo "<img src=\"relatorios/imageconfig.php?" . $querystring . "\" />";

}

?>