<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);
include('paginaBase.php');

function instanciarRN(){
	page::$rn = new Copia_Curso();
}

function carregarRN(){
	
	page::$rn->cursoOrigem = $_POST["cboCursoOrigem"];
	page::$rn->cursoDestino = $_REQUEST["cboCursoDestino"];

}

function pagePreRender() {

    if (count($_POST) > 0 && (!isset($_POST["btnSalvar"]))) {

        instanciarRN();

        carregarRN();

    }

}

function pageRenderEspecifico(){
	
?>					<TR>

						<TD class="textblk">Curso de Origem: *</TD>
					</TR>
					<TR>
						<TD><?php comboboxPasta("cboCursoOrigem",  page::$rn->cursoOrigem, "Curso de Origem"); ?></TD>
					</TR>
					<TR>
						<TD colspan="2">&nbsp;</TD>
					</TR>
                    <TR>
                        <TD class="textblk">Curso de Destino: *</TD>
                    </TR>
                    <TR>
                        <TD><?php comboboxPasta("cboCursoDestino",  page::$rn->cursoDestino, "Curso de Destino", true); ?></TD>
                    </TR>
                    <TR>
                        <TD colspan="2" style="color: red;">
                            &nbsp;
                            <?php
                            $destino = page::$rn->cursoDestino;

                            if($destino > 0){
                                $sql = "SELECT COUNT(1) FROM tb_pastasindividuais WHERE CD_EMPRESA IS NOT NULL AND ID = $destino;";
                                $resultado = DaoEngine::getInstance()->executeQuery($sql);
                                $linha = mysql_fetch_row($resultado);
                                if($linha[0] == 0){

                                    echo "O curso de destino deve estar associado a um programa.";

                                    page::$complementoBotaoSalvar = "disabled";

                                }

                            }

                            ?>
                        </TD>
                    </TR>


<?php

}

function cadastrarSimuladoCapitulo($oTitulo, $idEmpresa, $idCapitulo){

    $rn = new col_prova();
    $rn->DS_PROVA = $oTitulo;
    $rn->DT_REALIZACAO = date('d/m/Y');
    $rn->DT_FIM_REALIZACAO = '31/12/2030';
    $rn->DURACAO_PROVA = 30;
    $rn->HORARIO_FIM_DISPONIVEL = "{$rn->DT_FIM_REALIZACAO} 23:59:59";
    $rn->HORARIO_INICIO_DISPONIVEL = "{$rn->DT_REALIZACAO} 00:00:01";
    $rn->NR_QTD_PERGUNTAS_PAGINA = 50;
    $rn->VL_MEDIA_APROVACAO = 7;
    $rn->IN_PROVA = "0";
    $rn->CD_CAPITULO = $idCapitulo;

    $rn->NR_TENTATIVA = 999;
    $rn->IN_GABARITO = 1;
    $rn->IN_OBS = "0";
    $rn->IN_SENHA_MESTRA = "0";
    $rn->cargo = '';
    $rn->cargos = array();
    //$rn->CD_CICLO = $_POST["cboCiclo"];
    $empresas = array();
    $empresas[] = $idEmpresa;
    $rn->empresasCadastradas = $empresas;
    $rn->lotacoesCadastradas = array();

    $usuarios = array();
    $sql = "SELECT CD_USUARIO FROM col_usuario WHERE Status = 1 AND empresa = $idEmpresa";
    $resultado = DaoEngine::getInstance()->executeQuery($sql);
    while($linha = mysql_fetch_row($resultado)){

        $usuarios[] = $linha[0];

    }

    $rn->usuariosCadastrados = $usuarios;

    $disciplinas = array();

    $rn->disciplinas = $disciplinas;

    $rn->NR_QTD_PERGUNTAS_TOTAL = 5;

    $chamada = new col_prova_chamada();
    $chamada->DT_INICIO_REALIZACAO = date('d/m/Y/');
    $chamada->DT_TERMINO_REALIZACAO = '31/12/2030';
    $chamada->HR_INICIO_REALIZACAO = "{$chamada->DT_INICIO_REALIZACAO} {'00:00:01'}";
    $chamada->HR_TERMINO_REALIZACAO = "{$chamada->DT_TERMINO_REALIZACAO} {23:59:59}";
    $chamada->IN_PRIMEIRA_CHAMADA = 1;

    $chamadas[] = $chamada;

    $rn->chamadas = $chamadas;

    $rn->inserir();

}

function cadastrarSimuladoPagina($oTitulo, $idEmpresa, $idDisciplina, $idPaginaIndividual){

    $rn = new col_prova();
    $rn->DS_PROVA = $oTitulo;
    $rn->DT_REALIZACAO = date('d/m/Y');
    $rn->DT_FIM_REALIZACAO = '31/12/2030';
    $rn->DURACAO_PROVA = 30;
    $rn->HORARIO_FIM_DISPONIVEL = "{$rn->DT_FIM_REALIZACAO} 23:59:59";
    $rn->HORARIO_INICIO_DISPONIVEL = "{$rn->DT_REALIZACAO} 00:00:01";
    $rn->NR_QTD_PERGUNTAS_PAGINA = 50;
    $rn->VL_MEDIA_APROVACAO = 7;
    $rn->IN_PROVA = "0";
    $rn->ID_PAGINA_INDIVIDUAL = $idPaginaIndividual;

    $rn->NR_TENTATIVA = 999;
    $rn->IN_GABARITO = 1;
    $rn->IN_OBS = "0";
    $rn->IN_SENHA_MESTRA = "0";
    $rn->cargo = '';
    $rn->cargos = array();
    //$rn->CD_CICLO = $_POST["cboCiclo"];
    $empresas = array();
    $empresas[] = $idEmpresa;
    $rn->empresasCadastradas = $empresas;
    $rn->lotacoesCadastradas = array();

    $usuarios = array();
    $sql = "SELECT CD_USUARIO FROM col_usuario WHERE Status = 1 AND empresa = $idEmpresa";
    $resultado = DaoEngine::getInstance()->executeQuery($sql);
    while($linha = mysql_fetch_row($resultado)){

        $usuarios[] = $linha[0];

    }

    $rn->usuariosCadastrados = $usuarios;

    $disciplinas = array();
    $disciplina = new col_prova_disciplina();
    $disciplina->CD_DISCIPLINA = $idDisciplina;
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_1 = 5;
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_2 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_3 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_4 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_5 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_6 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_7 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_8 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_9 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_10 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_11 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_12 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_13 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_14 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_15 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_16 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_17 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_18 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_19 = "0";
    $disciplina->NR_QTD_PERGUNTAS_NIVEL_20 = "0";

    $disciplinas[] = $disciplina;
    $rn->disciplinas = $disciplinas;

    $rn->NR_QTD_PERGUNTAS_TOTAL = 5;

    $chamada = new col_prova_chamada();
    $chamada->DT_INICIO_REALIZACAO = date('d/m/Y/');
    $chamada->DT_TERMINO_REALIZACAO = '31/12/2030';
    $chamada->HR_INICIO_REALIZACAO = "{$chamada->DT_INICIO_REALIZACAO} {'00:00:01'}";
    $chamada->HR_TERMINO_REALIZACAO = "{$chamada->DT_TERMINO_REALIZACAO} {23:59:59}";
    $chamada->IN_PRIMEIRA_CHAMADA = 1;

    $chamadas[] = $chamada;

    $rn->chamadas = $chamadas;

    $rn->inserir();

}

function pageDepoisDeSalvar(){

    $_SESSION["msg"] = "Banco atualizado com sucesso!     ";

}

?>