<?php
include('../page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include ('../../canal/relatorios/funcao_ranking_individual.php');

function instanciarRN(){
	page::$rn = new col_interacao_programada();
}

function carregarRN(){
	
//	page::$rn->CD_INTERACAO_PROGRAMADA;
	page::$rn->DS_ASSUNTO = $_POST['assunto'];
	$codEmp = $_POST["empresa"];
	$sql = "select DS_EMPRESA from col_empresa WHERE CD_EMPRESA = '$codEmp'";
	$query = DaoEngine::getInstance()->executeQuery($sql,true);
	$nomeProgramaRs = mysql_fetch_row($query);
	
	page::$rn->DS_PROGRAMA = htmlentities($nomeProgramaRs[0]);
	page::$rn->DS_TEXTO_HEADER = $_POST['txtCabecalho'];
	page::$rn->DS_TEXTO_FOOTER = $_POST['txtRodape'];
	page::$rn->CD_RELATORIOS = implode(",", $_POST['chkRel']);
//	page::$rn->QUERY_SQL = implode(",", $_POST['chkFiltro']); //"filipeiack@gmail.com,rafael.lima.net@gmail.com"; //,iunes@colaborae.com.br
	page::$rn->CD_USUARIO_CADASTRADOR = $_SESSION['cd_usu'];
//	page::$rn->IN_REALIZADO;
//	page::$rn->DT_CADASTRO;
//	page::$rn->DT_REALIZADO;
//	page::$rn->IN_ATIVO;
	page::$rn->CD_TIPO_USUARIO = implode(",", $_POST['chkTUsu']);
	page::$rn->CD_PERIODICIDADE = implode(",", $_POST['chkDia']);
	page::$rn->DT_INICIO_VIGENCIA = $_POST['txtDataInicioVigencia'];
	page::$rn->DT_TERMINO_VIGENCIA = $_POST['txtDataTerminoVigencia'];
	page::$rn->HORA_ENVIO = $_POST['txtHoraAgenda'];
}

function carregarFiltroRN(){
	global $filtroVO;
	$filtroVO = new FiltroNewsLetter();
	$filtroVO->empresa = $_POST["empresa"];
	$filtroVO->ciclo = implode(",",$_POST["ciclo"]);
	if ($_POST["prova"])
		$filtroVO->prova = implode(",",$_POST["prova"]);

	if ($_POST["lot"])
		$filtroVO->lot = implode(",",$_POST["lot"]);
	
	if ($_POST["cargo"])
		$filtroVO->cargo = implode(",",$_POST["cargo"]);
	
	$filtroVO->txtNotaInicio = $_POST["txtNotaInicio"];
	$filtroVO->txtNotaTermino = $_POST["txtNotaTermino"];
	$filtroVO->tipoUsuario = implode(",", $_POST['chkTUsu']);
	$filtroVO->usuariosEnviar = $_POST['chkFiltroAll'] == "true" ? null : implode(",", $_POST['chkFiltro']);
}

$filtroVO=null;
$pageMode = $_POST['pageMode'];
$empresa = $_POST['empresa'];
$ciclo = $_POST['ciclo'];

$assunto = $_POST['assunto'];
$area1 = $_POST['txtCabecalho'];
$area2 = $_POST['txtRodape'];
$chkRel = $_POST['chkRel'];
$chkTUsu = $_POST['chkTUsu'];
$chkFiltro = $_POST['chkFiltro'];

switch ($pageMode) {
    case 'btnEnviar':
        preparaEnvioEmail();
        break;
    
    case 'btnFiltrar':
        filtrarUsuarios();
        break;
    
    case 'btnAgendar':
        salvarAgenda();
        break;
    
    case 'btnPreview':
        previewEmail();
        break;
    
    default:
        if($empresa != null)
			buscarCiclos();
		elseif ($ciclo != null)
			buscarProvas();
}

	
	
function buscarCiclos(){
	global $empresa;
	
	$sql = "select CD_CICLO, NM_CICLO from col_ciclo WHERE CD_EMPRESA = '$empresa' and in_ativo = 1 ORDER BY NM_CICLO ASC";
	$query = DaoEngine::getInstance()->executeQuery($sql,true);
	
	if(mysql_num_rows($query) != 0){
		while($oRs = mysql_fetch_row($query)){
	      echo '<option value="'.$oRs[0].'">'.htmlentities($oRs[1]).'</option>';
	   }
	}
   echo '::';

	$sql2 = "select CD_LOTACAO, DS_LOTACAO from col_lotacao WHERE CD_EMPRESA = '$empresa' and in_ativo = 1 ORDER BY DS_LOTACAO ASC";
	$query2 = DaoEngine::getInstance()->executeQuery($sql2,true);
	
	if(mysql_num_rows($query2) != 0){
		while($oRs2 = mysql_fetch_row($query2)){
	      echo '<option value="'.$oRs2[0].'">'.htmlentities($oRs2[1]).'</option>';
	   }
	}
    echo '::';

	$sql3 = "select distinct(cargofuncao) from col_usuario WHERE EMPRESA = '$empresa' and status = 1 and cargofuncao <> '' and cargofuncao is not null ORDER BY cargofuncao ASC";
	$query3 = DaoEngine::getInstance()->executeQuery($sql3,true);
	
	if(mysql_num_rows($query3) != 0){
		while($oRs3 = mysql_fetch_row($query3)){
	      echo '<option value="'.$oRs3[0].'">'.htmlentities($oRs3[0]).'</option>';
	   }
	}
}


function buscarProvas(){
	global $ciclo;
	$cicloStr = implode(",",$ciclo);
	
	$sql = "select CD_PROVA, DS_PROVA from col_prova WHERE CD_CICLO IN (".$cicloStr.") and in_ativo = 1 ORDER BY ds_prova ASC";
	$query = DaoEngine::getInstance()->executeQuery($sql,true);
	
	if(mysql_num_rows($query) != 0){
		while($oRs = mysql_fetch_row($query)){
	      echo '<option value="'.$oRs[0].'">'.htmlentities($oRs[1]).'</option>';
	   }
	}
}


function obterQueryFiltrarUsuarios(){
	global $filtroVO, $chkTUsu;
	carregarFiltroRN();

	$sql = " select u.cd_usuario, u.login, u.email, u.nm_usuario, u.NM_SOBRENOME, c.cd_empresa, c.cd_ciclo ";
	if ($filtroVO->prova != null){
		$sql = $sql . " from col_ciclo c 
				inner join col_prova p on p.cd_ciclo = c.cd_ciclo
				inner join col_prova_aplicada pa on pa.cd_prova = p.cd_prova
				inner join col_usuario u on u.cd_usuario = pa.cd_usuario 
				where c.cd_empresa = ".$filtroVO->empresa."
				and p.cd_prova in (".$filtroVO->prova.") ";
				if ($filtroVO->txtNotaInicio != null){
					$sql = $sql . " and (pa.VL_MEDIA >= ".$filtroVO->txtNotaInicio." or pa.VL_MEDIA <= ".$filtroVO->txtNotaTermino.") ";
				}
	}else{
		$sql = $sql . " from col_usuario u
				inner join col_empresa e on e.cd_empresa = u.empresa
				left join col_ciclo c on c.cd_empresa = e.cd_empresa 
				where c.cd_empresa = ".$filtroVO->empresa;
	}
		
		if ($filtroVO->ciclo != null){
			$sql = $sql . " and c.cd_ciclo in (".$filtroVO->ciclo.") ";
		}
		if ($filtroVO->usuariosEnviar != null){
			$sql = $sql . " and u.cd_usuario in (".$filtroVO->usuariosEnviar.") ";
		}
		if (in_array("SU", $chkTUsu) && in_array("P", $chkTUsu)) {
			$sql = $sql . " and u.tipo <= 1";
		} elseif (in_array("P", $chkTUsu)) {
			$sql = $sql . " and u.tipo = 1";
		} elseif (in_array("SU", $chkTUsu)) {
			$sql = $sql . " and u.tipo < 0";
		}
		if ($filtroVO->lot != null){
			$sql = $sql . " and u.lotacao in (".$filtroVO->lot.") ";
		}
		if ($filtroVO->cargo != null && sizeof($filtroVO->cargo) > 0){
			$sql = $sql . " and (u.cargofuncao = '".$cargoArray[0]."'  ";
			for($i = 1; $i < sizeof($cargoArray); ++$i)
			{
			    $sql = $sql . " or u.cargofuncao = '".$cargoArray[$i]."' ";
			}
			$sql = $sql . " ) ";
		}
		
	$sql = $sql . " group by u.cd_usuario ORDER BY u.nm_usuario";
	return $sql;
}

function filtrarUsuarios(){
	$iCont = 0;
	$bgcolor = "#f1f1f1";
	
	$sql = obterQueryFiltrarUsuarios();
	$query = DaoEngine::getInstance()->executeQuery($sql,true);
	
	if(mysql_num_rows($query) != 0){
		echo 'Total de registros: '.mysql_num_rows($query).'
		<table id="tabelaCadastro" class="header" cellpadding="0" cellspacing="0" border="0" width="100%">
			<thead>
				<tr class="textblk">
					<th style="width:56%;">Nome</th>
					<th>E-mail</th>
					<th style="width:65px;"><input type="checkbox" id="chkFiltroAll" onclick="marcaTodos(this)" value="true"/>Todos</th>
				</tr>
			</thead>
	    </table>
	    <div class="scroller">
			<table id="tabelaCadastro" class="textblk" cellpadding="0" cellspacing="0" border="0" width="100%"><tbody>
		';
		while($oRs = mysql_fetch_row($query)){
			echo '<tr class="textblk" bgcolor="'.$bgcolor.'" >';
	      	echo '<td style="width:55%;">'.htmlentities($oRs[1]).' / '.htmlentities($oRs[3]).' '.htmlentities($oRs[4]).'</td>';
	      	echo '<td>&nbsp;'.($oRs[2]).'</td>';
	      	echo '<td style="width:65px"><input type="checkbox" name="chkFiltro" value="'.htmlentities($oRs[0]).'"/></td>';
//	      	echo '<td></td>';
	      	echo '</tr>';
			if($iCont % 2 == 0) $bgcolor = "#ffffff"; else $bgcolor = "#f1f1f1";
			$iCont++;
	   }
	   echo '</tbody></table></div>';
	}
}

function previewEmail(){
	global $assunto, $chkFiltro, $empresa, $area1, $area2, $ciclo, $chkRel;
	$codUsu=0;
	$codUsu = $chkFiltro[0] != null ? $chkFiltro[0] : 15047;
	$emailGerado = '<p style="text-align:left; width:94%">
						<b><span style="color: black; letter-spacing: -0.35pt; text-decoration: none;">
							Assunto: &nbsp;&nbsp; '.$assunto.'</span></b></p><br/><br/>';
	
	$sql2 = "select NM_USUARIO, NM_SOBRENOME, email from col_usuario WHERE CD_USUARIO = $codUsu";
	$query2 = DaoEngine::getInstance()->executeQuery($sql2,true);
	$usuarioRs = mysql_fetch_row($query2);
	$emailGerado = $emailGerado . gerarEmailInteracaoProgramada($codUsu, $usuarioRs[0]." ".$usuarioRs[1], $usuarioRs[2], $empresa, $area1, $area2, $ciclo, $chkRel);
	echo $emailGerado;
}

function preparaEnvioEmail(){
	global $assunto, $chkFiltro, $empresa, $area1, $area2, $ciclo, $chkRel;
	$i=0;
	$emailsdestinatario=array();
	//for com todos os usuarios para enviar email a cada um.
	foreach ($chkFiltro as $itt){
		$sql2 = "select NM_USUARIO, NM_SOBRENOME, email from col_usuario WHERE CD_USUARIO = '$itt'";
		$query2 = DaoEngine::getInstance()->executeQuery($sql2,true);
		$usuarioRs = mysql_fetch_row($query2);
		
		$emailGerado = gerarEmailInteracaoProgramada($itt, $usuarioRs[0]." ".$usuarioRs[1], $usuarioRs[2], $empresa, $area1, $area2, $ciclo, $chkRel);
//		enviarEmail($emailGerado, $assunto, $usuarioRs[2]);
//		$emailsdestinatario[$i]=$usuarioRs[2];
		$i++;
	}
//	$emailsdestinatario = array("filipeiack@gmail.com", "rafael.lima.net@gmail.com", "iunes@colaborae.com.br");
	$emailsdestinatario = array("filipeiack@gmail.com");
	enviarEmail($emailGerado, $assunto, implode(", ", $emailsdestinatario));
	
//	echo "<b>Mensagem <i>$assunto</i> enviada com sucesso!</b><br>Para: ".implode(", ", $emailsdestinatario)." <br>";
	echo "<b>Mensagem <i>$assunto</i> enviada com sucesso!</b><br>Para: ".$i." destinat&aacute;rio(s)<br>";
	
}

function cabecalhoEmailPadrao($cdEmpresa, $nomeUsuario, $emailUsuario){
	
	$sql = "select DS_EMPRESA from col_empresa WHERE CD_EMPRESA = '$cdEmpresa'";
	$query = DaoEngine::getInstance()->executeQuery($sql,true);
	$nomeProgramaRs = mysql_fetch_row($query);

	$body = '
		<style type="text/css">
			.textblk {FONT-WEIGHT: normal; FONT-SIZE: 8.5px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "tahoma","arial","helvetica","sans-serif"}
		</style>
		<table style="width:500px; border-collapse: collapse;" border="1"
			cellpadding="0" cellspacing="0" width="500">
			<tbody>
				<tr>
					<td style="padding: 0cm;">
					<table style="width: 100%; border-collapse: collapse;" border="0"
						cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td style="background: none repeat scroll 0% 0% rgb(59, 89, 152); padding: 7.5pt 22.5pt 3pt;">
									<p class="MsoNormal"><b><span
										style="color: white; letter-spacing: -0.35pt;">
										<a href="http://'.$_SERVER["SERVER_NAME"].'/canal/index.php" target="_blank">
										<span style="color: white; background: none repeat scroll 0% 0% rgb(59, 89, 152); text-decoration: none;">
											Programa '.htmlentities($nomeProgramaRs[0]).' </span></a></span></b></p>
								</td>
							</tr>
						</tbody>
					</table>
					<table style="width: 100%; border-collapse: collapse;"
						border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td style="padding: 1pt 0cm 5pt 5pt;">
									<p class="MsoNormal"><span style="font-size: 8.5pt;">Ol&aacute; '.htmlentities($nomeUsuario).'</span></p>
								</td>
							</tr> ';
	return $body;
}

function rodapeEmailPadrao($emailUsuario){
	
	$body = '				<tr>
								<td style="padding: 0pt 0cm 0pt 0pt;">
								<table style="width: 100%; border-collapse: collapse;"
									border="0" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td style="border-right: medium none; border-width: 1pt medium medium; border-style: solid none none; border-color: rgb(204, 204, 204) -moz-use-text-color -moz-use-text-color; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% white; padding: 7.5pt;">
											<table style="border-collapse: collapse;" border="0"
												cellpadding="0" cellspacing="0">
												<tbody>
													<tr>
														<td style="padding: 0cm 7.5pt 0cm 0cm;">
														<table style="border-collapse: collapse;" border="0"
															cellpadding="0" cellspacing="0">
															<tbody>
																<tr>
																	<td
																		style="border-width: 1pt; border-style: solid; border-color: rgb(41, 68, 126) rgb(41, 68, 126) rgb(26, 53, 110); -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; background: none repeat scroll 0% 0% rgb(91, 116, 168); padding: 0cm;">
																	<table style="border-collapse: collapse;"
																		border="0" cellpadding="0" cellspacing="0">
																		<tbody>
																			<tr>
																				<td style="border-right: medium none; border-width: 1pt medium medium; border-style: solid none none; border-color: rgb(138, 156, 194) -moz-use-text-color -moz-use-text-color; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; -moz-border-image: none; padding: 3pt 7.5pt 3.75pt;">
																				<p class="MsoNormal">
																					<span style="font-size: 8.5pt;"><a href="http://'.$_SERVER["SERVER_NAME"].'/canal/index.php" target="_blank">
																					<b><span style="color: white; text-decoration: none;">Acesso R&aacute;pido ao Programa</span></b>
																					</a></span>
																				</p>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																	</td>
																</tr>
															</tbody>
														</table>
														</td>
													</tr>
												</tbody>
											</table>
											</td>
										</tr>
									</tbody>
								</table>
								</td>
							</tr>
						</tbody>
					</table>
					<table style="width: 100%; border-collapse: collapse;" border="0"
						cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td style="padding: 7.5pt;">
								<p class="MsoNormal"><span
									style="font-size: 8.5pt; color: rgb(153, 153, 153);">A
								mensagem foi enviada para <a href="mailto:'.$emailUsuario.'"
									target="_blank">'.$emailUsuario.'</a>. E-mail de envio
								autom&aacute;tico. N&atilde;o responda a este email. ';

	$body .= '
								Caso queira
								entrar em contato conosco utilize o <a
									href="http://'.$_SERVER["SERVER_NAME"].'/canal/index.php?m'.base64_encode("tabCont09").'"
									target="_blank"><span
									style="color: rgb(59, 89, 152); text-decoration: none;">Fale
								Conosco</span></a> em nossa &aacute;rea interna ou envie um email para <a
									href="mailto:introducaotecnologica@colaborae.com.br" target="_blank">introducaotecnologica@colaborae.com.br</a>.';

	$body .= '
								<br>
								&copy; Colabor&aelig; '.date("Y").'</span></p>
								</td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
			</tbody>
		</table>';
	
	return $body;
}

function gerarEmailInteracaoProgramada($codUsuario, $nomeUsuario, $emailUsuario, $cdEmpresa, $cabecalho=null, $rodape=null, $ciclo=null, $strChkRel=null){
	
	$arrayRel=array();
	$arrayRel[0][0] = "Desempenho Individual";
	$arrayRel[1][0] = "Grafico de Desempenho";

	$arrayRel[2][0] = "Consolidado";
	$arrayRel[2][1] = "http://".$_SERVER["SERVER_NAME"]."/canal/index.php?m".base64_encode("tabCont07");
	$arrayRel[3][0] = "Diagn�stico";
	$arrayRel[3][1] = "http://".$_SERVER["SERVER_NAME"]."/canal/index.php?m".base64_encode("tabCont07");
	$arrayRel[4][0] = "Aprendizado";
	$arrayRel[4][1] = "http://".$_SERVER["SERVER_NAME"]."/canal/index.php?m".base64_encode("tabCont07");
	$arrayRel[5][0] = "Desempenho";
	$arrayRel[5][1] = "http://".$_SERVER["SERVER_NAME"]."/canal/index.php?m".base64_encode("tabCont07");
	$arrayRel[6][0] = "Certifica��o";
	$arrayRel[6][1] = "http://".$_SERVER["SERVER_NAME"]."/canal/index.php?m".base64_encode("tabCont07");
	
	$emailGerado = cabecalhoEmailPadrao($cdEmpresa, $nomeUsuario, $emailUsuario);
	$emailGerado = $emailGerado . ' <tr>
								<td style="padding: 1pt 0cm 5pt 5pt;">
								<span style="font-size: 8.5pt;">'.$cabecalho.'</span>
								</td>
							</tr>

							<tr>
								<td style="padding: 0cm 0cm 10pt 5pt;">';
								
								$tdTab = 100;
								$tdGr = 100;
								if (in_array("0", $strChkRel) && in_array("1", $strChkRel)) {
									$tdTab = 60;
									$tdGr = 40;
								}
	$emailGerado = $emailGerado . '<table style="width: 99%; border-collapse: collapse;"
									border="0" cellpadding="0" cellspacing="0" width="99%">
									<tbody>
										<tr>';
										if (in_array("0", $strChkRel)) {
											$emailGerado =	$emailGerado . '
											<td style="width:'.$tdTab.'%; border-width: 1pt medium; border-style: solid none; border-color: rgb(204, 204, 204) -moz-use-text-color; background: none repeat scroll 0% 0% rgb(242, 242, 242); padding: 7.5pt;">' .gerarRadarParticipante($cdEmpresa, count($ciclo)==1 ? $ciclo[0] : -1, $codUsuario).'
											</td>';
										}
										
										if (in_array("1", $strChkRel)) {
	$emailGerado = $emailGerado . '			<td style="width:'.$tdGr.'%; border-width: 1pt medium; border-style: solid none; border-color: rgb(204, 204, 204) -moz-use-text-color; padding: 7.5pt;">
												<img src="https://chart.googleapis.com/chart?cht=rs&chs=200x300&chd=t:69.80,71.67,76&chxt=x&chxl=0:|Empresa|Grupo|Participante&chdl=certificacoes&chdlp=t&chdlp=bv"/>
											</td>';
										}
	$emailGerado = $emailGerado . '		</tr>
									</tbody>
								</table>
								</td>
							</tr>
							<tr>
								<td style="padding: 1pt 0cm 5pt 5pt;">
								<span style="font-size: 8.5pt;">'.$rodape.'</span>
								</td>
							</tr>';
	$emailGerado = $emailGerado . rodapeEmailPadrao($emailUsuario);
	return $emailGerado;
}


function enviarEmail($mensagem, $assunto, $emaildestinatario){
	 
	/* Medida preventiva para evitar que outros dom�nios sejam remetente da sua mensagem.
	if (eregi('tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$', $_SERVER[HTTP_HOST])) {
	        $emailsender='colaborae@colaborae.com.br'; // Substitua essa linha pelo seu e-mail@seudominio
	} else {
	        $emailsender = "colaborae@" . $_SERVER[HTTP_HOST];
	}
	*/
	 
	/* Verifica qual � o sistema operacional do servidor para ajustar o cabe�alho de forma correta.  */
	if(PATH_SEPARATOR == ";") $quebra_linha = "\r\n"; //Se for Windows
	else $quebra_linha = "\n"; //Se "nao for Windows"
	 
	// Passando os dados obtidos pelo formul�rio para as vari�veis abaixo
	$nomeremetente		= "Programa de Introdu��o Tecnol�gica";
	$emailsender		= "introducaotecnologica@colaborae.com.br";
	$emailremetente		= "introducaotecnologica@colaborae.com.br";
//	$comcopiaoculta		= $_POST['comcopiaoculta'];
	 
	 
	/* Montando a mensagem a ser enviada no corpo do e-mail. */
	$mensagemHTML = $mensagem.'<hr>';
	 
	 
	/* Montando o cabecalho da mensagem */
	$headers = "MIME-Version: 1.1" .$quebra_linha;
	$headers .= "Content-type: text/html; charset=iso-8859-1" .$quebra_linha;
	$headers .= "From: " . $emailsender.$quebra_linha;
//	$headers .= "Cc: " . $comcopia . $quebra_linha;
//	$headers .= "Bcc: " . $comcopiaoculta . $quebra_linha;
	$headers .= "Reply-To: " . $emailremetente . $quebra_linha;
	 
	/* Enviando a mensagem 
	 * � obrigat�rio o uso do par�metro -r (concatena��o do "From na linha de envio"), aqui na Locaweb:
	 * */
	if(!mail($emaildestinatario, $assunto, $mensagemHTML, $headers ,"-r".$emailsender)){ // Se for Postfix
	    $headers .= "Return-Path: " . $emailsender . $quebra_linha; // Se "n�o for Postfix"
	    mail($emaildestinatario, $assunto, $mensagemHTML, $headers );
	}
}

function salvarAgenda(){

	instanciarRN();
	carregarRN();
	page::$rn->QUERY_SQL = obterQueryFiltrarUsuarios();
	page::$rn = page::$rn->inserir();
	echo '<br/><b>Agendamento realizado com sucesso!</b><br/><br/>';
//	obterAgenda();	
}

function obterAgenda(){
	$iCont   = 0;
	$bgcolor = "#ffffff";
	
	
	$sql = " select ds_programa, ds_assunto, ds_texto_header, ds_texto_footer, dt_inicio_vigencia, hora_envio, in_realizado from col_interacao_programada ORDER BY ds_programa ";
	$query = DaoEngine::getInstance()->executeQuery($sql,true);
	
	if(mysql_num_rows($query) != 0){
		echo '<table id="tabelaCadastro" class="textblk">
		<thead><tr><th>Programa</th><th>Assunto</th><th>Texto Cabe�alho</th><th>Texto Rodap�</th><th>Data Programada</th><th>Realizado</th></tr></thead><tbody>';
		while($oRs = mysql_fetch_row($query)){
			echo "<tr bgcolor=\"$bgcolor\">";
	      	echo '<td class="textblk" style="padding-left:20px">'.$oRs[0].'</td>';
	      	echo '<td class="textblk" style="padding-left:20px">&nbsp;'.$oRs[1].'</td>';
	      	echo '<td class="textblk" style="padding-left:20px">&nbsp;'.$oRs[2].'</td>';
	      	echo '<td class="textblk" style="padding-left:20px">&nbsp;'.$oRs[3].'</td>';
	      	echo '<td class="textblk" style="padding-left:20px">'.$oRs[4].' '.$oRs[5].'</td>';
	      	echo "<td class=\"textblk\" style=\"padding-left:20px;text-align:center\">".($oRs[5] == "1" ? "sim" : "nao" )."</td></tr>";
			if($iCont % 2 == 0) $bgcolor = "#ffffff"; else $bgcolor = "#f1f1f1";
			$iCont++;
	   }
	   echo '</tbody></table><br/><br/>';
	}
	
}

function updateAgendaRealizado($oID){
	global $searchString;
	$sql = "UPDATE col_interacao_programada SET IN_REALIZADO = 1, DT_REALIZADO = sysdate() 
			WHERE CD_INTERACAO_PROGRAMADA = ". $oID;

	if(!($RS_query = mysql_query($sql)))
	{
		echo $_SESSION["msg"] = ERROR_MSG_SQLQUERY . mysql_error();
	}
}

class FiltroNewsLetter{
	
	public $empresa;
	public $ciclo;
    public $prova; 
    public $lot; 
    public $cargo;
    public $tipoUsuario;
    public $txtNotaInicio; 
    public $txtNotaTermino;
    public $usuariosEnviar;
	
}
?>