<?
include "../../include/security.php";
include "../../include/defines.php";
include "../framework/crud.php";
include "../../include/genericfunctions.php";
include "../../admin/controles.php";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>Colabor&aelig; - Consultoria e Educa��o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="../include/css/adminstyles.css">
<!-- IE6 "fix" for the close png image -->
<!--[if lt IE 7]>
<link type='text/css' href='../include/css/basic_ie.css' rel='stylesheet' media='screen' />
<![endif]-->
<style type="text/css">
    <!--
	.all {
		overflow:hidden;
		height:auto;
		width:99%;
		margin:auto;
	}
	#container{
		width: 1000px;             
		height:auto;
		margin:auto;
		vertical-align:top; 
	}
	#loading {
		position: absolute;
		overflow:auto;
		width: 100px;             
		height:80px;
		background-color:#fff;
		border: 1px solid #eee;
        left: 45%;
        top: 90%;
		display:none;
	}
	#resultadoFiltro{
		overflow:auto;
		width:95%;
		margin-left:auto;
		margin-right:auto;
	}
	#tabelaCadastro td
	{
		font-size:10px;
		font-family:tahoma,arial,helvetica,sans-serif;
		border: 1px inset #eee;
		padding:2px 5px 2px 5px;
	}
	#tabelaCadastro th
	{
		text-align:left;
		font-size:11px;
		font-family:tahoma,arial,helvetica,sans-serif;
		background-color:#ccc;
		padding:2px 5px 2px 5px;
	}
	div.scrollTable table.header, div.scrollTable div.scroller table{
	    width: 100%;
	    border-collapse: collapse;
	}
	div.scrollTable div.scroller{
	    height: 300px;
	    overflow: auto;
	}

	DIV.selHolder { width:238px; height:140px; float:left; border:2px solid #ccc; margin:2px; padding:0px; }
	DIV.selHolder2 { width:480px; height:528px; float:left; border:2px solid #ccc; margin:2px; padding:0px; }
	FIELDSET { border: 1px solid #ccc; padding: 1em; margin: 0; }
	LABEL { display: block; margin-top: 10px; color:#000;} 
	LABEL.erro { color: #ff0000;} 
	SELECT { margin-top: 10px; margin-bottom:0px; margin-left:0px; margin-right:0px; width: 225px; }
	INPUT, TEXTAREA { margin-top: 10px; margin-bottom:0px; margin-left:10px; margin-right:5px; color:#000;}
	.textblk {FONT-WEIGHT: normal; FONT-SIZE: 9px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica","sans-serif"}
	.textCab{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000}

	#divMsg {color: #ff0000; font-weight: bold;}
    #basic-modal-content, #agenda-modal {overflow:auto; display:none; margin: auto; padding: 0;}
	#simplemodal-overlay {background-color:#ddd; cursor:wait;}
	#simplemodal-container {height:500px; max-height:500px; width:600px; color:#bbb; background-color:#fff; border:4px solid #444; padding:5px; margin: auto;}
	#simplemodal-container a.modalCloseImg {background:url(../images/x.png) no-repeat; width:25px; height:29px; display:inline; z-index:3200; position:absolute; top:-15px; right:-16px; cursor:pointer;}
    -->
    <!--
	#box-table-a
	{
		width: 100%;
		text-align: left;
		border-collapse: collapse;
	}
	#box-table-a th
	{
		font-size: 13px;
		font-weight: normal;
		padding: 8px;
		background: #b9c9fe;
		border-top: 4px solid #aabcfe;
		border-bottom: 1px solid #fff;
		color: #039;
	}
	#box-table-a td
	{
		padding: 8px;
		background: #e8edff; 
		border-bottom: 1px solid #fff;
		color: #669;
		border-top: 1px solid transparent;
	}
	#box-table-a tr:hover td
	{
		background: #d0dafd;
		color: #339;
	}
	-->
</style>

<script type="text/javascript" src="../../include/js/jquery-1.5.2.min.js"></script>
<script type='text/javascript' src='../../include/js/jquery.simplemodal.js'></script>

<script type="text/javascript" src="../include/js/functions.js"></script>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../ckeditor/adapters/jquery.js"></script>
<script language="JavaScript" src="../include/js/adminfunctions.js"></script>

<script language="JavaScript">

	$(document).bind("ajaxStart", function(){ $('#loading').show(); }).bind("ajaxStop", function(){ $('#loading').hide(); });
	
	$(document).ready(function(){

		$('textarea').ckeditor(function() { /* callback code */ }, { skin:'office2003', enterMode:CKEDITOR.ENTER_BR /*, toolbar:'Basic' ) */ });
		
        $("select[name=empresa]").change(function(){
        	$("#empresaLabel").html('Carregando...');
            
            $.post("newsLetterControls.php", 
                  {empresa:$(this).val()},
                  function(valor){
					if(valor == "::::"){
						$("#empresaLabel").html('N�o existem ciclos nesta empresa.');
						$("#btnFiltro").hide("slow");
					}else{
						$("#btnFiltro").show("slow");
						$("#divCiclo").show("slow");
						$("#divLotacao").show("slow");
						$("#divCargo").show("slow");
						$("#empresaLabel").html('');
					}
					valorArray = valor.split('::');
					$("select[name=ciclo]").html(valorArray[0]);
					$("select[name=lot]").html(valorArray[1]);
					$("select[name=cargo]").html(valorArray[2]);
					$("select[name=prova]").html('');
                  }
                  );
            
         });
         
         $("select[name=ciclo]").change(function(){
        	 $("#cicloLabel").html('Carregando...');
             
             $.post("newsLetterControls.php", 
                   {ciclo:$(this).val()},
                   function(valor){
                	   if(valor == ""){
							$("#cicloLabel").html('N�o existem provas para o(s) ciclo(s).');
                        }else{
		                    $("#divProva").show("slow");
                     		$("#cicloLabel").html('');
                        }
						$("select[name=prova]").html(valor);
                   }
                   );
          });
         
        $("select[name=prova]").change(function(){
			$("#divNota").show("slow");
		});

        $(".mySubmit").click(function(event){
                event.preventDefault();
                var empresa = $("#empresa").val();
                if ((empresa == null) || (empresa == -1)){
                	alert("Selecione um programa para executar esta a��o.");
                	return;
                }
                var ciclo=new Array();
                $("#ciclo option:selected").each(function(i) {
            		ciclo[i] = $(this).val();
            	});
                if ((ciclo == null) || (ciclo == "")){
                	alert("Selecione ao menos um ciclo para executar esta a��o.");
                	return;
                }

//				$("#divMsg").html('<b>Aguarde...</b>');

                var prova=new Array();
                $("#prova option:selected").each(function(i) {
                	prova[i] = $(this).val();
            	});
            	
                var lot=new Array();
                $("#lot option:selected").each(function(i) {
                	lot[i] = $(this).val();
            	});
                
                var cargo=new Array();
                $("#cargo option:selected").each(function(i) {
                	cargo[i] = $(this).val();
            	});
            	
                var txtCabecalho = $("#txtCabecalho").val();
                var txtRodape = $("#txtRodape").val();
                var assunto = $("#assunto").val();
                var txtNotaInicio = $("#txtNotaInicio").val();
                var txtNotaTermino = $("#txtNotaTermino").val();
                var tipoUsuario = $("#tipoUsuario").val();
                var txtDataInicioVigencia = $("#txtDataInicioVigencia").val();
                var txtDataTerminoVigencia = $("#txtDataTerminoVigencia").val();
                var txtHoraAgenda = $("#txtHoraAgenda").val();
				var mode = $(this).attr("id");
				
                var chkRel=new Array();
                $("input[type=checkbox][name=chkRel][checked]").each( 
            	    function(i) {
            	    	chkRel[i] = $(this).val();
            	    } 
            	);
        		
                var chkTUsu=new Array();
                $("input[type=checkbox][name=chkTUsu][checked]").each( 
            	    function(i) {
            	    	chkTUsu[i] = $(this).val();
            	    } 
            	);
        		
                var chkFiltro=new Array();
                $("input[type=checkbox][name=chkFiltro][checked]").each( 
            	    function(i) {
            	    	chkFiltro[i] = $(this).val();
            	    } 
            	);
        		
                var chkDia=new Array();
                $("input[type=checkbox][name=chkDia][checked]").each( 
            	    function(i) {
            	    	chkDia[i] = $(this).val();
            	    } 
            	);
        		
                $.post("newsLetterControls.php", 
                        {pageMode:mode, empresa:empresa, ciclo:ciclo, 
                    	prova:prova, lot:lot, cargo:cargo, txtCabecalho:txtCabecalho, txtRodape:txtRodape, assunto:assunto,
                    	txtNotaInicio:txtNotaInicio, txtNotaTermino:txtNotaTermino, tipoUsuario:tipoUsuario,
                     	chkRel:chkRel, chkTUsu:chkTUsu, chkFiltro:chkFiltro, chkDia:chkDia, txtDataTerminoVigencia:txtDataTerminoVigencia,
                     	txtDataInicioVigencia:txtDataInicioVigencia,txtHoraAgenda:txtHoraAgenda},
                        function(json){
                     		$("#divMsg").html('');
                            if (mode == "btnPreview"){
                            	$("#basic-modal-content").html(json);
          						$("#basic-modal-content").modal();
                            }else if (mode == "btnFiltrar"){
     							$("#resultadoFiltro").html(json);
     							$("#areaDados").show("slow");
                            }else if (mode == "btnAgendar"){
                            	$("#agenda-modal").html(json);
                            }else{
     							$("#divMsg").html(json);
                            }
                        }
                        );
                
                return false;
         });
         
        $("#btnOpenAgenda").click(function(event){
			$("#agenda-modal").modal();
        });

        
	});

	function marcaTodos(t) {
		var vlw = t.checked;
        $("input[type=checkbox][name=chkFiltro]").each( 
              function(i) {
              	$(this).attr("checked", vlw);
              } 
          );
	}
</script>

</head>

<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td>&nbsp;</td></tr>
<tr>
<td background="../../images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="../../images/layout/logo_admin.png">
	<tr>
	<td height="32">&nbsp;</td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td height="2"></td></tr>
<tr><td bgcolor="#cccccc" height="3"></td></tr>
</table>
<table class="home" id="filtroTable">
<tr>
	<td width="30%" height="10"></td>
	<td width="35%"></td>
	<td width="35%"></td>
</tr>
<tr valign="bottom">
	<td class="textCab"><strong>USU�RIO:&nbsp;</strong><? echo $_SESSION["alias"]; ?></td>
	<td align="center">
		<input id="btnConsultarAgenda" class="buttonsty" value="Consultar Agenda" onclick="document.location.href='/admin/admin_interacao.php'" type="button" onfocus="noFocus(this)"/>
	</td>
	<td align="right">
		<input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('/admin/logout.php')" onfocus="noFocus(this)">
		<input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='/admin/admin_menu.php'" onfocus="noFocus(this)">
	</td>
</tr>
<tr><td colspan="3" class="textCab"><strong>P�GINA ATUAL:&nbsp;</strong>Intera��o Programada</td></tr>
<tr><td class="tarjaTitulo" colspan="3">Intera��o Programada</td></tr>
</table>
<form id="filtroForm" action="" method="post">

<div class="all">
    <div id="container">
    	<div class="selHolder2">
			<label>Assunto Email:</label>
	      	<input name="assunto" type="text" class="textbox3" id="assunto" value="Intera��o Programada">
			<hr style="width:90%; color:#000;">
			<label>Texto a ser inserido no cabe�alho do email:</label>
			<textarea name="txtCabecalho" id="txtCabecalho">
			</textarea>
			
			<?php //criarHtmlEditor("txtCabecalho"); ?>
			<?php //textarea("txtCabecalho", "500", "", "", "80px", "textbox3", "Area 1"); ?>
		<!--	Comentado por enquanto: 
				<input id="btnFiltro" style="display:none; margin-top: 10px;" class="buttonsty" type="submit" value="Pesquisar"></input>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		-->
		</div>

    	<div class="selHolder2">
			<label>Relat�rios no corpo email:</label>
			<input type="checkbox" name="chkRel" value="0" checked="checked"/>Desempenho Individual
			<input type="checkbox" name="chkRel" value="1"/>Grafico de Desempenho
			<hr style="width:90%; color:#000;">
			<label>Texto a ser inserido no rodap� do email:</label>
			<textarea name="txtRodape" id="txtRodape">
			</textarea>
			<?php //criarHtmlEditor("txtRodape"); ?>
<!--			<input type="checkbox" name="chkRel" value="2"/>Relat�rios Consolidado-->
<!--			<input type="checkbox" name="chkRel" value="3"/>Resultado do Diagn�stico-->
<!--			<input type="checkbox" name="chkRel" value="4"/>Aprendizado-->
<!--			<input type="checkbox" name="chkRel" value="5"/>Desempenho-->
<!--			<input type="checkbox" name="chkRel" value="6"/>Certifica��o-->
		</div>
		
        <div class="selHolder">
         	<label>Programa</label>
         	<?php comboboxEmpresa("empresa", null, true, false, false, "textboxbase"); ?>
         	<label>Tipo de Usu�rio:</label>
         	<input type="checkbox" name="chkTUsu" value="SU" checked="checked"/>Super-usu�rios
			<input type="checkbox" name="chkTUsu" value="P" checked="checked"/>Participantes
         	<?php //comboboxTipoUsuario("tipoUsuario",null,false,"textboxbase",true); ?>
			<label class="erro" id="empresaLabel">&nbsp;</label>
		</div>
		
        <div id="divCiclo" class="selHolder" style="display:">
			<label>Escolha o(s) Ciclo(s)</label>
			<SELECT id="ciclo" name="ciclo" size="5" multiple="multiple" class="textboxbase"></SELECT>
			<label class="erro" id="cicloLabel">&nbsp;</label>
	    </div>
		
		<div id="divLotacao" class="selHolder" style="display:">
			<label>Escolha a(s) Lota��o(�es)</label>
			<SELECT id="lot" name="lot" size="5" multiple="multiple" class="textboxbase"></SELECT>
			<label class="erro" id="lotLabel">&nbsp;</label>
		</div>

		<div id="divCargo" class="selHolder" style="display:">
			<label>Escolha o(s) Cargo(s)</label>
			<SELECT id="cargo" name="cargo" size="5" multiple="multiple" class="textboxbase"></SELECT>
			<label class="erro" id="cargoLabel">&nbsp;</label>
		</div>
		
		<div id="divProva" class="selHolder" style="display:none">
			<label>Escolha a(s) Prova(s)</label>
			<SELECT id="prova" name="prova" size="5" multiple="multiple" class="textboxbase"></SELECT>
			<label class="erro" id="provaLabel">&nbsp;</label>
		</div>
		
		<div id="divNota" class="selHolder" style="display:none">
			<label>Escolha um intervalo de notas</label>
			de <?php textbox("txtNotaInicio", "2", "", "60px", "textboxbase", "In�cio Nota", Mascaras::Numero) ?>
			at� <?php textbox("txtNotaTermino", "2", "", "60px", "textboxbase", "T�rmino Nota", Mascaras::Numero) ?>
			<label class="erro" id="notaLabel">&nbsp;</label>
		</div>

		<div id="divButton" style="width:100%;float:left;margin:auto;">
			<input class="buttonsty" type="reset" name="Reset" value="Limpar" >
			<input id="btnFiltrar" style="margin-top:10px auto;" class="buttonsty mySubmit" type="button" value="Pesquisar"/>
		</div>
		
	</div>
</div>
	<br>
	<div id="divMsg"></div>
	<div id="loading"><br><img src='../images/carregando.gif' alt=''/><br>Carregando...</div>
	
	<div id="areaDados" class="all" style="display:none;">
			<input type="hidden" name="hdnParticipante"	id="hdnParticipante" value="" />
			<div id="resultadoFiltro" class="scrollTable"></div>
			
			<input id="btnPreview" type="button" class="buttonsty mySubmit" value="Preview" onfocus="noFocus(this)"/>
<!--		<input id="btnEnviar" style="margin-top: 10px;" class="buttonsty mySubmit" type="submit" value="Enviar Email" onfocus="noFocus(this)"/> -->
			<input id="btnOpenAgenda" class="buttonsty" value="Criar Agenda" type="button" onfocus="noFocus(this)"/>
	</div>
	
</form>

<!--	Comentado por enquanto: 
	<span class="tarjaTitulo" style="width:756;">Usu�rios a enviar - Listagem por Ordem Alfab�tica</span>
	<form name="homeForm" action="" method="post">
	</form>
-->

	<div id="agenda-modal">
		<label>Agenda para Maquina de Intera��o Programada:</label>
		<label>Data:
		<?php textbox("txtDataInicioVigencia", "10", null, "110px", "textbox3", "Data Inicio", Mascaras::Data) ?> (dd/mm/aaaa)
		Hora: <?php textbox("txtHoraAgenda", "5", null, "110px", "textbox3", "Hor�rio de In�cio", Mascaras::Hora) ?> (hh:mm)
		<!--
		a <?php //textbox("txtDataTerminoVigencia", "10", "", "110px", "textbox3", true, Mascaras::Data) ?>
		-->
		</label>
		
		<div style="width:35%; height:290px; float:left;"></div>
		<div style="width:30%; height:auto; float:left; text-align: left;">
						<!--<label>Hora de execu��o</label>
						<label><input type="checkbox" name="chkDia" value="segunda" checked="checked"/>Todas as segundas</label>
						<label><input type="checkbox" name="chkDia" value="terca"/>Todas as ter�as</label>
						<label><input type="checkbox" name="chkDia" value="quarta"/>Todas as quartas</label>
						<label><input type="checkbox" name="chkDia" value="quinta"/>Todas as quintas</label>
						<label><input type="checkbox" name="chkDia" value="sexta"/>Todas as sextas</label>
						<label><input type="checkbox" name="chkDia" value="sabado"/>Todos os s�bados</label>
						<label><input type="checkbox" name="chkDia" value="domingo"/>Todos os domingos</label>
		-->
		<input id="btnAgendar" class="buttonsty mySubmit" value="Gravar Agenda" type="button" onfocus="noFocus(this)"/>
		</div>
		<div style="width:35%; height:290px; float:left;"></div>
	</div>

	<!-- popup modal content -->
	<div id="basic-modal-content"></div>
	<!-- preload the images -->
	<div style='display:none'>
		<img src='../images/x.png' alt='' />
	</div>
<br>
</body>
</html>