<?php

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_prova();
}

function pagePreRender(){
	
	page::$atributos = "<style>

	li { list-style: none }
	
	.nodeH{ display: none }
	
	.nodeS{ display: block }

</style>";

    page::$validador = "javascript:tipoElementoFimValidacao='checkbox';return VerificarPreenchimentoObrigatorioTag();";
	
	page::$script = "
	
	function expandCollapse(obj){
	
		if(obj.parentElement.children.length <= 2){
			return;
		}
	
		if(obj.parentElement.children(2).className == 'nodeH'){
			obj.parentElement.children(2).className = 'nodeS';
			obj.src = 'minus_r.gif';
		}else{
			obj.parentElement.children(2).className = 'nodeH'
			obj.src = 'plusik_r.gif';
		}
	}
	
	function marcar(obj){
		
		var y = 0;
		if (obj.parentElement.children.length > 2){
				
			var itemAbaixo = obj.parentElement.children(2).children.length;
			
			for (y = 0; y < itemAbaixo; y++){
			
				obj.parentElement.children(2).children(y).children(1).checked = obj.checked;
				marcar(obj.parentElement.children(2).children(y).children(1));
				
			}
			
		}
	}	
";
	
}

function carregarRN(){
	
	page::$rn->CD_PROVA = $_REQUEST["id"];
	page::$rn->DS_PROVA = $_POST["txtProva"];
	page::$rn->DT_REALIZACAO = $_POST["txtDataRealizacao"];
	page::$rn->DT_FIM_REALIZACAO = $_POST["txtDataRealizacaoFim"];
	page::$rn->DURACAO_PROVA = $_POST["txtDuracao"];
	page::$rn->HORARIO_FIM_DISPONIVEL = "{$_POST['txtDataRealizacaoFim']} {$_POST['txtHorarioTermino']}";
	page::$rn->HORARIO_INICIO_DISPONIVEL = "{$_POST['txtDataRealizacao']} {$_POST['txtHorarioInicio']}";
	page::$rn->IN_DIFICULDADE = $_POST["cboDificuldade"];
	page::$rn->NR_QTD_PERGUNTAS_PAGINA = $_POST["txtPaginaPergunta"];
	page::$rn->VL_MEDIA_APROVACAO = $_POST["txtMedia"];
    page::$rn->IN_PROVA = $_POST["rdoSimulado"];
    
    //page::$rn->NR_QTD_PERGUNTAS_NIVEL_1 = $_POST["txtQtdPerguntaNivel1"];
    //page::$rn->NR_QTD_PERGUNTAS_NIVEL_2 = $_POST["txtQtdPerguntaNivel2"];
    //page::$rn->NR_QTD_PERGUNTAS_NIVEL_3 = $_POST["txtQtdPerguntaNivel3"];
    //page::$rn->CD_DISCIPLINA = $_POST["cboDisciplina"];
    
    
    page::$rn->NR_TENTATIVA = $_POST["txtQuantidadeTentativa"];
    page::$rn->IN_GABARITO = $_POST["rdoGabarito"];
    page::$rn->IN_OBS = $_POST["rdoObs"];
    page::$rn->IN_SENHA_MESTRA = $_POST["rdoSenha"];
    page::$rn->CD_CICLO = $_POST["cboCiclo"];

    //page::$rn->cargo = $_POST["cboCargo"];
    page::$rn->cargo = '';

    $cargos = array();

    if (is_array($_REQUEST["cboCargo"]))
    {
        foreach ($_REQUEST["cboCargo"] as $cargo)
        {

            $cargoFuncao = new col_prova_cargo();
            $cargoFuncao->NM_CARGO_FUNCAO = $cargo;
            $cargoFuncao->CD_PROVA = page::$rn->CD_PROVA;

            $cargos[] = $cargoFuncao;
        }
    }

    page::$rn->cargos = $cargos;

//    if ($_POST["cboCargo"] == '-1') {
//    	page::$rn->cargo = '';
//    }
    
    $empresas = array();
    $lotacoes = array();
    $usuarios = array();
    
    foreach($_POST as $key => $valor){
		//echo "$key $valor \n";
		if (strstr($key, "chkEmpresa") != ""){
			$empresas[] = $valor;
		}elseif (strstr($key, "chkLotacao") != ""){
			$lotacoes[] = $valor;
		}elseif(strstr($key, "chkUsuario") != ""){
			$usuarios[] = $valor;
		}
    }
    
    
    
	page::$rn->usuariosCadastrados = $usuarios;
	page::$rn->empresasCadastradas = $empresas;
	page::$rn->lotacoesCadastradas = $lotacoes;
    
	//echo count(page::$rn->usuariosCadastrados);
	
	//$codigoUsuarios = $_POST["listUsuario"];
	
	//$codigoUsuarios = split(";", $codigoUsuarios);
	
	//if (is_array($codigoUsuarios)){
		
	//	for ($i = 0; $i < count($codigoUsuarios); $i++){
	//		$usuario = new col_prova_usuario();
	//		$usuario->CD_USUARIO = $codigoUsuarios[$i];
	//		$codigoUsuarios[$i] = $usuario;
	//	}
	//	
	//}
	
	//page::$rn->usuarios = $codigoUsuarios;
	
	//echo $_POST["cboDisciplina"];
	
	
	$codigoDisciplinas = $_POST["cboDisciplina"];
	$perguntasNivel1 = $_POST["txtQtdPerguntaNivel1"];
	$perguntasNivel2 = $_POST["txtQtdPerguntaNivel2"];
	$perguntasNivel3 = $_POST["txtQtdPerguntaNivel3"];
    $perguntasNivel4 = $_POST["txtQtdPerguntaNivel4"];
    $perguntasNivel5 = $_POST["txtQtdPerguntaNivel5"];
    $perguntasNivel6 = $_POST["txtQtdPerguntaNivel6"];
    $perguntasNivel7 = $_POST["txtQtdPerguntaNivel7"];
    $perguntasNivel8 = $_POST["txtQtdPerguntaNivel8"];
    $perguntasNivel9 = $_POST["txtQtdPerguntaNivel9"];
    $perguntasNivel10 = $_POST["txtQtdPerguntaNivel10"];
    $perguntasNivel11 = $_POST["txtQtdPerguntaNivel11"];
    $perguntasNivel12 = $_POST["txtQtdPerguntaNivel12"];
    $perguntasNivel13 = $_POST["txtQtdPerguntaNivel13"];
    $perguntasNivel14 = $_POST["txtQtdPerguntaNivel14"];
    $perguntasNivel15 = $_POST["txtQtdPerguntaNivel15"];
    $perguntasNivel16 = $_POST["txtQtdPerguntaNivel16"];
    $perguntasNivel17 = $_POST["txtQtdPerguntaNivel17"];
    $perguntasNivel18 = $_POST["txtQtdPerguntaNivel18"];
    $perguntasNivel19 = $_POST["txtQtdPerguntaNivel19"];
    $perguntasNivel20 = $_POST["txtQtdPerguntaNivel20"];
	
	$totalPerguntas = 0;
	
	$disciplinas = array();
	
	for ($i = 0; $i < count($codigoDisciplinas); $i++){

		$disciplina = new col_prova_disciplina();
		$disciplina->CD_DISCIPLINA = $codigoDisciplinas[$i];
		$disciplina->NR_QTD_PERGUNTAS_NIVEL_1 = $perguntasNivel1[$i];
		$disciplina->NR_QTD_PERGUNTAS_NIVEL_2 = $perguntasNivel2[$i];
		$disciplina->NR_QTD_PERGUNTAS_NIVEL_3 = $perguntasNivel3[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_4 = $perguntasNivel4[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_5 = $perguntasNivel5[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_6 = $perguntasNivel6[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_7 = $perguntasNivel7[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_8 = $perguntasNivel8[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_9 = $perguntasNivel9[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_10 = $perguntasNivel10[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_11 = $perguntasNivel11[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_12 = $perguntasNivel12[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_13 = $perguntasNivel13[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_14 = $perguntasNivel14[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_15 = $perguntasNivel15[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_16 = $perguntasNivel16[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_17 = $perguntasNivel17[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_18 = $perguntasNivel18[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_19 = $perguntasNivel19[$i];
        $disciplina->NR_QTD_PERGUNTAS_NIVEL_20 = $perguntasNivel20[$i];
		
		$totalPerguntas = $totalPerguntas + $disciplina->NR_QTD_PERGUNTAS_NIVEL_1 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_2 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_3 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_4 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_5 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_6 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_7 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_8 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_9 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_10 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_11 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_12 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_13 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_14 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_15 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_16 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_17 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_18 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_19 +
                            $disciplina->NR_QTD_PERGUNTAS_NIVEL_20;

            $disciplinas[] = $disciplina;
		
	}
	
	page::$rn->NR_QTD_PERGUNTAS_TOTAL = $totalPerguntas;
	page::$rn->disciplinas = $disciplinas;
	
	
	$datasInicio = $_POST["txtDataRealizacao"];
	$datasTermino = $_POST["txtDataRealizacaoFim"];
	$horariosInicio = $_POST['txtHorarioInicio'];
	$horariosTermino = $_POST['txtHorarioTermino'];

	$chamadas = array();
	
	for ($i = 0; $i < count($datasInicio); $i++){

		$chamada = new col_prova_chamada();
		$chamada->DT_INICIO_REALIZACAO = $datasInicio[$i];
		$chamada->DT_TERMINO_REALIZACAO = $datasTermino[$i];
		$chamada->HR_INICIO_REALIZACAO = "{$datasInicio[$i]} {$horariosInicio[$i]}";
		$chamada->HR_TERMINO_REALIZACAO = "{$datasTermino[$i]} {$horariosTermino[$i]}";
		$chamada->IN_PRIMEIRA_CHAMADA = ($i == 0? 1: 2);
		
		$chamadas[] = $chamada;
		
	}
	
	page::$rn->chamadas = $chamadas;

}

function pageRenderEspecifico(){
	
?>				<TR>
				    <TD>
				        <table cellpadding="0" cellspacing="0" border="0">
					        <TR>
								<TD class="textblk" colspan="3"><a href="javascript:adicionarDisciplina()">Adicionar Disciplinas</a></TD>
							</TR>
							<TR>
								<TD colspan="3">
									<div id="dvDisciplinas">
										<?php
											$linhas = 1;
											$disciplinas = page::$rn->disciplinas;
										
											if (is_array($disciplinas)){
												if (count($disciplinas) > 1)
													$linhas = count($disciplinas);
											}
											
											for ($i = 0; $i < $linhas; $i++){
												$codigoDisciplina = 0;
												$quantidadeNivel1 = "";
												$quantidadeNivel2 = "";
												$quantidadeNivel3 = "";
                                                $quantidadeNivel4 = "";
                                                $quantidadeNivel5 = "";
                                                $quantidadeNivel6 = "";
                                                $quantidadeNivel7 = "";
                                                $quantidadeNivel8 = "";
                                                $quantidadeNivel9 = "";
                                                $quantidadeNivel10 = "";
                                                $quantidadeNivel11 = "";
                                                $quantidadeNivel12 = "";
                                                $quantidadeNivel13 = "";
                                                $quantidadeNivel14 = "";
                                                $quantidadeNivel15 = "";
                                                $quantidadeNivel16 = "";
                                                $quantidadeNivel17 = "";
                                                $quantidadeNivel18 = "";
                                                $quantidadeNivel19 = "";
                                                $quantidadeNivel20 = "";
												
												if (is_array($disciplinas)){
													$disciplina = $disciplinas[$i];
													$codigoDisciplina = $disciplina->CD_DISCIPLINA;
													$quantidadeNivel1 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_1;
													$quantidadeNivel2 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_2;
													$quantidadeNivel3 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_3;
                                                    $quantidadeNivel4 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_4;
                                                    $quantidadeNivel5 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_5;
                                                    $quantidadeNivel6 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_6;
                                                    $quantidadeNivel7 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_7;
                                                    $quantidadeNivel8 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_8;
                                                    $quantidadeNivel9 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_9;
                                                    $quantidadeNivel10 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_10;
                                                    $quantidadeNivel11 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_11;
                                                    $quantidadeNivel12 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_12;
                                                    $quantidadeNivel13 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_13;
                                                    $quantidadeNivel14 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_14;
                                                    $quantidadeNivel15 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_15;
                                                    $quantidadeNivel16 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_16;
                                                    $quantidadeNivel17 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_17;
                                                    $quantidadeNivel18 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_18;
                                                    $quantidadeNivel19 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_19;
                                                    $quantidadeNivel20 = $disciplina->NR_QTD_PERGUNTAS_NIVEL_20;
												}
											
										?>
									
										<div id="dvDisciplina" style="border: 1px solid black; margin-top: 2px;">
											<table cellpadding="1" cellspacing="0" width="98%">
												<tr>
													<td colspan="10" class="textblk" >Disciplina:</td>
													<td rowspan="4" class="textblk" align="center"><input type="image" src="/images/layout/bt_delsession.gif" value="" onclick="javascript:removerDisciplina(this); return false;" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'"></td>
												</tr>												
												<tr>
													<td colspan="10" class="textblk" ><?php comboboxDisciplina("cboDisciplina[]", $codigoDisciplina, "Disciplina"); ?></td>
												</tr>
												<tr>
													<td class="textblk" >N1</td>
													<td class="textblk" >N2</td>
													<td class="textblk" >N3</td>
                                                    <td class="textblk" >N4</td>
                                                    <td class="textblk" >N5</td>
                                                    <td class="textblk" >N6</td>
                                                    <td class="textblk" >N7</td>
                                                    <td class="textblk" >N8</td>
                                                    <td class="textblk" >N9</td>
                                                    <td class="textblk" >N10</td>
												</tr>
												<tr>
													<td class="textblk" ><?php textbox("txtQtdPerguntaNivel1[]", "30", $quantidadeNivel1, "50%", "textbox3", "Qtd. N�vel 1", Mascaras::Numero) ?></td>
													<td class="textblk" ><?php textbox("txtQtdPerguntaNivel2[]", "30", $quantidadeNivel2, "50%", "textbox3", "Qtd. N�vel 2", Mascaras::Numero) ?></td>
													<td class="textblk" ><?php textbox("txtQtdPerguntaNivel3[]", "30", $quantidadeNivel3, "50%", "textbox3", "Qtd. N�vel 3", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel4[]", "30", $quantidadeNivel4, "50%", "textbox3", "Qtd. N�vel 4", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel5[]", "30", $quantidadeNivel5, "50%", "textbox3", "Qtd. N�vel 5", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel6[]", "30", $quantidadeNivel6, "50%", "textbox3", "Qtd. N�vel 6", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel7[]", "30", $quantidadeNivel7, "50%", "textbox3", "Qtd. N�vel 7", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel8[]", "30", $quantidadeNivel8, "50%", "textbox3", "Qtd. N�vel 8", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel9[]", "30", $quantidadeNivel9, "50%", "textbox3", "Qtd. N�vel 9", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel10[]", "30", $quantidadeNivel10, "50%", "textbox3", "Qtd. N�vel 10", Mascaras::Numero) ?></td>
												</tr>
                                                <tr>
                                                    <td class="textblk" >N11</td>
                                                    <td class="textblk" >N12</td>
                                                    <td class="textblk" >N13</td>
                                                    <td class="textblk" >N14</td>
                                                    <td class="textblk" >N15</td>
                                                    <td class="textblk" >N16</td>
                                                    <td class="textblk" >N17</td>
                                                    <td class="textblk" >N18</td>
                                                    <td class="textblk" >N19</td>
                                                    <td class="textblk" >N20</td>
                                                </tr>
                                                <tr>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel11[]", "30", $quantidadeNivel11, "50%", "textbox3", "Qtd. N�vel 11", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel12[]", "30", $quantidadeNivel12, "50%", "textbox3", "Qtd. N�vel 12", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel13[]", "30", $quantidadeNivel13, "50%", "textbox3", "Qtd. N�vel 13", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel14[]", "30", $quantidadeNivel14, "50%", "textbox3", "Qtd. N�vel 14", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel15[]", "30", $quantidadeNivel15, "50%", "textbox3", "Qtd. N�vel 15", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel16[]", "30", $quantidadeNivel16, "50%", "textbox3", "Qtd. N�vel 16", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel17[]", "30", $quantidadeNivel17, "50%", "textbox3", "Qtd. N�vel 17", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel18[]", "30", $quantidadeNivel18, "50%", "textbox3", "Qtd. N�vel 18", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel19[]", "30", $quantidadeNivel19, "50%", "textbox3", "Qtd. N�vel 19", Mascaras::Numero) ?></td>
                                                    <td class="textblk" ><?php textbox("txtQtdPerguntaNivel20[]", "30", $quantidadeNivel20, "50%", "textbox3", "Qtd. N�vel 20", Mascaras::Numero) ?></td>
                                                </tr>
											</table>
										</div>
										
										<?php
											}
										?>
										
									</div>
									
								</TD>
							</TR>
				        	<TR><TD colspan="3">&nbsp;</TD></TR>
				            <TR>
        					    <TD class="textblk" colspan="3">Avalia��o:</TD>
		        		    </TR>
				            <TR>
					            <TD colspan="3">
						            <?php textbox("txtProva", "50", page::$rn->DS_PROVA, "100%", "textbox3", "Disciplina") ?>
					            </TD>
	        			    </TR>
    		    		    <TR><TD colspan="3">&nbsp;</TD></TR>

    		    		    <TR>
								<TD class="textblk" colspan="3"><a href="javascript:adicionarChamada()">Adicionar Chamadas</a></TD>
							</TR>
							
							
							
							
							<TR>
								<TD colspan="3">
									<div id="dvChamadas">
										<?php
											$linhas = 1;
											$chamadas = page::$rn->chamadas;
										
											if (is_array($chamadas)){
												if (count($chamadas) > 1)
													$linhas = count($chamadas);
											}
											
											for ($i = 0; $i < $linhas; $i++){
												$codigoChamada = 0;
												$dataInicio = "";
												$dataTermino = "";
												$horaInicio = "";
												$horaTermino = "";
												
												if (is_array($chamadas)){
													$chamada = $chamadas[$i];
													$dataInicio = $chamada->DT_INICIO_REALIZACAO;
													$dataTermino = $chamada->DT_TERMINO_REALIZACAO;
													$horaInicio = $chamada->HR_INICIO_REALIZACAO;
													$horaTermino = $chamada->HR_TERMINO_REALIZACAO;
													
													
													
												}
												
										?>
									
										<div id="dvChamada" style="border: 1px solid black; margin-top: 2px;">
											<table cellpadding="2" cellspacing="0" width="98%">
												<tr>
													<td class="textblk" >Data de In�cio:</td>
													<td class="textblk" >Data de T�rmino:</td>
													<td rowspan="4" class="textblk" align="center"><input type="image" src="/images/layout/bt_delsession.gif" value="" onclick="javascript:removerChamada(this); return false;" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'"></td>
												</tr>												
												<tr>
													<TD><?php textbox("txtDataRealizacao[]", "10", $dataInicio, "110px", "textbox3", "In�cio Realiza��o", Mascaras::Data) ?></TD>
					            					<TD><?php textbox("txtDataRealizacaoFim[]", "10", $dataTermino, "110px", "textbox3", "T�rmino Realiza��o", Mascaras::Data) ?></TD>
												</tr>
												<tr>
													<td class="textblk" >Hora de In�cio</td>
													<td class="textblk" >Hora de T�rmino</td>
												</tr>
												<tr>
													<TD><?php textbox("txtHorarioInicio[]", "5", $horaInicio, "110px", "textbox3", "Hor�rio de In�cio", Mascaras::Hora) ?></TD>
													<TD><?php textbox("txtHorarioTermino[]", "5", $horaTermino, "110px", "textbox3", "Hor�rio de T�rmino", Mascaras::Hora) ?></TD>
												</tr>
											</table>
										</div>
										
										<?php
											}
										?>
										
									</div>
									
								</TD>
							</TR>
        				    <TR><TD colspan="3">&nbsp;</TD></TR>
        				    <TR>
					            <TD class=textblk colspan="2">Dura��o Prova (min)</TD>
					            <TD class=textblk>M�dia para Aprova��o</TD>
        				    </TR>
		        		    <TR>
					            <TD colspan="2"><?php textbox("txtDuracao", "3", page::$rn->DURACAO_PROVA, "110px", "textbox3", "Dura��o", Mascaras::Numero) ?></TD>
					            <TD><?php textbox("txtMedia", "4", page::$rn->VL_MEDIA_APROVACAO, "110px", "textbox3", "M�dia para Aprova��o", Mascaras::Numero) ?></TD>
        				    </TR>
        				    <TR><TD colspan="3">&nbsp;</TD></TR>
        				    <TR>
        				    	<TD class=textblk>Perguntas por p�gina</TD>
					            <TD class=textblk colspan="2"></TD>
        				    </TR>
		        		    <TR>
		        		    	<TD><?php textbox("txtPaginaPergunta", "50", page::$rn->NR_QTD_PERGUNTAS_PAGINA, "110px", "textbox3", "Perguntas por P�gina", Mascaras::Numero) ?></TD>
					            <TD colspan="2" class=textblk>
					            
					            <?php
	                                    $simuladoSim = "";
	                                    $simuladoNao = "";
	                                    
	                                    if (page::$rn->IN_PROVA == 1){
	                                         $simuladoNao = "checked";
	                                    }else{
	                                         $simuladoSim = "checked";
	                                    }
	                                    
	                                ?>
	                                    <INPUT onclick="javascript:controlarAvaliacao()" name="rdoSimulado" type="radio" value="0" <?php echo $simuladoSim ?>/>Avalia��o&nbsp;de&nbsp;Conhecimento&nbsp;&nbsp;<INPUT onclick="javascript:controlarAvaliacao()" name="rdoSimulado" type="radio" value="1" <?php echo $simuladoNao ?>/>Avalia��o&nbsp;de&nbsp;Aprendizado
					            	
					            </TD>
        				    </TR>
        				    <TR><TD colspan="3">&nbsp;</TR>
        				    <TR>
        				    	<TD class="textblk"">Tentativas</TD>
        				    	<TD class="textblk">Exibe Gabarito</TD>
        				    	<TD class="textblk">Exibe Obs</TD>
        				    </TR>
        				    <TR>
        				    	<TD><?php textbox("txtQuantidadeTentativa", "50", page::$rn->NR_TENTATIVA, "50%", "textbox3", "Tentativas", Mascaras::Numero) ?></TD>
        				    	<TD class=textblk>
 									<?php
	                                    $gabaritoSim = "";
	                                    $gabaritoNao = "";
	                                    
	                                    if (page::$rn->IN_GABARITO == 0){
	                                         $gabaritoNao = "checked";
	                                    }else{
	                                         $gabaritoSim = "checked";
	                                    }
	                                ?>        				    	
        				    	
        				    	<INPUT id="rdoGabaritoSim" name="rdoGabarito" type="radio" value="1" <?php echo $gabaritoSim ?>/>Sim &nbsp;&nbsp;<INPUT id="rdoGabaritoNao" name="rdoGabarito" type="radio" value="0" <?php echo $gabaritoNao ?>/>N�o
        				    	</TD>
		   				    	<TD class=textblk>
 									<?php
	                                    $obsSim = "";
	                                    $obsNao = "";
	                                    
	                                    if (page::$rn->IN_OBS == 0){
	                                         $obsNao = "checked";
	                                    }else{
	                                         $obsSim = "checked";
	                                    }
	                                ?>        				    	
        				    	
        				    	<INPUT id="rdoObsSim" name="rdoObs" type="radio" value="1" <?php echo $obsSim ?>/>Sim &nbsp;&nbsp;<INPUT id="rdoObsNao" name="rdoObs" type="radio" value="0" <?php echo $obsNao ?>/>N�o
        				    	</TD>
        				    </TR>
        				    <TR><TD colspan="3">&nbsp;</TD></TR>
        				    <tr>
        				    	<td colspan="3" class=textblk>Ciclo:</td>
        				    </tr>
        				    <tr>
        				    	<td colspan="3"><?php comboboxCicloEmpresa("cboCiclo",  page::$rn->CD_CICLO, "Ciclo"); ?></td>
        				    </tr>
        				    <TR><TD colspan="3">&nbsp;</TD></TR>
        				    <tr>
        				    	<td colspan="3" class=textblk>Cargo:</td>
        				    </tr>
        				    <tr>
        				    	<td colspan="3">
                                    <?php

                                        $cargos = array();
                                        if (page::$rn->cargos != null){
                                            foreach(page::$rn->cargos as $cargo){
                                                $cargos[] =  $cargo->NM_CARGO_FUNCAO;
                                            }
                                        }

                                        comboboxCargoFuncaoPrograma("cboCargo[]",  $cargos, -1, false, true);
                                    ?>
                                </td>
        				    </tr>
        				    
        				    
        				    <TR><TD colspan="3">&nbsp;</TD></TR>
        				    <TD class=textblk>
 									<?php
	                                    $senhaSim = "";
	                                    $senhaNao = "";
	                                    
	                                    if (page::$rn->IN_SENHA_MESTRA == 1){
	                                    	$senhaSim = "checked";
	                                    }else{
	                                         $senhaNao = "checked";
	                                    }
	                                ?>        				    	
        				    	Senha Mestra: <br />
        				    	<INPUT id="rdoSenhaSim" name="rdoSenha" type="radio" value="1" <?php echo $senhaSim ?>/>Sim &nbsp;&nbsp;<INPUT id="rdoSenhaNao" name="rdoSenha" type="radio" value="0" <?php echo $senhaNao ?>/>N�o
        				    	</TD>
        				    
        				    <TR><TD colspan="3">&nbsp;</TD></TR>
        				    <TR>
        				    	<TD class=textblk colspan="3">Usu�rios Selecionados</TD>
        				    </TR>
        				    <TR>
        				    	<TD colspan="3">
        				    		<?php //$usuario = new col_usuario();
        				    		 //dualListBox("listUsuario",10, $usuario->listar(),page::$rn->usuarios, "CD_USUARIO", "NM_USUARIO", "Usu�rios n�o selecionados", "Usu�rios Selecionados");
        				    		 echo "<div style='height:100px; overflow : auto; background-color: #ffffff; border: 1px solid #000000' >";
        				    		 treeViewUsuario();
        				    		 echo "</div>";
        				    		  ?>
        				    		 
        				    	</TD>
        				    </TR>
        				    
                        </table>
				    </TD> 
				</TR>
				
				
				<script language="javascript">
					//<!--
						function controlarAvaliacao()
						{
							
							if(!document.getElementById("rdoSimulado").checked)
							{
								document.getElementById("txtQuantidadeTentativa").readOnly = true;
								document.getElementById("txtQuantidadeTentativa").value = 1;
								document.getElementById("txtQuantidadeTentativa").style.backgroundColor = "#dddddd";
								document.getElementById("rdoGabaritoNao").disabled = false;
								
							}
							else
							{
								document.getElementById("txtQuantidadeTentativa").readOnly = false;
								document.getElementById("rdoGabarito").checked = true;
								document.getElementById("txtQuantidadeTentativa").style.backgroundColor = "";
								document.getElementById("rdoGabaritoNao").disabled = true;
							}
							
						}
						
						controlarAvaliacao();
					
						function adicionarDisciplina(){
							
							var no = document.getElementById("dvDisciplina").cloneNode(true);
							var dv = document.getElementById("dvDisciplinas");
							
							//alert(no.all.length)
							
							var itens = no.all.length;
							
							for (i=0; i<itens; i++){
								if (no.all[i].type == "text"){
									no.all[i].value = "";
								}else if (no.all[i].type == "select-one"){
									no.all[i].selectedIndex = 0;
								}
							}
							
							dv.appendChild(no);
							
						}
						
						function removerDisciplina(obj){
							if (document.getElementById("dvDisciplinas").children.length==1){
								alert('Deve existir ao menos uma disciplina.')
								return;
							}
							
							 document.getElementById("dvDisciplinas").removeChild(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
							
							//alert(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
								
						}
						
						function adicionarChamada(){
							
							var no = document.getElementById("dvChamada").cloneNode(true);
							var dv = document.getElementById("dvChamadas");
							
							//alert(no.all.length)
							
							var itens = no.all.length;
							
							for (i=0; i<itens; i++){
								if (no.all[i].type == "text"){
									no.all[i].value = "";
								}else if (no.all[i].type == "select-one"){
									no.all[i].selectedIndex = 0;
								}
							}
							
							dv.appendChild(no);
							
						}
						
						function removerChamada(obj){
							if (document.getElementById("dvChamadas").children.length==1){
								alert('Deve existir ao menos uma chamada.')
								return;
							}
							
							 document.getElementById("dvChamadas").removeChild(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
							
							//alert(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
								
						}
						
					
					//-->
				</script>
				

<?php


}

function treeViewUsuario(){
	
	$idProva = $_REQUEST["id"];
	
	if ($idProva == "") $idProva = 0;
	
	$sql = "SELECT
				e.CD_EMPRESA, e.DS_EMPRESA,
				l.CD_LOTACAO, l.DS_LOTACAO,
				u.CD_USUARIO, u.login,
				pa.CD_PROVA,
				pv1.CD_VINCULO AS VINCULO_EMPRESA,
				pv2.CD_VINCULO AS VINCULO_LOTACAO
				
			FROM
				col_empresa e
				LEFT OUTER JOIN col_lotacao l ON l.CD_EMPRESA = e.CD_EMPRESA
				LEFT OUTER JOIN col_usuario u ON u.empresa = e.CD_EMPRESA AND u.lotacao = l.CD_LOTACAO
				LEFT OUTER JOIN col_prova_aplicada pa ON pa.CD_USUARIO = u.CD_USUARIO AND pa.CD_PROVA = $idProva
				LEFT OUTER JOIN col_prova_vinculo pv1 ON pv1.CD_PROVA = $idProva AND pv1.CD_VINCULO = e.CD_EMPRESA AND pv1.IN_TIPO_VINCULO = 1
				LEFT OUTER JOIN col_prova_vinculo pv2 ON pv2.CD_PROVA = $idProva AND pv2.CD_VINCULO = l.CD_LOTACAO AND pv2.IN_TIPO_VINCULO = 2
			WHERE
				e.IN_ATIVO = 1
			AND l.IN_ATIVO = 1
			AND u.`Status` = 1
			ORDER BY
				e.DS_EMPRESA, l.DS_LOTACAO, u.login";
	
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql);
	
	
	$codigoEmpresaAnterior = 0;
	$codigoLotacaoAnterior = 0;
	$empresas = array();
	$objetoEmpresa = array();
	$objetoLotacao = array();
	while($linha = mysql_fetch_array($resultado)) {
		
		
		
		if ($codigoEmpresaAnterior != $linha["CD_EMPRESA"]) {

			if ($codigoEmpresaAnterior != 0) {
				if ($codigoEmpresaAnterior != 0 && count($objetoLotacao) > 0) {
					$objetoEmpresa["LOTACOES"][] = $objetoLotacao;
					$objetoLotacao = null;
				}
				$empresas[] = $objetoEmpresa;
			}
			
			
			$objetoEmpresa["CD_EMPRESA"] = $linha["CD_EMPRESA"]; 
			$objetoEmpresa["DS_EMPRESA"] = $linha["DS_EMPRESA"];
			$objetoEmpresa["VINCULO_EMPRESA"] = ($linha["VINCULO_EMPRESA"] > 0)? "checked" : "";
			$objetoEmpresa["LOTACOES"] = array();
			$codigoEmpresaAnterior = $linha["CD_EMPRESA"];
			
		}
		
		if ($codigoLotacaoAnterior != $linha["CD_LOTACAO"]) {
			
			if ($codigoEmpresaAnterior != 0 && count($objetoLotacao) > 0) {
				$objetoEmpresa["LOTACOES"][] = $objetoLotacao;;
			}
			
			$objetoLotacao = array();
			$objetoLotacao["CD_LOTACAO"] = $linha["CD_LOTACAO"];
			$objetoLotacao["DS_LOTACAO"] = $linha["DS_LOTACAO"];
			$objetoLotacao["USUARIOS"] = array();
			
			$codigoLotacaoAnterior = $linha["CD_LOTACAO"];
		}
		
		$objetoUsuario = array();
		$objetoUsuario["CD_USUARIO"] = $linha["CD_USUARIO"];
		$objetoUsuario["login"] = $linha["login"];
		$objetoUsuario["CD_PROVA"] = ($linha["CD_PROVA"] > 0)? "checked" : "";;
		$objetoLotacao["USUARIOS"][] = $objetoUsuario;
		
		
	}
	
	$objetoEmpresa["LOTACOES"][] = $objetoLotacao;
	$empresas[] = $objetoEmpresa;
	
	echo "<ul style='margin-left: 0px'>";
	
	foreach ($empresas as $empresa) {
		
		
		$imagem = "plusik_r";
		
		if (count($empresa["LOTACOES"]) == 0) {
			$imagem = "minus_r";
		}
		
		echo "<li class='textblk' style='font-weight: bold'><img src='$imagem.gif' onclick='expandCollapse(this)'><input name='chkEmpresa{$empresa["CD_EMPRESA"]}' {$empresa["VINCULO_EMPRESA"]} value='{$empresa["CD_EMPRESA"]}' type='checkbox' onclick='marcar(this)'> {$empresa["DS_EMPRESA"]}";
		
		if (count($empresa["LOTACOES"]) > 0) {
			echo "<ul class='nodeH'>";	
		}
		
		foreach ($empresa["LOTACOES"] as $lotacao) {
			$imagem = "plusik_r";
			
			if (count($lotacao["USUARIOS"]) == 0) {
				$imagem = "minus_r";
			}
			
			echo "<li class='textblk'><img src='$imagem.gif' onclick='expandCollapse(this)'><input name='chkLotacao{$lotacao["CD_LOTACAO"]}' $checked value='{$lotacao["CD_LOTACAO"]}' type='checkbox' onclick='marcar(this)'> {$lotacao["DS_LOTACAO"]}";
			
			if (count($lotacao["USUARIOS"]) > 0) {
				echo "<ul class='nodeH'>";	
			}
			
			foreach ($lotacao["USUARIOS"] as $usuario) {
				
				echo "<li class='textblk'><IMG style='display: none'><input name='chkUsuario{$usuario["CD_USUARIO"]}' {$usuario["CD_PROVA"]} value='{$usuario["CD_USUARIO"]}' type='checkbox' onclick='marcar(this)'> {$usuario["login"]}</li>";
				
			}
			
			if (count($lotacao["USUARIOS"]) > 0) {
				echo "</ul>";	
			}
			
			
			echo "</li>";
			
			
		}
		
		if (count($empresa["LOTACOES"]) > 0) {
			echo "</ul>";	
		}

		echo "</li>";
		
	}
	
	echo "</ul>";
}



function treeViewUsuarioOld(){
	
	//echo "ssadasdas";
	
	$empresa = new col_empresa();
	$parametro = new ParametroDB("IN_ATIVO","1");
	$resultadoEmpresa = $empresa->listar(array($parametro),"DS_EMPRESA");
	
	echo "<ul style='margin-left: 0px'>";
	
	while ($linhaEmpresa = mysql_fetch_array($resultadoEmpresa)) {

		//if ($linhaEmpresa["IN_ATIVO"] == 0)
			//continue;		
		
		$sql = "SELECT * FROM col_lotacao WHERE CD_EMPRESA = {$linhaEmpresa["CD_EMPRESA"]} ORDER BY DS_LOTACAO";
		$resultadoLotacao = $empresa->dao->executeQuery($sql);
		
		$imagem = "plusik_r";
		
		$existeLotacao = mysql_num_rows($resultadoLotacao) != 0;
		
		if (!$existeLotacao){
			$imagem = "minus_r";
		}
		
		$idProva = $_REQUEST["id"];
		$checked = "";
		if (isset($_REQUEST["id"])) {
			
			$sql = "SELECT 1 FROM col_prova_vinculo WHERE CD_PROVA = $idProva AND CD_VINCULO = {$linhaEmpresa["CD_EMPRESA"]} AND IN_TIPO_VINCULO = 1";
			$resultadoCheck = $empresa->dao->executeQuery($sql);
			if (mysql_num_rows($resultadoCheck) > 0){
				$checked = "checked";
			}
			
		}
		
				
	echo "<li class='textblk' style='font-weight: bold'><img src='$imagem.gif' onclick='expandCollapse(this)'><input name='chkEmpresa{$linhaEmpresa["CD_EMPRESA"]}' $checked value='{$linhaEmpresa["CD_EMPRESA"]}' type='checkbox' onclick='marcar(this)'> {$linhaEmpresa["DS_EMPRESA"]}";

		if ($existeLotacao){
			echo "<ul class='nodeH'>";
		}
				
		while ($linhaLotacao = mysql_fetch_array($resultadoLotacao)) {
			
			
			$colunaSelect = "";
			$join = "";
			$where = "";
			if ($_REQUEST["id"] != "")
			{
				$colunaSelect = " ,(select a.CD_PROVA from col_prova_aplicada a where a.CD_USUARIO = u.CD_USUARIO AND a.CD_PROVA = '{$_REQUEST["id"]}') AS CD_PROVA ";
				//$join = "LEFT OUTER JOIN col_prova_aplicada a ON a.CD_USUARIO = u.CD_USUARIO";
				//$where = "AND (a.CD_PROVA = '{$_REQUEST["id"]}' OR a.CD_PROVA IS NULL)";			
			}
			
			$sql = "SELECT u.CD_USUARIO, login $colunaSelect FROM col_usuario u $join WHERE lotacao = {$linhaLotacao["CD_LOTACAO"]} AND login IS NOT NULL $where ORDER BY login";
			//$resultadoUsuario = $empresa->dao->executeQuery($sql);
			//echo $sql;
			//exit();
			$imagem = "plusik_r";
			
			$existeUsuario = mysql_num_rows($resultadoUsuario) != 0;
			
			if (!$existeUsuario){
				$imagem = "minus_r";
			}

			$checked = "";
			if (isset($_REQUEST["id"])) {
			
				$sql = "SELECT 1 FROM col_prova_vinculo WHERE CD_PROVA = $idProva AND CD_VINCULO = {$linhaLotacao["CD_LOTACAO"]} AND IN_TIPO_VINCULO = 2";
				$resultadoCheck = $empresa->dao->executeQuery($sql);
				if (mysql_num_rows($resultadoCheck) > 0){
					$checked = "checked";
				}
			
			}

			
			echo "<li class='textblk'><img src='$imagem.gif' onclick='expandCollapse(this)'><input name='chkLotacao{$linhaLotacao["CD_LOTACAO"]}' $checked value='{$linhaLotacao["CD_LOTACAO"]}' type='checkbox' onclick='marcar(this)'> {$linhaLotacao["DS_LOTACAO"]}";
			
			if ($existeUsuario){
				echo "<ul class='nodeH'>";
			}
			
			while ($linhaUsuario = mysql_fetch_array($resultadoUsuario)) {
				
				$checked = "";
				if (isset($_REQUEST["id"])) {
				
					//$sql = "SELECT 1 FROM col_prova_aplicada WHERE CD_PROVA = $idProva AND CD_USUARIO = {$linhaUsuario["CD_USUARIO"]}";
					//$resultadoCheck = $empresa->dao->executeQuery($sql);
					//if (mysql_num_rows($resultadoCheck) > 0){
					if ($linhaUsuario["CD_PROVA"] != "")
					{
						$checked = "checked";
					}
				
				}

				
				echo "<li class='textblk'><IMG style='display: none'><input name='chkUsuario{$linhaUsuario["CD_USUARIO"]}' $checked value='{$linhaUsuario["CD_USUARIO"]}' type='checkbox' onclick='marcar(this)'> {$linhaUsuario["login"]}</li>";
				
			}
			
			
			if ($existeUsuario){
				echo "</ul>";
			}
			
			echo "</li>";
			
		}
		
		if ($existeLotacao){
			echo "</ul>";
		}
		
		
	echo "</li>";
	}
	
	echo "</ul>";
	
}


?>