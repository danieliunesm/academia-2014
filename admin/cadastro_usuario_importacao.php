<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_usuario_importacao();
}

function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/admin/cadastro_usuario_importacao.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

function carregarRN(){
	
	$erro = "";

	//Realizacao do upload do arquivo
	$empresa = $_POST["cboEmpresa"];
	$dataAtual = date("YmdHis");
	$nome = $_FILES['fleImportacao']['name'];
	$savefile = "";
    $UPLOAD_BASE_IMG_DIR	= getDocumentRoot()."/admin/importacoes/";
    $nomeArquivo = substr("{$empresa}_{$dataAtual}_{$nome}",0, 100);

	if(is_uploaded_file($_FILES['fleImportacao']['tmp_name'])){

        $savefile = "{$UPLOAD_BASE_IMG_DIR}{$nomeArquivo}";
		if(move_uploaded_file($_FILES['fleImportacao']['tmp_name'], $savefile)){
			chmod($savefile, 0666);
			//$fotofile = $_FILES['flLogo']['name'];
		}
	}
	//Fim do upload

	
	//$savefile = "D:\\Zend\\Apache2\\htdocs\\colaborae\\admin\\importacoes\\teste.xml";
	
	try {
		//$dom = DOMDocument::load($_FILES['fleImportacao']['tmp_name']) or die("O arquivo n�o � um XML v�lido");
		//$dom = DOMDocument::load($savefile) or die("O arquivo n�o � um XML v�lido");
		//page::$rn->usuarios = $dom;
		page::$rn->acao = $_POST["rdoAcao"];
	
		page::$rn->empresa = $_POST["cboEmpresa"];
		page::$rn->senhaUsuarios = $_POST["txtSenha"];
        page::$rn->nomeArquivo = $nomeArquivo;
        page::$rn->diretorioArquivo = $UPLOAD_BASE_IMG_DIR;
	}catch (Exception $e){
		$erro = "O arquivo n�o � um XML v�lido";
	}

	if ($erro == "" && $_POST["rdoAcao"] == "I"){

		include "../include/cripto.php";

		//Verificar se existe a senha para outras empresas
		//$sql = "SELECT COUNT(1) AS TOTAL FROM col_usuario WHERE empresa <> " . $_POST["cboEmpresa"] . "
		//AND senha = '" .  Cripto($_POST["txtSenha"],'C') . "'";
		
		//echo "<!--$sql-->";
		//exit();
		
		//$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		//$linha = mysql_fetch_array($resultado);
		
		//if ($linha["TOTAL"] > 0){
		//	$erro = "Existem usu�rios com a senha digitada. Por favor escolha outra senha.";
		//}

	}
	
	if ($erro != ""){
		echo $erro;
		pageRender();
		exit();
	}
	
	

}

function pagePreRender(){

	page::$enctype = "enctype=\"multipart/form-data\"";
	
}

function pageDepoisDeSalvar()
{
	
	//resumoImportacao();
	pageRender();
	
	exit();
	
}

function resumoImportacao()
{
	
	$rn = page::$rn->resumoImportacao;
	
?>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr class="textblk">
			<td colspan="2" align="center"><b>Resumo da Importa��o</b></td>
		</tr>
		<tr class="textblk">
			<td>Linhas Processadas:</td>
			<td><?php echo $rn->quantidadeLinhasProcessadas; ?></td>
		</tr>
		<tr class="textblk">
			<td>Linhas N�o Processadas:</td>
			<td><?php echo $rn->quantidadeLinhasNaoProcessadas; ?></td>
		</tr>
		<tr class="textblk">
			<td>Lota��es:</td>
			<td><?php echo $rn->quantidadeLotacoesProcessadas; ?></td>
		</tr>
		<tr class="textblk">
			<td>Grupos Gerenciais:</td>
			<td><?php echo $rn->quantidadeFiltrosProcessados; ?></td>
		</tr>
		<tr class="textblk">
			<td>Superusu�rio do Programa:</td>
			<td><?php echo $rn->quantidadeSuperUsuariosPrograma; ?></td>
		</tr>
		<tr class="textblk">
			<td>Superusu�rio Gerencial:</td>
			<td><?php echo $rn->quantidadeSuperUsuariosGrupo; ?></td>
		</tr>
				<tr class="textblk">
			<td>Participantes:</td>
			<td><?php echo $rn->quantidadeParticipantes; ?></td>
		</tr>
	
		<tr class="textblk">
			<td>Erros:</td>
			<td>
				<?php 
					foreach ($rn->erros as $erro)
					{
						echo "Erro na linha: $erro <br />";
					}
				?>
			</td>
		
		
		</tr>
	
	
	</table>

<?php 
	
	
	
	
}

function pageRenderEspecifico(){
	
?>	
				<tr>
					<td class="textblk">A��o:</td>
				</tr>
				<tr>
					<td class="textblk">
						<input type="radio" name="rdoAcao" id="rdoAcao" value="I" checked="true">Importar</input>
						<input type="radio" name="rdoAcao" id="rdoAcao" value="E" >Excluir</input>
                        <input disabled="disabled" type="radio" name="rdoAcao" id="rdoAcao" value="H" >Planilha HCT</input>
					</td>
				</tr>
				<TR><TD>&nbsp;</TD></TR>
				<TR>

						<TD class="textblk">Programa: *</TD>
					</TR>
					<TR>
						<TD><?php comboboxEmpresa("cboEmpresa", null, "Programa"); ?></TD>
					</TR>
					<TR><TD>&nbsp;</TD></TR>
					<TR>
						<TD class="textblk">Elegibilidade: *</TD>
					</TR>
					<TR class="textblk">
						<TD>
						<input type="radio" name="rdoElegibilidade" id="rdoElegibilidade" value="S" checked="true">N�o aplicar</input>
						<input type="radio" name="rdoElegibilidade" id="rdoElegibilidade" value="E" >Tornar Eleg�vel</input>
						<input type="radio" name="rdoElegibilidade" id="rdoElegibilidade" value="N" >Tornar Ineleg�vel</input>
					</td>
					</TR>
					<TR><TD>&nbsp;</TD></TR>
					<TR>
						<TD class="textblk">Arquivo para importa��o:</TD>
					</TR>
					<TR>
						<TD><input type="file" name="fleImportacao" style="width: 100%" TagMensagemObrigatorio="Arquivo para importa��o" /></TD>
					</TR>
					<TR><TD>&nbsp;</TD></TR>
					<TR>
						<TD class="textblk">Senha para usu�rios importados:</TD>
					</TR>
					<TR>
						<TD><input type="text" name="txtSenha" style="width: 100%" maxlength="8" TagMensagemObrigatorio="Senha para usu�rios importados" onblur="javascript:if(this.value.length > 0 && this.value.length < 6){ alert('A senha deve possuir entre 6 e 8 caracteres'); this.focus(); }" /></TD>
					</TR>
					<TR><TD>&nbsp;</TD></TR>
					<TR>
						<TD class="textblk">O arquivo deve ser salvo como planilha XML no Excel e estar no seguinte formato: <br><br>
						Departamento | Nome | Cargo | Email | Login | Perfil | Localidade
						</TD>
					</TR>
<?php

}


?>