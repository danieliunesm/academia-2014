<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";
include "../include/cripto.php";

if($_SESSION["usuario"]!="webmaster") header("Location: /admin/admin_menu.php");


$id	= $_GET["id"];

$tipo					= 1;
$grupo					= 1;
$permissao				= "";
$nome					= "";
$cd_usuario 			= 0;

$nome					= '';
$sobrenome				= '';
$datanasc				= '';
$naturalidade			= '';
$uf						= '';
$nacionalidade			= '';
$enderecores			= '';
$cepres					= '';
$dddtelres				= '';
$telres					= '';
$dddcel					= '';
$cel					= '';
$email					= '';
$cd_empresa				= 0;
$nomeempresa			= '';
$cd_lotacao				= 0;
$cd_localidade			= 0;
$lotacao				= '';
$cargofuncao			= '';
$enderecocom			= '';
$cepcom					= '';
$dddtelcom				= '';
$telcom					= '';
$ramal					= '';
$experiencia			= '';
$outrosexperiencia		= '';
$especializacao			= '';
$outrosespecializacao	= '';
$fotofile				= '';
$cvfile					= '';
$newsletteragreement	= "0";
$policyagreement		= "0";

$experiencia			= '';
$especializacao			= '';
$newsletteragreement	= "0";
$policyagreement		= "0";

$login					= '';
$senha					= '';

$cd_cliente				= 0;

$filtro = 0;

if($id != '')
{
	$sql = "SELECT * FROM col_usuario WHERE CD_USUARIO = " . $id;
	$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
	if($oRs = mysql_fetch_array($RS_query))
	{
		$tipo					= $oRs['tipo'];
		$grupo					= 1;
		$permissao				= "";
		
		$nome					= $oRs['NM_USUARIO'];
		$sobrenome				= $oRs['NM_SOBRENOME'];
		$datanasc				= $oRs['datanasc'];
		$naturalidade			= $oRs['naturalidade'];
		$uf						= $oRs['uf'];
		$nacionalidade			= $oRs['nacionalidade'];
		$enderecores			= $oRs['enderecores'];
		$cepres					= $oRs['cepres'];
		$dddtelres				= $oRs['dddtelres'];
		$telres					= $oRs['telres'];
		$dddcel					= $oRs['dddcel'];
		$cel					= $oRs['cel'];
		$email					= $oRs['email'];
		$cd_empresa				= (Integer)$oRs['empresa'];
		$cd_lotacao				= (Integer)$oRs['lotacao'];
		$cd_localidade			= (Integer)$oRs['CD_LOCALIDADE'];
		$cargofuncao			= $oRs['cargofuncao'];
		$enderecocom			= $oRs['enderecocom'];
		$cepcom					= $oRs['cepcom'];
		$dddtelcom				= $oRs['dddtelcom'];
		$telcom					= $oRs['telcom'];
		$ramal					= $oRs['ramal'];
		$experiencia			= $oRs['experiencia'];
		$outrosexperiencia		= $oRs['outrosexperiencia'];
		$especializacao			= $oRs['especializacao'];
		$outrosespecializacao	= $oRs['outrosespecializacao'];
		$fotofile				= $oRs['fotofile'];
		$cvfile					= $oRs['cvfile'];
		$experiencia			= $oRs['experiencia'];
		$especializacao			= $oRs['especializacao'];
		$newsletteragreement	= $oRs['newsletteragreement'];
		$policyagreement		= $oRs['policyagreement'];
		$login					= $oRs['login'];
		$senha					= Cripto($oRs['senha'],'D');
		$filtro					= $oRs["CD_FILTRO"];
		
		$sql2 = "SELECT DS_EMPRESA, CD_CLIENTE FROM col_empresa WHERE CD_EMPRESA = " . $cd_empresa;
		$RS_query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());
		if($oRs = mysql_fetch_row($RS_query2)){
			$nomeempresa = $oRs[0];
			$cd_cliente = $oRs[1];
		}
		mysql_free_result($RS_query2);
		
		$sql2 = "SELECT DS_LOTACAO FROM col_lotacao WHERE CD_LOTACAO = " . $cd_lotacao;
		$RS_query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());
		if($oRs = mysql_fetch_row($RS_query2)){
			$lotacao = $oRs[0];
		}
		mysql_free_result($RS_query2);
		
			
		
	}
	mysql_free_result($RS_query);
}

function checkIfOptionExperienciaIsSelected($str){
	global $experiencia;
	$pos = strpos($experiencia, $str);
	if($pos === false)
		return '';
	else
		return 'selected';
}

function checkIfOptionEspecializacaoIsSelected($str){
	global $especializacao;
	$pos = strpos($especializacao, $str);
	if($pos === false)
		return '';
	else
		return 'selected';
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Educa��o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<script type="text/javascript" src="/include/js/browser.js"></script>
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script type="text/javascript" src="/include/js/formatadados.js"></script>
<script type="text/javascript" src="/include/js/ajax.js"></script>
<script language="JavaScript">
function ValidaData(DtVal){
	DtVal = Exclui_inval(DtVal, 8);
	var msg = "";
	if(DtVal.length != 8){
		msg = "Preencha a Data de Nascimento no formato DDMMAAAA!     \n";
        return msg;
	}
	var nChar = null;
	for (i=0;i<8;i++){
		nChar = DtVal.charAt(i);
		if(isNaN(nChar)){
			msg = "Caracter \ " + nChar + " \ n�o � v�lido.     \n";
			return msg;
		}
	}
	var DtAcerto = null;
	DtAcerto = DtVal.substring(2,4) + "/" + DtVal.substring(0,2) + "/" + DtVal.substring(4,8);
	var bError 		= false;
	var aChar 		= null;
	var holder 		= null;
	var Short 		= null;
	var FebCheck 	= null;
	var NovCheck 	= null;
	var MonthCheck	= null;
	
	if(parseInt(DtVal.substring(2,4)) == 0 || parseInt(DtVal.substring(0,2)) == 0 || parseInt(DtVal.substring(4,8)) == 0) bError = true;
	
    for(i=0;i<10;i++){
		aChar = DtAcerto.charAt(i);
		if(i == 0 && aChar > 1) bError = true;
		if(i == 0 && aChar < 0) bError = true;
		if(i == 0 && aChar == 1) MonthCheck = 1;
		if(i == 0 && aChar == 1) NovCheck = 1;
		if(i == 1 && aChar < "0" || aChar > "9") bError = true;
		if(i == 1 && aChar == 2) FebCheck=1;
		if(i == 1 && MonthCheck == 1 && aChar >2) bError = true;
		if(i == 1 && aChar == "4" || aChar =="6" || aChar =="9") Short = 1;
		if(i == 1 && NovCheck == 1 && aChar == 1) Short = 1;
		if(i == 2 && aChar != "/") bError = true;
        if(FebCheck == 1 && NovCheck != 1){
			if(i==3 && aChar > 2) bError = true;
		}
		if (i == 3 && aChar > 3) bError = true;
		if (i == 3 && aChar ==3) holder=1;
		if (i == 4 && aChar >0 && Short==1 && holder==1) bError = true;
		if (i == 4 && aChar >1 && holder==1) bError = true;
		if (i == 4 && aChar < "0" || aChar > "9") bError = true;
		if (i == 5 && aChar != "/")  bError = true;
		if (i == 8 && aChar < "0" || aChar > "9") bError = true;
		if (i == 9 && aChar < "0" || aChar > "9") bError = true;
	}
	if((DtAcerto.substring(0,5) == "02/29") && DtAcerto.substring(6,10) % 4 != 0){
		msg =  "Data de Nascimento inv�lida. N�o � ano bissexto!     \n";
		return msg;
	}
    if(bError){
		msg =  "Data de Nascimento inv�lida!     \n";
		return msg;
	}
	else{
		return "";
	}
}

function validaEmail(emailStr){
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")	;
	var matchArray=emailStr.match(emailPat);

	if(matchArray==null)return false;

	var user=matchArray[1];
	var domain=matchArray[2];

	if(user.match(userPat)==null)return false;

	var IPArray=domain.match(ipDomainPat);
	if(IPArray!=null){
		for(var i=1;i<=4;i++){
			if(IPArray[i]>255)return false;
		}
		return true;
	}	
	var domainArray=domain.match(domainPat);

	if(domainArray==null)return false;
	
	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;

	if(domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>4)return false;

	if(len<2)return false;

	return true;
}

function checkFormFields(){
	var msg 	 = "";
	var msgdata	 = "";
	var f   	 = document.cadastroForm;
	var objfocus = null;

	if(f.nome.value 		== ""){
		msg+="O campo Nome precisa ser preenchido!       \n";
		f.nome.style.backgroundColor = '#ddd';
		objfocus = f.nome;
	}
	if(f.sobrenome.value	== ""){
		//msg+="O campo Sobrenome precisa ser preenchido!       \n";
		//f.sobrenome.style.backgroundColor = '#ddd';
		//if(objfocus == null) objfocus = f.sobrenome;
	}
//	if(f.datanasc.value != '') msgdata = ValidaData(f.datanasc.value);
	if(msgdata != ''){
		msg+=msgdata;
		f.datanasc.style.backgroundColor = '#ddd';
		if(objfocus == null) objfocus = f.datanasc;
	}
	if(f.email.value 		== ""){
		//msg+="O campo E-mail precisa ser preenchido!       \n";
		//f.email.style.backgroundColor = '#ddd';
		//if(objfocus == null) objfocus = f.email;
	}
	else{
		if(!validaEmail(f.email.value)){
			msg+="O e-mail fornecido n�o � v�lido! Verifique os dados digitados.       \n";
			f.email.style.backgroundColor = '#ddd';
			if(objfocus == null) objfocus = f.email;
		}
	}

	if (f.cd_cliente.selectedIndex < 1){
		msg+="O campo Empresa precisa ser preenchido!       \n";
		f.cd_cliente.style.backgroundColor = '#ddd';
		objfocus = f.cd_cliente;
	}

	var itemSelecionado = false;
	for(i=0;i<document.getElementById("nomeempresa").length;i++)
	{
		if(document.getElementById("nomeempresa").options[i].selected)
		{
			itemSelecionado = true;
			break;
		}
	}

	if (!itemSelecionado){
		msg+="O campo Programa precisa ser preenchido!       \n";
		document.getElementById("nomeempresa").style.backgroundColor = '#ddd';
		objfocus = document.getElementById("nomeempresa");
	}
	
	if (f.cd_lotacao.value < 1){
		msg+="O campo Lota��o precisa ser preenchido!       \n";
		f.cd_lotacao.style.backgroundColor = '#ddd';
		objfocus = f.cd_lotacao;
	}

	if (f.cargofuncao.value == ""){
		msg+="O campo Cargo/Fun��o precisa ser preenchido!       \n";
		f.cargofuncao.style.backgroundColor = '#ddd';
		objfocus = f.cargofuncao;
	}
	
	
	
	if(f.userlogin.value == ""){
		msg+="O campo Login precisa ser preenchido!       \n";
		f.userlogin.style.backgroundColor = '#ddd';
		objfocus = f.userlogin;
	}
	if((f.userpass1.value == '')||(f.userpass1.value.length < 6)){
		msg+="A senha atual est� vazia ou com menos de 6 caracteres!       \n";
		f.userpass1.style.backgroundColor = '#ddd';
		if(objfocus == null)objfocus = f.userpass1;
	}
	if(f.userpass1.value != f.userpass2.value){
		msg+="A confirma��o de senha n�o foi digitada corretamente!       \n";
		f.userpass2.style.backgroundColor = '#ddd';
		if(objfocus == null)objfocus = f.userpass2;
	}
	
	if(msg != ""){
		alert(msg);
		if(objfocus){
			if(objfocus == f.nome) self.scrollTo(0,0);
			objfocus.focus();
		}
		return false;
	}
	else{

		f.submit();
	}
}

createRequestObject('lotacao');
function sendRequest(oId){
	objectId = 'lotacao';
	sendReq('/getlotacao.php','emp',String(oId));
	sendRequestLocalidade(oId);
}

function sendRequestPrograma(oId){
	objectId = 'nomeempresa';
	sendReq('/getPrograma.php','emp',String(oId));
}

function sendRequestLocalidade(oId){
	if (waitingResponse) {
		setTimeout("sendRequestLocalidade(" + oId + ")", 1000);
		return;
	}
	objectId = 'localidade';
	sendReq('/getLocalidade.php','emp',String(oId));
}

</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><span class="title">USU�RIO:&nbsp;</span><?php echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_usuarios.php'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="1"></td></tr>
</table>
<br><br>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr class="tarjaTitulo">
	<td height="20" colspan="9" align="center">CADASTRO DE USU�RIO</td>
</tr>
</table>
<br><br>
		<table border="0" cellpadding="0" cellspacing="5" width="400" align="center">
		<form name="cadastroForm" enctype="multipart/form-data" method="post" action="gravarcadastro.php">
		<tr>
		<td class="textblk2" colspan="2">
		Os campos assinalados com <span>*</span> devem ser preenchidos:<br /><br />
		</td>
		</tr>
		<tr>
		<td width="1%" class="textblk2" align="right" nowrap><span>*</span>Nome:</td>
		<td width="99%"><input type="text" class="textbox8" name="nome" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="50" tabIndex="10" value="<? echo $nome; ?>"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap><span>*</span>Sobrenome:</td>
		<td><input type="text" class="textbox8" name="sobrenome" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="50" tabIndex="11" value="<? echo $sobrenome; ?>"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Nascimento:</td>
		<td class="textblk2"><input type="text" class="textbox8b" name="datanasc" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="10" tabIndex="12" onkeypress="return(Formata_Dados(this,'/','',event,'DATA'))" value="<? echo $datanasc; ?>">&nbsp;(ddmmaaaa)</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Naturalidade:</td>
		<td class="textblk2"><input type="text" class="textbox8d" name="naturalidade" size="31" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="50" tabIndex="13" value="<? echo $naturalidade; ?>">&nbsp;UF&nbsp;<input type="text" class="textbox9" name="uf" size="2" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="2" tabIndex="14" onkeyup="this.value=this.value.toUpperCase()" value="<? echo $uf; ?>"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Nacionalidade:</td>
		<td><input type="text" class="textbox8d" name="nacionalidade" size="31" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="10" tabIndex="15" value="<? echo $nacionalidade; ?>"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Endere�o res.:</td>
		<td><input type="text" class="textbox8" name="enderecores" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="16" value="<? echo $enderecores; ?>"></td>
		</tr>
		
		<tr>
		<td class="textblk2" align="right" nowrap>CEP res.:</td>
		<td class="textblk2"><input type="text" class="textbox8b" name="cepres" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="10" tabIndex="17" onkeypress="return(Formata_Dados(this,'.','-',event,'CEP'))" value="<? echo $cepres; ?>">&nbsp;(apenas n�meros)</td>
		</tr>
		
		<tr>
		<td class="textblk2" align="right" nowrap><span>*</span>(DDD) Telefone res.:</td>
		<td><input type="text" class="textbox9" name="dddtelres" size="3" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="3" tabIndex="18" onkeypress="return(Formata_Dados(this,'','',event,'DDD'))" value="<? echo $dddtelres; ?>"><input type="text" class="textbox8b" name="telres" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="8" tabIndex="19" onkeypress="return(Formata_Dados(this,'','',event,'TEL'))" value="<? echo $telres; ?>"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>(DDD) Celular:</td>
		<td><input type="text" class="textbox9" name="dddcel" size="3" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="3" tabIndex="20" onkeypress="return(Formata_Dados(this,'','',event,'DDD'))" value="<? echo $dddcel; ?>"><input type="text" class="textbox8b" name="cel" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="8" tabIndex="21" onkeypress="return(Formata_Dados(this,'','',event,'TEL'))" value="<? echo $cel; ?>"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap><span>*</span>E-mail:</td>
		<td><input type="text" class="textbox8" name="email" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="22" value="<? echo $email; ?>"></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
		
		<tr>
		<td class="textblk2" align="right" nowrap><span>*</span>Empresa:</td>
		<td>
			<select class="textbox8e" name="cd_cliente" id="cd_cliente" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" tabIndex="23" onchange="sendRequestPrograma(this[this.selectedIndex].value)">
				<option>Selecione uma empresa</option>
				<?php 
					
					$sql = "SELECT CD_CLIENTE, NM_CLIENTE FROM col_cliente WHERE IN_ATIVO = 1";
					$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
					while($oRs = mysql_fetch_row($query)){
						if ($oRs[0] == $cd_cliente){
							echo "<option value='$oRs[0]' selected>$oRs[1]</option>";	
						}else{
							echo "<option value='$oRs[0]'>$oRs[1]</option>";	
						}
					}
				?>
			</select>
		
		</td>
		</tr>
		<tr>	
		
		<td class="textblk2" align="right" nowrap><span>*</span>Programa:</td>
		<td>
<?php
//if($id != ''){
//	echo '<input type="text" class="textbox8" name="nomeempresa" size="40" onfocus="this.style.backgroundColor=\'#ffc\'" onblur="this.style.backgroundColor=\'#fff\'" maxlength="50" tabIndex="23" value="'.$nomeempresa.'" readonly>';
//}
//else{
	echo '<select class="textbox8c" name="cd_empresa" id="nomeempresa" onfocus="this.style.backgroundColor=\'#ffc\'" onblur="this.style.backgroundColor=\'#fff\'" tabIndex="23" onchange="sendRequest(this[this.selectedIndex].value)">';
	//echo '<option value="0">Selecione uma Empresa</option>';
	
	$sql = "SELECT CD_EMPRESA, DS_EMPRESA FROM col_empresa WHERE IN_ATIVO = 1 AND CD_CLIENTE = $cd_cliente ORDER BY DS_EMPRESA";
	$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
	while($oRs = mysql_fetch_row($query)){
		if ($oRs[0] == $cd_empresa){
			echo "<option value='$oRs[0]' selected>$oRs[1]</option>";	
		}else{
			echo "<option value='$oRs[0]'>$oRs[1]</option>";	
		}
		
		
	}
	echo '</select>';
//}
?>
		</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap><span>*</span>Lota��o:</td>
		<td>
<?php
//if($id != ''){
//	echo '<input type="text" class="textbox8" name="lotacao" size="40" onfocus="this.style.backgroundColor=\'#ffc\'" onblur="this.style.backgroundColor=\'#fff\'" maxlength="50" tabIndex="24" value="'.$lotacao.'" readonly>';
//}
//else{
	echo '<select class="textbox8e" name="cd_lotacao" id="lotacao" onfocus="this.style.backgroundColor=\'#ffc\'" onblur="this.style.backgroundColor=\'#fff\'" tabIndex="24">';
if ($id != ''){
	
	$sql = "SELECT CD_LOTACAO, DS_LOTACAO FROM col_lotacao WHERE IN_ATIVO = 1 AND CD_EMPRESA = $cd_empresa ORDER BY DS_LOTACAO";
	$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());	

	while($oRs = mysql_fetch_row($query))
	{
		if ($oRs[0] == $cd_lotacao){
			echo "<option value='$oRs[0]' selected>$oRs[1]</option>";	
		}else{
			echo "<option value='$oRs[0]'>$oRs[1]</option>";	
		}
	}

}
	echo '</select>';
//}
?>
		</td>
		</tr>
		
		
		<tr>
		<td class="textblk2" align="right" nowrap><span></span>Localidade:</td>
		<td>
<?php

	echo '<select class="textbox8e" name="cd_localidade" id="localidade" onfocus="this.style.backgroundColor=\'#ffc\'" onblur="this.style.backgroundColor=\'#fff\'" tabIndex="24">';
if ($id != ''){
	
	$sql = "SELECT CD_LOCALIDADE, NM_LOCALIDADE FROM col_localidade WHERE IN_ATIVO = 1 AND CD_EMPRESA = $cd_empresa ORDER BY NM_LOCALIDADE";
	$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());	

	echo "<option value='-1'>Selecione uma Localidade</option>";
	
	while($oRs = mysql_fetch_row($query))
	{
		if ($oRs[0] == $cd_localidade){
			echo "<option value='$oRs[0]' selected>$oRs[1]</option>";	
		}else{
			echo "<option value='$oRs[0]'>$oRs[1]</option>";	
		}
	}

}
	echo '</select>';
//}
?>
		</td>
		</tr>
		
		
		
		
		<tr>
		<td class="textblk2" align="right" nowrap><span>*</span>Cargo/Fun��o:</td>
		<td><input type="text" class="textbox8" name="cargofuncao" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="50" tabIndex="25" value="<? echo $cargofuncao; ?>"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Endere�o com.:</td>
		<td><input type="text" class="textbox8" name="enderecocom" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="26" value="<? echo $enderecocom; ?>"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>CEP com.:</td>
		<td class="textblk2"><input type="text" class="textbox8b" name="cepcom" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="10" tabIndex="27" onkeypress="return(Formata_Dados(this,'.','-',event,'CEP'))" value="<? echo $cepcom; ?>">&nbsp;(apenas n�meros)</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>(DDD) Telefone com.:</td>
		<td class="textblk2"><input type="text" class="textbox9" name="dddtelcom" size="3" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="3" tabIndex="28" onkeypress="return(Formata_Dados(this,'','',event,'DDD'))" value="<? echo $dddtelcom; ?>"><input type="text" class="textbox8b" name="telcom" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="8" tabIndex="29" onkeypress="return(Formata_Dados(this,'','',event,'TEL'))" value="<? echo $telcom; ?>">&nbsp;Ramal&nbsp;<input type="text" class="textbox8b" name="ramal" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="8" tabIndex="30" onkeypress="return(Formata_Dados(this,'','',event,'TEL'))" value="<? echo $ramal; ?>"></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
		<td class="textblk2" style="vertical-align:top;text-align:right" nowrap><span>*</span>Experi�ncia:</td>
		<td class="textblk2">
			<select class="textbox8c" name="experiencia[]" id="experiencia" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" multiple tabIndex="31">
			<option <?php echo checkIfOptionExperienciaIsSelected('Planejamento Estrat�gico'); ?>>Planejamento Estrat�gico
			<option <?php echo checkIfOptionExperienciaIsSelected('Planejamento de Marketing'); ?>>Planejamento de Marketing
			<option <?php echo checkIfOptionExperienciaIsSelected('Planejamento Operacional'); ?>>Planejamento Operacional
			<option <?php echo checkIfOptionExperienciaIsSelected('Avalia��o Financeira'); ?>>Avalia��o Financeira
			<option <?php echo checkIfOptionExperienciaIsSelected('Ger�ncia de Servi�o'); ?>>Ger�ncia de Servi�o
			<option <?php echo checkIfOptionExperienciaIsSelected('Vendas Complexas'); ?>>Vendas Complexas
			<option <?php echo checkIfOptionExperienciaIsSelected('Vendas Pequenas Empresas'); ?>>Vendas Pequenas Empresas
			<option <?php echo checkIfOptionExperienciaIsSelected('Vendas Varejo'); ?>>Vendas Varejo
			<option <?php echo checkIfOptionExperienciaIsSelected('Projeto e Gest�o'); ?>>Projeto e Gest�o
			<option <?php echo checkIfOptionExperienciaIsSelected('Desenvolvimento de Conte�do'); ?>>Desenvolvimento de Conte�do
			<option <?php echo checkIfOptionExperienciaIsSelected('M�todos e Ferramentas de Aprendizado'); ?>>M�todos e Ferramentas de Aprendizado
			<option <?php echo checkIfOptionExperienciaIsSelected('Otimiza��o de Processos'); ?>>Otimiza��o de Processos
			<option <?php echo checkIfOptionExperienciaIsSelected('Instrutoria'); ?>>Instrutoria
			<option <?php echo checkIfOptionExperienciaIsSelected('Intelig�ncia Competitiva'); ?>>Intelig�ncia Competitiva
			<option <?php echo checkIfOptionExperienciaIsSelected('Projetos TIC'); ?>>Projetos TIC
			</select><br />Para selecionar mais de um item, tecle CTRL + clique.<br /><br /><a href="javascript:void(0)" onclick="this.parentNode.childNodes[0].selectedIndex = -1" title="   Limpar itens selecionados acima   "><img src="/images/layout/bt_clearselect.gif" style="width:23px;height:23px;margin-right:5px" align="absmiddle"></a>Limpar itens selecionados acima<br /><br />
		</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Outros:</td>
		<td><input type="text" class="textbox8" name="outrosexperiencia" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="32" value="<? echo $outrosexperiencia; ?>"></td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
		<td class="textblk2" style="vertical-align:top;text-align:right" nowrap><span>*</span>Especializa��o:</td>
		<td class="textblk2">
			<select class="textbox8c" name="especializacao[]" id="especializacao" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" multiple tabIndex="33">
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Converg�ncia IP'); ?>>Converg�ncia IP
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Converg�ncia Fixo-M�vel'); ?>>Converg�ncia Fixo-M�vel
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Mobilidade'); ?>>Mobilidade
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Wireless'); ?>>Wireless
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Tecnologia de Acesso'); ?>>Tecnologia de Acesso
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Telefonia Fixa'); ?>>Telefonia Fixa
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Telefonia M�vel'); ?>>Telefonia M�vel
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Integra��o de Voz e Dados'); ?>>Integra��o de Voz e Dados
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Seguran�a de Redes'); ?>>Seguran�a de Redes
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Call Center/Contact Center'); ?>>Call Center/Contact Center
			<option <?php echo checkIfOptionEspecializacaoIsSelected('Regulamenta��o'); ?>>Regulamenta��o
			</select><br />Para selecionar mais de um item, tecle CTRL + clique.<br /><br /><a href="javascript:void(0)" onclick="this.parentNode.childNodes[0].selectedIndex = -1" title="   Limpar itens selecionados acima  "><img src="/images/layout/bt_clearselect.gif" style="width:23px;height:23px;margin-right:5px" align="absmiddle"></a>Limpar itens selecionados acima<br /><br />
		</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Outros:</td>
		<td><input type="text" class="textbox8" name="outrosespecializacao" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="34" value="<? echo $outrosespecializacao; ?>"></td>
		</tr>
		<tr><td><br /></td></tr>
<?php
		if($fotofile != '')
			echo '<tr><td class="textblk2" align="right" nowrap>Foto anexada:</td><td class="textblk2">'.$fotofile.'</td></tr>';
		if($cvfile != '')
			echo '<tr><td class="textblk2" align="right" nowrap>Curriculum anexado:</td><td class="textblk2">'.$cvfile.'</td></tr>';
?>
		<tr>
		<td class="textblk2" align="right" nowrap>Anexar foto:</td>
		<td><input type="file" class="textbox8e" name="foto" size="26" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="100" tabIndex="35"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Anexar Curriculum:</td>
		<td><input type="file" class="textbox8e" name="cv" size="26" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="100" tabIndex="36"></td>
		</tr>
		
		<tr><td><br /></td></tr>
		
		<tr>
		<td class="textblk2" align="right"><span>*</span><input type="checkbox" name="policyagreement" tabIndex="37" <? echo ($policyagreement == 1 ? 'checked' : ''); ?>></td>
		<td class="textblk2">Concordo com os termos de privacidade.</td>
		</tr>
		<!-- <tr>
		<td class="textblk2" align="right"><input type="checkbox" name="newsletteragreement" tabIndex="38" <? echo ($newsletteragreement == 1 ? 'checked' : ''); ?>></td>
		<td class="textblk2">Desejo receber regularmente o newsletter da Colaborae.</td>
		</tr> -->
		
		<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>
		<tr class="tarjaTitulo"><td colspan="2"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
		<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>
		<tr>
		<td class="title" align="right" width="1%">Login:&nbsp;</td>
		<td class="textblk2"><input name="userlogin" type="text" class="textbox2" style="width: 150px" maxlength="20" tabIndex="39" value="<?php echo $login; ?>">&nbsp;(entre&nbsp;6&nbsp;e&nbsp;20&nbsp;caracteres)</td>
		</tr>
		<tr>
		<td class="title" align="right" width="1%">Senha:&nbsp;</td>
		<td class="textblk2"><input name="userpass1" type="password" class="textbox2" maxlength="8" tabIndex="40" value="<?php echo $senha; ?>"> (min 6 caracteres, max 8 caracteres)</td>
		</tr>
		<tr>
		<td class="title" align="right" width="1%">Confirmar&nbsp;Senha:&nbsp;</td>
		<td><input name="userpass2" type="password" class="textbox2" maxlength="8" tabIndex="41" value="<?php echo $senha; ?>"></td>
		</tr>
		<tr>
		<td class="title" align="right" width="1%">Tipo usu�rio:&nbsp;</td>
		<td><select name="tipo" tabIndex="42"><option value="0" selected>Selecione o Tipo de Usu�rio</option>
<?
$sql = "SELECT ID, NM_TIPO_USU FROM tb_tipo_usuario";
$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

while($oRs = mysql_fetch_row($RS_query))
{
	$selected = $oRs[0] == $tipo ? 'selected' : '';
	echo "<option value=\"" . $oRs[0] . "\" $selected>" . $oRs[1] . "</option>";
}

?>
		</select></td>
		</tr>
		<tr>
			<td class="title" align="left" width="1%">
				&nbsp;
			</td>
			<td class="title">
			<input type="checkbox" name="chkAlteraSenha" id="chkAlteraSenha" value="1" /> Solicitar Alterar Senha
			</td>
			
		</tr>
		
		<tr>
			<td class="title" align="right" width="1%">Filtro</td>
			<td><select name="filtro" tabIndex="43">
				<option value="0" selected>Selecione o Filtro do Usu�rio</option>
<?
$sql = "SELECT CD_FILTRO, NM_FILTRO FROM col_filtro";
if ($cd_empresa > 0 && $cd_empresa != 5)
{
	$sql = "$sql WHERE CD_EMPRESA = $cd_empresa";
}

$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

while($oRs = mysql_fetch_row($RS_query))
{
	$selected = $oRs[0] == $filtro ? 'selected' : '';
	echo "<option value=\"" . $oRs[0] . "\" $selected>" . $oRs[1] . "</option>";
}

?>
			</select></td>
		</tr>
		
		<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>
		<tr class="tarjaTitulo"><td colspan="2"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
		<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>
		
		<tr>
		<td colspan="2" align="center"><input class="buttonsty8" type="button" value="<?php if($id == "") echo "Gravar"; else echo "Atualizar"; ?>" onclick="checkFormFields()" tabIndex="43">&nbsp;&nbsp;<input class="buttonsty8" type="reset" name="Reset" value="Limpar" onblur="document.cadastroForm.nome.focus()" tabIndex="44"><br /><br /></td>
		</tr>
		<input type="hidden" name="id" value="<?php echo $id; ?>">
<?php
if($id != ''){
	//echo '<input type="hidden" name="cd_empresa" value="'.$cd_empresa.'">';
	//echo '<input type="hidden" name="cd_lotacao" value="'.$cd_lotacao.'">';
}
?>
		<input type="hidden" name="fotofileuploaded" value="<?php echo $fotofile; ?>">
		<input type="hidden" name="cvfileuploaded" value="<?php echo $cvfile; ?>">
		</form>
		</table>

<br><br>
</body>
</html>
<?php
mysql_close();
?>
