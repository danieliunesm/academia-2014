<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_controle_assessment();
}


function carregarRN(){
	
	if (isset($_REQUEST["id"])) {
		page::$rn->CD_EMPRESA = $_REQUEST["id"];
	}
	else {
		page::$rn->CD_EMPRESA = $_REQUEST["cboEmpresa"];
	}

    page::$rn->assessments = array();

    page::$rn->gruposDisciplinas = array();

	foreach($_POST["cboProva"] as $prova){
        $assessment = new col_assessment();
        $assessment->CD_EMPRESA = page::$rn->CD_EMPRESA;
        $assessment->CD_PROVA = $prova;
        page::$rn->assessments[] = $assessment;
    }

	foreach($_POST as $chave=>$valor) {
		
		if (substr($chave,0,23) == "txtNomeGrupoDisciplinas") {
			$idCampo = substr($chave,23);
			$grupoDisciplina = new col_grupo_disciplina();
			$grupoDisciplina->CD_EMPRESA = page::$rn->CD_EMPRESA;
			$grupoDisciplina->CD_GRUPO_DISCIPLINA = $_POST["hdnCodigoGrupoDisciplina$idCampo"];
			$grupoDisciplina->NM_GRUPO_DISCIPLINA = $_POST["txtNomeGrupoDisciplinas$idCampo"];
            $grupoDisciplina->SG_GRUPO_DISCIPLINA = $_POST["txtSiglaGrupoDisciplinas$idCampo"];
            $grupoDisciplina->NR_ORDEM_DISCIPLINA = $_POST["txtOrdemGrupoDisciplinas$idCampo"];
			$grupoDisciplina->disciplinas = array();
			
			foreach($_POST["cboDisciplina$idCampo"] as $codigoDisciplina) {
				$disciplinaGrupoDisciplina = new col_disciplina_grupo_disciplina();
				$disciplinaGrupoDisciplina->CD_DISCIPLINA = $codigoDisciplina;
				$grupoDisciplina->disciplinas[] = $disciplinaGrupoDisciplina;
			}
			
			page::$rn->gruposDisciplinas[] = $grupoDisciplina;
		}
		
	}
	
	//if (count($_POST) > 0 && (isset($_POST["btnSalvar"]))) {
	//	exit();
	//}
}

function pagePreRender() {
	if (count($_POST) > 0 && (!isset($_POST["btnSalvar"]))) {
		instanciarRN();
		carregarRN();
	}
}

function pageRenderEspecifico(){
?>

					<TR>

						<TD class="textblk">Programa: *</TD>
					</TR>
					<TR>
						<TD><?php comboboxEmpresa("cboEmpresa", page::$rn->CD_EMPRESA, "Programa", true, false,"textbox3", !isset($_REQUEST["id"])); ?></TD>
					</TR>
					
					<TR>
						<TD colspan="2">&nbsp;</TD>
					</TR>
					<TR>
						<TD class="textblk">Avalia��o Inicial: *</TD>
					</TR>
<?php
    $provas = array();
    if (page::$rn->assessments != null){
        foreach(page::$rn->assessments as $assessment){
           $provas[] =  $assessment->CD_PROVA;
        }
    }
?>
					<TR>
						<TD><?php comboboxProvaEmpresa("cboProva[]",$provas, page::$rn->CD_EMPRESA, false, false, true); ?></TD>
					</TR>
					<TR>
						<TD colspan="2">&nbsp;</TD>
					</TR>
					
					
					
					
					<TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<td colspan="2" class="textblk"><a href="javascript:adicionarGrupoDisciplina()">Adicionar Grupo de Disciplinas</a></td>
					
				</TR>
				<tr>
					<td colspan="2" class="textblk">
					
						<script type="text/javascript" language="javascript">

							var numeroLinha = 1000;
						
							function adicionarGrupoDisciplina(){
							
								var no = document.getElementById("dvGrupoDisciplinas").cloneNode(true);
								var dv = document.getElementById("dvGruposDisciplinas");
								
								//alert(no.all.length)
								
								var itens = no.all.length;
								numeroLinha++;
								for (i=0; i<itens; i++){
									if (no.all[i].type == "text"){
										no.all[i].value = "";
										if(no.all[i].name.substr(0, 7) == "txtNome"){
                                            no.all[i].name = "txtNomeGrupoDisciplinas" + numeroLinha;
                                        }else if (no.all[i].name.substr(0, 8) == "txtSigla"){
                                            no.all[i].name = "txtSiglaGrupoDisciplinas" + numeroLinha;
                                        }else if (no.all[i].name.substr(0, 8) == "txtOrdem"){
                                            no.all[i].name = "txtOrdemGrupoDisciplinas" + numeroLinha;
                                        }
									}else if (no.all[i].type == "select-multiple"){
										no.all[i].selectedIndex = -1;
										no.all[i].name = "cboDisciplina" + numeroLinha + "[]";
										
									}else if (no.all[i].type == "hidden"){
										no.all[i].value = "";
										no.all[i].name = "hdnGrupoDisciplina" + numeroLinha;
										
									}
									//alert(no.all[i].type);
								}
								
								dv.appendChild(no);
								
							}
							
							function removerCiclo(obj){
								if (document.getElementById("dvGruposDisciplinas").children.length==1){
									alert('Deve existir ao menos um grupo de disciplinas.');
									return;
								}
								
								 document.getElementById("dvGruposDisciplinas").removeChild(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
								
								//alert(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
									
							}
						
						
						</script>
					
					
						<div id="dvGruposDisciplinas">
							<?php
								$linhas = 1;
								$gruposDisciplinas = page::$rn->gruposDisciplinas;

								if (is_array($gruposDisciplinas))
								{
									if (count($gruposDisciplinas) > 1)
										$linhas = count($gruposDisciplinas);

                                    usort($gruposDisciplinas, "ordenarGruposDisciplinas");
								}
								
								$i=0;
								for ($i=0; $i < $linhas; $i++)
								{
									$nomeGrupoDisciplina = "";
									$codigoGrupoDisciplina = "";
									
									if (is_array($gruposDisciplinas))
									{
										$grupoDisciplina = $gruposDisciplinas[$i];
										$nomeGrupoDisciplina = $grupoDisciplina->NM_GRUPO_DISCIPLINA;
										$codigoGrupoDisciplina = $grupoDisciplina->CD_GRUPO_DISCIPLINA;
										$siglaGrupoDisciplina = $grupoDisciplina->SG_GRUPO_DISCIPLINA;
                                        $ordemGrupoDisciplina = $grupoDisciplina->NR_ORDEM_DISCIPLINA;
									}
									
								
							
							?>
						
							<div id="dvGrupoDisciplinas" style="border: 1px solid black; margin-top: 2px;">
								<table cellpadding="2" cellspacing="0" width="98%">
									<tr>
										<td class="textblk" >Nome do Grupo de Disciplinas:</td>
										<td rowspan="4" class="textblk" align="center"><input type="image" src="/images/layout/bt_delsession.gif" value="" onclick="javascript:removerCiclo(this)" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'"></td>
									</tr>												
									<tr>
										<td class="textblk" >
                                            <?php textbox("txtOrdemGrupoDisciplinas$i", "2", $ordemGrupoDisciplina, "10%", "textbox3", "Ordem do Grupo de Disciplinas"); ?>
											<?php textbox("txtNomeGrupoDisciplinas$i", "100", $nomeGrupoDisciplina, "70%", "textbox3", "Nome do Grupo de Disciplinas"); ?>
                                            <?php textbox("txtSiglaGrupoDisciplinas$i", "5", $siglaGrupoDisciplina, "15%", "textbox3", "Sigla do Grupo de Disciplinas"); ?>
										</td>
									</tr>
									<tr>
										<td class="textblk" >
											<?php
												$disciplinasGrupoDisciplinas = $_POST["cboDisciplina{$i}"];
												if (count($_POST) == 0 && $grupoDisciplina != null) {
													$disciplinasGrupoDisciplinas = array();
													foreach ($grupoDisciplina->disciplinas as $disciplina) {
														$disciplinasGrupoDisciplinas[] = $disciplina->CD_DISCIPLINA;
													}
												}
											
												comboboxDisciplina("cboDisciplina{$i}[]", $disciplinasGrupoDisciplinas, "Disciplina", true);
											?>	
											<input type="hidden" id="<?php echo "hdnCodigoGrupoDisciplina$i";?>" name="<?php echo "hdnCodigoGrupoDisciplina$i";?>" value="<?php echo $codigoGrupoDisciplina; ?>" />
										</td>
									</tr>
																		
								</table>
							</div>
								<?php
								}
								?>
							
						</div>
						
					</td>
				</tr>
					
					
					
<?php

}

function ordenarGruposDisciplinas($a, $b){

    if ($a->NR_ORDEM_DISCIPLINA == $b->NR_ORDEM_DISCIPLINA)
        return 0;

    return ($a->NR_ORDEM_DISCIPLINA > $b->NR_ORDEM_DISCIPLINA) ? +1:-1;

}


?>