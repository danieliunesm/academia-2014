<?php

	function exibirParametrosFarolCadastro($farol)
	{
		
		//ExibirFiltro($farol->filtro->CD_EMPRESA, $farol->filtro->lotacoes, $farol->filtro->cargos_funcoes, $farol->filtro->usuarios, $farol->filtro->provas, $farol->filtro->foruns, true, true, false, true, true);
		
		exibirParametrosFarol($farol);
		
	}
	
	function exibirParametrosFarolConsulta($exibirEmpresa = true, $codigoEmpresa = 0)
	{
		
		$farol = new col_farol();
		$farol->DT_INICIO = $_REQUEST["txtDe"];
		$farol->DT_TERMINO = $_REQUEST["txtAte"];
		$farol->NM_FAROL = $_REQUEST["txtNomeRelatorio"];
		$farol->NR_PESO_CONTRIBUICAO_FORUM = $_REQUEST["txtPesoContribuicaoForum"];
		$farol->NR_PESO_MEDIA_AVALIACAO = $_REQUEST["txtPesoMediaAvaliacao"];
		$farol->NR_PESO_MEDIA_SIMULADO = $_REQUEST["txtPesoMediaSimulado"];
		$farol->NR_PESO_PARTICIPANTE_AVALIACAO = $_REQUEST["txtPesoParticipanteAvaliacao"];
		$farol->NR_PESO_PARTICIPANTE_SIMULADO = $_REQUEST["txtPesoParticipanteSimulado"];
		$farol->NR_PESO_QUANTIDADE_DOWNLOAD = $_REQUEST["txtPesoQuantidadeDownload"];
		$farol->NR_PESO_QUANTIDADE_HITS = $_REQUEST["txtPesoQuantidadeHits"];
		$farol->NR_PESO_QUANTIDADE_SESSAO = $_REQUEST["txtPesoQuantidadeSessao"];
		$farol->NR_PESO_QUANTIDADE_USUARIO = $_REQUEST["txtPesoQuantidadeUsuario"];
		$farol->NR_PESO_VELOCIDADE_PROVA = $_REQUEST["txtPesoVelocidadeProva"];
		
		if (count($_GET) > 0)
		{
		
			$farol->IN_PESO_CONTRIBUICAO_FORUM = $_REQUEST["chktxtPesoContribuicaoForum"];
			$farol->IN_PESO_MEDIA_AVALIACAO = $_REQUEST["chktxtPesoMediaAvaliacao"];
			$farol->IN_PESO_MEDIA_SIMULADO = $_REQUEST["chktxtPesoMediaSimulado"];
			$farol->IN_PESO_PARTICIPANTE_AVALIACAO = $_REQUEST["chktxtPesoParticipanteAvaliacao"];
			$farol->IN_PESO_PARTICIPANTE_SIMULADO = $_REQUEST["chktxtPesoParticipanteSimulado"];
			$farol->IN_PESO_QUANTIDADE_DOWNLOAD = $_REQUEST["chktxtPesoQuantidadeDownload"];
			$farol->IN_PESO_QUANTIDADE_HITS = $_REQUEST["chktxtPesoQuantidadeHits"];
			$farol->IN_PESO_QUANTIDADE_SESSAO = $_REQUEST["chktxtPesoQuantidadeSessao"];
			$farol->IN_PESO_QUANTIDADE_USUARIO = $_REQUEST["chktxtPesoQuantidadeUsuario"];
			$farol->IN_PESO_VELOCIDADE_PROVA = $_REQUEST["chktxtPesoVelocidadeProva"];
			

		}
			
		if ($exibirEmpresa)
			ExibirFiltroConsulta();
		else
			ExibirFiltroConsulta(false, false, false, false, true, true, false, $codigoEmpresa);
		
		exibirParametrosFarol($farol);
		
	}

	function exibirParametrosFarol($farol)
	{
		
		?>
		
		<TR>
			<TD class="textblk">Programa: *</TD>
		</TR>
		<TR>
			<TD>
				<?php comboboxEmpresa("cboEmpresa", $farol->CD_EMPRESA, "Empresa", false, false); ?>
				<input type="hidden" id="hdnEmpresa" name="hdnEmpresa" value="<?php echo $p_cdEmpresa; ?>" />
			</TD>
		</TR>
		<tr><td colspan="2">&nbsp;</tr>
		
		<tr>
			<td class="textblk">
				Nome do Relat�rio: &nbsp;
			</td>
		</tr>
		<tr>
			<td class="textblk">
				<?php textbox("txtNomeRelatorio", "100", $farol->NM_FAROL, "", "textbox3",false); ?>
			</td>
		</tr>		
		<tr><td colspan="2">&nbsp;</tr>

		<!--
		<tr>
			<td class="textblk">
				Per�odo: &nbsp;
			</td>
		</tr>
		
		<tr>
			<td class="textblk">
				De: <?php textbox("txtDe", "10", $farol->DT_INICIO, "75px", "textbox3",false, Mascaras::Data); ?>
				At�: <?php textbox("txtAte", "10", $farol->DT_TERMINO, "75px", "textbox3",false, Mascaras::Data); ?>
			</td>
		</tr>		
		<tr><td colspan="2">&nbsp;</tr>
		-->
		
		<tr>
			<td class="textblk">
				<b>Pesos:</b> &nbsp;
			</td>
		</tr>
		<tr>
			<td class="textblk">
				<?php labelTextBox("Participa��o", "txtPesoParticipacao", 6, $farol->NR_PESO_PARTICIPACAO, $farol->IN_PESO_PARTICIPACAO,"30%", "textbox3", false, Mascaras::Numero) ?>	
				<?php labelTextBox("Acesso", "txtPesoAcesso", 6, $farol->NR_PESO_ACESSO, $farol->IN_PESO_ACESSO,"30%", "textbox3", false, Mascaras::Numero) ?>				
				<?php labelTextBox("Download", "txtPesoDownload", 6, $farol->NR_PESO_DOWNLOAD, $farol->IN_PESO_DOWNLOAD,"30%", "textbox3", false, Mascaras::Numero) ?>				
				<?php labelTextBox("Participa��o em Simulado", "txtPesoParticipacaoSimulado", 6, $farol->NR_PESO_PARTICIPACAO_SIMULADO, $farol->IN_PESO_PARTICIPACAO_SIMULADO,"30%", "textbox3", false, Mascaras::Numero) ?>				
				<?php labelTextBox("Nota em Simulado", "txtPesoNotaSimulado", 6, $farol->NR_PESO_NOTA_SIMULADO, $farol->IN_PESO_NOTA_SIMULADO,"30%", "textbox3", false, Mascaras::Numero) ?>				
				<?php labelTextBox("Quantidade de Simulado", "txtPesoQuantidadeSimulado", 6, $farol->NR_PESO_QUANTIDADE_SIMULADO, $farol->IN_PESO_QUANTIDADE_SIMULADO,"30%", "textbox3", false, Mascaras::Numero) ?>
				<?php labelTextBox("Participa��o em F�runs", "txtPesoParticipacaoForum", 6, $farol->NR_PESO_PARTICIPACAO_FORUM, $farol->IN_PESO_PARTICIPACAO_FORUM,"30%", "textbox3", false, Mascaras::Numero) ?>
				<?php labelTextBox("Contribui��o em F�rum", "txtPesoContribuicaoForum", 6, $farol->NR_PESO_CONTRIBUICAO_FORUM, $farol->IN_PESO_CONTRIBUICAO_FORUM ,"30%", "textbox3", false, Mascaras::Numero) ?>
				<?php labelTextBox("Participa��o em Avalia��o", "txtPesoParticipacaoAvaliacao", 6, $farol->NR_PESO_PARTICIPACAO_AVALIACAO, $farol->IN_PESO_PARTICIPACAO_AVALIACAO,"30%", "textbox3", false, Mascaras::Numero) ?>
				<?php labelTextBox("Nota em Avalia��o", "txtPesoNotaAvaliacao", 6, $farol->NR_PESO_NOTA_AVALIACAO, $farol->IN_PESO_NOTA_AVALIACAO,"30%", "textbox3", false, Mascaras::Numero) ?>
			</td>
		</tr>		
		<tr><td colspan="2">&nbsp;</tr>
		<tr>
			<td class="textblk">
				Limitar resultado em <font color="Red">(deixe em branco para n�o limitar)</font>: &nbsp;
			</td>
		</tr>
		<tr>
			<td class="textblk">
				<?php textbox("txtLimiteResultado", "10", $farol->NR_LIMITE, "75px", "textbox3",false, Mascaras::Numero); ?>
			</td>
		</tr>		
		<tr><td colspan="2">&nbsp;</tr>
		
<!--		<tr>
			<td class="textblk">
				Exibir Informa��es de: &nbsp;
			</td>
		</tr>
		<tr>
			<td class="textblk">
				<?php //comboboxAgrupamento("cboAgrupamento[]", $farol->IN_TIPO_AGRUPAMENTO); ?>
			</td>
		</tr>		
		<tr><td colspan="2">&nbsp;</tr>
		-->
<?php
	}
	
	
?>