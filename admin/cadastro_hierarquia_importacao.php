<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_hierarquia_importacao();
}

function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/admin/cadastro_hierarquia_importacao.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

function carregarRN(){
	
	$erro = "";

	//Realizacao do upload do arquivo
	$empresa = $_POST["cboEmpresa"];
	$dataAtual = date("YmdHis");
	$nome = $_FILES['fleImportacao']['name'];
	$savefile = "";
	$UPLOAD_BASE_IMG_DIR	= getDocumentRoot()."/admin/importacoes/";
    $nomeArquivo = substr("{$empresa}_{$dataAtual}_{$nome}",0, 100);

	if(is_uploaded_file($_FILES['fleImportacao']['tmp_name'])){
		
		$savefile = "{$UPLOAD_BASE_IMG_DIR}{$nomeArquivo}";
		if(move_uploaded_file($_FILES['fleImportacao']['tmp_name'], $savefile)){
			chmod($savefile, 0666);
		}
	}
	//Fim do upload

	
	//$savefile = "D:\\Zend\\Apache2\\htdocs\\colaborae\\admin\\importacoes\\teste.xml";
	
	try {
		//$dom = DOMDocument::load($_FILES['fleImportacao']['tmp_name']) or die("O arquivo n�o � um XML v�lido");

		/**$dom = DOMDocument::load($savefile) or die("O arquivo n�o � um XML v�lido");
		page::$rn->xml = $dom;**/
		page::$rn->nomeArquivo = $nomeArquivo;
		page::$rn->empresa = $_POST["cboEmpresa"];
        page::$rn->diretorioArquivo = $UPLOAD_BASE_IMG_DIR;

	}catch (Exception $e){
		$erro = "O arquivo n�o � um XML v�lido";
	}
	
	
	
	if ($erro != ""){
		echo $erro;
		pageRender();
		exit();
	}
	

}

function pagePreRender(){

	page::$enctype = "enctype=\"multipart/form-data\"";
	
}

function pageDepoisDeSalvar()
{
	
	resumoImportacao();
	pageRender();
	
	exit();
	
}

function resumoImportacao()
{
	
	//$codigoImportacao = page::$rn->codigoImportacao;
	//$erros = page::$rn->erros;
	
?>

	<table cellpadding="0" cellspacing="0" border="0">
		<tr class="textblk">
		
		<?php 
		
			$mensagem = "A importa��o ocorreu sem erros!";
			//if ($erros != "") $mensagem = "Houve erros na importa��o. <a href=\"relatorio_erro_hierarquia.php?id=$codigoImportacao\" target=\"relatorio\">Clique aqui para ver o resumo dos erros</a>";
		
		?>
		
			<td colspan="2" align="center"><b><?php echo $mensagem;?></b></td>
		</tr>
		
	</table>

<?php 
	
	
	
	
}

function pageRenderEspecifico(){
	
?>	
				<TR><TD>&nbsp;</TD></TR>
				<TR>

						<TD class="textblk">Programa: *</TD>
					</TR>
					<TR>
						<TD><?php comboboxEmpresa("cboEmpresa", null, "Programa"); ?></TD>
					</TR>
					<TR><TD>&nbsp;</TD></TR>
					<TR>
						<TD class="textblk">Arquivo para importa��o:</TD>
					</TR>
					<TR>
						<TD><input type="file" name="fleImportacao" style="width: 100%" TagMensagemObrigatorio="Arquivo para importa��o" /></TD>
					</TR>
					<TR><TD>&nbsp;</TD></TR>
					<TR>
						<TD class="textblk">O arquivo deve ser salvo como planilha XML no Excel e estar no seguinte formato: <br><br>
						1a. Coluna: Identifica��o da Unidade Organizacional <br />
						2a. Coluna:	Nome da Unidade Organizacional <br />
						3a. Coluna: Tipo da Unidade Organizacional <br />
						4a. Coluna: Identifica��o da Unidade Organizacional Superior

						</TD>
					</TR>
<?php

}


?>