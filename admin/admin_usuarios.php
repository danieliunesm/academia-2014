<?
include('filtrocomponente.php');
include "../include/security.php";
include "../include/defines.php";
include "../include/genericfunctions.php";
include('framework/crud.php');
include('controles.php');

if($_SESSION["alias"]!="webmaster") header("Location: /admin/admin_menu.php");

//TratarFiltro();

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<link rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="JavaScript">
//queryfilter = 'e=<? echo $e; ?>&l=<? echo $l; ?>';
//oEmp	    = <? echo $e; ?>;
//oLot 	    = <? echo $l; ?>;
function setFilter(emp,lot){
	oEmp = emp;
	oLot = lot;
	if(oEmp == 999) oLot = 999;
	queryfilter = 'e='+oEmp+'&l='+oLot;
	document.location.href = 'admin_usuarios.php?'+queryfilter;
}

function delPeople()
{
	
	document.forms[0].action = "admin_deleteuser.php";
	document.forms[0].submit();
	
}

function openWindow(url){
	newWin=null;
	var w=520;
	var h=450;
	var l=(screen.width-w)/2;
	var t=(screen.height-h)/2;

	
	
	newWin=window.open(url,'importacaoEditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
	if(newWin!=null)setTimeout('newWin.focus()',100);
}

function ordena(ordenacao){
	//queryfilter = 'e='+oEmp+'&l='+oLot+'&ordenacao=' + ordenacao;
	//document.location.href = 'admin_usuarios.php?'+queryfilter;
	document.forms[0].action = "admin_usuarios.php?ordenacao=" + ordenacao;
	document.getElementById("hdnPesquisa").value = "Filtrar";
	document.forms[0].submit();
}

function exportarExcel(){
	
	var action = document.forms[0].action;
	document.forms[0].action = "relatorio_cadastro.php"
	document.forms[0].submit();
	document.forms[0].action = action;
	
}

</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><span class="title">USU�RIO:&nbsp;</span><?php echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="1"></td></tr>
</table>
<br><br>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr class="tarjaTitulo">
	<td height="20" colspan="12" align="center">GER�NCIA DE CADASTRO</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
<tr>
<td colspan="12">
	<form name="usuariosForm" method="post">
	<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
	<tr><td colspan="7" class="title"><a href="admin_formcadastro.php" onfocus="noFocus(this)"><img src="/images/layout/bt_addpeople.gif" width="20" height="20" border="0" vspace="10" align="absmiddle"><span class="title">&nbsp;Incluir Usu�rio</span></a>
		<td colspan="5" class="title" align="right">
			<a href="javascript:openWindow('cadastro_usuario_importacao.php')" onfocus="noFocus(this)"><img src="/images/layout/excel2.jpg" width="20" height="20" border="0" vspace="10" align="absmiddle"><span class="title">&nbsp;Rotinas em Lote</span></a>&nbsp;
			<a href="javascript:openWindow('backup_usuario.php')" onfocus="noFocus(this)"><img src="/images/layout/backup.jpg" width="20" height="20" border="0" vspace="10" align="absmiddle"><span class="title">&nbsp;Backup de Usu�rios</span></a>
		</td></tr>
	</table>
	<table border="0" cellpadding="0" cellspacing="0" width="756">
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr class="tarjaTitulo"><td colspan="12"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
	</table>
	
	<table border="0" cellpadding="0" cellspacing="0" align="center">

	<?php
		ExibirFiltroConsulta(false, false, true, true, true, false)
	?>
	
	<tr><td colspan="2">&nbsp;</tr>
		<tr>
			<td class="textblk">
				Tipo Usu�rio
			</td>
		</tr>
		<tr>
			<td>
				
				<?php
					$sql = "SELECT ID, NM_TIPO_USU FROM tb_tipo_usuario";
					$RS_query = DaoEngine::getInstance()->executeQuery($sql,true);
					
					echo "<select id='cboTipoUsuario[]' name='cboTipoUsuario[]' multiple class=\"select3\">";
					
					while($oRs = mysql_fetch_row($RS_query))
					{
						
						$selected = "";
						foreach ($_POST["cboTipoUsuario"] as $tipoUsuario)
						{
							if ($oRs[0] == $tipoUsuario)
							{
								$selected = "selected";
								break;
							}
						}
						
						 
						echo "<option value=\"" . $oRs[0] . "\" $selected>" . $oRs[1] . "</option>";
					}
					
					echo "</select>";
					
				?>
			
			</td>
		</tr>
	
	<tr><td colspan="2">&nbsp;</tr>
	<tr>
		<td colspan="2" align="center"><input type='submit' name='btnPesquisa' class='buttonsty' value='Filtrar'>
		<input type='submit' name='btnExcel' onclick="exportarExcel();return false;" class='buttonsty' value='Excel'>
		<input type="hidden" name="hdnPesquisa" id="hdnPesquisa" /></td>
	</tr>
	
	</table>

<p></p>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">

	<?php
	
		if ($_POST["btnPesquisa"] == 'Filtrar' || $_POST["hdnPesquisa"] == "Filtrar" || $_POST["btnExcel"] == "Excel")
		{
			include "../include/dbconnection.php";
			
			$whereEmp = ($e != 999) ? "AND U.empresa = $e" : "";
			$whereLot = ($l != 999) ? "AND U.lotacao = $l" : "";
			
			$idOrdenacao = 0;
			
			if (isset($_GET["ordenacao"]))
			{
				$idOrdenacao = $_GET["ordenacao"];
			}
			
			switch ($idOrdenacao)
			{
				case 1:
					$ordenacao = "L.DS_LOTACAO";
					break;
				case 2:
					$ordenacao = "U.cargofuncao";
					break;
				case 3:
					$ordenacao = "U.login";
					break;
				case 4:
					$ordenacao = "U.datacadastro";
					break;
				default:
					$ordenacao = "U.NM_USUARIO";
					break;
			}
			
			$sqlWhere = "";
			
			if ($_POST["cboEmpresa"] > -1)
			{
				$sqlWhere = " AND U.empresa = {$_POST["cboEmpresa"]}";
			}
			
			if (is_array($_POST["cboLotacao"]))
			{
				
				$lotacoes = join(",", $_POST["cboLotacao"]);
				
				$sqlWhere = "$sqlWhere AND U.lotacao IN ($lotacoes) ";
			}
			
			if (is_array($_POST["cboCargo"]))
			{
				
				$cargos = join("','", $_POST["cboCargo"]);
				$cargos = "'$cargos'";
				$sqlWhere = "$sqlWhere AND U.cargofuncao IN ($cargos) ";
			}
			
			if (is_array($_POST["cboTipoUsuario"]))
			{
				$tipos = join(",", $_POST["cboTipoUsuario"]);
				$sqlWhere = "$sqlWhere AND U.tipo IN ($tipos) ";
			}
			
			$sql = "SELECT
						U.CD_USUARIO, U.NM_USUARIO, U.NM_SOBRENOME,
						U.login, U.datacadastro, T.NM_TIPO_USU, T.LV_ACESSO,
						L.DS_LOTACAO, U.cargofuncao
					FROM
						col_usuario U
						INNER JOIN tb_tipo_usuario T ON U.tipo = T.ID
						LEFT OUTER JOIN col_lotacao L ON U.lotacao = L.CD_LOTACAO
					WHERE (1=1 $sqlWhere) AND Status = 1 ORDER BY $ordenacao";
			
			$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
			
			echo "<!-- $sql -->";
		
	?>
<tr>
	<td colspan="14" class="textblk">Registros Encontrados: <?php echo mysql_num_rows($RS_query) ?></td>
</tr>
<tr>
	<td width="1%"></td>
	<td width="33%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="15%"><img src="/images/layout/blank.gif" width="100" height="1"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="15%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="15%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="15%"><img src="/images/layout/blank.gif" width="110" height="1"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	
	<td width="15%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	
	<td width="1%"></td>
</tr>
<tr class="tarjaItens">
	<td><img src="/images/layout/blank.gif" width="10" height="25"></td>
	<td align="center" class="title"><span onclick="javascript:ordena(0);" style="cursor: hand">USU�RIO</span></td>
	<td></td>
	<td align="center" class="title"><span onclick="javascript:ordena(3);" style="cursor: hand">LOGIN</span></td>
	<td></td>
	<td align="center" class="title"><span onclick="javascript:ordena(1);" style="cursor: hand">LOTA��O</span></td>
	<td></td>
	<td align="center" class="title"><span onclick="javascript:ordena(2);" style="cursor: hand">CARGO/FUN��O</span></td>
	<td></td>
	<td align="center" class="title" nowrap><span onclick="javascript:ordena(4);" style="cursor: hand">DATA INCLUS�O</span></td>
	<td></td>
	<td align="center" class="title" nowrap>DETALHES</td>
	<td></td>
	<td align="center" class="title">&nbsp;&nbsp;Excluir&nbsp;&nbsp;</td>
</tr>
<?
//$sql = "SELECT L.ID, L.NM_USU, L.NM_LOGIN, L.DT_INCLUSAO, U.NM_TIPO_USU, U.LV_ACESSO FROM tb_login L, tb_tipo_usuario U WHERE L.TIPO_USU = U.LV_ACESSO ORDER BY L.NM_USU";


$iCont = 0;
$numeroLinha = 0;
$bgcolor = "#ffffff";
while($oRs = mysql_fetch_row($RS_query))
{
	$numeroLinha++;
?>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
<tr bgcolor="<? echo $bgcolor; ?>">
	<td  class="textblk"><?php echo $numeroLinha; ?>&nbsp;</td>
	<td class="textblk"><? echo $oRs[1] . " " . $oRs[2]; ?></td>
	<td></td>
	<td align="center" class="textblk"><? echo $oRs[3]; ?></td>
	<td></td>
	<td align="center" class="textblk"><? echo $oRs[7]; ?></td>
	<td></td>
	<td align="center" class="textblk"><? echo $oRs[8]; ?></td>
	<td></td>
	<td align="center" class="dataarquivos"><? echo FmtData($oRs[4]); ?></td>
	<td></td>
	
	<td align="center"><a href="admin_formcadastro.php?id=<? echo $oRs[0]; ?>" onfocus="noFocus(this)"><img src="/images/layout/bt_edit.gif" width="20" height="20" border="0"></a></td>
	<td></td>
	
	<td align="center">
<?
	//if(!($oRs[3]==1 && $oRs[2]=="admin"))
	if(!($oRs[3]=="webmaster"))
	{
?>
	<input type="checkbox" name="userID<? echo $oRs[0]; ?>" value="<? echo $oRs[0]; ?>">
<?
	}
?>
	</td>
</tr>
<?
	$iCont++;
	if($iCont % 2 == 0) $bgcolor="#ffffff";
	else $bgcolor="#f1f1f1";
}
?>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
<tr class="tarjaTitulo"><td colspan="14"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
	<td colspan="12" align="right" class="textblk">Excluir Usu�rios selecionados&nbsp;</td>
	<td><img src="/images/layout/seta.gif" width="10" height="10" border="0" align="absmiddle"></td>
	<td align="center"><input type="image" onfocus="noFocus(this)" src="/images/layout/bt_delpeople.gif" width="20" height="20" border="0" onclick="delPeople(); return false;"></td>
</tr>
	<?php
				mysql_free_result($RS_query);
				mysql_close();
			}
	?>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr class="tarjaTitulo"><td colspan="14"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="40"></td></tr>
<tr><td colspan="14" align="center"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"></td></tr>
</table>
<br><br></form>
<?

?>
</body>
</html>
