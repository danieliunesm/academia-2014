<?php
include "../include/security.php";
include "../include/genericfunctions.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Colabor� - Consultoria e Educa&ccedil;&atilde;o Corporativa</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META http-equiv="pragma" content="no-cache">
		<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">
		<script type="text/javascript" src="include/js/functions.js"></script>
		<script language="javascript">

            function ShowDialog(pagePath, args, width, height, left, top){
                return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
            }
            
             var oVsession = "";
             var inativos = 0;
        
		     function viewControl(vSession){
                oVsession = vSession;
                if (document.getElementById("hdnRemovidos").value != ""){
                    inativos = document.getElementById("hdnRemovidos").value;
                }
                
                var html = ShowDialog("modoVisualizacao.htm", window, 420, 210);
                if(html!=null){
                    document.getElementById("hdnRemovidos").value = html;
                    document.forms[0].submit();
                }
            }
        
			var paginaEdicao = '<?php echo $paginaEdicao; ?>';
			
			<?php
				if (!isset($paginaEdicaoLargura)){
					$paginaEdicaoLargura = 450;
				}
				
				if (!isset($paginaEdicaoAltura)){
					$paginaEdicaoAltura = 280;
				}
			?>
		
			function noFocus(obj){if(obj.blur())obj.blur()}
		
			function openWindow(url){
				newWin=null;
				var w=<?php echo $paginaEdicaoLargura; ?>;
				var h=<?php echo $paginaEdicaoAltura; ?>;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				newWin=window.open(url,'menueditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
				if(newWin!=null)setTimeout('newWin.focus()',100);
			}
			
			function getURL(){
				getURL(null);
			}
			
			function getURL(id){
				if(id != null){
					id = "?id=" + id;
				}else{
					id = "";
				}
				
				openWindow(paginaEdicao + id);
				
			}
		
		</script>		
	</HEAD>
	
	<BODY>
	<!-- Inicio do T�tulo -->
		<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
			<TR>
				<TD><IMG height="2" src="images/blank.gif" width="100%"/></TD>
			</TR>
			<TR>
				<TD background="images/bg_logo_admin.png">
					<TABLE cellpadding="0" cellspacing="0" width="663" background="images/logo_admin.png" border="0">
						<TR>
							<TD><IMG src="images/blank.gif" height="32" width="1"/></TD>
							<TD class="data" align="right"><?php echo getServerDate(); ?></TD>
						</TR>
					
					</TABLE>
				
				</TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" width="100%" height="2" /></TD>
			</TR>
			<TR>
				<TD bgcolor="#cccccc"><IMG src="images/blank.gif" height="3" width="100%"/></TD>
			</TR>
		</TABLE>
		<!-- Fim do T�tulo -->
		
		<!-- In�cio Cabe�alho -->
		<TABLE cellspacing="0" cellpadding="0" width="756" align="center" border="0">
			<TR>
				<TD width="1%"><IMG height="20" src="images/blank.gif" width="289"/></TD>
				<TD></TD>
				<TD></TD>
			</TR>
			<TR valign="top">
				<TD class="textblk"><SPAN class="title">USU�RIO: </SPAN><?php echo $_SESSION['alias']; ?></TD>
				<TD width="1%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.replace('logout.php')"
									type="button" value="Logout" /></TD>
				<TD align="right" width="98%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href='admin_menu.php?'"
							type="button" value="Voltar"></TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" height="4" width="1" /></TD>
			</TR>
			<TR>
				<TD class="textblk" colSpan="2"><SPAN class="title">P�GINA ATUAL:</SPAN>
						<?php echo TITULO_PAGINA; ?></TD>
			</TR>
			<TR>
				<TD><IMG height="1" src="images/blank.gif" width="280"></TD>
			</TR>
		</TABLE>
		<BR>
		<!-- Fim Cabe�alho -->
		
		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="post">
			
            <input type="hidden" name="hdnRemovidos" value="<?php echo $_POST["hdnRemovidos"] ?>" />
            		
			<TABLE cellpadding="0" cellspacing="0" width="756" align="center" border="0">
				<TR class="tarjaTitulo">
						<TD align="middle" height="20"><?php echo TITULO_PAGINA; ?></TD>
				</TR>
				<TR>
					<TD class="title" width="100%">
						<A onfocus="noFocus(this)" onclick="viewControl('<?php echo TITULO_PAGINA; ?>');return false" style="cursor: hand;">
							<SPAN class="title" style="width: 49%"><IMG height="26" src="images/bt_all.gif" width="22" align="absMiddle" border="0" /> <?php echo BOTAO_MODO_EXIBICAO ?> </SPAN></A>
						
						<A onfocus="noFocus(this)" onclick="getURL();return false" style="cursor: hand;">
							<SPAN class="title" style="width: 49%"><IMG height="20" src="images/bt_novo.gif" width="20" align="absMiddle" vspace="10"
									border="0" /><?php echo BOTAO_ADICIONAR ?></SPAN></A>							
					</TD>
				</TR>
				<TR>
					<TD><IMG src="images/blank.gif" height="1" width="10"/></TD>
				</TR>
				<TR>
					<TD width="100%">
