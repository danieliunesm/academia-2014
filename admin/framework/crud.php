<?php

class ParametroDB{
	
	public $nomeCampo;
	public $valorParametro;
	
	public function ParametroDB($nomeCampo, $valorParametro){
		
		$this->nomeCampo = $nomeCampo;
		$this->valorParametro = $valorParametro;
		
	}
	
}


class DaoEngine {

	const NUMERO = "numero";
	const TEXTO = "texto";
	const DATA = "data";
	const DATA_HORA_MINUTO = "data_hora_minuto";
	const ATIVO = "ativo";
	const COLECAO = "colecao";
	
	private static $me = null;
	private $donoConexao = null;
	
	public static function getInstance(){
		
		if (DaoEngine::$me == null)
			DaoEngine::$me = new DaoEngine();
			
		return DaoEngine::$me;
	}
	
	private function push($array, $item){
		
        if(is_array($array))
        {
            $array[] = $item;
        }
        else 
        {
        	$array = array();
            array_push($array, $item);
        }
		
        return $array;
	}
	
	
	public function inserirObjeto($objeto)
	{
		
		$classe = new ReflectionClass(get_class($objeto));
        
        $tabela = $this->obterTabela($classe->getDocComment());    
		
		$propriedades = $classe->getProperties();  //get_class_vars(get_class($objeto));
		$colunas = "";
		$valores = "";
		$propriedadeIdentity = null;
		$colecao = null;
		$propriedadeChave = null;
		
		foreach($propriedades as $propriedade){
			
			if($propriedade->isPublic()){
				
				$comentario = $propriedade->getDocComment();
				$ehIdentity = $this->ehIdentity($comentario);
				$tipo = $this->obterTipo($comentario);
				$ehChave = $this->ehChave($comentario);
				
				if ($tipo==null){
					continue;
				} else if ($tipo == DaoEngine::COLECAO){
					$colecao = $this->push($colecao, $propriedade->getValue($objeto));
					continue;
				}

				
				
				if ($ehIdentity){
					if (!($this->identityInsert($comentario))){
						$propriedadeIdentity = $propriedade;
						continue;
					}
				}elseif ($ehChave){
					$propriedadeChave = $propriedade;
				}
					
				if ($colunas != ""){
					$colunas = "$colunas, ";
					$valores = "$valores, ";
				}
				
				$nomePropriedade = $propriedade->getName();
				$colunas = "$colunas$nomePropriedade";
				$valor = $this->obterParametro($propriedade->getValue($objeto), $tipo);
				$valores = "$valores $valor";
			
			}
			
		}
		
		$sql = "INSERT INTO $tabela ($colunas) VALUES ($valores)";
		
		try{

			$this->iniciarTransacao($objeto);
			
			$this->executeQuery($sql, false);

			$id = null;
			if($propriedadeIdentity != null){
				//ger identity values para o objeto
				$id = mysql_insert_id();
				$propriedadeIdentity->setValue($objeto, $id);
			}
			
			if ($colecao != null){
				
				foreach($colecao as $itemColecao){
					
					foreach($itemColecao as $item){
						$propriedadeValorChave = $propriedadeIdentity;
						if ($propriedadeValorChave == null){
							$propriedadeValorChave = $propriedadeChave;
						}
						$propriedadeItem = new ReflectionProperty(get_class($item), $propriedadeValorChave->getName());
						$propriedadeItem->setValue($item, $propriedadeValorChave->getValue($objeto));
						$item->dao = $this;
						$item->inserir();
					}
					
				}
				
			}

			if ($classe->hasMethod("depoisDeInserir")){
				$objeto->depoisDeInserir();
			}
			
			$this->FinalizarTransacao($objeto);
			
			return $objeto;
			
		}catch (Exception $e){
			$this->CancelarTransacao($objeto);
			throw ($e);
		}
		
	}
	
	public function alterarObjeto($objeto){
		
		$classe = new ReflectionClass(get_class($objeto));
		
		$tabela = $this->obterTabela($classe->getDocComment());
		
		if ($tabela==null){
			throw new Exception("Nome da tabela n�o selecionado.");
		}
		
		$propriedades = $classe->getProperties();
		
		$colunasUpdate = "";
		$colunasWhere = "";
		$colecao = null;
		$propriedadeChave = null;
		
		foreach($propriedades as $propriedade){
			//$propriedade = new ReflectionProperty(get_class($objeto), $nomePropriedade);
			if ($propriedade->isPublic()){
				
				$comentario = $propriedade->getDocComment();
				
				$ehChave = $this->ehChave($comentario);
				$tipo = $this->obterTipo($comentario);
				
				if ($tipo == null){
					continue;
				} else if ($tipo == DaoEngine::COLECAO){
					$colecao = $this->push($colecao, $propriedade->getValue($objeto));
					continue;
				}
				
				$coluna = $propriedade->getName();
				$valor = $this->obterParametro($propriedade->getValue($objeto), $tipo);

				if ($ehChave){
					$propriedadeChave = $propriedade;
					if ($colunasWhere != ""){
						$colunasWhere = "$colunasWhere AND ";
					}
					
					$colunasWhere = "$colunasWhere $coluna = $valor";
					
				}else {
					if ($colunasUpdate != ""){
						$colunasUpdate = "$colunasUpdate, ";
					}
					
					$colunasUpdate = "$colunasUpdate $coluna = $valor";
				}
				
			}
			
		}
		
		$sql = "UPDATE $tabela SET $colunasUpdate WHERE $colunasWhere";
		
		try{

			$this->iniciarTransacao($objeto);
			
			$this->executeQuery($sql, false);

			if ($colecao != null){
				
				foreach($colecao as $itemColecao){

					$excluidos = false;
					
					foreach($itemColecao as $item){
						$propriedadeItem = new ReflectionProperty(get_class($item), $propriedadeChave->getName());
						$propriedadeItem->setValue($item, $propriedadeChave->getValue($objeto));
						$item->dao = $this;
						
						if (!$excluidos){
							$item->excluir();
							$excluidos = true;
						}
						
						
						$item->inserir();
					}
					
				}
				
			}
			
			$this->FinalizarTransacao($objeto);
			
			if ($classe->hasMethod("depoisDeAlterar")){
				$objeto->depoisDeAlterar();
			}
			
		}catch (Exception $e){
			$this->CancelarTransacao($objeto);
			throw ($e);
		}
		
	}
	
	public function excluirObjeto($objeto){
		
		$classe = new ReflectionClass(get_class($objeto));
		
		$tabela = $this->obterTabela($classe->getDocComment());
		
		if ($tabela==null){
			throw new Exception("Nome da tabela n�o selecionado.");
		}
		
		$propriedades = $classe->getProperties();
		
		$colunasWhere = "";
		
		foreach($propriedades as $propriedade){
			//$propriedade = new ReflectionProperty(get_class($objeto), $nomePropriedade);
			if ($propriedade->isPublic()){
				
				$comentario = $propriedade->getDocComment();
				
				$ehChave = $this->ehChave($comentario);
				
				if (!$ehChave){
					continue;
				}
				
				if ($this->identityInsert($comentario)){
					continue;
				}
				
				$tipo = $this->obterTipo($comentario);
				
				if ($tipo == null || $tipo == DaoEngine::COLECAO){
					continue;
				}
				
				$coluna = $propriedade->getName();
				$valor = $this->obterParametro($propriedade->getValue($objeto), $tipo);
				
				if ($colunasWhere != ""){
					$colunasWhere = "$colunasWhere AND ";
				}
				
				$colunasWhere = "$colunasWhere $coluna = $valor";
					
			}
			
		}
		
		$sql = "DELETE FROM $tabela WHERE $colunasWhere";
		
		$this->executeQuery($sql, false);
		
	}
	
	public function listarObjeto($objeto, $parametros = null, $orderBy = ""){
		
		$classe = new ReflectionClass(get_class($objeto));
		
		$tabela = $this->obterTabela($classe->getDocComment());
		
		if ($tabela==null){
			throw new Exception("Nome da tabela n�o selecionado.");
		}
		
		$propriedades = $classe->getProperties();
		
		$colunasSelect = "";
		
		foreach($propriedades as $propriedade){
			//$propriedade = new ReflectionProperty(get_class($objeto), $nomePropriedade);
			if ($propriedade->isPublic()){
				
				$comentario = $propriedade->getDocComment();
				
				$tipo = $this->ehColuna($comentario);
				
				if ($tipo == null || $tipo == DaoEngine::COLECAO){
					continue;
				}
				
				$coluna = $propriedade->getName();
				
				if ($colunasSelect != ""){
					$colunasSelect = "$colunasSelect, ";
				}
				
				$colunasSelect = "$colunasSelect $coluna";
					
			}
			
		}
		
		$sqlWhere = "";
		
		if ($parametros != null){
		
			foreach($parametros as $parametro){
				//$parametro = new ParametroDB("","");
				$valorParametro = $this->obterParametro($parametro->valorParametro);
				
				if ($sqlWhere == ""){
					$sqlWhere = " WHERE ";
				}else{
					$sqlWhere .= " AND ";
				}
				
				$nomeCampo = $parametro->nomeCampo;

				$sqlWhere = "$sqlWhere $nomeCampo = $valorParametro ";
				


			}
			
		}
		
		if ($orderBy != ""){
			$orderBy = " ORDER BY $orderBy ";
		}
		
		
		$sql = "SELECT $colunasSelect FROM $tabela $sqlWhere $orderBy";
		
		//echo $sql;

		return $this->executeQuery($sql);
		
	}
	
	public function obterObjeto($objeto){
		
		$classe = new ReflectionClass(get_class($objeto));
		
		$tabela = $this->obterTabela($classe->getDocComment());
		
		if ($tabela==null){
			throw new Exception("Nome da tabela n�o selecionado.");
		}
		
		$propriedades = $classe->getProperties();
		
		$colunasSelect = "";
		$colunasWhere = "";
		$colecao = null;
		
		foreach($propriedades as $propriedade){
			//$propriedade = new ReflectionProperty(get_class($objeto), $nomePropriedade);
			if ($propriedade->isPublic()){
				
				$comentario = $propriedade->getDocComment();
				
				$tipo = $this->obterTipo($comentario);
				
				if ($tipo == null){
					continue;
				} elseif ($tipo == DaoEngine::COLECAO){
					//$colecao = $this->push($colecao, obterClasseColecao($comentario));
					continue;
				}
				
				$coluna = $propriedade->getName();

				if ($this->ehChave($comentario)){
					if ($colunasWhere != ""){
						$colunasWhere = "$colunasWhere, ";
					}
					
					$valor = $this->obterParametro($propriedade->getValue($objeto), $tipo);
					
					$colunasWhere = "$colunasWhere $coluna = $valor";
					
					if ($colunasSelect != ""){
						$colunasSelect = "$colunasSelect, ";
					}
					
					$colunasSelect = "$colunasSelect $coluna";
					
				}else{
					if ($colunasSelect != ""){
						$colunasSelect = "$colunasSelect, ";
					}
					
					$colunasSelect = "$colunasSelect $coluna";

				}
					
			}
			
		}
		
		$sql = "SELECT $colunasSelect FROM $tabela WHERE $colunasWhere";
		
		//echo $sql;
		
		$resultados = $this->executeQuery($sql);
		
		$linha = mysql_fetch_array($resultados);
		
		if ($linha){
			
			foreach($propriedades as $propriedade){
				
				$comentario = $propriedade->getDocComment();
				
				$tipo = $this->obterTipo($comentario);
				
				if ($tipo == null){
					continue;
				}elseif ($tipo == DaoEngine::COLECAO){
					$propriedade->setValue($objeto, $this->obterObjetos($this->obterClasseColecao($comentario), $colunasWhere));
					continue;
				}
				
				//$propriedade = new ReflectionProperty(get_class($objeto), $nomePropriedade);
				$propriedade->setValue($objeto, $this->formataPropriedade($linha[$propriedade->getName()], $tipo));
				
			}
			
			return $objeto;
			
		}

	}
	
	public function obterObjetos($classe, $colunasWhere){
		
		
		$classeColecao = new ReflectionClass($classe);
		
		$tabela = $this->obterTabela($classeColecao->getDocComment());
		
		$resultado = $this->executeQuery("SELECT * FROM $tabela WHERE $colunasWhere");
		//echo "SELECT * FROM $tabela WHERE $colunasWhere";
		$colecao = array();
		
		while ($linhaColecao = mysql_fetch_array($resultado)) {
			//$classeColecao = new ReflectionClass($classe);
			$objetoColecao = $classeColecao->newInstance();
			
			$propriedadesColecao = $classeColecao->getProperties();
			$propriedadeChaveColecao = null;
			
			foreach($propriedadesColecao as $propriedadeColecao){
				
				$comentarioColecao = $propriedadeColecao->getDocComment();
				
				$tipoColecao = $this->obterTipo($comentarioColecao);
				
				if($tipoColecao == null){
					continue;
				}elseif ($tipoColecao == DaoEngine::COLECAO) {
					$colunaColecao = $propriedadeChaveColecao->getName();
					$colunasWhereColecao = " $colunaColecao = {$linhaColecao[$colunaColecao]}";
					$propriedadeColecao->setValue($objetoColecao, $this->obterObjetos($this->obterClasseColecao($comentarioColecao), $colunasWhereColecao));
					continue;
				}elseif ($this->ehChave($comentarioColecao)) {
					$propriedadeChaveColecao = $propriedadeColecao;
				}
				
				$propriedadeColecao->setValue($objetoColecao, $this->formataPropriedade($linhaColecao[$propriedadeColecao->getName()], $tipoColecao));
				
			}
			
			$colecao = $this->push($colecao, $objetoColecao);
			
		}
		
		return $colecao;
		
	}
	
	public function logToFile($msg)
	{ 
		$filename = "log.txt";
		$fd = fopen($filename, "a");
		$str = "[" . date("Y/m/d h:i:s.u") . "] " . $msg; 
		fwrite($fd, $str . "\n");
		fclose($fd);
	}
	
	
	public function executeQuery($sql, $controlarConexao = true){
		
		try{
			if ($controlarConexao)
				$this->obterConexao();
			//$this->logToFile("Inicia Query");
			//$this->logToFile($sql);
			$resultados = mysql_query($sql);
			//$this->logToFile("Finaliza Query");
			if ($controlarConexao)
				$this->fecharConexao();
			//echo "<!-- $sql -->\n" ;
			//exit();
			return $resultados;
		}catch (Exception $e){
			if ($controlarConexao)
				$this->fecharConexao();
			throw ($e);
		}

	}
	
	public function executeQueryInsert($sql, $controlarConexao = true){
		
		try{
			if ($controlarConexao)
				$this->obterConexao();
			$resultados = mysql_query($sql);
			$codigoInserido = mysql_insert_id();
			if ($controlarConexao)
				$this->fecharConexao();
			//echo "<!-- $sql -->\n" ;
			//exit();
			return $codigoInserido;
		}catch (Exception $e){
			if ($controlarConexao)
				$this->fecharConexao();
			throw ($e);
		}

	}

	function iniciarTransacao($proprietario) 
	{
		if ($proprietario == $this->donoConexao)
			return;
		
		$this->abrirConexao($proprietario);	
		//mysql_query("BEGIN");
	}
	
	function FinalizarTransacao($proprietario = null)
	{
		if ($proprietario == $this->donoConexao)
			return;
		
		//mysql_query("COMMIT");
		
		$this->fecharConexao($proprietario);
	}

	function CancelarTransacao($proprietario = null)
	{
		if ($proprietario == $this->donoConexao)
			return;
		
		
		//mysql_query("ROLLBACK");
		
		$this->fecharConexao($proprietario);
	}

	
	
	public  function abrirConexao(){

//		$this->cn = mysql_connect("mysql01.colaborae.com.br","colaborae1","jpif3112571");
		
//		$this->cn = mysql_connect("localhost","root","");		

		$this->cn = mysql_connect(DBSERVERNAME,DBUSER,DBPWRD);
		mysql_select_db(DBNAME);
		mysql_set_charset('latin1');
	}
	

	public function fecharConexao($proprietario = null){
		
		if(isset($this->cn) || $this->cn == null){
			if ($proprietario != null && $this->donoConexao != $proprietario)
				return;
			
			mysql_close($this->cn);
			$this->cn = null;
		}
		
	}
	
	private $cn = null;
	public function obterConexao($proprietario = null){
		
		if ($this->cn == null){
			$this->abrirConexao();
			$this->donoConexao = $proprietario;
		}
		
		return $this->cn;
		
	}
	
	
	private function ehIdentity($comentario){
		
		return (bool) (strpos($comentario,"@identity"));
		
	}
	
	public function ehChave($comentario){
		
		return (bool) (strpos($comentario,"@pk") || $this->ehIdentity($comentario));
		
	}
	
	private function identityInsert($comentario){
		
		return (bool) (strpos($comentario,"##identity_insert##"));
		
	}
	
	
	private function ehColuna($comentario){
		
		$valor = $this->obterTipo($comentario);
		
		if ($valor != null){
			return $valor;
		}
		
		$inicio = strpos($comentario, "__");
		$final = strrpos($comentario, "__");
		
		if($inicio === $final)
			return null;
			
		return substr($comentario, $inicio + 2, $final - $inicio - 2);
		
	}
	

	private function obterTipo($comentario){
		
		$inicio = strpos($comentario, "::");
		$final = strrpos($comentario, "::");
		
		if($inicio === $final)
			return null;
			
		return substr($comentario, $inicio + 2, $final - $inicio - 2);
		
	}
	
	private function obterClasseColecao($comentario){
		
		$inicio = strpos($comentario, "##");
		$final = strrpos($comentario, "##");
		
		if($inicio === $final)
			return null;
			
		return substr($comentario, $inicio + 2, $final - $inicio - 2);
		
	}	
	
	public function obterTabela($comentario){
		
		$inicio = strpos($comentario, "::");
		$final = strrpos($comentario, "::");
		
		if($inicio === $final)
			return null;
			
		return substr($comentario, $inicio + 2, $final - $inicio - 2);
		
	}
	
	private function obterParametro($valor, $tipo = null){
		
		if ($valor==null)
			return "null";
		
		$valor = str_replace("'", "\\'", $valor);
			
		switch ($tipo){
			case DaoEngine::DATA:
				$valor = $this->formataParametro($valor, $tipo);
				return "'$valor'";
				
			case DaoEngine::TEXTO:
				return "'$valor'";
				
			case DaoEngine::NUMERO:
				return $valor;
				
			case DaoEngine::DATA_HORA_MINUTO:
				$valor = $this->formataParametro($valor, $tipo);
				return "'$valor'";
				
		}
		
		//throw new Exception("O parametro n�o possui um tipo v�lido.");
		
		return "'$valor'";
	
	}
	
	private function formataParametro($valor, $tipoParametro){
		
		switch ($tipoParametro){
			case DaoEngine::DATA:
				if (strlen($valor)==10){
					return substr($valor, 6, 4) . "-" . substr($valor, 3, 2) . "-" . substr($valor, 0, 2);
				}
				return $valor;
				break;
			
			case DaoEngine::DATA_HORA_MINUTO:
				if (strlen($valor)==16){
					return substr($valor, 6, 4) . "-" . substr($valor, 3, 2) . "-" . substr($valor, 0, 2) .
						" " . substr($valor, 11, 5);
					;
				}
				return $valor;
				break;
			
		}
		
		return $valor;
		
	}
	
	private function formataPropriedade($valor, $tipoPropriedade){
		switch ($tipoPropriedade){
			case DaoEngine::DATA:
				$valor = strftime("%d/%m/%Y", strtotime($valor));
				break;
				
			case DaoEngine::DATA_HORA_MINUTO:
				$valor = strftime("%H:%M", strtotime($valor));
				break;
			
		}
		
		return $valor;
		
	}

	
}


/**
 * ::col_disciplina::
 */
class col_disciplina extends RnBaseStatus {
	
	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_DISCIPLINA;
	
	/**
	 * ::texto::
	 */
	public $DS_DISCIPLINA;
	
	public $CD_DISCIPLINAS_COPIADAS;
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	public function depoisDeInserir()
	{
		
		$this->copiarPerguntas();
		
	}
	
	public function depoisDeAlterar()
	{
		
		$this->copiarPerguntas();
		
	}
	
	public function copiarPerguntas($controlarConexao = false)
	{
		
		$CD_DISCIPLINAS_COPIADAS = $this->CD_DISCIPLINAS_COPIADAS;
		
		if (is_array($CD_DISCIPLINAS_COPIADAS) && count($CD_DISCIPLINAS_COPIADAS) > 0)
		{
			$codigoDisciplinas = join(",", $CD_DISCIPLINAS_COPIADAS);
			
			$sql = "SELECT * FROM col_perguntas WHERE CD_DISCIPLINA IN ($codigoDisciplinas)";


			$resultado = $this->dao->executeQuery($sql,  $controlarConexao);
			
			while ($linhaPergunta = mysql_fetch_array($resultado)) {
				
				$pergunta = new col_perguntas($this->dao);
				
				$pergunta->CD_PERGUNTAS = $linhaPergunta["CD_PERGUNTAS"];
				
				$pergunta = $pergunta->obter();
				
				$pergunta->IN_ATIVO = $linhaPergunta["IN_ATIVO"];
				
				$pergunta->CD_DISCIPLINA = $this->CD_DISCIPLINA;

				$pergunta = $pergunta->inserir();
				
				$sql = "UPDATE col_perguntas SET IN_ATIVO = {$linhaPergunta["IN_ATIVO"]} WHERE CD_PERGUNTAS = {$pergunta->CD_PERGUNTAS}";
				
				$this->dao->executeQuery($sql,  $controlarConexao);
				
			}
			
		}
		
	}

}


/**
 * ::col_prova_disciplina::
 */
class col_prova_disciplina extends RnBase {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	/* @pk
	 * ::numero::
	 */	
	public $CD_PROVA;
	
	/**
	 * ::numero::
	 */	
	public $CD_DISCIPLINA;
	
	/**
	 * ::numero::
	 */		
	public $NR_QTD_PERGUNTAS_NIVEL_1;

	/**
	 * ::numero::
	 */		
	public $NR_QTD_PERGUNTAS_NIVEL_2;

	/**
	 * ::numero::
	 */		
	public $NR_QTD_PERGUNTAS_NIVEL_3;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_4;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_5;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_6;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_7;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_8;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_9;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_10;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_11;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_12;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_13;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_14;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_15;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_16;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_17;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_18;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_19;

    /**
     * ::numero::
     */
    public $NR_QTD_PERGUNTAS_NIVEL_20;
	
}

class col_controle_assessment extends RnBase {

    public function __construct($daoObject = null){
        parent::__construct($daoObject);
    }

    public $assessments;

    public $gruposDisciplinas;

    public $CD_EMPRESA;

    public function alterar() {

        //Exclui os dados
        foreach ($this->gruposDisciplinas as $grupoDisciplinas) {
            $sql = "DELETE FROM col_disciplina_grupo_disciplina WHERE CD_GRUPO_DISCIPLINA = {$grupoDisciplinas->CD_GRUPO_DISCIPLINA}";
            $this->dao->executeQuery($sql);
        }

        $sql = "DELETE FROM col_grupo_disciplina WHERE CD_EMPRESA = {$this->CD_EMPRESA}";
        $this->dao->executeQuery($sql);

        $sql = "DELETE FROM col_assessment WHERE CD_EMPRESA = {$this->CD_EMPRESA}";
        $this->dao->executeQuery($sql);

        $this->inserir();

    }

    public function inserir() {

        foreach($this->assessments as $assessment){
            $assessment->inserir();
        }

        foreach($this->gruposDisciplinas as $grupoDisciplina){
            $grupoDisciplina->inserir();
        }

    }


    public function obter(){
        $this->assessments = $this->dao->obterObjetos("col_assessment","CD_EMPRESA = {$this->CD_EMPRESA}");

        $this->gruposDisciplinas = $this->dao->obterObjetos("col_grupo_disciplina","CD_EMPRESA = {$this->CD_EMPRESA}");

        return $this;

    }


    public function excluir(){
        return $this->dao->excluirObjeto($this);
    }



}


/**
 * ::col_assessment::
 */
class col_assessment extends RnBase {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	
	/**
	/* @pk
	 * ::numero::
	 */	
	public $CD_EMPRESA;
	
	/**
	 * ::numero::
	 */	
	public $CD_PROVA;
	
	/**
	 * ##col_grupo_disciplina##
	 * ::colecao::
	 */
	//public $gruposDisciplinas;

	
	public function listarTodos() {
		$sql = "SELECT DISTINCT
					a.CD_EMPRESA, e.DS_EMPRESA, 1 as IN_ATIVO
				FROM
					col_assessment a
					INNER JOIN col_empresa e ON a.CD_EMPRESA = e.CD_EMPRESA
				WHERE
					e.IN_ATIVO = 1
				ORDER BY
					e.DS_EMPRESA";
		
		return $this->dao->executeQuery($sql, true);
		
	}
	/*
	public function alterar() {
		
		//Exclui os dados 
		foreach ($this->gruposDisciplinas as $grupoDisciplinas) {
			$sql = "DELETE FROM col_disciplina_grupo_disciplina WHERE CD_GRUPO_DISCIPLINA = {$grupoDisciplinas->CD_GRUPO_DISCIPLINA}";
			$this->dao->executeQuery($sql);
		}
		
		$sql = "DELETE FROM col_grupo_disciplina WHERE CD_EMPRESA = {$this->CD_EMPRESA}";
        $this->dao->executeQuery($sql);

        //$sql = "DELETE FROM col_assessment WHERE CD_EMPRESA = {$this->CD_EMPRESA}";
        //$this->dao->executeQuery($sql);
		
		parent::alterar();
		
	}
	*/
	
}

/**
 * ::col_grupo_disciplina::
 */
class col_grupo_disciplina extends RnBase {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	
	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_GRUPO_DISCIPLINA;
	
	
	/**
	 * ::numero::
	 */
	public $CD_EMPRESA;
	
	
	/**
	 * ::texto::
	 */
	public $NM_GRUPO_DISCIPLINA;

    /**
     * ::texto::
     */
    public $SG_GRUPO_DISCIPLINA;

    /**
     * ::numero::
     */
    public $NR_ORDEM_DISCIPLINA;

	/**
	 * ##col_disciplina_grupo_disciplina##
	 * ::colecao::
	 */
	public $disciplinas;
	
}

/**
 * ::col_disciplina_grupo_disciplina::
 */
class col_disciplina_grupo_disciplina extends RnBase {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}

	/**
	/* @pk
	 * ::numero::
	 */	
	public $CD_GRUPO_DISCIPLINA;
	
	/**
	 * ::numero::
	 */
	public $CD_DISCIPLINA;
	
}


/**
 * ::col_prova_cargo::
 */
class col_prova_cargo extends RnBase {

    public function __construct($daoObject = null){
        parent::__construct($daoObject);
    }

    /**
    /* @pk
     * ::numero::
     */
    public $CD_PROVA;

    /**
     * ::texto::
     */
    public $NM_CARGO_FUNCAO;

}

/**
 * ::col_prova::
 */
class col_prova extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_PROVA;
	
	/**
	 * ::texto::
	 */
	public $DS_PROVA;
	
	/**
	 * ::data_hora_minuto::
	 */		
	public $HORARIO_INICIO_DISPONIVEL;
	
	/**
	 * ::data_hora_minuto::
	 */		
	public $HORARIO_FIM_DISPONIVEL;
	
	/**
	 * ::data::
	 */		
	public $DT_REALIZACAO;
	
	/**
	 * ::data::
	 */
	public $DT_FIM_REALIZACAO;
	
	/**
	 * ::numero::
	 */		
	public $DURACAO_PROVA;
	
	/**
	 * ::numero::
	 */		
	public $NR_QTD_PERGUNTAS_PAGINA;
	
	/**
	 * ::numero::
	 */		
	public $NR_QTD_PERGUNTAS_TOTAL;
	
	/**
	 * ::numero::
	 */		
	public $VL_MEDIA_APROVACAO;
	
	/**
	 * ::numero::
	 */		
	public $IN_DIFICULDADE;
	
	/**
	 * ::numero::
	 */		
	public $IN_PROVA;
	
	/**
	 * ::numero::
	 */
	public $NR_TENTATIVA;
	
	/**
	 * ::numero::
	 */
	public $IN_GABARITO;
	
	/**
	 * ::numero::
	 */
	public $IN_OBS;
	
	/**
	 * ::numero::
	 */		
	public $CD_CICLO;
	

	/**
	 * ::numero::
	 */
	public $IN_SENHA_MESTRA;

    /**
     * ::numero::
     */
    public $ID_PAGINA_INDIVIDUAL;


    /**
     * ::numero::
     */
    public $CD_CAPITULO;

    /**
     * ::numero::
     */
    public $ID_PASTA_INDIVIDUAL;
	
	/**
	 * ::texto::
	 */
	public $cargo;
	
	/**
	 * ##col_prova_disciplina##
	 * ::colecao::
	 */
	public $disciplinas;

    /**
     * ##col_prova_cargo##
     * ::colecao::
     */
    public $cargos;

	/**
	 * ##col_prova_chamada##
	 * ::colecao::
	 */
	public $chamadas;
	
	//public function alterar(){
	//	return $this->dao->alterarObjeto($this);
	//	$this->InserirUsuarios();
	//}
	
	public $usuariosCadastrados;
	public $empresasCadastradas;
	public $lotacoesCadastradas;
	
	public function depoisDeInserirOld(){
		
		for($i=0; $i < count($this->usuariosCadastrados); $i++){
			
			$usuarioLinha = $this->usuariosCadastrados[$i];
			
				$sql = "INSERT INTO col_prova_aplicada (CD_PROVA, CD_USUARIO) VALUES ($this->CD_PROVA, $usuarioLinha)";
				
				$this->dao->executeQuery($sql,  false);
		
		}
		
		
		for ($i=0; $i < count($this->empresasCadastradas); $i++){
			$empresaLinha = $this->empresasCadastradas[$i];
			
				$sql = "INSERT INTO col_prova_vinculo (CD_PROVA, CD_VINCULO, IN_TIPO_VINCULO ) VALUES ($this->CD_PROVA, $empresaLinha, 1)";
				
				$this->dao->executeQuery($sql,  false);
			
		}
		
		for ($i=0; $i < count($this->lotacoesCadastradas); $i++){
			$lotacaoLinha = $this->lotacoesCadastradas[$i];
			
				$sql = "INSERT INTO col_prova_vinculo (CD_PROVA, CD_VINCULO, IN_TIPO_VINCULO ) VALUES ($this->CD_PROVA, $lotacaoLinha, 2)";
				
				$this->dao->executeQuery($sql,  false);
		
		}
		
	}
	
	public function depoisDeAlterarOld(){
		
		for($i=0; $i < count($this->usuariosCadastrados); $i++){
			
			$usuarioLinha = $this->usuariosCadastrados[$i];
			
			$sql = "SELECT 1 from col_prova_aplicada WHERE CD_PROVA = $this->CD_PROVA AND CD_USUARIO = $usuarioLinha";
			
			$resultado = $this->dao->executeQuery($sql, false);
			
			//echo mysql_num_rows($resultado);
			
			if (mysql_num_rows($resultado) == 0){
				$sql = "INSERT INTO col_prova_aplicada (CD_PROVA, CD_USUARIO) VALUES ($this->CD_PROVA, $usuarioLinha)";
				
				$this->dao->executeQuery($sql,  false);
			}
		
		}
		
		$usuariosSelecionados = join(",", $this->usuariosCadastrados);
		
		if ($usuariosSelecionados=="") {
			$usuariosSelecionados = -1;
		}
		
		$sql = "DELETE FROM col_prova_aplicada WHERE CD_PROVA = $this->CD_PROVA AND CD_USUARIO NOT IN ($usuariosSelecionados)";
		//echo $sql;
		$this->dao->executeQuery($sql, false);
		
		for ($i=0; $i < count($this->empresasCadastradas); $i++){
			$empresaLinha = $this->empresasCadastradas[$i];
			
			$sql = "SELECT 1 FROM col_prova_vinculo WHERE CD_PROVA = $this->CD_PROVA AND CD_VINCULO = $empresaLinha AND IN_TIPO_VINCULO = 1";
			$resultado = $this->dao->executeQuery($sql, false);
			
			if (mysql_num_rows($resultado) == 0){
				$sql = "INSERT INTO col_prova_vinculo (CD_PROVA, CD_VINCULO, IN_TIPO_VINCULO ) VALUES ($this->CD_PROVA, $empresaLinha, 1)";
				
				$this->dao->executeQuery($sql,  false);
			}
			
		}
		
		$empresasSelecionados = join(",", $this->empresasCadastradas);
		if ($empresasSelecionados==""){
			$empresasSelecionados = -1;
		}
		
		$sql = "DELETE FROM col_prova_vinculo WHERE CD_PROVA = $this->CD_PROVA AND IN_TIPO_VINCULO = 1 AND  CD_VINCULO NOT IN ($empresasSelecionados)";
		//echo $sql;
		$this->dao->executeQuery($sql, false);
		

		for ($i=0; $i < count($this->lotacoesCadastradas); $i++){
			$lotacaoLinha = $this->lotacoesCadastradas[$i];
			
			$sql = "SELECT 1 FROM col_prova_vinculo WHERE CD_PROVA = $this->CD_PROVA AND CD_VINCULO = $lotacaoLinha AND IN_TIPO_VINCULO = 2";
			$resultado = $this->dao->executeQuery($sql, false);
			
			if (mysql_num_rows($resultado) == 0){
				$sql = "INSERT INTO col_prova_vinculo (CD_PROVA, CD_VINCULO, IN_TIPO_VINCULO ) VALUES ($this->CD_PROVA, $lotacaoLinha, 2)";
				
				$this->dao->executeQuery($sql,  false);
			}
			
		}
		
		$lotacoesSelecionados = join(",", $this->lotacoesCadastradas);
		
		if($lotacoesSelecionados==""){
			$lotacoesSelecionados = -1;
		}
		
		$sql = "DELETE FROM col_prova_vinculo WHERE CD_PROVA = $this->CD_PROVA AND IN_TIPO_VINCULO = 2 AND  CD_VINCULO NOT IN ($lotacoesSelecionados)";
		//echo $sql;
		$this->dao->executeQuery($sql, false);

		
	}
	
	public function listarPorPrograma($codigoPrograma = 0, $tipoProva = 2)
	{
		
		$sql = "SELECT
					p.CD_PROVA, p.DS_PROVA, p.IN_ATIVO
				FROM
					col_prova p
					LEFT OUTER JOIN col_prova_vinculo pv ON pv.CD_PROVA = p.CD_PROVA AND pv.IN_TIPO_VINCULO = 1
				WHERE
					(pv.CD_VINCULO = $codigoPrograma OR $codigoPrograma = -1)
				AND	(p.IN_PROVA = $tipoProva OR $tipoProva = 2)
				ORDER BY
					p.DS_PROVA";
		
		return $this->dao->executeQuery($sql, true);
		
		
	}
	
}


/**
 * ::col_prova_chamada::
 */
class col_prova_chamada extends RnBase {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * @pk
	 * ::numero::
	 */
	public $CD_PROVA;
	
	/**
	 * ::data::
	 */	
	public $DT_INICIO_REALIZACAO;
	
	/**
	 * ::data::
	 */	
	public $DT_TERMINO_REALIZACAO;
	
	/**
	 * ::data_hora_minuto::
	 */	
	public $HR_INICIO_REALIZACAO;
	
	/**
	 * ::data_hora_minuto::
	 */	
	public $HR_TERMINO_REALIZACAO;
	
	/**
	 * ::numero::
	 */	
	public $IN_PRIMEIRA_CHAMADA;
	
}


/**
 * ::col_prova_aplicada::
 */
class col_prova_usuario extends RnBase {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * ::numero::
	 */	
	public $CD_USUARIO;
	
	
	/**
	 * @pk
	 * ::numero::
	 */	
	public $CD_PROVA;
	
}


/**
 * ::col_empresa::
 */
class col_empresa extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * @identity
	 * ::numero::
	 */		
	public $CD_EMPRESA;
	
	/**
	 * ::texto::
	 */	
	public $DS_EMPRESA;
	
	/**
	 * ::data::
	 */
	public $DT_INICIO_VIGENCIA;
	
	/**
	 * ::data::
	 */
	public $DT_TERMINO_VIGENCIA;
	
	/**
	 * ::texto::
	 */
	public $TX_EMAIL_PROGRAMA;
	
	/**
	 * ::texto::
	 */
	public $DS_IP;
	
	/**
	 * ::data::
	 */
	public $DT_INICIO_CADASTRO;
	
	/**
	 * ::data::
	 */
	public $DT_TERMINO_CADASTRO;
	
	/**
	 * ::texto::
	 */
	public $TX_BLOG_PROGRAMA;

    /**
     * ::texto::
     */
    public $TX_DOMINIO;
	
	/**
	 * ##col_ciclo##
	 * ::colecao::
	 */
	public $ciclos;

    /**
     *  ::numero::
     */
    public $IN_ASSESSMENT;

    /**
     *  ::numero::
     */
    public $IN_OBRIGA_DIAGNOSTICO;


	/**
	 * ::numero::
	 */	
	public $CD_CLIENTE;
	
	public function depoisDeInserir()
	{
		$this->uploadArquivo();
	}
	
	public function depoisDeAlterar()
	{
		$this->uploadArquivo();
	}
	
	private function uploadArquivo()
	{
		$idEmpresa = $this->CD_EMPRESA;
		
		$UPLOAD_BASE_IMG_DIR	= $this->getDocumentRoot()."/canal/images/empresas/";
	
		if(is_uploaded_file($_FILES['flLogo']['tmp_name']) && $this->CD_EMPRESA != ""){
			
			$savefile = "{$UPLOAD_BASE_IMG_DIR}logo$idEmpresa.png";
			if(move_uploaded_file($_FILES['flLogo']['tmp_name'], $savefile)){
				chmod($savefile, 0666);
				$fotofile = $_FILES['flLogo']['name'];
			}
		}
		else{
			$uploadfotoerror = (Integer)$_FILES['flLogo']['error'] + 1;
		}
		
		if(is_uploaded_file($_FILES['flBoasVindas']['tmp_name']) && $this->CD_EMPRESA != ""){
			
			$savefile = "{$UPLOAD_BASE_IMG_DIR}boasvindas$idEmpresa.png";
			if(move_uploaded_file($_FILES['flBoasVindas']['tmp_name'], $savefile)){
				chmod($savefile, 0666);
				$fotofile = $_FILES['flBoasVindas']['name'];
			}
		}
		else{
			$uploadfotoerror = (Integer)$_FILES['flBoasVindas']['error'] + 1;
		}
		
		
		
		
	}
	
	function getDocumentRoot()
	{
		$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
		$thispath 	 = '/admin/cadastro_empresa.php';
		$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
	//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
		$doc_root	 = substr($fullpath,0,$doc_rootpos);
		
		return $doc_root;
	}
	
}

/**
 * ::col_lotacao::
 */
class col_lotacao extends RnBaseStatus {
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_LOTACAO;
	
	/**
	 * ::numero::
	 */	
	public $CD_EMPRESA;
	
	/**
	 * ::texto::
	 */
	public $DS_LOTACAO;
	
	/**
	 * ::numero::
	 */	
	public $CD_LOTACAO_PAI;
	
	/**
	 * ::texto::
	 */
	public $CD_IDENTIFICADOR_LOTACAO;
	
	
	public $CD_NIVEL_HIERARQUICO;
	

	public $DS_LOTACAO_HIERARQUIA;

    public function inserir(){

        $objeto = $this->dao->inserirObjeto($this);
        $this->acertaHierarquia($objeto);
        return $objeto;
    }

    public function alterar(){

        $objeto = $this->dao->alterarObjeto($this);
        $this->acertaHierarquia($this);
        return $objeto;
    }



    public function acertaHierarquia($objeto){

        $descricaoHierarquiaPai = $objeto->CD_LOTACAO;
        if($objeto->CD_LOTACAO_PAI > 0){
            $sql = "SELECT DS_LOTACAO_HIERARQUIA FROM col_lotacao WHERE CD_LOTACAO = {$objeto->CD_LOTACAO_PAI}";
            $resultado = $this->dao->executeQuery($sql);
            $linha = mysql_fetch_row($resultado);

            $descricaoHierarquiaPai = "{$linha[0]}.{$objeto->CD_LOTACAO}";
        }

        $sql = "UPDATE col_lotacao SET DS_LOTACAO_HIERARQUIA = '$descricaoHierarquiaPai' WHERE CD_LOTACAO = {$objeto->CD_LOTACAO}";
        $this->dao->executeQuery($sql);
        //echo $sql;
        //exit();

    }

}


/**
 * ::col_perguntas::
 */
class col_perguntas extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}	

	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_PERGUNTAS;
	
	
	/**
	 * ::numero::
	 */	
	public $CD_DISCIPLINA;
	
	/**
	 * ::texto::
	 */
	public $DS_PERGUNTAS;
	
	/**
	 * ::numero::
	 */	
	public $IN_PESO;
	
	/**
	 * ::texto::
	 */
	public $TX_OBSERVACAO;
	
	/**
	 * ::texto::
	 */	
	public $DS_LINK_SUMARIO;
	
	/**
	 * ##col_respostas##
	 * ::colecao::
	 */
	public $respostas;
	

	
	
	
}

/**
 * ::col_respostas::
 */
class col_respostas extends RnBase {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * ::numero::
	 */	
	public $CD_RESPOSTAS;
	
	/**
	 * @pk
	 * ::numero::
	 */	
	public $CD_PERGUNTAS;
	
	/**
	 * ::texto::
	 */	
	public $DS_RESPOSTAS;
	
	/**
	 * ::numero::
	 */	
	public $IN_CERTA = "0";
	
}

/**
 * ::col_usuario::
  */
class col_usuario extends RnBase {
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * @pk
	 * ::numero::
	 */		
	public $CD_USUARIO;
	
	/**
	 * ::texto::
	 */		
	public $NM_USUARIO;
	
	
}




class col_usuario_importacao extends RnBase {
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	public $usuarios;

	private $lotacoes;
	
	private $filtros;
	
	private $perfis;
	
	public $empresa;
	
	public $senhaUsuarios;
	
	public $resumoImportacao;
	
	public $acao;

    public $nomeArquivo;

    public $diretorioArquivo;
	
	private $localidades;

    public function inserir()
    {

        require_once '../Classes/PHPExcel.php';
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '32MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        if (!(isset($definesIncluido) && $definesIncluido)){
            include_once("../include/defines.php");
        }

        $inputFileType = PHPExcel_IOFactory::identify("{$this->diretorioArquivo}{$this->nomeArquivo}");
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objReader->setReadDataOnly(true);

        $objPHPExcel = $objReader->load("{$this->diretorioArquivo}{$this->nomeArquivo}");

        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);

        $highestRow = $objWorksheet->getHighestRow();

        $codigoEmpresa = $this->empresa;

        //Abre a conex�o
        DaoEngine::getInstance()->obterConexao();

        for ($row = 2; $row <= $highestRow;++$row)
        {

            $lotacao = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue()));
            $nome = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue()));
            $cargo = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(2, $row)->getCalculatedValue()));
            $email = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue()));
            $login = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(4, $row)->getCalculatedValue()));
            $perfil = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(5, $row)->getCalculatedValue()));
            $localidade = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(6, $row)->getCalculatedValue()));

            if($login == "" || $nome == "")
                continue;

            if($this->acao == "E"){
                $query = "UPDATE col_usuario SET Status = 0 WHERE login = '$login' AND empresa = $codigoEmpresa";
                DaoEngine::getInstance()->executeQuery($query, false);
                continue;
            }

            $senha = Cripto($this->senhaUsuarios,'C');

            $query = "INSERT INTO col_localidade (NM_LOCALIDADE, CD_EMPRESA, IN_ATIVO)
                     SELECT '$localidade', $codigoEmpresa, 1 FROM DUAL
                     WHERE NOT EXISTS(SELECT 1 FROM col_localidade WHERE NM_LOCALIDADE = '$localidade' AND CD_EMPRESA = $codigoEmpresa)" ;

            //echo $query;

            DaoEngine::getInstance()->executeQuery($query, false);

            $query = "INSERT INTO col_usuario(
                        empresa,
                        DT_IMPORTACAO,
                        datacadastro,
                        tipo,
                        lotacao,
                        NM_USUARIO,
                        cargofuncao,
                        email,
                        login,
                        senha,
                        IN_SENHA_ALTERADA,
                        CD_LOCALIDADE,
                        STATUS
                    )
                    SELECT
                        $codigoEmpresa,
                        SYSDATE(),
                        SYSDATE(),
                        COALESCE((SELECT ID FROM tb_tipo_usuario WHERE NM_TIPO_USU = '$perfil'), 1),
                        (SELECT CD_LOTACAO FROM col_lotacao WHERE CD_IDENTIFICADOR_LOTACAO = '$lotacao' AND CD_EMPRESA = $codigoEmpresa),
                        '$nome',
                        '$cargo',
                        '$email',
                        '$login',
                        '$senha',
                        0,
                        (SELECT CD_LOCALIDADE FROM col_localidade WHERE CD_EMPRESA = $codigoEmpresa AND NM_LOCALIDADE = '$localidade' AND IN_ATIVO = 1),
                        1
                    FROM
                        DUAL
                    WHERE
                        NOT EXISTS(SELECT 1 FROM col_usuario WHERE login = '$login' AND empresa = $codigoEmpresa AND Status = 1)";

            DaoEngine::getInstance()->executeQuery($query, false);

            $linhaAfetada = mysql_affected_rows();

            if($linhaAfetada == 0){

                $query = "UPDATE
                            col_usuario
                          SET
                            tipo = COALESCE((SELECT ID FROM tb_tipo_usuario WHERE NM_TIPO_USU = '$perfil'), 1),
                            cargofuncao = '$cargo',
                            email = '$email',
                            lotacao = (SELECT CD_LOTACAO FROM col_lotacao WHERE CD_IDENTIFICADOR_LOTACAO = '$lotacao' AND CD_EMPRESA = $codigoEmpresa),
                            CD_LOCALIDADE = (SELECT CD_LOCALIDADE FROM col_localidade WHERE CD_EMPRESA = $codigoEmpresa AND NM_LOCALIDADE = '$localidade' AND IN_ATIVO = 1)
                          WHERE
                            login = '$login'
                          AND empresa = $codigoEmpresa
                          AND Status = 1";



                DaoEngine::getInstance()->executeQuery($query, false);

            }

        }

        DaoEngine::getInstance()->fecharConexao();


    }
	
	
	public function excluir(){
		
		$dom = $this->usuarios;
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		
		$empresa = $this->empresa;
		$linhaAtual = 0;
		$logins = array();
		foreach ($rows as $row)
		{
			if ( $linhaAtual > 0 )
			{
				$cells = $row->getElementsByTagName( 'Cell' );
				
				foreach( $cells as $cell )
				{ 
					$ind = $cell->getAttribute( 'Index' );
					if ( $ind != null ) $index = $ind;
					
					if ( $ind == 0 ) $login = $this->trataSQL($cell->nodeValue);
					$logins[] = "'$login'";
				}
			}
			
			$linhaAtual++;
		}
		
		$login = join(",",$logins);
		
		
		$sql = "UPDATE col_usuario SET Status = 0 WHERE login in ($login) AND empresa = $empresa";		
		//echo $sql;
		//exit();
		$this->dao->executeQuery($sql,true);

		$resumoImportacao = new ResumoImportacao();
		$resumoImportacao->quantidadeLinhasProcessadas = $linhaAtual - 1;
		$resumoImportacao->quantidadeLotacoesProcessadas = 0;
		$resumoImportacao->quantidadeFiltrosProcessados = 0;
		$resumoImportacao->erros = 0;
		$this->resumoImportacao = $resumoImportacao;
		
		return $this;
		
	}
	
	public function inserirOld(){
		
		//if ($acao = "E")
		//{
		//	return  $this->excluir();
		//}
		
		$resumoImportacao = new ResumoImportacao();
		$erros = array();
		
		$dom = $this->usuarios;
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		
		$this->lotacoes = array();
		
		$this->localidades = array();
		
		$this->perfis = array();
		
		$this->filtros = array();
		
		page::$rn->usuarios = array();
		
		$timestamp = time();

		$empresa = $this->empresa;
		
		$senhaUsuarios = $this->senhaUsuarios;
		
		$sql = "SELECT CD_USUARIO, login FROM col_usuario WHERE empresa = $empresa";
		$listaUsuarios = $this->dao->executeQuery($sql,true);
		$usuariosCadastrados = array();
		
		while ($linhaUsuario = mysql_fetch_array($listaUsuarios)) {
			
			$usuariosCadastrados["{$linhaUsuario["login"]}"] = $linhaUsuario["CD_USUARIO"];
			
		}
		
		$linhaAtual = 0;
		
		foreach ($rows as $row)
		{
			if ( $linhaAtual > 0 )
			{
				
				$existeErroFiltro = false;
				
				$index = 1;
				$cells = $row->getElementsByTagName( 'Cell' );
				
				$login = "";
				
				foreach( $cells as $cell )
				{ 
					$ind = $cell->getAttribute( 'Index' );
					if ( $ind != null ) $index = $ind;
					
					if ( $index == 1 ) $lotacao = $this->obterLotacao($this->lotacoes, $this->trataSQL($cell->nodeValue));
					if ( $index == 2 ) $NM_USUARIO = $this->trataSQL($cell->nodeValue);
					if ( $index == 3 ) $cargofuncao = $this->trataSQL($cell->nodeValue);
					if ( $index == 4 ) $email = $cell->nodeValue;
					if ( $index == 5 ) $login = $this->trataSQL($cell->nodeValue);
					if ( $index == 6 ) $tipoUsuario = $this->obterPerfil($this->trataSQL($cell->nodeValue));
					if ( $index == 7 ) $localidade = $this->obterLocalidade($this->localidades, $this->trataSQL($cell->nodeValue));
					
					//if ( $index == 7 ) $filtro = $this->montarFiltro($lotacao, $this->trataSQL($cell->nodeValue), $existeErroFiltro);
					
					$index += 1;
				}
				
				if ($login == "") {
					continue;
				}
				
				$login = trim($login);
				$email = trim($email);
				$NM_USUARIO = trim($NM_USUARIO);
				$cargofuncao = trim($cargofuncao);
				
				$senha = Cripto($senhaUsuarios,'C');
				
				$inserirProvaAplicada = false;
				
				$sql = "";
				
				if ($existeErroFiltro)
				{
					$linhadoErro = $linhaAtual + 1;
					$resumoImportacao->quantidadeLinhasNaoProcessadas++;
					$erros[] = "$linhadoErro - Lota��o j� associada a outro Grupo Gerencial";
					$linhaAtual++;
					continue;
				}
				
				$resumoImportacao->quantidadeLinhasProcessadas++;
				
				switch ($tipoUsuario)
				{
					case -1:
						$resumoImportacao->quantidadeSuperUsuariosPrograma++;
						break;
					case -2:
						$resumoImportacao->quantidadeSuperUsuariosGrupo++;
						break;
					case -3:
					
						break;
					default:
						$resumoImportacao->quantidadeParticipantes++;
						break;
				}
				
				if ($usuariosCadastrados[$login] == null)
				{
						$sql = "INSERT INTO col_usuario (empresa, DT_IMPORTACAO, datacadastro, tipo, lotacao, NM_USUARIO, cargofuncao, email, login, senha, IN_SENHA_ALTERADA, CD_LOCALIDADE, Status)
						VALUES ($empresa, '$timestamp', sysdate(), $tipoUsuario, $lotacao, '$NM_USUARIO', '$cargofuncao', LOWER('$email'), '$login', '$senha', 0, $localidade, 1)";
						
						$inserirProvaAplicada = true;
						
						//echo "$sql
						//";
				}
				else
				{
					$sql = "UPDATE col_usuario SET DT_IMPORTACAO = '$timestamp', lotacao = $lotacao, NM_USUARIO = '$NM_USUARIO', cargofuncao = '$cargofuncao', CD_LOCALIDADE = $localidade, 
							email = LOWER('$email'), tipo = $tipoUsuario, Status=1 WHERE CD_USUARIO = {$usuariosCadastrados[$login]}";
					
					//echo "$sql
					//	";
				}
				
				if ($sql != ""){
					$this->dao->executeQuery($sql,true);
				
					if ($inserirProvaAplicada){
	
						$codigoUsuario = 0;
						$RS_query = $this->dao->executeQuery('SELECT MAX(CD_USUARIO) FROM col_usuario', true);
						if($oRs = mysql_fetch_row($RS_query)){
							$codigoUsuario = $oRs[0];
						}
						
						$resultado = $this->dao->executeQuery("SELECT CD_PROVA FROM col_prova_vinculo WHERE (CD_VINCULO = '$empresa' AND IN_TIPO_VINCULO = 1)
									OR (CD_VINCULO = '$lotacao' AND IN_TIPO_VINCULO = 2)", true);
						
						
						while($linhaProva = mysql_fetch_array($resultado)) {
		
							$sql = "INSERT INTO col_prova_aplicada (CD_PROVA, CD_USUARIO) VALUES ({$linhaProva["CD_PROVA"]}, $codigoUsuario)";
							$this->dao->executeQuery($sql,true);
						
						}
						
						
					}
				}				
				
			}
			$linhaAtual++;
		}
		
		$resumoImportacao->quantidadeLotacoesProcessadas = count($this->lotacoes);
		$resumoImportacao->quantidadeFiltrosProcessados = count($this->filtros);
		$resumoImportacao->erros = $erros;
		$this->resumoImportacao = $resumoImportacao;
		
		return $this;
		
	}
	
	private function trataSQL($valor)
	{
		$valor = utf8_decode($valor);
		$valor = str_replace("'", "\\'", $valor);
		$valor = trim($valor);
		
		return $valor;
		
	}
	
	private function obterLocalidade($localidades, $nomeLocalidade){
		
		$empresa = $this->empresa;
		$nomeLocalidade = trim($nomeLocalidade);
		
		//if ($this->localidades[$nomeLocalidade] == null){
		if (!array_key_exists($nomeLocalidade,$this->localidades)){
			$sql = "SELECT CD_LOCALIDADE FROM col_localidade WHERE NM_LOCALIDADE = '$nomeLocalidade' AND CD_EMPRESA = $empresa";
			$resultado = $this->dao->executeQuery($sql,true);
			
			if (mysql_num_rows($resultado) == 0){
				//Insere a lota��o
				$sql = "INSERT INTO col_localidade (NM_LOCALIDADE, CD_EMPRESA) VALUES ('$nomeLocalidade', $empresa)";
				$this->dao->obterConexao();
				$this->dao->executeQuery($sql,false);
				$idLocalidade = mysql_insert_id();
				$this->dao->fecharConexao();
				$this->lotacoes[$nomeLocalidade] = $idLocalidade;
				return $idLocalidade;
				
			}else {
				$linha = mysql_fetch_array($resultado);
				$this->lotacoes[$nomeLocalidade] = $linha["CD_LOCALIDADE"];
				return $linha["CD_LOCALIDADE"];
			}

		}else{
			return $this->localidades[$nomeLocalidade];
		}
		
		
	}
	
	private function obterLotacao($lotacoes, $nomeLotacao){
		
		$empresa = $this->empresa;
		$nomeLotacao = trim($nomeLotacao);
		
		if (!array_key_exists($nomeLotacao,$this->lotacoes)){
			$sql = "SELECT CD_LOTACAO FROM col_lotacao WHERE CD_IDENTIFICADOR_LOTACAO = '$nomeLotacao' AND CD_EMPRESA = $empresa";
			$resultado = $this->dao->executeQuery($sql,true);
			
			if (mysql_num_rows($resultado) == 0){
				//Insere a lota��o
				$sql = "INSERT INTO col_lotacao (CD_IDENTIFICADOR_LOTACAO, DS_LOTACAO, CD_EMPRESA) VALUES ('$nomeLotacao', $nomeLotacao', $empresa)";
				$this->dao->obterConexao();
				$this->dao->executeQuery($sql,false);
				$idLotacao = mysql_insert_id();
				$this->dao->fecharConexao();
				$this->lotacoes[$nomeLotacao] = $idLotacao;
				return $idLotacao;
				
			}else {
				$linha = mysql_fetch_array($resultado);
				$this->lotacoes[$nomeLotacao] = $linha["CD_LOTACAO"];
				return $linha["CD_LOTACAO"];
			}

		}else{
			return $this->lotacoes[$nomeLotacao];
		}
		
	}
	
	private function montarFiltro($lotacao, $nomeFiltro, &$existeErro)
	{
		$nomeFiltro = trim($nomeFiltro);
		$codigoFiltro = 0;
		$empresa = $this->empresa;
		
		
		if ($this->filtros[$nomeFiltro] == null)
		{
			$sql = "SELECT CD_FILTRO FROM col_filtro WHERE CD_EMPRESA = $empresa AND NM_FILTRO = '$nomeFiltro'";
			$resultado = $this->dao->executeQuery($sql,true);
			
			if (mysql_num_rows($resultado) == 1){
				$linha = mysql_fetch_array($resultado);
				$this->filtros[$nomeFiltro] = $linha[CD_FILTRO];
				$codigoFiltro = $linha[CD_FILTRO];
			}
			else 
			{
				$sql = "INSERT INTO col_filtro (NM_FILTRO,CD_EMPRESA,IN_TIPO_FILTRO,IN_ATIVO,IN_FILTRO_RANKING)
										VALUES ('$nomeFiltro', $empresa, 1, 1, 1) ";
				
				$this->dao->obterConexao();
				$this->dao->executeQuery($sql,false);
				$codigoFiltro = mysql_insert_id();
				$this->dao->fecharConexao();
				
				$this->filtros[$nomeFiltro] = $codigoFiltro;
				
			}
			
		}
		else 
		{
			$codigoFiltro = $this->filtros[$nomeFiltro];
		}
		
		$sql = "SELECT COUNT(1) FROM col_filtro_lotacao WHERE CD_FILTRO <> $codigoFiltro AND CD_LOTACAO = $lotacao";
		$this->dao->obterConexao();
		$resultado = $this->dao->executeQuery($sql,false);
		$linha = mysql_fetch_row($resultado);
		$lotacaoOutroFiltro = $linha[0];		
		
		if ($lotacaoOutroFiltro == 0)
		{
			$sql = "INSERT INTO col_filtro_lotacao (CD_FILTRO, CD_LOTACAO) VALUES ($codigoFiltro, $lotacao)";
			
			//$this->dao->obterConexao();
			$this->dao->executeQuery($sql,false);
		}
		else
		{
			$existeErro = true;
		}
		
		$this->dao->fecharConexao();
		
		return $codigoFiltro;
		
	}
	
	private function obterPerfil($nomePerfil)
	{
		
		if (count($this->perfis) == 0)
		{
			$sql = "SELECT * FROM tb_tipo_usuario";
			$resultado = $this->dao->executeQuery($sql,true);
			
			while ($linha = mysql_fetch_array($resultado))
			{
				
				$nome = strtolower($linha["NM_TIPO_USU"]);
				$id = $linha["ID"];
				
				$this->perfis[$nome] = $id;
					
			}
			
		}
		
		$codigoPerfil = 1;
		
		$nomePerfil = strtolower($nomePerfil);
		
		//if ($this->perfis[$nomePerfil] != null)
		if (array_key_exists($nomePerfil,$this->perfis))
		{
			$codigoPerfil = $this->perfis[$nomePerfil];
		}
		
		return $codigoPerfil;
		
	}
	
}

/**
 * ::col_pagina_acesso::
  */
class col_pagina_acesso extends RnBase {
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	public function listar($parametros = null, $ordernacao = ""){
		
		$sql = "SELECT * FROM col_pagina_acesso ORDER BY DS_PAGINA_ACESSO";
		return $this->dao->executeQuery($sql,true);
		
	}
	
	public function prepararRelatorios($dataInicial, $dataFinal, $empresa){
		
		//VERIFICAR OS DOWNLOADS VIGENTES
		return;
		$sql = "SELECT TX_COMENTARIO FROM col_comentario WHERE TX_COMENTARIO LIKE '%download.php?%' AND CD_EMPRESA = $empresa";
		$resultadoComentario = $this->dao->executeQuery($sql,true);
		
		$sql = "SELECT conteudo FROM col_empresa WHERE conteudo LIKE '%download.php?%' AND CD_EMPRESA = $empresa";
		$resultadoEmpresa = $this->dao->executeQuery($sql,true);
		
		$sql = "SELECT TEXTAREA AS TX_COMENTARIO FROM col_foruns WHERE TEXTAREA LIKE '%download.php?%' AND CD_EMPRESA = $empresa";
		$resultadoForum = $this->dao->executeQuery($sql,true);
		
		$arquivos = array();
		
		while ($linha = mysql_fetch_array($resultadoEmpresa)) {
			$texto = $linha["conteudo"];
			
			while (strstr($texto, "download.php?") != "") {
				
				$texto = strstr($texto, "download.php?");
			
				$indice = strpos($texto,"\"");
		
				$arquivos[] = substr($texto,17,$indice - 17);
		
				$texto = substr($texto, $indice);
				
			}
			
		}
		
		while ($linha = mysql_fetch_array($resultadoComentario)) {
			$texto = $linha["conteudo"];
			
			while (strstr($texto, "download.php?") != "") {
				
				$texto = strstr($texto, "download.php?");
			
				$indice = strpos($texto,"\"");
		
				$arquivos[] = substr($texto,17,$indice - 17);
		
				$texto = substr($texto, $indice);
				
			}
			
		}
		
		while ($linha = mysql_fetch_array($resultadoForum)) {
			$texto = $linha["conteudo"];
			
			while (strstr($texto, "download.php?") != "") {
				
				$texto = strstr($texto, "download.php?");
			
				$indice = strpos($texto,"\"");
		
				$arquivos[] = substr($texto,17,$indice - 17);
		
				$texto = substr($texto, $indice);
				
			}
			
		}
		
		$downloads = "'" . join("','", $arquivos) . "'";
		
		//Configura valores padr�o
		$this->updateInOrdenar(999, "IN_ORDENAR <> -1");
		$this->updateInOrdenar(1, "CD_PAGINA_ACESSO = 1");
		$this->updateInOrdenar(2, "CD_PAGINA_ACESSO = 2");
		$this->updateInOrdenar(3, "DS_PAGINA_ACESSO LIKE 'Download %'");
		$this->updateInOrdenar(4, "CD_PAGINA_ACESSO = 5");
		
		if ($dataInicial == "")
		{
			$dataInicial = "01/01/2000";
		}
		if ($dataFinal == "")
		{
			$dataFinal = "31/12/2030";
		}
		
		$dataInicio = substr($dataInicial, 6, 4) . substr($dataInicial, 3, 2) . substr($dataInicial, 0, 2);
		$dataTermino = substr($dataFinal, 6, 4) . substr($dataFinal, 3, 2) . substr($dataFinal, 0, 2);
		
		//Verificar Provas vigentes
		$sqlData = "(DATE_FORMAT(DT_REALIZACAO, '%Y%m%d') <= '$dataInicio' AND DATE_FORMAT(DT_FIM_REALIZACAO, '%Y%m%d') >= '$dataInicio')
					OR (DATE_FORMAT(DT_REALIZACAO, '%Y%m%d') <= '$dataTermino' AND DATE_FORMAT(DT_FIM_REALIZACAO, '%Y%m%d') >= '$dataTermino')
					OR (DATE_FORMAT(DT_REALIZACAO, '%Y%m%d') >= '$dataInicio'   AND DATE_FORMAT(DT_REALIZACAO, '%Y%m%d') <= '$dataTermino')
					OR (DATE_FORMAT(DT_FIM_REALIZACAO, '%Y%m%d') >= '$dataInicio'   AND DATE_FORMAT(DT_FIM_REALIZACAO, '%Y%m%d') <= '$dataTermino'))
					AND (EXISTS(SELECT 1 FROM col_prova_aplicada pa INNER JOIN col_usuario u ON pa.CD_USUARIO = u.CD_USUARIO WHERE p.CD_PROVA = pa.CD_PROVA AND u.empresa = $empresa)";
		
		
		$sqlSimulado = "SELECT p.CD_PROVA, p.DS_PROVA FROM col_prova p WHERE p.IN_PROVA = 0"; // AND ($sqlData)";
		
		$resultadoSimulado = $this->dao->executeQuery($sqlSimulado,true);
		
		while ($linha = mysql_fetch_array($resultadoSimulado))
		{
			$codigo = $linha["CD_PROVA"];
			$nome = $linha["DS_PROVA"];
			$this->updateInOrdenarProva(6, "NM_ARQUIVO LIKE '%?$codigo'", $nome);
		}

		$sqlProva = "SELECT p.CD_PROVA, p.DS_PROVA FROM col_prova p WHERE p.IN_PROVA = 1"; // AND ($sqlData)";
		
		$resultadoProva = $this->dao->executeQuery($sqlProva,true);
		
		while ($linha = mysql_fetch_array($resultadoProva))
		{
			$codigo = $linha["CD_PROVA"];
			$nome = $linha["DS_PROVA"];
			$this->updateInOrdenarProva(7, "NM_ARQUIVO LIKE '%?$codigo'", $nome);
		}

		
		//Verificar Downloads Realizados
		
		//$sql = "UPDATE col_pagina_acesso SET IN_ORDENAR = -1";
		//$this->dao->executeQuery($sql,true);
		
		//$arrPaginas = split(";", $paginasSelecionadas);
		
		//$i = 0;
		//foreach ($arrPaginas as $pagina){
		//	$sql = "UPDATE col_pagina_acesso SET IN_ORDENAR = $i WHERE CD_PAGINA_ACESSO = $pagina";
		//	$this->dao->executeQuery($sql,true);
		//	$i++;
		//}
		
	}
	
	private function updateInOrdenar($inOrdenar, $Where)
	{
		
		$sqlUpdate = "update col_pagina_acesso set IN_ORDENAR = $inOrdenar WHERE $Where";
		
		//echo $sqlUpdate;
		
		$this->dao->executeQuery($sqlUpdate,true);
		
	}
	
	private function updateInOrdenarProva($inOrdenar, $Where, $nomeProva)
	{
		$nomeProva = str_ireplace("'","\'",$nomeProva);
		$sqlUpdate = "update col_pagina_acesso set IN_ORDENAR = $inOrdenar, DS_PAGINA_ACESSO = '$nomeProva' WHERE $Where";
		
		//echo $sqlUpdate;
		
		$this->dao->executeQuery($sqlUpdate,true);
		
	}
	
}

/**
 * ::col_pagina_comentario ::
  */
class col_pagina_comentario extends RnBase {
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	public function listarTodos($parametros = null, $ordernacao = ""){
		
		$sql = "SELECT * FROM col_pagina_comentario ORDER BY DS_PAGINA";
		return $this->dao->executeQuery($sql,true);
		
	}
	
}


/**
 * ::col_comentario::
 */
class col_comentario extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_COMENTARIO;
	
	/**
	 * ::texto::
	 */		
	public $NM_PAGINA;
	
	/**
	 * ::texto::
	 */	
	public $TX_COMENTARIO;
	
	/**
	 * ::numero::
	 */
	public $CD_EMPRESA;


    /**
     * ::numero::
     */
	public $CD_CICLO;

	public function listarTodos($codigoPrograma = null, $ordernacao = ""){
		
		$sqlWhere = "";
		if ($codigoPrograma != null && $codigoPrograma != -1){
			$sqlWhere = "WHERE e.CD_EMPRESA = $codigoPrograma"; 
		}
		
		$sql = "SELECT
					e.DS_EMPRESA, p.DS_PAGINA, c.CD_COMENTARIO, c.IN_ATIVO
				FROM
					col_comentario c
					INNER JOIN col_empresa e ON c.CD_EMPRESA = e.CD_EMPRESA
					INNER JOIN col_pagina_comentario p ON p.NM_PAGINA = c.NM_PAGINA
				$sqlWhere
				 ORDER BY $ordernacao";
		
		return $this->dao->executeQuery($sql,true);
		
	}
	
}

/**
 * ::col_farol::
 */
class col_farol extends RnBaseStatus
{
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_FAROL;
	
	/**
	 * ::numero::
	 */	
	public $CD_FILTRO;
	
	/**
	 * ::data::
	 */	
	public $DT_INICIO;
	
	/**
	 * ::data::
	 */	
	public $DT_TERMINO;
	
	/**
	 * ::texto::
	*/	
	public $NM_FAROL;
	
	/**
	 * ::numero::
	 */	
	public $NR_PESO_PARTICIPACAO;
	
	/**
	 * ::numero::
	 */		
	public $NR_PESO_ACESSO;
	
	/**
	 * ::numero::
	 */		
	public $NR_PESO_DOWNLOAD;
	
	/**
	 * ::numero::
	 */		
	public $NR_PESO_PARTICIPACAO_SIMULADO;
	
	/**
	 * ::numero::
	 */		
	public $NR_PESO_QUANTIDADE_SIMULADO;
	
	/**
	 * ::numero::
	 */		
	public $NR_PESO_PARTICIPACAO_FORUM;
	
	/**
	 * ::numero::
	 */		
	public $NR_PESO_CONTRIBUICAO_FORUM;
	

	/**
	 * ::numero::
	 */		
	public $NR_PESO_PARTICIPACAO_AVALIACAO;
	
	/**
	 * ::numero::
	 */		
	public $NR_PESO_NOTA_AVALIACAO;
	
	/**
	 * ::numero::
	 */		
	public $NR_PESO_NOTA_SIMULADO;

	/**
	 * ::numero::
	 */	
	public $IN_PESO_PARTICIPACAO = 1;
	
	/**
	 * ::numero::
	 */		
	public $IN_PESO_ACESSO = 1;
	
	/**
	 * ::numero::
	 */		
	public $IN_PESO_DOWNLOAD = 1;
	
	/**
	 * ::numero::
	 */		
	public $IN_PESO_PARTICIPACAO_SIMULADO = 1;
	
	/**
	 * ::numero::
	 */		
	public $IN_PESO_QUANTIDADE_SIMULADO = 1;
	
	/**
	 * ::numero::
	 */		
	public $IN_PESO_PARTICIPACAO_FORUM = 1;
	
	/**
	 * ::numero::
	 */		
	public $IN_PESO_CONTRIBUICAO_FORUM = 1;
	
	/**
	 * ::numero::
	 */		
	public $IN_PESO_PARTICIPACAO_AVALIACAO = 1;
	
	/**
	 * ::numero::
	 */		
	public $IN_PESO_NOTA_AVALIACAO = 1;
	
	/**
	 * ::numero::
	 */		
	public $IN_PESO_NOTA_SIMULADO = 1;
	
	
	/**
	 * ::texto::
	*/	
	public $IN_TIPO_AGRUPAMENTO;
	
	/**
	 * ::numero::
	 */
	public $NR_LIMITE;
	
	/**
	 * ::numero::
	 */
	public $CD_EMPRESA;

	//public $filtro;
	
	/*
	public function inserir(){

		$this->filtro->IN_TIPO_FILTRO = 2;
		
		$filtro = $this->dao->inserirObjeto($this->filtro);
		
		$this->CD_FILTRO = $filtro->CD_FILTRO;
		
		$this->dao->inserirObjeto($this);
		
	}
	
	public function alterar(){
		
		$farolAnterior = new col_farol();
		$farolAnterior->CD_FAROL = $this->CD_FAROL;
		
		$farolAnterior = $farolAnterior->obter();
		
		$this->CD_FILTRO = $farolAnterior->CD_FILTRO;
		$this->filtro->IN_TIPO_FILTRO = 2;
		$this->filtro->CD_FILTRO = $farolAnterior->CD_FILTRO;
		
		$this->filtro->alterar();
		
		$this->dao->alterarObjeto($this);

	}
	
	public function obter(){
		
		$farol = $this->dao->obterObjeto($this);
		
		$filtro = new col_filtro_farol();
		$filtro->CD_FILTRO = $farol->CD_FILTRO;
		
		$filtro = $filtro->obter();
		
		$farol->filtro = $filtro;
		
		return $farol;
	}
	*/
}


/**
 * ::col_filtro::
 */
class col_filtro extends RnBaseStatus
{
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	
	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_FILTRO;
	
	/**
	 * ::texto::
	*/	
	public $NM_FILTRO;
	
	/**
	 * ::numero::
	 */
	public $CD_EMPRESA;
	
	/**
	 * ::numero::
	*/
	public $IN_TIPO_FILTRO;
	
	/**
	 * ::numero::
	*/
	public $IN_FILTRO_RANKING;
	
	/**
	 * ##col_filtro_cargo_funcao##
	 * ::colecao::
	 */
	public $cargos_funcoes;
	
	/**
	 * ##col_filtro_forum##
	 * ::colecao::
	 */
	public $foruns;
	
	/**
	 * ##col_filtro_lotacao##
	 * ::colecao::
	 */
	public $lotacoes;
	
	/**
	 * ##col_filtro_prova##
	 * ::colecao::
	 */
	public $provas;
	
	/**
	 * ##col_filtro_usuario##
	 * ::colecao::
	 */
	public $usuarios;
	
	
	public function listarTodos($parametros = null, $ordernacao = "DS_EMPRESA"){
		
		$sql = "SELECT
					f.*, e.DS_EMPRESA
				FROM
					col_filtro f
					LEFT JOIN col_empresa e ON f.CD_EMPRESA = e.CD_EMPRESA
				 ORDER BY $ordernacao";
		
		return $this->dao->executeQuery($sql,true);
		
	}
	
}

/**
 * ::col_filtro_cargo_funcao::
 */
class col_filtro_cargo_funcao extends RnBase 
{
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	
	/**
	/* @pk
	 * ::numero::
	 */	
	public $CD_FILTRO;
	
	/**
	 * ::texto::
	*/	
	public $NM_CARGO_FUNCAO;
	
}

/**
 * ::col_filtro_forum::
 */
class col_filtro_forum  extends RnBase 
{
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	
	/**
	/* @pk
	 * ::numero::
	 */	
	public $CD_FILTRO;
	
	/**
	 * ::numero::
	*/	
	public $CD_FORUM;
	
}


/**
 * ::col_filtro_lotacao::
 */
class col_filtro_lotacao  extends RnBase 
{
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	
	/**
	/* @pk
	 * ::numero::
	 */	
	public $CD_FILTRO;
	
	/**
	 * ::numero::
	*/	
	public $CD_LOTACAO;
	
}

/**
 * ::col_filtro_prova::
 */
class col_filtro_prova  extends RnBase 
{
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	
	/**
	/* @pk
	 * ::numero::
	 */	
	public $CD_FILTRO;
	
	/**
	 * ::numero::
	*/	
	public $CD_PROVA;
	
}

/**
 * ::col_filtro_usuario::
 */
class col_filtro_usuario  extends RnBase 
{
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	
	/**
	/* @pk
	 * ::numero::
	 */	
	public $CD_FILTRO;
	
	/**
	 * ::numero::
	*/	
	public $CD_USUARIO;
	
}

/**
 * ::col_filtro_farol::
 */
class col_filtro_farol extends col_filtro 
{
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}

	public function listarTodos($parametros = null, $ordernacao = "DS_EMPRESA"){
		
		$sql = "SELECT
					*, e.DS_EMPRESA
				FROM
					col_filtro_farol f
					LEFT JOIN col_empresa e ON f.CD_EMPRESA = e.CD_EMPRESA
				 ORDER BY $ordernacao";
		
		return $this->dao->executeQuery($sql,true);
		
	}
	
}




/**
 * ::col_programa::
 */
class col_programa extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}	

	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_PROGRAMA;
	
	
	/**
	 * ::numero::
	 */	
	public $CD_EMPRESA;
	
	/**
	 * ::texto::
	 */
	public $DS_PROGRAMA;
	
	/**
	 * ##col_ciclo##
	 * ::colecao::
	 */
	public $ciclos;

}

/**
 * ::col_ciclo::
 */
class col_ciclo extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}	

	/**
	 * @identity
	 * ::numero::
	 * ##identity_insert##
	 */
	public $CD_CICLO;
	
	/**
	 * @pk
	 * ::numero::
	 */	
	public $CD_EMPRESA;
	
	/**
	 * ::numero::
	 */	
	public $CD_PROGRAMA;
	
	/**
	 * ::texto::
	 */
	public $NM_CICLO;
	
	/**
	 * ::data::
	 */		
	public $DT_INICIO;
	
	/**
	 * ::data::
	 */
	public $DT_TERMINO;


}

/**
 * ::col_capitulo::
 */
class col_capitulo extends RnBaseStatus {

    public function __construct($daoObject = null){
        parent::__construct($daoObject);
    }

    /**
     * @identity
     * ::numero::
     */
    public $CD_CAPITULO;

    /**
     * ::numero::
     */
    public $CD_PASTA;

    /**
     * ::texto::
     */
    public $NM_CAPITULO;

    /**
     * ::numero::
     */
    public $NR_ORDEM_CAPITULO;


    private function SalvarProvas()
    {

        $codigoCapitulo = $this->CD_CAPITULO;
        $nomeCapitulo = $this->NM_CAPITULO;

        if(!$this->dao->executeQuery("UPDATE col_prova SET DS_PROVA = '$nomeCapitulo' WHERE CD_CAPITULO = $codigoCapitulo")){

        }else{

            //Inserir a prova
            $sql = "INSERT INTO col_prova
                (DS_PROVA, DT_REALIZACAO, DURACAO_PROVA, NR_QTD_PERGUNTAS_PAGINA, NR_QTD_PERGUNTAS_TOTAL, VL_MEDIA_APROVACAO, IN_PROVA, IN_ATIVO, DT_FIM_REALIZACAO, NR_TENTATIVA, IN_GABARITO, IN_OBS, CD_CAPITULO)
                SELECT
                NM_CAPITULO, 	'2013-07-04 00:00:00', 30, 			50, 											5,											7,									0,				1,				'2030-12-31 00:00:00', 999,			1,						0,			CD_CAPITULO
                from col_capitulo WHERE CD_CAPITULO NOT IN (SELECT CD_CAPITULO FROM col_prova WHERE CD_CAPITULO IS NOT NULL)";

            $this->dao->executeQuery($sql);

            $sql = "INSERT INTO col_prova_chamada
                (CD_PROVA, DT_INICIO_REALIZACAO, DT_TERMINO_REALIZACAO, HR_INICIO_REALIZACAO, HR_TERMINO_REALIZACAO, IN_PRIMEIRA_CHAMADA)
                SELECT
                    p.CD_PROVA, '0000-00-00 00:00:00', '2030-12-31 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1
                FROM
                    col_prova p
                    LEFT JOIN col_prova_chamada pc ON p.CD_PROVA = pc.CD_PROVA
                WHERE
                    p.CD_CAPITULO IS NOT NULL
                AND pc.CD_PROVA IS NULL";

            $this->dao->executeQuery($sql);

        }

        $sql = "INSERT INTO col_prova_vinculo
                (CD_PROVA, CD_VINCULO, IN_TIPO_VINCULO)
                SELECT
                    distinct CD_PROVA, pi.CD_EMPRESA, 1
                FROM
                    col_prova p
                    INNER JOIN col_capitulo c ON c.CD_CAPITULO = p.CD_CAPITULO
                    INNER JOIN tb_paginasindividuais pi ON pi.CD_CAPITULO = c.CD_CAPITULO
                WHERE
                    pi.Status = 1
                AND pi.CD_EMPRESA IS NOT NULL
                AND
                    p.CD_PROVA NOT IN (SELECT CD_PROVA FROM col_prova_vinculo pv WHERE pv.CD_VINCULO = pi.CD_EMPRESA AND pv.IN_TIPO_VINCULO = 1)";

        $this->dao->executeQuery($sql);

        $sqlAplicada = "INSERT INTO col_prova_aplicada
                            (CD_PROVA, CD_USUARIO)
                            SELECT
                                pv.CD_PROVA, u.CD_USUARIO
                            FROM
                                col_usuario u
                                INNER JOIN col_prova_vinculo pv ON u.empresa = pv.CD_VINCULO
                            WHERE
                              pv.IN_TIPO_VINCULO = 1
                            AND NOT EXISTS(SELECT pa.CD_PROVA FROM col_prova_aplicada pa WHERE pa.CD_PROVA = pv.CD_PROVA AND pa.CD_USUARIO = u.CD_USUARIO)";

        $this->dao->executeQuery($sqlAplicada);


    }

    public function depoisDeInserir()
    {
        $this->SalvarProvas();
    }

    public function depoisDeAlterar()
    {
        $this->SalvarProvas();
    }

}

/**
 * ::col_pesquisa::
 */
class col_pesquisa extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * @identity
	 * ::numero::
	 */		
	public $CD_PESQUISA;
	
	/**
	 * ::numero::
	 */		
	public $CD_EMPRESA;
	
	/**
	 * ::texto::
	 */	
	public $DS_PESQUISA;
	
	/**
	 * ::data::
	 */
	public $DT_INICIO;
	
	/**
	 * ::data::
	 */
	public $DT_TERMINO;

    /**
     * ::data::
     */
    public $DT_INICIO_RELATORIO;

    /**
     * ::data::
     */
    public $DT_TERMINO_RELATORIO;
	
	/**
	 * ##col_pergunta_pesquisa##
	 * ::colecao::
	 */
	public $perguntas;
}

/**
 * ::col_pergunta_pesquisa::
 */
class col_pergunta_pesquisa extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}	

	/**
	 * @identity
	 * ::numero::
	 * ##identity_insert##
	 */
	public $CD_PERGUNTA_PESQUISA;
	
	/**
	 * @pk
	 * ::numero::
	 */	
	public $CD_PESQUISA;
	
	/**
	 * @fk
	 * ::numero::
	 */	
	public $CD_EMPRESA;
	
	/**
	 * ::texto::
	 */
	public $DS_PERGUNTA;
	
	/**
	 * ::numero::
	 */		
	public $NR_ORDEM;
}

/**
 * ::col_interacao_programada::
 */
class col_interacao_programada extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}	
	
	/**
	 * @identity
	 * ::numero::
	 */
	public $CD_INTERACAO_PROGRAMADA;
	
	/**
	 * ::texto::
	 */	
	public $DS_ASSUNTO;
	
	/**
	 * ::texto::
	 */	
	public $DS_PROGRAMA;
	
	/**
	 * ::texto::
	 */	
	public $DS_TEXTO_HEADER;
	
	/**
	 * ::texto::
	 */	
	public $DS_TEXTO_FOOTER;
	
	/**
	 * ::texto::
	 */	
	public $CD_RELATORIOS;
	
	/**
	 * ::texto::
	 */	
	public $QUERY_SQL;
	
	/**
	 * @FK
	 * ::numero::
	 */
	public $CD_USUARIO_CADASTRADOR;

	/**
	 * ::numero::
	 */
	public $DT_CADASTRO = 'sysdate()';
	
	/**
	 * ::data::
	 */
	public $DT_REALIZADO;
	
	/**
	 * ::data::
	 */
	public $DT_INICIO_VIGENCIA;
	
	/**
	 * ::data::
	 */
	public $DT_TERMINO_VIGENCIA;
	
	/**
	 * ::texto::
	 */	
	public $CD_TIPO_USUARIO;
	
	/**
	 * ::texto::
	 */	
	public $CD_PERIODICIDADE;
	
}

/**
 * ::col_cliente::
 */
class col_cliente extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}	

	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_CLIENTE;
	
	/**
	 * ::texto::
	 */
	public $NM_CLIENTE;
}


/**
 * ::col_localidade::
 */
class col_localidade extends RnBaseStatus {
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	/**
	 * @identity
	 * ::numero::
	 */	
	public $CD_LOCALIDADE;
	
	/**
	 * ::numero::
	 */	
	public $CD_EMPRESA;
	
	/**
	 * ::texto::
	 */
	public $NM_LOCALIDADE;
	
}

/**
 * ::wp_blogs::
 */
class wp_blogs extends RnBaseStatus {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}	

	/**
	 * @identity
	 * ::numero::
	 */	
	public $BLOG_ID;
	
	/**
	 * ::texto::
	 */
	public $PATH;
}


class RnBase{
	
	public $dao;
	
	public function __construct($daoObject = null){
		if ($daoObject == null)
			$this->dao = new DaoEngine();
		else
			$this->dao = $daoObject;

		//$this->dao = new DaoEngine();
	}
	
	public function listar($parametros = null, $ordernacao = ""){
		
		return $this->dao->listarObjeto($this,$parametros,$ordernacao);
		
	}
	
	public function inserir(){
		
		return $this->dao->inserirObjeto($this);
		
	}
	
	public function obter(){
		return $this->dao->obterObjeto($this);
	}
	
	public function alterar(){
		return $this->dao->alterarObjeto($this);
	}
	
	public function excluir(){
		return $this->dao->excluirObjeto($this);
	}

	
}


class RnBaseStatus extends RnBase {

	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}	
	
	public function alterarStatus($ativos, $inativos){
		
		$classe = new ReflectionClass(get_class($this));
	    
	    $tabela = $this->dao->obterTabela($classe->getDocComment());
	    
	    $propriedades = $classe->getProperties();
	    
	    $chave = "";
	    
	    foreach($propriedades as $propriedade){
			
			if ($propriedade->isPublic()){
				
				$comentario = $propriedade->getDocComment();
				
				$ehChave = $this->dao->ehChave($comentario);
				if ($ehChave){
					$chave = $propriedade->getName();
					break;
				}
			}
	    }
		
		$sql = "UPDATE $tabela SET IN_ATIVO = 1 WHERE $chave in ($ativos)";
		
		$this->dao->executeQuery($sql);
		$sql = "UPDATE $tabela SET IN_ATIVO = 0 WHERE $chave in ($inativos)";
		
		$this->dao->executeQuery($sql);
		
		//echo $sql;
		
	}
	
	/**
	 * __ativo__
	 */
	public $IN_ATIVO = 1;
	
	
}

class col_copia_programa extends RnBase {
	
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	public $empresaOrigem;
	public $empresaDestino;
	public $copiarProva;
	public $copiarSimulado;
	public $copiarForum;
	public $copiarPesquisa;
	public $copiarComentario;
	public $copiarRadar;
	public $copiarAssessment;
	
	public function inserir(){
		
		$empresaOrigem = $this->empresaOrigem;
		$empresaDestino = $this->empresaDestino;
		
		if ($this->copiarProva || $this->copiarSimulado) {
			
			
			
		}
				
		
		if ($this->copiarPesquisa) {
			
			$sql = "SELECT CD_EMPRESA, DT_INICIO, DT_TERMINO, DS_PESQUISA, IN_ATIVO, CD_PESQUISA
					FROM col_pesquisa
					WHERE CD_EMPRESA = $empresaOrigem";
			
			$resultado = $this->dao->executeQuery($sql,true);
			
			while($linha = mysql_fetch_array($resultado)) {
				
				$sql = "INSERT INTO col_pesquisa 
						(CD_EMPRESA, DS_PESQUISA, IN_ATIVO)
						VALUES
						($empresaDestino, '{$linha["DS_PESQUISA"]}' , {$linha["IN_ATIVO"]})";
				
				$codigoPesquisa = $this->dao->executeQueryInsert($sql,true);
				
				$sql = "INSERT INTO col_pergunta_pesquisa (CD_PESQUISA, CD_EMPRESA, DS_PERGUNTA, NR_ORDEM)
						SELECT $codigoPesquisa, $empresaDestino, DS_PERGUNTA, NR_ORDEM FROM col_pergunta_pesquisa WHERE CD_PESQUISA = {$linha["CD_PESQUISA"]} AND CD_EMPRESA = {$linha["CD_EMPRESA"]}";
				
				$this->dao->executeQueryInsert($sql,true);
			}
			
		}
		
		if ($this->copiarRadar) {
		
			$sql = "INSERT INTO col_farol
						(DT_TERMINO,
						DT_INICIO,
						NM_FAROL,
						IN_ATIVO,
						CD_FILTRO,
						NR_PESO_PARTICIPACAO,
						NR_PESO_ACESSO,
						NR_PESO_DOWNLOAD,
						NR_PESO_PARTICIPACAO_SIMULADO,
						NR_PESO_NOTA_SIMULADO,
						NR_PESO_QUANTIDADE_SIMULADO,
						NR_PESO_PARTICIPACAO_FORUM,
						NR_PESO_CONTRIBUICAO_FORUM,
						NR_PESO_PARTICIPACAO_AVALIACAO,
						NR_PESO_NOTA_AVALIACAO,
						NR_LIMITE,
						IN_TIPO_AGRUPAMENTO,
						IN_PESO_PARTICIPACAO,
						IN_PESO_ACESSO,
						IN_PESO_DOWNLOAD,
						IN_PESO_PARTICIPACAO_SIMULADO,
						IN_PESO_NOTA_SIMULADO,
						IN_PESO_QUANTIDADE_SIMULADO,
						IN_PESO_PARTICIPACAO_FORUM,
						IN_PESO_CONTRIBUICAO_FORUM,
						IN_PESO_PARTICIPACAO_AVALIACAO,
						IN_PESO_NOTA_AVALIACAO,
						CD_EMPRESA)
						SELECT
						DT_TERMINO,
						DT_INICIO,
						NM_FAROL,
						IN_ATIVO,
						CD_FILTRO,
						NR_PESO_PARTICIPACAO,
						NR_PESO_ACESSO,
						NR_PESO_DOWNLOAD,
						NR_PESO_PARTICIPACAO_SIMULADO,
						NR_PESO_NOTA_SIMULADO,
						NR_PESO_QUANTIDADE_SIMULADO,
						NR_PESO_PARTICIPACAO_FORUM,
						NR_PESO_CONTRIBUICAO_FORUM,
						NR_PESO_PARTICIPACAO_AVALIACAO,
						NR_PESO_NOTA_AVALIACAO,
						NR_LIMITE,
						IN_TIPO_AGRUPAMENTO,
						IN_PESO_PARTICIPACAO,
						IN_PESO_ACESSO,
						IN_PESO_DOWNLOAD,
						IN_PESO_PARTICIPACAO_SIMULADO,
						IN_PESO_NOTA_SIMULADO,
						IN_PESO_QUANTIDADE_SIMULADO,
						IN_PESO_PARTICIPACAO_FORUM,
						IN_PESO_CONTRIBUICAO_FORUM,
						IN_PESO_PARTICIPACAO_AVALIACAO,
						IN_PESO_NOTA_AVALIACAO,
						$empresaDestino
						FROM
							col_farol
						WHERE
							CD_EMPRESA = $empresaOrigem";
						
						$this->dao->executeQuery($sql,true);
		}
		
		if ($this->copiarComentario) {
			$sql = "INSERT INTO col_comentario (NM_PAGINA, TX_COMENTARIO, CD_EMPRESA, IN_ATIVO)
				SELECT NM_PAGINA, TX_COMENTARIO, $empresaDestino, IN_ATIVO FROM col_comentario WHERE CD_EMPRESA = $empresaOrigem";
		
			$this->dao->executeQuery($sql,true);
		}
		
		if ($this->copiarForum) {
			$sql = "INSERT INTO col_foruns
						(DS_FORUM, CD_EMPRESA, IN_ATIVO, SUMARIO, CD_USUARIO_MEDIADOR, TEXTAREA, EMAIL, DT_INICIO_VIGENCIA, DT_TERMINO_VIGENCIA)
						SELECT DS_FORUM, $empresaDestino, IN_ATIVO, SUMARIO, CD_USUARIO_MEDIADOR, TEXTAREA, EMAIL, DT_INICIO_VIGENCIA, DT_TERMINO_VIGENCIA FROM col_foruns WHERE CD_EMPRESA = $empresaOrigem
			";
			
			$this->dao->executeQuery($sql,true);
			
		}
		
		
		
	}
	
	
}

class col_usuario_backup extends RnBase {
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	public $empresa;
	
	public function inserir(){
		
		$codigoEmpresa = $this->empresa;
		
		$sql = "INSERT INTO col_usuario_backup
				(CD_USUARIO,
				tipo,
				grupo,
				permissao,
				NM_USUARIO,
				NM_SOBRENOME,
				datanasc,
				naturalidade,
				uf,
				nacionalidade,
				enderecores,
				cepres,
				dddtelres,
				telres,
				dddcel,
				cel,
				email,
				empresa,
				lotacao,
				cargofuncao,
				enderecocom,
				cepcom,
				dddtelcom,
				telcom,
				ramal,
				experiencia,
				outrosexperiencia,
				especializacao,
				outrosespecializacao,
				fotofile,
				cvfile,
				cv,
				newsletteragreement,
				policyagreement,
				datacadastro,
				`Status`,
				login,
				senha,
				DT_IMPORTACAO,
				IN_SENHA_ALTERADA,
				CD_FILTRO,
				CD_USUARIO_CADASTRADOR)
				
				
				select 
				
				CD_USUARIO,
				tipo,
				grupo,
				permissao,
				NM_USUARIO,
				NM_SOBRENOME,
				datanasc,
				naturalidade,
				uf,
				nacionalidade,
				enderecores,
				cepres,
				dddtelres,
				telres,
				dddcel,
				cel,
				email,
				empresa,
				lotacao,
				cargofuncao,
				enderecocom,
				cepcom,
				dddtelcom,
				telcom,
				ramal,
				experiencia,
				outrosexperiencia,
				especializacao,
				outrosespecializacao,
				fotofile,
				cvfile,
				cv,
				newsletteragreement,
				policyagreement,
				datacadastro,
				`Status`,
				login,
				senha,
				DT_IMPORTACAO,
				IN_SENHA_ALTERADA,
				CD_FILTRO,
				CD_USUARIO_CADASTRADOR
				from col_usuario
				
				WHERE empresa = $codigoEmpresa
				
				";
		$this->dao->executeQuery($sql,true);
		return null;
		
	}
	
}

/**
 * ::col_importacao::
 */
class col_importacao extends RnBase {
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	
	/**
	/* @identity
	 * ::numero::
	 */	
	public $CD_IMPORTACAO;
	
	/**
	 * ::texto::
	 */
	public $NM_ARQUIVO;
	
}




function InserirErroImportacao($codigoImportacao, $tipoImportacao, $codigoHierarquiaImportacao, $erroImportacao, $importado = 1) {
	$sql = "INSERT INTO col_erro_importacao (CD_IMPORTACAO, IN_TIPO_IMPORTACAO, CD_HIERARQUIA_IMPORTACAO, DS_ERRO, IN_IMPORTADO)
				VALUES ($codigoImportacao, '$tipoImportacao', $codigoHierarquiaImportacao, '$erroImportacao', $importado)";
	
	DaoEngine::getInstance()->executeQuery($sql, true);
}

class col_hierarquia_importacao extends RnBase
{
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	public $xml;
	public $empresa;
	public $nomeArquivo;
	public $codigoImportacao;
	public $estrutura;
	public $hierarquiaLotacao;
	public $erros;
    public $diretorioArquivo;

    public function inserir()
    {

        require_once '../Classes/PHPExcel.php';
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '32MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        if (!(isset($definesIncluido) && $definesIncluido)){
            include_once("../include/defines.php");
        }

        $inputFileType = PHPExcel_IOFactory::identify("{$this->diretorioArquivo}{$this->nomeArquivo}");
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objReader->setReadDataOnly(true);

        $objPHPExcel = $objReader->load("{$this->diretorioArquivo}{$this->nomeArquivo}");

        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);

        $highestRow = $objWorksheet->getHighestRow();

        $codigoEmpresa = $this->empresa;

        //Abre a conex�o
        DaoEngine::getInstance()->obterConexao();

        for ($row = 2; $row <= $highestRow;++$row)
        {

            $codigo = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue()));
            $nome = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue()));

            if($codigo == "" || $nome == "")
                continue;

            $query = "INSERT INTO col_lotacao (DS_LOTACAO, CD_EMPRESA, CD_IDENTIFICADOR_LOTACAO)
              SELECT '$nome', $codigoEmpresa, '$codigo' FROM DUAL WHERE NOT EXISTS(SELECT 1 FROM col_lotacao WHERE CD_IDENTIFICADOR_LOTACAO = '$codigo' AND CD_EMPRESA = $codigoEmpresa)";

            DaoEngine::getInstance()->executeQuery($query, false);

        }

        //Obter todas as lotacoes
        $query = "SELECT CD_LOTACAO, CD_IDENTIFICADOR_LOTACAO FROM col_lotacao WHERE CD_EMPRESA = $codigoEmpresa";
        $resultado = DaoEngine::getInstance()->executeQuery($query, false);

        $lotacoes = array();

        while($linha = mysql_fetch_array($resultado))
        {

            $lotacoes[$linha["CD_IDENTIFICADOR_LOTACAO"]] = $linha["CD_LOTACAO"];

        }

        mysql_free_result($resultado);

        $query = "UPDATE col_lotacao SET IN_ATIVO = 0 WHERE CD_EMPRESA = $codigoEmpresa";
        DaoEngine::getInstance()->executeQuery($query, false);
        //Update em todas as lota��es
        for ($row = 2; $row <= $highestRow;++$row)
        {

            $codigo = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(0, $row)->getCalculatedValue()));
            $nome = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue()));
            $codigoPai = trim(utf8_decode($objWorksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue()));

            if($codigo == "" || $nome == "")
                continue;

            if(array_key_exists($codigoPai,$lotacoes) && $codigoPai != $codigo){
                $codigoPai = $lotacoes[$codigoPai];
            }else{
                $codigoPai = "NULL";
            }

            $query = "UPDATE col_lotacao SET DS_LOTACAO = '$nome', CD_LOTACAO_PAI = $codigoPai, IN_ATIVO = 1
                WHERE CD_IDENTIFICADOR_LOTACAO = '$codigo' AND CD_EMPRESA = $codigoEmpresa";

            DaoEngine::getInstance()->executeQuery($query, false);
        }

        $query = "SELECT
                CONCAT('UPDATE col_lotacao SET DS_LOTACAO_HIERARQUIA = ''',
                CONCAT(IFNULL(CONCAT(l5.CD_LOTACAO, '.'),''), IFNULL(CONCAT(l4.CD_LOTACAO, '.'),''), IFNULL(CONCAT(l3.CD_LOTACAO, '.'),''), IFNULL(CONCAT(l2.CD_LOTACAO, '.'),''), l1.CD_LOTACAO),
                ''' WHERE CD_LOTACAO = ', l1.CD_LOTACAO, ';')
            FROM
                col_lotacao l1
                LEFT JOIN col_lotacao l2 ON l1.CD_LOTACAO_PAI = l2.CD_LOTACAO
                LEFT JOIN col_lotacao l3 ON l2.CD_LOTACAO_PAI = l3.CD_LOTACAO
                LEFT JOIN col_lotacao l4 ON l3.CD_LOTACAO_PAI = l4.CD_LOTACAO
                LEFT JOIN col_lotacao l5 ON l4.CD_LOTACAO_PAI = l5.CD_LOTACAO
            WHERE
                l1.CD_EMPRESA = $codigoEmpresa
            AND l1.IN_ATIVO = 1";

        $resultado = DaoEngine::getInstance()->executeQuery($query, false);

        while($linha = mysql_fetch_row($resultado))
        {

            DaoEngine::getInstance()->executeQuery($linha[0], false);

        }

        DaoEngine::getInstance()->fecharConexao();


    }

	public function inserirOld()
	{
		
		//Inserir Importa��o 
		$importacao = new col_importacao();
		$importacao->NM_ARQUIVO = $this->nomeArquivo;
		$importacao = $importacao->inserir();
		$codigoImportacao = $importacao->CD_IMPORTACAO;
		$empresa = $this->empresa;
		
		$dom = $this->xml;
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		
		$linhaAtual = 0;
		
		foreach ($rows as $row) {
			
			$index = 1;
			$cells = $row->getElementsByTagName( 'Cell' );
			
			$identificadorLotacao = null;
			$nomeLotacao = null;
			$identificadorLotacaoPai = null;
			$nomeHierarquia = null;
			
			if ( $linhaAtual > 0 ) { //Se for a primeira linha
			
				foreach( $cells as $cell )
				{
					$ind = $cell->getAttribute( 'Index' );
					if ( $ind != null ) $index = $ind;
					
					switch ($index) {
						case 1:
							$identificadorLotacao = $this->trataSQL($cell->nodeValue);
							break;
						case 2:
							$nomeLotacao = $this->trataSQL($cell->nodeValue);
							break;
						case 3:
							$nomeHierarquia = $this->trataSQL($cell->nodeValue);
							break;
						case 4:
							$identificadorLotacaoPai = $this->trataSQL($cell->nodeValue);
							break;
					}
					
					
					
					$index++;
					
				}
			
			if (trim($identificadorLotacao) == "" || $identificadorLotacao == null) break;
				
			$sql = "INSERT INTO
					col_hierarquia_importacao
						(CD_IMPORTACAO, CD_LOTACAO, NM_LOTACAO, CD_LOTACAO_PAI, NM_HIERARQUIA, CD_EMPRESA)
					VALUES
						($codigoImportacao, '$identificadorLotacao', '$nomeLotacao', '$identificadorLotacaoPai', '$nomeHierarquia', $empresa)";
			
			$this->dao->executeQuery($sql, true);
			
			}
			
			$linhaAtual++;
			
		}
		
		$sql = "SELECT * FROM col_hierarquia_importacao WHERE CD_EMPRESA = $empresa AND CD_IMPORTACAO = $codigoImportacao ORDER BY CD_LOTACAO_PAI";
		
		$resultado = $this->dao->executeQuery($sql, true);
		
		$itensInseridos = array();
		
		$niveis = $this->obterNiveisHierarquicos($empresa);
		$lotacoes = $this->obterLotacoes($empresa);
		
		while ($linha = mysql_fetch_array($resultado)) {
			
			$identificadorLotacao = $linha["CD_LOTACAO"];
			$nomeLotacao = $linha["NM_LOTACAO"];
			$nomeHierarquia = $linha["NM_HIERARQUIA"];
			$codigoHierarquiaImportacao = $linha["CD_HIERARQUIA_IMPORTACAO"];
			$erro = "";
			if ($nomeLotacao == "" ) {
				$erro = $erro . "A coluna nome da lota��o n�o foi preenchida.\n";
			}
			
			if ($erro == "") {
				
				$codigoHierarquia = null;
				if ($nomeHierarquia != "") {
					if ($niveis[strtoupper($nomeHierarquia)] == null) {
						$codigoHierarquia = count($niveis) + 1;
						$sql = "INSERT INTO col_hierarquia (DS_HIERARQUIA, CD_NIVEL_HIERARQUICO, CD_EMPRESA) VALUES ('$nomeHierarquia', $codigoHierarquia, $empresa)";
						$this->dao->executeQuery($sql, true);
						$niveis[strtoupper($nomeHierarquia)] = $codigoHierarquia;
					} else {
						$codigoHierarquia = $niveis[strtoupper($nomeHierarquia)];
					}
						
				}
				
				$codigoHierarquia = "null";
				
				$nomeLotacao = $this->trataSQL($nomeLotacao);
				
				
				if ($lotacoes[strtoupper($identificadorLotacao)] == null) {
				
					$identificadorLotacao = $this->trataSQL($identificadorLotacao);
					
					$sql = "INSERT INTO col_lotacao (DS_LOTACAO, CD_EMPRESA, IN_ATIVO, CD_NIVEL_HIERARQUICO, CD_IDENTIFICADOR_LOTACAO) 
								VALUES ('$nomeLotacao', $empresa, 1, $codigoHierarquia, '$identificadorLotacao')";
					
					
					$lotacoes[strtoupper($identificadorLotacao)] = $identificadorLotacao;
					
				} else {
					
					$identificadorLotacao = $this->trataSQL($identificadorLotacao);
					
					$sql = "UPDATE col_lotacao SET DS_LOTACAO = '$nomeLotacao', CD_NIVEL_HIERARQUICO = $codigoHierarquia 
								WHERE CD_IDENTIFICADOR_LOTACAO = '$identificadorLotacao' AND CD_EMPRESA = $empresa";
					
				}
				
				$this->dao->executeQuery($sql, true);
				
			} else { //Se ocorreu erro
				InserirErroImportacao($codigoImportacao, 'H', $codigoHierarquiaImportacao, $erro, 0);
			}
			
		}
		
		mysql_data_seek($resultado, 0);
		
		$lotacoes = $this->obterLotacoes($empresa);
		
		while ($linha = mysql_fetch_array($resultado)) {
			
			if ($nomeLotacao == "" ) continue;
			
			$identificadorLotacaoPai = $linha["CD_LOTACAO_PAI"];
			$identificadorLotacao = $linha["CD_LOTACAO"];
			
			if ($identificadorLotacaoPai == "") continue;
			
			if ($lotacoes[strtoupper($identificadorLotacaoPai)] == null) {
				$erro = "O identificador da unidade pai n�o foi encontrado";
				InserirErroImportacao($codigoImportacao, 'H', $codigoHierarquiaImportacao,$erro, 1);
				continue;
			}
			
			$sql = "UPDATE col_lotacao SET CD_LOTACAO_PAI = {$lotacoes[strtoupper($identificadorLotacaoPai)]["CD_LOTACAO"]}
						WHERE CD_IDENTIFICADOR_LOTACAO = '$identificadorLotacao' AND CD_EMPRESA = $empresa";
			
			$this->dao->executeQuery($sql, true);
			
		}
		
		
		$lotacoes = $this->obterLotacoes($empresa, true);
		
		foreach ($lotacoes as $lotacao) {
			
			$descricaoHierarquia = $this->obterDescricaoHierarquia($lotacoes, $lotacao);
			$sql = "UPDATE col_lotacao SET DS_LOTACAO_HIERARQUIA = '$descricaoHierarquia'
						WHERE CD_LOTACAO = '{$lotacao["CD_LOTACAO"]}' AND CD_EMPRESA = $empresa";
			
			$this->dao->executeQuery($sql, true);
		}
		
		
		//Obter erros duplicados
		$sql = "SELECT CD_HIERARQUIA_IMPORTACAO FROM col_hierarquia_importacao 
					WHERE 	CD_EMPRESA = $empresa AND CD_IMPORTACAO = $codigoImportacao AND
							CD_LOTACAO IN (
								SELECT CD_LOTACAO FROM col_hierarquia_importacao 
									WHERE CD_EMPRESA = $empresa AND CD_IMPORTACAO = $codigoImportacao GROUP BY CD_LOTACAO HAVING COUNT(1) > 1
					)";
		$resultado = $this->dao->executeQuery($sql, true);
		
		while ($linha = mysql_fetch_row($resultado)) {
			$codigoHierarquiaImportacao = $linha[0];
			$erro = "Registro com c�digos repetidos. Apenas uma das linhas foi importada";
			InserirErroImportacao($codigoImportacao, 'H', $codigoHierarquiaImportacao,$erro, 1);
		}
		
		$this->erros = $erro;
		$this->codigoImportacao = $codigoImportacao;
		return $this;
	}
	
	public function obterRelatorioErrosImportacao($codigoImportacao) {
		
		$sql = "SELECT
					e.DS_ERRO, h.CD_LOTACAO, h.NM_LOTACAO, h.CD_LOTACAO_PAI, h.NM_HIERARQUIA, i.DT_IMPORTACAO, p.DS_EMPRESA, IN_IMPORTADO,
					(SELECT COUNT(1) FROM col_hierarquia_importacao WHERE CD_IMPORTACAO = $codigoImportacao) AS QT_IMPORTADA
				FROM
					col_erro_importacao e
					INNER JOIN col_hierarquia_importacao h ON h.CD_HIERARQUIA_IMPORTACAO = e.CD_HIERARQUIA_IMPORTACAO
					INNER JOIN col_importacao i ON i.CD_IMPORTACAO = h.CD_IMPORTACAO
					INNER JOIN col_empresa p ON p.CD_EMPRESA = h.CD_EMPRESA
				WHERE e.CD_IMPORTACAO = $codigoImportacao";
		
		$resultado = $this->dao->executeQuery($sql, true);
		
		return $resultado;
		
	}
	
	private function obterDescricaoHierarquia(&$lotacoes, &$lotacao)
	{
		
		$lotacaoHierarquia = "";
		$codigoLotacaoPai = $lotacao["CD_LOTACAO_PAI"];
		if ($codigoLotacaoPai != null && $codigoLotacaoPai != "" && $lotacoes[$codigoLotacaoPai] != null && $codigoLotacaoPai != $lotacao["CD_LOTACAO"])
		{
			$lotacaoHierarquiaPai = $this->obterDescricaoHierarquia($lotacoes, $lotacoes[$codigoLotacaoPai]);
			$lotacaoHierarquia = "{$lotacaoHierarquia}{$lotacaoHierarquiaPai}.";
		}
	
		$lotacaoHierarquia = "$lotacaoHierarquia{$lotacao["CD_LOTACAO"]}";
		
		return $lotacaoHierarquia;
		
	}
	
	private function obterNiveisHierarquicos($empresa)
	{
		$sql = "SELECT CD_NIVEL_HIERARQUICO, DS_HIERARQUIA FROM col_hierarquia WHERE CD_EMPRESA = $empresa";
		$resultado = $this->dao->executeQuery($sql,true);
		
		$niveis = array();
		
		while ($linha = mysql_fetch_row($resultado))
		{
			
			$nivelHierarquico = $linha[0];
			$nomeHirarquia = strtoupper($linha[1]);
			
			$niveis[$nomeHirarquia] = $nivelHierarquico;
			
		}
		
		return $niveis;
		
	}
	
	private function obterLotacoes($empresa, $usarChaveComoid = false)
	{
		$sql = "SELECT DS_LOTACAO, CD_LOTACAO, CD_NIVEL_HIERARQUICO, CD_LOTACAO_PAI, CD_IDENTIFICADOR_LOTACAO FROM col_lotacao WHERE CD_EMPRESA = $empresa";
		$resultado = $this->dao->executeQuery($sql,true);
		
		$lotacoes = array();
		
		$chave = "CD_IDENTIFICADOR_LOTACAO";
		
		if ($usarChaveComoid) {
			$chave = "CD_LOTACAO";
		}
		
		while ($linha = mysql_fetch_array($resultado))
		{

			$identificadorLotacao = strtoupper($linha[$chave]);
			$lotacoes[$identificadorLotacao] = $linha;
			
		}
		
		return $lotacoes;
	}
	
	private function trataSQL($valor)
	{
		$valor = utf8_decode($valor);
		$valor = str_replace("'", "\\'", $valor);
		$valor = trim($valor);
		
		return $valor;
		
	}
	
}

class ResumoImportacao {
	
	public $quantidadeLinhasProcessadas = 0;
	public $quantidadeLinhasNaoProcessadas = 0;
	public $quantidadeLotacoesProcessadas = 0;
	public $quantidadeFiltrosProcessados = 0;
	public $quantidadeCargosProcessados = 0;
	public $quantidadeSuperUsuariosPrograma = 0;
	public $quantidadeSuperUsuariosGrupo = 0;
	public $quantidadeParticipantes = 0;

	public $erros;
	
}

class Copia_Curso extends RnBase{

    public $cursoOrigem;
    public $cursoDestino;

    public function inserir() {

        $cursoOrigem = $this->cursoOrigem;
        $cursoDestino = $this->cursoDestino;

        $rsEmpresa = $this->dao->executeQuery("SELECT CD_EMPRESA FROM tb_pastasindividuais WHERE ID = $cursoDestino");
        $linha = mysql_fetch_row($rsEmpresa);
        $oEmpresaId = $linha[0];

        //CopiarCapitulos
        //Obter os cap�tulos do curso
        $sql = "SELECT CD_CAPITULO, NM_CAPITULO FROM col_capitulo WHERE CD_PASTA = $cursoOrigem";
        $resultado = $this->dao->executeQuery($sql);

        while($linha = mysql_fetch_row($resultado)){
            $sql = "INSERT INTO col_capitulo (NM_CAPITULO, NR_ORDEM_CAPITULO, CD_PASTA, IN_ATIVO, Conteudo)
                    SELECT NM_CAPITULO, NR_ORDEM_CAPITULO, $cursoDestino, IN_ATIVO, Conteudo FROM col_capitulo WHERE CD_CAPITULO = {$linha[0]}";

            //Insere cada cap�tulo
            $codigoNovoCapitulo = $this->dao->executeQueryInsert($sql);
            $nomeCapitulo = $linha[1];

            cadastrarSimuladoCapitulo($nomeCapitulo, $oEmpresaId, $codigoNovoCapitulo);

            //Obter as P�ginas do cap�tulo
            $sql = "SELECT  ID, Titulo FROM tb_paginasindividuais WHERE CD_CAPITULO = {$linha[0]}";
            $resultado_paginas = $this->dao->executeQuery($sql);
            while($linhaPagina = mysql_fetch_row($resultado_paginas)){

                $sql = "INSERT INTO tb_paginasindividuais (PastaID, Titulo, Data, Conteudo, Draft, Status, DrafStatus, CD_EMPRESA, NR_ORDEM, CD_CAPITULO)
                        SELECT $cursoDestino, Titulo, Data, Conteudo, Draft, Status, DrafStatus, $oEmpresaId, NR_ORDEM, $codigoNovoCapitulo FROM tb_paginasindividuais WHERE ID = {$linhaPagina[0]}";

                $codigoNovaPagina = $this->dao->executeQueryInsert($sql);

                $nomePagina = $linhaPagina[1];

                $sqlDisciplina = "INSERT INTO col_disciplina (DS_DISCIPLINA, CD_EMPRESA, ID_PAGINA_INDIVIDUAL)
                  VALUES ('$nomePagina', $oEmpresaId,
                    (SELECT ID FROM tb_paginasindividuais WHERE TITULO = '$nomePagina' AND PASTAID = $cursoDestino AND CD_EMPRESA = $oEmpresaId)
                          )";

                $idDisciplina = $this->dao->executeQueryInsert($sqlDisciplina);

                $this->copiarDisciplinaPagina($linhaPagina[0] ,$idDisciplina);

                cadastrarSimuladoPagina($nomePagina, $oEmpresaId, $idDisciplina, $codigoNovaPagina);

            }

        }

        //CopiarPaginas
        //Obter as P�ginas do curso sem cap�tulos
        $sql = "SELECT  ID, Titulo FROM tb_paginasindividuais WHERE CD_CAPITULO IS NULL AND PastaID = $cursoOrigem";
        $resultado_paginas = $this->dao->executeQuery($sql);
        while($linhaPagina = mysql_fetch_row($resultado_paginas)){

            $sql = "INSERT INTO tb_paginasindividuais (PastaID, Titulo, Data, Conteudo, Draft, Status, DrafStatus, CD_EMPRESA, NR_ORDEM, CD_CAPITULO)
                        SELECT $cursoDestino, Titulo, Data, Conteudo, Draft, Status, DrafStatus, $oEmpresaId, NR_ORDEM, NULL FROM tb_paginasindividuais WHERE ID = {$linhaPagina[0]}";

            $codigoNovaPagina = $this->dao->executeQueryInsert($sql);
            $nomePagina = $linhaPagina[1];

            $sqlDisciplina = "INSERT INTO col_disciplina (DS_DISCIPLINA, CD_EMPRESA, ID_PAGINA_INDIVIDUAL)
                  VALUES ('$nomePagina', $oEmpresaId,
                    (SELECT ID FROM tb_paginasindividuais WHERE TITULO = '$nomePagina' AND PASTAID = $cursoDestino AND CD_EMPRESA = $oEmpresaId)
                          )";

            $idDisciplina = $this->dao->executeQueryInsert($sqlDisciplina);

            $this->copiarDisciplinaPagina($linhaPagina[0] ,$idDisciplina);

            cadastrarSimuladoPagina($nomePagina, $oEmpresaId, $idDisciplina, $codigoNovaPagina);

        }
    }

    private function copiarDisciplinaPagina($idPaginaOrigem, $idDisciplinaDestino){

        $sql = "SELECT CD_DISCIPLINA FROM col_disciplina WHERE ID_PAGINA_INDIVIDUAL = $idPaginaOrigem";

        $resultado = $this->dao->executeQuery($sql);

        $linha = mysql_fetch_row($resultado);
        $idDisciplinaOrigem = $linha[0];

        $disciplina = new col_disciplina();
        $disciplina->CD_DISCIPLINA = $idDisciplinaDestino;
        $disciplina->CD_DISCIPLINAS_COPIADAS = array();
        $disciplina->CD_DISCIPLINAS_COPIADAS[] = $idDisciplinaOrigem;
        $disciplina->copiarPerguntas(true);
    }

}

class col_empresa_assessment extends RnBase{

    public function __construct($daoObject = null){
        parent::__construct($daoObject);
    }

    public $CD_EMPRESA;

    public function alterar(){

        $this->salvar();

    }


    public function inserir(){

        $this->salvar();

    }

    private function salvar(){

        configurarAssessments($this->CD_EMPRESA);

    }

    public function obter(){

        return $this;

    }

}

class curso_empresa{

    public $curso;
    public $cor;
    public $ordem;

}

class area_conhecimento_empresa{
    public $area;
    public $cor;
    public $ordem;
}

class col_curso_empresa extends RnBase{

    public function __construct($daoObject = null){
        parent::__construct($daoObject);
    }

   public $CD_EMPRESA;

   public $cursos;

   public $areasConhecimento;

    public function alterar(){

        $this->salvar();

    }


    public function inserir(){

        $this->salvar();

    }

    private function salvar(){

        $sql = "DELETE FROM col_area_conhecimento_empresa WHERE CD_EMPRESA = " . $this->CD_EMPRESA;
        $this->dao->executeQuery($sql);

        foreach ($this->areasConhecimento as $area) {

            $idArea = $area->area;
            $nrordem = $area->ordem;
            $cor = $area->cor;

            if($nrordem == ""){
                $nrordem = "null";
            }

            $sql = "INSERT INTO col_area_conhecimento_empresa (CD_EMPRESA, CD_AREA_CONHECIMENTO, TX_COR, NR_ORDEM) VALUES ({$this->CD_EMPRESA},{$idArea}, '$cor', $nrordem)";
            $this->dao->executeQuery($sql);

        }


        $sql = "DELETE FROM col_curso_empresa WHERE CD_EMPRESA = " . $this->CD_EMPRESA;

        $this->dao->executeQuery($sql);

        foreach ($this->cursos as $curso) {

            $idcurso = $curso->curso;
            $nrordem = $curso->ordem;
            $cor = $curso->cor;

            if($nrordem == ""){
                $nrordem = "null";
            }

            $sql = "INSERT INTO col_curso_empresa (CD_EMPRESA, ID_PASTA_INDIVIDUAL, TX_COR, NR_ORDEM) VALUES ({$this->CD_EMPRESA},{$idcurso}, '$cor', $nrordem)";
            $this->dao->executeQuery($sql);

        }

        removerVinculosEmpresa($this->CD_EMPRESA);
        inserirTodosVinculos();

        configurarAssessments($this->CD_EMPRESA);

        //configurarGruposDisciplinas($this->CD_EMPRESA);

    }

    public function obter(){

        $sql = "SELECT ID_PASTA_INDIVIDUAL FROM col_curso_empresa WHERE CD_EMPRESA = " . $this->CD_EMPRESA;
        $resultado = $this->dao->executeQuery($sql);

        $this->cursos = array();

        while($linha = mysql_fetch_row($resultado)){

            $this->cursos[] = $linha[0];

        }

        $sql = "SELECT CD_AREA_CONHECIMENTO FROM col_area_conhecimento_empresa WHERE CD_EMPRESA = " . $this->CD_EMPRESA;
        $resultado = $this->dao->executeQuery($sql);

        $this->areasConhecimento = array();

        while($linha = mysql_fetch_row($resultado)){

            $this->areasConhecimento[] = $linha[0];

        }

        return $this;


    }

}

function removerVinculosEmpresa($cdEmpresa){

    $sql = "DELETE FROM col_prova_vinculo WHERE IN_TIPO_VINCULO = 1 AND CD_VINCULO = $cdEmpresa";
    DaoEngine::getInstance()->executeQuery($sql,true);

}

function inserirTodosVinculos(){

    $sql = "INSERT INTO col_prova_vinculo
            (CD_PROVA, CD_VINCULO, IN_TIPO_VINCULO)
            SELECT
                p.CD_PROVA, ce.CD_EMPRESA, 1
            FROM
                col_prova p
                INNER JOIN tb_pastasindividuais pi ON p.ID_PASTA_INDIVIDUAL = pi.ID
                INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pi.ID
            WHERE
                NOT EXISTS(SELECT 1 FROM col_prova_vinculo pv WHERE pv.CD_PROVA = p.CD_PROVA AND pv.CD_VINCULO = ce.CD_EMPRESA AND pv.IN_TIPO_VINCULO = 1)";

    DaoEngine::getInstance()->executeQuery($sql,true);


    $sql = "INSERT INTO col_prova_vinculo
            (CD_PROVA, CD_VINCULO, IN_TIPO_VINCULO)
            SELECT
                p.CD_PROVA, ce.CD_EMPRESA, 1
            FROM
                col_prova p
                INNER JOIN col_capitulo c ON c.CD_CAPITULO = p.CD_CAPITULO
                INNER JOIN tb_pastasindividuais pi ON c.CD_PASTA = pi.ID
                INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pi.ID
            WHERE
                NOT EXISTS(SELECT 1 FROM col_prova_vinculo pv WHERE pv.CD_PROVA = p.CD_PROVA AND pv.CD_VINCULO = ce.CD_EMPRESA AND pv.IN_TIPO_VINCULO = 1)";

    DaoEngine::getInstance()->executeQuery($sql,true);

    $sql = "INSERT INTO col_prova_vinculo
            (CD_PROVA, CD_VINCULO, IN_TIPO_VINCULO)
            SELECT
                p.CD_PROVA, ce.CD_EMPRESA, 1
            FROM
                col_prova p
                INNER JOIN tb_paginasindividuais pa ON pa.ID = p.ID_PAGINA_INDIVIDUAL
                INNER JOIN tb_pastasindividuais pi ON pa.PastaID = pi.ID
                INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pi.ID
            WHERE
                NOT EXISTS(SELECT 1 FROM col_prova_vinculo pv WHERE pv.CD_PROVA = p.CD_PROVA AND pv.CD_VINCULO = ce.CD_EMPRESA AND pv.IN_TIPO_VINCULO = 1)";

    DaoEngine::getInstance()->executeQuery($sql,true);

}



function configurarGruposDisciplinas($cdEmpresa){

    $sql = "DELETE FROM col_grupo_disciplina WHERE CD_EMPRESA = $cdEmpresa";
    DaoEngine::getInstance()->executeQuery($sql,true);

    $sql = "INSERT INTO col_grupo_disciplina
            (NM_GRUPO_DISCIPLINA, NR_ORDEM_DISCIPLINA, CD_EMPRESA, ID_PASTA)
            SELECT
                pi.Titulo, pi.NR_ORDEM, ce.CD_EMPRESA, pi.ID
            FROM
                tb_pastasindividuais pi
                INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pi.ID
            WHERE
                ce.CD_EMPRESA = $cdEmpresa ";

    DaoEngine::getInstance()->executeQuery($sql,true);

    $sql = "DELETE FROM col_disciplina_grupo_disciplina WHERE CD_GRUPO_DISCIPLINA NOT IN (SELECT CD_GRUPO_DISCIPLINA FROM col_grupo_disciplina)";
    DaoEngine::getInstance()->executeQuery($sql,true);

    $sql = "INSERT INTO col_disciplina_grupo_disciplina
            (CD_GRUPO_DISCIPLINA, CD_DISCIPLINA, CD_EMPRESA)
            SELECT
                gd.CD_GRUPO_DISCIPLINA, d.CD_DISCIPLINA, ce.CD_EMPRESA
            FROM
                tb_paginasindividuais pa
                INNER JOIN tb_pastasindividuais pi ON pa.PastaID = pi.ID
                INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pi.ID
                INNER JOIN col_disciplina d ON d.ID_PAGINA_INDIVIDUAL = pa.ID
                INNER JOIN col_grupo_disciplina gd ON gd.CD_EMPRESA = ce.CD_EMPRESA AND gd.ID_PASTA = pi.ID
            WHERE
                pa.Status = 1
            AND
                pi.Status = 1
            AND
                ce.CD_EMPRESA = $cdEmpresa";

    DaoEngine::getInstance()->executeQuery($sql,true);

}

function configurarAssessments($cdEmpresa){

    $sql = "DELETE FROM col_assessment WHERE CD_EMPRESA = $cdEmpresa";
    DaoEngine::getInstance()->executeQuery($sql,true);

    $sql = "INSERT INTO col_assessment
            (CD_EMPRESA, CD_PROVA)
            SELECT
                pv.CD_VINCULO, pv.CD_PROVA
            FROM
                col_prova_vinculo pv
                INNER JOIN col_prova p ON p.CD_PROVA = pv.CD_PROVA
            WHERE
                p.IN_PROVA = 1
              AND p.DS_PROVA LIKE 'PA - %'
              AND pv.IN_TIPO_VINCULO = 1
              AND pv.CD_VINCULO = $cdEmpresa";

    DaoEngine::getInstance()->executeQuery($sql,true);

    configurarGruposDisciplinas($cdEmpresa);


}

?>