<?php

function InserirErroImportacao($codigoImportacao, $tipoImportacao, $codigoHierarquiaImportacao, $erroImportacao, $importado = 1) {
	$sql = "INSERT INTO col_erro_importacao (CD_IMPORTACAO, IN_TIPO_IMPORTACAO, CD_HIERARQUIA_IMPORTACAO, DS_ERRO, IN_IMPORTADO)
				VALUES ($codigoImportacao, '$tipoImportacao', $codigoHierarquiaImportacao, '$erroImportacao', $importado)";
	
	DaoEngine::getInstance()->executeQuery($sql, true);
}

class col_hierarquia_importacao extends RnBase
{
	public function __construct($daoObject = null){
		parent::__construct($daoObject);
	}
	
	public $xml;
	public $empresa;
	public $nomeArquivo;
	public $codigoImportacao;
	public $estrutura;
	public $hierarquiaLotacao;
	public $erros;
	
	public function inserir()
	{
		
		//Inserir Importa��o 
		$importacao = new col_importacao();
		$importacao->NM_ARQUIVO = $this->nomeArquivo;
		$importacao = $importacao->inserir();
		$codigoImportacao = $importacao->CD_IMPORTACAO;
		$empresa = $this->empresa;
		
		$dom = $this->xml;
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		
		$linhaAtual = 0;
		
		foreach ($rows as $row) {
			
			$index = 1;
			$cells = $row->getElementsByTagName( 'Cell' );
			
			$identificadorLotacao = null;
			$nomeLotacao = null;
			$identificadorLotacaoPai = null;
			$nomeHierarquia = null;
			
			if ( $linhaAtual > 0 ) { //Se for a primeira linha
			
				foreach( $cells as $cell )
				{
					$ind = $cell->getAttribute( 'Index' );
					if ( $ind != null ) $index = $ind;
					
					switch ($index) {
						case 1:
							$identificadorLotacao = $this->trataSQL($cell->nodeValue);
							break;
						case 2:
							$nomeLotacao = $this->trataSQL($cell->nodeValue);
							break;
						case 3:
							$nomeHierarquia = $this->trataSQL($cell->nodeValue);
							break;
						case 4:
							$identificadorLotacaoPai = $this->trataSQL($cell->nodeValue);
							break;
					}
					
					
					
					$index++;
					
				}
			
			if (trim($identificadorLotacao) == "" || $identificadorLotacao == null) break;
				
			$sql = "INSERT INTO
					col_hierarquia_importacao
						(CD_IMPORTACAO, CD_LOTACAO, NM_LOTACAO, CD_LOTACAO_PAI, NM_HIERARQUIA, CD_EMPRESA)
					VALUES
						($codigoImportacao, '$identificadorLotacao', '$nomeLotacao', '$identificadorLotacaoPai', '$nomeHierarquia', $empresa)";
			
			$this->dao->executeQuery($sql, true);
			
			}
			
			$linhaAtual++;
			
		}
		
		$sql = "SELECT * FROM col_hierarquia_importacao WHERE CD_EMPRESA = $empresa AND CD_IMPORTACAO = $codigoImportacao ORDER BY CD_LOTACAO_PAI";
		
		$resultado = $this->dao->executeQuery($sql, true);
		
		$itensInseridos = array();
		
		$niveis = $this->obterNiveisHierarquicos($empresa);
		$lotacoes = $this->obterLotacoes($empresa);
		
		while ($linha = mysql_fetch_array($resultado)) {
			
			$identificadorLotacao = $linha["CD_LOTACAO"];
			$nomeLotacao = $linha["NM_LOTACAO"];
			$nomeHierarquia = $linha["NM_HIERARQUIA"];
			$codigoHierarquiaImportacao = $linha["CD_HIERARQUIA_IMPORTACAO"];
			$erro = "";
			if ($nomeLotacao == "" ) {
				$erro = $erro . "A coluna nome da lota��o n�o foi preenchida.\n";
			}
			
			if ($erro == "") {
				
				$codigoHierarquia = null;
				if ($nomeHierarquia != "") {
					if ($niveis[strtoupper($nomeHierarquia)] == null) {
						$codigoHierarquia = count($niveis) + 1;
						$sql = "INSERT INTO col_hierarquia (DS_HIERARQUIA, CD_NIVEL_HIERARQUICO, CD_EMPRESA) VALUES ('$nomeHierarquia', $codigoHierarquia, $empresa)";
						$this->dao->executeQuery($sql, true);
						$niveis[strtoupper($nomeHierarquia)] = $codigoHierarquia;
					} else {
						$codigoHierarquia = $niveis[strtoupper($nomeHierarquia)];
					}
						
				}
				
				$nomeLotacao = $this->trataSQL($nomeLotacao);
				
				
				if ($lotacoes[strtoupper($identificadorLotacao)] == null) {
				
					$identificadorLotacao = $this->trataSQL($identificadorLotacao);
					
					$sql = "INSERT INTO col_lotacao (DS_LOTACAO, CD_EMPRESA, IN_ATIVO, CD_NIVEL_HIERARQUICO, CD_IDENTIFICADOR_LOTACAO) 
								VALUES ('$nomeLotacao', $empresa, 1, $codigoHierarquia, '$identificadorLotacao')";
					
					
					$lotacoes[strtoupper($identificadorLotacao)] = $identificadorLotacao;
					
				} else {
					
					$identificadorLotacao = $this->trataSQL($identificadorLotacao);
					
					$sql = "UPDATE col_lotacao SET DS_LOTACAO = '$nomeLotacao', CD_NIVEL_HIERARQUICO = $codigoHierarquia 
								WHERE CD_IDENTIFICADOR_LOTACAO = '$identificadorLotacao' AND CD_EMPRESA = $empresa";
					
				}
				
				$this->dao->executeQuery($sql, true);
				
			} else { //Se ocorreu erro
				InserirErroImportacao($codigoImportacao, 'H', $codigoHierarquiaImportacao, $erro, 0);
			}
			
		}
		
		mysql_data_seek($resultado, 0);
		
		$lotacoes = $this->obterLotacoes($empresa);
		
		while ($linha = mysql_fetch_array($resultado)) {
			
			if ($nomeLotacao == "" ) continue;
			
			$identificadorLotacaoPai = $linha["CD_LOTACAO_PAI"];
			$identificadorLotacao = $linha["CD_LOTACAO"];
			
			if ($identificadorLotacaoPai == "") continue;
			
			if ($lotacoes[strtoupper($identificadorLotacaoPai)] == null) {
				$erro = "O identificador da unidade pai n�o foi encontrado";
				InserirErroImportacao($codigoImportacao, 'H', $codigoHierarquiaImportacao,$erro, 1);
				continue;
			}
			
			$sql = "UPDATE col_lotacao SET CD_LOTACAO_PAI = {$lotacoes[strtoupper($identificadorLotacaoPai)]["CD_LOTACAO"]}
						WHERE CD_IDENTIFICADOR_LOTACAO = '$identificadorLotacao' AND CD_EMPRESA = $empresa";
			
			$this->dao->executeQuery($sql, true);
			
		}
		
		
		$lotacoes = $this->obterLotacoes($empresa, true);
		
		foreach ($lotacoes as $lotacao) {
			
			$descricaoHierarquia = $this->obterDescricaoHierarquia($lotacoes, $lotacao);
			$sql = "UPDATE col_lotacao SET DS_LOTACAO_HIERARQUIA = '$descricaoHierarquia'
						WHERE CD_LOTACAO = '{$lotacao["CD_LOTACAO"]}' AND CD_EMPRESA = $empresa";
			
			$this->dao->executeQuery($sql, true);
		}
		
		
		//Obter erros duplicados
		$sql = "SELECT CD_HIERARQUIA_IMPORTACAO FROM col_hierarquia_importacao 
					WHERE 	CD_EMPRESA = $empresa AND CD_IMPORTACAO = $codigoImportacao AND
							CD_LOTACAO IN (
								SELECT CD_LOTACAO FROM col_hierarquia_importacao 
									WHERE CD_EMPRESA = $empresa AND CD_IMPORTACAO = $codigoImportacao GROUP BY CD_LOTACAO HAVING COUNT(1) > 1
					)";
		$resultado = $this->dao->executeQuery($sql, true);
		
		while ($linha = mysql_fetch_row($resultado)) {
			$codigoHierarquiaImportacao = $linha[0];
			$erro = "Registro com c�digos repetidos. Apenas uma das linhas foi importada";
			InserirErroImportacao($codigoImportacao, 'H', $codigoHierarquiaImportacao,$erro, 1);
		}
		
		$this->erros = $erro;
		$this->codigoImportacao = $codigoImportacao;
		return $this;
	}
	
	public function obterRelatorioErrosImportacao($codigoImportacao) {
		
		$sql = "SELECT
					e.DS_ERRO, h.CD_LOTACAO, h.NM_LOTACAO, h.CD_LOTACAO_PAI, h.NM_HIERARQUIA, i.DT_IMPORTACAO, p.DS_EMPRESA, IN_IMPORTADO,
					(SELECT COUNT(1) FROM col_hierarquia_importacao WHERE CD_IMPORTACAO = $codigoImportacao) AS QT_IMPORTADA
				FROM
					col_erro_importacao e
					INNER JOIN col_hierarquia_importacao h ON h.CD_HIERARQUIA_IMPORTACAO = e.CD_HIERARQUIA_IMPORTACAO
					INNER JOIN col_importacao i ON i.CD_IMPORTACAO = h.CD_IMPORTACAO
					INNER JOIN col_empresa p ON p.CD_EMPRESA = h.CD_EMPRESA
				WHERE e.CD_IMPORTACAO = $codigoImportacao";
		
		$resultado = $this->dao->executeQuery($sql, true);
		
		return $resultado;
		
	}
	
	private function obterDescricaoHierarquia(&$lotacoes, &$lotacao)
	{
		
		$lotacaoHierarquia = "";
		$codigoLotacaoPai = $lotacao["CD_LOTACAO_PAI"];
		if ($codigoLotacaoPai != null && $codigoLotacaoPai != "" && $lotacoes[$codigoLotacaoPai] != null && $codigoLotacaoPai != $lotacao["CD_LOTACAO"])
		{
			$lotacaoHierarquiaPai = $this->obterDescricaoHierarquia($lotacoes, $lotacoes[$codigoLotacaoPai]);
			$lotacaoHierarquia = "{$lotacaoHierarquia}{$lotacaoHierarquiaPai}.";
		}
	
		$lotacaoHierarquia = "$lotacaoHierarquia{$lotacao["CD_LOTACAO"]}";
		
		return $lotacaoHierarquia;
		
	}
	
	private function obterNiveisHierarquicos($empresa)
	{
		$sql = "SELECT CD_NIVEL_HIERARQUICO, DS_HIERARQUIA FROM col_hierarquia WHERE CD_EMPRESA = $empresa";
		$resultado = $this->dao->executeQuery($sql,true);
		
		$niveis = array();
		
		while ($linha = mysql_fetch_row($resultado))
		{
			
			$nivelHierarquico = $linha[0];
			$nomeHirarquia = strtoupper($linha[1]);
			
			$niveis[$nomeHirarquia] = $nivelHierarquico;
			
		}
		
		return $niveis;
		
	}
	
	private function obterLotacoes($empresa, $usarChaveComoid = false)
	{
		$sql = "SELECT DS_LOTACAO, CD_LOTACAO, CD_NIVEL_HIERARQUICO, CD_LOTACAO_PAI, CD_IDENTIFICADOR_LOTACAO FROM col_lotacao WHERE CD_EMPRESA = $empresa";
		$resultado = $this->dao->executeQuery($sql,true);
		
		$lotacoes = array();
		
		$chave = "CD_IDENTIFICADOR_LOTACAO";
		
		if ($usarChaveComoid) {
			$chave = "CD_LOTACAO";
		}
		
		while ($linha = mysql_fetch_array($resultado))
		{

			$identificadorLotacao = strtoupper($linha[$chave]);
			$lotacoes[$identificadorLotacao] = $linha;
			
		}
		
		return $lotacoes;
	}
	
	private function trataSQL($valor)
	{
		$valor = utf8_decode($valor);
		$valor = str_replace("'", "\\'", $valor);
		$valor = trim($valor);
		
		return $valor;
		
	}
	
}

class ResumoImportacao {
	
	public $quantidadeLinhasProcessadas = 0;
	public $quantidadeLinhasNaoProcessadas = 0;
	public $quantidadeLotacoesProcessadas = 0;
	public $quantidadeFiltrosProcessados = 0;
	public $quantidadeCargosProcessados = 0;
	public $quantidadeSuperUsuariosPrograma = 0;
	public $quantidadeSuperUsuariosGrupo = 0;
	public $quantidadeParticipantes = 0;

	public $erros;
	
}

?>