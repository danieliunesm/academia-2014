<?php

include('page.php');
include("../include/defines.php");
include('framework/crud.php');
include('relatorio_util.php');

//$paginaRn = new col_pagina_acesso();
//$paginaRn->prepararRelatorios($_GET['hdnPaginasSelecionadas']);

$acessoUnico = $_POST["hdnTipoRelatorio"];

$codigoEmpresa = $_POST['cboEmpresa'];

$codigoUsuario = joinArray($_POST['cboUsuario']);
$cargo = joinArray($_POST['cboCargo'],"','");
$codigoLotacao = joinArray($_POST['cboLotacao']);

$dataInicio = $_POST["txtDe"];
$dataTermino = $_POST["txtAte"];
$dataInicioTexto = $dataInicio;
$dataTerminoTexto = $dataTermino;

$nomeEmpresa = 'Todas';
$nomeUsuario = 'Todos';
$lotacao = "";

$sqlData = "";

if ($dataInicio != ""){
	$dataInicio = substr($dataInicio, 6, 4) . substr($dataInicio, 3, 2) . substr($dataInicio, 0, 2);
	
	$sqlData = " $sqlData AND  DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicio' ";
}

if ($dataTermino != ""){
	$dataTermino = substr($dataTermino, 6, 4) . substr($dataTermino, 3, 2) . substr($dataTermino, 0, 2);
		
	$sqlData = " $sqlData AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataTermino' ";
}


$paginaRn = new col_pagina_acesso();
$paginaRn->prepararRelatorios($dataInicio, $dataTermino, $codigoEmpresa);

$tipoAcessoContado = "";
if ($acessoUnico == "H"){
	$tipoAcessoContado = "COUNT(1)";
}
elseif ($acessoUnico == "D")
{
	//$tipoAcessoContado = "COUNT(DISTINCT(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d')))";
	$tipoAcessoContado = "1";
}
elseif ($acessoUnico == "S")
{
	$tipoAcessoContado = "COUNT(DISTINCT(DS_ID_SESSAO))";
}

$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];

$sql = "
SELECT
  u.login, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO, l.CD_LOTACAO,
  pa.CD_PAGINA_ACESSO, pa.DS_PAGINA_ACESSO,
  $tipoAcessoContado AS QD_ACESSO
FROM
  col_pagina_acesso pa
  LEFT JOIN col_acesso a ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
  LEFT JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
  LEFT JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
  LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
WHERE
	NOT pa.IN_ORDENAR = -1 AND NOT pa.IN_ORDENAR = 999
AND	u.empresa = $codigoEmpresa
AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
	$sqlData
GROUP BY
  u.login, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO, pa.DS_PAGINA_ACESSO
ORDER BY
	pa.IN_ORDENAR, pa.DS_PAGINA_ACESSO, l.DS_LOTACAO, l.CD_LOTACAO";


$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

if (!mysql_num_rows($resultado) > 0){
	echo '<script>alert("N�o h� dados para exibi��o do relat�rio.");self.close();</script>';
	exit();
}

if ($acessoUnico == "D")
{
	$tipoAcessoContado = "COUNT(DISTINCT(u.CD_USUARIO))";
}

$sql = "
SELECT
  $tipoAcessoContado AS QD_ACESSO
FROM
  col_acesso a
  LEFT JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
  LEFT JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
  LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
WHERE
	u.empresa = $codigoEmpresa
AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
	$sqlData";

$resultadoAcessosEmpresa = DaoEngine::getInstance()->executeQuery($sql,true);
$linha = mysql_fetch_row($resultadoAcessosEmpresa);
$totalEmpresa = $linha[0];

$acessosFuncionalidades = array();
$sql = "
SELECT
  CD_PAGINA_ACESSO,
  $tipoAcessoContado AS QD_ACESSO
FROM
  col_acesso a
  LEFT JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
  LEFT JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
  LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
WHERE
	u.empresa = $codigoEmpresa
AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
	$sqlData
GROUP BY
	  CD_PAGINA_ACESSO";

$resultadoFuncionalidades = DaoEngine::getInstance()->executeQuery($sql,true);
while ($linha = mysql_fetch_row($resultadoFuncionalidades)) {
		//echo "aaa";
	$acessosFuncionalidades[$linha[0]] = $linha[1];
}


if ($codigoUsuario > -1){
	//filtrar usuario
	$nomeUsuario = $_GET['nomeUsuario'];
}

if ($codigoLotacao > -1){
	
	$lotacao = $_GET['nomeLotacao'];
}

if ($_POST["btnExcel"] != "") {
	header("Content-Type: application/vnd.ms-excel; name='excel'");
	header("Content-disposition:  attachment; filename=RelatorioAcessos.xls");
}


?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Relat�rio de Acessos<?php if ($acessoUnico == "D") echo ' - Quantidade de Acessos de Usu�rios'; elseif ($acessoUnico == "S") echo ' - Quantidade de Sess�es Abertas por Usu�rios'; elseif ($acessoUnico == "H") echo ' - Quantidade de Hits de todas as Sess�es e Usu�rios'; ?> - <?php echo date("d/m/Y") ?></title>
	
	<?php
	
	if ($_POST["btnExcel"] == "") {

	?>
	<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">		
	<?php
	}
	?>
	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			
			.textoInformativo{
				display: none;
			}
			
			.sumirImpressao{
				display:none;
			}
			
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
		
		.impressao{
			margin-top: -23px;
		}
		
		.sumirImpressao{
			height: 30px;
		}

	</style>
	
</head>

<body>
	<table cellpadding="2" cellspacing="0" border="0" width="95%" align="center" class="impressao">
		<tr style="height: 1px; font: 1px">
			<td class="textblk" width="50%" colspan="2">&nbsp;</td>
			<td width="50%" align="left" class="textblk" colspan="2">
				&nbsp;
			</td>
			<td class="textblk" style="width: 1%">
				&nbsp;
			</td>
		</tr>
		<tr class="sumirImpressao">
			<td class="textblk" width="50%" colspan="2"><b>Colaborae Consultoria e Educa��o Corporativa</b></td>
			<td width="50%" align="left" class="textblk" colspan="2">
				<b><?php if ($acessoUnico == "D") echo 'Quantidade de Acessos de Usu�rios'; elseif ($acessoUnico == "S") echo 'Quantidade de Sess�es Abertas por Usu�rios';  elseif ($acessoUnico == "H") echo 'Quantidade de Hits de todas as Sess�es e Usu�rios'; ?></b>
			</td>
			<td class="textblk" style="width: 1%; text-align: right;">
				<?php echo date("d/m/Y") ?>
			</td>
		</tr>
		
<?php

$qtdAcessoEmpresa = 0;
$lotacaoAnterior = 0;
$qtdAcessoLotacao = 0;
$qtdAcessoSecao = 0;

$secoes = array();
$lotacoes = array();

while ($linha = mysql_fetch_array($resultado)) {
	
	$qtdAcessoEmpresa = $qtdAcessoEmpresa + $linha["QD_ACESSO"];
	
	//if ($lotacaoAnterior =! $linha["CD_LOTACAO"] && $lotacaoAnterior > 0)
	//{
	//	$lotacoes[] = $qtdAcessoLotacao;
	//	$qtdAcessoLotacao = 0;
	//}
	
	//$lotacaoAnterior = $linha["CD_LOTACAO"];
	
	$indiceCodigoLinha = "{$linha["CD_PAGINA_ACESSO"]}{$linha["CD_LOTACAO"]}";
	$indiceSecao = $linha["CD_PAGINA_ACESSO"];
	
	if ($lotacoes[$indiceCodigoLinha] == null){
		$lotacoes[$indiceCodigoLinha] = $linha["QD_ACESSO"];
	}
	else
	{
		$lotacoes[$indiceCodigoLinha] = $lotacoes[$indiceCodigoLinha] + $linha["QD_ACESSO"];
	}
	
	if ($secoes[$indiceSecao] == null) {
		$secoes[$indiceSecao] = $linha["QD_ACESSO"];
	}
	else
	{
		$secoes[$indiceSecao] = $secoes[$indiceSecao] + $linha["QD_ACESSO"];
	}
	
	
	
}

//$lotacoes[] = $qtdAcessoLotacao;
//$qtdAcessoLotacao = 0;
mysql_data_seek($resultado, 0);

?>


			<?php //<tr class="textblk">
					//<td width="100%" colspan="5"> ?>
			<tr class='textblk' valign='top'><td align='left' style='width:65%' colspan='5'>
											Per�odo:&nbsp; <?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
			echo '</td>';
			
			echo "</tr>";
			echo "<tr class='textblk' valign='top'>";
			echo "<td align='left'>Total de Cadastrados: ";
			
			if ($_POST["btnExcel"] == "")
			{
				echo obterTotalUsuarios($codigoEmpresa);
			}
			else{
				echo "</td><td align='center'>";
				echo obterTotalUsuarios($codigoEmpresa);
			}
			
			echo "</td>";
			
			
			echo "</tr>";
			
			
			echo "<tr class='textblk' style='height: 25px' valign='top'>";
			
			
			if ($_POST["btnExcel"] == "")
			{
				echo "<td align='left' style='width:65%' colspan='2'>Total do Grupamento: ";
				echo obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);
			}
			else 
			{
				echo "<td align='left' style='width:65%'>Total do Grupamento: </td><td align='center'>";
				echo obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);
				
			}
			
			
			echo "</td>";
			//echo "</td></tr>";
			//echo "<tr class='textblk' style='height: 25px' valign='top'><td align='left' style='width:65%' colspan='2'>&nbsp;</td>";
			echo "<td style='width: 35%' colspan='2'>Login/Nome Participante</td><td align='right; width:1%'>&nbsp;&nbsp;QT&nbsp;de&nbsp;Acessos:</td></tr>";
			echo "<tr class='textblk'><td colspan='4'  style='border-top: 1px solid black'><b>$nomeEmpresa</b></td>";
			echo "<td align='right' class='textblk' style='border-top: 1px solid black'>$totalEmpresa";
										?>

			</td>
		</tr>

<?php

	$codigoPagina = 0;
	$codigoLotacao = 0;
	
	while ($linha = mysql_fetch_array($resultado)) {
		
		if ($codigoPagina != $linha["CD_PAGINA_ACESSO"])
		{
			if ($codigoPagina > 0)
			{
				echo "<tr><td colspan='5'>&nbsp;</td></tr>";
			}
			
			$codigoPagina =  $linha["CD_PAGINA_ACESSO"];
			$codigoLotacao = 0;
			
			echo "<tr  class='textblk'><td colspan='4' style='border-bottom: 1px solid #000000'><b>Se��o: {$linha["DS_PAGINA_ACESSO"]}</b></td>
					<td align='right' style='border-bottom: 1px solid #000000'>{$acessosFuncionalidades[$linha["CD_PAGINA_ACESSO"]]}</td></tr>";	
			
		}
		
		$indiceCodigoLinha = "{$linha["CD_PAGINA_ACESSO"]}{$linha["CD_LOTACAO"]}";
		
		if ($codigoLotacao != $linha["CD_LOTACAO"]){
			echo "<tr height='40' class='textblk'>
					<td colspan='4' class='textblk'><b>Lota��o: {$linha["DS_LOTACAO"]}</b></td>
						<td align='right' width='50%' >{$lotacoes[$indiceCodigoLinha]}</td>
					</tr>
				";
			
			$codigoLotacao = $linha["CD_LOTACAO"];
			
		}
		
		$qdAcesso = "&nbsp;";
		
		if ($acessoUnico != "D")
		{
			$qdAcesso = $linha["QD_ACESSO"];
		}
		
		echo "<tr class='textblk'>
				<td colspan='2'>&nbsp;</td>
				<td colspan='2'>{$linha["login"]} / {$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}</td>
				<td align='right'>$qdAcesso</td>
				</tr>";
		
		
		
	}



?>
	<tr><td colspan="5" style="border-bottom: 1px solid #000000">&nbsp;</td></tr>

</table>

	<?php
		if ($_GET["excel"] != 1) {
	?>

	<p align="center">
		<br />
		<form method="POST">
		
			<?php
				escreveHiddensPost();
			?>
		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">
		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
		<p class="textoInformativo" align="center">Favor definir o formato paisagem para fins de impress�o</p>
		</form>	
	</p>
	<?php
		}
	?>
	
</body>
</html>