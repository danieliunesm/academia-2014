<?php
include('page.php');

page::$isPostBack = count($_POST) > 0 ? true : false;

if (page::$isPostBack){
	
	if (isset($_POST["btnAtualizarStatus"])){
		alterarStatus();
	}
}

function alterarStatus(){
	
	$ativos = "";
	$inativos = "";
	
	foreach ($_POST as $item => $valor){
		
        if (strlen($item) < 4){
            continue;
        }
        
        if (substr($item,0,3) != "rdo"){
            continue;
        }
        
		if ($valor == "1"){
			if ($ativos != ""){
				$ativos = "$ativos, ";
			}
			$ativos = "$ativos " . substr($item,3,strlen($item));
			
		}elseif ($valor == "0"){
			if ($inativos != ""){
				$inativos = "$inativos, ";
			}
			$inativos = "$inativos " . substr($item,3,strlen($item));
		}
		
		
	}

	$rn = instanciarRN();
	
	$rn->alterarStatus($ativos, $inativos);
	
}

function dataGridColaborae($tituloColunas, $campoColunas, $tamanhoColunas, $resultado, $chaves, $numerada=false, $removerInativos = true, $checks=false)
{
	
//	$tituloColunas = split("|", $tituloColuna);
//	$campoColunas = split("|", $campoColuna);
//	$tamanhoColunas = split("|", $tamanhoColuna);
	
	$qtdColunas = count($campoColunas);
	
	echo '						<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
									<TR class="tarjaItens">';
	
	if ($numerada){
		echo "<TD class='title' style='PADDING-LEFT: 40px' align='left' rowspan='2' width='30'>&nbsp;</TD>";
	}
	
	for($i=0; $i < $qtdColunas; $i++){
		
		if($i==0){

			if ($numerada){
				echo "								<TD class='title' align='left' rowspan='2' width='$tamanhoColunas[$i]'>$tituloColunas[$i]</TD>";
			}else {
				echo "								<TD class='title' style='PADDING-LEFT: 40px' align='left' rowspan='2' width='$tamanhoColunas[$i]'>$tituloColunas[$i]</TD>";	
			}
			
		}else{
			echo "								<TD class='title' align='center' rowspan='2' width='$tamanhoColunas[$i]'>$tituloColunas[$i]</TD>";
		}
	}
		
	$colspan = $qtdColunas + 3;
	
	if ($numerada){
		$colspan++;
	}
	
	echo "								<TD class='title' align='middle' colSpan='3' width='5%'>STATUS</TD>
									</TR>
									<TR class='tarjaItens'>
										<TD align='middle'><IMG height='32' alt='   publicar   ' hspace='5' src='images/bt_publicar.gif'
												width='32'></TD>
										<TD align='middle'><IMG height='32' alt='   remover   ' hspace='5' src='images/bt_remover.gif' width='32'></TD>
										<TD align='middle'><IMG height='32' alt='   editar   ' hspace='5' src='images/bt_editar.gif' width='32'></TD>
									</TR>
									<TR class='tarjaItens'>
										<TD colspan='$colspan'><IMG height='7' src='images/blank.gif' width='1'></TD>
									</TR>";
		
	$corLinha = "#ffffff";
	$numeroLinha = 0;
	
	while ($linha = mysql_fetch_array($resultado)){
		
		if ($removerInativos){
	        if (isset($_POST["hdnRemovidos"]) && $_POST["hdnRemovidos"] != "1"){
	            if ($linha["IN_ATIVO"] != 1){
	                continue;
	            }
	        }
		}
        
        $numeroLinha++;
        
		echo "							<TR bgcolor='$corLinha'>";
		
		if ($numerada){
			echo 						"<TD class='textblk' style='PADDING-LEFT: 40px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px'>$numeroLinha&nbsp;&nbsp;</TD>";
		}
		
		for($i=0; $i < $qtdColunas; $i++){
			$checkLabel = "";
			$campo = $campoColunas[$i];
			
			if ($i == 0){

                if($checks)
                    $checkLabel = "<input type='checkbox' name='chkItem{$linha[$chaves]}' value='{$linha[$chaves]}' />";

				if ($numerada){
					echo 						"<TD class='textblk'>$linha[$campo]</TD>";	
				}else{
					echo 						"<TD class='textblk' style='PADDING-LEFT: 40px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px'>$checkLabel $linha[$campo]</TD>";
				}
				
			}else{
				echo 						"<TD class='textblk' align='center'>$linha[$campo]</TD>";
			}
			
		}
		
		$ativo = "";
		$inativo = "";
		
		if ($linha["IN_ATIVO"] == 1){
			$ativo = "checked";
		} else {
			$inativo = "checked";
		}

		echo "						<TD align='middle'><INPUT type='radio' value='1' name='rdo{$linha[$chaves]}' $ativo/></TD>
									<TD align='middle'><INPUT type='radio' value='0' name='rdo{$linha[$chaves]}' $inativo/></TD>";
									
		echo "							<TD align='middle'><A onfocus=noFocus(this) 
										onclick=\"getURL('$linha[$chaves]');return false;\" 
										href='#'><IMG 
										height=20 src='images/bt_edit.gif' width=20 
										border=0/></A></TD>
								</TR>";
		
			
		if ($corLinha == "#ffffff"){
			$corLinha = "#f1f1f1";
		}else{
			$corLinha = "#ffffff";
		}
		
		
	}
		
	echo "				</TABLE>";
	
}

	



?>