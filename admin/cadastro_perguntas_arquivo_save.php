<?php
//include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";

error_reporting(E_ALL ^ E_NOTICE);

$identificador = isset($_POST['id']) ? $_POST['id'] : '';
$nu_respostas  = isset($_POST['nr']) ? $_POST['nr'] : '';
$file		   = isset($_POST['f'])  ? $_POST['f']  : '';

if($identificador == '' || $nu_respostas == '' || $file == '') die(utf8_encode('� preciso entrar com o Arquivo, identificador das Se��es e respostas/quest�o!'));

$file = '../dbquestions/' . $file;

$titulo		= '';
$secao		= array();
$questao	= array();

function odt2text($filename) {
    return readZippedXML($filename, "content.xml");
}

function docx2text($filename) {
    return readZippedXML($filename, "word/document.xml");
}

function readZippedXML($archiveFile, $dataFile) {
	$zip = new ZipArchive();
	
	if(true === $zip->open($archiveFile)) {
		if(($index = $zip->locateName($dataFile)) !== false){
			$data = $zip->getFromIndex($index);
			$zip->close();
			$xml = DOMDocument::loadXML($data, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
			
			$content = $xml->saveXML();
			
//			return $content;
			
			$content = str_replace('“', '"', $content);
			$content = str_replace('”', '"', $content);
			$content = str_replace('–', " - ", $content);
			
			$content = str_replace(' xml:space="preserve"', '', $content);
			$content = str_replace('<w:lastRenderedPageBreak/>', '', $content);
			
			$content = str_replace('</w:t></w:r><w:proofErr w:type="gramEnd"/></w:p></w:tc></w:tr>', "\r\n", $content);
			
			$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);//</w:t></w:r></w:p></w:tc><w:tc><w:tcPr>
			$content = str_replace('</w:r></w:p>', "\r\n", $content);//</w:t></w:r></w:p></w:tc></w:tr>
			
			$striped_content = nl2br(strip_tags($content));
			
			return explode('<br />', trim(utf8_decode($striped_content)));
//			return utf8_decode($striped_content);
		}
		$zip->close();
	}
	return "";
}

$s = docx2text($file);

//var_dump($s);
//echo $s;die();

$tituloflag = false;
$k = -1;

for($i = 0; $i < count($s); $i++) {
	if($s[$i] == '') continue;
	if(!$tituloflag){
		$titulo = $s[$i];
		$tituloflag = true;
	}
	if(!(strpos($s[$i], $identificador) === false)) {
		$k++;
		$secao[$k]				= array();
		$secao[$k]['titulo']	= $s[$i];
		$secao[$k]['questao']	= array();
		$it = -1;
		$q  = -1;
		continue;
	}
	if($k >= 0) {
		++$it;
		$item = $it % ($nu_respostas + 2);
		switch($item) {
			case 0:
				$q++;
				$secao[$k]['questao'][$q] = array();
				$secao[$k]['questao'][$q]['enunciado'] = $s[$i];
				$secao[$k]['questao'][$q]['resposta'] = array();
				$r = 0;
			break;
			
			case 1:
				$secao[$k]['questao'][$q]['pergunta'] = (strpos($s[$i], 'XXX') === false) ? $s[$i] : '';
			break;
			
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				$secao[$k]['questao'][$q]['resposta'][$r] = array();
				$secao[$k]['questao'][$q]['resposta'][$r]['certa'] = 0;										// 1
				$secao[$k]['questao'][$q]['resposta'][$r]['texto'] = $s[$i];
				if(!(strpos($s[$i], 'C:') === false)){
					$secao[$k]['questao'][$q]['resposta'][$r]['certa'] = 2;
					$secao[$k]['questao'][$q]['resposta'][$r]['texto'] = str_replace('C: ', '', $s[$i]);
				}
				if(!(strpos($s[$i], 'E:') === false)){
					$secao[$k]['questao'][$q]['resposta'][$r]['certa'] = 1;									// 0
					$secao[$k]['questao'][$q]['resposta'][$r]['texto'] = str_replace('E: ', '', $s[$i]);
				}
				$r++;
			break;
		}
	}
}


for($i = 0; $i < count($secao); $i++) {
	if(trim($secao[$i]['titulo']) == '') continue;
	
	$sql1 = "INSERT INTO col_disciplina (DS_DISCIPLINA) VALUES ('" . trim($secao[$i]['titulo']) . "')";
	
	if(!($query1 = mysql_query($sql1))){
		die(mysql_error());
	}
	else{
		$sql2 = "SELECT MAX(CD_DISCIPLINA) AS disciplina FROM col_disciplina WHERE DS_DISCIPLINA = '" . trim($secao[$i]['titulo']) . "'";
		
//		echo '<br />' . $sql2 . '<br />';
		
		$query2 = mysql_query($sql2) or die(mysql_error());
		$row2   = mysql_fetch_assoc($query2);
		if($row2){
			$questoes = $secao[$i]['questao'];
			
			for($j = 0; $j < count($questoes); $j++) {
				if(trim($questoes[$j]['enunciado']) == '') continue;
				
				$sql3  = "INSERT INTO col_perguntas (CD_DISCIPLINA, DS_PERGUNTAS, IN_PESO, IN_ATIVO) VALUES (";
				$sql3 .= $row2['disciplina'] . ", ";
				$sql3 .= "'" . trim($questoes[$j]['enunciado']) . ' ' . trim($questoes[$j]['pergunta']) . "', ";
				$sql3 .= "1, ";
				$sql3 .= "0";
				$sql3 .= ")";
				
//				echo $sql3 . '<br />';
				
				if(!($query3 = mysql_query($sql3))){
					die(mysql_error());
				}
				else{
					$sql4  = "SELECT MAX(CD_PERGUNTAS) AS pergunta FROM col_perguntas WHERE DS_PERGUNTAS = '" . trim($questoes[$j]['enunciado']) . ' ' . trim($questoes[$j]['pergunta']) . "'";
					$query4 = mysql_query($sql4) or die(mysql_error());
					$row4   = mysql_fetch_assoc($query4);
					if($row4){
						$respostas = $questoes[$j]['resposta'];
						
						for($k = 0; $k < count($respostas); $k++) {
							$sql5  = "INSERT INTO col_respostas (CD_PERGUNTAS, CD_RESPOSTAS, DS_RESPOSTAS, IN_CERTA, IN_SWAP) VALUES (";
							$sql5 .= $row4['pergunta'] . ", ";
							$sql5 .= ($k + 1) . ", ";
							$sql5 .= "'" . trim($respostas[$k]['texto']) . "', ";
							$sql5 .= ($respostas[$k]['certa'] > 0 ? 1 : 0) . ", ";
							$sql5 .= ($respostas[$k]['certa'] > 1 ? 2 : 0);
							$sql5 .= ")";
							
//							echo "&nbsp;&nbsp;&nbsp;&nbsp; $sql5 <br />";
							
							if(!($query5 = mysql_query($sql5))){
								die(mysql_error());
							}
						}
//						echo "<br />";
					}
				}
			}
		}
	}
}

echo utf8_encode('Arquivo gravado com sucesso no banco de perguntas!     ');

mysql_close();
?>