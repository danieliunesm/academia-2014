<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";

function formataParametroData($valor){
		
	if (strlen($valor)==10){
		return substr($valor, 6, 4) . "-" . substr($valor, 3, 2) . "-" . substr($valor, 0, 2);
	}
	return $valor;
		
}

$id		  	= $_POST["id"];
$titulo 	= $_POST["titulo"];
$pastaid	= $_POST["pastaid"];
$mediador	= $_POST["mediador"];
$conteudo 	= $_POST["htmleditor"];
$email 		= $_POST["email"];
$vigenciaInicio = $_POST["txtDataInicioVigencia"];
$vigenciaTermino= $_POST["txtDataTerminoVigencia"];

$vigenciaInicio = formataParametroData($vigenciaInicio);
$vigenciaTermino = formataParametroData($vigenciaTermino);

function updateForum($oId,$oTitulo,$oPastaId,$oMediador,$oConteudo,$oEmail)
{
//	global $msg;
	global $vigenciaInicio, $vigenciaTermino;
	
	if($oId!="")
	{
		$sql = "UPDATE col_foruns SET ";
		$sql = $sql . "DS_FORUM = '" . $oTitulo . "', ";
		$sql = $sql . "CD_EMPRESA = '" . $oPastaId . "', ";
		$sql = $sql . "CD_USUARIO_MEDIADOR = '" . $oMediador . "', ";
		$sql = $sql . "TEXTAREA = '" . $oConteudo . "', ";
		$sql = $sql . "DT_INICIO_VIGENCIA = '" . $vigenciaInicio . "', ";
		$sql = $sql . "DT_TERMINO_VIGENCIA = '" . $vigenciaTermino . "', ";
		$sql = $sql . "EMAIL = '" . $oEmail . "' ";
		$sql = $sql . "WHERE CD_FORUM = " . $oId;
	}
	else
	{
		$sql = "INSERT INTO col_foruns (DS_FORUM,IN_ATIVO,CD_EMPRESA,CD_USUARIO_MEDIADOR,TEXTAREA,EMAIL,DT_INICIO_VIGENCIA, DT_TERMINO_VIGENCIA) VALUES (";
		$sql = $sql . "'" . $oTitulo . "'";
		$sql = $sql . ",'" . "1" . "'";
		$sql = $sql . ",'" . $oPastaId . "'";
		$sql = $sql . ",'" . $oMediador . "'";
		$sql = $sql . ",'" . $oConteudo . "'";
		$sql = $sql . ",'" . $oEmail . "'";
		$sql = $sql . ",'" . $vigenciaInicio . "'";
		$sql = $sql . ",'" . $vigenciaTermino . "'";
		$sql = $sql . ")";
	}
	if($RS_query = mysql_query($sql)) $_SESSION["msg"] = "Banco atualizado com sucesso!     ";
	else $_SESSION["msg"] = ERROR_MSG_SQLQUERY . mysql_error();
}

updateForum($id,$titulo,$pastaid,$mediador,$conteudo,$email);

mysql_close();
?>
<head>
<script language="JavaScript">
if(self.opener&&!self.opener.closed)self.opener.location.reload();
else alert('<? echo $_SESSION["msg"]; ?>');
<?
if($_SESSION["msg"] == "Banco atualizado com sucesso!     ") echo "self.close();";
else echo "window.history.back();";
?>
</script>
</head>