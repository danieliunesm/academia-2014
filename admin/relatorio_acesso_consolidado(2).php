<?php

include('page.php');
include("../include/defines.php");
include('framework/crud.php');
include('relatorio_util.php');

$acessoUnico = $_POST["hdnTipoRelatorio"];

$codigoEmpresa = $_POST['cboEmpresa'];

$dataInicial = $_POST["txtDe"];
$dataFinal = $_POST["txtAte"];

$codigoUsuario = joinArray($_POST['cboUsuario']);
$cargo = joinArray($_POST['cboCargo'],"','");
$codigoLotacao = joinArray($_POST['cboLotacao']);

$nomeUsuario = '';
$lotacao = '';

$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];

$dataAtual = $dataInicial;

$paginaRn = new col_pagina_acesso();
$paginaRn->prepararRelatorios($dataInicial, $dataFinal);


$sql = "SELECT CD_PAGINA_ACESSO, DS_PAGINA_ACESSO FROM col_pagina_acesso WHERE NOT IN_ORDENAR = -1 AND NOT IN_ORDENAR = 999 ORDER BY IN_ORDENAR";
$colunas = DaoEngine::getInstance()->executeQuery($sql,true);
$qtdColunas = mysql_num_rows($colunas);
$codigosPaginas = array();


if ($_GET["excel"] == 1) {
	header("Content-Type: application/vnd.ms-excel; name='excel'");
	header("Content-disposition:  attachment; filename=RelatorioConsolidadoAcesso.xls");
}

$tipoAcessoContado = "";
if ($acessoUnico == "H"){
	$tipoAcessoContado = "COUNT(1)";
}
elseif ($acessoUnico == "D")
{
	$tipoAcessoContado = "COUNT(DISTINCT(a.CD_USUARIO))";
}
elseif ($acessoUnico == "S")
{
	$tipoAcessoContado = "COUNT(DISTINCT(DS_ID_SESSAO))";
}


if ($codigoUsuario > -1){
	//filtrar usuario
	$nomeUsuario = $_GET['nomeUsuario'];
}

if ($codigoLotacao > -1){
	
	$lotacao = $_GET['nomeLotacao'];
}

ob_start();
	
?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	
	<?php
	
	if ($_GET["excel"] != 1) {

	?>
	<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">		
	<?php
	}
	?>
	
	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
	</style>
	
</head>

<body>

<table cellpadding="0" cellspacing="0" border="0" width="95%" align="center">
	<tr style="height: 30px">
			<td class='textblk' colspan="3">
				<p><b><?php echo $nomeEmpresa; ?></b></p>
			</td>
			<td align="center" class="textblk" colspan="<?php echo $qtdColunas;?>">
				<b>Gest�o do Programa de Capacita��o em Tecnologia para Vendas <br>
					Relat�rio Consolidado de Acessos<?php if ($acessoUnico == "D") echo ' �nicos - Acessos Di�rios'; elseif ($acessoUnico == "S") echo ' �nicos por Sess�o';  elseif ($acessoUnico == "H") echo ' - Todos os Hits'; ?>
				</b>
			</td>
			<td style="text-align: right" class='textblk'>
				<p><?php echo date("d/m/Y") ?></p>
			</td>
	</tr>
	<tr class="textblk">
		<td width="100%" colspan="<?php echo $qtdColunas + 4; ?>">
			Per�odo:&nbsp; <?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
		</td>
	</tr>
	
	<?php
		if ($lotacao != "" || $cargo != "-1" || $codigoUsuario != -1)
		{
			
			$colSpan = $qtdColunas + 4;
			
			echo "<tr>";
			
			echo "<td class='textblk' colspan='$colSpan'>";
			
			if ($lotacao != "")
			{
				echo "Lota��o: $lotacao";
			}
			
			if ($lotacao != "" && $cargo != "")
			{
				echo "</td><tr><td colspan='$colSpan' class='textblk'>";
			}

			if ($cargo != "-1")
			{
				echo "Cargo/Fun��o: $cargo";
			}
				
			echo "</td>";
			
			echo "</tr>";
			
			if ($codigoUsuario != -1) {
				echo "<td class='textblk' colspan='$colSpan'>";
				echo "Nome: $nomeUsuario";
				echo "</td></tr>"; 
			}
			
		}
		
		
		
	
	?>
	
	
<?php	
	//<tr>
	//	<td align="center" class="textblk" style="font-weight: bold" colspan="<?php echo $qtdColunas + 4;">Gest�o do Programa de Capacita��o em Tecnologias para Vendas</td>
	//</tr>
?>
	
	<tr align="center" class="textblk" style="font-weight: bold">
		<td colspan="3" class="titRelatEsq">
			Data
		</td>
		<td colspan="<?php echo $qtdColunas + 1;?>" class="titRelat">
			Acesso �s Ferramentas
		</td>
	</tr>
	<tr align="center" class="textblk">
		<?php
			if ($usaData){
		?>
				<td class="titRelatEsqRod">M�s</td>
				<td class="titRelatRod">Dia</td>
				<td class="titRelatRod">Dia</td>
		<?php
			}else {
		?>
				<td colspan="3" class="titRelatEsqRod">&nbsp;</td>
		<?php
			}
		
		?>
	
<?php
	
	while ($linha = mysql_fetch_array($colunas)) {
		echo "<td style='word-wrap: break-word' class=\"titRelatRod\">{$linha["DS_PAGINA_ACESSO"]}</td>";
		$codigosPaginas[] = $linha["CD_PAGINA_ACESSO"];
	}

?>
	<td class="titRelatRod">Total de Acessos</td>
	</tr>

<?php
	
	$totalPagina = array();
	$maximoPagina = array();
	$qtdTotalAcesso = 0;
	$linhas = 0;
	$maximoDia = 0;
	
	$sql = "SELECT
				  CD_PAGINA_ACESSO, DATE_FORMAT(DT_ACESSO, '%Y%m%d') AS DT_ACESSO,
				  $tipoAcessoContado AS QD_TOTAL
				FROM
				  col_acesso a
				  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
				WHERE
				  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
				  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
				  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
				  AND   (u.cargofuncao in ('$cargo') OR '$cargo' = '-1')
				GROUP BY
				  CD_PAGINA_ACESSO, DATE_FORMAT(DT_ACESSO, '%Y%m%d')";

	//				  OR (a.CD_PAGINA_ACESSO = 10)		
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	
	$resultados = array();
		
	while ($linha = mysql_fetch_array($resultado))
	{
		$indice = "{$linha["CD_PAGINA_ACESSO"]}{$linha["DT_ACESSO"]}";
		$resultados[$indice] = $linha["QD_TOTAL"];
	}
	
	
	$sql = "SELECT
				  DATE_FORMAT(DT_ACESSO, '%Y%m%d') AS DT_ACESSO,
				  $tipoAcessoContado AS QD_TOTAL
				FROM
				  col_acesso a
				  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
				  LEFT JOIN col_pagina_acesso pa ON pa.CD_PAGINA_ACESSO = a.CD_PAGINA_ACESSO
				WHERE
				   (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
				  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
				  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
				  AND   (u.cargofuncao in ('$cargo') OR '$cargo' = '-1')
				  AND	(NOT pa.IN_ORDENAR = -1 AND NOT pa.IN_ORDENAR = 999)
				GROUP BY
				  DATE_FORMAT(DT_ACESSO, '%Y%m%d')";
	
	//				  OR (a.CD_PAGINA_ACESSO = 10)

	$resultadoDiario = DaoEngine::getInstance()->executeQuery($sql,true);
	
	$resultadosDiarios = array();
		
	while ($linha = mysql_fetch_array($resultadoDiario))
	{
		$indice = "{$linha["DT_ACESSO"]}";
		$resultadosDiarios[$indice] = $linha["QD_TOTAL"];
	}
	
	if (!$usaData){
		$dataAtual = '01/01/2008';
	}
	
	while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal))) {

		$aData = split("/", $dataAtual);
		$dia = date("w", strtotime("{$aData[1]}/{$aData[0]}/{$aData[2]}"));
		$dia = obterTextoDiaSemana($dia);

		if ($usaData){
			echo "<tr  align=\"center\"><td class='bdLatEsq' >{$aData[1]}</td><td class='bdLat'>{$aData[0]}</td><td class='bdLat'>$dia</td>";
		}
		
		$quantidadesPagina = array();
		$totalAcessosDia = 0;
		
		$quantidadesPagina = $resultados;
		
		for ($i = 0; $i < $qtdColunas; $i++)
		{
			$acessosPagina = 0;
			$indice = "{$codigosPaginas[$i]}{$aData[2]}{$aData[1]}{$aData[0]}";
			if ($quantidadesPagina[$indice] != null){
				$acessosPagina = $quantidadesPagina[$indice];
			}
			if ($usaData)
				echo "<td class='bdLat'>$acessosPagina</td>";
				
			$totalAcessosDia = $totalAcessosDia + $acessosPagina;
			
			if ($totalPagina[$codigosPaginas[$i]] == null){
				$totalPagina[$codigosPaginas[$i]] = 0;
			}
			
			if ($maximoPagina[$codigosPaginas[$i]] == null) {
				$maximoPagina[$codigosPaginas[$i]] = 0;
			}
			
			$totalPagina[$codigosPaginas[$i]] = $totalPagina[$codigosPaginas[$i]] + $acessosPagina;
			$maximoPagina[$codigosPaginas[$i]] = max($maximoPagina[$codigosPaginas[$i]], $acessosPagina);
			
		}
		
		$indiceDia = "{$aData[2]}{$aData[1]}{$aData[0]}";
		$totalAcessosDia = $resultadosDiarios[$indiceDia];
		
		if ($totalAcessosDia == "")
			$totalAcessosDia = 0;
		
		if ($usaData)
			echo "<td class='bdLat'>$totalAcessosDia</td>";
		
		$maximoDia = max($maximoDia, $totalAcessosDia);
		
		$qtdTotalAcesso = $qtdTotalAcesso + $totalAcessosDia;
		
		$linhas++;
		
		$dataAtual = somarUmDia($dataAtual);
	}
	

	echo "<tr style='font-weight: bold'>
	<td colspan=\"3\" class='titRelatEsq'>Total</td>";

	for ($i = 0; $i < $qtdColunas; $i++)
	{
		echo "<td align='center' class='titRelat'>{$totalPagina[$codigosPaginas[$i]]}</td>";
	}
	
	echo "<td align='center' class='titRelat'>$qtdTotalAcesso</td>";
	
	echo "</tr><tr style='font-weight: bold'>
	<td colspan=\"3\" class='titRelatEsq'>M�dia</td>";
	
	for ($i = 0; $i < $qtdColunas; $i++)
	{
		$media = round($totalPagina[$codigosPaginas[$i]] / $linhas);
		echo "<td align='center' class='titRelat'>$media</td>";
	}
	
	$media = round($qtdTotalAcesso / $linhas);
	
	echo "<td align='center' class='titRelat'>$media</td>";
	
	
	echo "<tr style='font-weight: bold'>
	<td colspan=\"3\" class='titRelatEsq' style='border-bottom: 1px solid #000000'>M�ximo</td>";

	for ($i = 0; $i < $qtdColunas; $i++)
	{
		echo "<td align='center' class='titRelatRod'>{$maximoPagina[$codigosPaginas[$i]]}</td>";
	}
	
	echo "<td align='center' class='titRelatRod'>$maximoDia</td>";
	
	
?>
		
	</tr>
</table>

	<?php
		if ($_GET["excel"] != 1) {
	?>
	<p align="center">
		<br />
		<INPUT class="buttonsty" onclick="javascript:document.location.href = '<?php echo $_SERVER['REQUEST_URI']."&excel=1"; ?>'" type="button" value="Excel">
		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
		<p class="textoInformativo" align="center">Favor definir o formato paisagem para fins de impress�o</p>
	</p>
	<?php
		}
	?>
	
</body>
</html>

<?php

	ob_end_flush();

	function obterTextoDiaSemana($numero)
	{

		switch ($numero) {
			case 0:
				return 'dom';
				break;
			case 1:
				return 'seg';
				break;
			case 2:
				return 'ter';
				break;
			case 3:
				return 'qua';
				break;
			case 4:
				return 'qui';
				break;
			case 5:
				return 'sex';
				break;
			case 6:
				return 'sab';
				break;

		}
		
	}

	function conveterDataParaYMD($data)
	{
		$aData = split("/", $data);
		$data = "{$aData[2]}{$aData[1]}{$aData[0]}";
		
		return $data;
		
	}
	
	
	
	function somarUmDia($data){
		$aData = split("/", $data);
		$data = date("d/m/Y", strtotime("{$aData[1]}/{$aData[0]}/{$aData[2]}") + 90000);
		
		return  $data;
	}
		
?>