<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

include('../include/defines.php');
include('framework/importacao.php');
include('../include/cripto.php');

$codigoEmpresa = 34;
$errosImportacao = array();
$senhaSelecionada = "TIM2010";
$sucesso = "Importa��o Realizada com Sucesso";

function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/admin/importacao_usuario.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

function uploadXml()
{
	global $codigoEmpresa;
	$empresa = $codigoEmpresa;
	$dataAtual = date("YmdHis");
	$nome = $_FILES['fleImportacao']['name'];
	$savefile = "";
	
	$UPLOAD_BASE_IMG_DIR	= getDocumentRoot()."/admin/importacoes/";
	
	//$UPLOAD_BASE_IMG_DIR = "importacoes/";
	
	
	
	if(is_uploaded_file($_FILES['fleImportacao']['tmp_name'])){
		
		$savefile = "{$UPLOAD_BASE_IMG_DIR}{$nome}_{$empresa}_{$dataAtual}.xml";
		if(move_uploaded_file($_FILES['fleImportacao']['tmp_name'], $savefile)){
			chmod($savefile, 0666);
		}
		
	}

	//$savefile = "D:\\Zend\\Apache2\\htdocs\\colaborae\\admin\\importacoes\\PlanilhaTim.xml";
	
	$dom = DOMDocument::load($savefile) or die("O arquivo n�o � um XML v�lido");
	
	return $dom;
	
}

function trataSQL($valor)
{
	$valor = utf8_decode($valor);
	$valor = str_replace("'", "\\'", $valor);
	$valor = trim($valor);
	
	return $valor;
	
}

class Lotacao
{
	public $codigo;
	public $codigoPai;
	public $nome;
	public $nivel;
	public $hierarquia;
}

class Usuario
{
	public $codigo;
	public $login;
	public $nome;
	public $lotacao;
	public $cargo;
	public $tipo;
	public $nivelLotacao;
}

function montaUsuario(&$usuarios, &$usuario, &$linhaAtual, &$linha)
{
	
	if ($usuario->login == "") {
		$erro = "Usu�rio sem login";
		imprimeLinhaErroUsuario($linha, $erro, $linhaAtual);
		return;
	}
	
	if ($usuarios[substr("000000000{$usuario->login}",-11)] != null)
	{
		$erro = "Login {$usuario->login} existe mais de uma vez na planilha. Apenas a primeira entrada deste login foi inserida.";
		imprimeLinhaErroUsuario($linha, $erro, $linhaAtual);
		return;
	}
	
	$usuario->login = substr("000000000{$usuario->login}",-11);
	
	$usuarios[$usuario->login] = $usuario;
	
}

function montaLotacao(&$lotacoes, &$celula, &$indice, &$pai)
{
	
	if (trataSQL($celula->nodeValue) == "")
		return;
	
	$lotacao = new Lotacao();
	$lotacao->nivel = $indice;
	$lotacao->nome = trataSQL($celula->nodeValue);
	$lotacao->codigoPai = $pai;
	
	if ($lotacoes["{$indice}_{$lotacao->nome}"] != null) //Se a lota��o j� foi inserida na lista verifica se o pai � o mesmo
	{
		if (strtoupper($lotacoes["{$indice}_{$lotacao->nome}"]->codigoPai) != strtoupper($pai))
		{
			$problemasLotacao = array();
				 
			$problemasLotacao["pai"] = $lotacoes["{$indice}_{$lotacao->nome}"]->codigoPai;
			$problemasLotacao["lotacao"] = $lotacao;
			return $problemasLotacao;
				 
		}
	}
	
	$lotacoes["{$indice}_{$lotacao->nome}"] = $lotacao;
	return null;
}

function obterNomesNivel($nivel)
{
	switch($nivel)
	{
		case 1:
			return "Diretor";
			break;	
		case 2:
			return "Gerente";
			break;
		case 3:
			return "Coordenador";
			break;
		case 4:
			return "Trader";
			break;
		case 5:
			return "TBP";
			break;
	}
}

function imprimeLinhaErro(&$linha, &$erros, $linhaAtual)
{
	
	global $errosImportacao;
	
	if ($errosImportacao[$linhaAtual] == null)
	{
	
		$errosImportacao[$linhaAtual] = array();
	
		$celulas = $linha->getElementsByTagName( 'Cell' );
		foreach($celulas as $celula)
		{
			$errosImportacao[$linhaAtual][] = trataSQL($celula->nodeValue);
		}

	}
		
	$nivel = obterNomesNivel($erros["lotacao"]->nivel);
	$nome = $erros["lotacao"]->nome;
	$pai = $erros["lotacao"]->codigoPai;
	
	if ($errosImportacao[$linhaAtual]["erros"] != "") $errosImportacao[$linhaAtual]["erros"] = "{$errosImportacao[$linhaAtual]["erros"]}";
	
	$errosImportacao[$linhaAtual]["erros"] = "{$errosImportacao[$linhaAtual]["erros"]} - Hierarquia inv�lida na linha $linhaAtual! $nome ($nivel) possui mais de um pai: {$erros["pai"]} e $pai";
	
}

function imprimeLinhaErroUsuario(&$linha, &$erro, $linhaAtual)
{
	
	global $errosImportacao;
	
	if ($errosImportacao[$linhaAtual] == null)
	{
	
		$errosImportacao[$linhaAtual] = array();
	
		$celulas = $linha->getElementsByTagName( 'Cell' );
		foreach($celulas as $celula)
		{
			$errosImportacao[$linhaAtual][] = trataSQL($celula->nodeValue);
		}

	}
		
	if ($errosImportacao[$linhaAtual]["erros"] != "") $errosImportacao[$linhaAtual]["erros"] = "{$errosImportacao[$linhaAtual]["erros"]}";
	
	$errosImportacao[$linhaAtual]["erros"] = "{$errosImportacao[$linhaAtual]["erros"]} - Usu�rio Inv�lido: $erro ";
	
}

if (isset($_POST["btnImportar"]))
{
	$xml = uploadXml();
	
	//exit();
	
	$linhas = $xml->getElementsByTagName( 'Row' );
	
	$lotacoes = array();
	$usuarios = array();
	$colunasLotacao = 5;
	
	$primeiraLinha = true;
	$linhaAtual = 2;
	
	foreach($linhas as $linha)
	{
		
		if ($primeiraLinha) { $primeiraLinha = false; continue;}
		
		$celulas = $linha->getElementsByTagName( 'Cell' );
		
		$indice = 1;
		$lotacaoPai = null;
		$linhaProblema = false;
		
		$nomeUsuario = "";
		$cargoUsuario = "";
		$lotacaoUsuario = "";
		$loginUsuario = "";
		$tipoUsuario = 1;
		foreach($celulas as $celula)
		{
			
			$ind = $celula->getAttribute( 'Index' );
			if ( $ind != null ) $indice = $ind;
			
			//Lota��es
			if (!($indice > $colunasLotacao))
			{
				$erros = montaLotacao($lotacoes, $celula, $indice, $lotacaoPai);
				if ($erros != null)
				{
					imprimeLinhaErro($linha, $erros, $linhaAtual);
				}
				if (trataSQL($celula->nodeValue) != "")
				{
					$nivelLotacao = $indice;
					$lotacaoUsuario = trataSQL($celula->nodeValue);	
				}
				
			}
			else //Usuarios
			{
				if ( $indice == $colunasLotacao + 1 ) $nomeUsuario = trataSQL($celula->nodeValue);
				if ( $indice == $colunasLotacao + 2 ) $cargofuncao = trataSQL($celula->nodeValue);
				if ( $indice == $colunasLotacao + 3 ) $login = trataSQL($celula->nodeValue);
				if ( $indice == $colunasLotacao + 4 ) $tipoUsuario = obterPerfil($celula->nodeValue);
				
			}
			
			$indice++;
			$lotacaoPai = trataSQL($celula->nodeValue);
			
		}
		
		$usuario = new Usuario();
		$usuario->nome = $nomeUsuario;
		$usuario->login = $login;
		$usuario->tipo = $tipoUsuario;
		$usuario->cargo = $cargofuncao;
		$usuario->lotacao = $lotacaoUsuario;
		$usuario->nivelLotacao = $nivelLotacao;
		
		montaUsuario($usuarios, $usuario, $linhaAtual, $linha);
		
		$linhaAtual++;
		
	}
	
	$lotacoesCadastradas = trataLotacoes($lotacoes, $codigoEmpresa);
	trataUsuarios($usuarios, $codigoEmpresa, $lotacoesCadastradas);
	
	acertaHierarquia($codigoEmpresa);
	
}
elseif (isset($_POST["btnAcertaHierarquia"]))
{
	acertaHierarquia($codigoEmpresa);
}
elseif (isset($_POST["btnExcluir"]))
{
	
	$xml = uploadXml();
	
	//exit();
	$linhas = $xml->getElementsByTagName( 'Row' );
	
	$primeiraLinha = true;
	$linhaAtual = 2;
	$itensExcluidos = "";
	
	foreach($linhas as $linha)
	{
		
		if ($primeiraLinha) { $primeiraLinha = false; continue;}
		
		$celulas = $linha->getElementsByTagName( 'Cell' );
		$indice = 1;
		foreach($celulas as $celula)
		{
			
			$ind = $celula->getAttribute( 'Index' );
			if ( $ind != null ) $indice = $ind;
			
			//S� le a primeira coluna
			$itemExcluido = trataSQL($celula->nodeValue);
			$itemExcluido = substr("000000000$itemExcluido",-11);
			$itensExcluidos = "$itensExcluidos '$itemExcluido',";
			break;
			
		}
	}
	
	$itensExcluidos = "($itensExcluidos '')";
	
	//Backup dos itens
	$sql = "INSERT INTO col_usuario_historico SELECT * FROM col_usuario WHERE empresa = $codigoEmpresa AND login IN $itensExcluidos";
	DaoEngine::getInstance()->executeQuery($sql,true);
	
	$sql = "DELETE FROM col_usuario WHERE empresa = $codigoEmpresa AND login IN $itensExcluidos";
	DaoEngine::getInstance()->executeQuery($sql,true);
	
	acertaHierarquia($codigoEmpresa);
	acertaHierarquia($codigoEmpresa);
	acertaHierarquia($codigoEmpresa);
	acertaHierarquia($codigoEmpresa);
	acertaHierarquia($codigoEmpresa);
	
}


function obterPerfil($tipoUsuario)
{
	switch(strtoupper($tipoUsuario))
	{
		case "SUPERUSU�RIO":
			return -1;
			break;
		case "GERENTE":
			return -3;
			break;
		default:
			return 1;
			break;
	}
}

function trataUsuarios(&$usuarios, &$codigoEmpresa, &$lotacoesCadastradas)
{
	$usuariosCadastrados = obterListaUsuariosCadastrados($codigoEmpresa);
	
	global $senhaSelecionada;
	
	foreach($usuarios as $usuario)
	{
		$usuarioCadastrado = $usuariosCadastrados[$usuario->login];
		
		$tipoUsuario = $usuario->tipo;
		$nomeUsuario = $usuario->nome;
		$cargofuncao = $usuario->cargo;
		$login = $usuario->login;
		$lotacao = 'null';
		$senha = Cripto($senhaSelecionada,'C');
		
		foreach($lotacoesCadastradas as $lotacao)
		{
			if (strtoupper($lotacao->nome) == strtoupper($usuario->lotacao) && $lotacao->nivel == $usuario->nivelLotacao)
			{
				$lotacao = $lotacao->codigo;
				break;
			}
		}
			

		if ($usuarioCadastrado == null) //Se n�o existe usu�rio, insere
		{
			$sql = "INSERT INTO col_usuario (empresa, DT_IMPORTACAO, datacadastro, tipo, lotacao, NM_USUARIO, cargofuncao, login, senha, IN_SENHA_ALTERADA)
			VALUES ($codigoEmpresa, sysdate(), sysdate(), $tipoUsuario, $lotacao, '$nomeUsuario', '$cargofuncao', '$login', '$senha', 0)";
			
			$usuario->codigo = DaoEngine::getInstance()->executeQueryInsert($sql,true);
			
			$sql = "INSERT INTO col_prova_aplicada (CD_USUARIO, CD_PROVA, NR_TENTATIVA_USUARIO)
					SELECT {$usuario->codigo}, CD_PROVA, 0 FROM col_prova_vinculo WHERE (CD_VINCULO = $codigoEmpresa AND IN_TIPO_VINCULO = 1)
					OR (CD_VINCULO = $lotacao AND IN_TIPO_VINCULO = 2)";
			
			DaoEngine::getInstance()->executeQuery($sql,true);
			
		}
		else //Se j� existe, altera
		{
			
			$sql = "UPDATE col_usuario SET DT_IMPORTACAO = sysdate(), lotacao = $lotacao, NM_USUARIO = '$nomeUsuario', cargofuncao = '$cargofuncao',
					tipo = $tipoUsuario WHERE CD_USUARIO = {$usuarioCadastrado->codigo}";
			
			DaoEngine::getInstance()->executeQuery($sql,true);
			
		}
	}
}

function trataLotacoes(&$lotacoes, $codigoEmpresa)
{
	
	$lotacoesCadastradas = obterListaLotacoesHierarquia($codigoEmpresa);
	
	foreach ($lotacoes as $lotacao)
	{
		$lotacaoCadastrada = obterLotacao($lotacao, $lotacoesCadastradas);
		$lotacaoPai = obterLotacaoPaiCadastrada($lotacao, $lotacoes, $lotacoesCadastradas);
		
		if ($lotacaoCadastrada == null) //Se n�o existe a lota��o insere
		{
			
			$nomeLotacao = $lotacao->nome;
			$nivelHierarquico = $lotacao->nivel;
			$lotacaoHierarquia = "";
			$codigoPai = 'null';
			if ($lotacaoPai != null)
			{
				$codigoPai = $lotacaoPai->codigo;
				$lotacaoHierarquia = $lotacaoPai->hierarquia; 
			}
			
			$sql = "
					INSERT INTO col_lotacao	(DS_LOTACAO, CD_EMPRESA, IN_ATIVO, CD_LOTACAO_PAI, CD_NIVEL_HIERARQUICO, DS_LOTACAO_HIERARQUIA)
					VALUES					('$nomeLotacao', $codigoEmpresa, 1, $codigoPai, $nivelHierarquico, '')";
			
			$lotacao->codigo = DaoEngine::getInstance()->executeQueryInsert($sql,true);
			$lotacao->codigoPai = $lotacaoPai->codigo;
			
			$codigoNovaLotacao = $lotacao->codigo;
			$lotacaoHierarquia = "$lotacaoHierarquia.$codigoNovaLotacao";
			if ($nivelHierarquico == 1)
			{
				$lotacaoHierarquia = $codigoNovaLotacao;
			}
			
			$lotacao->hierarquia = $lotacaoHierarquia;
			
			$sql = "
					UPDATE col_lotacao SET DS_LOTACAO_HIERARQUIA = CONCAT(DS_LOTACAO_HIERARQUIA, '$lotacaoHierarquia') WHERE CD_EMPRESA = $codigoEmpresa AND CD_LOTACAO = $codigoNovaLotacao";
			
			DaoEngine::getInstance()->executeQuery($sql,true);
			
			$lotacoesCadastradas[$lotacao->codigo] = $lotacao;
			
			//echo "<br />$sql";
		}
		else //Se existe na base de dados atualiza se necess�rio 
		{
			
			//Obtem a Lota��o pai Original
			if (strtoupper($lotacao->codigoPai) != strtoupper($lotacoesCadastradas[$lotacaoCadastrada->codigoPai]->nome))
			{
				$codigoPai = 'null';
				if ($lotacaoPai != null)
				{
					$codigoPai = $lotacaoPai->codigo;
					$lotacaoHierarquia = $lotacaoPai->hierarquia;
				}
				
				$codigoLotacao = $lotacaoCadastrada->codigo;
				
				$lotacaoHierarquia = "$lotacaoHierarquia.$codigoLotacao";
				
				if ($nivelHierarquico == 1)
				{
					$lotacaoHierarquia = $codigoLotacao;
				}
				
				$lotacaoCadastrada->codigoPai = $codigoPai;
				$lotacaoCadastrada->hierarquia = $lotacaoHierarquia;
				
				$sql = "
					UPDATE col_lotacao SET CD_LOTACAO_PAI = $codigoPai, DS_LOTACAO_HIERARQUIA = '$lotacaoHierarquia' WHERE CD_EMPRESA = $codigoEmpresa AND CD_LOTACAO = $codigoLotacao";
			
				DaoEngine::getInstance()->executeQuery($sql,true);
				
			}
		}
		
		
		
	}

	return $lotacoesCadastradas;
	
}

function obterLotacaoPaiCadastrada(&$lotacao, &$lotacoes, &$lotacoesCadastradas)
{
	$nomeLotacaoPai = $lotacao->codigoPai;
	$nivelPai = $lotacao->nivel - 1;
	
	if ($nivelPai < 0)
	{
		return null;
	}
	
	$lotacaoPai = $lotacoes["{$nivelPai}_{$nomeLotacaoPai}"];
	return obterLotacao($lotacaoPai, $lotacoesCadastradas);
	
}

function obterListaUsuariosCadastrados($codigoEmpresa)
{
	$sql = "SELECT CD_USUARIO, login FROM col_usuario WHERE empresa = $codigoEmpresa"; 
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$usuariosCadastrados = array();
	
	while($linha = mysql_fetch_array($resultado))
	{
		$usuario = new Usuario();
		$usuario->login = $linha["login"];
		$usuario->codigo = $linha["CD_USUARIO"];
		
		$usuariosCadastrados[$usuario->login] = $usuario;
		
	}
	
	return $usuariosCadastrados;
}

function obterListaLotacoesHierarquia($codigoEmpresa)
{
	
	$sql = "SELECT * FROM col_lotacao WHERE CD_EMPRESA = $codigoEmpresa ORDER BY CD_NIVEL_HIERARQUICO";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$lotacoesCadastradas = array();
	
	while($linha = mysql_fetch_array($resultado))
	{
		$lotacao = new Lotacao();
		$lotacao->codigo = $linha["CD_LOTACAO"];
		$lotacao->codigoPai = $linha["CD_LOTACAO_PAI"];
		$lotacao->nivel = $linha["CD_NIVEL_HIERARQUICO"];
		$lotacao->nome = $linha["DS_LOTACAO"];
		$lotacao->hierarquia = $linha["DS_LOTACAO_HIERARQUIA"];
		
		$lotacoesCadastradas[$lotacao->codigo] = $lotacao;
		
	}
	
	return $lotacoesCadastradas;
	
}

function obterLotacao(&$lotacaoBusca, &$baseLotacoes)
{
	foreach($baseLotacoes as $lotacao)
	{
		if (strtoupper($lotacaoBusca->nome) == strtoupper($lotacao->nome) && $lotacaoBusca->nivel == $lotacao->nivel)
		{
			return $lotacao;
		}
	}
	
	return null;
}


function escreveTexto($texto, $nomeArquivo)
{
	$fp = fopen($nomeArquivo, "a") or die("Couldn't create new file");
	$numBytes = fwrite($fp, $texto);
	fclose($fp); 
}

function acertaHierarquia($codigoEmpresa)
{
	$lotacoes = obterListaLotacoesHierarquia($codigoEmpresa);
	
	foreach ($lotacoes as $lotacao)
	{
		
		$lotacaoHierarquia = obterDescricaoHierarquia($lotacao, $lotacoes);
		
		$sql = "UPDATE col_lotacao SET DS_LOTACAO_HIERARQUIA = '$lotacaoHierarquia' WHERE CD_LOTACAO = {$lotacao->codigo}";
		DaoEngine::getInstance()->executeQuery($sql,true);
		
	}
	
	$sql = "SELECT CD_LOTACAO FROM col_lotacao WHERE
			CD_EMPRESA = $codigoEmpresa AND
			CD_LOTACAO NOT IN (SELECT CD_LOTACAO_PAI FROM col_lotacao WHERE CD_EMPRESA = $codigoEmpresa AND CD_LOTACAO_PAI IS NOT NULL)
			AND CD_LOTACAO NOT IN (SELECT lotacao FROM col_usuario WHERE empresa = $codigoEmpresa AND lotacao IS NOT NULL)";
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	
	while($linha = mysql_fetch_row($resultado))
	{
		
		$sql = "DELETE FROM col_lotacao WHERE CD_LOTACAO = {$linha[0]} AND CD_EMPRESA = $codigoEmpresa";
		DaoEngine::getInstance()->executeQuery($sql,true);
		
	}
	
}

function obterDescricaoHierarquia(&$lotacao, &$lotacoes)
{
	
	$lotacaoHierarquia = "";
	if ($lotacao->codigoPai != null && $lotacao->codigoPai > 0)
	{
		$lotacaoHierarquiaPai = obterDescricaoHierarquia($lotacoes[$lotacao->codigoPai], $lotacoes);
		$lotacaoHierarquia = "{$lotacaoHierarquia}{$lotacaoHierarquiaPai}.";
	}

	$lotacaoHierarquia = "$lotacaoHierarquia{$lotacao->codigo}";
	
	return $lotacaoHierarquia;
	
}

//acertaHierarquia($codigoEmpresa);

?>
<html>
<body>
		<?php 
			$dataAtual = date("YmdHis");
			$nomeArquivo = "importacoes/importacao$dataAtual{$_FILES['fleImportacao']['name']}.xls";
			
			if (count($errosImportacao) > 0)
			{
				escreveTexto("<table>", $nomeArquivo);
			}
				
			foreach($errosImportacao as $linhasErros)
			{
				
				escreveTexto("<tr>", $nomeArquivo);

				foreach($linhasErros as $linhaErro)
				{
					escreveTexto("<td>$linhaErro</td>", $nomeArquivo);
				}
				
				escreveTexto( "</tr>", $nomeArquivo );
			}
		
			if (count($errosImportacao) > 0)
			{
				escreveTexto("</table>", $nomeArquivo);
				
				echo "Existem erros no arquivo importado. Para a listagem dos erros acesse o <a href='$nomeArquivo'>link</a>"; 
			}
			
			if (count($_POST) > 0)
			{
				echo "<p>$sucesso</p>";
			}
		
		?>

	<form name="frmUpload" method="post" enctype="multipart/form-data">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>Arquivo:</td>
			</tr>
			<tr>
				<td>
					<input type="file" name="fleImportacao" id="fleImportacao" />
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Importar" name="btnImportar" id="btnImportar" />
					&nbsp;
					<input type="submit" value="Excluir" name="btnExcluir" id="btnExcluir" />
				</td>
			</tr>
			<tr>
				<td>
					<?php if ($_REQUEST["hierarquia"] == 1){?>
					<input type="submit" value="Acertar Hierarquia" name="btnAcertaHierarquia" id="btnAcertaHierarquia" />
					<?php }?>
				</td>
			</tr>
		</table>
	
	</form>
	<a href="exportacadastro.php?id=34" target="_blank">Link Para o Cadastro da Tim</a>
	
	<div>Para excluir deve-se usar o mesmo procedimento da importa��o. Por�m o arquivo xml deve possuir em <b>sua primeira coluna o login (CPF) do usu�rio</b>. O formato � diferente para evitar que usu�rios sejam exclu�dos indevidamente.</div>
	
</body>
</html>