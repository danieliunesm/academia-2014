<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";

$searchString = $_SERVER["QUERY_STRING"];

$vm = isset($_GET["vm"])?$_GET["vm"]:1;

function getSTATUScheck($oView,$tb)
{
	switch($oView)
	{
		case 1:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 5)";
		case 3:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 2 OR " . $tb . "STATUS = 5)";
		case 5:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 3 OR " . $tb . "STATUS = 5)";
		case 7:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 2 OR " . $tb . "STATUS = 3 OR " . $tb . "STATUS = 5)";
	}
}

function escreveItemMenu($rowId, $oRs, $bgcolor, $iCount, $nRows){
?>
<tr bgcolor="<? echo $bgcolor; ?>" id="<? echo $rowId; ?>">
	<td class="textblk3" style="background-color:#fff" colspan="2"><?
if($oRs[6] == 1)
	echo "<div class=\"menuitem\"";
else
	echo "<div class=\"menusubitem\"";
if($oRs[2] != 1) echo " style=\"color:#c00\"";
?>"><? echo $oRs[0]; ?></div></td>
	<td id="<? echo ($rowId . "upTD"); ?>">
	<? if($iCount > 1){ ?>
		<a href="#" onclick="return up('tb1','<? echo $rowId; ?>')" onmouseover="swapImg('','<? echo ($rowId . "up"); ?>','bt_up_ovr')"  onmouseout="swapImg('','<? echo ($rowId . "up"); ?>','bt_up')" onfocus="noFocus(this)"><img id="<? echo ($rowId . "up"); ?>" src="images/bt_up.gif" width="11" height="20"></a>
	<? }else{ ?>
		<img src="images/blank.gif" width="11" height="20">
	<? } ?>
	</td>
	<td id="<? echo ($rowId . "dwTD"); ?>">
	<? if($iCount < $nRows){ ?>
		<a href="#" onclick="return dw('tb1','<? echo $rowId; ?>')" onmouseover="swapImg('','<? echo ($rowId . "dw"); ?>','bt_down_ovr')"  onmouseout="swapImg('','<? echo ($rowId . "dw"); ?>','bt_down')" onfocus="noFocus(this)"><img id="<? echo ($rowId . "dw"); ?>" src="images/bt_down.gif" width="11" height="20"></a>
	<? }else{ ?>
		<img src="images/blank.gif" width="11" height="20">
	<? } ?>
	</td>
	<td align="center"><input type="checkbox" name="SubMnuOrMnu<? echo $rowId; ?>"<?
if($oRs[6] != 1) echo " checked";
if($iCount == 1) echo " disabled";
	?> onclick="setMenuSubMenuItem(this,this.checked)"><input type="hidden" name="MnuGroup<? echo $rowId; ?>" value="<? echo $oRs[6]; ?>"></td>
	<td class="item"><input type="radio" name="Menu<? echo $oRs[3]; ?>" value="1" <? if($oRs[2] == 1 || $oRs[2] == 5) echo "checked"; ?> onfocus="noFocus(this)" <? if($oRs[2] == 5) echo "style=\"background-image:url(images/bg_radio.gif)\""; ?> onclick="<? if($oRs[2] == 5) echo "document.inputForm.Menu" . $oRs[3] . "[0].style.backgroundImage='url(images/bg_radio.gif)'"; ?>"></td>
	<td class="item"><input type="radio" name="Menu<? echo $oRs[3]; ?>" value="3" <? if($oRs[2] == 3) echo "checked"; ?> onfocus="noFocus(this)" onclick="document.inputForm.Menu<? echo $oRs[3]; ?>[0].style.backgroundImage=''" <? if($oRs[5] == 'colaboradoresitens.php' || $oRs[5] == 'faleconosco.php') echo " disabled"; ?>><input type="hidden" name="SubMnuGroup<? echo $rowId; ?>" value="<? echo $oRs[7]; ?>"></td>
<?
if($oRs[5] == 'colaboradoresitens.php' || $oRs[5] == 'faleconosco.php'){
	echo "<td class=\"item\"><img src=\"images/bt_blockedlink.gif\" width=\"20\" height=\"20\"></td>";
}else{
	echo "<td class=\"item\"><a href=\"#\" onclick=\"getURL('menuitem'," . $oRs[3] . ", 2);return false\" onfocus=\"noFocus(this)\"><img src=\"images/";
	if($oRs[4] == 1 || $oRs[4] == 2){
		echo "bt_sitelink.gif";
	}else{
		echo "bt_nolink3.gif";
	}
	echo "\" width=\"20\" height=\"20\"></a></td>";
}
?>
</tr>
<?
}

function escreveItensMenu(){
	global $vm, $nRows;
	
	$sql = "SELECT ID FROM tb_menuitens WHERE " . getSTATUScheck($vm,"");
	$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
	$nRows = mysql_num_rows($RS_query);
	mysql_free_result($RS_query);
	
	$sql = "SELECT Titulo, Data, Status, ID, TipoLink, Link, MenuGroup, SubmenuGroup FROM tb_menuitens WHERE " . getSTATUScheck($vm,"") . " AND MenuGroup = 1 ORDER BY ItemOrder, Titulo";
	$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
	
	$iCount = 0;
	$bgcolor = "#ffffff";
	$rowId = "";
	while($oRs = mysql_fetch_row($RS_query))
	{
		$iCount++;
		if($iCount % 2 == 0) $bgcolor="#ffffff"; else $bgcolor="#f1f1f1";
		$rowId = (string)$oRs[3];//$iCount;
		escreveItemMenu($rowId, $oRs, $bgcolor, $iCount, $nRows);
		
		if($oRs[7] != 0){
			$sql2 = "SELECT Titulo, Data, Status, ID, TipoLink, Link, MenuGroup, SubmenuGroup FROM tb_menuitens WHERE " . getSTATUScheck($vm,"") . " AND MenuGroup = " . $oRs[7] . " ORDER BY ItemOrder, Titulo";
			$query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());
			while($oRs2 = mysql_fetch_row($query2)){
				$iCount++;
				if($iCount % 2 == 0) $bgcolor="#ffffff"; else $bgcolor="#f1f1f1";
				$rowId = (string)$oRs2[3];//$iCount;
				escreveItemMenu($rowId, $oRs2, $bgcolor, $iCount, $nRows);
			}
			mysql_free_result($query2);
		}
	}
	mysql_free_result($RS_query);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>ADMINISTRA��O DO SITE</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/admin/include/css/adminstyles.css">
<script type="text/javascript" src="/include/js/utilities.js"></script>
<script type="text/javascript" src="/admin/include/js/adminfunctions.js"></script>
<script type="text/javascript" src="/admin/include/js/dyntable2.js"></script>
<script type="text/javascript">
function init(){
<?
if($_SESSION["msg"] != "")
{
	echo "	alert(\"" . $_SESSION["msg"] . "\");";
	$_SESSION["msg"] = "";
}
?>
}

function openWindow_(url){
	newWin=null;
	var w=screen.width-40;
	var h=screen.height-80;
	var l=10;
	var t=20;
	newWin=window.open(url,'htmleditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=0,resizable=1');
	if(newWin!=null)setTimeout('newWin.focus()',100);
}

function openWindow(url){
	newWin=null;
	var w=450;
	var h=280;
	var l=(screen.width-w)/2;
	var t=(screen.height-h)/2;
	newWin=window.open(url,'menueditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=0,resizable=0');
	if(newWin!=null)setTimeout('newWin.focus()',100);
}

function getURL(pg,i,a){
	openWindow('/admin/htmleditor/input_'+pg+'.php?id='+i+'&action='+a);
}

function ShowDialog(pagePath, args, width, height, left, top){
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
}

oVm = <? echo $vm; ?>;<? echo "\n"; ?>
function viewControl(vSession){
	oVsession = vSession;
	var html = ShowDialog("/admin/htmleditor/dialog/viewmenuitenscontrol.html", window, 410, 210);
	if(html)document.location.href=html;
}

function submitPage(url,tgt){
	document.inputForm.action=url;
	document.inputForm.target=tgt;
	document.inputForm.submit();
}

function setBold(obj,str){
	var brotherObj = obj.parentNode.parentNode.childNodes[0].childNodes[0];
	if(obj.checked)	brotherObj.innerHTML = '<span>' + str + '</span>';
	else brotherObj.innerHTML = str;
}

function setMenuSubMenuItem(obj,st){		//,rObj
	var rowobjID = obj.name.substring(11,obj.name.length);
	if(st){									// turn into submenu item
		document.getElementById(rowobjID).childNodes[0].childNodes[0].className = 'menusubitem';
		restoreParentsAndBrothers('tb1');
	}
	else{									// turn into menu item
		document.getElementById(rowobjID).childNodes[0].childNodes[0].className = 'menuitem';
		restoreParentsAndBrothers('tb1');
	}
}

function checkChecked(){
	f   = document.inputForm;
	chk = false;
	for(var i=0; i < f.length; i++){
		if(f[i].type == 'radio' && f[i].name.indexOf('Menu')!=-1){
			if(f[f[i].name][1].checked){
				chk = true;
				break;
			}
		}
	}
	return chk;
}

function delRegisters(url){
	if(!checkChecked(document.inputForm)){
		alert('N�o h� itens exclu�dos!     \nPara verificar se h� itens exclu�dos, clique em "Exibir itens" e assinale a op��o "Removidos".     ');
		return false;
	}
	else{
		if(confirm('Confirma dele��o das p�ginas assinaladas? (Esta opera��o n�o poder� ser desfeita.)     ')){
			document.inputForm.action='/admin/delmenuitensregisters.php?<? echo $searchString; ?>';
			document.inputForm.target='';
			document.inputForm.submit();
		}
		else return false;
	}
}

window.onload = init;
</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><strong>USU�RIO:&nbsp;</strong><? echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="4"></td></tr>
<tr><td class="textblk" colspan="3"><span class="title">ITENS MENU</span></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="1"></td></tr>
</table>
<br>

<form name="inputForm" action="" method="post">
<!-- ITENS MENU -->
<table class="home" align="center">
<tbody id="tb1">
<tr><td class="tarjaTitulo" colspan="8">ITENS MENU</td></tr>
<tr>
	<td><a href="#" onclick="viewControl('vm');return false" onfocus="noFocus(this)"><img src="images/bt_all.gif" width="22" height="26" align="absmiddle"><span>&nbsp;Exibir Itens</span></a></td>
	<td colspan="7"><a href="#" onclick="getURL('menuitem','',1);return false" onfocus="noFocus(this)"><img src="images/bt_novo.gif" width="20" height="20" vspace="10" align="absmiddle"><span>&nbsp;Incluir Item</span></a></td>
</tr>
<tr class="tarjaItens">
	<td class="title" colspan="2" rowspan="2" width="68%">ITENS</td>
	<td></td>
	<td></td>
	<td class="title" rowspan="2" width="29%">ITENS SUBMENU</td>
	<td class="title" colspan="2">STATUS</td>
	<td></td>
</tr>
<tr class="tarjaItens">
	<td></td>
	<td></td>
	<td><img class="headerimg" src="images/bt_publicar.gif" width="32" height="32" alt="   publicar   "></td>
	<td><img class="headerimg" src="images/bt_remover.gif" width="32" height="32" alt="   excluir provisoriamente   "></td>
	<td><img class="headerimg" src="images/bt_editar.gif" width="32" height="32" alt="   editar   "></td>
</tr>
<tr class="fioItens"><td><img src="images/blank.gif" width="200" height="1"></td><td colspan="7"></td></tr>
<tr id="tb1FR"><td><div style="width:180px;text-align:right;font-size:10px;padding-bottom:6px;border-bottom:1px solid #ccc;margin-left:25px;margin-bottom:3px">&nbsp;</div></td></tr><!-- identifica o in�cio das linhas reposicion�veis -->
<?
escreveItensMenu();
?>
<tr id="tb1LR"><td>&nbsp;</td></tr><!-- identifica o fim das linhas reposicion�veis -->
<tr class="fioTitulo"><td colspan="8"></td></tr>
</tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="1" height="40"></td></tr>
<tr><td align="center" style="font-family:tahoma,arial,helvetica,sans-serif;font-size:11px;color:#000;white-space:nowrap"><input type="button" class="buttonsty" value="Atualizar Status" onclick="submitPage('/admin/updatemenuitensstatus.php','')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttondelsty" value="" onclick="delRegisters()" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'">Excluir definitivamente<img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"><br><br><br></td></tr>
</table>
<input type="hidden" name="Nmenuitens" value="<? echo $nRows; ?>">
</form>
</body>
</html>
<?
mysql_close();
?>