<?php
include "../include/security.php";
include "../include/genericfunctions.php";
include('page.php');
include("../include/defines.php");
include('framework/crud.php');
include('controles.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Colabor� - Consultoria e Educa&ccedil;&atilde;o Corporativa</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META http-equiv="pragma" content="no-cache">
		<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">
		<script type="text/javascript" src="include/js/functions.js"></script>
		
		<style>

			li { list-style: none }
			
			.nodeH{ display: none }
			
			.nodeS{ display: block }
		
		</style>
	
	<script language="javascript">
	
		function expandCollapse(obj){
		
			if(obj.parentElement.children.length <= 2){
				return;
			}
		
			if(obj.parentElement.children(2).className == 'nodeH'){
				obj.parentElement.children(2).className = 'nodeS';
				obj.src = 'minus_r.gif';
			}else{
				obj.parentElement.children(2).className = 'nodeH'
				obj.src = 'plusik_r.gif';
			}
		}
		
		function marcar(obj){
			
			var y = 0;
			if (obj.parentElement.children.length > 2){
					
				var itemAbaixo = obj.parentElement.children(2).children.length;
				
				for (y = 0; y < itemAbaixo; y++){
				
					obj.parentElement.children(2).children(y).children(1).checked = obj.checked;
					marcar(obj.parentElement.children(2).children(y).children(1));
					
				}
				
			}
		}	
	</script>
	
	
	<?php
	
		$usuarios = "";
	
		foreach($_POST as $key => $valor){
			//echo "$key $valor \n";
			//if (strstr($key, "chkEmpresa") != ""){
			//	$empresas[] = $valor;
			//}elseif (strstr($key, "chkLotacao") != ""){
			//	$lotacoes[] = $valor;
			//}
			if(strstr($key, "chkUsuario") != ""){
				if($usuarios != ""){
					$usuarios = "$usuarios, ";
				}
				$usuarios = "$usuarios $valor";
			}
	    }
	    
	    if ($usuarios != "") {
	    	$sql = "SELECT DISTINCT email FROM col_usuario WHERE CD_USUARIO IN ($usuarios)";
	    	
	    	//$dao = new DaoEngine();
	    	
	    	//$resultado = $dao->executeQuery($sql, true);
	    	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	    	//$resultado = mysql_query($sql);
	    	
	    	$emails = "";
	    	while ($linha = mysql_fetch_array($resultado)) {
	    		if ($linha["email"] != "") {
	    			if ($emails != "") {
	    				$emails = "$emails,";
	    			}
	    			$emailAtual = trim($linha["email"]);
	    			$emails = "$emails{$emailAtual}";
	    		}
	    	}
	    }
	    if ($emails != "") {
	    	
	?>

		<script language="javascript">

			function copiarEmails(){
				
				document.getElementById("txtEmails").select();
				document.getElementById("txtEmails").focus();
				textRange = document.getElementById("txtEmails").createTextRange();
				textRange.execCommand("Copy");

			}

			var exibiuEmail = false;
				
			try{
				document.location.href = 'mailto:colaborae@colaborae.com.br?bcc=<?php echo $emails; ?>'
				exibiuEmail = true;
			}catch(e){
				alert('N�o foi poss�vel carregar a lista de e-mails. Favor copiar e colar a listagem de e-mails selecionados.');
				exibiuEmail = false;
				document.location.href = 'mailto:colaborae@colaborae.com.br?bcc=colaborae@colaborae.com.br';
			}
			
		
		</script>

	<?php
		}
	?>
		
	</HEAD>
	
	<BODY>
	<!-- Inicio do T�tulo -->
		<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
			<TR>
				<TD><IMG height="2" src="images/blank.gif" width="100%"/></TD>
			</TR>
			<TR>
				<TD background="images/bg_logo_admin.png">
					<TABLE cellpadding="0" cellspacing="0" width="663" background="images/logo_admin.png" border="0">
						<TR>
							<TD><IMG src="images/blank.gif" height="32" width="1"/></TD>
							<TD class="data" align="right"><?php echo getServerDate(); ?></TD>
						</TR>
					
					</TABLE>
				
				</TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" width="100%" height="2" /></TD>
			</TR>
			<TR>
				<TD bgcolor="#cccccc"><IMG src="images/blank.gif" height="3" width="100%"/></TD>
			</TR>
		</TABLE>
		<!-- Fim do T�tulo -->
		
		<!-- In�cio Cabe�alho -->
		<TABLE cellspacing="0" cellpadding="0" width="756" align="center" border="0">
			<TR>
				<TD width="1%"><IMG height="20" src="images/blank.gif" width="289"/></TD>
				<TD></TD>
				<TD></TD>
			</TR>
			<TR valign="top">
				<TD class="textblk"><SPAN class="title">USU�RIO: </SPAN><?php echo $_SESSION['alias']; ?></TD>
				<TD width="1%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.replace('logout.php')"
									type="button" value="Logout" /></TD>
				<TD align="right" width="98%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href='admin_menu.php?'"
							type="button" value="Voltar"></TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" height="4" width="1" /></TD>
			</TR>
			<TR>
				<TD class="textblk" colSpan="2"><SPAN class="title">P�GINA ATUAL:</SPAN>
						Newsletter</TD>
			</TR>
			<TR>
				<TD><IMG height="1" src="images/blank.gif" width="280"></TD>
			</TR>
		</TABLE>
		<BR>
		<!-- Fim Cabe�alho -->
		
		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="post">
			
            <input type="hidden" name="hdnRemovidos" value="<?php echo $_POST["hdnRemovidos"] ?>" />
            		
			<TABLE cellpadding="0" cellspacing="0" width="756" align="center" border="0">
				<TR class="tarjaTitulo">
						<TD align="middle" height="20">Newsletter</TD>
				</TR>
				<TR>
					<TD><IMG src="images/blank.gif" height="1" width="10"/></TD>
				</TR>
				<?php if ($emails != ""){ ?>
				<TR id="linhaEmails">
					<TD class="textblk">E-mails selecionados:<br /><TEXTAREA id="txtEmails" name="txtEmails" style="width:755px"><?php echo $emails ?></TEXTAREA>
					<div align="right"><input type="button" class="buttonsty" onclick="copiarEmails(); return false;" value="Copiar" /></div>
					 <br />
					</TD>
				</TR>
				<script>if(exibiuEmail)document.getElementById("linhaEmails").style.display = 'none';</script>
				<?php } ?>
				<TR>
					<TD width="100%">

					
					
					<?php treeViewUsuario(); ?>
					
					
					
					</TD>
				</TR>
				<TR class="tarjaTitulo">
						<TD colSpan="4"><IMG height="1" src="images/blank.gif" width="100"></TD>
				</TR>
				
			</TABLE>

			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD><IMG height="40" src="images/blank.gif" width="1"></TD>
					</TR>
					<TR>
						<TD align="middle">
							<INPUT class="buttonsty" onfocus="noFocus(this)"
								type="submit" value="Enviar para Outlook"/>
							<IMG height="1" src="images/blank.gif" width="20"/><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href='admin_menu.php?'"
								type="button" value="Voltar"/>
							
							<BR>
							<BR>
							<BR>
						</TD>
					</TR>
			</TABLE>
	
		</FORM>
		
	</BODY>
	
</HTML>


<?php



function treeViewUsuario(){
	
	$empresa = new col_empresa();
	$resultadoEmpresa = $empresa->listar(null,"DS_EMPRESA");
	
	echo "<ul style='margin-left: 0px'>";
	
	while ($linhaEmpresa = mysql_fetch_array($resultadoEmpresa)) {
		
		if($linhaEmpresa["IN_ATIVO"] != 1)
			continue;

		$sql = "SELECT * FROM col_lotacao WHERE CD_EMPRESA = {$linhaEmpresa["CD_EMPRESA"]}";
		$resultadoLotacao = $empresa->dao->executeQuery($sql);
		
		$imagem = "plusik_r";
		
		$existeLotacao = mysql_num_rows($resultadoLotacao) != 0;
		
		if (!$existeLotacao){
			$imagem = "minus_r";
		}
		
		$idProva = $_REQUEST["id"];
		$checked = "";
		if (isset($_REQUEST["id"])) {
			
			$sql = "SELECT 1 FROM col_prova_vinculo WHERE CD_PROVA = $idProva AND CD_VINCULO = {$linhaEmpresa["CD_EMPRESA"]} AND IN_TIPO_VINCULO = 1";
			$resultadoCheck = $empresa->dao->executeQuery($sql);
			if (mysql_num_rows($resultadoCheck) > 0){
				$checked = "checked";
			}
			
		}
		
				
	echo "<li class='textblk' style='font-weight: bold'><img src='$imagem.gif' onclick='expandCollapse(this)'><input name='chkEmpresa{$linhaEmpresa["CD_EMPRESA"]}' $checked value='{$linhaEmpresa["CD_EMPRESA"]}' type='checkbox' onclick='marcar(this)'> {$linhaEmpresa["DS_EMPRESA"]}";

		if ($existeLotacao){
			echo "<ul class='nodeH'>";
		}
				
		while ($linhaLotacao = mysql_fetch_array($resultadoLotacao)) {
			
			$sql = "SELECT CD_USUARIO, login FROM col_usuario WHERE lotacao = {$linhaLotacao["CD_LOTACAO"]} AND login IS NOT NULL";
			$resultadoUsuario = $empresa->dao->executeQuery($sql);
			
			$imagem = "plusik_r";
			
			$existeUsuario = mysql_num_rows($resultadoUsuario) != 0;
			
			if (!$existeUsuario){
				$imagem = "minus_r";
			}

			$checked = "";
			if (isset($_REQUEST["id"])) {
			
				$sql = "SELECT 1 FROM col_prova_vinculo WHERE CD_PROVA = $idProva AND CD_VINCULO = {$linhaLotacao["CD_LOTACAO"]} AND IN_TIPO_VINCULO = 2";
				$resultadoCheck = $empresa->dao->executeQuery($sql);
				if (mysql_num_rows($resultadoCheck) > 0){
					$checked = "checked";
				}
			
			}

			
			echo "<li class='textblk'><img src='$imagem.gif' onclick='expandCollapse(this)'><input name='chkLotacao{$linhaLotacao["CD_LOTACAO"]}' $checked value='{$linhaLotacao["CD_LOTACAO"]}' type='checkbox' onclick='marcar(this)'> {$linhaLotacao["DS_LOTACAO"]}";
			
			if ($existeUsuario){
				echo "<ul class='nodeH'>";
			}
			
			while ($linhaUsuario = mysql_fetch_array($resultadoUsuario)) {
				
				$checked = "";
				if (isset($_REQUEST["id"])) {
				
					$sql = "SELECT 1 FROM col_prova_aplicada WHERE CD_PROVA = $idProva AND CD_USUARIO = {$linhaUsuario["CD_USUARIO"]}";
					$resultadoCheck = $empresa->dao->executeQuery($sql);
					if (mysql_num_rows($resultadoCheck) > 0){
						$checked = "checked";
					}
				
				}

				
				echo "<li class='textblk'><IMG style='display: none'><input name='chkUsuario{$linhaUsuario["CD_USUARIO"]}' $checked value='{$linhaUsuario["CD_USUARIO"]}' type='checkbox' onclick='marcar(this)'> {$linhaUsuario["login"]}</li>";
				
			}
			
			
			if ($existeUsuario){
				echo "</ul>";
			}
			
			echo "</li>";
			
		}
		
		if ($existeLotacao){
			echo "</ul>";
		}
		
		
	echo "</li>";
	}
	
	echo "</ul>";
	
}


?>