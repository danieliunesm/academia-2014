<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";
include "controles.php";


$searchString = $_SERVER["QUERY_STRING"];
//session_register("searchString");

$vpi = isset($_GET["vpi"])?$_GET["vpi"]:1;
$t   = isset($_GET["t"])?$_GET["t"]:2;
$o   = isset($_GET["o"])?$_GET["o"]:0;
$p   = isset($_GET["p"])?$_GET["p"]:'';

$folderlist = "";

$iTotal = 0;

function getSTATUScheck($oView){
	switch($oView){
		case 1:
			return "STATUS = 1";
		case 5:
			return "STATUS = 1 OR STATUS = 3";
	}
}

function getCapituloStatusCheck($oView){
    if ($oView == 1)
        return " AND IN_ATIVO = 1 ";
    else
        return "";

}

function getOrder($t,$o){
	$tipo = '';
	switch($t){
		case 1:
			$tipo = "TITULO";
			break;
		case 2:
			$tipo = "DATA";
			break;
        case 3:
            $tipo = "NR_ORDEM";
	}
	switch($o){
		case 0:
			return $tipo;
		case 1:
			return $tipo . " DESC";
	}
}

function escrevePaginasIndividuais(){
	global $vpi, $t, $o, $p, $folderlist, $iTotal;
	$nRows = 0;

    $sql = "SELECT CD_AREA_CONHECIMENTO, NM_AREA_CONHECIMENTO FROM col_area_conhecimento ORDER BY NM_AREA_CONHECIMENTO";
    $queryArea = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

    while($oRsArea = mysql_fetch_row($queryArea)) {
    //$cboEmpresa = $_GET["cboEmpresa"];
?>
    <tr bgcolor="#00008b"><td colspan="5" style="color:#fff; font-size: 13px; font-weight: bold "><? echo $oRsArea[1]; ?></td>
        <td class="item"><a href="#" onclick="openAreaConhecimento(<? echo $oRsArea[0]; ?>,2);return false" onfocus="noFocus(this)"><img src="/admin/images/bt_edit.gif" width="20" height="20" alt="   Editar   "></a></td>
    </tr>
<?php
    //if($cboEmpresa == 0) $cboEmpresa = -1;
	// AND (CD_EMPRESA = $cboEmpresa OR $cboEmpresa = -1)
	$sql = "SELECT ID, TITULO, NR_ORDEM FROM tb_pastasindividuais WHERE STATUS = 1 AND CD_AREA_CONHECIMENTO = {$oRsArea[0]} ORDER BY TITULO";
	$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
	
	while($oRs = mysql_fetch_row($query)){
		$sql2  = "SELECT COUNT(*) FROM tb_paginasindividuais WHERE PASTAID = " . $oRs[0] . " AND (" . getSTATUScheck($vpi) . ")";
		$query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());
		$oRs2 = mysql_fetch_row($query2);
		
		$nRows = (int)($oRs2[0]);

        if ($nRows == 0){

            $sql3 = "SELECT COUNT(*) FROM col_capitulo WHERE CD_PASTA = " . $oRs[0] . getCapituloStatusCheck($vpi);
            $query3 = mysql_query($sql3) or die(ERROR_MSG_SQLQUERY . mysql_error());
            $oRs3 = mysql_fetch_row($query3);
            $nRows = (int)($oRs2[0]);
            mysql_free_result($query3);
        }
		
		mysql_free_result($query2);
		
		if($folderlist != ""){
			$folderlist = $folderlist . "|";
		}
		$folderlist = $folderlist . $oRs[0];
?>
<tr class="fioItens" style="background-color:#fff"><td colspan="6"></td></tr>
<tr bgcolor="#cccccc">
<?
		if($nRows > 0){
?>
	<td class="inputtitle" colspan="5">
    <input name="ordPasta<?php echo $oRs[0];; ?>" type="text" style="width: 30px;" value="<?php echo $oRs[2]; ?>"/>
    <a href="#" onclick="opencloseFolder('<? echo $oRs[0]; ?>');return false" onfocus="noFocus(this)"><img id="imgFolder<? echo $oRs[0]; ?>" src="/admin/images/pgiclosedfolder.gif" align="absmiddle">
        <? echo $oRs[1]; ?>
    </a>
    </td>
<?
		}else{
?>
	<td class="inputtitle" colspan="5">
        <input name="ordPasta<?php echo $oRs[0];; ?>" type="text" style="width: 30px;" value="<?php echo $oRs[2]; ?>"/>
        <img id="imgFolder<? echo $oRs[0]; ?>" src="/admin/images/pgiemptyfolder.gif" align="absmiddle"><? echo $oRs[1]; ?></td>

<?
		}
?>
    <td class="item"><a href="#" onclick="openPasta(<? echo $oRs[0]; ?>,2);return false" onfocus="noFocus(this)"><img src="/admin/images/bt_edit.gif" width="20" height="20" alt="   Editar   "></a></td>
</tr>
<?

        $sql3 = "SELECT CD_CAPITULO, NM_CAPITULO, NR_ORDEM_CAPITULO, IN_ATIVO FROM col_capitulo WHERE CD_PASTA = {$oRs[0]} " . getCapituloStatusCheck($vpi) . " ORDER BY NR_ORDEM_CAPITULO";
        $query3 = mysql_query($sql3) or die(ERROR_MSG_SQLQUERY . mysql_error());

        $capitulos = array();
        $capitulos[] = null;
        while($linhaCapitulo = mysql_fetch_array($query3)){
            $capitulos[] = $linhaCapitulo;
        }

        foreach($capitulos as $capitulo){

            $filtroCapitulo = " CD_CAPITULO IS NULL AND ";
            if($capitulo != null){
                $filtroCapitulo = " CD_CAPITULO = {$capitulo["CD_CAPITULO"]} AND ";

            ?><tr id="itemfolder<? echo $oRs[0]; ?>" style="display:none; background-color: #ddff66;">
                <td class="textblk2" colspan="2">
                    <input disabled="disabled" name="ordCap<?php echo $capitulo["CD_CAPITULO"]; ?>" type="text" style="width: 30px;" value="<?php echo $capitulo["NR_ORDEM_CAPITULO"]; ?>"/><? echo $capitulo["NM_CAPITULO"]; ?></td>
                <td class="dataarquivos">&nbsp;</td>
                <td class="item"><input type="radio" name="Cap<? echo $capitulo["CD_CAPITULO"]; ?>" value="1" <? if($capitulo["IN_ATIVO"] == 1) echo "checked"; ?> onfocus="noFocus(this)"></td>
                <td class="item"><input type="radio" name="Cap<? echo $capitulo["CD_CAPITULO"]; ?>" value="3" <? if($capitulo["IN_ATIVO"] == 3) echo "checked"; ?> onfocus="noFocus(this)"></td>
                <td class="item"><a href="#" onclick="openCapitulo(<? echo $capitulo["CD_CAPITULO"]; ?>,2);return false" onfocus="noFocus(this)"><img src="/admin/images/bt_edit.gif" width="20" height="20" alt="   Editar   "></a></td>
            </tr>
            <?

            }

            $sql2 = "SELECT ID, TITULO, DATA, STATUS, NR_ORDEM FROM tb_paginasindividuais WHERE $filtroCapitulo PASTAID = " . $oRs[0] . " AND (" . getSTATUScheck($vpi) . ") ORDER BY " . getOrder($t,$o);
            $query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());
            $iCont = 0;
            $bgcolor = "#ffffff";
            while($oRs2 = mysql_fetch_row($query2)){
    ?><tr id="itemfolder<? echo $oRs[0]; ?>" bgcolor="<? echo $bgcolor; ?>" style="display:none">
        <td class="textblk2" colspan="2"<?
    if($oRs2[3] != 1){
        echo " style=\"color:#c00\"";
    } ?>><input name="ord<?php echo $oRs2[0]; ?>" type="text" style="width: 30px;" value="<?php echo $oRs2[4]; ?>"/><? echo $oRs2[1]; ?></td>
        <td class="dataarquivos"><? echo FmtData($oRs2[2]); ?></td>
        <td class="item"><input type="radio" name="PGI<? echo $oRs2[0]; ?>" value="1" <? if($oRs2[3] == 1) echo "checked"; ?> onfocus="noFocus(this)"></td>
        <td class="item"><input type="radio" name="PGI<? echo $oRs2[0]; ?>" value="3" <? if($oRs2[3] == 3) echo "checked"; ?> onfocus="noFocus(this)"></td>
        <td class="item"><a href="#" onclick="getURL(<? echo $oRs2[0]; ?>,2);return false" onfocus="noFocus(this)"><img src="/admin/images/bt_edit.gif" width="20" height="20" alt="   Editar   "></a></td>
    </tr>
    <?
                $iCont++;
                $iTotal++;
                if($iCont % 2 == 0) $bgcolor = "#ffffff"; else $bgcolor = "#f1f1f1";
            }
            mysql_free_result($query2);
        }
	}

    }
	echo "<input type=\"hidden\" id=\"Npgi\" name=\"Npgi\" value=\"" . $iTotal . "\" />";
	mysql_free_result($query);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/admin/include/css/adminstyles.css">
<script type="text/javascript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script type="text/javascript" src="/admin/include/js/dyntable.js"></script>
<script language="JavaScript">
function init(){
<?
if($_SESSION["msg"] != "")
{
	echo "	alert(\"" . $_SESSION["msg"]. "\");";
	$_SESSION["msg"] = "";
}
?>
	opencloseFolder('<? echo $p; ?>');
}

function ShowDialog(pagePath, args, width, height, left, top){
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
}

oVpi 	   = <? echo $vpi; ?>;
queryorder = 't=<? echo $t; ?>&o=<? echo $o; ?>&p=<? echo $p; ?>';
oTipo	   = <? echo $t; ?>;
oOrdem 	   = <? echo $o; ?>;
function viewControl(vSession){
	oVsession = vSession;
	queryorder = 't='+oTipo+'&o='+oOrdem+'&p='+getOpenedFolders();
	var html = ShowDialog("/admin/htmleditor/dialog/viewpgicontrol.html", window, 420, 180);
	if(html)document.location.href=html+((html.indexOf('?')!=-1?'&':'?')+queryorder);
}

function opencloseFolder(f){

	var folders = f.split('|');

	for(var i=0;i<folders.length;i++){
		var itemfolder   = document.getElementById('itemFolder'+folders[i]);
		var imgFolder    = document.getElementById('imgFolder'+folders[i]);
		var pgiTable = document.getElementById('pgiTable');
		var pgiTbody = pgiTable.childNodes[0];
		for(var j=0; j<pgiTbody.childNodes.length; j++){
			if(pgiTbody.childNodes[j].id == ('itemfolder'+folders[i])){
				var folderdisplay = pgiTbody.childNodes[j].style.display;
				pgiTbody.childNodes[j].style.display = folderdisplay=='none'?'inline':'none';
				imgFolder.src = folderdisplay=='none'?'/admin/images/pgiopenedfolder.gif':'/admin/images/pgiclosedfolder.gif';
			}
		}
	}
}

function getOpenedFolders(){
	var folders = folderlist.split('|');
	var obj, openedfolders = '';
	for(var i=0;i<folders.length;i++){
		obj = document.getElementById('imgFolder'+folders[i]);
		if(obj.src.indexOf('opened')!=-1){
			if(openedfolders != '')openedfolders += '|';
			openedfolders += folders[i];
		}
	}
	return openedfolders;
}

function setOrder(tipo,ordem){
	oTipo  = tipo;
	oOrdem = ordem;
	queryorder = 't='+tipo+'&o='+ordem+'&p='+getOpenedFolders()+(oVpi>1?('&vpi='+oVpi):'');
	document.location.href = '/admin/admin_paginasindividuais.php?'+queryorder;
}

function openWindow(url,w,h,l,t){
	newWin=null;
	if(!l)l=(screen.width-w)/2;
	if(!t)t=(screen.height-h)/2;
	newWin=window.open(url,'htmleditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=0,resizable=1');
	if(newWin!=null)setTimeout('newWin.focus()',100);
}

function getURL(i,a){
	openWindow('/admin/htmleditor/input_paginasindividuais.php?id='+i+'&action='+a,(screen.width-40),(screen.height-80),10,20);
}


function openCapitulo(i,a){
    openWindow('/admin/htmleditor/input_capitulos.php?id='+i+'&action='+a,(screen.width-40),(screen.height-80),10,20);
}

function openArea(i,a) {
    openWindow('/admin/htmleditor/input_areaconhecimento.php?id='+i+'&action='+a,(screen.width-40),(screen.height-80),10,20);
}

function openPasta(i,a){
    openWindow('/admin/htmleditor/input_pastasindividuais.php?id='+i+'&action='+a,(screen.width-40),(screen.height-80),10,20);
}

function openAreaConhecimento(i,a){
    openWindow('/admin/htmleditor/input_areaconhecimento.php?id='+i+'&action='+a,(screen.width-40),(screen.height-80),10,20);
}

function openCopiarCurso(){

    openWindow('cadastro_copia_curso.php',500, 300,500, 300);

}

function openInputNMPGIFolders(){
	queryorder = 't='+oTipo+'&o='+oOrdem+'&p='+getOpenedFolders()+(oVpi>1?('&vpi='+oVpi):'');
	openWindow('/admin/htmleditor/input_nmpgifolders.php?'+queryorder,450,530);
}

function submitPage(url,tgt){
	queryorder = 't='+oTipo+'&o='+oOrdem+'&p='+getOpenedFolders()+(oVpi>1?('&vpi='+oVpi):'');
    queryorder = queryorder + "&Npgi=" + document.getElementById("Npgi").value;
	document.homeForm.action=url+'?'+queryorder;
	document.homeForm.target=tgt;
	document.homeForm.submit();
}

function checkChecked(){
	f   = document.homeForm;
	chk = false;
	for(var i=0; i < f.length; i++){
		if(f[i].type == 'radio' && f[i].name.indexOf('PGI')!=-1){
			if(f[f[i].name][1].checked){
				chk = true;
				break;
			}
		}
	}
	return chk;
}

function delRegisters(url){
	queryorder = 't='+oTipo+'&o='+oOrdem+'&p='+getOpenedFolders()+(oVpi>1?('&vpi='+oVpi):'');
	
	if(!checkChecked(document.homeForm)){
		alert('N�o h� p�ginas exclu�das!     \nPara verificar se h� p�ginas exclu�das, clique em "Exibir p�ginas" e assinale a op��o "Removidos".     ');
		return false;
	}
	else{
		if(confirm('Confirma a dele��o das p�ginas assinaladas? (Esta opera��o n�o poder� ser desfeita.)     ')){
			document.homeForm.action='/admin/delpgiregisters.php?'+queryorder;
			document.homeForm.target='';
			document.homeForm.submit();
		}
		else return false;
	}
}

window.onload = init;
</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><strong>USU�RIO:&nbsp;</strong><? echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="4"></td></tr>
<tr><td class="textblk" colspan="3"><span class="title">P�GINAS INDIVIDUAIS</span></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="1"></td></tr>
</table>
<br>

<form name="homeForm" action="" method="post">
<?
$listOrder = '';
if($t == 1){
	if($o == 1)
		$listOrder = "Listagem por T�tulo em ordem alfab�tica decrescente";
	else
		$listOrder = "Listagem por T�tulo em ordem alfab�tica crescente";
}else{
	if($o == 0)
		$listOrder = "Listagem por ordem crescente de Data";
	else
		$listOrder = "Listagem por ordem decrescente de Data";
}
?>
<table class="home" id="pgiTable">
<tr><td class="tarjaTitulo" colspan="6">P�GINAS INDIVIDUAIS - <? echo $listOrder; ?></td></tr>
<tr>
	<td><a href="#" onclick="viewControl('vpi');return false" onfocus="noFocus(this)"><img src="images/bt_all.gif" width="22" height="26" align="absmiddle"><span>&nbsp;Exibir p�ginas</span></a></td>
	<td colspan="5" style="padding-left:75px">
        <a href="#" onclick="openArea('',1);return false" onfocus="noFocus(this)"><img src="images/bt_novo.gif" width="20" height="20" vspace="10" align="absmiddle"><span>&nbsp;Incluir �rea de Conhecimento</span></a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="#" onclick="openPasta('',1);return false" onfocus="noFocus(this)"><img src="images/bt_novo.gif" width="20" height="20" vspace="10" align="absmiddle"><span>&nbsp;Incluir Curso</span></a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="#" onclick="getURL('',1);return false" onfocus="noFocus(this)"><img src="images/bt_novo.gif" width="20" height="20" vspace="10" align="absmiddle"><span>&nbsp;Incluir p�gina</span></a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="#" onclick="openCapitulo('',1);return false" onfocus="noFocus(this)"><img src="images/bt_novo.gif" width="20" height="20" vspace="10" align="absmiddle"><span>&nbsp;Incluir cap�tulo</span></a>
        <!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="#" onclick="openCopiarCurso();return false" onfocus="noFocus(this)"><img src="images/bt_novo.gif" width="20" height="20" vspace="10" align="absmiddle"><span>&nbsp;Copiar curso</span></a>-->
    </td>
</tr>
<tr>
    <td colspan="6">
        <!--
        <table cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td class="textblk">
                    Programa:
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                        //$resultado = mysql_query("SELECT CD_EMPRESA, DS_EMPRESA, IN_ATIVO FROM col_empresa");
                        //combobox("cboEmpresa", $_GET["cboEmpresa"], $resultado, "DS_EMPRESA", "CD_EMPRESA", true, "100%", "textbox3", false, true, false, false, "", null, true);
                    ?>
                    <input type='button' name='btnPesquisa' class='buttonsty' value='Buscar' onclick="window.location = 'admin_paginasindividuais.php?cboEmpresa=' + document.getElementById('cboEmpresa').value">
                </td>
            </tr>

            <tr><td>&nbsp;</td></tr>

        </table>
        -->
    </td>


</tr>


<tr class="tarjaItens">
	<td class="title2" rowspan="2" colspan="2">
        <span style="float: left;"><?
            if($t == 3 && $o == 1)
                echo "<a href=\"#\" onclick=\"setOrder(3,0);return false\" onfocus=\"noFocus(this)\"><img class=\"headerimg\" src=\"images/bt_order_dw.gif\" width=\"13\" height=\"13\" alt=\"   listar por Ordena��o em ordem alfab�tica crescente   \" align=\"absmiddle\"></a>";
            else
                echo "<a href=\"#\" onclick=\"setOrder(3,1);return false\" onfocus=\"noFocus(this)\"><img class=\"headerimg\" src=\"images/bt_order_up.gif\" width=\"13\" height=\"13\" alt=\"   listar por Ordena��o em ordem alfab�tica decrescente   \" align=\"absmiddle\"></a>";
            ?>&nbsp;ORDENA��O</span>

        <span><?
if($t == 1 && $o == 1)
	echo "<a href=\"#\" onclick=\"setOrder(1,0);return false\" onfocus=\"noFocus(this)\"><img class=\"headerimg\" src=\"images/bt_order_dw.gif\" width=\"13\" height=\"13\" alt=\"   listar por T�tulo em ordem alfab�tica crescente   \" align=\"absmiddle\"></a>";
else
	echo "<a href=\"#\" onclick=\"setOrder(1,1);return false\" onfocus=\"noFocus(this)\"><img class=\"headerimg\" src=\"images/bt_order_up.gif\" width=\"13\" height=\"13\" alt=\"   listar por T�tulo em ordem alfab�tica decrescente   \" align=\"absmiddle\"></a>";
?>&nbsp;T�TULO</span></td>
	<td class="title" rowspan="2" style="padding-left:17px;padding-right:16px"><?
if($t == 2 && $o == 0)
	echo "<a href=\"#\" onclick=\"setOrder(2,1);return false\" onfocus=\"noFocus(this)\"><img class=\"headerimg\" src=\"images/bt_order_up.gif\" width=\"13\" height=\"13\" alt=\"   listar por Data mais recente � mais antiga   \" align=\"absmiddle\"></a>";
else
	echo "<a href=\"#\" onclick=\"setOrder(2,0);return false\" onfocus=\"noFocus(this)\"><img class=\"headerimg\" src=\"images/bt_order_dw.gif\" width=\"13\" height=\"13\" alt=\"   listar por Data mais antiga � mais recente   \" align=\"absmiddle\"></a>";
?>&nbsp;DATA</td>
	<td class="title" colspan="2">STATUS</td>
 	<td></td>
</tr>
<tr class="tarjaItens">
	<td><img class="headerimg" src="images/bt_publicar.gif" width="32" height="32" alt="   publicar   "></td>
	<td><img class="headerimg" src="images/bt_remover.gif" width="32" height="32" alt="   excluir provisoriamente   "></td>
	<td><img class="headerimg" src="images/bt_editar.gif" width="32" height="32" alt="   editar   "></td>
</tr>
<tr class="fioItens"><td width="1%"><img src="images/blank.gif" width="200" height="1"></td><td colspan="5"></td></tr>
<?
escrevePaginasIndividuais();
?>
<tr class="fioTitulo"><td colspan="6"></td></tr>
<tr>
	<td colspan="6"><a href="#" onclick="openInputNMPGIFolders();return false" onfocus="noFocus(this)"><img src="images/bt_editfolders.gif" width="20" height="20" vspace="5" align="absmiddle"><span>&nbsp;Editar nome das pastas</span></a></td>
</tr>
<tr class="fioTitulo"><td colspan="6"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="1" height="40"></td></tr>
<tr><td align="center" style="font-family:tahoma,arial,helvetica,sans-serif;font-size:11px;color:#000;white-space:nowrap"><input type="button" class="buttonsty" value="Atualizar Status" onclick="submitPage('/admin/updatepgistatus.php','')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttondelsty" value="" onclick="delRegisters()" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'">Excluir definitivamente<img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"><br><br><br></td></tr>
</table>
</form>
</div>
<script type="text/javascript"><? echo "folderlist = '" . $folderlist . "';" ?></script>
</body>
</html>
<?
mysql_close();
?>