<?php
//Includes
//if ($_REQUEST["hdnCiclo"] == -1)
//{
//	header("Location: relatorio_acesso_consolidado_ciclo.php?{$_SERVER['QUERY_STRING']}");
//	exit();
//}
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include('relatorio_acesso_util.php');
include('relatorio_ranking_util.php');

$notaCentesimal = false;
$assessment = true;

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

$tituloTotal = "";
$tituloRelatorio = "Gest�o de Evolu��o do Programa";


//Obter parametros
$POST = obterPost();
$codigoEmpresa = $POST['cboEmpresa'];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = $POST['cboCargo'];
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$codigoCiclo = $_REQUEST["hdnCiclo"];
$nomeCiclo = obterNomeCiclo($codigoCiclo);
$localidade = $_REQUEST['cboLocalidade'];


$usaData = true;

if ($codigoEmpresa == 34)
{
	$labelAvaliacao = "Certifica��es";
}

if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}


if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}

$hoje = date("d/m/Y");
if ($dataFinal=="" || conveterDataParaYMD($dataFinal) > conveterDataParaYMD($hoje)) {
	$dataFinal = $hoje;
}

$dataAtual = $dataInicial;
// Fim obter parametros

//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa

//Acertar as p�ginas a serem exibidas
$paginaRn = new col_pagina_acesso();
$paginaRn->prepararRelatorios($dataInicial, $dataFinal, $codigoEmpresa);

if ($_POST["btnExcel"] != "") {
	header("Content-Type: application/vnd.ms-excel; name='excel'");
	header("Content-disposition:  attachment; filename=RadarConsolidado.xls");
}

$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

//Obter o total de participantes do filtro
$sqlTotalGeral = "SELECT
		  COUNT(DISTINCT a.CD_USUARIO) AS QD_TOTAL
		FROM
		  col_acesso_diario a
		  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
		  LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
		WHERE
		  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
		  
		  AND (u.Status = 1)
		  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		  
		  AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
		  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";

$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);

$linha = mysql_fetch_row($resultadoTotalGeral);

$totalGeralTodos = $linha[0];

ob_start();
	
?>
<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	<?php
	
	if ($_POST["btnExcel"] == "") {
	?>
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">		
	<?php
	}
	?>
	<style type="text/css">
		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		.titRelatD
		{
			text-align: left;
		}
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		.bdEsq
		{
			border-left: 1px solid #000000;
		}
		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		.bdTop
		{
			border-top: 1px solid #000000;
		}
		body{overflow:auto}
	</style>
</head>
<body>
<br>
<?php
	$colSpan = 0;
	$colunasMaximo = 1000;
	$colunasAparecer = 0;
	$colunasTotal = 0;
	
	if ($usaData)
	{
		$dataAtualOriginal = $dataAtual;
		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal))) {
			if ($colunasAparecer < $colunasMaximo)
			{
				$colSpan++;
				$colunasAparecer++;
			}
			
			$colunasTotal++;
			$dataAtual = somarUmDia($dataAtual);
		}
		$dataAtual = $dataAtualOriginal;
		
	}
	
	$colunaInicial = 1;
	if ($colunasTotal > $colunasAparecer)
	{
		$colunaInicial = $colunasTotal - $colunasAparecer + 1;
	}
?>
<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk' nowrap>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" colspan="<?php echo $colSpan + 1; ?>" align="center">
				<b><?php echo $tituloRelatorio; ?></b>
		</td>
		<td style="text-align: right" class='textblk'>
			<p><?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?></p>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<?php
		if ($POST["nomeFiltro"] != "")
		{
			linhaCabecalho("<b>Nome do Grupo Gerencial","{$POST["nomeFiltro"]}</b>");
		}
		
		linhaCabecalho("<b>Ciclo","$nomeCiclo</b>");
	?>
	<tr class="textblk">
		<td width="100%" colspan="<?php echo $colSpan + 3; ?>">
			<b>Per�odo:&nbsp; <?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
		</b></td>
	</tr>
	<?php 
	if ($codigoLotacao > -1) {
	?>
	<tr class="textblk">
		<td width="100%" colspan="<?php echo $colSpan + 3; ?>">
			<b>Grupo de Gest�o: <?php echo $POST["nomeLotacao"];?></b>
		</td>
	</tr>
	<?php 
	}
	?>
	<?php 
	if ($cargo > "-1") {
	?>
	<tr class="textblk">
		<td width="100%" colspan="<?php echo $colSpan + 3; ?>">
			<b>Cargo: <?php echo $cargo;?></b>
		</td>
	</tr>
	<?php 
	}
	?>
	
</table>
<table cellpadding="1" cellspacing="0" style="border: 1px solid black;" align="center" width="95%">
	<tr class='textblk'>
		<td  rowspan="2" class="bdLat" width="100%" colspan="2">
			<b>Ades�o</b>
		</td>
<?php
	$dataAtualOriginal = $dataAtual;
	
	$trDiasMeses = "";
	
	if ($usaData)
	{
		$meses = array();
		$contador = 1;
		
		//Define os meses envolvidos e a quantidade de dias de cada um at� a data limite
		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal))) {
			if (!($colunaInicial > $contador))
			{
				$aData = split("/", $dataAtual);
				$mesAtual = $aData[1];
				if ($meses[$mesAtual] == null)
				{
					$meses[$mesAtual] = 0;
				}
				$meses[$mesAtual] = $meses[$mesAtual] + 1;
				
				$trDiasMeses =  "$trDiasMeses
									<td class='titRelat' >
										{$aData[0]}
									</td>";
			}
			$dataAtual = somarUmDia($dataAtual);
			$contador++;
			
		}
	
	
		foreach($meses as $chave => $valor)
		{
			$nomeMes = obterTextoMes($chave);
			echo "	<td class='bdLat' colspan=\"$valor\" align=\"center\">
						$nomeMes
					</td>";
		}
	
	}
	
?>
		<td align="center" rowspan="2">
			Total
		</td>
	</tr>
	<tr class='textblk'>
<?php
	if ($usaData)
	{
		//Percorre os dias at� a data limite
		echo $trDiasMeses;
		
	}
	$totalUsuariosAcumuladoDia = obterTotaisUsuariosDia();
?>
	</tr>
<?php
	$contador = 1;
	$dataAtual = $dataAtualOriginal;
	
	$totalAcumulado = 0;
	$totalNaoParticipantes = 0;
	$totalCadastrado = 0;
	$totalParticipacao = 0;
	$totalElegiveis = 0;
	$totalNaoElegiveis = 0;
	$totalElegibilidade = 0;
	$totalCadastradoElegivel = 0;
	$totalParticipanteSuperusuario = 0;
	$jaContou = false;
	
	$trCadastrados = "";
	$trParticipantes = "";
	$trNaoParticipantes = "";
	$trParticipacao = "";
	$trElegiveis = "";
	$trNaoElegiveis = "";
	$trElegibilidade = "";
	$trCadastradosElegiveis = "";
	$trParticipanteSuperusuario = "";
	
	$elegiveisDiariosAcumulado = array();
	
	while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))
	{
		if (!($colunaInicial > $contador))
		{
			
			if ($jaContou == false)
			{
				$jaContou = true;
				
					$dataAtualConvertida = conveterDataParaYMD($dataAtual);
	
					foreach($totalUsuariosAcumuladoDia as $chave => $valor)
					{
						
						if ($dataAtualConvertida > $chave)
						{
							$totalAcumulado = $totalAcumulado + $valor["QTD_PARTICIPANTE"];
							$totalCadastrado = $totalCadastrado + $valor["QTD_CADASTRADO"];
							$totalElegiveis = $totalElegiveis + $valor["QTD_ELEGIVEL"];
							$totalCadastradoElegivel = $totalCadastradoElegivel + $valor["QTD_CADASTRADO_ELEGIVEL"];
							$totalParticipanteSuperusuario = $totalParticipanteSuperusuario + $valor["QTD_PARTICIPANTE_SUPERUSUARIO"];
						}
					
					}
				
			}
			
			if ($totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)] == null)
			{
				$totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_PARTICIPANTE"] = 0;
				$totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_CADASTRADO"] = 0;
				$totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_ELEGIVEL"] = 0;
				$totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_CADASTRADO_ELEGIVEL"] = 0;
				$totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_PARTICIPANTE_SUPERUSUARIO"] = 0;
			}
			
			$totalAcumulado = $totalAcumulado + $totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_PARTICIPANTE"];
			$totalCadastrado = $totalCadastrado + $totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_CADASTRADO"];
			$totalElegiveis = $totalElegiveis + $totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_ELEGIVEL"];
			$totalCadastradoElegivel = $totalCadastradoElegivel + $totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_CADASTRADO_ELEGIVEL"];
			$totalParticipanteSuperusuario = $totalParticipanteSuperusuario + $totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)]["QTD_PARTICIPANTE_SUPERUSUARIO"];
			
			$elegiveisDiariosAcumulado[conveterDataParaYMD($dataAtual)] = $totalElegiveis;
			$trParticipantes = "$trParticipantes
									<td class='titRelat'>
										$totalAcumulado
									</td>";
			
			$trCadastrados = "$trCadastrados
								<td class='titRelat'>
									$totalCadastrado
								</td>";				
			
			$trElegiveis = "$trElegiveis
								<td class='titRelat'>
									$totalElegiveis
								</td>";	
			
			
			$trParticipanteSuperusuario = "$trParticipanteSuperusuario
											<td class='titRelat'>
												$totalParticipanteSuperusuario
											</td>";
									
			$totalNaoParticipantes = $totalCadastrado - $totalAcumulado;
			
			$trNaoParticipantes = "$trNaoParticipantes
									<td class='titRelat'>
						
										$totalNaoParticipantes
						
									</td>";
										
			$totalParticipacao = $totalAcumulado/$totalCadastrado*100;
			$totalParticipacao = number_format(round($totalParticipacao, 2),2,",",".") . "%";
										
			$trParticipacao = "$trParticipacao
									<td class='titRelat'>
										$totalParticipacao
									</td>";
										
			$totalNaoElegiveis = $totalAcumulado - $totalElegiveis;
			$trNaoElegiveis = "$trNaoElegiveis
									<td class='titRelat'>
						
										$totalNaoElegiveis
						
									</td>";
										
			$totalElegibilidade = $totalElegiveis/$totalCadastrado*100;
			$totalElegibilidade = number_format(round($totalElegibilidade, 2),2,",",".") . "%";
			$trElegibilidade = "$trElegibilidade
									<td class='titRelat'>
										$totalElegibilidade
									</td>";
										
										
			$trCadastradosElegiveis = "$trCadastradosElegiveis
						<td class='titRelat'>
							$totalCadastradoElegivel
						</td>";
		}
		$contador++;			
		$dataAtual = somarUmDia($dataAtual);
	}
	$trCadastrados = "$trCadastrados
								<td class='titRelatTop'>
									$totalCadastrado
								</td>";				
	
	$trParticipantes = "$trParticipantes
							<td class='titRelatTop'>
								$totalAcumulado
							</td>";
	
	$trNaoParticipantes = "$trNaoParticipantes
									<td class='titRelatTop'>
						
										$totalNaoParticipantes
						
									</td>";
										
	$trParticipacao = "$trParticipacao
									<td class='titRelatTop'>
										$totalParticipacao
									</td>";
										
	$trElegiveis = "$trElegiveis
									<td class='titRelatTop'>
										$totalElegiveis
									</td>";
										
	
	$trNaoElegiveis = "$trNaoElegiveis
									<td class='titRelatTop'>
										$totalNaoElegiveis
									</td>";
										
	$trElegibilidade = "$trElegibilidade
									<td class='titRelatTop'>
										$totalElegibilidade
									</td>";
										
	$trCadastradosElegiveis = "$trCadastradosElegiveis
									<td class='titRelatTop'>
										$totalCadastradoElegivel
									</td>";
										
										
	$trParticipanteSuperusuario = "$trParticipanteSuperusuario
											<td class='titRelatTop'>
												$totalParticipanteSuperusuario
											</td>";
	
?>	
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Cadastrados
		</td>
		<?php
			echo $trCadastrados;
		?>
	</tr>
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Participantes
		</td>
		<?php
			echo $trParticipantes;
		?>
	</tr>
	<?php 
	if ($codigoEmpresa == 654)
	{
	?>
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			N�o Participantes
		</td>
		<?php
			echo $trNaoParticipantes;
		?>
	</tr>
	<?php
	}
	else
	{
		if(!$assessment){
			
		
	?>
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Eleg�veis
		</td>
		<?php
			echo $trCadastradosElegiveis;
		?>
	</tr>
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Participantes&nbsp;Eleg�veis&nbsp;a&nbsp;Certifica��o	
		</td>
		<?php
			echo $trElegiveis;
		?>
	</tr>
	<?php 
		}
	?>
		<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Gerentes Participantes	
		</td>
		<?php
			echo $trParticipanteSuperusuario;
		?>
	</tr>
	<?php 
	}
	?>
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Participantes/Cadastrados&nbsp;(%)
		</td>
		
		<?php
			echo $trParticipacao;
		?>
		
	
	</tr>
	<?php 
	if ($codigoEmpresa == 34)
	{
	?>
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Participantes&nbsp;Eleg�veis&nbsp;a&nbsp;Certifica��o/Cadastrados&nbsp;(%)	
		</td>
		
		<?php
			echo $trElegibilidade;
		?>
		
	
	</tr>
	<?php 
	}
	?>
	
	<tr>
		<td class='titRelatTop' colspan="<?php echo $colunasAparecer + 3; ?>">&nbsp;</td>
	</tr>
	
	<tr>

<?php



	$totalizadorDias = array();

	$totalGeral = 0;

	$acessoUsuarios = array();
	
	$acessosDia = array();

	$sqlTotalDia = "SELECT

			  DATE_FORMAT(DT_ACESSO, '%Y%m%d') AS DT_ACESSO,

			  COUNT(DISTINCT a.CD_USUARIO) AS QD_TOTAL

			FROM

			  col_acesso_diario a

			  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
			  LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao

			WHERE

			  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)

			  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')

			  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
			  
			  AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')

			  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))

			  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')

			  GROUP BY

			  	DATE_FORMAT(DT_ACESSO, '%Y%m%d')";

	

	$resultadoTotalDiario = DaoEngine::getInstance()->executeQuery($sqlTotalDia,true);

	while ($linha = mysql_fetch_row($resultadoTotalDiario)) {

			//echo "aaa";

		$acessosDia[$linha[0]] = $linha[1];

	}

	$colspanTitulo = $colunasAparecer + 3;

echo "	<tr class='textblk'>

		<td class='bdTop' width='100%' colspan='$colspanTitulo'>

			<b>Acessos</b>

		</td>

	</tr>";
	
echo "<tr class='textblk'>

			<td class='titRelatD' colspan='2'>

				Acessos (Di�rio)

			</td>

			";

	

	if ($usaData)

	{

		

		$contador = 1;

		$dataAtual = $dataAtualOriginal;

		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))

		{

			if (!($colunaInicial > $contador))

			{

				if ($acessosDia[conveterDataParaYMD($dataAtual)] == null)

					$acessosDia[conveterDataParaYMD($dataAtual)] = 0;

				

				echo "<td class='titRelat'>

						{$acessosDia[conveterDataParaYMD($dataAtual)]}

				</td>";

			}

			$contador++;			

			$dataAtual = somarUmDia($dataAtual);

		}

	}

	

	if ($totalGeralTodos == "")

		$totalGeralTodos = 0;

	

	echo "<td class='titRelatTop'>

			$totalGeralTodos

		</td>";

	

	echo "</tr>";
	
	
	if(!$assessment){
		
	
		$nomesPaginas = obterNomePaginas();
		
		$acessosPorDia = obterTotalAcessosDia();
		
		foreach($nomesPaginas as $chave => $valor)
		{
			
			
			$paginaAcesso = $valor;
			$indiceAcesso = $chave;
			
			if ($indiceAcesso == 1 || $indiceAcesso == 3) {
				continue;
			}
				
			
			echo "<tr class='textblk'>";
	
			echo "<td class='titRelatD' colspan='2'>$paginaAcesso</td>";
	
			$dataAtual = $dataAtualOriginal;
			$totalPagina = 0;
			
			$contador = 1;
			
			while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))
			{
	
	
	
				$aData = split("/", $dataAtual);
	
				$indice = "$indiceAcesso{$aData[2]}{$aData[1]}{$aData[0]}";
				
				$acessosPagina = 0;
				
				if ($acessosPorDia[$indice] != null){
	
					$acessosPagina = $acessosPorDia[$indice];
	
				}
	
				$totalPagina = $totalPagina + $acessosPagina;
				
				if (!($colunaInicial > $contador))
	
				{
	
					if ($usaData)
	
					{
	
						echo "<td class='titRelat'>
	
								$acessosPagina
	
							</td>";
	
					}
	
				}
	
				$dataAtual = somarUmDia($dataAtual);
				$contador++;
			
			}
			
				echo "<td class='titRelatTop'>
	
					$totalPagina
	
				</td>";	
	
		echo "</tr>";
		}
	
	if ($codigoEmpresa == 36)
		$labelAvaliacao = "Diagn�sticos de Conhecimento";
?>

	<tr>
		<td class='titRelatTop' colspan="<?php echo $colunasAparecer + 3; ?>">&nbsp;</td>
	</tr>
		
	
	<tr class='textblk'>

		<td class="bdTop" width="100%" colspan="<?php echo $colunasAparecer + 3; ?>">
			
			<b><?php echo $labelAvaliacao; ?></b>
			
		</td>

	</tr>
	

	
	


<?php
	
	$mediaGeralProvasCalculada = 0;
	$quantidadeTotalAvaliacoesMaisSegundaChamada = 0;
	$quantidadeTotalAvaliacoeAcimaSeteSegundaChamada = 0;

	$mediaProvas = obterProvasDia("A");
	$mediaGeralProvas = obterProvasMediasGeral("A");
	$mediaProvasGeral = obterProvasMediaCiclo("A");
	
	$quantidadeTotalAvaliacoes = 0;
	$quantidadeTotalAvaliacoeAcimaSete = 0;
	
	
	
	$contador = 1;

	$dataAtual = $dataAtualOriginal;
	
	$trAvaliacoes = "";
	$trQuantidadeAvaliacoes = "";
	$trQuantidadeAvaliacoesAcimaSete = "";
	$trElegibilidadeAvaliacao = "";
	$trQuantidadeAprovacoes = "";
	$trQuantidadeAvaliacoesAcimaNove = "";
	$trPercentualAvaliacoesAcimaNove = "";
	
	while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))
	{
		$indiceAvaliacao = conveterDataParaYMD($dataAtual);
		
		if ($mediaProvas[$indiceAvaliacao] != null)
		{
			$quantidadeTotalAvaliacoes = $quantidadeTotalAvaliacoes + $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"];
			$quantidadeTotalAvaliacoeAcimaSete = $quantidadeTotalAvaliacoeAcimaSete + $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"];
		}
		
		if (!($colunaInicial > $contador))
		{

			$percentualAprovacao = "-";
			$percentualLouvour = "-";
			if ($mediaProvas[$indiceAvaliacao] == null)
			{
				$mediaProvas[$indiceAvaliacao]["VL_MEDIA"] = "-";
				$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"] = "-";
				$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"] = "-";
				$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_NOVE"] = "-";
			}
			else
			{
				$percentualAprovacao = $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"] / $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"] * 100;
				$percentualAprovacao = number_format(round($percentualAprovacao, 2),2,",",".") . "%";
				
				$percentualLouvour = $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_NOVE"] / $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"] * 100;
				$percentualLouvour = number_format(round($percentualAprovacao, 2),2,",",".") . "%";
			}
				
				
			$trQuantidadeAvaliacoes = "$trQuantidadeAvaliacoes <td class='titRelat'>

				{$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"]}

			</td>";
			
			
			$trQuantidadeAprovacoes = "$trQuantidadeAprovacoes <td class='titRelat'>

				{$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"]}

			</td>";
				
			$trQuantidadeAvaliacoesAcimaSete = "$trQuantidadeAvaliacoesAcimaSete <td class='titRelat'>

				$percentualAprovacao

			</td>";
				
			$trAvaliacoes = "$trAvaliacoes <td class='titRelat'>

				{$mediaProvas[$indiceAvaliacao]["VL_MEDIA"]}

			</td>";
				
			$trQuantidadeAvaliacoesAcimaNove = "$trQuantidadeAvaliacoesAcimaNove <td class='titRelat'>

				{$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_NOVE"]}

			</td>";
				
			$trPercentualAvaliacoesAcimaNove = "$trPercentualAvaliacoesAcimaNove <td class='titRelat'>

				$percentualLouvour

			</td>";
				
			$percentualElegiveisAvaliacao = $quantidadeTotalAvaliacoes / $elegiveisDiariosAcumulado[$indiceAvaliacao] * 100;
			if ($percentualElegiveisAvaliacao > 0)
				$percentualElegiveisAvaliacao = number_format(round($percentualElegiveisAvaliacao, 2),2,",",".") . "%";
			else
				$percentualElegiveisAvaliacao = "-";
				
			$trElegibilidadeAvaliacao = "$trElegibilidadeAvaliacao <td class='titRelat'>

				$percentualElegiveisAvaliacao

			</td>";
			
			
		}

		$contador++;			

		$dataAtual = somarUmDia($dataAtual);

	}

	if ($mediaGeralProvas["1"] == null)
	{
		$mediaGeralProvasCalculada ="-";
		$mediaGeralProvas["1"] = "-";
	}
	
	$trAvaliacoes = "$trAvaliacoes <td class='titRelatTop'>

		{$mediaGeralProvasCalculada}

			</td>";
		
	$trQuantidadeAvaliacoes = "$trQuantidadeAvaliacoes <td class='titRelatTop'>

		$quantidadeTotalAvaliacoesMaisSegundaChamada

			</td>";
		
	$trQuantidadeAprovacoes = "$trQuantidadeAprovacoes <td class='titRelatTop'>
	
		$quantidadeTotalAvaliacoeAcimaSeteSegundaChamada	
	
			</td>";
	
	//$percentualAprovacao = $quantidadeTotalAvaliacoeAcimaSete / $quantidadeTotalAvaliacoes;
	$percentualAprovacao = $quantidadeTotalAvaliacoeAcimaSeteSegundaChamada / $quantidadeTotalAvaliacoesMaisSegundaChamada * 100;
	$percentualAprovacao = number_format(round($percentualAprovacao, 2),2,",",".") . "%";
	
	
	
	$trQuantidadeAvaliacoesAcimaSete = "$trQuantidadeAvaliacoesAcimaSete <td class='titRelatTop'>

		$percentualAprovacao

			</td>";

		
	$percentualElegiveisAvaliacao = $quantidadeTotalAvaliacoesMaisSegundaChamada / $totalElegiveis * 100;
	$percentualElegiveisAvaliacao = number_format(round($percentualElegiveisAvaliacao, 2),2,",",".") . "%";
	
	$trElegibilidadeAvaliacao = "$trElegibilidadeAvaliacao <td class='titRelatTop'>

		$percentualElegiveisAvaliacao

			</td>";
	
?>

	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			Qtde de <?php echo $labelAvaliacao; ?>
		</td>	

	<?php echo $trQuantidadeAvaliacoes;?>
	
	</tr>
	
	<?php 
	if (1==2) {
	?>
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			Qtde de Aprova��es <?php echo $trQuantidadeAprovacoes;?>
		</td>
	</tr>
	<?php 
	}
	if ($codigoEmpresa == 34)
	{
	?>
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			Qtde&nbsp;de&nbsp;<?php echo $labelAvaliacao; ?>/Participantes&nbsp;Eleg�veis&nbsp;(%)
		</td>	

	<?php echo $trElegibilidadeAvaliacao;?>
	
	</tr>
	<?php 
	}
	if (1==2) {
	?>
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			% Aprova��o
		</td>	

	<?php echo $trQuantidadeAvaliacoesAcimaSete;?>
	
	</tr>
	<?php 
	}
	?>

	<tr class="textblk">
		<td colspan="2" class="titRelatD" nowrap>
			M�dia dos <?php echo $labelAvaliacao; //das ?>
		</td>	

	<?php echo $trAvaliacoes;?>
	
	</tr>
<?php 

	}
?>
	<tr>
		<td class='titRelatTop' colspan="<?php echo $colunasAparecer + 3; ?>">&nbsp;</td>
	</tr>
		
	
	<tr class='textblk'>
		<td class="bdTop" width="100%" colspan="<?php echo $colunasAparecer + 3; ?>">
			<b><?php echo "Testes"; ?></b>
		</td>
	</tr>
	<?php //Inicia a parte de c�digo nova?>
	<?php
	$mediaGeralProvasCalculada = 0;
	$quantidadeTotalAvaliacoesMaisSegundaChamada = 0;
	$quantidadeTotalAvaliacoeAcimaSeteSegundaChamada = 0;
	$quantidadeParticipanteAvaliacao = 0;
	$quantidadeTotalAvaliacoeAcimaNoveSegundaChamada = 0;
	
	$mediaProvas = obterProvasDia("P");
	$mediaGeralProvas = obterProvasMediasGeral("P");
	$mediaProvasGeral = obterProvasMediaCiclo("P");
	
	$quantidadeTotalAvaliacoes = 0;
	$quantidadeTotalAvaliacoeAcimaSete = 0;
	$quantidadeTotalParticipantesAvaliacoes = 0;
	
	
	$contador = 1;
	$dataAtual = $dataAtualOriginal;
	
	$trAvaliacoes = "";
	$trQuantidadeAvaliacoes = "";
	$trQuantidadeAvaliacoesAcimaSete = "";
	$trElegibilidadeAvaliacao = "";
	$trQuantidadeAprovacoes = "";
	$trQuantidadeParticipantesAvaliacoes = "";
	$trQuantidadeAvaliacoesAcimaNove = "";
	$trPercentualAvaliacoesAcimaNove = "";
	
	while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))
	{
		$indiceAvaliacao = conveterDataParaYMD($dataAtual);
		
		if ($mediaProvas[$indiceAvaliacao] != null)
		{
			$quantidadeTotalAvaliacoes = $quantidadeTotalAvaliacoes + $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"];
			$quantidadeTotalAvaliacoeAcimaSete = $quantidadeTotalAvaliacoeAcimaSete + $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"];
			$quantidadeTotalParticipantesAvaliacoes = $quantidadeTotalParticipantesAvaliacoes + $mediaProvas[$indiceAvaliacao]["QTD_PARTICIPANTE_AVALIACAO"];
		}
		
		if (!($colunaInicial > $contador))
		{
			$percentualAprovacao = "-";
			if ($mediaProvas[$indiceAvaliacao] == null)
			{
				$mediaProvas[$indiceAvaliacao]["VL_MEDIA"] = "-";
				$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"] = "-";
				$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"] = "-";
				$mediaProvas[$indiceAvaliacao]["QTD_PARTICIPANTE_AVALIACAO"] = "-";
				$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_NOVE"] = "-";
			}
			else
			{
				$percentualAprovacao = $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"] / $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"] * 100;
				$percentualAprovacao = number_format(round($percentualAprovacao, 2),2,",",".") . "%";
				
				$percentualLouvour = $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_NOVE"] / $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"] * 100;
				$percentualLouvour = number_format(round($percentualLouvour, 2),2,",",".") . "%";
			}
				
				
			$trQuantidadeAvaliacoes = "$trQuantidadeAvaliacoes <td class='titRelat'>
				{$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"]}
			</td>";
			
			
			$trQuantidadeAprovacoes = "$trQuantidadeAprovacoes <td class='titRelat'>
				{$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"]}
			</td>";
				
			$trQuantidadeParticipantesAvaliacoes = "$trQuantidadeParticipantesAvaliacoes <td class='titRelat'>
				{$mediaProvas[$indiceAvaliacao]["QTD_PARTICIPANTE_AVALIACAO"]}
			</td>";
				
			$trQuantidadeAvaliacoesAcimaSete = "$trQuantidadeAvaliacoesAcimaSete <td class='titRelat'>
				$percentualAprovacao
			</td>";
				
			$trQuantidadeAvaliacoesAcimaNove = "$trQuantidadeAvaliacoesAcimaNove <td class='titRelat'>
				{$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_NOVE"]}
			</td>";
				
			$trAvaliacoes = "$trAvaliacoes <td class='titRelat'>
				{$mediaProvas[$indiceAvaliacao]["VL_MEDIA"]}
			</td>";
				
			$trPercentualAvaliacoesAcimaNove = "$trPercentualAvaliacoesAcimaNove <td class='titRelat'>
				$percentualLouvour
			</td>";
				
			$percentualElegiveisAvaliacao = $quantidadeTotalAvaliacoes / $elegiveisDiariosAcumulado[$indiceAvaliacao] * 100;
			if ($percentualElegiveisAvaliacao > 0)
				$percentualElegiveisAvaliacao = number_format(round($percentualElegiveisAvaliacao, 2),2,",",".") . "%";
			else
				$percentualElegiveisAvaliacao = "-";
				
			$trElegibilidadeAvaliacao = "$trElegibilidadeAvaliacao <td class='titRelat'>
				$percentualElegiveisAvaliacao
			</td>";
			
			
		}
		$contador++;			
		$dataAtual = somarUmDia($dataAtual);
	}
	if ($mediaGeralProvas["1"] == null)
	{
		$mediaGeralProvasCalculada ="-";
		$mediaGeralProvas["1"] = "-";
	}
	
	$trAvaliacoes = "$trAvaliacoes <td class='titRelatTop'>
		{$mediaGeralProvasCalculada}
			</td>";
		
	$trQuantidadeParticipantesAvaliacoes = "$trQuantidadeParticipantesAvaliacoes <td class='titRelatTop'>
		$quantidadeParticipanteAvaliacao
			</td>";
		
	$trQuantidadeAvaliacoes = "$trQuantidadeAvaliacoes <td class='titRelatTop'>
		$quantidadeTotalAvaliacoesMaisSegundaChamada
			</td>";
		
	$trQuantidadeAprovacoes = "$trQuantidadeAprovacoes <td class='titRelatTop'>
	
		$quantidadeTotalAvaliacoeAcimaSeteSegundaChamada	
	
			</td>";
	
	$trQuantidadeAvaliacoesAcimaNove = "$trQuantidadeAvaliacoesAcimaNove <td class='titRelatTop'>
	
		$quantidadeTotalAvaliacoeAcimaNoveSegundaChamada	
	
			</td>";
	
		
	//$percentualAprovacao = $quantidadeTotalAvaliacoeAcimaSete / $quantidadeTotalAvaliacoes;
	$percentualAprovacao = $quantidadeTotalAvaliacoeAcimaSeteSegundaChamada / $quantidadeTotalAvaliacoesMaisSegundaChamada * 100;
	$percentualAprovacao = number_format(round($percentualAprovacao, 2),2,",",".") . "%";
	
	
	
	$trQuantidadeAvaliacoesAcimaSete = "$trQuantidadeAvaliacoesAcimaSete <td class='titRelatTop'>
		$percentualAprovacao
			</td>";
		
	$percentualElegiveisAvaliacao = $quantidadeTotalAvaliacoesMaisSegundaChamada / $totalElegiveis * 100;
	$percentualElegiveisAvaliacao = number_format(round($percentualElegiveisAvaliacao, 2),2,",",".") . "%";
	
	$trElegibilidadeAvaliacao = "$trElegibilidadeAvaliacao <td class='titRelatTop'>
		$percentualElegiveisAvaliacao
			</td>";
	
		
		$labelAvaliacao = "Avalia��es";
		
		//$percentualAprovacao = $quantidadeTotalAvaliacoeAcimaSete / $quantidadeTotalAvaliacoes;
	$percentualLouvour = $quantidadeTotalAvaliacoeAcimaNoveSegundaChamada / $quantidadeTotalAvaliacoesMaisSegundaChamada * 100;
	$percentualLouvour = number_format(round($percentualLouvour, 2),2,",",".") . "%";
	
	
	
	$trPercentualAvaliacoesAcimaNove = "$trPercentualAvaliacoesAcimaNove <td class='titRelatTop'>
		$percentualLouvour
			</td>";
		
?>
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			Quantidade <?php //de <?php echo $labelAvaliacao; ?>
		</td>	
	<?php echo $trQuantidadeAvaliacoes;?>
	</tr>
	<?php if (!$assessment){?>
		<tr class="textblk">
			<td colspan="2" class="titRelatD">
				Qtde de Participantes nas <?php echo $labelAvaliacao; ?>
			</td>	
		<?php echo $trQuantidadeParticipantesAvaliacoes;?>
		</tr>
	<?php
	}
	if (1==1) {
	?>
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			M�dia <?php //das <?php echo $labelAvaliacao; //das ?>
		</td>	
	<?php echo $trAvaliacoes;?>
	</tr>
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			Qtde de M�dias > 70 <?php echo $trQuantidadeAprovacoes;?>
		</td>
	</tr>
	<?php 
	}
	if (!$assessment){
		if ($codigoEmpresa == 34)
		{
		?>
		<tr class="textblk">
			<td colspan="2" class="titRelatD">
				Qtde&nbsp;de&nbsp;<?php echo $labelAvaliacao; ?>/Participantes&nbsp;Eleg�veis&nbsp;(%)
			</td>	
		<?php echo $trElegibilidadeAvaliacao;?>
		</tr>
		<?php 
		}
	}
	if (1==1) {
	?>
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			% de Aprova��o > 70
		</td>	
	<?php echo $trQuantidadeAvaliacoesAcimaSete;?>
	</tr>
	<?php 
	}
	
	?>
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			Qtde de M�dias > 90 <?php echo $trQuantidadeAvaliacoesAcimaNove;?>
		</td>
	</tr>
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			% de Aprova��o > 90 <?php echo $trPercentualAvaliacoesAcimaNove;?>
		</td>
	</tr>
</table>
	<?php
		if ($_POST["btnExcel"] == "") {
	?>
	<div style="width:100%;text-align:center">
		<br />
		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']; ?>">
		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">
		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
		</form>	
	</div>
	<?php
		}
	?>
</body>
</html>
<?php
	ob_end_flush();
	function obterTextoMes($numero)
	{
		switch ($numero)
		{
			case 1:
				return "Janeiro";
				break;
			case 2:
				return "Fevereiro";
				break;
			case 3:
				return "Mar�o";
				break;
			case 4:
				return "Abril";
				break;
			case 5:
				return "Maio";
				break;
			case 6:
				return "Junho";
				break;
			case 7:
				return "Julho";
				break;
			case 8:
				return "Agosto";
				break;
			case 9:
				return "Setembro";
				break;
			case 10:
				return "Outubro";
				break;
			case 11:
				return "Novembro";
				break;
			case 12:
				return "Dezembro";
				break;
		}
	}
	
	
	function obterTextoDiaSemana($numero)
	{
		switch ($numero) {
			case 0:
				return 'dom';
				break;
			case 1:
				return 'seg';
				break;
			case 2:
				return 'ter';
				break;
			case 3:
				return 'qua';
				break;
			case 4:
				return 'qui';
				break;
			case 5:
				return 'sex';
				break;
			case 6:
				return 'sab';
				break;
		}
		
	}
	function linhaCabecalho($titulo, $valor)
	{
		
		global $colSpan;
		
		$colunasCabecalhoTitulo = "";
		$colunasCabecalhoValor = "";
		
		if ($_POST["btnExcel"] != "")
		{
			$colunasCabecalhoValor = $colSpan + 2;
			$colunasCabecalhoValor = "</td><td align=\"left\" width=\"100%\" colspan=\"$colunasCabecalhoValor\">" ;
		}
		else 
		{
			$colunasCabecalhoTitulo = $colSpan + 3;
			$colunasCabecalhoTitulo = "colspan=\"$colunasCabecalhoTitulo\"";
		}
		
		
		echo "<tr class=\"textblk\">
				<td $colunasCabecalhoTitulo>
					$titulo:
				$colunasCabecalhoValor
					$valor
				</td>
			</tr>";
		
	}
	
?>