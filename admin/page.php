<?php

session_start();

page::$usuario = $_SESSION["cd_usu"];

class page{
	
	static $rn;
	static $isPostBack;
	static $validador = "javascript:return VerificarPreenchimentoObrigatorioTag();";
	static $script = "";
	static $atributos = "";
	static $exibeBotoes = true;
	static $enctype = "";
    static $complementoBotaoSalvar = "";
	
	public static function formataPropriedade($valor, $tipoPropriedade){
		switch ($tipoPropriedade){
			case DaoEngine::DATA:
				$valor = strftime("%d/%m/%Y", strtotime($valor));
				break;
				
			case DaoEngine::DATA_HORA_MINUTO:
				$valor = strftime("%H:%M", strtotime($valor));
				break;
			
		}
		
		return $valor;
		
	}
	
	
	static $usuario;
	
}

?>