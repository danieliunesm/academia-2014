<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Treinamento em Telecom</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="JavaScript">
function validarFormulario(){
	if (document.cadastroForm.userlogin.value == "")	{
		alert('   Preencha o nome do usu�rio!   ');
		return false;
	}
	if (document.cadastroForm.userpass1.value.length < 6){
		alert('   Preencha uma senha com no m�nimo 6 caracteres!   ');
		return false;
	}
	if (document.cadastroForm.userpass1.value != document.cadastroForm.userpass2.value){
		alert('   Preencha a confirma��o de senha corretamente!   ');
		return false;
	}
	if (document.cadastroForm.useraccess.value == 0){
		alert('   Selecione um tipo de usu�rio!   ');
		return false;
	}	
	document.cadastroForm.submit();
}
</script>
</head>
<body onload="document.cadastroForm.username.focus()">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><span class="title">USU�RIO:&nbsp;</span><?php echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_usuarios.php'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="11"></td></tr>
<tr>
<td colspan="3" align="center" valign="bottom">
<?
$erro = empty($_GET["erro"])?"":$_GET["erro"];

if	  ($erro==1)  echo "<font class=\"alert\">ERRO: Os campos obrigat�rios n�o foram preenchidos.<br><br></font>";
elseif($erro==2)  echo "<font class=\"alert\">ERRO: Senhas n�o coincidem. Digite novamente.<br><br></font>";
elseif($erro==3)  echo "<font class=\"alert\">ERRO: Usu�rio j� existente. Digite novos dados.<br><br></font>";
elseif($erro==-1) echo "<font class=\"alert\">O cadastro foi realizado com sucesso!<br><br></font>";
else 			  echo "<img src=\"/images/layout/blank.gif\" width=\"280\" height=\"28\">";
?>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<form name="cadastroForm" action="admin_cadastro.php" method="post">
<tr class="tarjaTitulo">
	<td height="20" colspan="2" align="center">CADASTRAMENTO DE USU�RIO</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="289" height="20"></td></tr>
<tr>
	<td class="title" align="right" width="1%">Nome&nbsp;completo:&nbsp;</td>
	<td><input name="username" type="text" class="textbox3" maxlength="50" tabIndex="1"></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
	<td class="title" align="right" width="1%">Login:&nbsp;</td>
	<td><input name="userlogin" type="text" class="textbox2" maxlength="8" tabIndex="2"></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
	<td class="title" align="right" width="1%">Senha:&nbsp;</td>
	<td><input name="userpass1" type="password" class="textbox2" maxlength="8" tabIndex="3"></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
	<td class="title" align="right" width="1%">Confirmar&nbsp;Senha:&nbsp;</td>
	<td><input name="userpass2" type="password" class="textbox2" maxlength="8" tabIndex="4"></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
	<td class="title" align="right" width="1%">Tipo usu�rio:&nbsp;</td>
	<td><select name="useraccess" tabIndex="5"><option value="0" selected>Selecione o tipo de usu�rio</option>
<?
$sql = "SELECT ID, NM_TIPO_USU FROM tb_tipo_usuario";
$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

while($oRs = mysql_fetch_row($RS_query))
{
	//if($oRs[0] >= 4)
	echo "<option value=\"" . $oRs[0] . "\">" . $oRs[1] . "</option>";
}
?>
	</select></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>
<tr><td></td><td><input type="button" class="buttonsty" value="Cadastrar" onclick="validarFormulario()" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
</form>
</table>
<?php
mysql_free_result($RS_query);
mysql_close();
?>
</body>
</html>
