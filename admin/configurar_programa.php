<?php


//error_reporting(E_ALL);
//ini_set("display_errors", 1);

include("../include/defines.php");
include('framework/crud.php');
include('controles.php');
include('datagrid.php');

function instanciarRN(){
	return new col_cliente();
}

$paginaEdicao = "copiar_programa.php";
$paginaEdicaoAltura = 300;

define(TITULO_PAGINA, "Configurar Programas");
define(BOTAO_ADICIONAR, "Incluir Programas");
define(BOTAO_MODO_EXIBICAO, "Exibi��o das Programas");

include "../include/security.php";
include "../include/genericfunctions.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Colabor� - Consultoria e Educa&ccedil;&atilde;o Corporativa</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META http-equiv="pragma" content="no-cache">
		<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">
		<script type="text/javascript" src="include/js/functions.js"></script>
		<script language="javascript">

            function ShowDialog(pagePath, args, width, height, left, top){
                return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
            }
            
             var oVsession = "";
             var inativos = 0;
        
		     function viewControl(vSession){
                oVsession = vSession;
                if (document.getElementById("hdnRemovidos").value != ""){
                    inativos = document.getElementById("hdnRemovidos").value;
                }
                
                var html = ShowDialog("modoVisualizacao.htm", window, 420, 210);
                if(html!=null){
                    document.getElementById("hdnRemovidos").value = html;
                    document.forms[0].submit();
                }
            }
        
			var paginaEdicao = '<?php echo $paginaEdicao; ?>';
			
			<?php
				if (!isset($paginaEdicaoLargura)){
					$paginaEdicaoLargura = 450;
				}
				
				if (!isset($paginaEdicaoAltura)){
					$paginaEdicaoAltura = 280;
				}
			?>
		
			function noFocus(obj){if(obj.blur())obj.blur()}

			function abrirComentario(id) {
				newWin=null;
				var w=(screen.width-40);
				var h=(screen.height-80);
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;

				var complemento = "";
				if (id != '')
					complemento = "?id=" + id
				
				newWin=window.open("cadastro_comentario.php" + complemento,'menueditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
				if(newWin!=null)setTimeout('newWin.focus()',100);
			}

            function abrirCursos(id) {
                newWin=null;
                var w=(screen.width-40);
                var h=(screen.height-80);
                var l=(screen.width-w)/2;
                var t=(screen.height-h)/2;

                newWin=window.open("admin_cursos.php?id=" + id,'menueditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
                if(newWin!=null)setTimeout('newWin.focus()',100);

            }

            function abrirAssessment(id) {
                newWin=null;
                var w=600;
                var h=300;
                var l=(screen.width-w)/2;
                var t=(screen.height-h)/2;

                newWin=window.open("admin_assessment.php?id=" + id,'menueditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
                if(newWin!=null)setTimeout('newWin.focus()',100);

            }
			
			function openWindow(url){
				newWin=null;
				var w=<?php echo $paginaEdicaoLargura; ?>;
				var h=<?php echo $paginaEdicaoAltura; ?>;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				newWin=window.open(url,'menueditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
				if(newWin!=null)setTimeout('newWin.focus()',100);
			}
			
			function getURL(){
				getURL(null);
			}
			
			function getURL(id){
				if(id != null){
					id = "?id=" + id;
				}else{
					id = "";
				}
				
				openWindow(paginaEdicao + id);
				
			}
		
		</script>		
	</HEAD>
	
	<BODY>
	<!-- Inicio do T�tulo -->
		<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
			<TR>
				<TD><IMG height="2" src="images/blank.gif" width="100%"/></TD>
			</TR>
			<TR>
				<TD background="images/bg_logo_admin.png">
					<TABLE cellpadding="0" cellspacing="0" width="663" background="images/logo_admin.png" border="0">
						<TR>
							<TD><IMG src="images/blank.gif" height="32" width="1"/></TD>
							<TD class="data" align="right"><?php echo getServerDate(); ?></TD>
						</TR>
					
					</TABLE>
				
				</TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" width="100%" height="2" /></TD>
			</TR>
			<TR>
				<TD bgcolor="#cccccc"><IMG src="images/blank.gif" height="3" width="100%"/></TD>
			</TR>
		</TABLE>
		<!-- Fim do T�tulo -->
		
		<!-- In�cio Cabe�alho -->
		<TABLE cellspacing="0" cellpadding="0" width="756" align="center" border="0">
			<TR>
				<TD width="1%"><IMG height="20" src="images/blank.gif" width="289"/></TD>
				<TD></TD>
				<TD></TD>
			</TR>
			<TR valign="top">
				<TD class="textblk"><SPAN class="title">USU�RIO: </SPAN><?php echo $_SESSION['alias']; ?></TD>
				<TD width="1%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.replace('logout.php')"
									type="button" value="Logout" /></TD>
				<TD align="right" width="98%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href='admin_menu.php?'"
							type="button" value="Voltar"></TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" height="4" width="1" /></TD>
			</TR>
			<TR>
				<TD class="textblk" colSpan="2"><SPAN class="title">P�GINA ATUAL:</SPAN>
						<?php echo TITULO_PAGINA; ?></TD>
			</TR>
			<TR>
				<TD><IMG height="1" src="images/blank.gif" width="280"></TD>
			</TR>
		</TABLE>
		<BR>
		<!-- Fim Cabe�alho -->
		
		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="post">
			
            <input type="hidden" name="hdnRemovidos" value="<?php echo $_POST["hdnRemovidos"] ?>" />
            		
			<TABLE cellpadding="0" cellspacing="0" width="756" align="center" border="0">
				<TR class="tarjaTitulo">
						<TD align="middle" height="20"><?php echo TITULO_PAGINA; ?></TD>
				</TR>
				<TR>
					<TD class="title" width="100%">
						
						<A onfocus="noFocus(this)" onclick="getURL();return false" style="cursor: hand;">
							<SPAN class="title" style="width: 49%"><IMG height="20" src="images/bt_novo.gif" width="20" align="absMiddle" vspace="10"
									border="0" /><?php echo "Copiar Programa" ?></SPAN></A>							
					</TD>
				</TR>
				<TR>
					<TD><IMG src="images/blank.gif" height="1" width="10"/></TD>
				</TR>
				<TR>
					<TD width="100%">


<table cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td class="textblk">
			Programa:
		</td>
	</tr>
	<tr>
		<td>
			<?php
			comboboxEmpresa("cboEmpresa", $_POST["cboEmpresa"]);
			?>	
			<input type='submit' name='btnPesquisa' class='buttonsty' value='Buscar'>	
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>

</table>

<?php 
if (count($_POST) > 0 && $_POST["cboEmpresa"] > 0)
{

	
	$rn = new col_empresa();
	$rn->CD_EMPRESA = $_POST["cboEmpresa"];
	$rn = $rn->obter();
	
	
	
	$linha = array();
	$codigoEmpresa = $_POST["cboEmpresa"];
	$sql = 
			"SELECT
				COUNT(1) AS QTD_FAROL
			 FROM
				col_farol
			 WHERE
				CD_EMPRESA = $codigoEmpresa
			 AND IN_ATIVO = 1";

	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaFarol = mysql_fetch_row($resultado);
	$linha["QTD_FAROL"] = $linhaFarol[0];

	$sql = "SELECT
				COUNT(1) AS QTD_FORUM
			FROM
				col_foruns
			WHERE
				CD_EMPRESA = $codigoEmpresa
			AND IN_ATIVO = 1";

	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaFarol = mysql_fetch_row($resultado);
	$linha["QTD_FORUM"] = $linhaFarol[0];

	$sql = "SELECT
				COUNT(IF(p.IN_PROVA=1,1,NULL)) AS QTD_PROVA,
				COUNT(IF(p.IN_PROVA=0,1,NULL)) AS QTD_SIMULADO
			FROM
				col_prova p
				INNER JOIN col_prova_vinculo pv ON p.CD_PROVA = pv.CD_PROVA
			WHERE
				pv.IN_TIPO_VINCULO = 1
				AND pv.CD_VINCULO = $codigoEmpresa
				AND p.IN_ATIVO = 1";
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaFarol = mysql_fetch_row($resultado);
	$linha["QTD_PROVA"] = $linhaFarol[0];
	$linha["QTD_SIMULADO"] = $linhaFarol[1];
	
	
	$sql = "SELECT
				COUNT(1) AS QTD_PESQUISA
			FROM
				col_pesquisa
			WHERE
				CD_EMPRESA = $codigoEmpresa
			AND IN_ATIVO = 1";
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaFarol = mysql_fetch_row($resultado);
	$linha["QTD_PESQUISA"] = $linhaFarol[0];
	
	
	$sql = "SELECT COUNT(1) AS QTD_ASSESSMENT FROM col_assessment WHERE CD_EMPRESA = $codigoEmpresa";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaAssessment = mysql_fetch_row($resultado);
	$linha["QTD_ASSESSMENT"] = $linhaAssessment[0];


    $sql = "SELECT
                COUNT(1) AS QTD_CURSO
            FROM
                col_curso_empresa ce
                INNER JOIN tb_pastasindividuais t ON t.ID = ce.ID_PASTA_INDIVIDUAL
            WHERE
                ce.CD_EMPRESA = $codigoEmpresa";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);
    $linhaCursos = mysql_fetch_row($resultado);
    $linha["QTD_CURSO"] = $linhaCursos[0];


	//echo $sql;
	$sql = "SELECT
	pc.DS_PAGINA,
	(
		SELECT
			c.CD_COMENTARIO
		FROM
			col_comentario c
		WHERE
			c.NM_PAGINA = pc.NM_PAGINA
		AND c.IN_ATIVO = 1
		AND c.CD_EMPRESA = $codigoEmpresa
		AND (
			c.CD_CICLO IS NULL
			OR c.CD_CICLO < 0
		)
		LIMIT 0,
		1
	) AS CD_COMENTARIO,
	NULL AS DT_INICIO,
	NULL AS NM_CICLO
FROM
	col_pagina_comentario pc

UNION ALL

	SELECT
		pc.DS_PAGINA,
		co.CD_COMENTARIO,
		c.DT_INICIO,
		c.NM_CICLO
	FROM
		col_pagina_comentario pc
	INNER JOIN col_ciclo c ON pc.NM_PAGINA = 'sumario_artigo_video.php'
	AND c.CD_EMPRESA = $codigoEmpresa
	LEFT JOIN col_comentario co ON co.NM_PAGINA = pc.NM_PAGINA
	AND co.CD_CICLO = c.CD_CICLO
	ORDER BY
		DS_PAGINA,
		DT_INICIO;";
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);



	
?>
<table cellpadding="0" cellspacing="0" align="center" width="80%">
	<tr class="textblk">
		<td width="60%">Per�odo:</td>
		<td width="40%"><?php echo "{$rn->DT_INICIO_VIGENCIA} a {$rn->DT_TERMINO_VIGENCIA}"; ?></td>
	</tr>
	<tr class="textblk">
		<td>E-mail do Programa:</td>
		<td><?php echo $rn->TX_EMAIL_PROGRAMA; ?></td>
	</tr>
	<tr class="textblk" >
		
		<td colspan="2" style="border: 1px solid #000">
		<b>Ciclos</b><br />
	<?php 
		$ciclos = $rn->ciclos;
		if (is_array($ciclos))
		{
			if (count($ciclos) > 1)
				$linhas = count($ciclos);
				
			usort($ciclos, "ordenarCiclos");
			
		}
		
		foreach($ciclos as $ciclo) {
			echo "{$ciclo->NM_CICLO} - {$ciclo->DT_INICIO} a {$ciclo->DT_TERMINO}<br />";
		}
	?>
		</td>
	</tr>
	<tr class="textblk">
		<td>
			<a href="grid_prova.php"  >Provas</a>:
		</td>
		<td>
			<?php echo $linha["QTD_PROVA"]; ?>
		</td>
	</tr>
	<tr class="textblk">
		<td>
			<a href="grid_prova.php"  >Simulados</a>:
		</td>
		<td>
			<?php echo $linha["QTD_SIMULADO"]; ?>
		</td>
	</tr>
    <tr class="textblk">
        <td>
            <a href="javascript:abrirCursos(<?php echo $codigoEmpresa ?>)"  >Cursos</a>:
        </td>
        <td>
            <?php echo $linha["QTD_CURSO"]; ?>
        </td>
    </tr>

	<tr class="textblk">
		<td>
			<a href="admin_foruns.php"  >Foruns</a>:
		</td>
		<td>
			<?php echo $linha["QTD_FORUM"]; ?>
		</td>
	</tr>
	<tr class="textblk">
		<td>
			<a href="admin_pesquisas.php"  >Pesquisas</a>:
		</td>
		<td>
			<?php echo $linha["QTD_PESQUISA"]; ?>
		</td>
	</tr>
	<tr class="textblk">
		<td>
			<a href="grid_relatorio_farol.php"  >Radar Configurado</a>:
		</td>
		<td>
			<?php echo ($linha["QTD_FAROL"]==1?"Sim":"N�o"); ?>
		</td>
	</tr>
		<tr class="textblk">
		<td>
			<a href="javascript:abrirAssessment(<?php echo $codigoEmpresa ?>)"  >Assessment Configurado</a>:
		</td>
		<td>
			<?php echo ($linha["QTD_ASSESSMENT"]>=1?"Sim":"N�o"); ?>
		</td>
	</tr>
	
	<tr class="textblk">
		<td colspan="2">
			&nbsp;
		</td>
	</tr>
	
	<tr class="textblk">
		<td colspan="2">
			<a href="grid_comentario.php"  >Coment�rios Configurados</a>:
		</td>
	</tr>
		<?php 
			while($linhaComentario = mysql_fetch_array($resultado)) {
				
				$situacaoComentario = ($linhaComentario["CD_COMENTARIO"]>0?"Sim":"N�o");
				$cssComentarioOk = "";
				if ($linhaComentario["CD_COMENTARIO"]) {
					$cssComentarioOk = "style=\"background-color:#cccccc;\"";

				}
				
				$codigoComentario = $linhaComentario["CD_COMENTARIO"];

                $nomePagina = $linhaComentario["DS_PAGINA"];

                if ($linhaComentario["NM_CICLO"] != ""){
                    $nomePagina = "$nomePagina - {$linhaComentario["NM_CICLO"]}";
                }
				
				echo "<tr class=\"textblk\" $cssComentarioOk>
						<td style=\"padding-left: 30px;\">
							<a href=\"javascript:abrirComentario('$codigoComentario')\"> $nomePagina </a>
						</td>
						<td>
							$situacaoComentario
						</td>
					</tr>";
				
			}
		?>
		
				
</table>



<?php 
}
?>


					</TD>
				</TR>
				<TR class="tarjaTitulo">
						<TD colSpan="4"><IMG height="1" src="images/blank.gif" width="100"></TD>
				</TR>
				
			</TABLE>

			
	
		</FORM>
		
	</BODY>
	
</HTML>