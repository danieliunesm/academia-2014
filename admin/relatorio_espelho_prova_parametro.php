<?php

include("../include/defines.php");
include('framework/crud.php');
include('controles.php');
include('datagrid.php');

function instanciarRN(){
	return new col_disciplina();
}

$paginaEdicao = "relatorio_espelho_prova.php";
$paginaEdicaoAltura = 600;
$paginaEdicaoLargura = 900;

$obterParametros = "'cboEmpresa=' + document.getElementById('cboEmpresa').value + '&nomeEmpresa=' + document.getElementById('cboEmpresa').options[document.getElementById('cboEmpresa').selectedIndex].text + 
					'&cboUsuario=' + document.getElementById('cboUsuario').value + '&nomeUsuario=' + document.getElementById('cboUsuario').options[document.getElementById('cboUsuario').selectedIndex].text + 
					'&cboProva=' + document.getElementById('cboProva').value + '&nomeProva=' + escape(document.getElementById('cboProva').options[document.getElementById('cboProva').selectedIndex].text)";

define(TITULO_PAGINA, "Espelho de Avalia��o");

include('relatorio_cabecalho.php');

exibirParametros();

include('relatorio_rodape.php');


function exibirParametros(){
	
	?>
	
	<table cellpadding="0" cellspacing="0" border="0" align="center">
		<tr>
			<td class="textblk">
				Empresa: &nbsp;
			</td>
			<td>
				<?php comboboxEmpresa("cboEmpresa",$_POST["cboEmpresa"]); ?>
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</tr>
		<tr>
			<td class="textblk">
				Usu�rio: &nbsp;
			</td>
			<td>
				<?php comboboxUsuario("cboUsuario",$_POST["cboUsuario"], true); ?>
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</tr>
		<tr>
			<td class="textblk">
				Avalia��o: &nbsp;
			</td>
			<td>
				<?php comboboxProva("cboProva",$_POST["cboProva"], false, false); ?>
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</tr>
	</table>
	
	<?php
	
}


?>