<?php

include('filtrocomponente.php');
include('farol_util.php');
include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_farol();
}

$CarregouRN = false;

function carregarRN(){
	
	global $CarregouRN;
	$CarregouRN = true;
	
	page::$rn->CD_FAROL = $_REQUEST["id"];
	page::$rn->DT_INICIO = $_REQUEST["txtDe"];
	page::$rn->DT_TERMINO = $_REQUEST["txtAte"];
	page::$rn->IN_ATIVO = 1;
	page::$rn->NM_FAROL = $_REQUEST["txtNomeRelatorio"];
	page::$rn->NR_PESO_PARTICIPACAO = $_REQUEST["txtPesoParticipacao"];
	page::$rn->NR_PESO_ACESSO = $_REQUEST["txtPesoAcesso"];
	page::$rn->NR_PESO_DOWNLOAD = $_REQUEST["txtPesoDownload"];
	page::$rn->NR_PESO_PARTICIPACAO_SIMULADO = $_REQUEST["txtPesoParticipacaoSimulado"];
	page::$rn->NR_PESO_NOTA_SIMULADO = $_REQUEST["txtPesoNotaSimulado"];
	page::$rn->NR_PESO_QUANTIDADE_SIMULADO = $_REQUEST["txtPesoQuantidadeSimulado"];
	page::$rn->NR_PESO_PARTICIPACAO_FORUM = $_REQUEST["txtPesoParticipacaoForum"];
	page::$rn->NR_PESO_CONTRIBUICAO_FORUM = $_REQUEST["txtPesoContribuicaoForum"];
	page::$rn->NR_PESO_PARTICIPACAO_AVALIACAO = $_REQUEST["txtPesoParticipacaoAvaliacao"];
	page::$rn->NR_PESO_NOTA_AVALIACAO = $_REQUEST["txtPesoNotaAvaliacao"];
	
	if (count($_REQUEST) > 0)
	{
	
		page::$rn->IN_PESO_PARTICIPACAO = TratarCheckFarol($_REQUEST["chktxtPesoParticipacao"]);
		page::$rn->IN_PESO_ACESSO = TratarCheckFarol($_REQUEST["chktxtPesoAcesso"]);
		page::$rn->IN_PESO_DOWNLOAD = TratarCheckFarol($_REQUEST["chktxtPesoDownload"]);
		page::$rn->IN_PESO_PARTICIPACAO_SIMULADO = TratarCheckFarol($_REQUEST["chktxtPesoParticipacaoSimulado"]);
		page::$rn->IN_PESO_NOTA_SIMULADO = TratarCheckFarol($_REQUEST["chktxtPesoNotaSimulado"]);
		page::$rn->IN_PESO_QUANTIDADE_SIMULADO = TratarCheckFarol($_REQUEST["chktxtPesoQuantidadeSimulado"]);
		page::$rn->IN_PESO_PARTICIPACAO_FORUM = TratarCheckFarol($_REQUEST["chktxtPesoParticipacaoForum"]);
		page::$rn->IN_PESO_CONTRIBUICAO_FORUM = TratarCheckFarol($_REQUEST["chktxtPesoContribuicaoForum"]);
		page::$rn->IN_PESO_PARTICIPACAO_AVALIACAO = TratarCheckFarol($_REQUEST["chktxtPesoParticipacaoAvaliacao"]);
		page::$rn->IN_PESO_NOTA_AVALIACAO = TratarCheckFarol($_REQUEST["chktxtPesoNotaAvaliacao"]);
	
	}
	
	page::$rn->NR_LIMITE = $_REQUEST["txtLimiteResultado"];
	
	if (is_array($_REQUEST["cboAgrupamento"]))
	{
		page::$rn->IN_TIPO_AGRUPAMENTO = join(";",$_REQUEST["cboAgrupamento"]);	
	}

	page::$rn->CD_EMPRESA = $_REQUEST["cboEmpresa"];
	
	//$rnFiltro = ObterDadosFiltroCadastro(true);
	
	//$rnFiltro->NM_FILTRO = page::$rn->NM_FAROL;
	
	//page::$rn->filtro = $rnFiltro;
	
}

function pageRenderEspecifico(){
	
	global $CarregouRN;
	
	if (count($_POST) > 0 && $CarregouRN == false)
	{
		carregarRN();
	}

	if (page::$rn == null)
	{
		page::$rn = new col_farol();
	}
	
	exibirParametrosFarolCadastro(page::$rn);

}

function adicionarBotoes()
{
	return;
	?>
	
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="buttonsty" value="Preview" onclick="javascript:visualizarPreview();" onfocus="noFocus(this)">
	 
	<?php
	
}

?>

<script language="javascript">
	
	function visualizarPreview()
	{
		
			var newWin=null;
			var w=900;
			var h=600;
			var l=(screen.width-w)/2;
			var t=(screen.height-h)/2;
		
			newWin=window.open('','relatorio','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
			document.forms[0].target = "relatorio";
			document.forms[0].action = 'relatorio_farol.php';
			document.forms[0].method = "post";
			document.forms[0].submit();
			
			document.forms[0].target = '';
			document.forms[0].action = '';
		
	}

</script>