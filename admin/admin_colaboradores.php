<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";

$searchString = $_SERVER["QUERY_STRING"];
$_SESSION['searchString'] = $searchString;

$vpp  = isset($_GET["vpp"])?$_GET["vpp"]:1;
$vp   = isset($_GET["vp"])?$_GET["vp"]:7;

function getSTATUScheck($oView,$tb)
{
	switch($oView)
	{
		case 1:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 5)";
		case 3:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 2 OR " . $tb . "STATUS = 5)";
		case 5:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 3 OR " . $tb . "STATUS = 5)";
		case 7:
			return "(" . $tb . "STATUS = 1 OR " . $tb . "STATUS = 2 OR " . $tb . "STATUS = 3 OR " . $tb . "STATUS = 5)";
	}
}

function getTIPOUSUcheck($oView,$tb)
{
	switch($oView)
	{
		case 1:
			return "(" . $tb . "TIPO = 1 OR " . $tb . "TIPO = 5)";
		case 3:
			return "(" . $tb . "TIPO = 1 OR " . $tb . "TIPO = 2 OR " . $tb . "TIPO = 5)";
		case 5:
			return "(" . $tb . "TIPO = 1 OR " . $tb . "TIPO = 3 OR " . $tb . "TIPO = 5)";
		case 7:
			return "(" . $tb . "TIPO = 1 OR " . $tb . "TIPO = 2 OR " . $tb . "TIPO = 3 OR " . $tb . "TIPO = 5)";
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Educa��o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="JavaScript">
function init(){
<?
if($_SESSION["msg"] != "")
{
	echo "	alert(\"" . $_SESSION["msg"]. "\");";
	$_SESSION["msg"] = "";
}
?>
}

function openWindow(url){
	newWin=null;
	var w=screen.width-40;
	var h=screen.height-80;
	var l=10;
	var t=20;
	newWin=window.open(url,'htmleditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=0,resizable=1');
	if(newWin!=null)setTimeout('newWin.focus()',100);
}

function getURL(pg,s,i,a){
	openWindow('/admin/htmleditor/input_'+pg+'.php?sessid='+s+'&id='+i+'&action='+a);
}

function ShowDialog(pagePath, args, width, height, left, top){
	return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
}

oVpp = <? echo $vpp; ?>;
oVp  = <? echo $vp; ?>;

function viewControl(vSession){
	oVsession = vSession;
	var html = ShowDialog("/admin/htmleditor/dialog/viewcolaboradorescontrol.html", window, 420, 210);
	if(html)document.location.href=html;
}

function errObj(iref,x,y){
	this.iref	   = iref; 
	this.pos	   = x+','+y;
	this.arr	   = [];
	this.addIndice = function(I){this.arr[this.arr.length] = I;}
}

function checkValues(f){
	var msg 	 = '';
	var posArray = [];
	var x		 = 0;
	var y		 = 0;
	var errArray = [];
	for(var i=0;i<f.length;i++){
		if(f[i].name.indexOf('pxc')!=-1){
			x=f[i].value;
			y=f[i+1].value;
			if((f[i+2]&&f[i+2].type=='radio')&&(f[i+2].checked)){
				posArray[posArray.length]=[x,y,false];
				i++;
			}
		}
	}
	
//	for(var i=0;i < posArray.length;i++){
//		alert(i+'    '+posArray[i])
//	}
	
	for(var i=0;i<posArray.length;i++){
		errArray[i] = new errObj((i+1),posArray[i][0],posArray[i][1]);
		if(posArray[i][2])continue;
		for(var k=0;k<posArray.length;k++){
			if(k==i)continue;
			if(posArray[k][0]==posArray[i][0]&&posArray[k][1]==posArray[i][1]){
				posArray[k][2]=true;
				errArray[i].addIndice((k+1));
			}
		}
	}
	for(var i=0;i<errArray.length;i++){
		if(errArray[i].arr.length>0){
			msg += 'As c�lulas nas linhas '+errArray[i].iref+','+errArray[i].arr+' est�o na mesma posi��o: ('+errArray[i].pos+')! Altere suas posi��es para que n�o haja sobreposi��o entre elas.     \n';
		}
	}
	for(var i=0;i<posArray.length;i++){
		if(!(((Math.abs(posArray[i][0]) % 2 == 0) && (Math.abs(posArray[i][1]) % 2 == 0))||((Math.abs(posArray[i][0]) % 2 == 1) && (Math.abs(posArray[i][1]) % 2 == 1))))
		msg += 'Na c�lula da linha '+(i+1)+' os pares de coordenadas devem ser ambos pares ou �mpares!     \n';
		if(posArray[i][0]>7)msg += 'Na c�lula da linha '+(i+1)+' o valor de X n�o pode ser superior a 7!     \n';
		if(posArray[i][0]<0 || posArray[i][1]<0)msg += 'Na c�lula da linha '+(i+1)+' os valores de X e Y n�o podem ser negativos!     \n';
	}
	if(msg!=""){
		alert(msg);
		return false;
	}
	else return true;
}

function submitPage(url,tgt){
	if(!checkValues(document.dadosForm))return false;
	document.dadosForm.action=url;
	document.dadosForm.target=tgt;
	document.dadosForm.submit();
}
</script>
</head>
<body onload="init()">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><span class="title">USU�RIO:&nbsp;</span><? echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php?<? echo $searchString; ?>'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="4"></td></tr>
<tr><td class="textblk" colspan="2"><span class="title">P�GINA ATUAL:</span> Colaboradores</td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="1"></td></tr>
</table>
<br>

<form name="dadosForm" action="" method="post">
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr class="tarjaTitulo">
	<td height="20" colspan="9" align="center">COLABORADORES</td>
</tr>
<tr><td colspan="2" class="title"><a href="#" onclick="viewControl('vpp');return false" onfocus="noFocus(this)"><img src="/images/layout/bt_all.gif" width="22" height="26" border="0" align="absmiddle"><span class="title">&nbsp;Modo de exibi��o&nbsp;dos&nbsp;Colaboradores</span></a></td><td colspan="5" class="title" style="padding-left:10px"><a href="#" onclick="getURL('colaboradores',1,'',1);return false" onfocus="noFocus(this)"><img src="/images/layout/bt_novo.gif" width="20" height="20" border="0" vspace="10" align="absmiddle"><span class="title">&nbsp;Incluir novo Colaborador</span></a></td></tr>
<tr>
	<td width="60%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="33%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="1%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="1%"></td>
	<td width="1%"></td>
	<td width="1%"></td>
</tr>
<tr class="tarjaItens">
	<td align="left" class="title" rowspan="2" style="padding-left:40px">COLABORADORES</td>
	<td></td>
	<td align="center" class="title" rowspan="2">POS X,Y MOSAICO</td>
	<td></td>
	<td align="center" class="title" rowspan="2" style="padding-left:10px;padding-right:10px">DT&nbsp;INCLUS�O</td>
	<td></td>
	<td colspan="3" align="center" class="title">STATUS</td>
	<td></td>
</tr>
<tr class="tarjaItens">
	<td></td>
	<td></td>
	<td></td>
	<td align="center"><img src="/images/layout/bt_publicar.gif" width="32" height="32" hspace="5" alt="   publicar   "></td>
	<td align="center"><img src="/images/layout/bt_remover.gif" width="32" height="32" hspace="5" alt="   remover   "></td>
	<td align="center"><img src="/images/layout/bt_editar.gif" width="32" height="32" hspace="5" alt="   editar   "></td>
</tr>
<tr class="tarjaItens"><td colspan="9"><img src="/images/layout/blank.gif" width="1" height="7"></td></tr>
<?
//$sql = "SELECT ID, ITEM, STATUS, TIPO_LINK FROM tb_usuariostmp WHERE " . getSTATUScheck($vpp,"") . " ORDER BY ITEM";
//$sql = "SELECT ID, NOME, SNOME, FOTO, STATUS FROM tb_usuariostmp WHERE STATUS = " . getSTATUScheck($vpp,"") . " AND (TIPO_USU = 3 OR TIPO_USU = 5) ORDER BY NOME";
//$sql = "SELECT ID, FOTO, NOME, SNOME, STATUS, X, Y FROM tb_usuariostmp WHERE " . getSTATUScheck($vpp,"") . " AND " . getTIPOUSUcheck($vp,"") . " ORDER BY NOME";

//$sql = "SELECT ID, FOTO, NOME, SNOME, STATUS, X, Y FROM tb_usuariostmp WHERE " . getSTATUScheck($vpp,"") . " ORDER BY NOME";

$sql = "SELECT c.ID, u.FOTOFILE, u.NM_USUARIO, u.NM_SOBRENOME, c.STATUS, c.X, c.Y FROM col_usuario u, tb_colaboradores c WHERE c.IDusuario = u.CD_USUARIO AND " . getSTATUScheck($vpp,"c.") . " ORDER BY u.NM_USUARIO";

$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
$iCount = 0;
$bgcolor = "#ffffff";
while($oRs = mysql_fetch_row($RS_query))
{
//	if($oRs[1]!=''){
?>
		<tr bgcolor="<? echo $bgcolor; ?>">
		<td class="textblk" style="padding-left:40px;padding-top:5px;padding-bottom:5px<? echo $oRs[4]!=1?';color:#c00':''; ?>"><img src="/images/colaboradores/miniaturas/<? echo $oRs[1]; ?>" width="54" height="48" style="border:1px solid #999999" align="absmiddle" vspace="1"><img src="/images/layout/blank.gif" width="10" height="50" border="0" align="absmiddle"><? echo $oRs[2] . " " . $oRs[3]; ?></td>
		<td class="text" align="right"><? echo $iCount+1; ?></td>
		<td align="center"><input type="text" name="pxc<? echo $oRs[0]; ?>" style="width:20px;height:20px;border:1px solid #999;text-align:right;margin:2px" value="<? echo $oRs[5]; ?>"><input type="text" name="pyc<? echo $oRs[0]; ?>" style="width:20px;height:20px;border:1px solid #999;text-align:right;margin:2px" value="<? echo $oRs[6]; ?>"></td>
		<td></td>
		<td class="dataarquivos"><? if(!empty($oRs2[5])) echo FmtDataTempo($oRs2[5]); ?></td>
		<td></td>
		<td align="center"><input type="radio" name="col<? echo $oRs[0]; ?>" value="1" <? if($oRs[4]==1 || $oRs[4]==5) echo "checked"; ?> onfocus="noFocus(this)" <? if($oRs[4]==5) echo "style=\"background-image:url(/images/layout/bg_radio.gif)\""; ?> onclick="<? if($oRs[4]==5) echo "document.dadosForm.col" . $oRs[0] . "[0].style.backgroundImage='url(/images/layout/bg_radio.gif)'"; ?>"></td>
		<td align="center"><input type="radio" name="col<? echo $oRs[0]; ?>" value="3" <? if($oRs[4]==3) echo "checked"; ?> onfocus="noFocus(this)" onclick="document.dadosForm.col<? echo $oRs[0]; ?>[0].style.backgroundImage=''"></td>
		<td align="center"><a href="#" onclick="getURL('colaboradores',1,<? echo $oRs[0]; ?>,2);return false" onfocus="noFocus(this)"><img src="/images/layout/bt_edit.gif" width="20" height="20" border="0"></a></td>
		</tr>
		<input type="hidden" name="nmc<? echo $oRs[0]; ?>" value="<? echo $oRs[2] . " " . $oRs[3]; ?>">
		<input type="hidden" name="imc<? echo $oRs[0]; ?>" value="<? echo "/images/colaboradores/miniaturas/" . $oRs[1]; ?>">
<?
		$iCount++;
		if($iCount % 2 == 0) $bgcolor="#ffffff"; else $bgcolor="#f1f1f1";
//	}
}
mysql_free_result($RS_query);

$sql = "SELECT ID, STATUS, X, Y FROM tb_colaboradores WHERE IDusuario = 0 AND " . getSTATUScheck($vpp,"");

$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
while($oRs = mysql_fetch_row($RS_query))
{
//	if($oRs[1]==''){
?>
		<tr bgcolor="<? echo $bgcolor; ?>">
		<td class="textblk" style="padding-left:40px;padding-top:5px;padding-bottom:5px<? echo $oRs[1]!=1?';color:#c00':''; ?>"><img src="/images/layout/celulavazia.gif" width="56" height="50" align="absmiddle"><img src="/images/layout/blank.gif" width="10" height="50" border="0" align="absmiddle">C�LULA VAZIA</td>
		<td class="text" align="right"><? echo $iCount+1; ?></td>
		<td align="center"><input type="text" name="pxc<? echo $oRs[0]; ?>" style="width:20px;height:20px;border:1px solid #999;text-align:right;margin:2px" value="<? echo $oRs[2]; ?>"><input type="text" name="pyc<? echo $oRs[0]; ?>" style="width:20px;height:20px;border:1px solid #999;text-align:right;margin:2px" value="<? echo $oRs[3]; ?>"></td>
		<td></td>
		<td class="dataarquivos"></td>
		<td></td>
		<td align="center"><input type="radio" name="col<? echo $oRs[0]; ?>" value="1" <? if($oRs[1]==1 || $oRs[1]==5) echo "checked"; ?> onfocus="noFocus(this)" <? if($oRs[1]==5) echo "style=\"background-image:url(/images/layout/bg_radio.gif)\""; ?> onclick="<? if($oRs[1]==5) echo "document.dadosForm.col" . $oRs[0] . "[0].style.backgroundImage='url(/images/layout/bg_radio.gif)'"; ?>"></td>
		<td align="center"><input type="radio" name="col<? echo $oRs[0]; ?>" value="3" <? if($oRs[1]==3) echo "checked"; ?> onfocus="noFocus(this)" onclick="document.dadosForm.col<? echo $oRs[0]; ?>[0].style.backgroundImage=''"></td>
		<td align="center"><a href="#" onclick="getURL('colaboradores',1,<? echo $oRs[0]; ?>,2);return false" onfocus="noFocus(this)"><img src="/images/layout/bt_edit.gif" width="20" height="20" border="0"></a></td>
		</tr>
		<input type="hidden" name="nmc<? echo $oRs[0]; ?>" value="">
		<input type="hidden" name="imc<? echo $oRs[0]; ?>" value="">
<?
		$iCount++;
		if($iCount % 2 == 0) $bgcolor="#ffffff"; else $bgcolor="#f1f1f1";
//	}
}
mysql_free_result($RS_query);

echo "<input type=\"hidden\" name=\"Ncol\" value=\"" . $iCount . "\">";
?>
<tr class="tarjaTitulo"><td colspan="9"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="1" height="40"></td></tr>
<tr><td align="center"><input type="button" class="buttonsty" value="Preview" onclick="submitPage('/admin/htmleditor/preview_colaboradores.php','previewWin')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Atualizar Status" onclick="submitPage('/admin/updatecolaboradoresstatus.php','')" onfocus="noFocus(this)"><img src="/images/layout/blank.gif" width="20" height="1"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php?<? echo $searchString; ?>'" onfocus="noFocus(this)"><br><br><br></td></tr></table>
</body>
</html>
<?
mysql_close();
?>