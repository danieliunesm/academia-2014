<?php

	function TratarFiltro(&$p_cdEmpresa, &$lotacoes, &$cargos_funcoes, &$usuarios, &$provas, &$foruns)
	{
		
		$filtro = ObterDadosFiltro();
		
		$p_cdEmpresa = $filtro->CD_EMPRESA;
		$lotacoes = $filtro->lotacoes;
		$cargos_funcoes = $filtro->cargos_funcoes;
		$usuarios = $filtro->usuarios;
		$provas = $filtro->provas;
		$foruns = $filtro->foruns;
		
		
	}

	function ObterDadosFiltroCadastro($filtroFarol=false)
	{
		$filtro = ObterDadosFiltro($filtroFarol);
		
		if (count($filtro->cargos_funcoes) == 0) $filtro->cargos_funcoes[] = new col_filtro_cargo_funcao();
		if (count($filtro->lotacoes) == 0) $filtro->lotacoes[] = new col_filtro_lotacao();
		if (count($filtro->usuarios) == 0) $filtro->usuarios[] = new col_filtro_usuario();
		if (count($filtro->provas) == 0) $filtro->provas[] = new col_filtro_prova();
		if (count($filtro->foruns) == 0) $filtro->foruns[] = new col_filtro_forum();

		return $filtro;
	}
	
	function ObterDadosFiltro($filtroFarol=false)
	{
		
		$filtro = null;
		
		if ($filtroFarol)
		{
			$filtro = new col_filtro_farol();
		}else
		{
			$filtro = new col_filtro();
		}
		
		$filtro->CD_FILTRO = $_REQUEST["id"];
		$filtro->NM_FILTRO = $_REQUEST["txtNomeFiltro"];
		$filtro->CD_EMPRESA = $_REQUEST["cboEmpresa"];
		
		$filtro->cargos_funcoes = array();
		$filtro->lotacoes = array();
		$filtro->usuarios = array();
		$filtro->provas = array();
		$filtro->foruns = array();
		
		if (is_array($_REQUEST["cboCargo"]))
		{
		
			foreach ($_REQUEST["cboCargo"] as $cargo)
			{
				
				$cargoFuncao = new col_filtro_cargo_funcao();
				$cargoFuncao->NM_CARGO_FUNCAO = $cargo;
				
				$filtro->cargos_funcoes[] = $cargoFuncao;		
				
			}
		}
	
	
		if (is_array($_REQUEST["cboLotacao"]))
		{
			foreach ($_REQUEST["cboLotacao"] as $lotacao)
			{
				
				$lotacaoFiltro = new col_filtro_lotacao();
				$lotacaoFiltro->CD_LOTACAO = $lotacao;
				
				$filtro->lotacoes[] = $lotacaoFiltro;		
				
			}
		}
		
		if (is_array($_REQUEST["cboUsuario"]))
		{
			foreach ($_REQUEST["cboUsuario"] as $usuario)
			{
				
				$usuarioFiltro = new col_filtro_usuario();
				$usuarioFiltro->CD_USUARIO = $usuario;
				
				$filtro->usuarios[] = $usuarioFiltro;		
				
			}
		}
		
		if (is_array($_REQUEST["cboProva"]))
		{
			foreach ($_REQUEST["cboProva"] as $prova)
			{
				
				$provaFiltro = new col_filtro_prova();
				$provaFiltro->CD_PROVA = $prova;
				
				$filtro->provas[] = $provaFiltro;		
				
			}
		}
		
		if (is_array($_REQUEST["cboForum"]))
		{
			foreach ($_REQUEST["cboForum"] as $forum)
			{
				
				$forumFiltro = new col_filtro_forum();
				$forumFiltro->CD_FORUM = $forum;
				
				$filtro->foruns[] = $forumFiltro;		
				
			}
		}
		
		return $filtro;
		
		
	}


	function ExibirFiltroConsulta($p_bExibirProva = false, $p_bExibirForum = false, $p_bTratarFiltro = true, $p_bExibirEmpresa = true, $p_bExibirFiltro = true, $p_bExibirUsuario = true, $carregarPorEmpresa = false, $p_cdEmpresa = 0)
	{
		ExibirFiltro($p_cdEmpresa, 0, 0, 0, 0, 0, $p_bExibirProva, $p_bExibirForum, $p_bTratarFiltro, $p_bExibirEmpresa, $p_bExibirFiltro, $p_bExibirUsuario, $carregarPorEmpresa);
	}
	
	function ExibirFiltro($p_cdEmpresa, $lotacoes, $cargos_funcoes, $usuarios, $provas = 0, $foruns = 0, $p_bExibirProva = false, $p_bExibirForum = false, $p_bTratarFiltro = false, $p_bExibirEmpresa = true, $p_bExibirFiltro = false, $p_bExibirUsuario = true, $carregarPorEmpresa = false, $exibirInativos = true)
	{
		
		$p_cdFiltro = $_REQUEST["cboFiltro"];
		$p_cdEmpresaAnterior = $_REQUEST["hdnEmpresa"];
		
		
		if ($p_bTratarFiltro)
		{
			
			TratarFiltro($p_cdEmpresa, $lotacoes, $cargos_funcoes, $usuarios, $provas, $foruns);
			
		}

		if ($p_bExibirEmpresa)
		{
			
			?>

					<TR>
						<TD class="textblk">Programa: *</TD>
					</TR>
					<TR>
						<TD>
							<?php comboboxEmpresa("cboEmpresa", $p_cdEmpresa, "Empresa", true, false); ?>
							<input type="hidden" id="hdnEmpresa" name="hdnEmpresa" value="<?php echo $p_cdEmpresa; ?>" />
						</TD>
					</TR>
					<tr><td colspan="2">&nbsp;</tr>
		
			<?php
			
		}
		else
		{
			
			?>
			
				<input type="hidden" id="cboEmpresa" name="cboEmpresa" value="<?php echo $p_cdEmpresa; ?>" />
			
			<?php
			
		}
		
		
		if ($p_bExibirFiltro){
			
			if (($p_bExibirEmpresa && $p_cdEmpresaAnterior == $p_cdEmpresa) || !$p_bExibirEmpresa)
			{
				
				if ($p_cdFiltro > 0)
				{
					
					$filtro =  new col_filtro();
					$filtro->CD_FILTRO = $p_cdFiltro;
					$filtro = $filtro->obter();
					
					$lotacoes = $filtro->lotacoes;
					$cargos_funcoes = $filtro->cargos_funcoes;
					$usuarios = $filtro->usuarios;
					$provas = $filtro->provas;
					$foruns = $filtro->foruns;
					
				}
				
			}
	
	?>
					<tr>
						<td class="textblk">Filtros Pr�-definidos: &nbsp;</td>
					</tr>
					<tr>
						<td>
							<?php comboboxFiltro("cboFiltro", $p_cdFiltro, $p_cdEmpresa, false, false, true); ?>
						</td>
					</tr>
					<tr><td colspan="2">&nbsp;</tr>
	<?php } ?>	
					<tr>
						<td class="textblk">
							Lota��o: &nbsp;
						</td>
					</tr>
					<tr>
						<td>
							<?php comboboxLotacao("cboLotacao[]",obterValoresLotacao($lotacoes), $p_cdEmpresa, false, true); ?>
						</td>
					</tr>
					<tr><td colspan="2">&nbsp;</tr>
					<tr>
						<td class="textblk">
							Cargo/Fun��o: &nbsp;
						</td>
					</tr>
					<tr>
						<td>
							<?php comboboxCargoFuncao("cboCargo[]",obterValoresCargo($cargos_funcoes), $p_cdEmpresa, false, true); ?>
						</td>
					</tr>
					
					<?php
						if ($p_bExibirUsuario)
						{
					?>
					<tr><td colspan="2">&nbsp;</tr>
					<tr>
						<td class="textblk">
							Usu�rio: &nbsp;
						</td>
					</tr>
					<tr>
						<td>
							<?php comboboxUsuarioEmpresa("cboUsuario[]",obterValoresUsuario($usuarios), $p_cdEmpresa, false, true); ?>
						</td>
					</tr>
					<?php
						}
						if ($p_bExibirProva)
						{
					?>
					<tr><td colspan="2">&nbsp;</tr>
					<tr>
						<td class="textblk">
							Avalia��o: &nbsp;
						</td>
					</tr>
					<tr>
						<td>
							<?php comboboxProvaEmpresa("cboProva[]",obterValoresProva($provas), $p_cdEmpresa, false, !$exibirInativos, true); ?>
						</td>
					</tr>
					
					<?php
						}
					
						if ($p_bExibirForum)
						{
							
					?>
					<tr><td colspan="2">&nbsp;</tr>
					<tr>
						<td class="textblk">
							F�rum: &nbsp;
						</td>
					</tr>
					<tr>
						<td>
							<?php comboboxForum("cboForum[]",obterValoresForum($foruns), $p_cdEmpresa, false, true, $exibirInativos); ?>
						</td>
					</tr>
					
					<?php
						}
					

	}
?>