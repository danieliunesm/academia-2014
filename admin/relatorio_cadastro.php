<?php
include "../include/security.php";
include "../include/defines.php";
include "../include/genericfunctions.php";
include('framework/crud.php');
include('controles.php');

$sqlWhere = "";

if ($_POST["cboEmpresa"] > -1)
{
	$sqlWhere = " AND u.empresa = {$_POST["cboEmpresa"]}";
}

if (is_array($_POST["cboLotacao"]))
{
	
	$lotacoes = join(",", $_POST["cboLotacao"]);
	
	$sqlWhere = "$sqlWhere AND u.lotacao IN ($lotacoes) ";
}

if (is_array($_POST["cboCargo"]))
{
	
	$cargos = join("','", $_POST["cboCargo"]);
	$cargos = "'$cargos'";
	$sqlWhere = "$sqlWhere AND u.cargofuncao IN ($cargos) ";
}

if (is_array($_POST["cboTipoUsuario"]))
{
	$tipos = join(",", $_POST["cboTipoUsuario"]);
	$sqlWhere = "$sqlWhere AND u.tipo IN ($tipos) ";
}

$sql = "SELECT
			NM_USUARIO, login, email, cargofuncao, DS_LOTACAO, tu.NM_TIPO_USU
		FROM
			col_usuario u
			LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
			LEFT JOIN tb_tipo_usuario tu ON tu.id = u.tipo
		WHERE
		 u.Status = 1 $sqlWhere
		ORDER BY
			DS_LOTACAO, NM_USUARIO";

$resultado = DaoEngine::getInstance()->executeQuery($sql, true);

header("Content-Type: application/vnd.ms-excel; name='excel'");

header("Content-disposition:  attachment; filename=Cadastro.xls");

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<meta http-equiv=pragma content=no-cache>

<style type="text/css">
#tabelaCadastro td
{
	font-size:11px;
	font-family:tahoma,arial,helvetica,sans-serif;
	border: 1px solid #999;
	padding:2px 10px 2px 10px;
}

#tabelaCadastro th
{
	font-size:11px;
	font-family:tahoma,arial,helvetica,sans-serif;
	background-color:#ccc;
	border: 1px solid #999;
}
</style>

</head>

<body>

	<table id="tabelaCadastro" border="0" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse">
		<tr class="textblk">
			<th>Unidade Organizacional</th>
			<th>Nome</th>
			<th>Cargo</th>
			<th>E-mail</th>
			<th>Login</th>
			<th>Perfil</th>
		</tr>
		
		<?php 
		
		while ($linha = mysql_fetch_array($resultado)){
			
			echo "
				<tr>
					<td>{$linha["DS_LOTACAO"]}</td>
					<td>{$linha["NM_USUARIO"]}</td>
					<td>{$linha["cargofuncao"]}</td>
					<td>{$linha["email"]}</td>
					<td>{$linha["login"]}</td>
					<td>{$linha["NM_TIPO_USU"]}</td>
				
				</tr>
			
			
			";
			
			
		}
		
		
		
		?>
		
		
	</table>
</body>
<html>