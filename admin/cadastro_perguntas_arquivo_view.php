<?php
//include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";

error_reporting(E_ALL ^ E_NOTICE);

$identificador = isset($_POST['id']) ? $_POST['id'] : '';
$nu_respostas  = isset($_POST['nr']) ? $_POST['nr'] : '';
$file		   = isset($_POST['f'])  ? $_POST['f']  : '';

if($identificador == '' || $nu_respostas == '' || $file == '') die(utf8_encode('� preciso entrar com o Arquivo, identificador das Se��es e respostas/quest�o!'));

$file = '../dbquestions/' . $file;

$titulo		= '';
$secao		= array();
$questao	= array();
$result		= '';

function odt2text($filename) {
    return readZippedXML($filename, "content.xml");
}

function docx2text($filename) {
    return readZippedXML($filename, "word/document.xml");
}

function readZippedXML($archiveFile, $dataFile) {
	$zip = new ZipArchive();
	
	if(true === $zip->open($archiveFile)) {
		if(($index = $zip->locateName($dataFile)) !== false){
			$data = $zip->getFromIndex($index);
			$zip->close();
			$xml = DOMDocument::loadXML($data, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
			
			$content = $xml->saveXML();
			
//			return $content;
			
			$content = str_replace('“', '"', $content);
			$content = str_replace('”', '"', $content);
			$content = str_replace('–', " - ", $content);
			
			$content = str_replace(' xml:space="preserve"', '', $content);
			$content = str_replace('<w:lastRenderedPageBreak/>', '', $content);
			
			$content = str_replace('</w:t></w:r><w:proofErr w:type="gramEnd"/></w:p></w:tc></w:tr>', "\r\n", $content);
			$content = str_replace('</w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc></w:tr>', "\r\n", $content);
			
			$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);//</w:t></w:r></w:p></w:tc><w:tc><w:tcPr>
			$content = str_replace('</w:r></w:p>', "\r\n", $content);//</w:t></w:r></w:p></w:tc></w:tr>
			
			$striped_content = nl2br(strip_tags($content));
			
			return explode('<br />', trim(utf8_decode($striped_content)));
//			return utf8_decode($striped_content);
		}
		$zip->close();
	}
	return "";
}

$s = docx2text($file);

//var_dump($s);
//echo $s;die();

$tituloflag = false;
$k = -1;

for($i = 0; $i < count($s); $i++) {
	if($s[$i] == '') continue;
	if(!$tituloflag){
		$titulo = $s[$i];
		$tituloflag = true;
	}
	if(!(strpos($s[$i], $identificador) === false)) {
		$k++;
		$secao[$k]				= array();
		$secao[$k]['titulo']	= $s[$i];
		$secao[$k]['questao']	= array();
		$it = -1;
		$q  = -1;
		continue;
	}
	if($k >= 0) {
		++$it;
		$item = $it % ($nu_respostas + 2);
		switch($item) {
			case 0:
				$q++;
				$secao[$k]['questao'][$q] = array();
				$secao[$k]['questao'][$q]['enunciado'] = $s[$i];
				$secao[$k]['questao'][$q]['resposta'] = array();
				$r = 0;
			break;
			
			case 1:
				$secao[$k]['questao'][$q]['pergunta'] = (strpos($s[$i], 'XXX') === false) ? $s[$i] : '';
			break;
			
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				$secao[$k]['questao'][$q]['resposta'][$r] = array();
				$secao[$k]['questao'][$q]['resposta'][$r]['certa'] = 0;										// 1
				$secao[$k]['questao'][$q]['resposta'][$r]['texto'] = $s[$i];
				if(!(strpos($s[$i], 'C:') === false)){
					$secao[$k]['questao'][$q]['resposta'][$r]['certa'] = 2;
					$secao[$k]['questao'][$q]['resposta'][$r]['texto'] = str_replace('C: ', '', $s[$i]);
				}
				if(!(strpos($s[$i], 'E:') === false)){
					$secao[$k]['questao'][$q]['resposta'][$r]['certa'] = 1;									// 0
					$secao[$k]['questao'][$q]['resposta'][$r]['texto'] = str_replace('E: ', '', $s[$i]);
				}
				$r++;
			break;
		}
	}
}

$result .= "$titulo <br /><br />";

for($i = 0; $i < count($secao); $i++) {
	
	if($secao[$i]['titulo'] == '') continue;
	
	$result .= 'Se��o: ' . $secao[$i]['titulo'] . '<br />';
	
	$questoes = $secao[$i]['questao'];
	
	for($j = 0; $j < count($questoes); $j++) {
		$result .= '<br />';
		
		if($questoes[$j]['enunciado'] == '') continue;
		
		$result .= "&nbsp;&nbsp;&nbsp;&nbsp;" . $questoes[$j]['enunciado'] . "<br />";
		$result .= "&nbsp;&nbsp;&nbsp;&nbsp;" . $questoes[$j]['pergunta'] . "<br />";
		
		$respostas = $questoes[$j]['resposta'];
		
		for($k = 0; $k < count($respostas); $k++) {
			$result .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . ($respostas[$k]['certa'] > 0 ? 1 : 0) . ' - ';
			$result .= $respostas[$k]['texto'] . '<br />';
		}
	}
	$result .= "<br /><br />";
}

echo utf8_encode($result);

mysql_close();
?>