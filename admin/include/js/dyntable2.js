imageFiles=['images/','bt_up.gif','bt_down.gif','bt_up_ovr.gif','bt_down_ovr.gif'];
preLoad();

function up(tbObj,rObj){
	getIndices(tbObj,rObj);
	if(trInd>(FRind+1)){
		trObjAnt=tableObj.rows[trInd-1];
		tmpObj=ie?trObj.cloneNode(true):trObj;
		ischecked = tableObj.rows[trInd].childNodes[3].childNodes[0].checked;
		tableObj.removeChild(trObj);
		tableObj.insertBefore(tmpObj,trObjAnt);
		resetTable(tableObj,FRind,LRind);
		tmpObj.childNodes[3].childNodes[0].checked = ischecked;
	}

	var rowObj = document.getElementById(rObj);
	if(trInd == FRind + 2){
		var rowObjPost = tableObj.rows[trInd];
		rowObj.childNodes[0].childNodes[0].className = 'menuitem';
		rowObj.childNodes[3].childNodes[0].checked   = false;
		rowObj.childNodes[3].childNodes[0].disabled  = true;
		rowObjPost.childNodes[3].childNodes[0].disabled  = false;
	}
	restoreParentsAndBrothers('tb1');
	return false;
}

function dw(tbObj,rObj){
	getIndices(tbObj,rObj);
	tmpObj=ie?trObj.cloneNode(true):trObj;
	ischecked = tableObj.rows[trInd].childNodes[3].childNodes[0].checked;
	if(trInd<(LRind-2))trObjPost=tableObj.rows[trInd+2];
	else if(trInd<(LRind-1)) trObjPost=tableObj.rows[LRind];
	tableObj.removeChild(trObj);
	tableObj.insertBefore(tmpObj,trObjPost);
	resetTable(tableObj,FRind,LRind);
		
	var rowObj = document.getElementById(rObj);
	if(!(trInd == LRind - 2)){
		rowObj.childNodes[3].childNodes[0].disabled = false;
		rowObj.childNodes[3].childNodes[0].checked  = false;
	}
	if(trInd == FRind + 1){
		var rowObjAnt = tableObj.rows[trInd];
		rowObjAnt.childNodes[0].childNodes[0].className = 'menuitem';
		rowObjAnt.childNodes[3].childNodes[0].checked   = false;
		rowObjAnt.childNodes[3].childNodes[0].disabled  = true; 
	}
	tmpObj.childNodes[3].childNodes[0].checked = ischecked;
	restoreParentsAndBrothers('tb1');
	return false;
}

function getIndices(tbObj,rObj){
	FR=document.getElementById(tbObj+'FR');
	LR=document.getElementById(tbObj+'LR');
	tableObj=document.getElementById(tbObj);
	trObj=document.getElementById(rObj);
	trInd=getRowIndex(tableObj,trObj);
	FRind=getRowIndex(tableObj,FR);
	LRind=getRowIndex(tableObj,LR);
}

function getRowIndex(TblObj,RwObj){
	for(var i=0;i<TblObj.rows.length;i++){
		if(RwObj==TblObj.rows[i]){
			return i;
			break;
		}
	}
	return null;
}

function resetTable(obj,fi,li){
	var cor1=(fi % 2)?'#f1f1f1':'#ffffff';
	var cor2=(fi % 2)?'#ffffff':'#f1f1f1';
	for(var i=fi+1;i<li;i++){
		var bgcolor=(i % 2)?cor1:cor2;
		obj.rows[i].style.backgroundColor=bgcolor;
		document.getElementById(obj.rows[i].id+'upTD').innerHTML='<a href="#" onclick="return up(\''+obj.id+'\',\''+obj.rows[i].id+'\')" onfocus="noFocus(this)" onmouseover="swapImg(\'\',\''+(obj.rows[i].id+'up')+'\',\'bt_up_ovr\')"  onmouseout="swapImg(\'\',\''+(obj.rows[i].id+'up')+'\',\'bt_up\')"><img id="'+(obj.rows[i].id+'up')+'" src="images/bt_up.gif" width="11" height="20"></a>';
		document.getElementById(obj.rows[i].id+'dwTD').innerHTML='<a href="#" onclick="return dw(\''+obj.id+'\',\''+obj.rows[i].id+'\')" onfocus="noFocus(this)" onmouseover="swapImg(\'\',\''+(obj.rows[i].id+'dw')+'\',\'bt_down_ovr\')"  onmouseout="swapImg(\'\',\''+(obj.rows[i].id+'dw')+'\',\'bt_down\')"><img id="'+(obj.rows[i].id+'dw')+'" src="images/bt_down.gif" width="11" height="20"></a>';
	}
	document.getElementById(obj.rows[fi+1].id+'upTD').innerHTML='<img src="images/blank.gif" width="11" height="20">';
	document.getElementById(obj.rows[li-1].id+'dwTD').innerHTML='<img src="images/blank.gif" width="11" height="20">';
}

function restoreParentsAndBrothers(tbObj){
	FR=document.getElementById(tbObj+'FR');
	LR=document.getElementById(tbObj+'LR');
	tableObj=document.getElementById(tbObj);
	FRind=getRowIndex(tableObj,FR);
	LRind=getRowIndex(tableObj,LR);
	var parentRow    = null;
	var brotherRow   = null;
	var submenuIndex = 1;
	for(var i = FRind+1; i < LRind; i++){
		if(!tableObj.rows[i].childNodes[3].childNodes[0].checked){
			parentRow = tableObj.rows[i];
			parentRow.childNodes[3].childNodes[1].value = 1;				// MenuGroup
			parentRow.childNodes[5].childNodes[1].value = 0;				// SubMenuGroup while not childs
			submenuIndex++;
		}
		else{
			brotherRow = tableObj.rows[i];
			brotherRow.childNodes[3].childNodes[1].value = submenuIndex;	// MenuGroup
			parentRow.childNodes[5].childNodes[1].value  = submenuIndex;	// SubMenuGroup
		}
	}
//	for(var i = FRind+1; i < LRind; i++){
//		alert('item: '+tableObj.rows[i].childNodes[0].childNodes[0].innerHTML+'   MenuGroup: '+tableObj.rows[i].childNodes[3].childNodes[1].value+'   SubMenuGroup: '+tableObj.rows[i].childNodes[5].childNodes[1].value);
//	}
}