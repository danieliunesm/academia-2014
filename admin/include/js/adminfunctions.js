agent	= navigator.userAgent.toLowerCase();
version	= parseInt(navigator.appVersion);
ie		= agent.indexOf('msie')!=-1&&(version>=4);
ie5		= ie&&(agent.indexOf('msie 5')!=-1);
ie55	= ie&&(agent.indexOf('msie 5.5')!=-1)
ie6		= ie&&(agent.indexOf('msie 6.0')!=-1);
ie7		= ie&&(agent.indexOf('msie 7')!=-1);
op		= agent.indexOf('opera')!=-1;
mac		= agent.indexOf('mac')!=-1;
//if(!((ie7||ie6||ie55||ie5)&&!op))document.location.replace('/adminnosuportedbrowser.html');

function preLoad(){imagePath=imageFiles[0];for(var i=1;i<imageFiles.length;i++){imageFile=imageFiles[i];imageName=imageFile.substring(0,imageFile.indexOf('.'));eval(imageName+'=new Image();'+imageName+'.src="'+imagePath+imageFile+'"')}}
function swapImg(dynLayer,imgName,imgSrc){
document.getElementById(imgName).src=eval(imgSrc+'.src')}
function noFocus(obj){if(obj.blur())obj.blur()}

function ValidaData(dd,mm,yyyy,DtAtual){
	DtVal=dd+mm+yyyy;
	var msg = "";

	if(dd=="00"||mm=="00"||yyyy=="0000"){
		msg = "Data inv�lida!     \n";
		return msg;
	}
	if(DtVal.length != 8){
		msg = "Preencha a data no formato DD/MM/AAAA!     \n";
		return msg;
	}
	var nChar = null;
	var nCharacteres = 0;
	for (i=0;i<8;i++){
		nChar = DtVal.charAt(i);
		if(isNaN(nChar)){
			msg += nChar;
			nCharacteres +=1;
		}
	}
	if(msg!=""){
		msg = "Caracter" + (nCharacteres>1?"es ":" ") + msg + " na data n�o "  + (nCharacteres>1?"s�o v�lidos":"� v�lido") + ".     \n";
		return msg;
	}
	
	var DtAcerto = null;
	DtAcerto = DtVal.substring(2,4) + "/" + DtVal.substring(0,2) + "/" + DtVal.substring(4,8);
	var bError 		= false;
	var aChar 		= null;
	var holder 		= null;
	var Short 		= null;
	var FebCheck 	= null;
	var NovCheck 	= null;
	var MonthCheck	= null;
    for(i=0;i<10;i++){
		aChar = DtAcerto.charAt(i);
		if(i == 0 && aChar > 1) bError = true;
		if(i == 0 && aChar < 0) bError = true;
		if(i == 0 && aChar == 1) MonthCheck =1;
		if(i == 0 && aChar ==1) NovCheck=1;
		if(i == 1 && aChar < "0" || aChar > "9") bError = true;
		if(i == 1 && aChar == 2) FebCheck=1;
		if(i == 1 && MonthCheck == 1 && aChar >2) bError = true;
		if(i == 1 && aChar == "4" || aChar =="6" || aChar =="9") Short=1;
		if(i == 1 && NovCheck == 1 && aChar == 1) Short=1;
		if(i == 2 && aChar != "/") bError = true;

        if(FebCheck == 1 && NovCheck != 1){
			if(i==3 && aChar > 2) bError = true;
		}
		if (i == 3 && aChar > 3) bError = true;
		if (i == 3 && aChar ==3) holder=1;
		if (i == 4 && aChar >0 && Short==1 && holder==1) bError = true;
		if (i == 4 && aChar >1 && holder==1) bError = true;
		if (i == 4 && aChar < "0" || aChar > "9") bError = true;
		if (i == 5 && aChar != "/")  bError = true;
		if (i == 8 && aChar < "0" || aChar > "9") bError = true;
		if (i == 9 && aChar < "0" || aChar > "9") bError = true;
	}
	if((DtAcerto.substring(0,5) == "02/29") && DtAcerto.substring(6,10) %4 !=0){
		msg = "Data inv�lida. N�o � ano bissexto!     \n";
		return msg;
	}
    if(bError){
		msg = "Data inv�lida!   \n";
		return msg;
	}
	DtInv = parseInt(yyyy+mm+dd);
	DtAtualInv = parseInt(DtAtual.substring(6,10) + DtAtual.substring(3,5) + DtAtual.substring(0,2));
	if(DtInv>DtAtualInv){
		msg = "A data n�o pode ser posterior � data atual!     \n";
		return msg;		
	}
	return msg;
}

function validaCaracteres(ch,objName){
	if(objName=="DD"||objName=="MM"||objName=="YYYY"){
		if(ch>47&&ch<58)return true;
		return false;
	}
	return true;
}

function mascara(e){
	if(window.event){
		if(validaCaracteres(event.keyCode,event.srcElement.name))event.returnValue=true;
		else event.returnValue=false;
	}
}

srcflag=false;
function checkSrc(){
	if(self.event.srcElement.type=='text'||self.event.srcElement.type=='textarea')srcflag=true;
}

function unselectElement(){
	if(srcflag){
		self.event.cancelBubble=true;
		return true;
	}
	else return false;
}

function releaseSrc(){srcflag=false;}