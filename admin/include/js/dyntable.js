imageFiles=['images/','bt_up.gif','bt_down.gif','bt_up_ovr.gif','bt_down_ovr.gif'];
preLoad();

function up(tbObj,rObj){
	getIndices(tbObj,rObj);
	if(trInd>(FRind+1)){
		trObjAnt=tableObj.rows[trInd-1];
		tmpObj=ie?trObj.cloneNode(true):trObj;
		saveChecked(tmpObj.childNodes[0].firstChild.name);
		tableObj.removeChild(trObj);
		tableObj.insertBefore(tmpObj,trObjAnt);
		resetTable(tableObj,FRind,LRind);
		restoreChecked(tmpObj.childNodes[0].firstChild.name);
		restoreLtbItens();
	}
	return false;
}

function dw(tbObj,rObj){
	getIndices(tbObj,rObj);
	tmpObj=ie?trObj.cloneNode(true):trObj;
	saveChecked(tmpObj.childNodes[0].firstChild.name);
	if(trInd<(LRind-2))trObjPost=tableObj.rows[trInd+2];
	else if(trInd<(LRind-1)) trObjPost=tableObj.rows[LRind];
	tableObj.removeChild(trObj);
	tableObj.insertBefore(tmpObj,trObjPost);
	resetTable(tableObj,FRind,LRind);
	restoreChecked(tmpObj.childNodes[0].firstChild.name);
	restoreLtbItens();
	return false;
}

function getIndices(tbObj,rObj){
	FR=document.getElementById(tbObj+'FR');
	LR=document.getElementById(tbObj+'LR');
	tableObj=document.getElementById(tbObj);
	trObj=document.getElementById(rObj);
	trInd=getRowIndex(tableObj,trObj);
	FRind=getRowIndex(tableObj,FR);
	LRind=getRowIndex(tableObj,LR);
}

function getRowIndex(TblObj,RwObj){
	for(var i=0;i<TblObj.rows.length;i++){
		if(RwObj==TblObj.rows[i]){
			return i;
			break;
		}
	}
	return null;
}

function resetTable(obj,fi,li){
	var cor1=(fi % 2)?'#f1f1f1':'#ffffff';
	var cor2=(fi % 2)?'#ffffff':'#f1f1f1';
	for(var i=fi+1;i<li;i++){
		var bgcolor=(i % 2)?cor1:cor2;
		obj.rows[i].style.backgroundColor=bgcolor;
		document.getElementById(obj.rows[i].id+'upTD').innerHTML='<a href="#" onclick="return up(\''+obj.id+'\',\''+obj.rows[i].id+'\')" onfocus="noFocus(this)" onmouseover="swapImg(\'\',\''+(obj.rows[i].id+'up')+'\',\'bt_up_ovr\')"  onmouseout="swapImg(\'\',\''+(obj.rows[i].id+'up')+'\',\'bt_up\')"><img id="'+(obj.rows[i].id+'up')+'" src="images/bt_up.gif" width="11" height="20"></a>';
		document.getElementById(obj.rows[i].id+'dwTD').innerHTML='<a href="#" onclick="return dw(\''+obj.id+'\',\''+obj.rows[i].id+'\')" onfocus="noFocus(this)" onmouseover="swapImg(\'\',\''+(obj.rows[i].id+'dw')+'\',\'bt_down_ovr\')"  onmouseout="swapImg(\'\',\''+(obj.rows[i].id+'dw')+'\',\'bt_down\')"><img id="'+(obj.rows[i].id+'dw')+'" src="images/bt_down.gif" width="11" height="20"></a>';
	}
	document.getElementById(obj.rows[fi+1].id+'upTD').innerHTML='<img src="images/blank.gif" width="11" height="20">';
	document.getElementById(obj.rows[li-1].id+'dwTD').innerHTML='<img src="images/blank.gif" width="11" height="20">';
}

function saveChecked(radioObj){
	iChecked=null;
	for(var i=0;i<6;i++){
		if(document.homeForm[radioObj][i].checked){
			iChecked=i;
			break;
		}
	}
}

function restoreChecked(radioObj){
	document.homeForm[radioObj][iChecked].checked=true;
}

function restoreLtbItens(){
	tbObj = 'tb1';
	FR=document.getElementById(tbObj+'FR');
	LR=document.getElementById(tbObj+'LR');
	tableObj=document.getElementById(tbObj);
	FRind=getRowIndex(tableObj,FR);
	LRind=getRowIndex(tableObj,LR);
	for(var i=LRind-1;i>FRind;i--){
		if(document.getElementById(tableObj.rows[i].id+'LtbTD').firstChild.checked){
			if(i==FRind+1){
				alert('N�o h� not�cia acima que possa ser a not�cia raiz.     \nEsta not�cia ser� reposicionada temporariamente na coluna da esquerda.     ');
				document.getElementById(tableObj.rows[i].id).childNodes[1].firstChild.checked = true;
				document.getElementById(tableObj.rows[i].id+'Content').style.paddingLeft=0;
			}
			else{	
				document.homeForm[document.getElementById(tableObj.rows[i].id).firstChild.firstChild.name][5].value='4,'+
				getParentId(i,FRind,tableObj);
			}
		}
	}
}

function getParentId(k,ki,tb){
	for(var j=k-1;j>ki;j--){
		if(!document.getElementById(tb.rows[j].id+'LtbTD').firstChild.checked){
			if(document.getElementById(tb.rows[j].id+'Col5TD').firstChild.checked){
				alert('Uma chamada na �rea de "Outras not�cias" n�o pode conter uma do tipo "Leia tamb�m".     \nSer� reposicionada temporariamente na coluna da esquerda.     ');
				document.getElementById(tb.rows[j].id).childNodes[1].firstChild.checked = true;
			};
			return tb.rows[j].childNodes[1].id;
		}
	}
}