<?php

class InteracaoProgramada{

    private function listarTodosUsuarios($codigoEmpresa, $tipo = -1, $codigoLotacao, $cargo, $localidade){

        $sql = "SELECT
                    CD_USUARIO, login, email, NM_USUARIO
                FROM
                    col_usuario u
                    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
                WHERE
                    empresa = $codigoEmpresa
                AND Status = 1
                AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
                AND (CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
                AND (cargofuncao = '$cargo' OR '$cargo' = '-1')
                AND (tipo in ($tipo) OR '-1' = '$tipo')
                ";

        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

        //echo $sql;

        return $resultado;

    }

    public function listarParticipantes($codigoEmpresa, $codigoLotacao, $cargo, $localidade){

        return $this->listarTodosUsuarios($codigoEmpresa, 1, $codigoLotacao, $cargo, $localidade);

    }

    public function listarGerentes($codigoEmpresa, $codigoLotacao, $cargo, $localidade){

        return $this->listarTodosUsuarios($codigoEmpresa, -3, $codigoLotacao, $cargo, $localidade);

    }

    public function listarUsuariosAcessoSemPA($codigoEmpresa, $codigoLotacao, $cargo, $localidade, $codigoCiclo = -1){

        $sql = "SELECT
                    DISTINCT u.CD_USUARIO, u.login, u.email, u.NM_USUARIO
                FROM
                    col_usuario u
                    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
                    INNER JOIN col_acesso_diario d ON u.CD_USUARIO = d.CD_USUARIO
                WHERE
                    u.empresa = $codigoEmpresa
                AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
                AND (CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
                AND (cargofuncao = '$cargo' OR '$cargo' = '-1')
                AND u.Status = 1
                AND u.tipo = 1
                AND NOT EXISTS(
                    SELECT
                        1
                    FROM
                        col_prova_aplicada pa
                        INNER JOIN col_assessment a ON a.CD_PROVA = pa.CD_PROVA
                        INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
                    WHERE
                        pa.VL_MEDIA IS NOT NULL
                        AND pa.CD_USUARIO = u.CD_USUARIO
                        AND a.CD_EMPRESA = u.empresa
                        AND (p.CD_CICLO = $codigoCiclo OR $codigoCiclo = -1)
                )";

        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

        return $resultado;

    }

    public function listarUsuariosComPA($codigoEmpresa, $codigoLotacao, $cargo, $localidade, $codigoCiclo = -1){

        $sql = "SELECT
                    DISTINCT u.CD_USUARIO, u.login, u.email, u.NM_USUARIO
                FROM
                    col_usuario u
                    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
                WHERE
                    u.empresa = $codigoEmpresa
                AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
                AND (CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
                AND (cargofuncao = '$cargo' OR '$cargo' = '-1')
                AND u.Status = 1
                AND u.tipo = 1
                AND EXISTS(
                    SELECT
                        1
                    FROM
                        col_prova_aplicada pa
                        INNER JOIN col_assessment a ON a.CD_PROVA = pa.CD_PROVA
                        INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
                    WHERE
                        pa.VL_MEDIA IS NOT NULL
                        AND pa.CD_USUARIO = u.CD_USUARIO
                        AND a.CD_EMPRESA = u.empresa
                        AND (p.CD_CICLO = $codigoCiclo OR $codigoCiclo = -1)
                )";

        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

        return $resultado;

    }

    public function listarUsuariosComPASemSimulado($codigoEmpresa, $codigoLotacao, $cargo, $localidade, $codigoCiclo = -1){

        $sql = "							SELECT
                    DISTINCT u.CD_USUARIO, u.login, u.email, u.NM_USUARIO
                FROM
                    col_usuario u
                    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
                WHERE
                    u.empresa = $codigoEmpresa
                AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
                AND (CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
                AND (cargofuncao = '$cargo' OR '$cargo' = '-1')
                AND u.Status = 1
                AND u.tipo = 1
                AND EXISTS(
                    SELECT
                        1
                    FROM
                        col_prova_aplicada pa
                        INNER JOIN col_assessment a ON a.CD_PROVA = pa.CD_PROVA
                        INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
                    WHERE
                        pa.VL_MEDIA IS NOT NULL
                        AND pa.CD_USUARIO = u.CD_USUARIO
                        AND a.CD_EMPRESA = u.empresa
                        AND (p.CD_CICLO = $codigoCiclo OR $codigoCiclo = -1)
                )
				AND NOT EXISTS(
                    SELECT
                        1
                    FROM
                        col_prova_aplicada pa
                        INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
                    WHERE
                        pa.VL_MEDIA IS NOT NULL
                        AND pa.CD_USUARIO = u.CD_USUARIO
                        AND (p.CD_CICLO = $codigoCiclo OR $codigoCiclo = -1)
						AND p.IN_PROVA = 0
				)";

        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

        return $resultado;

    }

    public function listarUsuariosComPASemAA($codigoEmpresa, $codigoLotacao, $cargo, $localidade, $codigoCiclo = -1){

        $sql = "SELECT
                    DISTINCT u.CD_USUARIO, u.login, u.email, u.NM_USUARIO
                FROM
                    col_usuario u
                    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
                WHERE
                    u.empresa = $codigoEmpresa
                AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
                AND (CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
                AND (cargofuncao = '$cargo' OR '$cargo' = '-1')
                AND u.Status = 1
                AND u.tipo = 1
                AND EXISTS(
                    SELECT
                        1
                    FROM
                        col_prova_aplicada pa
                        INNER JOIN col_assessment a ON a.CD_PROVA = pa.CD_PROVA
                        INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
                    WHERE
                        pa.VL_MEDIA IS NOT NULL
                        AND pa.CD_USUARIO = u.CD_USUARIO
                        AND a.CD_EMPRESA = u.empresa
                        AND (p.CD_CICLO = $codigoCiclo OR $codigoCiclo = -1)
                )
				AND NOT EXISTS(
                    SELECT
                        1
                    FROM
                        col_prova_aplicada pa
                        INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
                        LEFT JOIN col_assessment a ON a.CD_PROVA = pa.CD_PROVA AND a.CD_EMPRESA = $codigoEmpresa
                    WHERE
                        pa.VL_MEDIA IS NOT NULL
                        AND pa.CD_USUARIO = u.CD_USUARIO
                        AND (p.CD_CICLO = $codigoCiclo OR $codigoCiclo = -1)
						AND p.IN_PROVA = 1
						AND a.CD_PROVA IS NULL
				)";

        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

        return $resultado;

    }

    public function listarUsuariosComAANotaMaior($codigoEmpresa, $codigoLotacao, $cargo, $localidade, $codigoCiclo, $nota){

        $sql = "SELECT
                    DISTINCT u.CD_USUARIO, u.login, u.email, u.NM_USUARIO
                FROM
                    col_usuario u
                    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
                    INNER JOIN col_prova_aplicada pa ON pa.CD_USUARIO = u.CD_USUARIO
                    INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
                WHERE
                    u.empresa = $codigoEmpresa
                AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
                AND (CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
                AND (cargofuncao = '$cargo' OR '$cargo' = '-1')
                AND u.Status = 1
                AND u.tipo = 1
                AND p.IN_PROVA = 1
                AND pa.VL_MEDIA >= $nota
                AND (p.CD_CICLO = $codigoCiclo OR $codigoCiclo = -1)
                AND NOT EXISTS(
                    SELECT
                        1
                    FROM
                        col_assessment a
                    WHERE
                        a.CD_EMPRESA = u.empresa AND a.CD_PROVA = p.CD_PROVA
                )
                ";

        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

        return $resultado;

    }

    public function listarUsuariosResponderamPesquisa($codigoEmpresa, $codigoLotacao, $cargo, $localidade, $respondeu = true){

        $notRespondeu = "";

        $codigoPesquisa = $this->obterCodigoPesquisaCorrente($codigoEmpresa);

        if ($codigoPesquisa == '0'){
            return null;
        }

        if(!$respondeu)
            $notRespondeu = "NOT";

        $sql = "SELECT
                  u.CD_USUARIO, u.login, u.email, u.NM_USUARIO
                FROM
                    col_usuario u
                    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
                WHERE
                u.empresa = $codigoEmpresa
                AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
                AND (CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
                AND (cargofuncao = '$cargo' OR '$cargo' = '-1')
                AND u.Status = 1
                AND u.tipo = 1
                AND $notRespondeu EXISTS(
                    SELECT
                        1
                    FROM
                        col_usuario_pesquisa up
                    WHERE
                        up.CD_USUARIO = u.CD_USUARIO
                            AND up.CD_PESQUISA = $codigoPesquisa
                 )";


        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

        return $resultado;

    }

    public function listarUsuariosAcessoMenorQue($codigoEmpresa, $codigoLotacao, $cargo, $localidade, $quantidade){

        $sql = "SELECT
                        u.CD_USUARIO, u.login, u.email, u.NM_USUARIO
                FROM
                        col_usuario u
                        INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
                        LEFT JOIN col_acesso_diario a ON a.CD_USUARIO = u.CD_USUARIO
                WHERE
                u.empresa = $codigoEmpresa
                AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
                AND (CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
                AND (cargofuncao = '$cargo' OR '$cargo' = '-1')
                AND u.Status = 1
                AND u.tipo = 1
                GROUP BY
                    u.CD_USUARIO, u.login, u.email, u.NM_USUARIO
                HAVING COUNT(a.CD_USUARIO) < $quantidade
                ";

        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

        return $resultado;

    }

    private function obterCodigoPesquisaCorrente($codigoEmpresa){
        $sql = "SELECT
                  *
                FROM
                    col_pesquisa
                WHERE
                    CD_EMPRESA = $codigoEmpresa
                AND IN_ATIVO = 1
                AND DATE_FORMAT(DT_INICIO_RELATORIO, '%Y%m%d') <= DATE_FORMAT(SYSDATE(), '%Y%m%d')
                AND DATE_FORMAT(DT_TERMINO_RELATORIO, '%Y%m%d') >= DATE_FORMAT(SYSDATE(), '%Y%m%d')";

        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

        $codigoPesquisa = '0';

        while($linha = mysql_fetch_array($resultado)){

            if($codigoPesquisa == '0'){
            $codigoPesquisa = $linha["CD_PESQUISA"];
            }else{
                $codigoPesquisa = $codigoPesquisa . "," . $linha["CD_PESQUISA"];
            }

        }

        return $codigoPesquisa;

    }

}
?>