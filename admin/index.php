<?
include "../include/defines.php";
include "../include/genericfunctions.php";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="JavaScript">
function validaForm()
{	
	if (document.loginForm.Login.value == "")
	{
		alert('Preencha o campo Login.       ');
		document.loginForm.Login.focus();
		return false;
	}
	if (document.loginForm.Senha.value == "")
	{
		alert('Preencha o campo Senha.       ');
		document.loginForm.Senha.focus();
		return false;
	}
	document.loginForm.submit();
}

function checkKey()
{
	key=event.keyCode;
	if(key==16)return false;
	if(key==9&&event.srcElement.tabIndex==2)setTimeout('document.loginForm.Login.focus()',50);
	if(key==13)document.getElementById('submitbutton').click();
}

function init()
{
	if(self.opener)
	{
		self.opener.location.href='/admin/index.php';self.close()
	}
	document.onkeydown = checkKey;
	document.loginForm.Login.focus();
}
</script>
</head>
<body onload="init()">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<br><br><br>
<form name="loginForm" action="login.php" method="post">
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%" class="textbld" align="right">Login:&nbsp;</td><td><input type="text" class="textbox2" name="Login" size="9" maxlength="50" tabIndex="1"></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="122" height="5"></td></tr>
<tr>
<td width="1%" class="textbld" align="right">Senha:&nbsp;</td><td><input type="password" class="textbox2" name="Senha" size="9" maxlength="8" tabIndex="2"></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
<tr>
<td></td><td><input type="button" id="submitbutton" class="buttonsty2" value="Login" onclick="validaForm()" onfocus="noFocus(this)"></td>
</tr>
</table>
</form>
</body>
</html>
