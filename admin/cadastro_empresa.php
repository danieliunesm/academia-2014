<?php

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_empresa();
}

function carregarRN(){
	
	$rn = new col_empresa();
	
	page::$rn->CD_EMPRESA = $_REQUEST["id"];
	page::$rn->DS_EMPRESA = $_POST["txtEmpresa"];
	page::$rn->CD_CLIENTE = $_POST["cboCliente"];
	page::$rn->DT_INICIO_VIGENCIA = $_POST["txtDataInicioVigencia"];
	page::$rn->DT_TERMINO_VIGENCIA = $_POST["txtDataTerminoVigencia"];
	page::$rn->TX_EMAIL_PROGRAMA = $_POST["txtEmailPrograma"];
	page::$rn->DS_IP = $_POST["txtIP"];
	page::$rn->DT_INICIO_CADASTRO = $_POST["txtDataInicioCadastro"];
	page::$rn->DT_TERMINO_CADASTRO = $_POST["txtDataTerminoCadastro"];
	page::$rn->TX_BLOG_PROGRAMA = $_POST["txtBlogPrograma"] != "-1" ? $_POST["txtBlogPrograma"] : "";
	page::$rn->IN_ASSESSMENT = $_POST["rdoAssessment"];
    page::$rn->TX_DOMINIO = $_POST["txtDominio"];
    page::$rn->IN_OBRIGA_DIAGNOSTICO = $_POST["rdoDiagnostico"];

	$ciclos = array();
	
	$nomesCiclo = $_POST["txtCiclo"];
	$inicioCiclos = $_POST["txtDataInicio"];
	$terminoCiclos = $_POST["txtDataTermino"];
	$codigosCiclo = $_POST["hdnCodigoCiclo"];
	
	
	for ($i = 0; $i < count($nomesCiclo); $i++){
        
		$ciclo = new col_ciclo();
		$ciclo->NM_CICLO = $nomesCiclo[$i];
		$ciclo->CD_EMPRESA = $_POST["cboEmpresa"];
		$ciclo->DT_INICIO = $inicioCiclos[$i];
		$ciclo->DT_TERMINO = $terminoCiclos[$i];
		$ciclo->CD_CICLO = $codigosCiclo[$i];
			
		$ciclos[] = $ciclo;
	}
	
	page::$rn->ciclos = $ciclos;
	
}

function pagePreRender()
{
	page::$enctype = ' enctype="multipart/form-data" ';
}


function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/admin/cadastro_empresa.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

function pageRenderEspecifico(){
	
	$id = $_REQUEST["id"];
	
?>	
				<TR>
						<TD class="textblk" colspan="2">Empresa:</TD>
					</TR>
					<TR>
						<TD colspan="2">
							<?php comboboxCliente("cboCliente",  page::$rn->CD_CLIENTE, "Cliente"); ?>
						</TD>
					</TR>
					<TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<TD class="textblk" colspan="2">Nome do Programa: *</TD>
				</TR>
				<TR>
					<TD colspan="2"><?php textbox("txtEmpresa", "100", page::$rn->DS_EMPRESA, "100%", "textbox3", "Empresa"); ?></TD>
				</TR>
				<TR><TD colspan="2">&nbsp;</TD></TR>
                <tr>
                    <td class="textblk" colspan="2">Dom�nio do Programa (sem o www):</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php textbox("txtDominio", "100", page::$rn->TX_DOMINIO, "100%", "textbox3", "Dom�nio do Programa"); ?>
                    </td>
                </tr>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<TD class="textblk" colspan="2">Somente Assessment: *</TD>
				</TR>
				<TR>
					<TD colspan="2" class="textblk">
					
					<?php
	                                    $assessmentSim = "";
	                                    $assessmentNao = "";
	                                    
	                                    if (page::$rn->IN_ASSESSMENT == 1){
	                                         $assessmentSim = "checked";
	                                    }else{
	                                         $assessmentNao = "checked";
	                                    }
	                                    
	                                ?>
	                                    <INPUT name="rdoAssessment" type="radio" value="0" <?php echo $assessmentNao ?>/>N�o&nbsp;&nbsp;<INPUT name="rdoAssessment" type="radio" value="1" <?php echo $assessmentSim ?>/>Sim
					            	
					
					</TD>
				</TR>
                <TR><TD colspan="2">&nbsp;</TD></TR>
                <TR>
                    <TD class="textblk" colspan="2">Obriga o Diagn�stico: *</TD>
                </TR>
                <TR>
                    <TD colspan="2" class="textblk">

                        <?php
                        $diagnosticoSim = "";
                        $diagnosticoNao = "";

                        if (page::$rn->IN_OBRIGA_DIAGNOSTICO == 1){
                            $diagnosticoSim = "checked";
                        }else{
                            $diagnosticoNao = "checked";
                        }

                        ?>
                        <INPUT name="rdoDiagnostico" type="radio" value="0" <?php echo $diagnosticoNao ?>/>N�o&nbsp;&nbsp;<INPUT name="rdoDiagnostico" type="radio" value="1" <?php echo $diagnosticoSim ?>/>Sim


                    </TD>
                </TR>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				
				
				<TR>
					<TD class="textblk" colspan="2">Logotipo da Empresa: (Marca d'�gua canal) *</TD>
				</TR>
				<TR>
					<TD class="textblk" colspan="2">
					<input type="file" class="textbox8e" name="flLogo" size="26" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="100" tabIndex="35">
					<?php
						if ($id != "" && file_exists(getDocumentRoot()."/canal/images/empresas/logo$id.png"))
						{
							echo "<a href='/canal/images/empresas/logo$id.png' target='foto'>Visualizar Logotipo</a>";
						};
					?>
					</TD>
				</TR>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<TD class="textblk" colspan="2">Imagem da Empresa: (Boas Vindas) *</TD>
				</TR>
				<TR>
					<TD class="textblk" colspan="2">
					<input type="file" class="textbox8e" name="flBoasVindas" size="26" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="100" tabIndex="35">
					<?php
						if ($id != "" && file_exists(getDocumentRoot()."/canal/images/empresas/boasvindas$id.png"))
						{
							echo "<a href='/canal/images/empresas/boasvindas$id.png' target='foto'>Visualizar Logotipo</a>";
						};
					?>
					</TD>
				</TR>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<tr>
					<td class="textblk" colspan="2">
						Per�odo de Vig�ncia: *
					</td>
				</tr>
				<tr>
					<td class="textblk" colspan="2">
						<?php textbox("txtDataInicioVigencia", "10", page::$rn->DT_INICIO_VIGENCIA, "110px", "textbox3", "In�cio Vig�ncia", Mascaras::Data) ?>
						a
						<?php textbox("txtDataTerminoVigencia", "10", page::$rn->DT_TERMINO_VIGENCIA, "110px", "textbox3", "Termino Vig�ncia", Mascaras::Data) ?>
					</td>
				</tr>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<tr>
					<td class="textblk" colspan="2">
						E-mail do Programa: *
					</td>
				</tr>
				<tr>
					<td class="textblk" colspan="2">
						<?php textbox("txtEmailPrograma", "200", page::$rn->TX_EMAIL_PROGRAMA, "100%", "textbox3", "E-mail do Programa") ?>
					</td>
				</tr>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<tr>
					<td class="textblk" colspan="2">
						Blog do Programa:
					</td>
				</tr>
				<tr>
					<td class="textblk" colspan="2">
						<?php echo 'http://'.$_SERVER["SERVER_NAME"].'/blog/';
						comboboxBlog("txtBlogPrograma",  page::$rn->TX_BLOG_PROGRAMA, false); ?>
					</td>
				</tr>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<tr>
					<td class="textblk" colspan="2">
						IP's Permitidos: (Se nenhum marcado o sistema n�o valida o IP do usu�rio. Separar os IP's por "," (v�rgula))
					</td>
				</tr>
				<tr>
					<td class="textblk" colspan="2">
						<?php textbox("txtIP", "300", page::$rn->DS_IP, "100%", "textbox3") ?>
					</td>
				</tr>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<tr>
					<td class="textblk" colspan="2">
						Per�odo para Cadastro Online:
					</td>
				</tr>
				<tr>
					<td class="textblk" colspan="2">
						A Partir de
						<?php
							if (page::$rn->DT_INICIO_CADASTRO == '31/12/1969')
								page::$rn->DT_INICIO_CADASTRO = "";
							
							textbox("txtDataInicioCadastro", "10", page::$rn->DT_INICIO_CADASTRO, "110px", "textbox3", 0, Mascaras::Data)
							
						?>
						at�
						<?php 
							if (page::$rn->DT_TERMINO_CADASTRO == '31/12/1969')
								page::$rn->DT_TERMINO_CADASTRO = "";
						
						
							textbox("txtDataTerminoCadastro", "10", page::$rn->DT_TERMINO_CADASTRO, "110px", "textbox3", 0, Mascaras::Data)
							
						?>
					</td>
				</tr>
				
				
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<td colspan="2" class="textblk"><a href="javascript:adicionarCiclo()">Adicionar Ciclos</a></td>
					
				</TR>
				<tr>
					<td colspan="2" class="textblk">
					
						<script type="text/javascript" language="javascript">
						
							function adicionarCiclo(){
							
								var no = document.getElementById("dvCiclo").cloneNode(true);
								var dv = document.getElementById("dvCiclos");
								
								//alert(no.all.length)
								
								var itens = no.all.length;
								
								for (i=0; i<itens; i++){
									if (no.all[i].type == "text"){
										no.all[i].value = "";
									}else if (no.all[i].type == "select-one"){
										no.all[i].selectedIndex = 0;
									}else if (no.all[i].type == "hidden"){
										no.all[i].value = "";
									}
								}
								
								dv.appendChild(no);
								
							}
							
							function removerCiclo(obj){
								if (document.getElementById("dvCiclos").children.length==1){
									alert('Deve existir ao menos um ciclo.')
									return;
								}
								
								 document.getElementById("dvCiclos").removeChild(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
								
								//alert(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
									
							}
						
						
						</script>
					
					
						<div id="dvCiclos">
							<?php
								$linhas = 1;
								$ciclos = page::$rn->ciclos;
								
								if (is_array($ciclos))
								{
									if (count($ciclos) > 1)
										$linhas = count($ciclos);
										
									usort($ciclos, "ordenarCiclos");
									
								}
								
								for ($i=0; $i < $linhas; $i++)
								{
									$nomeCiclo = "";
									$dataInicio = "";
									$dataTermino = "";
									$codigoCiclo = "";
									
									if (is_array($ciclos))
									{
										$ciclo = $ciclos[$i];
										$nomeCiclo = $ciclo->NM_CICLO;
										$dataInicio = $ciclo->DT_INICIO;
										$dataTermino = $ciclo->DT_TERMINO;
										$codigoCiclo = $ciclo->CD_CICLO;
										
									}
									
								
							
							?>
						
							<div id="dvCiclo" style="border: 1px solid black; margin-top: 2px;">
								<table cellpadding="2" cellspacing="0" width="98%">
									<tr>
										<td class="textblk" >Ciclo:</td>
										<td rowspan="4" class="textblk" align="center"><input type="image" src="/images/layout/bt_delsession.gif" value="" onclick="javascript:removerCiclo(this)" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'"></td>
									</tr>												
									<tr>
										<td class="textblk" ><?php textbox("txtCiclo[]", "100", $nomeCiclo, "100%", "textbox3", "Ciclo"); ?></td>
									</tr>
									<tr>
										<td class="textblk" >Per�odo:</td>
									</tr>
									<tr>
										<td class="textblk" >
											De <?php textbox("txtDataInicio[]", "10", $dataInicio, "100px", "textbox3", "Data de In�cio", Mascaras::Data) ?>
											At�	<?php textbox("txtDataTermino[]", "10", $dataTermino, "100px", "textbox3", "Data de T�rmino", Mascaras::Data) ?>
											<input type="hidden" id="hdnCodigoCiclo[]" name="hdnCodigoCiclo[]" value="<?php echo $codigoCiclo; ?>" />
										</td>
									</tr>
								</table>
							</div>
								<?php
								}
								?>
							
						</div>
						
					</td>
				</tr>
<?php

}

function ordenarCiclos($a, $b)
{
	return ordenarDados($a, $b, "DT_INICIO");
}

function ordenarDados($a, $b, $campo)
{
	$dataA = conveterDataParaYMD($a->DT_INICIO);
	$dataB = conveterDataParaYMD($b->DT_INICIO);
	
	if ($dataA == $dataB)
		return 0;
		
	return ($dataA > $dataB) ? +1:-1;
}


?>