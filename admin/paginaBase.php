<?php

include('page.php');

if (!(isset($definesIncluido) && $definesIncluido)){
	include_once("../include/defines.php");
}

include('framework/crud.php');
include('controles.php');

page::$isPostBack = count($_POST) > 0 ? true : false;

function pageLoad(){
	
	if (page::$isPostBack){
		
		if (isset($_POST["btnSalvar"])){
			save();
		}
		
	}else{
		if (isset($_REQUEST["id"]) &&  $_REQUEST["id"] != ""){
			obter();	
		}
		
	}
	
}

function save(){

	instanciarRN();
	
	carregarRN();
	
	if (isset($_REQUEST["id"]) &&  $_REQUEST["id"] != ""){
		page::$rn = page::$rn->alterar();
	}else{
		page::$rn = page::$rn->inserir();
	}
	
	if (function_exists('pageDepoisDeSalvar')){
		pageDepoisDeSalvar();
	}

	returnCadastro();
	
}

function obter(){
	
	instanciarRN();
	carregarRN();
	page::$rn = page::$rn->obter();
	
	//$linha = mysql_fetch_array($resultado);
	
	//carregarInterface($linha);
	
}

function pageRender(){
	
	if (function_exists('pagePreRender')){
		pagePreRender();
	}
	
	include('cadastro_cabecalho.php');

	pageRenderEspecifico();

	include('cadastro_rodape.php');
}

function returnCadastro(){
	
	//exit();
	
	echo "<head>
		<script language=\"JavaScript\">
		//if(self.opener&&!self.opener.closed)self.opener.location.reload();
		if(self.opener&&!self.opener.closed)self.opener.document.forms[0].submit();
		else alert('Banco atualizado com sucesso!     ');
		self.close();</script>
		</head>";
	
	exit();
	
}

pageLoad();
pageRender();

?>