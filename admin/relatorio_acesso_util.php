<?php

	function obterTotaisUsuariosAcumuladoCiclo()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		$sql = "SELECT
					NM_CICLO, SUM(QT_PARTICIPANTE) AS QD_PARTICIPANTE, SUM(QT_CADASTRADO) AS QD_CADASTRADO, DT_INICIO, CICLO_VALIDO
				FROM
					(
						SELECT
  							c.NM_CICLO, COUNT(DISTINCT u.CD_USUARIO) AS QT_PARTICIPANTE, NULL AS QT_CADASTRADO, DT_INICIO, 1 AS CICLO_VALIDO
						FROM
  							col_acesso_diario a
  							INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
  							INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
							INNER JOIN col_ciclo c ON DATE_FORMAT(c.DT_INICIO,'%Y%m%d') <= DATE_FORMAT(DT_ACESSO,'%Y%m%d') AND DATE_FORMAT(DT_TERMINO,'%Y%m%d') >= DATE_FORMAT(DT_ACESSO,'%Y%m%d') AND c.CD_EMPRESA = u.empresa
						WHERE
	    					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
						  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
						 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		
						  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  			AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
						GROUP BY
						    c.CD_CICLO, c.NM_CICLO, DT_INICIO
						
						UNION ALL
						
						SELECT
							c.NM_CICLO,
							NULL AS TOTAL,
							COUNT(1),
							DT_INICIO,
							NULL AS CICLO_VALIDO
						FROM
							col_ciclo c
							INNER JOIN col_usuario u ON u.empresa = c.CD_EMPRESA
							INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
						WHERE
						    (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
						    
						    AND DATE_FORMAT(u.datacadastro,'%Y%m%d') <= DATE_FORMAT(DT_TERMINO,'%Y%m%d')
			
						  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
						 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		
						  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
						  	
						GROUP BY
							c.NM_CICLO, DT_INICIO
						) AS TABELA
						GROUP BY
  							NM_CICLO, DT_INICIO
						ORDER BY
  							DT_INICIO
						    ";
		
		//echo $sql;
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			if ($linha["CICLO_VALIDO"] == 1)
			{
				$resultados[$linha["NM_CICLO"]]["QD_PARTICIPANTE"] = $linha["QD_PARTICIPANTE"];
				$resultados[$linha["NM_CICLO"]]["QD_CADASTRADO"] = $linha["QD_CADASTRADO"];
			}
			
			
		}
		
		return $resultados;
		
		
	}
	
	
	function obterTotalAcessosCiclo()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		//DATE_FORMAT(c.DT_INICIO,'%Y%m%d') <= DATE_FORMAT(pr.DT_INICIO,'%Y%m%d') AND DATE_FORMAT(c.DT_TERMINO,'%Y%m%d') >= DATE_FORMAT(pr.DT_INICIO,'%Y%m%d') AND u.empresa = c.CD_EMPRESA
		
		$sql = "SELECT
				    IN_ORDENAR, NM_CICLO, TOTAL
				FROM
				(
				SELECT
				    IN_ORDENAR, NM_CICLO, COUNT(DISTINCT DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'), a.CD_USUARIO) AS TOTAL
				FROM
				    col_acesso_diario a
				    INNER JOIN col_pagina_acesso pa ON pa.CD_PAGINA_ACESSO = a.CD_PAGINA_ACESSO
				    INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
				    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
            		INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND DATE_FORMAT(c.DT_INICIO,'%Y%m%d') <= DATE_FORMAT(DT_ACESSO,'%Y%m%d') AND DATE_FORMAT(c.DT_TERMINO,'%Y%m%d') >= DATE_FORMAT(DT_ACESSO,'%Y%m%d') AND u.empresa = c.CD_EMPRESA
				WHERE
				    IN_ORDENAR < 999  AND IN_ORDENAR NOT IN (1,6,7)
					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
				  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
				  	AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    IN_ORDENAR,  NM_CICLO
				    
				UNION ALL
				
					SELECT
				    5 AS IN_ORDENAR, NM_CICLO, COUNT(1) AS TOTAL
				FROM
				    tb_forum f
				    INNER JOIN col_usuario u ON u.empresa = f.cd_empresa and u.login = f.name
				    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				    INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND DATE_FORMAT(c.DT_INICIO,'%Y%m%d') <= DATE_FORMAT(f.data,'%Y%m%d') AND DATE_FORMAT(c.DT_TERMINO,'%Y%m%d') >= DATE_FORMAT(f.data,'%Y%m%d') AND u.empresa = c.CD_EMPRESA
				WHERE
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(f.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(f.data, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    NM_CICLO
				    
				UNION ALL

				SELECT
          			6 AS IN_ORDENAR, NM_CICLO, COUNT(1) AS TOTAL
        		FROM
          			col_prova_realizada pr
          			INNER JOIN col_prova p ON p.CD_PROVA = pr.CD_PROVA
  		    		  INNER JOIN col_usuario u ON pr.CD_USUARIO = u.CD_USUARIO
  		    		  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
    				    INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND c.CD_CICLO = p.CD_CICLO
				WHERE
          			p.IN_PROVA = 0 AND
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(c.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(c.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				  	
				  	GROUP BY
				      NM_CICLO
				    
				UNION ALL

				SELECT
          			7 AS IN_ORDENAR, NM_CICLO, COUNT(1) AS TOTAL
        		FROM
          				col_prova_realizada pr
          				INNER JOIN col_prova p ON p.CD_PROVA = pr.CD_PROVA
    		    		INNER JOIN col_usuario u ON pr.CD_USUARIO = u.CD_USUARIO
    		    		INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
    				    INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND c.CD_CICLO = p.CD_CICLO
				WHERE
          			p.IN_PROVA = 0 AND pr.VL_MEDIA >= p.VL_MEDIA_APROVACAO AND
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(c.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(c.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				  	
				  	GROUP BY
				      NM_CICLO

				)
				AS
				    TABELA
				GROUP BY
				    IN_ORDENAR, NM_CICLO
				ORDER BY NM_CICLO";
		
		//echo "<!-- $sql -->";
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$indice = "{$linha["IN_ORDENAR"]}{$linha["NM_CICLO"]}";
			$resultados[$indice] = $linha["TOTAL"];
			
		}
		
		return $resultados;
		
	}
	
	
	function obterProvasMediaCiclo($tipo, $retirarAssessment = true)
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery, $codigoCiclo, $localidade;
		
		if ($codigoCiclo == "")
			$codigoCiclo = -1;
			
		$codigoAssessment = obterProvasQueryAssessment($tipo, $codigoEmpresa, $retirarAssessment);
		
		$sql = "SELECT
			          
					COUNT(pr.DT_INICIO) AS QTD_AVALIACAO,
					COUNT(IF(pr.VL_MEDIA >= p.VL_MEDIA_APROVACAO,1,NULL)) AS QTD_AVALIACAO_SETE,
					COUNT(IF(pr.VL_MEDIA >= 9,1,NULL)) AS QTD_AVALIACAO_NOVE,
					AVG(pr.VL_MEDIA) AS VL_MEDIA,
					
					COUNT(DISTINCT u.CD_USUARIO) AS QTD_PARTICIPANTE_AVALIACAO
				FROM
					col_prova_realizada pr
					INNER JOIN col_prova p ON pr.CD_PROVA = p.CD_PROVA
					INNER JOIN col_usuario u ON u.CD_USUARIO = pr.CD_USUARIO
					INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
          			LEFT JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND p.CD_CICLO = c.CD_CICLO
				WHERE
 					p.CD_PROVA IN ($codigoAssessment)
 					
					AND pr.DT_INICIO IS NOT NULL AND pr.VL_MEDIA IS NOT NULL

					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				 	
				 	AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
				  	
				  	
				  	
				  	AND	(DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				  		
				  	
				";
		
		//AND (p.CD_CICLO = $codigoCiclo OR (($codigoCiclo = -1 OR p.CD_CICLO IS NULL)
		//echo "<!--$sql-->";
		
		//u.CD_USUARIO,
		//u.CD_USUARIO,
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		global $mediaGeralProvasCalculada, $quantidadeTotalAvaliacoesMaisSegundaChamada, $quantidadeTotalAvaliacoeAcimaSeteSegundaChamada, $quantidadeParticipanteAvaliacao, $quantidadeTotalAvaliacoeAcimaNoveSegundaChamada;
		$mediaGeralProvasCalculada = 0;
		$ciclos = 0;
		
		while ($linha = mysql_fetch_array($resultado))
		{
			$indice = "{$linha["NM_CICLO"]}";
			$resultados[$indice]["VL_MEDIA"] = str_ireplace(".",",",round($linha["VL_MEDIA"] * 10, 2));
			//$resultados[$indice]["VL_MEDIA"] = $linha["VL_MEDIA"] * 10;
			$resultados[$indice]["QTD_AVALIACAO"] = $linha["QTD_AVALIACAO"];
			$resultados[$indice]["QTD_AVALIACAO_SETE"] = $linha["QTD_AVALIACAO_SETE"];
			$resultados[$indice]["QTD_AVALIACAO_NOVE"] = $linha["QTD_AVALIACAO_NOVE"];
			
			$mediaGeralProvasCalculada = $mediaGeralProvasCalculada + $linha["VL_MEDIA"];
			$quantidadeTotalAvaliacoesMaisSegundaChamada = $quantidadeTotalAvaliacoesMaisSegundaChamada + $linha["QTD_AVALIACAO"];
			$quantidadeTotalAvaliacoeAcimaSeteSegundaChamada = $quantidadeTotalAvaliacoeAcimaSeteSegundaChamada + $linha["QTD_AVALIACAO_SETE"];
			$quantidadeTotalAvaliacoeAcimaNoveSegundaChamada = $quantidadeTotalAvaliacoeAcimaNoveSegundaChamada + $linha["QTD_AVALIACAO_NOVE"];
			$quantidadeParticipanteAvaliacao = $quantidadeParticipanteAvaliacao + $linha["QTD_PARTICIPANTE_AVALIACAO"];
			$ciclos++;
			//echo $quantidadeTotalAvaliacoesMaisSegundaChamada;
			//exit();
			
		}
		
		if ($ciclos > 0)
		{
			$mediaGeralProvasCalculada = $mediaGeralProvasCalculada / $ciclos;
			$mediaGeralProvasCalculada = str_ireplace(".",",",round($mediaGeralProvasCalculada * 10, 2));
		}
			
		
		return $resultados;
		
	}

	
	function obterTotaisUsuariosDia()
	{
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery, $codigoCiclo, $localidade;
		
		$sql = "SELECT
    				DT_ACESSO, COUNT(if(tipo=1,1,null)) AS QTD_PARTICIPANTE, COUNT(if(NOT tipo=1,1,null)) AS QTD_PARTICIPANTE_SUPERUSUARIO, NULL AS QTD_CADASTRADO, SUM(ELEGIVEL) AS QTD_ELEGIVEL, QTD_CADASTRADO_ELEGIVEL
				FROM
				(
					SELECT
    					a.CD_USUARIO,  MIN(DATE_FORMAT(DT_ACESSO,'%Y%m%d'))  AS DT_ACESSO,
    					IF(EXISTS(SELECT 1 FROM col_prova_aplicada pa INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA AND p.IN_PROVA = 1 WHERE pa.CD_USUARIO = a.CD_USUARIO AND (p.CD_CICLO = $codigoCiclo OR $codigoCiclo = -1)) AND u.tipo = 1,1,0) AS ELEGIVEL,
    					NULL AS QTD_CADASTRADO_ELEGIVEL,
    					tipo
					FROM
    					col_acesso_diario a
    					INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
    					LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
    				WHERE
    				
	    				(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
	    				
	    			AND (u.Status = 1)
			
				  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				 	
				 	AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
				  	
					GROUP BY
    					CD_USUARIO
				)
				AS
    				TABELA
				GROUP BY
    				DT_ACESSO
    				
    				
    			UNION
    			
				SELECT
					DATE_FORMAT(datacadastro,'%Y%m%d')  AS DT_ACESSO, NULL, NULL, COUNT(1), 
					NULL,
					COUNT(IF(EXISTS(SELECT 1 FROM col_prova_aplicada pa INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA AND p.IN_PROVA = 1 WHERE pa.CD_USUARIO = u.CD_USUARIO AND (p.CD_CICLO = $codigoCiclo OR $codigoCiclo = -1) ) AND u.tipo = 1,1,NULL)) AS QTD_CADASTRADO_ELEGIVEL
				FROM
  					col_usuario u
  					LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				WHERE
    				(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
    				
    				AND (u.Status = 1)
					
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				 	
				 	AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				GROUP BY DT_ACESSO
    				
				ORDER BY
    				DT_ACESSO";
		
		//echo "<!--$sql-->";
		//exit();
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			if ($linha["QTD_PARTICIPANTE"] != null)
				$resultados[$linha["DT_ACESSO"]]["QTD_PARTICIPANTE"] = $linha["QTD_PARTICIPANTE"];
				
			if ($linha["QTD_CADASTRADO"] != null)
				$resultados[$linha["DT_ACESSO"]]["QTD_CADASTRADO"] = $linha["QTD_CADASTRADO"];
				
			if ($linha["QTD_ELEGIVEL"] != null)
				$resultados[$linha["DT_ACESSO"]]["QTD_ELEGIVEL"] = $linha["QTD_ELEGIVEL"];
				
			if ($linha["QTD_CADASTRADO_ELEGIVEL"] != null)
				$resultados[$linha["DT_ACESSO"]]["QTD_CADASTRADO_ELEGIVEL"] = $linha["QTD_CADASTRADO_ELEGIVEL"];
				
			if ($linha["QTD_PARTICIPANTE_SUPERUSUARIO"] != null)
				$resultados[$linha["DT_ACESSO"]]["QTD_PARTICIPANTE_SUPERUSUARIO"] = $linha["QTD_PARTICIPANTE_SUPERUSUARIO"];
			
		}
		
		return $resultados;
		
		
	}
	
		
	function obterTotaisUsuariosAcumuladoDia()
	{
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		
		$sql = "SELECT
    				DT_ACESSO, COUNT(1) AS TOTAL
				FROM
				(
					SELECT
    					a.CD_USUARIO,  MIN(DATE_FORMAT(DT_ACESSO,'%Y%m%d'))  AS DT_ACESSO
					FROM
    					col_acesso_diario a
    					INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
    					INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
    				WHERE
    				
	    				(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
    				
					GROUP BY
    					CD_USUARIO
				)
				AS
    				TABELA
				GROUP BY
    				DT_ACESSO
				ORDER BY
    				DT_ACESSO";
		
		
		//echo $sql;
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$resultados[$linha["DT_ACESSO"]] = $linha["TOTAL"];
			
		}
		
		return $resultados;
		
	}


	function obterTotalAcessosDia()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery, $localidade;
		
		$sql = "SELECT
				    IN_ORDENAR, DT_ACESSO, TOTAL, TOTAL_APROVADO
				FROM
				(
				SELECT
				    IN_ORDENAR, DATE_FORMAT(DT_ACESSO, '%Y%m%d') AS DT_ACESSO, COUNT(DISTINCT DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'), a.CD_USUARIO) AS TOTAL, NULL AS TOTAL_APROVADO
				FROM
				    col_acesso_diario a
				    INNER JOIN col_pagina_acesso pa ON pa.CD_PAGINA_ACESSO = a.CD_PAGINA_ACESSO
				    INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
				    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				WHERE
				    IN_ORDENAR < 999  AND IN_ORDENAR NOT IN (1,6,7)
					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
				  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				 	AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
				  	AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    IN_ORDENAR,  DATE_FORMAT(DT_ACESSO, '%Y%m%d')
				    
				UNION ALL
				
				SELECT
				    5 AS IN_ORDENAR, DATE_FORMAT(f.data, '%Y%m%d')  AS DT_ACESSO, COUNT(1) AS TOTAL, NULL AS TOTAL_APROVADO
				FROM
				    tb_forum f
				    INNER JOIN col_usuario u ON u.empresa = f.cd_empresa and u.login = f.name
				    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				WHERE
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				 	
				 	AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(f.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(f.data, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    DATE_FORMAT(data, '%Y%m%d') 
				    
				UNION ALL

				SELECT
          			6 AS IN_ORDENAR,
          			DATE_FORMAT(pr.DT_INICIO, '%Y%m%d')  AS DT_ACESSO,
          			COUNT(1) AS TOTAL,
          			COUNT(IF(pr.VL_MEDIA >= p.VL_MEDIA_APROVACAO, 1, NULL)) AS TOTAL_APROVADO
        		FROM
          			col_prova_realizada pr
          			INNER JOIN col_prova p ON p.CD_PROVA = pr.CD_PROVA
  		    		INNER JOIN col_usuario u ON pr.CD_USUARIO = u.CD_USUARIO
  		    		INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				WHERE
          			p.IN_PROVA = 0 AND
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				 	
				 	AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				  	
				  	GROUP BY
				    DATE_FORMAT(pr.DT_INICIO, '%Y%m%d')
				    
				UNION ALL
				
				SELECT
					8 AS IN_ORDENAR,
					DATE_FORMAT(up.DT_RESPOSTA, '%Y%m%d')  AS DT_ACESSO,
          			COUNT(1) AS TOTAL,
          			NULL AS TOTAL_APROVADO
          		FROM
          			col_usuario_pesquisa up
          			INNER JOIN col_usuario u ON up.CD_USUARIO = u.CD_USUARIO
  		    		INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  		    	WHERE
          			(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				 	
				 	AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(up.DT_RESPOSTA, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(up.DT_RESPOSTA, '%Y%m%d') <= '$dataFinalQuery')
				  	
				  GROUP BY
				  	
				    DATE_FORMAT(up.DT_RESPOSTA, '%Y%m%d')
				
				)
				AS
				    TABELA
				GROUP BY
				    IN_ORDENAR, DT_ACESSO
				ORDER BY DT_ACESSO";
		
		
		//echo $sql;
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$indice = "{$linha["IN_ORDENAR"]}{$linha["DT_ACESSO"]}";
			$resultados[$indice] = $linha["TOTAL"];
			if ($linha["IN_ORDENAR"] == 6) {
				$indice = "7{$linha["DT_ACESSO"]}";
				$resultados[$indice] = $linha["TOTAL_APROVADO"];
			}
			
		}
		
		return $resultados;
		
	}
	
	function obterProvasDia($tipo, $retirarAssessment = true)
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery, $lotacao, $localidade;
		
		$codigoAssessment = obterProvasQueryAssessment($tipo, $codigoEmpresa, $retirarAssessment);
		
		$sql = "SELECT
					COUNT(pr.DT_INICIO) AS QTD_AVALIACAO,
					COUNT(IF(pr.VL_MEDIA >= p.VL_MEDIA_APROVACAO,1,NULL)) AS QTD_AVALIACAO_SETE,
					AVG(pr.VL_MEDIA) AS VL_MEDIA,
					DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') AS DT_INICIO,
					COUNT(DISTINCT pr.CD_USUARIO) AS QTD_PARTICIPANTE_AVALIACAO,
					COUNT(IF(pr.VL_MEDIA >= 9,1,NULL)) AS QTD_AVALIACAO_NOVE
				FROM
					col_prova_realizada pr
					INNER JOIN col_prova p ON pr.CD_PROVA = p.CD_PROVA
					INNER JOIN col_usuario u ON u.CD_USUARIO = pr.CD_USUARIO
					INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				WHERE
 					p.CD_PROVA IN ($codigoAssessment)
 					
					AND pr.DT_INICIO IS NOT NULL AND pr.VL_MEDIA IS NOT NULL

					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				 	
				 	AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') 
				ORDER BY
				    pr.DT_INICIO";
		
		//echo "<!--$sql-->";
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			$multiplicador = 1;
			global $notaCentesimal;
			if ($notaCentesimal){
				$multiplicador = 10;
			}
			
			$indice = "{$linha["DT_INICIO"]}";
			//$resultados[$indice]["VL_MEDIA"] = str_ireplace(".",",",round($linha["VL_MEDIA"] * 10, 2));
			$resultados[$indice]["VL_MEDIA"] = $linha["VL_MEDIA"];
			$resultados[$indice]["QTD_AVALIACAO"] = $linha["QTD_AVALIACAO"];
			$resultados[$indice]["QTD_AVALIACAO_SETE"] = $linha["QTD_AVALIACAO_SETE"];
			$resultados[$indice]["QTD_PARTICIPANTE_AVALIACAO"] = $linha["QTD_PARTICIPANTE_AVALIACAO"];
			$resultados[$indice]["QTD_AVALIACAO_NOVE"] = $linha["QTD_AVALIACAO_NOVE"];
		}
		
		return $resultados;
		
	}
	
	
	function obterProvasMediasDia()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		$sql = "SELECT
				    IN_PROVA, DT_INICIO, SUM(SOMA_MEDIA) / SUM(QD_PROVA) AS VL_MEDIA
				FROM
				(
				SELECT
				    p.IN_PROVA ,DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') AS DT_INICIO, SUM(pa.VL_MEDIA) AS SOMA_MEDIA, count(pa.CD_PROVA) AS QD_PROVA
				FROM
				    col_prova_aplicada pa
				    INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
				    INNER JOIN col_usuario u ON u.CD_USUARIO = pa.CD_USUARIO
				    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				WHERE
				    pa.VL_MEDIA IS NOT NULL
				    AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    p.IN_PROVA ,DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') 
				
				UNION ALL
				
				SELECT
				    p.IN_PROVA ,DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') AS DT_INICIO, SUM(pa.VL_MEDIA) AS SOMA_MEDIA, count(pa.CD_PROVA) AS QD_PROVA
				FROM
				    col_prova_aplicada_hist pa
				    INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
				    INNER JOIN col_usuario u ON u.CD_USUARIO = pa.CD_USUARIO
				    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				WHERE
					pa.VL_MEDIA IS NOT NULL
					
					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1'  OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    p.IN_PROVA ,DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') 
				)
				AS TABELA
				GROUP BY
				    IN_PROVA, DT_INICIO
				ORDER BY
				    DT_INICIO";
		
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$indice = "{$linha["IN_PROVA"]}{$linha["DT_INICIO"]}";
			$resultados[$indice] = str_ireplace(".",",",round($linha["VL_MEDIA"], 2));
			
		}
		
		return $resultados;
		
		
	}
	
	
	function obterProvasMediasGeral($tipo, $retirarAssessment = true)
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery, $codigoCiclo, $localidade;
		
		if ($codigoCiclo == "")
			$codigoCiclo = -1;
			
		$codigoAssessment = obterProvasQueryAssessment($tipo, $codigoEmpresa, $retirarAssessment);
		
		$sql = "SELECT
				    p.IN_PROVA, SUM(pa.VL_MEDIA) / count(pa.CD_PROVA) AS VL_MEDIA
				FROM
				    col_prova_realizada pa
				    INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
				    INNER JOIN col_usuario u ON u.CD_USUARIO = pa.CD_USUARIO
				    INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				WHERE
				    pa.VL_MEDIA IS NOT NULL
				    
				    AND p.CD_PROVA IN ($codigoAssessment)
				    
				    AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
				 	
				 	AND	(u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
				  	
				  	AND (p.CD_CICLO = $codigoCiclo OR (($codigoCiclo = -1 OR p.CD_CICLO IS NULL)
				  		AND	(DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'))
				  		)
				GROUP BY
				    p.IN_PROVA
				";
		
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$indice = "{$linha["IN_PROVA"]}";
			$resultados[$indice] = str_ireplace(".",",",round($linha["VL_MEDIA"] * 10, 2));
			
		}
		
		return $resultados;
		
		
	}
	
		
	function obterNomePaginas()
	{
		
		global $codigoEmpresa;
		
		$paginas = array();
		
		$paginas[1] = "Boas Vindas";
		$paginas[2] = "Senha";
		$paginas[3] = "Download";
		
		
		if ($codigoEmpresa != 29)
		{
			$paginas[4] = "Visita ao F�rum";
			$paginas[5] = "Contribui��o&nbsp;ao&nbsp;F�rum";
		}
		$paginas[6] = "Simulados";
		//$paginas[7] = "Certifica��es";
		$paginas[7] = "Simulados com Aprova��o";
		
		$paginas[8] = "Participa��o em Pesquisa";
		
		return $paginas;
		
	}
	
	
	function obterNomeCiclos($filtrarData = false)
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		$whereCiclo = "";
		if ($filtrarData)
		{
			$whereCiclo = "AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= $dataFinalQuery";
		}
		
		$sql = "SELECT
				    CD_CICLO, NM_CICLO
				FROM
				    col_ciclo
				WHERE
				    CD_EMPRESA = $codigoEmpresa
				    $whereCiclo
				ORDER BY
					DT_INICIO";
		
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$resultados[] = $linha["NM_CICLO"];
			
		}
		
		return $resultados;
		
		
	}
	
	

?>