﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	config.toolbar_thisHtmlEditorToolbarSet = 
	[
	  	['EditSource','-','Cut','Copy','Paste','PasteText','PasteWord','-','Find','-','Undo','Redo','-','SelectAll','RemoveFormat','-','Link','RemoveLink','-','Image','Table','SpecialChar','-','Anchor','-','Video'] ,
	  	['FontStyle','-','Font','-','FontSize','-','TextColor','Bold','Italic','-','Subscript','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','-','InsertOrderedList','InsertUnorderedList','-','Outdent','Indent']
	];
	
	config.toolbar_HPEditorToolbarSet = 
	[
	  	['EditSource','-','Cut','Copy','Paste','PasteText','PasteWord','-','Find','-','Undo','Redo','-','RemoveFormat','-','Link','RemoveLink','-','Image','Table','SpecialChar'] ,
	  	['FontStyle','-','Font','-','FontSize','-','TextColor','Bold','Italic','-','Subscript','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','-','InsertOrderedList','InsertUnorderedList','-','Outdent','Indent']
	];
	
	config.toolbar_IMG = 
	[
	  	['Image']
	];
	
};
