<?php
include "../include/security.php";
include "../include/genericfunctions.php";
include('page.php');
include("../include/defines.php");
include('framework/crud.php');
include('controles.php');


$empresa = $_POST["cboEmpresa"];
$ciclo = $_POST["cboCiclo"];
$lotacao = $_POST["cboLotacao"];

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <TITLE>Colabor� - Consultoria e Educa&ccedil;&atilde;o Corporativa</TITLE>
    <META http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <META http-equiv="pragma" content="no-cache">
    <LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">
    <script type="text/javascript" src="include/js/functions.js"></script>

    <script type="text/javascript" language="JavaScript">

        function preparaFiltroConsulta(obj){

            var option = obj.options[obj.selectedIndex];

            var tipo = option.getAttribute("tipo");

            var exibeQuantidade = tipo == "Q" || tipo == "QN";
            var exibeNota = tipo == "N" || tipo == "QN";

            document.getElementById("spnQuantidade").style.display = "none";
            document.getElementById("spnNota").style.display = "none";

            var textoLabel = "";

            if (exibeQuantidade){

                document.getElementById("spnQuantidade").style.display = "inline-block";

                textoLabel = "*Quantidade:";

            }


            if (exibeNota) {

                document.getElementById("spnNota").style.display = "inline-block";

                if (textoLabel != ""){
                    textoLabel = textoLabel + "/";
                }

                textoLabel = textoLabel + "*Nota:";

            }

            document.getElementById("spnLabelAuxiliar").innerText = textoLabel;


        }


        function exportarExcel(){

            var action = document.forms[0].action;
            document.forms[0].action = "relatorio_email_massa.php"
            document.forms[0].submit();
            document.forms[0].action = action;

        }

    </script>

</HEAD>

<BODY>
<!-- Inicio do T�tulo -->
<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
    <TR>
        <TD><IMG height="2" src="images/blank.gif" width="100%"/></TD>
    </TR>
    <TR>
        <TD background="images/bg_logo_admin.png">
            <TABLE cellpadding="0" cellspacing="0" width="663" background="images/logo_admin.png" border="0">
                <TR>
                    <TD><IMG src="images/blank.gif" height="32" width="1"/></TD>
                    <TD class="data" align="right"><?php echo getServerDate(); ?></TD>
                </TR>

            </TABLE>

        </TD>
    </TR>
    <TR>
        <TD><IMG src="images/blank.gif" width="100%" height="2" /></TD>
    </TR>
    <TR>
        <TD bgcolor="#cccccc"><IMG src="images/blank.gif" height="3" width="100%"/></TD>
    </TR>
</TABLE>
<!-- Fim do T�tulo -->

<!-- In�cio Cabe�alho -->
<TABLE cellspacing="0" cellpadding="0" width="756" align="center" border="0">
    <TR>
        <TD width="1%"><IMG height="20" src="images/blank.gif" width="289"/></TD>
        <TD></TD>
        <TD></TD>
    </TR>
    <TR valign="top">
        <TD class="textblk"><SPAN class="title">USU�RIO: </SPAN><?php echo $_SESSION['alias']; ?></TD>
        <TD width="1%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.replace('logout.php')"
                              type="button" value="Logout" /></TD>
        <TD align="right" width="98%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href='admin_menu.php?'"
                                             type="button" value="Voltar"></TD>
    </TR>
    <TR>
        <TD><IMG src="images/blank.gif" height="4" width="1" /></TD>
    </TR>
    <TR>
        <TD class="textblk" colSpan="2"><SPAN class="title">P�GINA ATUAL:</SPAN>
            Newsletter</TD>
    </TR>
    <TR>
        <TD><IMG height="1" src="images/blank.gif" width="280"></TD>
    </TR>
</TABLE>
<BR>
<!-- Fim Cabe�alho -->

<!-- inicio do form -->


<FORM name="dadosForm" action="" method="post">

    <TABLE cellpadding="0" cellspacing="0" width="756" align="center" border="0">
        <TR class="tarjaTitulo">
            <TD align="middle" height="20">E-mail Marketing</TD>
        </TR>
        <TR>
            <TD><IMG src="images/blank.gif" height="1" width="10"/></TD>
        </TR>
        <TR>
            <TD width="100%">

                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td class="textblk">
                            * Programa:
                        </td>
                        <td>
                            <?php comboboxEmpresa("cboEmpresa", $empresa, "Programa", true, false); ?>
                        </td>
                    </tr>
                    <tr><td colspan="2" class="textblk">&nbsp;</td></tr>
                    <tr>
                        <td class="textblk">
                            Ciclo:
                        </td>
                        <td>
                            <?php comboboxCicloPorEmpresa("cboCiclo", $ciclo, $empresa); ?>
                        </td>
                    </tr>
                    <tr><td colspan="2" class="textblk">&nbsp;</td></tr>
                    <tr>
                        <td class="textblk" nowrap="nowrap">
                            Grupo de Gest�o:
                        </td>
                        <td>
                            <?php comboboxLotacao("cboLotacao",$lotacao, $empresa, false, false); ?>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td> </tr>
                    <tr>
                        <td class="textblk">
                            Cargo/Fun��o:
                        </td>
                        <td>
                            <?php comboboxCargoFuncao("cboCargo",'', $empresa); ?>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td class="textblk">
                            Localidade:
                        </td>
                        <td>
                            <?php comboboxLocalidade("cboLocalidade",'', $empresa, false, false); ?>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td class="textblk">Tipo de Consulta:</td>
                        <td><?php comboboxTipoConsulta("cboConsulta", $_POST["cboConsulta"], "Tipo de Consulta"); ?></td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td class="textblk"><span id="spnLabelAuxiliar"></span></td>
                        <td><span id="spnQuantidade" style="display:none;">
                            <?php
                                textbox("txtQuantidade", 3, $quantidade, "30px", null, true, Mascaras::Numero);
                            ?></span>
                            <span id="spnNota" style="display:none;">
                            <?php
                                textbox("txtNota", 3, $quantidade, "30px", null, true, Mascaras::Numero);
                            ?></span>
                        </td>
                    </tr>
                </table>

            </TD>
        </TR>
        <TR class="tarjaTitulo">
            <TD colSpan="4"><IMG height="1" src="images/blank.gif" width="100"></TD>
        </TR>

    </TABLE>

    <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
        <TR>
            <TD><IMG height="40" src="images/blank.gif" width="1"></TD>
        </TR>
        <TR>
            <TD align="middle">
                <INPUT class="buttonsty" onfocus="noFocus(this)"
                       type="submit" value="Gerar Lista em Excel" onclick="exportarExcel();return false;"/>
                <IMG height="1" src="images/blank.gif" width="20"/><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href='admin_menu.php?'"
                                                                          type="button" value="Voltar"/>


            </TD>
        </TR>
    </TABLE>

</FORM>

</BODY>

</HTML>


<?php




?>