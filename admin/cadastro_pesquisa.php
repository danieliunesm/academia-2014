<?php

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_pesquisa();
}

function carregarRN(){
	
	page::$rn->CD_PESQUISA = $_REQUEST["id"];
	page::$rn->CD_EMPRESA = $_POST["cboEmpresa"];
	page::$rn->DS_PESQUISA = $_POST["txtPesquisa"];
	page::$rn->DT_INICIO = $_POST["txtDataInicio"];
	page::$rn->DT_TERMINO = $_POST["txtDataTermino"];
    page::$rn->DT_INICIO_RELATORIO = $_POST["txtDataInicioRelatorio"];
    page::$rn->DT_TERMINO_RELATORIO = $_POST["txtDataTerminoRelatorio"];
	
	$Perguntas = array();
	
	$nomesPergunta = $_POST["txtPergunta"];
	$nrOrdem = $_POST["txtOrdem"];
	$codigosPergunta = $_POST["hdnCodigoPergunta"];
	
	for ($i = 0; $i < count($nomesPergunta); $i++){
        
		$pergunta = new col_pergunta_pesquisa();
//		$$pergunta->CD_PESQUISA = $_REQUEST["id"];
		$pergunta->CD_EMPRESA = $_POST["cboEmpresa"];
		$pergunta->DS_PERGUNTA = $nomesPergunta[$i];
		$pergunta->NR_ORDEM = $nrOrdem[$i];
		$pergunta->CD_PERGUNTA_PESQUISA = $codigosPergunta[$i];
			
		$Perguntas[] = $pergunta;
	}
	
	page::$rn->perguntas = $Perguntas;
	
}

function pagePreRender()
{
	page::$enctype = ' enctype="multipart/form-data" ';
}


function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/admin/cadastro_pesquisa.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

function pageRenderEspecifico(){
	
	$id = $_REQUEST["id"];
	
?>				
				<TR>
					<TD class="textblk" colspan="2">Nome da Pesquisa: *</TD>
				</TR>
				<TR>
					<TD colspan="2"><?php textbox("txtPesquisa", "100", page::$rn->DS_PESQUISA, "100%", "textbox3", "Pesquisa"); ?></TD>
				</TR>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<TD class="textblk" colspan="2">Empresa: *</TD>
				</TR>
				<TR>
					<TD colspan="2"><?php comboboxEmpresa("cboEmpresa",  page::$rn->CD_EMPRESA, "Empresa"); ?></TD>
				</TR>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<tr>
					<td class="textblk" colspan="2">
						Per�odo de Vig�ncia: *
					</td>
				</tr>
				<tr>
					<td class="textblk" colspan="2">
						<?php textbox("txtDataInicio", "10", page::$rn->DT_INICIO, "110px", "textbox3", "In�cio Vig�ncia", Mascaras::Data) ?>
						a
						<?php textbox("txtDataTermino", "10", page::$rn->DT_TERMINO, "110px", "textbox3", "Termino Vig�ncia", Mascaras::Data) ?>
					</td>
				</tr>
				<TR><TD colspan="2">&nbsp;</TD></TR>
                <tr>
                    <td class="textblk" colspan="2">
                        Per�odo de Exibi��o do Relat�rio: *
                    </td>
                </tr>
                <tr>
                    <td class="textblk" colspan="2">
                        <?php textbox("txtDataInicioRelatorio", "10", page::$rn->DT_INICIO_RELATORIO, "110px", "textbox3", "In�cio de Exibi��o do Relat�rio", Mascaras::Data) ?>
                        a
                        <?php textbox("txtDataTerminoRelatorio", "10", page::$rn->DT_TERMINO_RELATORIO, "110px", "textbox3", "Termino de Exibi��o do Relat�rio", Mascaras::Data) ?>
                    </td>
                </tr>
                <TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<td colspan="2" class="textblk"><a href="javascript:adicionarPergunta()">Adicionar Perguntas</a></td>
					
				</TR>
				<tr>
					<td colspan="2" class="textblk">
					
						<script type="text/javascript" language="javascript">
						
							function adicionarPergunta(){
							
								var no = document.getElementById("dvPergunta").cloneNode(true);
								var dv = document.getElementById("dvPerguntas");
								
								//alert(no.all.length)
								
								var itens = no.all.length;
								
								for (i=0; i<itens; i++){
									if (no.all[i].type == "text"){
										no.all[i].value = "";
									}else if (no.all[i].type == "select-one"){
										no.all[i].selectedIndex = 0;
									}else if (no.all[i].type == "hidden"){
										no.all[i].value = "";
									}
								}
								
								dv.appendChild(no);
							}
							
							function removerPergunta(obj){
								if (document.getElementById("dvPerguntas").children.length==1){
									alert('Deve existir ao menos um pergunta.')
									return;
								}
								
								 document.getElementById("dvPerguntas").removeChild(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
							}
						
						
						</script>
					
					
						<div id="dvPerguntas">
							<?php
								$linhas = 1;
								$perguntas = page::$rn->perguntas;
								
								if (is_array($perguntas))
								{
									if (count($perguntas) > 1)
										$linhas = count($perguntas);
										
									usort($perguntas, "ordenarPerguntas");
									
								}
								
								for ($i=0; $i < $linhas; $i++)
								{
									$nomePergunta = "";
									$nrOrdem = "";
									$codigoPergunta = "";
									
									if (is_array($perguntas))
									{
										$pergunta = $perguntas[$i];
										$nomePergunta = $pergunta->DS_PERGUNTA;
										$nrOrdem = $pergunta->NR_ORDEM;
										$codigoPergunta = $pergunta->CD_PERGUNTA_PESQUISA;
										
									}
									
								
							
							?>
						
							<div id="dvPergunta" style="border: 1px solid black; margin-top: 2px;">
								<table cellpadding="2" cellspacing="0" width="98%">
									<tr>
										<td class="textblk" >Pergunta:</td>
										<td rowspan="4" class="textblk" align="center"><input type="image" src="/images/layout/bt_delsession.gif" value="" onclick="javascript:removerPergunta(this)" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'"></td>
									</tr>												
									<tr>
										<td class="textblk" ><?php textbox("txtPergunta[]", "500", $nomePergunta, "100%", "textbox3", "Pergunta"); ?></td>
									</tr>
									<tr>
										<td class="textblk" >N�mero de Ordem:</td>
									</tr>
									<tr>
										<td class="textblk" >
											<?php textbox("txtOrdem[]", "10", $nrOrdem, "100px", "textbox3", "N�mero Ordem"); ?>
											<input type="hidden" id="hdnCodigoPergunta[]" name="hdnCodigoPergunta[]" value="<?php echo $codigoPergunta; ?>" />
										</td>
									</tr>
								</table>
							</div>
								<?php
								}
								?>
							
						</div>
						
					</td>
				</tr>
<?php

}

function ordenarPerguntas($a, $b)
{
	return ordenarDados($a, $b, "NR_ORDEM");
}

function ordenarDados($a, $b, $campo)
{
	$dataA = $a->NR_ORDEM;
	$dataB = $b->NR_ORDEM;
	
	if ($dataA == $dataB)
		return 0;
		
	return ($dataA > $dataB) ? +1:-1;
}


?>