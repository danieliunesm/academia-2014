<?php

function obterNomeCurso($codigoCurso){

    $nomeCurso = "Todos";

    if ($codigoCurso != -1)
    {
        //Obter o nome do Ciclo
        $sql = "SELECT Titulo FROM tb_pastasindividuais WHERE ID = $codigoCurso";
        $resultado = DaoEngine::getInstance()->executeQuery($sql,true);
        $linhaCiclo = mysql_fetch_array($resultado);
        $nomeCurso = $linhaCiclo["Titulo"];
        //Fim obter o nome do Ciclo
    }

    return $nomeCurso;

}

function obterCodigosProvas($codigoCurso){

    if($codigoCurso == -1) return -1;



}

function joinArray($array, $separador = ",")
{
	if (is_array($array))
	{
		return join($separador, $array);
	}
	else
	{
		return "-1";
	}
	
}

function corrigePlic($texto)
{
	
	return str_ireplace("'","\'", $texto);
	
}

function escreveHiddensPost($POST = null)
{
	
	if ($POST == null)
	{
		$POST = $_POST;
	}
	
	foreach($POST as $key=>$value)
	{
		if (is_array($value))
		{
			foreach ($value as $valor)
			{
				escreveHidden("$key" . "[]", $valor);
			}
		}
		else 
		{
			escreveHidden($key, $value);
		}
	}
	
}

function escreveHidden($id, $valor)
{
	echo "<input type=\"hidden\" id=\"$id\" name=\"$id\" value=\"$valor\" />";
}

function obterTotalUsuarios($empresa)
{
	
	$sql = "SELECT COUNT(1) as TOTAL FROM col_usuario WHERE empresa = $empresa";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linha = mysql_fetch_row($resultado);
	return $linha[0];
	
	
}

function obterCargosAssessment($empresa, $codigoLotacao, $cargo, $localidade){
	
	$sql = "SELECT
				DISTINCT cargofuncao
			FROM
				col_usuario u
				INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao AND u.empresa = l.CD_EMPRESA
			WHERE
			(cargofuncao LIKE '%-%')
			AND	(empresa = $empresa OR $empresa = -1)
			AND (u.Status = 1)
			AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
			AND	(cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
			AND (u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
			ORDER BY SUBSTRING_INDEX(cargofuncao,'-', -1), SUBSTRING_INDEX(cargofuncao,'-', 1)";

	//AND EXISTS(SELECT 1 FROM col_prova p INNER JOIN col_prova_vinculo pv ON pv.CD_PROVA = p.CD_PROVA WHERE pv.CD_VINCULO = u.empresa AND pv.IN_TIPO_VINCULO = 1 AND p.cargo = u.cargofuncao OR p.cargo IS NULL AND p.IN_ATIVO = 1)
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	
	//echo $sql;
	
	return $resultado;
	
}

function obterTotalUsuariosGrupamento($empresa, $codigoUsuario, $codigoLotacao, $cargo, $dataTermino = "")
{
	
	if ($dataTermino != "")
	{
		$dataTermino = " AND DATE_FORMAT(datacadastro,'%Y%m%d') <= $dataTermino ";
	}
	
	$sql =	"SELECT
			COUNT(1) as TOTAL
		FROM
			col_usuario u
			LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao AND u.empresa = l.CD_EMPRESA
		WHERE
			(empresa = $empresa OR $empresa = -1)
		AND (u.Status = 1)
		AND	(CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
		AND	(cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		$dataTermino";
	
	//echo $sql;
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linha = mysql_fetch_row($resultado);
	return $linha[0];
	
	

	
}

function obterTotalUsuariosLotacao($lotacao)
{
	$sql = "SELECT COUNT(1) FROM col_usuario WHERE lotacao = $lotacao";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linha = mysql_fetch_row($resultado);
	return $linha[0];
}

function tranformaArrayVazioMenos1($array)
{
	if (count($array) == 0)
	{
		return "-1";
	}
	
	return $array;

}

function nullPara0(&$valor)
{
	if ($valor == null || $valor == "")
	{
		return 0;
	}
	
	return $valor;
	
	
}

function nullPara0Ranking(&$valor, $exibirIndicador)
{

	if ($exibirIndicador == 2)
	{
		return 0;
	}
	
	return  nullPara0($valor);
	
}


function obterPost($codigoFiltro = 0)
{
	
	$POST = null;
	
	if (isset($_GET["id"]) || $codigoFiltro > 0)
	{

			if ($_GET["id"] == "" && $codigoFiltro == 0 && ((!isset($_GET["hdnLotacao"])) || $_GET["hdnLotacao"] == "" || $_GET["hdnLotacao"] == "-1"))
			{
				$POST = $_POST;
				$POST["cboEmpresa"] = $_GET["cboEmpresa"];
			}
			elseif (($_GET["id"] == "-1" || $_GET["id"] == "") && $codigoFiltro == 0 && (isset($_GET["hdnLotacao"])))
			{
				$POST["cboEmpresa"] = $_GET["cboEmpresa"];
				$POST["cboUsuario"] = "-1";
				$POST["cboProva"] = "-1";
				$POST["cboCargo"] = $_GET["cboCargo"];
				$POST["cboLotacao"] = array($_GET["hdnLotacao"]);
				$POST["cboForum"] = "-1";
				
				$sql = "SELECT DS_LOTACAO, DS_HIERARQUIA, l.CD_NIVEL_HIERARQUICO FROM col_lotacao l LEFT OUTER JOIN col_hierarquia h ON l.CD_NIVEL_HIERARQUICO = h.CD_NIVEL_HIERARQUICO and h.CD_EMPRESA = l.CD_EMPRESA WHERE CD_LOTACAO = {$_GET["hdnLotacao"]}";
				$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
				$linha = mysql_fetch_row($resultado);
				$POST["nomeLotacao"] = $linha[0];
				$POST["nomeHierarquia"] = $linha[1];
				$POST["nivelHierarquia"] = $linha[2];
				
				
			}
			else 
			{
				if ($codigoFiltro == 0)
				{
					$codigoFiltro = $_GET["id"];
				}
				$filtro = new col_filtro();
				$filtro->CD_FILTRO = $codigoFiltro;
				$filtro = $filtro->obter();
			
				$POST = array();
				
				$POST["nomeFiltro"] = $filtro->NM_FILTRO;
				$POST["cboEmpresa"] = $filtro->CD_EMPRESA;
				
				if (isset($_GET["cboEmpresa"]) && $_GET["cboEmpresa"] > 0)
				{
					$POST["cboEmpresa"] = $_GET["cboEmpresa"];
				}
				
				$POST["cboUsuario"] = tranformaArrayVazioMenos1(obterValoresUsuario($filtro->usuarios));
				$POST["cboProva"] = tranformaArrayVazioMenos1(obterValoresProva($filtro->provas));
				$POST["cboCargo"] = tranformaArrayVazioMenos1(obterValoresCargo($filtro->cargos));
				$POST["cboLotacao"] = tranformaArrayVazioMenos1(obterValoresLotacao($filtro->lotacoes));
				$POST["cboForum"] = tranformaArrayVazioMenos1(obterValoresForum($filtro->foruns));
			}
			
			if (isset($_GET["txtDe"]))
			{
				$POST["txtDe"] = $_GET["txtDe"];
				$POST["txtAte"] = $_GET["txtAte"];
			}
			
	}
	else 
	{
		$POST = $_POST;
	}
	
	return $POST;
	
}


function obterNomeCiclo($codigoCiclo)
{
	$nomeCiclo = "Todos os Ciclos";
	if ($codigoCiclo != -1)
	{
		//Obter o nome do Ciclo
		$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		$linhaCiclo = mysql_fetch_array($resultado);
		$nomeCiclo = $linhaCiclo["NM_CICLO"];
		//Fim obter o nome do Ciclo
	}
	return $nomeCiclo;
}

/*
function obterPerguntasStatus($empresa, $codigoLotacao, $cargo, $localidade){
	
	$sql = "SELECT
				u.cargofuncao,
				p.CD_PERGUNTAS,
				p.DS_PERGUNTAS,
				gd.NM_GRUPO_DISCIPLINA,
				COUNT(pa.CD_PERGUNTAS) AS TOTAL_OCORRENCIA,
				COUNT(IF(r.IN_CERTA = 1, 1, NULL)) AS TOTAL_ACERTO,
				COUNT(IF(r.IN_CERTA = 1, 1, NULL)) / COUNT(pa.CD_PERGUNTAS) AS MEDIA_ACERTO,
				(SELECT DS_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.CD_RESPOSTAS = 1) AS RESPOSTA_1,
				COUNT(IF(r.CD_RESPOSTAS = 1, 1, NULL)) AS TOTAL_RESPOSTA_1,
				(SELECT DS_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.CD_RESPOSTAS = 2) AS RESPOSTA_2,
				COUNT(IF(r.CD_RESPOSTAS = 2, 1, NULL)) AS TOTAL_RESPOSTA_2,
				(SELECT DS_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.CD_RESPOSTAS = 3) AS RESPOSTA_3,
				COUNT(IF(r.CD_RESPOSTAS = 3, 1, NULL)) AS TOTAL_RESPOSTA_3,
				(SELECT DS_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.CD_RESPOSTAS = 4) AS RESPOSTA_4,
				COUNT(IF(r.CD_RESPOSTAS = 4, 1, NULL)) AS TOTAL_RESPOSTA_4,
				(SELECT r1.CD_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.IN_CERTA = 1) AS RESPOSTA_CERTA
			FROM
				col_usuario u
				INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				INNER JOIN col_prova_aplicada pra ON u.CD_USUARIO = pra.CD_USUARIO
				INNER JOIN col_perguntas_aplicadas pa ON pra.CD_PROVA = pa.CD_PROVA AND pa.CD_USUARIO = pra.CD_USUARIO
				INNER JOIN col_perguntas p ON p.CD_PERGUNTAS = pa.CD_PERGUNTAS
				LEFT JOIN col_respostas r ON r.CD_PERGUNTAS = p.CD_PERGUNTAS AND r.CD_RESPOSTAS = pa.CD_RESPOSTA
				INNER JOIN col_prova pr ON pr.CD_PROVA = pra.CD_PROVA
				INNER JOIN col_disciplina_grupo_disciplina dgd ON dgd.CD_DISCIPLINA = p.CD_DISCIPLINA
				INNER JOIN col_grupo_disciplina gd ON gd.CD_GRUPO_DISCIPLINA = dgd.CD_GRUPO_DISCIPLINA AND gd.CD_EMPRESA = u.empresa
			WHERE
                u.empresa = $empresa
			AND u.Status = 1
			AND pra.VL_MEDIA IS NOT NULL
			AND tipo = 1
			AND pr.IN_PROVA = 1
			AND pr.IN_ATIVO = 1
			AND	(lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
			AND	(cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
			AND (u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
			GROUP BY
				u.cargofuncao,
				p.CD_PERGUNTAS,
				p.DS_PERGUNTAS,
				gd.NM_GRUPO_DISCIPLINA
			ORDER BY
				u.cargofuncao,
				gd.NM_GRUPO_DISCIPLINA,
				MEDIA_ACERTO,
				TOTAL_ACERTO";


    //SUBSTRING_INDEX(u.cargofuncao,'-', -1), SUBSTRING_INDEX(u.cargofuncao,'-', 1),

	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

	//echo "<!--$sql-->";
	
	return $resultado;
	
}
*/

function obterPerguntasStatus($empresa, $codigoLotacao, $cargo, $localidade, $codigoCiclo){
    
    $sql = "SELECT

                p.CD_PERGUNTAS,
                p.DS_PERGUNTAS,
                gd.NM_GRUPO_DISCIPLINA,
                COUNT(pa.CD_PERGUNTAS) AS TOTAL_OCORRENCIA,
                COUNT(IF(r.IN_CERTA = 1, 1, NULL)) AS TOTAL_ACERTO,
                COUNT(IF(r.IN_CERTA = 1, 1, NULL)) / COUNT(pa.CD_PERGUNTAS) AS MEDIA_ACERTO,
                (SELECT DS_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.CD_RESPOSTAS = 1) AS RESPOSTA_1,
                COUNT(IF(r.CD_RESPOSTAS = 1, 1, NULL)) AS TOTAL_RESPOSTA_1,
                (SELECT DS_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.CD_RESPOSTAS = 2) AS RESPOSTA_2,
                COUNT(IF(r.CD_RESPOSTAS = 2, 1, NULL)) AS TOTAL_RESPOSTA_2,
                (SELECT DS_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.CD_RESPOSTAS = 3) AS RESPOSTA_3,
                COUNT(IF(r.CD_RESPOSTAS = 3, 1, NULL)) AS TOTAL_RESPOSTA_3,
                (SELECT DS_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.CD_RESPOSTAS = 4) AS RESPOSTA_4,
                COUNT(IF(r.CD_RESPOSTAS = 4, 1, NULL)) AS TOTAL_RESPOSTA_4,
                (SELECT r1.CD_RESPOSTAS FROM col_respostas r1 WHERE r1.CD_PERGUNTAS = pa.CD_PERGUNTAS AND r1.IN_CERTA = 1) AS RESPOSTA_CERTA
            FROM
                col_usuario u
                INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
                INNER JOIN col_prova_aplicada pra ON u.CD_USUARIO = pra.CD_USUARIO
                INNER JOIN col_perguntas_aplicadas pa ON pra.CD_PROVA = pa.CD_PROVA AND pa.CD_USUARIO = pra.CD_USUARIO
                INNER JOIN col_perguntas p ON p.CD_PERGUNTAS = pa.CD_PERGUNTAS
                LEFT JOIN col_respostas r ON r.CD_PERGUNTAS = p.CD_PERGUNTAS AND r.CD_RESPOSTAS = pa.CD_RESPOSTA
                INNER JOIN col_prova pr ON pr.CD_PROVA = pra.CD_PROVA
                INNER JOIN col_disciplina_grupo_disciplina dgd ON dgd.CD_DISCIPLINA = p.CD_DISCIPLINA
                INNER JOIN col_grupo_disciplina gd ON gd.CD_GRUPO_DISCIPLINA = dgd.CD_GRUPO_DISCIPLINA AND gd.CD_EMPRESA = u.empresa
            WHERE
                u.empresa = $empresa
            AND u.Status = 1
            AND pra.VL_MEDIA IS NOT NULL
            AND tipo = 1
            AND pr.IN_PROVA = 1
            AND pr.IN_ATIVO = 1
            AND (pr.CD_CICLO = $codigoCiclo OR $codigoCiclo = '-1' OR pr.CD_CICLO IS NULL)
            AND    (lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
            AND    (cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
            AND (u.CD_LOCALIDADE IN ($localidade) OR '$localidade' = '-1')
            GROUP BY

                p.CD_PERGUNTAS,
                p.DS_PERGUNTAS,
                gd.NM_GRUPO_DISCIPLINA
            ORDER BY

                gd.NM_GRUPO_DISCIPLINA,
                MEDIA_ACERTO,
                TOTAL_ACERTO";
    
    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    //(u.cargofuncao LIKE '%-%')
    //AND
    //SUBSTRING_INDEX(u.cargofuncao,'-', -1), SUBSTRING_INDEX(u.cargofuncao,'-', 1),
    //echo $sql;
    
    return $resultado;
    
}

?>