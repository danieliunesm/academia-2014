<?php

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_disciplina();
}

function carregarRN(){
	
	page::$rn->DS_DISCIPLINA = $_POST["txtDisciplina"];
	page::$rn->CD_DISCIPLINA = $_REQUEST["id"];
	
	page::$rn->CD_DISCIPLINAS_COPIADAS = $_POST["cboDisciplinaCopiada"];
	
}

function pageRenderEspecifico(){
	
?>					<TR>

						<TD class="textblk">Nome da disciplina: *</TD>
					</TR>
					<TR>
						<TD><?php textbox("txtDisciplina", "50", page::$rn->DS_DISCIPLINA, "100%", "textbox3", "Disciplina") ?></TD>
					</TR>
					<TR>
						<TD colspan="2">&nbsp;</TD>
					</TR>
					<TR>
						<TD class="textblk">Selecione uma disciplina se desejar que esta disciplina copie suas perguntas j� cadastradas:</TD>
					</TR>
					<TR>
						<TD><?php comboboxDisciplina("cboDisciplinaCopiada[]", "", false, true); ?></TD>
					</TR>
<?php

}

function pagePreRender(){

	page::$validador = "javascript:return validarDisciplina();";

	page::$script = '
	
	function validarDisciplina(){
		
		if(!(confirm("Esta opera��o copiar� todas as perguntas das disciplinas selecionadas. \nDeseja realmente realiz�-la?")))
		{
			return false;
		}
		
		var validacao = VerificarPreenchimentoObrigatorioTag();
		
		if (!validacao){
			return false;
		}
		
		return true;
		
	
	}';
	
}


?>