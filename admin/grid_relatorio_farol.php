<?php
include("../include/defines.php");
include('framework/crud.php');
include('controles.php');
include('datagrid.php');

$paginaEdicao = "cadastro_relatorio_farol.php";
$paginaEdicaoAltura = "(screen.height-80)";
$paginaEdicaoLargura = "600";

function instanciarRN(){
	return new col_farol();
}


define('TITULO_PAGINA', "Configuração Radar");
define('BOTAO_ADICIONAR', "Incluir Configuração Radar");
define('BOTAO_MODO_EXIBICAO', "Visualizar Configuração Radar");

include('grid_cabecalho.php');

//$rn = new col_farol();
//$resultado = $rn->listar(null, "NM_FAROL");

$sql = "SELECT CD_FAROL, NM_FAROL, DS_EMPRESA, f.IN_ATIVO FROM col_farol f INNER JOIN col_empresa e ON e.CD_EMPRESA = f.CD_EMPRESA ORDER BY DS_EMPRESA";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

dataGridColaborae(array("Programa", "Configuração Radar"), array("DS_EMPRESA","NM_FAROL"), array("10%","30%","60%"), $resultado, "CD_FAROL", true);

include('grid_rodape.php');

?>