<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";

$id					= $_POST["id"];

$logo1				= isset($_POST['logo1'])?(trim($_POST['logo1'])!=''?$_POST['logo1']:""):"";
$logo1Dx			= isset($_POST['logo1Dx'])?$_POST['logo1Dx']:0;
$logo1Dy			= isset($_POST['logo1Dy'])?$_POST['logo1Dy']:0;
$logo1W				= isset($_POST['logo1W'])?$_POST['logo1W']:0;
$logo1H				= isset($_POST['logo1H'])?$_POST['logo1H']:0;

$logo2				= isset($_POST['logo2'])?(trim($_POST['logo2'])!=''?$_POST['logo2']:""):"";
$logo2Dx			= isset($_POST['logo2Dx'])?$_POST['logo2Dx']:0;
$logo2Dy			= isset($_POST['logo2Dy'])?$_POST['logo2Dy']:0;
$logo2W				= isset($_POST['logo2W'])?$_POST['logo2W']:0;
$logo2H				= isset($_POST['logo2H'])?$_POST['logo2H']:0;

$certificadoDy		= isset($_POST['certificadoDy'])?$_POST['certificadoDy']:0;

$marcadagua			= isset($_POST['marcadagua'])?(trim($_POST['marcadagua'])!=''?$_POST['marcadagua']:""):"";
$marcadaguaDx		= isset($_POST['marcadaguaDx'])?$_POST['marcadaguaDx']:0;
$marcadaguaDy		= isset($_POST['marcadaguaDy'])?$_POST['marcadaguaDy']:0;
$marcadaguaW		= isset($_POST['marcadaguaW'])?$_POST['marcadaguaW']:0;
$marcadaguaH		= isset($_POST['marcadaguaH'])?$_POST['marcadaguaH']:0;

$txt1				= isset($_POST['txt1'])?($_POST['txt1']):"";
$txt1Dy				= isset($_POST['txt1Dy'])?$_POST['txt1Dy']:0;

$linhaDy			= isset($_POST['linhaDy'])?$_POST['linhaDy']:0;
$nome				= isset($_POST['nome'])?$_POST['nome']:"";

$txt2				= isset($_POST['txt2'])?($_POST['txt2']):"";
$txt2Dy				= isset($_POST['txt2Dy'])?$_POST['txt2Dy']:0;

$programa			= isset($_POST['programa'])?($_POST['programa']):"";
$programaDy			= isset($_POST['programaDy'])?$_POST['programaDy']:0;

$curso				= isset($_POST['curso'])?($_POST['curso']):"";
$cursoDy			= isset($_POST['cursoDy'])?$_POST['cursoDy']:0;

$txt3				= isset($_POST['txt3'])?($_POST['txt3']):"";
$txt3Dy				= isset($_POST['txt3Dy'])?$_POST['txt3Dy']:0;

$data				= isset($_POST['data'])?$_POST['data']:"";
$dataDy				= isset($_POST['dataDy'])?$_POST['dataDy']:0;

$medalhaDx			= isset($_POST['medalhaDx'])?$_POST['medalhaDx']:0;
$medalhaDy			= isset($_POST['medalhaDy'])?$_POST['medalhaDy']:0;
$medalhaStatus		= isset($_POST['medalhaStatus'])?$_POST['medalhaStatus']:0;

$conjunto1Dx		= isset($_POST['conjunto1Dx'])?$_POST['conjunto1Dx']:0;
$conjunto1Dy		= isset($_POST['conjunto1Dy'])?$_POST['conjunto1Dy']:0;
$assinatura1		= isset($_POST['assinatura1'])?(trim($_POST['assinatura1'])!=''?$_POST['assinatura1']:""):"";
$assinatura1Dx		= isset($_POST['assinatura1Dx'])?$_POST['assinatura1Dx']:0;
$assinatura1Dy		= isset($_POST['assinatura1Dy'])?$_POST['assinatura1Dy']:0;
$assinatura1W		= isset($_POST['assinatura1W'])?$_POST['assinatura1W']:0;
$assinatura1H		= isset($_POST['assinatura1H'])?$_POST['assinatura1H']:0;
$nome1				= isset($_POST['nome1'])?$_POST['nome1']:"";
$cargo1				= isset($_POST['cargo1'])?$_POST['cargo1']:"";

$conjunto2Dx		= isset($_POST['conjunto2Dx'])?$_POST['conjunto2Dx']:0;
$conjunto2Dy		= isset($_POST['conjunto2Dy'])?$_POST['conjunto2Dy']:0;
$assinatura2		= isset($_POST['assinatura2'])?(trim($_POST['assinatura2'])!=''?$_POST['assinatura2']:""):"";
$assinatura2Dx		= isset($_POST['assinatura2Dx'])?$_POST['assinatura2Dx']:0;
$assinatura2Dy		= isset($_POST['assinatura2Dy'])?$_POST['assinatura2Dy']:0;
$assinatura2W		= isset($_POST['assinatura2W'])?$_POST['assinatura2W']:0;
$assinatura2H		= isset($_POST['assinatura2H'])?$_POST['assinatura2H']:0;
$nome2				= isset($_POST['nome2'])?$_POST['nome2']:"";
$cargo2				= isset($_POST['cargo2'])?$_POST['cargo2']:"";
$nu_certificado		= isset($_POST['nu_certificado'])?$_POST['nu_certificado']:"";

$programa = mysql_real_escape_string($programa);
$curso	  = mysql_real_escape_string($curso);
$txt1	  = mysql_real_escape_string($txt1);
$txt2	  = mysql_real_escape_string($txt2);
$txt3	  = mysql_real_escape_string($txt3);

if($id!="")
{
	$sql = "UPDATE col_certificado SET ";
	$sql = $sql . "DS_PROGRAMA = '" . $programa . "', ";
	$sql = $sql . "DS_CURSO = '" . $curso . "', ";
	$sql = $sql . "programaDy = '" . $programaDy . "', ";
	$sql = $sql . "cursoDy = '" . $cursoDy . "', ";
	$sql = $sql . "logo1 = '" . $logo1 . "', ";
	$sql = $sql . "logo1Dy = '" . $logo1Dy . "', ";
	$sql = $sql . "logo1Dx = '" . $logo1Dx . "', ";
	$sql = $sql . "logo1W = '" . $logo1W . "', ";
	$sql = $sql . "logo1H = '" . $logo1H . "', ";
	$sql = $sql . "logo2 = '" . $logo2 . "', ";
	$sql = $sql . "logo2Dy = '" . $logo2Dy . "', ";
	$sql = $sql . "logo2Dx = '" . $logo2Dx . "', ";
	$sql = $sql . "logo2W = '" . $logo2W . "', ";
	$sql = $sql . "logo2H = '" . $logo2H . "', ";
	$sql = $sql . "marcadagua = '" . $marcadagua . "', ";
	$sql = $sql . "marcadaguaDy = '" . $marcadaguaDy . "', ";
	$sql = $sql . "marcadaguaDx = '" . $marcadaguaDx . "', ";
	$sql = $sql . "marcadaguaW = '" . $marcadaguaW . "', ";
	$sql = $sql . "marcadaguaH = '" . $marcadaguaH . "', ";
	$sql = $sql . "nome = '" . $nome . "', ";
	$sql = $sql . "linhaDy = '" . $linhaDy . "', ";
	$sql = $sql . "txt1 = '" . $txt1 . "', ";
	$sql = $sql . "txt1Dy = '" . $txt1Dy . "', ";
	$sql = $sql . "txt2 = '" . $txt2 . "', ";
	$sql = $sql . "txt2Dy = '" . $txt2Dy . "', ";
	$sql = $sql . "txt3 = '" . $txt3 . "', ";
	$sql = $sql . "txt3Dy = '" . $txt3Dy . "', ";
	$sql = $sql . "assinatura1 = '" . $assinatura1 . "', ";
	$sql = $sql . "assinatura1Dy = '" . $assinatura1Dy . "', ";
	$sql = $sql . "assinatura1Dx = '" . $assinatura1Dx . "', ";
	$sql = $sql . "assinatura1W = '" . $assinatura1W . "', ";
	$sql = $sql . "assinatura1H = '" . $assinatura1H . "', ";
	$sql = $sql . "nome1 = '" . $nome1 . "', ";
	$sql = $sql . "cargo1 = '" . $cargo1 . "', ";
	$sql = $sql . "conjunto1Dy = '" . $conjunto1Dy . "', ";
	$sql = $sql . "conjunto1Dx = '" . $conjunto1Dx . "', ";
	$sql = $sql . "assinatura2 = '" . $assinatura2 . "', ";
	$sql = $sql . "assinatura2Dy = '" . $assinatura2Dy . "', ";
	$sql = $sql . "assinatura2Dx = '" . $assinatura2Dx . "', ";
	$sql = $sql . "assinatura2W = '" . $assinatura2W . "', ";
	$sql = $sql . "assinatura2H = '" . $assinatura2H . "', ";
	$sql = $sql . "nome2 = '" . $nome2 . "', ";
	$sql = $sql . "cargo2 = '" . $cargo2 . "', ";
	$sql = $sql . "conjunto2Dy = '" . $conjunto2Dy . "', ";
	$sql = $sql . "conjunto2Dx = '" . $conjunto2Dx . "', ";
	$sql = $sql . "data = '" . $data . "', ";
	$sql = $sql . "dataDy = '" . $dataDy . "', ";
	$sql = $sql . "medalhaStatus = '" . $medalhaStatus . "', ";
	$sql = $sql . "medalhaDy = '" . $medalhaDy . "', ";
	$sql = $sql . "medalhaDx = '" . $medalhaDx . "', ";
	$sql = $sql . "nu_certificado = '" . $nu_certificado . "', ";
	$sql = $sql . "DT_CRIACAO = NOW() ";
	$sql = $sql . "WHERE CD_CERTIFICADO = " . $id;
}
else
{
	$sql = "INSERT INTO col_certificado (";
	$sql = $sql . "DS_PROGRAMA,";
	$sql = $sql . "DS_CURSO,";
	$sql = $sql . "programaDy,";
	$sql = $sql . "cursoDy,";
	$sql = $sql . "logo1,";
	$sql = $sql . "logo1Dy,";
	$sql = $sql . "logo1Dx,";
	$sql = $sql . "logo1W,";
	$sql = $sql . "logo1H,";
	$sql = $sql . "logo2,";
	$sql = $sql . "logo2Dy,";
	$sql = $sql . "logo2Dx,";
	$sql = $sql . "logo2W,";
	$sql = $sql . "logo2H,";
	$sql = $sql . "marcadagua,";
	$sql = $sql . "marcadaguaDy,";
	$sql = $sql . "marcadaguaDx,";
	$sql = $sql . "marcadaguaW,";
	$sql = $sql . "marcadaguaH,";
	$sql = $sql . "nome,";
	$sql = $sql . "linhaDy,";
	$sql = $sql . "txt1,";
	$sql = $sql . "txt1Dy,";
	$sql = $sql . "txt2,";
	$sql = $sql . "txt2Dy,";
	$sql = $sql . "txt3,";
	$sql = $sql . "txt3Dy,";
	$sql = $sql . "assinatura1,";
	$sql = $sql . "assinatura1Dy,";
	$sql = $sql . "assinatura1Dx,";
	$sql = $sql . "assinatura1W,";
	$sql = $sql . "assinatura1H,";
	$sql = $sql . "nome1,";
	$sql = $sql . "cargo1,";
	$sql = $sql . "conjunto1Dy,";
	$sql = $sql . "conjunto1Dx,";
	$sql = $sql . "assinatura2,";
	$sql = $sql . "assinatura2Dy,";
	$sql = $sql . "assinatura2Dx,";
	$sql = $sql . "assinatura2W,";
	$sql = $sql . "assinatura2H,";
	$sql = $sql . "nome2,";
	$sql = $sql . "cargo2,";
	$sql = $sql . "conjunto2Dy,";
	$sql = $sql . "conjunto2Dx,";
	$sql = $sql . "data,";
	$sql = $sql . "dataDy,";
	$sql = $sql . "medalhaStatus,";
	$sql = $sql . "medalhaDy,";
	$sql = $sql . "medalhaDx,";
	$sql = $sql . "nu_certificado,";
	$sql = $sql . "DT_CRIACAO,";
	$sql = $sql . "IN_ATIVO";
	$sql = $sql . ") VALUES (";
	$sql = $sql . "'" . $programa . "'";
	$sql = $sql . ",'" . $curso . "'";
	$sql = $sql . ",'" . $programaDy . "'";
	$sql = $sql . ",'" . $cursoDy . "'";
	$sql = $sql . ",'" . $logo1 . "'";
	$sql = $sql . ",'" . $logo1Dy . "'";
	$sql = $sql . ",'" . $logo1Dx . "'";
	$sql = $sql . ",'" . $logo1W . "'";
	$sql = $sql . ",'" . $logo1H . "'";
	$sql = $sql . ",'" . $logo2 . "'";
	$sql = $sql . ",'" . $logo2Dy . "'";
	$sql = $sql . ",'" . $logo2Dx . "'";
	$sql = $sql . ",'" . $logo2W . "'";
	$sql = $sql . ",'" . $logo2H . "'";
	$sql = $sql . ",'" . $marcadagua . "'";
	$sql = $sql . ",'" . $marcadaguaDy . "'";
	$sql = $sql . ",'" . $marcadaguaDx . "'";
	$sql = $sql . ",'" . $marcadaguaW . "'";
	$sql = $sql . ",'" . $marcadaguaH . "'";
	$sql = $sql . ",'" . $nome . "'";
	$sql = $sql . ",'" . $linhaDy . "'";
	$sql = $sql . ",'" . $txt1 . "'";
	$sql = $sql . ",'" . $txt1Dy . "'";
	$sql = $sql . ",'" . $txt2 . "'";
	$sql = $sql . ",'" . $txt2Dy . "'";
	$sql = $sql . ",'" . $txt3 . "'";
	$sql = $sql . ",'" . $txt3Dy . "'";
	$sql = $sql . ",'" . $assinatura1 . "'";
	$sql = $sql . ",'" . $assinatura1Dy . "'";
	$sql = $sql . ",'" . $assinatura1Dx . "'";
	$sql = $sql . ",'" . $assinatura1W . "'";
	$sql = $sql . ",'" . $assinatura1H . "'";
	$sql = $sql . ",'" . $nome1 . "'";
	$sql = $sql . ",'" . $cargo1 . "'";
	$sql = $sql . ",'" . $conjunto1Dy . "'";
	$sql = $sql . ",'" . $conjunto1Dx . "'";
	$sql = $sql . ",'" . $assinatura2 . "'";
	$sql = $sql . ",'" . $assinatura2Dy . "'";
	$sql = $sql . ",'" . $assinatura2Dx . "'";
	$sql = $sql . ",'" . $assinatura2W . "'";
	$sql = $sql . ",'" . $assinatura2H . "'";
	$sql = $sql . ",'" . $nome2 . "'";
	$sql = $sql . ",'" . $cargo2 . "'";
	$sql = $sql . ",'" . $conjunto2Dy . "'";
	$sql = $sql . ",'" . $conjunto2Dx . "'";
	$sql = $sql . ",'" . $data . "'";
	$sql = $sql . ",'" . $dataDy . "'";
	$sql = $sql . ",'" . $medalhaStatus . "'";
	$sql = $sql . ",'" . $medalhaDy . "'";
	$sql = $sql . ",'" . $medalhaDx . "'";
	$sql = $sql . ",'" . $nu_certificado . "'";
	$sql = $sql . "," . "NOW()";
	$sql = $sql . ",'" . "1" . "'";
	$sql = $sql . ")";
}
if($RS_query = mysql_query($sql)) $_SESSION["msg"] = "Banco atualizado com sucesso!     ";
else $_SESSION["msg"] = ERROR_MSG_SQLQUERY . mysql_error();

mysql_close();
?>
<head>
<script language="JavaScript">
if(self.opener&&!self.opener.closed)self.opener.location.reload();
else alert('<? echo $_SESSION["msg"]; ?>');
<?
if($_SESSION["msg"] == "Banco atualizado com sucesso!     ") echo "self.close();";
else echo "window.history.back();";
?>
</script>
</head>