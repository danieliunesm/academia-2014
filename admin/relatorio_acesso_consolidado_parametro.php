<?php


include('filtrocomponente.php');
include("../include/defines.php");
include('framework/crud.php');
include('controles.php');
include('datagrid.php');

function instanciarRN(){
	return new col_disciplina();
}

$paginaEdicao = "relatorio_acesso_consolidado.php";
$paginaEdicaoAltura = 600;
$paginaEdicaoLargura = 900;

$obterParametros = "'cboEmpresa=' + document.getElementById('cboEmpresaTodos').value + '&nomeEmpresa=' + document.getElementById('cboEmpresaTodos').options[document.getElementById('cboEmpresaTodos').selectedIndex].text + 
					'&cboLotacao=' + document.getElementById('cboLotacao').value + '&nomeLotacao=' + escape(document.getElementById('cboLotacao').options[document.getElementById('cboLotacao').selectedIndex].text) + 
					'&cboCargo=' + escape(document.getElementById('cboCargo').value) +
					'&cboUsuario=' + document.getElementById('cboUsuario').value + '&nomeUsuario=' + document.getElementById('cboUsuario').options[document.getElementById('cboUsuario').selectedIndex].text +
					'&txtDe=' + document.getElementById('txtDe').value + '&txtAte=' + document.getElementById('txtAte').value +
					'&hdnTipoRelatorio=' + document.getElementById('hdnTipoRelatorio').value + 
					'&hdnPaginasSelecionadas=' + selecionarlistPaginas()";

define(TITULO_PAGINA, "Relat�rios Consolidados de Acesso");

include('relatorio_cabecalho.php');

exibirParametros();

include('relatorio_rodape.php');

function exibirParametros(){
	
	?>
	
	<table cellpadding="0" cellspacing="0" border="0" align="center">
		
		<?php
			ExibirFiltroConsulta(false, false);
		?>
	
		<tr><td colspan="2">&nbsp;</tr>
		<tr>
			<td class="textblk">
				Per�odo: &nbsp;
			</td>
		</tr>
		<tr>
			<td class="textblk">
				De: <?php textbox("txtDe", "10", $_GET["txtDe"], "75px", "textbox3",false, Mascaras::Data); ?>
				At�: <?php textbox("txtAte", "10", $_GET["txtAte"], "75px", "textbox3",false, Mascaras::Data); ?>
			</td>
		</tr>	
		<tr><td colspan="1">&nbsp;</td></tr>
		<tr>
			<td class="textblk" colspan="2">
			
				<?php
					$hits = "";
					$dia = "";
					$sessao = "";
					
					switch ($_GET["rdoTipoRelatorio"])
					{
						case "H":
							$hits = "checked";
							break;
						case "D":
							$dia = "checked";
							break;
						case "S":
							$sessao = "checked";
							break;
						default:
							$dia = "checked";
							break;
					}
				
				?>
			
				<input type="radio" id="rdoTipoRelatorioDia" value="D" name="rdoTipoRelatorio" onclick="javascript:document.getElementById('hdnTipoRelatorio').value = 'D'" <?php echo $dia; ?> />Acessos de usu�rios<br />
				<input type="radio" id="rdoTipoRelatorioSessao" value="S" name="rdoTipoRelatorio" onclick="javascript:document.getElementById('hdnTipoRelatorio').value = 'S'" <?php echo $sessao; ?>/>Sess�es abertas<br />
				<input type="radio" id="rdoTipoRelatorioHits" value="H" name="rdoTipoRelatorio"  onclick="javascript:document.getElementById('hdnTipoRelatorio').value = 'H'" <?php echo $hits; ?>/>Hits de todas as Sess�es
				<input type="hidden" id="hdnTipoRelatorio" name="hdnTipoRelatorio" value="D" />
			</td>
		</tr>

	</table>
	
	<?php
	
}


?>