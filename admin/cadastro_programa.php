<?php

include('paginaBase.php');

function instanciarRN(){
	page::$rn = new col_programa();
}

function carregarRN(){
	
	$rn = new col_programa();
	
	page::$rn->CD_PROGRAMA = $_REQUEST["id"];
	page::$rn->CD_EMPRESA = $_POST["cboEmpresa"];
	page::$rn->DS_PROGRAMA = $_POST["txtPrograma"];
	
	$ciclos = array();
	
	$nomesCiclo = $_POST["txtCiclo"];
	$inicioCiclos = $_POST["txtDataInicio"];
	$terminoCiclos = $_POST["txtDataTermino"];
	
	
	
	for ($i = 0; $i < count($nomesCiclo); $i++){
        
		$ciclo = new col_ciclo();
		$ciclo->NM_CICLO = $nomesCiclo[$i];
		$ciclo->CD_EMPRESA = $_POST["cboEmpresa"];
		$ciclo->DT_INICIO = $inicioCiclos[$i];
		$ciclo->DT_TERMINO = $terminoCiclos[$i];
			
		$ciclos[] = $ciclo;
	}
	
	page::$rn->ciclos = $ciclos;
	
}

function pageRenderEspecifico(){
	
?>				<TR>
					<TD class="textblk" colspan="2">Empresa: *</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<?php comboboxEmpresa("cboEmpresa",  page::$rn->CD_EMPRESA, "Empresa"); ?>
					</TD>
				</TR>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<TD colspan="2" class=textblk>Programa: *</TD>
				</TR>
				<TR>
					<TD colspan="2"><?php textbox("txtPrograma", "100", page::$rn->DS_PROGRAMA, "100%", "textbox3", "Programa"); ?></TD>
				</TR>
				<TR><TD colspan="2">&nbsp;</TD></TR>
				<TR>
					<td colspan="2" class="textblk"><a href="javascript:adicionarCiclo()">Adicionar Ciclos</a></td>
					
				</TR>
				<tr>
					<td colspan="2" class="textblk">
					
						<script type="text/javascript" language="javascript">
						
							function adicionarCiclo(){
							
								var no = document.getElementById("dvCiclo").cloneNode(true);
								var dv = document.getElementById("dvCiclos");
								
								//alert(no.all.length)
								
								var itens = no.all.length;
								
								for (i=0; i<itens; i++){
									if (no.all[i].type == "text"){
										no.all[i].value = "";
									}else if (no.all[i].type == "select-one"){
										no.all[i].selectedIndex = 0;
									}
								}
								
								dv.appendChild(no);
								
							}
							
							function removerCiclo(obj){
								if (document.getElementById("dvCiclos").children.length==1){
									alert('Deve existir ao menos um ciclo.')
									return;
								}
								
								 document.getElementById("dvCiclos").removeChild(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
								
								//alert(obj.parentNode.parentNode.parentNode.parentNode.parentNode);
									
							}
						
						
						</script>
					
					
						<div id="dvCiclos">
							<?php
								$linhas = 1;
								$ciclos = page::$rn->ciclos;
								
								if (is_array($ciclos))
								{
									if (count($ciclos) > 1)
										$linhas = count($ciclos);
								}
								
								for ($i=0; $i < $linhas; $i++)
								{
									$nomeCiclo = "";
									$dataInicio = "";
									$dataTermino = "";
									$codigoCiclo = "";
									
									if (is_array($ciclos))
									{
										$ciclo = $ciclos[$i];
										$nomeCiclo = $ciclo->NM_CICLO;
										$dataInicio = $ciclo->DT_INICIO;
										$dataTermino = $ciclo->DT_TERMINO;
										$codigoCiclo = $ciclo->CD_CICLO;
										
									}
									
								
							
							?>
						
							<div id="dvCiclo" style="border: 1px solid black; margin-top: 2px;">
								<table cellpadding="2" cellspacing="0" width="98%">
									<tr>
										<td class="textblk" >Ciclo:</td>
										<td rowspan="4" class="textblk" align="center"><input type="image" src="/images/layout/bt_delsession.gif" value="" onclick="javascript:removerCiclo(this)" onfocus="noFocus(this)" onmousedown="this.style.backgroundPosition='5px 1px'" onmouseup="this.style.backgroundPosition='4px 0px'"></td>
									</tr>												
									<tr>
										<td class="textblk" ><?php textbox("txtCiclo[]", "100", $nomeCiclo, "100%", "textbox3", "Ciclo"); ?></td>
									</tr>
									<tr>
										<td class="textblk" >Per�odo:</td>
									</tr>
									<tr>
										<td class="textblk" >
											De <?php textbox("txtDataInicio[]", "10", $dataInicio, "100px", "textbox3", "Data de In�cio", Mascaras::Data) ?>
											At�	<?php textbox("txtDataTermino[]", "10", $dataTermino, "100px", "textbox3", "Data de T�rmino", Mascaras::Data) ?>
											<input type="hidden" id="hdnCodigoCiclo[]" name="hdnCodigoCiclo[]" value="<?php echo $codigoCiclo; ?>" />
										</td>
									</tr>
								</table>
							</div>
								<?php
								}
								?>
							
						</div>
						
					</td>
				</tr>
<?php

}

?>