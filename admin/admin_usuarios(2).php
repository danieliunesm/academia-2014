<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";

if($_SESSION["alias"]!="webmaster") header("Location: /admin/admin_menu.php");

$e = isset($_GET["e"])?$_GET["e"]:999;
$l = isset($_GET["l"])?$_GET["l"]:999;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript" src="/admin/include/js/adminfunctions.js"></script>
<script language="JavaScript">
queryfilter = 'e=<? echo $e; ?>&l=<? echo $l; ?>';
oEmp	    = <? echo $e; ?>;
oLot 	    = <? echo $l; ?>;
function setFilter(emp,lot){
	oEmp = emp;
	oLot = lot;
	if(oEmp == 999) oLot = 999;
	queryfilter = 'e='+oEmp+'&l='+oLot;
	document.location.href = '/admin/admin_usuarios.php?'+queryfilter;
}

function openWindow(){
	newWin=null;
	var w=520;
	var h=350;
	var l=(screen.width-w)/2;
	var t=(screen.height-h)/2;
	newWin=window.open('cadastro_usuario_importacao.php','importacaoEditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
	if(newWin!=null)setTimeout('newWin.focus()',100);
}

</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/images/layout/logo_admin.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><span class="title">USU�RIO:&nbsp;</span><?php echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="1"></td></tr>
</table>
<br><br>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr class="tarjaTitulo">
	<td height="20" colspan="12" align="center">USU�RIOS CADASTRADOS</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
<tr>
<td colspan="12">
	<table border="0" cellpadding="0" cellspacing="0" width="756">
	<tr>
	<td class="title">Empresa:&nbsp;</td><td><select style="width:300px;border:1px solid #666" onchange="setFilter(this[this.selectedIndex].value,oLot)">
	<option value="999">(todas as Empresas)</option>
<?php
$sql = "SELECT CD_EMPRESA, DS_EMPRESA FROM col_empresa WHERE IN_ATIVO = 1 ORDER BY DS_EMPRESA";
$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
while($oRs = mysql_fetch_row($RS_query))
{
	if($oRs[0] == $e) $selected = 'selected'; else $selected = '';
	echo "<option value='$oRs[0]' $selected>$oRs[1]</option>";
}
mysql_free_result($RS_query);
?>
	</select></td>
	<td class="title">Lota��o:&nbsp;</td><td><select style="width:300px;border:1px solid #666" onchange="setFilter(oEmp,this[this.selectedIndex].value)">
	<option value="999">(todas as Lota��es)</option>
<?php
$sql = "SELECT CD_LOTACAO, DS_LOTACAO FROM col_lotacao WHERE CD_EMPRESA = $e ORDER BY DS_LOTACAO";
$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
while($oRs = mysql_fetch_row($RS_query))
{
	if($oRs[0] == $l) $selected = 'selected'; else $selected = '';
	echo "<option value='$oRs[0]' $selected>$oRs[1]</option>";
}
mysql_free_result($RS_query);
?>
	</select></td>
	</tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
	<tr class="tarjaTitulo"><td colspan="12"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
	<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
	</table>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<form name="usuariosForm" action="admin_deleteuser.php" method="post">
<tr><td colspan="7" class="title"><a href="admin_formcadastro.php" onfocus="noFocus(this)"><img src="/images/layout/bt_addpeople.gif" width="20" height="20" border="0" vspace="10" align="absmiddle"><span class="title">&nbsp;Incluir Usu�rio</span></a>
<td colspan="5" class="title" align="right"><a href="javascript:openWindow()" onfocus="noFocus(this)"><img src="/images/layout/excel2.jpg" width="20" height="20" border="0" vspace="10" align="absmiddle"><span class="title">&nbsp;Importar Usu�rios</span></a></td></tr>
<tr>
	<td width="1%"></td>
	<td width="33%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="15%"><img src="/images/layout/blank.gif" width="100" height="1"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="15%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	<td width="15%"><img src="/images/layout/blank.gif" width="110" height="1"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	
	<td width="15%"></td>
	<td width="1%"><img src="/images/layout/blank.gif" width="10" height="1"></td>
	
	<td width="1%"></td>
</tr>
<tr class="tarjaItens">
	<td><img src="/images/layout/blank.gif" width="10" height="25"></td>
	<td align="center" class="title">USU�RIO</td>
	<td></td>
	<td align="center" class="title">LOGIN</td>
	<td></td>
	<td align="center" class="title">TIPO</td>
	<td></td>
	<td align="center" class="title" nowrap>DATA INCLUS�O</td>
	<td></td>
	<td align="center" class="title" nowrap>DETALHES</td>
	<td></td>
	<td align="center" class="title">&nbsp;&nbsp;Excluir&nbsp;&nbsp;</td>
</tr>
<?
//$sql = "SELECT L.ID, L.NM_USU, L.NM_LOGIN, L.DT_INCLUSAO, U.NM_TIPO_USU, U.LV_ACESSO FROM tb_login L, tb_tipo_usuario U WHERE L.TIPO_USU = U.LV_ACESSO ORDER BY L.NM_USU";
$whereEmp = ($e != 999) ? "AND U.empresa = $e" : "";
$whereLot = ($l != 999) ? "AND U.lotacao = $l" : "";
$sql = "SELECT U.CD_USUARIO, U.NM_USUARIO, U.NM_SOBRENOME, U.login, U.datacadastro, T.NM_TIPO_USU, T.LV_ACESSO FROM col_usuario U, tb_tipo_usuario T WHERE (U.tipo = T.ID $whereEmp $whereLot) ORDER BY U.NM_USUARIO";
$RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

$iCont = 0;
$numeroLinha = 0;
$bgcolor = "#ffffff";
while($oRs = mysql_fetch_row($RS_query))
{
	$numeroLinha++;
?>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
<tr bgcolor="<? echo $bgcolor; ?>">
	<td  class="textblk"><?php echo $numeroLinha; ?></td>
	<td class="textblk"><? echo $oRs[1] . " " . $oRs[2]; ?></td>
	<td></td>
	<td align="center" class="textblk"><? echo $oRs[3]; ?></td>
	<td></td>
	<td align="center" class="textblk"><? echo $oRs[5]; ?></td>
	<td></td>
	<td align="center" class="dataarquivos"><? echo FmtData($oRs[4]); ?></td>
	<td></td>
	
	<td align="center"><a href="admin_formcadastro.php?id=<? echo $oRs[0]; ?>" onfocus="noFocus(this)"><img src="/images/layout/bt_edit.gif" width="20" height="20" border="0"></a></td>
	<td></td>
	
	<td align="center">
<?
	//if(!($oRs[3]==1 && $oRs[2]=="admin"))
	if(!($oRs[3]=="webmaster"))
	{
?>
	<input type="checkbox" name="userID<? echo $oRs[0]; ?>" value="<? echo $oRs[0]; ?>">
<?
	}
?>
	</td>
</tr>
<?
	$iCont++;
	if($iCont % 2 == 0) $bgcolor="#ffffff";
	else $bgcolor="#f1f1f1";
}
?>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
<tr class="tarjaTitulo"><td colspan="12"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
	<td colspan="10" align="right" class="textblk">Excluir Usu�rios selecionados&nbsp;</td>
	<td><img src="/images/layout/seta.gif" width="10" height="10" border="0" align="absmiddle"></td>
	<td align="center"><input type="image" onfocus="noFocus(this)" src="/images/layout/bt_delpeople.gif" width="20" height="20" border="0"></td>
</tr>
</form>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr class="tarjaTitulo"><td colspan="12"><img src="/images/layout/blank.gif" width="100" height="1"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="40"></td></tr>
<tr><td colspan="12"><img src="/images/layout/blank.gif" width="288" height="1"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='admin_menu.php'" onfocus="noFocus(this)"></td></tr>
</table>
<br><br>
<?
mysql_free_result($RS_query);
mysql_close();
?>
</body>
</html>
