<?php
include "include/security2.php";
include "include/defines.php";
include "include/dbconnection.php";
include "include/genericfunctions.php";
include "include/codefunctions.php";
?>
<?php	writetopcode();?>
<script language="JavaScript">
function validaEmail(emailStr){
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")	;
	var matchArray=emailStr.match(emailPat);

	if(matchArray==null)return false;

	var user=matchArray[1];
	var domain=matchArray[2];

	if(user.match(userPat)==null)return false;

	var IPArray=domain.match(ipDomainPat);
	if(IPArray!=null){
		for(var i=1;i<=4;i++){
			if(IPArray[i]>255)return false;
		}
		return true;
	}	
	var domainArray=domain.match(domainPat);

	if(domainArray==null)return false;
	
	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;

	if(domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>4)return false;

	if(len<2)return false;

	return true;
}

function checkFormFields(){
	msg = "";
	if(document.mailForm.email.value == "" && document.mailForm.login.value == "" && document.mailForm.nome.value == "") msg+="Preencha ao menos o campo E-mail, CPF ou Nome!       \n";
	else{
		if(document.mailForm.email.value != "" && (!validaEmail(document.mailForm.email.value)))msg+="O e-mail fornecido n�o � v�lido! Verifique os dados digitados.       \n";
	}
	if(msg != ""){
		alert(msg);
		document.mailForm.email.focus();
		return false;
	}
	else{
		document.mailForm.submit();
	}
}

window.onload = function(){document.mailForm.email.focus();}
</script>
</head>
<body>
<div id="global">
<div id="globalpadding">
<?php writeheadercode();?>
<?php writemenucode();?>
	<div id="conteudo">
<?php
$titulo = "Solicita��o de dados cadastrais";
$conteudo = "Esqueceu seus dados de login? Favor preencher os campos abaixo.<br /><br /><br /><br />";
?>
		<h1><?php echo $titulo; writenavcode(1);?></h1>
<?php
		echo "<p>$conteudo</p>";
?>
		<table border="0">
		<form name="mailForm" method="post" action="requestdata.php">
		<tr>
		<td class="textblk" nowrap>Seu e-mail:</td>
		<td><input type="text" class="textbox" name="email" size="52" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'"></td>
		</tr>
		<tr>
		<td class="textblk" nowrap>CPF (Somente N�meros):</td>
		<td><input type="text" class="textbox" name="login" size="52" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'"></td>
		</tr>
		<tr>
		<td class="textblk" nowrap>Nome completo:</td>
		<td><input type="text" class="textbox" name="nome" size="52" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'"></td>
		</tr>
		<tr>
		<tr>
		<td class="textblk" nowrap>Matr�cula:</td>
		<td><input type="text" class="textbox" name="matricula" size="52" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'"></td>
		</tr>
		<tr>
		<td class="textblk" nowrap>Telefone (Com DDD):</td>
		<td><input type="text" class="textbox" name="telefone" size="52" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'"></td>
		</tr>
		<tr>
		<td class="textblk" nowrap>Estado:</td>
		<td><select class="textbox" style="width:45px" name="estado" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'">
			<option value="AC">AC</option>
			<option value="AL">AL</option>
			<option value="AM">AM</option>
			<option value="AP">AP</option>
			<option value="BA">BA</option>
			<option value="CE">CE</option>
			<option value="DF">DF</option>
			<option value="ES">ES</option>
			<option value="GO">GO</option>
			<option value="MA">MA</option>
			<option value="MG">MG</option>
			<option value="MS">MS</option>
			<option value="MT">MT</option>
			<option value="PA">PA</option>
			<option value="PB">PB</option>
			<option value="PE">PE</option>
			<option value="PI">PI</option>
			<option value="PR">PR</option>
			<option value="RJ">RJ</option>
			<option value="RN">RN</option>
			<option value="RO">RO</option>
			<option value="RR">RR</option>
			<option value="RS">RS</option>
			<option value="SC">SC</option>
			<option value="SE">SE</option>
			<option value="SP">SP</option>
			<option value="TO">TO</option>
			</select></td>
		<!-- <td><input type="text" class="textbox" name="estado" size="52" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'"></td> -->
		</tr>
		<tr>
		<td class="textblk" nowrap>Empresa:</td>
		<td><input type="text" class="textbox" name="tbp" size="52" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'"></td><!-- empresa -->
		</tr>
		<!-- <tr>
		<td class="textblk" nowrap>TBP:</td>
		<td><input type="text" class="textbox" name="tbp" size="52" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'"></td>
		</tr> -->
		<tr>
		<td></td>
		<td align="center"><input class="buttonsty" type="button" value="    Enviar    " onclick="checkFormFields()" onfocus="if(this.blur())this.blur()">&nbsp;&nbsp;<input class="buttonsty" type="reset" name="Reset" value="   Limpar   " onfocus="if(this.blur())this.blur();document.mailForm.email.focus()"><br /><br /></td>
		</tr>
		</form>
		</table>
	</div>
<?php writebottomcode(); ?>
</div>
</div>
</body>
</html>
<?
mysql_close();
?>