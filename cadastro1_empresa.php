<?php
include "include/security2.php";
include "include/defines.php";
include "include/dbconnection.php";
include "include/genericfunctions.php";
include "include/codefunctions.php";

$idhash = isset($_GET["b"])?(String)$_GET["b"]:'0';
$id 	= 0;

// df732b2327eb9f87266f511a49ba1ef1 -> Embratel

// d013aac43fbf37d43ddd710cd8f4a9e9 -> Colaborae

for($i = 1; $i < 1000; $i++){
	$str	 = "empresa_id=$i";
	$strhash = md5($str);
	if($idhash == $strhash){
		$id = $i;
		break;
	}
}
?>
<?php writetopcode(); ?>
<script type="text/javascript" src="include/js/ajax.js"></script>
<script language="JavaScript">
function ValidaData(DtVal){
	DtVal = Exclui_inval(DtVal, 8);
	var msg = "";
	if(DtVal.length != 8){
		msg = "Preencha a Data de Nascimento no formato DDMMAAAA!     \n";
        return msg;
	}
	var nChar = null;
	for (i=0;i<8;i++){
		nChar = DtVal.charAt(i);
		if(isNaN(nChar)){
			msg = "Caracter \ " + nChar + " \ n�o � v�lido.     \n";
			return msg;
		}
	}
	var DtAcerto = null;
	DtAcerto = DtVal.substring(2,4) + "/" + DtVal.substring(0,2) + "/" + DtVal.substring(4,8);
	var bError 		= false;
	var aChar 		= null;
	var holder 		= null;
	var Short 		= null;
	var FebCheck 	= null;
	var NovCheck 	= null;
	var MonthCheck	= null;
	
	if(parseInt(DtVal.substring(2,4)) == 0 || parseInt(DtVal.substring(0,2)) == 0 || parseInt(DtVal.substring(4,8)) == 0) bError = true;
	
    for(i=0;i<10;i++){
		aChar = DtAcerto.charAt(i);
		if(i == 0 && aChar > 1) bError = true;
		if(i == 0 && aChar < 0) bError = true;
		if(i == 0 && aChar == 1) MonthCheck = 1;
		if(i == 0 && aChar == 1) NovCheck = 1;
		if(i == 1 && aChar < "0" || aChar > "9") bError = true;
		if(i == 1 && aChar == 2) FebCheck=1;
		if(i == 1 && MonthCheck == 1 && aChar >2) bError = true;
		if(i == 1 && aChar == "4" || aChar =="6" || aChar =="9") Short = 1;
		if(i == 1 && NovCheck == 1 && aChar == 1) Short = 1;
		if(i == 2 && aChar != "/") bError = true;
        if(FebCheck == 1 && NovCheck != 1){
			if(i==3 && aChar > 2) bError = true;
		}
		if (i == 3 && aChar > 3) bError = true;
		if (i == 3 && aChar ==3) holder=1;
		if (i == 4 && aChar >0 && Short==1 && holder==1) bError = true;
		if (i == 4 && aChar >1 && holder==1) bError = true;
		if (i == 4 && aChar < "0" || aChar > "9") bError = true;
		if (i == 5 && aChar != "/")  bError = true;
		if (i == 8 && aChar < "0" || aChar > "9") bError = true;
		if (i == 9 && aChar < "0" || aChar > "9") bError = true;
	}
	if((DtAcerto.substring(0,5) == "02/29") && DtAcerto.substring(6,10) % 4 != 0){
		msg =  "Data de Nascimento inv�lida. N�o � ano bissexto!     \n";
		return msg;
	}
    if(bError){
		msg =  "Data de Nascimento inv�lida!     \n";
		return msg;
	}
	else{
		return "";
	}
}

function validaEmail(emailStr){
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")	;
	var matchArray=emailStr.match(emailPat);

	if(matchArray==null)return false;

	var user=matchArray[1];
	var domain=matchArray[2];

	if(user.match(userPat)==null)return false;

	var IPArray=domain.match(ipDomainPat);
	if(IPArray!=null){
		for(var i=1;i<=4;i++){
			if(IPArray[i]>255)return false;
		}
		return true;
	}	
	var domainArray=domain.match(domainPat);

	if(domainArray==null)return false;
	
	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;

	if(domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>4)return false;

	if(len<2)return false;

	return true;
}

function checkFormFields(){
	var msg 	 = "";
	var msgdata	 = "";
	var f   	 = document.cadastroForm;
	var objfocus = null;
	if(f.nome.value 		== ""){
		msg+="O campo Nome precisa ser preenchido!       \n";
		f.nome.style.backgroundColor = '#ddd';
		objfocus = f.nome;
	}
	if(f.sobrenome.value	== ""){
		msg+="O campo Sobrenome precisa ser preenchido!       \n";
		f.sobrenome.style.backgroundColor = '#ddd';
		if(objfocus == null) objfocus = f.sobrenome;
	}
	if(f.email.value 		== ""){
		msg+="O campo E-mail precisa ser preenchido!       \n";
		f.email.style.backgroundColor = '#ddd';
		if(objfocus == null) objfocus = f.email;
	}
	else{
		if(!validaEmail(f.email.value)){
			msg+="O e-mail fornecido n�o � v�lido! Verifique os dados digitados.       \n";
			f.email.style.backgroundColor = '#ddd';
			if(objfocus == null) objfocus = f.email;
		}
	}
	if(document.getElementById('nomeempresa').selectedIndex == -1){
		msg+="� preciso selecionar o campo Empresa!       \n";
		document.getElementById('nomeempresa').style.backgroundColor = '#ddd';
		if(objfocus == null) objfocus = document.getElementById('nomeempresa');
	}
	if(!f.policyagreement.checked){
		if(msg != '') msg+="\n";
		msg+="Voc� precisa concordar com os termos de privacidade para submeter os dados solicitados!       \n";
		if(objfocus == null) objfocus = f.policyagreement;
	}
	if(msg != ""){
		alert(msg);
		if(objfocus){
			if(objfocus == f.nome) self.scrollTo(0,0);
			objfocus.focus();
		}
		return false;
	}
	else{
		f.submit();
	}
}
</script>
</head>
<body>
<div id="global">
<div id="globalpadding">
<?php writeheadercode();?>
<?php writemenucode();?>
	<div id="conteudo">
<?php
$titulo = "Cadastro";
?>
		<h1><?php echo $titulo; writenavcode(1);?></h1>
		
		<table border="0" cellpadding="0" cellspacing="5">
		<form name="cadastroForm" enctype="multipart/form-data" method="post" action="gravarcadastro1.php">
		<tr>
		<td class="textblk2" colspan="2">
		Cadastre-se (os campos assinalados com <span>*</span> precisam ser preenchidos):<br /><br />
		</td>
		</tr>
		<tr>
		<td width="1%" class="textblk2" align="right" nowrap><span>*</span>Nome:</td>
		<td width="99%"><input type="text" class="textbox8" name="nome" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="50" tabIndex="10"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap><span>*</span>Sobrenome:</td>
		<td><input type="text" class="textbox8" name="sobrenome" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="50" tabIndex="11"></td>
		</tr>
<?php
if($id == 5)
{
?>
		<tr>
		<td class="textblk2" align="right" nowrap>Nascimento:</td>
		<td><input type="text" class="textbox8b" name="datanasc" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="10" tabIndex="12" onkeypress="return(Formata_Dados(this,'/','',event,'DATA'))">&nbsp;(ddmmaaaa)</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Naturalidade:</td>
		<td class="textblk2"><input type="text" class="textbox8d" name="naturalidade" size="31" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="50" tabIndex="13">&nbsp;UF&nbsp;<input type="text" class="textbox9" name="uf" size="2" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="2" tabIndex="14" onkeyup="this.value=this.value.toUpperCase()"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Nacionalidade:</td>
		<td><input type="text" class="textbox8d" name="nacionalidade" size="31" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="10" tabIndex="15"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Endere�o res.:</td>
		<td><input type="text" class="textbox8" name="enderecores" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="16"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>CEP res.:</td>
		<td><input type="text" class="textbox8b" name="cepres" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="10" tabIndex="17" onkeypress="return(Formata_Dados(this,'.','-',event,'CEP'))">&nbsp;(apenas n�meros)</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>(DDD) Telefone res.:</td>
		<td><input type="text" class="textbox9" name="dddtelres" size="3" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="3" tabIndex="18" onkeypress="return(Formata_Dados(this,'','',event,'DDD'))"><input type="text" class="textbox8b" name="telres" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="8" tabIndex="19" onkeypress="return(Formata_Dados(this,'','',event,'TEL'))"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>(DDD) Celular:</td>
		<td><input type="text" class="textbox9" name="dddcel" size="3" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="3" tabIndex="20" onkeypress="return(Formata_Dados(this,'','',event,'DDD'))"><input type="text" class="textbox8b" name="cel" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="8" tabIndex="21" onkeypress="return(Formata_Dados(this,'','',event,'TEL'))"></td>
		</tr>
<?php
}
?>
		<tr>
		<td class="textblk2" align="right" nowrap><span>*</span>E-mail:</td>
		<td><input type="text" class="textbox8" name="email" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="22"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap><span>*</span>Empresa:</td>
		<td>
			<select class="textbox8e" name="empresa" id="nomeempresa" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" tabIndex="23" onchange="sendRequest(this)">
<?php
$sql = "SELECT CD_EMPRESA, DS_EMPRESA FROM col_empresa WHERE IN_ATIVO = 1 AND CD_EMPRESA = $id ORDER BY DS_EMPRESA";
$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
while($oRs = mysql_fetch_row($query)){
	echo "<option value='$oRs[0]'>$oRs[1]</option>";
}
mysql_free_result($query);
?>
			</select>
		</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Departamento:</td>
		<td>
			<select class="textbox8e" name="lotacao" id="lotacao" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" tabIndex="24">
<?php
$sql = "SELECT CD_LOTACAO, DS_LOTACAO FROM col_lotacao WHERE CD_EMPRESA = $id ORDER BY DS_LOTACAO";
$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
while($oRs = mysql_fetch_row($query)){
	echo "<option value='$oRs[0]'>$oRs[1]</option>";
}
mysql_free_result($query);
?>
			</select>
		</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Cargo/Fun��o:</td>
		<td><input type="text" class="textbox8" name="cargofuncao" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="50" tabIndex="25"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Endere�o com.:</td>
		<td><input type="text" class="textbox8" name="enderecocom" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="26"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>CEP com.:</td>
		<td><input type="text" class="textbox8b" name="cepcom" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="10" tabIndex="27" onkeypress="return(Formata_Dados(this,'.','-',event,'CEP'))">&nbsp;(apenas n�meros)</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>(DDD) Telefone com.:</td>
		<td><input type="text" class="textbox9" name="dddtelcom" size="3" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="3" tabIndex="28" onkeypress="return(Formata_Dados(this,'','',event,'DDD'))"><input type="text" class="textbox8b" name="telcom" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="8" tabIndex="29" onkeypress="return(Formata_Dados(this,'','',event,'TEL'))">&nbsp;Ramal&nbsp;<input type="text" class="textbox8b" name="ramal" size="10" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="8" tabIndex="30" onkeypress="return(Formata_Dados(this,'','',event,'TEL'))"></td>
		</tr>
<?php
if($id == 5)
{
?>
		<tr>
		<td class="textblk2" style="vertical-align:top;text-align:right" nowrap>Experi�ncia:</td>
		<td>
			<select class="textbox8c" name="experiencia[]" id="experiencia" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" multiple tabIndex="31">
			<option>Planejamento Estrat�gico
			<option>Planejamento de Marketing
			<option>Planejamento Operacional
			<option>Avalia��o Financeira
			<option>Ger�ncia de Servi�o
			<option>Vendas Complexas
			<option>Vendas Pequenas Empresas
			<option>Vendas Varejo
			<option>Projeto e Gest�o
			<option>Desenvolvimento de Conte�o
			<option>M�todos e Ferramentas de Aprendizado
			<option>Otimiza��o de Processos
			<option>Instrutoria
			<option>Intelig�ncia Competitiva
			<option>Projetos TIC
			</select><br />Para selecionar mais de um item, tecle CTRL + clique.<br /><br /><a href="javascript:void(0)" onclick="this.parentNode.childNodes[0].selectedIndex = -1" title="   Limpar itens selecionados acima   "><img src="/images/layout/bt_clearselect.gif" style="width:23px;height:23px;margin-right:5px"></a>Limpar itens selecionados acima<br /><br />
		</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Outros:</td>
		<td><input type="text" class="textbox8" name="outrosexperiencia" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="32"></td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
		<td class="textblk2" style="vertical-align:top;text-align:right" nowrap>Especializa��o:</td>
		<td>
			<select class="textbox8c" name="especializacao[]" id="especializacao" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" multiple tabIndex="33">
			<option>Converg�ncia IP
			<option>Converg�ncia Fixo-M�vel
			<option>Mobilidade
			<option>Wireless
			<option>Tecnologia de Acesso
			<option>Telefonia Fixa
			<option>Telefonia M�vel
			<option>Integra��o de Voz e Dados
			<option>Seguran�a de Redes
			<option>Call Center/Contact Center
			<option>Regulamenta��o
			</select><br />Para selecionar mais de um item, tecle CTRL + clique.<br /><br /><a href="javascript:void(0)" onclick="this.parentNode.childNodes[0].selectedIndex = -1" title="   Limpar itens selecionados acima  "><img src="/images/layout/bt_clearselect.gif" style="width:23px;height:23px;margin-right:5px"></a>Limpar itens selecionados acima<br /><br />
		</td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Outros:</td>
		<td><input type="text" class="textbox8" name="outrosespecializacao" size="40" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="255" tabIndex="34"></td>
		</tr>
		
		<tr class="optionrow"><td></td><td><br />Se voc� desejar, pode anexar uma foto sua e o seu curriculum.<br /><br /></td></tr>
		
		<tr>
		<td class="textblk2" align="right" nowrap>Anexar foto:</td>
		<td><input type="file" class="textbox8e" name="foto" size="26" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="100" tabIndex="35"></td>
		</tr>
		<tr>
		<td class="textblk2" align="right" nowrap>Anexar Curriculum:</td>
		<td><input type="file" class="textbox8e" name="cv" size="26" onfocus="this.style.backgroundColor='#ffc'" onblur="this.style.backgroundColor='#fff'" maxlength="100" tabIndex="36"></td>
		</tr>
<?php
}
?>
		<tr><td><br /></td></tr>
		
		<tr><td colspan="2"><span>&nbsp;&nbsp;&nbsp;Pol�tica de Privacidade Colaborae:</span><div id="policy">A Pol�tica de Privacidade da Colaborae foi criada para demonstrar nosso compromisso  com a seguran�a dos conte�dos apresentados no site e com a privacidade das informa��es coletadas dos usu�rios.<br><br>Voc� poder� visitar nosso site, conhecer nossos servi�os, verificar oportunidades de carreira e obter informa��es e not�cias, sem nos fornecer nenhuma informa��o. Caso nos forne�a alguma informa��o, esta pol�tica esclarece como a Colaborae coleta e trata suas informa��es individuais.<br><br>Qualquer informa��o que o usu�rio deste site forne�a ser� coletada e guardada de acordo com padr�es r�gidos de seguran�a e confidencialidade. E, a menos que se tenha determina��o legal ou judicial, essas informa��es n�o ser�o transferidas a terceiros ou usadas para outras finalidades.<br><br>Para fins administrativos, eventualmente poderemos utilizar cookies, sendo que o usu�rio pode, a qualquer momento, ativar em seu browser mecanismos para inform�-lo quando estiverem acionados ou para evitar que sejam acionados.<br><br>Disponibilizamos para nossos usu�rios o canal Fale Conosco, para solucionar quaisquer d�vidas.<br><br><strong>Cookies</strong><br><br>Cookies s�o pequenos arquivos no formato texto que ficam gravados na sua m�quina. A utiliza��o desses arquivos � muito comum e nos d� grande flexibilidade na disposi��o das informa��es exibidas. Esses arquivos nos permitem gravar alguns dados importantes, como o n�mero de vezes que voc� acessou o site, para que mais tarde possamos n�o exibir um conte�do repetido, por exemplo. A maioria dos navegadores permite bloquear o recebimento de cookies.<br><br>N�o coletamos informa��es que poderiam identificar indiv�duos a n�o ser quando solicitado ou fornecido. Os dados pessoais recolhidos online receber�o tratamento automatizado, passando a integrar a base de dados do site, gerando estat�sticas e informa��es destinadas a proporcionar aos nossos usu�rios facilidades de navega��o, amplia��o e melhoria dos conte�dos oferecidos.</div></td></tr>
		
		<tr>
		<td class="textblk2" align="right"><span>*</span><input type="checkbox" name="policyagreement" tabIndex="37"></td>
		<td>Concordo com os termos de privacidade.</td>
		</tr>
		<tr>
		<td class="textblk2" align="right"><input type="checkbox" name="newsletteragreement" tabIndex="38"></td>
		<td>Desejo receber regularmente o newsletter da Colaborae.</td>
		</tr>
		<tr><td><br /></td></tr>
		<tr>
		<td align="center" colspan="2"><input class="buttonsty8" type="button" value="    Enviar    " onclick="checkFormFields()" tabIndex="39">&nbsp;&nbsp;<input class="buttonsty8" type="reset" name="Reset" value="   Limpar   " onblur="document.cadastroForm.nome.focus()" tabIndex="40"><br /><br /></td>
		</tr>
		</form>
		</table>
	</div>
<?php writebottomcode(); ?>
</div>
</div>
</body>
</html>
<?php
mysql_close();
?>