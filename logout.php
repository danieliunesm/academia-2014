<?
session_start();

include("include/defines.php");
include("include/contadorforum.php");
excluiControleSessaoUsuario($_SESSION['cd_usu']);

$ref = $_SESSION['ref'];

unset($_SESSION['userFake']);
unset($_SESSION['blogPrograma']);
unset($_SESSION['usuario']);
unset($_SESSION['nome']);
unset($_SESSION['empresaID']);
unset($_SESSION['email']);
unset($_SESSION['ref']);
unset($_SESSION['PASTA']);

setcookie("uid", null, time() - 3600, '/');

//header("Location:".$_SERVER["HTTP_REFERER"]);
header("Location:/index.php");
?>