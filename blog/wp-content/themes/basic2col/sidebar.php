<!-- begin sidebar -->
<div id="sidebar">
	<ul id="sb1">
	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar 1') ) : else : ?>

	<?php if((function_exists('is_front_page') && !is_front_page()) && !is_home())/*link to home */{ ?>
		<li id="homelink"><a href="<?php bloginfo('url'); ?>/"><?php _e('Home','basic2col'); ?></a></li>
	<?php } ?>


	<li id="search">
	<form id="searchform" method="get" action="<?php bloginfo('url') ?>">
	
		<h2><label for="s"><?php _e('Search','basic2col'); ?></label></h2>
		<div>
			<input type="text" value="" name="s" id="s" />
			<input type="submit" class="submit" accesskey="s" value="<?php _e('Search','basic2col'); ?>" />
		</div>
		</form>
	</li>
	<li id="tagcloud"><h2><?php _e('Artigos recentes','basic2col'); ?></h2>
		<ul>
		<?php
		global $post;
		$args = array( 'numberposts' => 5 );
		$myposts = get_posts( $args );
		foreach( $myposts as $post ) :	setup_postdata($post); ?>
			<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
		<?php endforeach; ?>
		</ul>
	</li>

	<?php wp_list_pages('sort_column=menu_order&title_li=<h2>' . __('Pages','basic2col') . '</h2>'); ?>


	
	<?php wp_list_categories('sort_column=name&show_count=1&title_li=<h2>' . __('Categories','basic2col') . '</h2>'); ?>
	<?php endif; ?>
	</ul>


	<!--Modificado por Filipe Iack-->
	<ul id="sb2">
		<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar 2') ) : else : ?>
		<li id="tagcloud"><h2><?php //_e('Tags','basic2col'); ?></h2>
			<?php //wp_tag_cloud('smallest=10&largest=12&unit=px'); ?>
		</li>

		<!--
		<li id="randomlinks"><h2><?php //_e('Random links','basic2col'); ?></h2>
			<ul>
			<?php //wp_list_bookmarks('orderby=rand&categorize=0&limit=10&title_li='); ?>
			</ul>
		</li>
		-->


			<!--
		<li><h2><?php //_e('Meta','basic2col'); ?></h2>
			<ul>
			<li>	<a href="<?php //bloginfo('rss2_url'); ?>">
				<img src="<?php //bloginfo('template_url')?>/gfx/rss.png" alt="RSS" class="rssimg" /> 
				<?php //_e('Posts','basic2col'); ?></a></li>
			<li>	<a href="<?php //bloginfo('comments_rss2_url'); ?>">
				<img src="<?php //bloginfo('template_url')?>/gfx/rss.png" alt="RSS" class="rssimg" />
				<?php //_e('Comments','basic2col'); ?></a></li>
				
			<?php //wp_register(); ?>
			<li><?php //wp_loginout(); ?></li>

			<?php /*add some support for MU*/ //if(is_basic2col_wpmu()) { global $current_site; ?>
			<li><a href="http://<?php //echo $current_site->domain . $current_site->path ?>" title="<?php //_e('Hosted by','basic2col'); ?> <?php //echo $current_site->site_name ?>">
			<?php //echo $current_site->site_name ?></a></li>
			
			<?php //} wp_meta(); ?>
			</ul>
		</li>
			-->

		<?php if ( is_single() ) {
			global $wpdb;
			$sql = "select cd_usuario, fotofile, cv from col_usuario where email = '".get_the_author_meta('user_email',$posts[0]->post_author)."' order by fotofile desc, cv desc LIMIT 1";
			$theAuthor = $wpdb->get_results( $wpdb->prepare($sql));
			if ($theAuthor!=null){
				if ($theAuthor[0]->fotofile != null && $theAuthor[0]->fotofile != ""){
		?>
		<li>
			<div style="position:relative;width:120px;">
				<div style="position:absolute;left:0px;top:0px;width:116px;height:100px;z-index:2"><img src="/images/layout/modulo_colmeia_big.gif" width="116" height="100"></div>
				<div style="position:absolute;left:1px;top:1px;width:114px;height:98px;z-index:1"><img src="/images/colaboradores/<?php echo $theAuthor[0]->fotofile;?>" width="114" height="98"></div>
				<span style="position:absolute;top:105px;width:100%;margin:auto;padding-left:30px;"><a target="_blank" href="/colaboradoressubitens.php?id=<?php echo $theAuthor[0]->cd_usuario;?>">Sobre o Autor</a></span>
			</div>
		</li>
		<?php } } } ?>
		
<?php endif; ?>
</ul>

</div>
<!-- end sidebar -->
<!--Filipe Iack-->

