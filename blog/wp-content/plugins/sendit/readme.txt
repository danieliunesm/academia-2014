=== Sendit ===
Contributors: Giuseppe Surace
Donate link: http://www.giuseppesurace.com/wordpress/
Tags: newsletter, mailing list, mailinglist, subscription form
Requires at least: 2.3.0
Tested up to: 3.0.3
Stable tag: 1.5.8

Sendit is a mailing lists and newsletter plugin for Wordpress to manage mailing list.

== Description == 
Sendit enable you to send newsletters and collect subscribers from your blog. You can manage one or more list and using in post/pages or using the widget(customizable). You can extract content from post or pages or create your content to send to subscribers. You can also edit the template for each mailing lists just put some XHTML code on header and footer. Newsletter will be send in XHTML format. Checkout the professional newsletter scheduler Sendit Pro to use with cron jobs: http://sendit.wordpressplanet.org

Version 1.5.8 set the jQuery in noconflict mode
Version 1.5.7 add the function to buy the tool to export mailing lists as CSV files.
Version 1.5.6 fix the .info emails subscription also on the wp-admin subscribtion.
Change Ajax Sack library with with Jquery Ajax for subscriptions so no more Ajax error in voting.
Also definitively removed PHPmailer using everywhere wp_mail()
You can buy a pro edition at this url
[Sendit Official Website](http://sendit.wordpressplanet.org "Sendit Wordpress Plugin")
 

Version 1.5.5 fix the .info emails subscrption.
New version fixes historical bugs like gmail smtp settings and confirmation message. Also 1.5.4 fixed the historical "T_OLD FUNCTION" bug, using directly the native wp_mail function instead to redeclare anoter phpmailer class. 


All other information (general, changelog, installation, upgrade, usage) you need about this plugin can be found here: [Sendit readme](http://www.giuseppesurace.com/ "Sendit Readme").
It is the exact same readme.html is included in the zip package.

== Development Blog ==

[Giuseppe Surace WP development blog](http://www.giuseppesurace.com "Italian Wp designer and plugin developer")
[Sendit Official Website](http://sendit.wordpressplanet.org "Sendit Wordpress Plugin")


== Installation ==

1. Upload `sendit dir` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to settings and edit your mailing list or create new one
4. Setup (if needed) your SMTP favorite settings
5. Setup (if you have large list) the interval and how many emails to sends
6. if you need to send large mailing list get the pro version at http://sendit.wordpressplanet.org
[Sendit Readme](http://www.giuseppesurace.com "Sendit Readme") (Installation Tab)

== Screenshots ==

[Sendit Screenshots](http://www.giuseppesurace.com/ "Sendit Screenshots")

== Frequently Asked Questions ==

[Sendit Support Forums](http://www.giuseppesurace.com/plugin-lab/ "Sendit Support Forums")