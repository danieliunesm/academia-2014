<?php
/**
 * @package Auto-Login Colaborae
 * @version 1.0
 */
/*
Plugin Name: Colaborae Plugin
Plugin URI: http://colaborae.com.br
Description: Este plugin foi criado para o site Colaborae. Verifica se o usuario do programa esta logado e realiza o login automatico no blog wordpress.

Author: Filipe Iack
Version: 1.0
Author URI: http://colaborae.com.br/
*/

//Codigos para seguranca
remove_filter('comment_text', 'make_clickable', 9);
remove_action('wp_head', 'wp_generator');
define('WP_HTTP_BLOCK_EXTERNAL', true);
define('WP_DEFAULT_THEME', 'basic2col');
//unregister_widget('WP_Widget_Meta');

/* Desabilitar Admin Bar. */
add_filter('show_admin_bar', '__return_false' );
/* Remover Admin Bar do painel de preferencias do usu�rio */
remove_action('personal_options', '_admin_bar_preferences');

// This will occur when the comment is posted
function plc_comment_post( $incoming_comment ) {
 
    // convert everything in a comment to display literally
    $incoming_comment['comment_content'] = htmlspecialchars($incoming_comment['comment_content']);
 
    // the one exception is single quotes, which cannot be #039; because WordPress marks it as spam
    $incoming_comment['comment_content'] = str_replace( "'", '&apos;', $incoming_comment['comment_content'] );
 
    return( $incoming_comment );
}
 
// This will occur before a comment is displayed
function plc_comment_display( $comment_to_display ) {
 
    // Put the single quotes back in
    $comment_to_display = str_replace( '&apos;', "'", $comment_to_display );
 
    return $comment_to_display;
}
 
add_filter( 'preprocess_comment', 'plc_comment_post', '', 1);
add_filter( 'comment_text', 'plc_comment_display', '', 1);
add_filter( 'comment_text_rss', 'plc_comment_display', '', 1);
add_filter( 'comment_excerpt', 'plc_comment_display', '', 1);



function verificarPermissao() {
	global $current_blog;
	
//	if ($current_blog->public == 0){	//blog nao publico
	if ($current_blog->public == 0 && !is_user_logged_in()){	//blog nao publico e nao tem usuario logado
		session_start();
		$urlBlog = split("/", $_SERVER["REQUEST_URI"]);
		$nameBlog = split("/", $_SESSION['blogPrograma']);
//		echo "0 - ".$_SESSION['alias'];
//		echo "<br>1 - ".$urlBlog[2] . "<br>2 - ".$nameBlog[0];
		if ((empty($_SESSION['blogPrograma']) ) || (strcasecmp($urlBlog[2],$nameBlog[0]) != 0))	//(empty($_SESSION['alias']) || empty($_SESSION['cd_usu']) || 
		{
			//header("Location: http://www.colaborae.com.br");
//			echo "<b>Sem permiss�o para este blog ou sess�o expirada!</b>";
//			exit();
		}
//		Parte necessaria para realizar o auto login.
//		autoLoginAdmin();
	}
}
add_action('init', 'verificarPermissao');

function my_custom_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image:url(http://www.colaborae.com.br/images/layout/plogo.png) !important; width:300px; }
	</style>';
}
add_action('login_head', 'my_custom_login_logo');

function wpbeginner_remove_version() {
	return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');


function extra_contact_info($contactmethods) {
    unset($contactmethods['aim']);
    unset($contactmethods['yim']);
    unset($contactmethods['jabber']);
    //$contactmethods['imgusuario'] = 'Imagem Usuario';
    $contactmethods['facebook'] = 'Facebook';
    $contactmethods['twitter'] = 'Twitter';
 
    return $contactmethods;
}
add_filter('user_contactmethods', 'extra_contact_info');

function autoLoginAdmin(){
	session_start();
	if (!empty($_SESSION['alias']) && $_SESSION['tipo'] >= 3 && is_admin())
	{
		$user_login = $_SESSION['alias'];
        $user = get_userdatabylogin($user_login);
        if (!empty($user)){
	        $user_id = $user->ID;
	        wp_set_current_user($user_id, $user_login);
	        wp_set_auth_cookie($user_id);
	        do_action('wp_login', $user_login);
        }
    }
}
add_action('init', 'autoLoginAdmin');

//function my_force_login() {
//	global $post;
//	 
//	if (!is_single()) return;
//	$ids = array(188, 185, 171); // array of post IDs that force login to read
//	 
//	if (in_array((int)$post->ID, $ids) && !is_user_logged_in()) {
//		auth_redirect();
//	}
//}
//add_action('admin_menu', 'my_force_login');
 
function alx_remove_meta_box(){
	// remove a box Quick Press
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'advanced' );
}
add_action( 'admin_menu', 'alx_remove_meta_box' ); //chama a fun��o que remove


//function get_author_bio ($content=''){
//    global $post;
//
//    $post_author_name=get_the_author_meta("display_name");
//    $post_author_description=get_the_author_meta("description");
//    $html="<hr style='width:90%; color:#000;'><div class='clearfix' id='about_author'>\n";
//    $html.="<img width='80' height='80' class='avatar'
//src='http://www.gravatar.com/avatar.php?gravatar_id=".md5(get_the_author_email()).
//"&default=".urlencode($GLOBALS['defaultgravatar'])."&size=80&r=PG' alt='PG'/>\n";
//    $html.="<div class='author_text'>\n";
//    $html.="<h4>Author: <span>".$post_author_name."</span></h4>\n";
//    $html.= $post_author_description."\n";
//    $html.="</div>\n";
//    $html.="<div class='clear'></div>\n";
//    $content .= $html;
//    }
//
//    return $content;
//
//add_filter('the_content', 'get_author_bio');

?>