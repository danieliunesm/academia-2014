function onSlideShowProReady() {
	var hss = document.getElementById("homeSlideShow"); 
	hss.addEventListener("imageData","onImageData");
//	hss.addEventListener("imageLoad","onImageLoad"); 
}
	
function onImageData(evt){
//	alert(evt.data.number);
	var hsst = document.getElementById("homeslideshowtext"); 
	var currentMovie = evt.data.number;
	for(var i = 1; i < 7; i++){
		if(i == currentMovie){
			hsst.TPlay("/cena"+i); 
		}
		else{
			hsst.TGotoFrame(("/cena"+i), 1);
			hsst.TStopPlay("/cena"+i);
		}
	}
}

function onImageLoad(evt){
//	alert(evt.num);
}

var xmlFile = "home.xml"; 
var d = new Date(); 
var xmlFilePath = xmlFile + "?rn=" + d.getTime() + (Math.random()*100);

var flashvars = {
	audioLoop: "On",
	audioPause: "Off",
	audioVolume: 50,
	autoFinishMode: "Switch",
	contentAlign: "Top Center",
	contentAreaAction: "Event",
	contentAreaInteractivity: "Action Area Only",
	contentAreaBackgroundAlpha: 1,
	contentAreaBackgroundColor: 0xFFFFFF,
	contentAreaStrokeAppearance: "Hidden",
	contentFormat: "Normal",
	contentFrameAlpha: 1,
	contentFrameStrokeAppearance: "Visible",
	contentFrameStrokeColor: 0xDDDDDD,
	contentScale: "Crop to Fit",
	displayMode: "Always Auto",
	feedbackPreloaderAppearance: "Hidden",
	feedbackTimerAppearance: "Hidden",
	iconInactiveAlpha: 0.4,
	iconShadowAlpha: 0,
	keyboardControl: "Off",
	navAppearance: "Hidden",
	panZoom: "On",
	smoothing: "On",
	toolAppearanceContentArea: "Hidden",
	toolAppearanceNav: "Hidden",
	transitionLength: 2,
	transitionPause: 6,
	transitionStyle: "Cross Fade",
	useExternalInterface: "true",
	xmlFilePath: xmlFilePath,
	xmlFileType: "Default"
}	
var params = {
	menu: "false",
	wmode: "transparent",
	allowfullscreen: "true"
};
var attributes = {
	id: "homeSlideShow"
};
swfobject.embedSWF("slideshow.swf", "homeslideshow", "956", "279", "9", false, flashvars, params, attributes);