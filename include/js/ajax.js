var http = createRequestObject();
var objectId = '';

function createRequestObject(htmlObjectId){
    var obj;
    var browser = navigator.appName;
	
    objectId = htmlObjectId;
    
	if(browser == "Microsoft Internet Explorer"){
        obj = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else{
        obj = new XMLHttpRequest();
    }
    return obj;    
}

function sendReq(serverFileName, variableNames, variableValues){
	var paramString = '';
	
	variableNames = variableNames.split(',');
	variableValues = variableValues.split(',');
	
	for(i=0; i<variableNames.length; i++) {
		paramString += variableNames[i]+'='+variableValues[i]+'&';
	}
	paramString = paramString.substring(0, (paramString.length-1));
			
	if (paramString.length == 0){
	   	http.open('get', serverFileName);
	}
	else {
		http.open('get', serverFileName+'?'+paramString);
	}
    http.onreadystatechange = handleResponse;
    http.send(null);
}

function handleResponse(){
	if(http.readyState == 4){
		responseText = http.responseText;
		responseText = unescape(responseText).replace(/\+/g," ");
		selectObj = document.getElementById(objectId);
		if(responseText != ''){
			lotOptions = responseText.split('|');
			if(isIE&&!isOP){
				var doc = selectObj.ownerDocument;
				if(!doc)
					doc = selectObj.document;
				j = 0;
				for(var i=0; i<lotOptions.length; i+=2){
					var opt   = doc.createElement('OPTION');
					opt.text  = lotOptions[i+1];
					opt.value = lotOptions[i];
					selectObj.options.add(opt,j++);
				}	
			}
			else{
				j = 0;
				for(var i=0; i<lotOptions.length; i+=2){
					selectObj.options[j++] = new Option(lotOptions[i+1],lotOptions[i]);
				}	
			}
		}
		else{
			selectObj.options.length = 0;
		}
    }
}