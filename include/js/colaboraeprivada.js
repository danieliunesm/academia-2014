function clearInitialValue(obj,txt){
	obj.style.background = '#fff url(images/layout/blank.gif) no-repeat center';
}

function restoreInitialValue(obj,txt){
	if(obj.value == '')obj.style.background = '#fff url(images/layout/'+txt+'.gif) no-repeat center';
}

function checkKey(evt){
	key = window.event ? event.keyCode : evt.which;
	if(key == 13){
		if(document.loginForm.login.value != '' || document.loginForm.senha.value != '') submitLoginForm();
	}
}

$(document).ready(function () {
	$("ul#topnav2 li").hover(function(){
		$(this).css({ 'background' : '#cccccc', 'border' : '1px solid #ccc'});
		$(this).find("span").show();
	} , function() {
		$(this).css({ 'background' : 'none', 'border' : 'none'});
		$(this).find("span").hide();
	});
	$('a').focus(
		function() {
			$(this).blur();
		}
	);
	document.onkeydown = checkKey;
	try{
		document.mailForm.subject.focus();
	}
	catch(e){}
	try{
		document.cadastroForm.nome.focus();
	}
	catch(e){}
});