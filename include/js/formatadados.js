function Exclui_inval(strCampo,tam) {
	nTamanho = strCampo.length;
	szCampo = "";
	j=0;
	for (var i = nTamanho-1;i>=0;i--)
	{
		if (!isNaN(strCampo.charAt(i)))	{
			szCampo = strCampo.charAt(i) + szCampo;
			j++;
			if (j > tam) break;
		}
	}
	if(tam != 999){
		if (szCampo.length < tam) {
			for (var i = szCampo.length;i<tam;i++)
			{
				szCampo = "0" + szCampo;
			}
		}
	}
    return szCampo;
}

function Formata_Dados(fld, Separador_1, Separador_2, e, v_tipo_dado) {
    var v_tam = 0;
	var sep = 0;
	var key = "";
	var i = j = 0;
	var len = len2 = 0;
	var carac_validos = "0123456789";
	var aux = aux2 = "";
	var whichCode = (window.Event) ? e.which : e.keyCode;
	if (whichCode == 13) return true;
	if (whichCode == 8) return true;
	key = String.fromCharCode(whichCode);  // Valor para o c�digo da Chave
	if (carac_validos.indexOf(key) == -1) return false;  // Chave inv�lida
	len = fld.value.length;
	for(i = 0; i < len; i++)
	if (fld.value.charAt(i) != Separador_2) break;
	if (v_tipo_dado == "COMPET") {
	   v_tam = 6
	}
	if (v_tipo_dado == "DATA") {
	   v_tam = 9;
    }
	if (v_tipo_dado == "MOEDA") {
       v_tam = 17;//13
    }
	if (v_tipo_dado == "CPF") {
       v_tam = 13;
    }
	if (v_tipo_dado == "CNPJ") {
       v_tam = 17;
    }
	if (v_tipo_dado == "CEI") {
       v_tam = 15;
	}
	if (v_tipo_dado == "CEP") {
       v_tam = 8;
	}
	if (v_tipo_dado == "DDD") {
       v_tam = 2;
	}
	if (v_tipo_dado == "TEL") {
       v_tam = 7;
	}
	
	if (len>v_tam) {
	 return
    }

    aux = "";
	for(; i < v_tam; i++)
   	if (carac_validos.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
	aux += key;
	len = aux.length;
	if (len == 0) fld.value = "";
    if (len == 1) fld.value = "" + "" + aux;
	if (len == 2) fld.value = "" + aux;
	if ( ( v_tipo_dado == "MOEDA" ) && ( fld.value.length < 2 ) ) {
		fld.value = ("0,0"+fld.value);
	}else if ( ( v_tipo_dado == "MOEDA" ) && ( fld.value.length < 3 ) ){
		fld.value = ("0,"+fld.value);	
	}
	if (len > 2)  {
		aux2 = "";
    	for (j = 0, i = len - 3; i >= 0; i--) {
             if ((v_tipo_dado == "COMPET") || (v_tipo_dado == "DATA")) {
                    if (j == 2) {
	                     aux2 += Separador_1;
		                 j = 0;
			        }
			 }
			 if ((v_tipo_dado == "MOEDA") || (v_tipo_dado == "CPF")) {
			           if (j == 3) {
	       	              aux2 += Separador_1;
		                  j = 0;
			           }
			 }
			 if (v_tipo_dado == "CNPJ") {
			           if (j == 4) {
	       	              aux2 += Separador_1;
	       	           }
			           if ((j == 7) || (j == 10)) {
	       	              aux2 += ".";
		               }
			 }
			
			 if  (v_tipo_dado == "CEI") {
			           if (j == 3) {
	       	              aux2 += Separador_1;
		                  j = 0;
			           }

			           if (j == 4) {
	       	              aux2 += Separador_1;
	       	           }
			           if ((j == 7) || (j == 10)) {
	       	              aux2 += ".";
		               }
			 }
			 if  (v_tipo_dado == "CEP") {
			           if (j == 3) {
	       	              aux2 += Separador_1;
			           }
			           if (j == 5) {
	       	              aux2 += Separador_2;
	       	           }
			 }
			 aux2 += aux.charAt(i);
			 j++;
        }
		fld.value = "";
		len2 = aux2.length;
		for (i = len2 - 1; i >= 0; i--) fld.value += aux2.charAt(i);
        fld.value += Separador_2 + aux.substr(len - 2, len);

		if ( ( v_tipo_dado == "MOEDA" ) && ( fld.value.length > 4 ) && ( fld.value.substr(0,1) == 0 ) ) {
			fld.value = fld.value.replace("0","");
		}
		
	}
	return false;
}