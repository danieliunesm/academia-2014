var agent, opversion;
agent = navigator.userAgent.toLowerCase();
isOP  = agent.indexOf('opera')!=-1;
if(isOP){
	var reg1 	   = new RegExp("opera.([0-9]+\.[0-9]+)");
	var foundArray = reg1.exec(agent);
	var reg2 	   = new RegExp("([0-9]+\.[0-9]+)");
	if(foundArray != null){
		opversion  = reg2.exec(foundArray[0]);
		version    = opversion[0];
	}
}
else version = 	parseInt(navigator.appVersion);
isOP7	 	 = 	isOP&&version==7;
isOP7UP	 	 = 	isOP&&version>=7;
isNS	 	 = 	(agent.indexOf('mozilla')!=-1) && 
				(agent.indexOf('spoofer')==-1) && 
				(agent.indexOf('compatible') == -1) && 
				(agent.indexOf('opera')==-1) && 
				(agent.indexOf('webtv')==-1) && 
				(agent.indexOf('hotjava')==-1);
isNS4	 	 = 	isNS&&version==4;
isNS60  	 = 	isNS&&version==5&&(parseInt(navigator.productSub)<20010726);
isNS6	 	 = 	isNS&&version>=5&&!isNS60;
isNS7		 = 	isNS6&&parseInt(navigator.vendorSub)==7;
isNS7UP		 = 	isNS6&&parseInt(navigator.vendorSub)>=7;
isFF		 =  agent.indexOf('firefox')!=-1;
isFF5UP		 =	isFF&&version>=5;  
isIE	 	 = 	agent.indexOf('msie')!=-1;
isIE4	 	 = 	isIE&&(agent.indexOf('msie 4')!=-1);
isIE5	 	 = 	isIE&&(agent.indexOf('msie 5')!=-1);
isIE55	 	 = 	isIE&&(agent.indexOf('msie 5.5')!=-1);
isIE6	 	 = 	isIE&&(agent.indexOf('msie 6.0')!=-1);
isIE7	 	 = 	isIE&&(agent.indexOf('msie 7')!=-1);
isMac	 	 = 	agent.indexOf('mac')!=-1;
isDOM		 = 	document.getElementById?1:0;
isDyn		 = 	isDOM||isIE||isNS4;
isMOZ		 =  geckoGetRv()>=1.0001&&navigator.productSub>=20040910;

function geckoGetRv(){
	if(navigator.product != 'Gecko') return -1;
	var rvValue = 0;
	var ua = navigator.userAgent.toLowerCase();
	var rvStart = ua.indexOf('rv:');
	var rvEnd = ua.indexOf(')', rvStart);
	var rv = ua.substring(rvStart+3, rvEnd);
	var rvParts = rv.split('.');
	var exp = 1;
	for(var i = 0; i < rvParts.length; i++){
		var val = parseInt(rvParts[i]);
		rvValue += val / exp;
		exp *= 100;
	}
	return rvValue;
}

var suportedbrowsers = ((isIE5||isIE55||isIE6||isIE7)&&!isOP)||isFF5UP||isMOZ||isNS7UP;
//if(!suportedbrowsers)document.location.replace('nosuportedbrowser.html');