Trim = function(TRIM_VALUE){
	if(TRIM_VALUE.length < 1)return "";
	TRIM_VALUE = RTrim(TRIM_VALUE);
	TRIM_VALUE = LTrim(TRIM_VALUE);
	if(TRIM_VALUE == "")return "";
	else return TRIM_VALUE;
}

RTrim = function(VALUE){
	var w_space = String.fromCharCode(32);
	var v_length = VALUE.length;
	var strTemp = "";
	if(v_length < 0) return"";
	var iTemp = v_length -1;
	while(iTemp > -1){
		if(VALUE.charAt(iTemp) != w_space){
			strTemp = VALUE.substring(0,iTemp +1);
			break;
		}
		iTemp = iTemp-1;
	}
	return strTemp;
}

LTrim = function(VALUE){
	var w_space = String.fromCharCode(32);
	if(v_length < 1) return"";
	var v_length = VALUE.length;
	var strTemp = "";
	var iTemp = 0;
	while(iTemp < v_length){
		if(VALUE.charAt(iTemp) != w_space){
			strTemp = VALUE.substring(iTemp,v_length);
			break;
		}
		iTemp = iTemp + 1;
	}
	return strTemp;
}

validaEmail = function(emailStr){
	var emailPat = /^(.+)@(.+)$/;
	var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars = "\[^\\s" + specialChars + "\]";
	var quotedUser = "(\"[^\"]*\")";
	var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom = validChars + '+';
	var word = "(" + atom + "|" + quotedUser + ")";
	var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat = new RegExp("^" + atom + "(\\." + atom +")*$")	;
	var matchArray = emailStr.match(emailPat);
	if(matchArray == null)return false;
	var user = matchArray[1];
	var domain = matchArray[2];
	if(user.match(userPat) == null)return false;
	var IPArray = domain.match(ipDomainPat);
	if(IPArray != null){
		for(var i = 1; i <= 4; i++){
			if(IPArray[i] > 255)return false;
		}
		return true;
	}	
	var domainArray = domain.match(domainPat);
	if(domainArray == null)return false;
	var atomPat = new RegExp(atom,"g");
	var domArr = domain.match(atomPat);
	var len = domArr.length;
	if(domArr[domArr.length-1].length < 2 || domArr[domArr.length-1].length > 4)return false;
	if(len < 2)return false;
	return true;
}

submitFaleconoscoForm = function(){
	var msg 	 = "";
	var objfocus = null;
	var f 		 = document.getElementById('faleconoscoForm');
	if(Trim(f.nome.value) == ""){
		msg+="O campo Nome precisa ser preenchido!       \n";
		objfocus = f.nome;
	}
	if(Trim(f.email.value) == ""){
		msg+="O campo E.mail precisa ser preenchido!       \n";
		if(objfocus == null) objfocus = f.email;
	}
	else{
		if(!validaEmail(f.email.value)){
			msg+="O e.mail fornecido n�o � v�lido! Verifique os dados digitados.       \n";
			if(objfocus == null) objfocus = f.email;
		}
	}
	if(Trim(f.subject.value) == ""){
		msg+="O campo Assunto precisa ser preenchido!       \n";
		if(objfocus == null) objfocus = f.subject;
	}
	if(Trim(f.message.value) == ""){
		msg+="N�o h� conte�do em Mensagem!       \n";
		if(objfocus == null) objfocus = f.message;
	}
	if(msg != ""){
		alert(msg);
		if(objfocus != null)objfocus.focus();
		return false;
	}
	else{
		f.submit();
	}
}

submitLoginForm = function(){
	var msg 	 = "";
	var objfocus = null;
	var f 		 = document.loginForm;
	if(Trim(f.login.value) == ""){
		msg += "O campo login precisa ser digitado!       \n";
		objfocus = f.login;
	}
	if(Trim(f.senha.value) == ""){
		msg += "O campo senha precisa ser digitado!       \n";
		if(objfocus == null) objfocus = f.senha;
	}
	if(msg != ""){
		alert(msg);
		if(objfocus != null)objfocus.focus();
		return false;
	}
	else f.submit();
}
