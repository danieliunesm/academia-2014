<?
function getServerDate(){
	$mountharray = array(	"01" => "janeiro",
							"02" => "fevereiro",
							"03" => "mar�o",
							"04" => "abril",
							"05" => "maio",
							"06" => "junho",
							"07" => "julho",
							"08" => "agosto",
							"09" => "setembro",
							"10" => "outubro",
							"11" => "novembro",
							"12" => "dezembro"
						);
	return date("d") . " de " . $mountharray[date("m")] . " de " . date("Y");
}

function FmtData($oDate){
	if($oDate != null) return strftime("%d/%m/%Y", strtotime($oDate));
	else return null;
}

function FmtDataTempo($oDate){
	if($oDate != null) return strftime("%d/%m/%Y %H:%M:%S", strtotime($oDate));
	else return null;
}

function obterCodigoEmpresaDominio(){

    $dominio = $_SERVER['HTTP_HOST'];
    $sql = "SELECT CD_EMPRESA FROM col_empresa WHERE TX_DOMINIO = '$dominio' OR CONCAT('www.', TX_DOMINIO) = '$dominio'";

    $RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
    $oRs = mysql_fetch_array($RS_query);

    if ($oRs) {
        return $oRs["CD_EMPRESA"];
    }

    return 5;

}
?>
