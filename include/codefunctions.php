<?
function writetopcode(){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<link rel="stylesheet" type="text/css" media="all" href="/include/css/colaborae.css">
<link rel="stylesheet" type="text/css" media="print" href="/include/css/pcolaborae.css">
<link rel="stylesheet" type="text/css" media="screen" href="/include/css/caroussel.css">
<script type="text/javascript" src="/include/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="/include/js/formfunctions.js"></script>
<script type="text/javascript" src="/include/js/formatadados.js"></script>
<script type="text/javascript" src="/include/js/colaborae.js"></script>
<?
}

function writeheadercode($opt=1){
?>
	<div id="logo"><a href="/index.php"><img src="images/layout/logo.png"></a></div>
	<div id="plogo"><img src="images/layout/plogo.png"></div>
	<div id="loginDiv">
		<form name="loginForm" id="loginForm" action="login.php" method="post">
		<ul>
			<li style="padding:2px 0 0 2px"><a href='<?php ($opt == 1) ? "esquecimeusdados.php" : "esquecimeusdadosprograma.php"; ?>'>Esqueci meus dados</a></li>
			<li><img src="images/layout/separador.gif"></li>
			<li><a href="javascript:void(submitLoginForm())" onmouseover="" onmouseout=""><img src="images/layout/btOK.png"></a></li>
			<li><input type="password" name="senha" id="senha" maxlength="8" tabIndex="2" onfocus="clearInitialValue(this,'senha')" onblur="restoreInitialValue(this,'senha')"></li>
			<li><input type="text" name="login" id="login" maxlength="50" tabIndex="1" onfocus="clearInitialValue(this,'login')" onblur="restoreInitialValue(this,'login')"></li>
			<li><?php writeloggedcode(); ?></li>
		</ul>
		</form>
	</div>
	<p class="clearboth"></p>
	<div id="dataDiv"><?php echo getServerDate(); ?></div>
<?php
}

function writeloggedcode(){
	if (empty($_SESSION["nome"]))
		session_start();
	
	$user = isset($_SESSION["nome"])?$_SESSION["nome"]:'';
	if($user){
		if(!empty($_SESSION["usuario"]) && $_SESSION["usuario"]){
			// echo '<p><strong>'.$user.'</strong> est� logado.&nbsp;-&nbsp;&nbsp;<a href="logout.php" title="   logout   ">Logout </a> &nbsp;&nbsp;&nbsp;</p>';
		}
		else{
			echo '<p><span><strong>'.$user.'&nbsp;&nbsp;&nbsp;</strong></span></p>';
			unset($_SESSION['usuario']);
			unset($_SESSION['nome']);
			unset($_SESSION['empresaID']);
			unset($_SESSION['email']);
		}
	}
}

function writemenucode(){
	echo '<div id="menu"><ul id="topnav">';
	$sql1 = 'SELECT ID, Titulo, SubmenuGroup, Link, TipoLink FROM tb_menuitens WHERE Status = 1 AND MenuGroup = 1 ORDER BY ItemOrder';
	$query1 = mysql_query($sql1) or die(ERROR_MSG_SQLQUERY . mysql_error());
	while($oRs1 = mysql_fetch_row($query1)){
		if($oRs1[4] != 0){
			if($oRs1[3] != ''){
				$target = is_integer(strpos($oRs1[3],'http://'))?' target="_blank"':'';
				echo '<li><a href="' . $oRs1[3] . '"' . $target . '>' . $oRs1[1] . '</a>';
			}else
				echo '<li><a href="/paginamenu.php?id=' . $oRs1[0] . '">' . $oRs1[1] . '</a>';
		}
		else
			echo '<li><a href="#">' . $oRs1[1] . '</a>';
		if($oRs1[2] != 0){
			$sql2 = 'SELECT ID, Titulo, Link FROM tb_menuitens WHERE Status = 1 AND MenuGroup = '.$oRs1[2].' ORDER BY ItemOrder';
			$query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());
			echo '<span>';
			while($oRs2 = mysql_fetch_row($query2)){
				if($oRs2[2] != ''){
					$target = is_integer(strpos($oRs2[2],'http://'))?' target="_blank"':'';
					echo '<a href="' . $oRs2[2] . '"' . $target . '>' . $oRs2[1] . '</a>';
				}else
					echo '<a href="/paginamenu.php?id=' . $oRs2[0] . '">' . $oRs2[1] . '</a>';
			}
			echo '</span>';
			mysql_free_result($query2);
		}
		echo '</li>';
	}
	mysql_free_result($query1);
	
	echo '</ul>';
	echo '</div>';
	echo '<p class="clearboth"></p>';
}

function writenavcode($oNav){
	if($oNav == 1){
?>
			<div id="navtop">
				<a href="javascript:history.back()" title="   p�gina anterior  "><img src="images/layout/btback.png" /></a>
				<a href="#" onclick="if(window.print())window.print();return false;return false" title="   imprimir p�gina   "><img src="images/layout/btprint.png" /></a>
				<a href="index.php"><img src="images/layout/bthome.png" /></a>
			</div>
<?php
	}
	if($oNav == 2){
?>
		<div id="navbottom">
			<a href="#" onclick="self.scrollTo(0,0)" title="   voltar ao topo da p�gina   "><img src="images/layout/bttop.png" /></a>
			<a href="javascript:history.back()" title="   p�gina anterior  "><img src="images/layout/btback2.png" /></a>
		</div>
<?
	}
}

function writebottomcode(){
?>
	<div id="rodape">
		<ul>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_facebook.png" />Facebook</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_twitter.png" />Twitter</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_linkedin.png" />LinkedIn</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_podcast2.png" />Podcasts</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_newsletter.png" />Newsletters</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_youtube.png" />YouTube</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_rss.png" />RSS</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_blogs.png" />Blogs</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_movies.png" />V�deos</a> --></li>
		</ul>
<?php writenavcode(2);?>
	</div>
	<div id="copyright">
		<ul>
		<!-- <li><a href="#">Mapa do Site</a>|</li> -->
		<li><a href="/paginaindividual.php?id=161">Pol�tica de Privacidade</a>|</li>
		<li><a href="/paginaindividual.php?id=162">Termos de Uso</a>|</li>
		</ul>
		<span>&copy; Colabor&aelig; <?php echo date("Y");?></span>
	</div>
<?php
}
?>