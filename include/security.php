<?
session_start();

session_set_cookie_params(86400);
ini_set('session.gc_maxlifetime', 86400);

if(empty($_SESSION["usuario"])){
	
	if ($_SERVER["QUERY_STRING"] != null && $_SESSION["urlOrigin"] == null){
//		$_SESSION["urlOrigin"] = $_SERVER["REQUEST_URI"];
		$_SESSION["urlOrigin"] = $_SERVER["QUERY_STRING"];
	}
	header('Location: http://'.$_SERVER["SERVER_NAME"].'/index.php');
//	header('Location: /paginamenu.php?id=2');
	exit();
}
?>