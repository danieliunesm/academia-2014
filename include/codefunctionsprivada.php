<?
function writetopcode(){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<link rel="stylesheet" type="text/css" media="all" href="/include/css/colaborae.css">
<link rel="stylesheet" type="text/css" media="print" href="/include/css/pcolaborae.css">
<link rel="stylesheet" type="text/css" media="screen" href="/include/css/caroussel.css">

<style type="text/css">

    html, body {height: 100%;}

    ul#topnav li:hover{background: #ccc;}

</style>

<script type="text/javascript" src="/include/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="/include/js/formfunctions.js"></script>
<script type="text/javascript" src="/include/js/formatadados.js"></script>

<link rel="stylesheet" type="text/css" href="include/js/chromemenu/chrometheme/chromestyle.css" />

<script type="text/javascript" src="include/js/chromemenu/chromejs/chrome.js" />

<script type="text/javascript">

    function closeMenu(){

        var atual = document.getElementById("tdMenu").style.display;
        document.getElementById("tdMenu").style.display = (atual=="none"? "block" : "none");
        document.getElementById("tdMenu").style.width = "190px";

        var idImagem = (atual=="none"? "" : "2");
        document.getElementById("imgOpenClose").src = "images/layout/btOpenClose" + idImagem + ".gif";

    }

    function redirecionaHistorico(obj, sim){

        if (sim == 1)
            window.location = 'listasimulado.php?id=' + obj.value;
        else
            window.location = 'paginaindividualprivada.php?id=' + obj.value;
    }

    function abrirSimulado(id){

        document.getElementById("divConteudo").style.display = 'none';
        document.getElementById("iprova").style.display = 'block';
        document.getElementById("iprova").src = "canal/certificacao/prova.php?all=1&header=0&id=" + id;

    }

    function resizeFrameProva(){

        var tamanho=500;

        try

        {

            var f = document.getElementById('iprova');

            if ( f!=null && f.contentWindow!=null && f.contentWindow.document!=null)

                var b = f.contentWindow.document.body;
            b = f.contentWindow.document.getElementById("globalpadding");


            if ( b!=null ){

                if (navigator.userAgent.indexOf('Firefox') != -1){

                    tamanho = (b.parentNode.offsetHeight+10);

                }else if (navigator.userAgent.indexOf('Chrome') != -1){

                    tamanho = (b.parentNode.scrollHeight+30);

                }else{

                    tamanho = (b.scrollHeight+10);

                }

            }

        }

        catch(e){}

        //tamanho = tamanho;

        $("#iprova").css("height", tamanho+'px');

        parent.parent.resizeFrameSimulado();
        parent.parent.resizeFrameConteudo();
        //window.location = "#";
    }


</script>

<script type="text/javascript" src="/include/js/colaboraeprivada.js"></script>
<?
}

function writeheadercode($id, $cdUsuario, $cdEmpresa, $simulado = 0){

    $sql = "SELECT a.ID_PAGINA_INDIVIDUAL, p.TITULO FROM col_acesso_pagina a
                    INNER JOIN tb_paginasindividuais p ON p.ID = ID_PAGINA_INDIVIDUAL
                    INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = p.PastaID
            WHERE a.CD_USUARIO = $cdUsuario AND ce.CD_EMPRESA = $cdEmpresa ORDER BY DT_ACESSO_PAGINA DESC LIMIT 0,10";

    $resultado = mysql_query($sql);


?>
	<div style="float: right; position: relative; padding-top: 6px;">

        <select style="color: navy; background: transparent; width: 170px;" onchange="javascript:redirecionaHistorico(this, <?php echo $simulado; ?>);">
            <option>Acessos recentes</option>

            <?
            while($linha = mysql_fetch_row($resultado)){

                echo "<option value=\"{$linha[0]}\">{$linha[1]}</option>";

            }

            ?>

        </select>

	</div>

<?php

    $sql = "INSERT INTO col_acesso_pagina (DT_ACESSO_PAGINA, ID_PAGINA_INDIVIDUAL, CD_USUARIO) VALUES
                                            (sysdate(), $id, $cdUsuario)";
    mysql_query($sql);


}

function writeloggedcode(){
	$user = isset($_SESSION["nome"])?$_SESSION["nome"]:'';
	if($user){
		if(isValidUser()){
			echo '<p><strong>'.$user.'</strong> est� logado.&nbsp;-&nbsp;&nbsp;<a href="logout.php" title="   logout   ">Logout</a></p>';
		}
		else{
			echo '<p><span><strong>'.$user.'</strong></span></p>';
			unset($_SESSION['usuario']);
			unset($_SESSION['nome']);
			unset($_SESSION['empresaID']);
			unset($_SESSION['email']);
		}
	}
}

function writemenucode($pasta, $nomePagina="paginaindividualprivada"){

    $sql1 = "SELECT
                pas.ID,
                pas.Titulo,
                pas.CD_AREA_CONHECIMENTO
            FROM
                tb_pastasindividuais pas
            INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pas.ID
            INNER JOIN col_prova p ON p.ID_PASTA_INDIVIDUAL = pas.ID
            AND p.IN_PROVA = 1
            INNER JOIN col_assessment a ON a.CD_EMPRESA = ce.CD_EMPRESA
            AND a.CD_PROVA = p.CD_PROVA
            WHERE
                pas. STATUS = 1
            AND ce.CD_EMPRESA = {$_SESSION["empresaID"]}
            AND(
                EXISTS(
                    SELECT
                        1
                    FROM
                        col_prova_aplicada pa
                    WHERE
                        pa.CD_PROVA = p.CD_PROVA
                    AND pa.CD_USUARIO = {$_SESSION['cd_usu']}
                AND pa.DT_TERMINO IS NOT NULL
                )
            OR {$_SESSION["tipo"]} != 1
            )
            ORDER BY
                pas.NR_ORDEM,
                pas.Titulo
            ";


    $cursos = array();
    $areasConhecimento = "0";

    $query1 = mysql_query($sql1) or die(ERROR_MSG_SQLQUERY . mysql_error());
    while($oRs1 = mysql_fetch_row($query1)){

        $areasConhecimento = "$areasConhecimento," . $oRs1[2];

        if(!is_array($cursos[$oRs1[2]])){
            $cursos[$oRs1[2]] = array();
        }

        $cursos[$oRs1[2]][] = $oRs1;

    }

    $sqlArea = "SELECT CD_AREA_CONHECIMENTO, NM_AREA_CONHECIMENTO FROM col_area_conhecimento WHERE CD_AREA_CONHECIMENTO IN ($areasConhecimento)";
    $query2 = mysql_query($sqlArea) or die(ERROR_MSG_SQLQUERY . mysql_error());

    echo '<div class="chromestyle" id="chromemenu"><ul>';

    $divsAuxiliares = "";

    while($oRs2 = mysql_fetch_row($query2)){

        echo "<li><a href='#' rel='dropmenu{$oRs2[0]}'>{$oRs2[1]}</a></li>";

        $divsAuxiliares = "$divsAuxiliares <div id='dropmenu{$oRs2[0]}' class='dropmenudiv'>";

        foreach ($cursos[$oRs2[0]] as $curso) {

            $divsAuxiliares = $divsAuxiliares . ' <a href="' . $nomePagina . '.php?pasta=' . $curso[0] . '">' . $curso[1] . '</a>';

        }

        $divsAuxiliares = "$divsAuxiliares </div>";

    }

    echo "</ul></div>";

    echo $divsAuxiliares;

    echo '<p class="clearboth"></p>';

    mysql_free_result($query1);

    ?>


<script type="text/javascript">

    cssdropdown.startchrome("chromemenu")

</script>

    <?php
}

function writemenucodeOld($pasta, $nomePagina="paginaindividualprivada"){
	echo '<div id="menu" style="width: 760px; float: left;"><ul id="topnav" style="width: 760px;">';

    $sql1 = "SELECT
                pas.ID,
                pas.Titulo
            FROM
                tb_pastasindividuais pas
            INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pas.ID
            INNER JOIN col_prova p ON p.ID_PASTA_INDIVIDUAL = pas.ID
            AND p.IN_PROVA = 1
            INNER JOIN col_assessment a ON a.CD_EMPRESA = ce.CD_EMPRESA
            AND a.CD_PROVA = p.CD_PROVA
            WHERE
                pas. STATUS = 1
            AND ce.CD_EMPRESA = {$_SESSION["empresaID"]}
            AND(
                EXISTS(
                    SELECT
                        1
                    FROM
                        col_prova_aplicada pa
                    WHERE
                        pa.CD_PROVA = p.CD_PROVA
                    AND pa.CD_USUARIO = {$_SESSION['cd_usu']}
                AND pa.DT_TERMINO IS NOT NULL
                )
            OR {$_SESSION["tipo"]} != 1
            )
            ORDER BY
                pas.NR_ORDEM,
                pas.Titulo
            ";


	$query1 = mysql_query($sql1) or die(ERROR_MSG_SQLQUERY . mysql_error());
	while($oRs1 = mysql_fetch_row($query1)){

        $relevo = "";
        if ($pasta == $oRs1[0]) $relevo = "background-color: #aaa;";

		echo '<li><a style="color: #000; '. $relevo .'" href="' . $nomePagina . '.php?pasta=' . $oRs1[0] . '">' . $oRs1[1] . '</a>';

		echo '</li>';
	}
	mysql_free_result($query1);
	
	echo '</ul>';
	echo '</div>';
	echo '<p class="clearboth"></p>';
}

function retornaLinhaAdjacente($idPasta, $ordem, $ordemCapitulo, $anterior=false){

    $multiplicador = 100000;
    if ($ordemCapitulo == "") $ordemCapitulo = 0;

    $sinal = ">";
    $ordenacao = "ASC";
    if ($anterior){
        $sinal = "<";
        $ordenacao = "DESC";

    }

    $sql = "SELECT
                pag.ID
            FROM
                tb_paginasindividuais pag
            LEFT JOIN col_capitulo cap ON cap.CD_CAPITULO = pag.CD_CAPITULO
            WHERE
                pag.PastaID = $idPasta
            AND pag. STATUS = 1
            AND ((COALESCE(cap.NR_ORDEM_CAPITULO,0) * $multiplicador) + pag.NR_ORDEM) $sinal (($ordemCapitulo * $multiplicador) + $ordem)
            ORDER BY
                cap.NR_ORDEM_CAPITULO $ordenacao,
                pag.NR_ORDEM $ordenacao
            LIMIT 0,1";
    //echo $sql;
    $query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
    $linha = mysql_fetch_array($query);
    return $linha[0];
    mysql_free_result($query);

}

function writenavcode($oNav){
    $id = isset($_GET["id"])?$_GET["id"]:0;
    $sql = "SELECT pag.ID, pag.NR_ORDEM, pag.PastaID, cap.NR_ORDEM_CAPITULO FROM tb_paginasindividuais pag LEFT JOIN col_capitulo cap ON cap.CD_CAPITULO = pag.CD_CAPITULO WHERE pag.ID = $id;";
    $query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
    $linha = mysql_fetch_array($query);
    $pasta = $linha["PastaID"];
    $ordem = $linha["NR_ORDEM"];
    $ordemCapitulo = $linha["NR_ORDEM_CAPITULO"];
    mysql_free_result($query);
    $menu = 0;
    if(isset($_GET["menu"])) $menu = $_GET["menu"];

    if(is_numeric($ordem) && $menu == 1){

        $idProximo = retornaLinhaAdjacente($pasta, $ordem, $ordemCapitulo);
        $idAnterior  = retornaLinhaAdjacente($pasta, $ordem, $ordemCapitulo, true);

    }

	if($oNav == 1){

?>
			<div id="navtop">
                <? if ($idProximo != "" && $menu == 1){ ?>
                <a href="paginaindividualprivada.php?id=<?echo "$idProximo&menu=1" ?>" title="   p�gina posterior  "><img src="images/layout/btfoward.png" /></a>
                <?}
                if ($idAnterior != "" && $menu == 1){ ?>
                <a href="paginaindividualprivada.php?id=<?echo "$idAnterior&menu=1" ?>" title="   p�gina anterior  "><img src="images/layout/btback.png" /></a>
                <?} elseif($menu == 0){ ?>
                    <a href="javascript:history.back();" title="   p�gina anterior  "><img src="images/layout/btback.png" /></a>
                <?} ?>
				<a href="#" onclick="if(window.print())window.print();return false;return false" title="   imprimir p�gina   "><img src="images/layout/btprint.png" /></a>
			</div>
<?php
	}
	if($oNav == 2){
?>
		<div id="navbottom">
             <? if ($idProximo != "" && $menu == 1){ ?>
            <a href="paginaindividualprivada.php?id=<?echo "$idProximo&menu=1" ?>" title="   p�gina posterior  "><img src="images/layout/btfoward2.png" /></a>
            <?}
            if ($idAnterior != "" && $menu == 1){ ?>
            <a href="paginaindividualprivada.php?id=<?echo "$idAnterior&menu=1" ?>" title="   p�gina anterior  "><img src="images/layout/btback2.png" /></a>
            <?} elseif($menu == 0){ ?>
                <a href="javascript:history.back();" title="   p�gina anterior  "><img src="images/layout/btback2.png" /></a>
            <?} ?>
		</div>
<?
	}
}

function writebottomcode($navigation=true){
?>
	<div id="rodape" style="border-radius: 3px 3px 0 0">
		<ul>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_facebook.png" />Facebook</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_twitter.png" />Twitter</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_linkedin.png" />LinkedIn</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_podcast2.png" />Podcasts</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_newsletter.png" />Newsletters</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_youtube.png" />YouTube</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_rss.png" />RSS</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_blogs.png" />Blogs</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_movies.png" />V�deos</a> --></li>
		</ul>
<?php if($navigation) writenavcode(2);?>
	</div>
	<div id="copyright">

		<span>&copy; Colabor&aelig; <?php echo date("Y");?></span>
	</div>
<?php
}

function getSqlSimulado($tipo, $empresaID, $usuario, $idPaginaIndividual, $idPasta = "", $idCapitulo = ""){

    $sqlWhere = " AND c.ID_PAGINA_INDIVIDUAL = $idPaginaIndividual ";

    if($idCapitulo != ""){
        $sqlWhere = " AND c.CD_CAPITULO = $idCapitulo ";
    }

    if($idPasta != ""){
        $sqlWhere = " AND c.ID_PASTA_INDIVIDUAL = $idPasta ";

    }

    $sql =          "SELECT
                        DT_INICIO_REALIZACAO,
                        DT_TERMINO_REALIZACAO,
                        HR_INICIO_REALIZACAO,
                        HR_TERMINO_REALIZACAO,
                        DURACAO_PROVA,
                        c.CD_PROVA,
                        DS_PROVA,
                        IN_SENHA_MESTRA,
                        CD_CICLO,
                        COUNT(DISTINCT pe.CD_PERGUNTAS) AS QTD_PERGUNTAS
                    FROM
                        col_prova c
                    INNER JOIN col_prova_chamada pc ON pc.CD_PROVA = c.CD_PROVA
                    LEFT JOIN col_prova_disciplina pd ON pd.CD_PROVA = c.CD_PROVA
                    LEFT JOIN col_disciplina d ON d.CD_DISCIPLINA = pd.CD_DISCIPLINA
                    LEFT JOIN col_perguntas pe ON pe.CD_DISCIPLINA = pd.CD_DISCIPLINA
                    INNER JOIN col_prova_vinculo pv ON pv.CD_PROVA = c.CD_PROVA
                    LEFT JOIN col_prova_cargo prc ON prc.CD_PROVA = c.CD_PROVA
                    LEFT JOIN col_prova_aplicada pa ON pa.CD_PROVA = c.CD_PROVA AND pa.CD_USUARIO = $usuario
                    WHERE
                    (
                        pv.CD_VINCULO = $empresaID
                    AND IN_TIPO_VINCULO = 1
                    )
                    AND c.IN_ATIVO = 1
                    AND(
                        prc.NM_CARGO_FUNCAO = '{$_SESSION["cargofuncao"]}'
                    OR prc.NM_CARGO_FUNCAO IS NULL
                    OR prc.NM_CARGO_FUNCAO = ''
                    )
                    AND CONCAT(
                        DATE_FORMAT(
                            DT_INICIO_REALIZACAO,
                            '%Y%m%d'
                        ),
                        DATE_FORMAT(
                            HR_INICIO_REALIZACAO,
                            '%H%i'
                        )
                    )<= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
                    AND CONCAT(
                        DATE_FORMAT(
                            DT_TERMINO_REALIZACAO,
                            '%Y%m%d'
                        ),
                        DATE_FORMAT(
                            HR_TERMINO_REALIZACAO,
                            '%H%i'
                        )
                    )>= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
                    $sqlWhere
                    AND(
                        (
                            c.IN_PROVA = 0
                        AND (c.NR_TENTATIVA > pa.NR_TENTATIVA_USUARIO OR pa.NR_TENTATIVA_USUARIO IS NULL)

                        )
                    )
                    GROUP BY
                        DT_INICIO_REALIZACAO,
                        DT_TERMINO_REALIZACAO,
                        HR_INICIO_REALIZACAO,
                        HR_TERMINO_REALIZACAO,
                        DURACAO_PROVA,
                        c.CD_PROVA,
                        DS_PROVA,
                        IN_SENHA_MESTRA,
                        CD_CICLO
                    ORDER BY
                        DS_PROVA,
                        DT_INICIO_REALIZACAO,
                        HR_INICIO_REALIZACAO
                    ";

    return $sql;

    //    ( OR $tipo > 3)
    //AND

}

function getSqlProva($tipo, $empresaID, $usuario, $idPastaIndividual){

    $obrigaDiagnostico = 0;
    $sql = "SELECT IN_OBRIGA_DIAGNOSTICO FROM col_empresa WHERE CD_EMPRESA = $empresaID";
    $RS_query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error() . " Erro!!");
    if($oRs = mysql_fetch_row($RS_query))
    {
        $obrigaDiagnostico = $oRs[0];
    }

    $sqlWhereTipoProva = "((((pa.DT_TERMINO IS NULL AND (DT_INICIO IS NULL OR DATE_ADD(DT_INICIO, INTERVAL c.DURACAO_PROVA *(60) + 5 SECOND) >= sysdate())) OR $tipo > 3
								OR (VL_MEDIA_APROVACAO > VL_MEDIA AND NR_TENTATIVA_USUARIO = 1 AND IN_PRIMEIRA_CHAMADA = 2)
								) AND c.IN_PROVA = 1
								  AND NOT EXISTS (SELECT 1 FROM col_prova_aplicada pa1 INNER JOIN col_prova p1 ON p1.CD_PROVA = pa1.CD_PROVA WHERE pa1.CD_USUARIO = pa.CD_USUARIO AND pa1.CD_PROVA = c.CD_PROVA_SUPERIOR AND COALESCE(pa1.VL_MEDIA,0) >= p1.VL_MEDIA_APROVACAO)
								))";


    /*if ($obrigaDiagnostico == 1 ){ //&& $tipo == 1

        $codigosPAs = obterPAs($empresaID, false);

        if($codigosPAs == ""){
            $codigosPAs = "0";
        }

        $sqlWhereTipoProva = " $sqlWhereTipoProva AND (c.CD_PROVA in ($codigosPAs) ) "; //OR c.CD_CICLO IN ($ciclosSql)
    }*/

        $sql = "SELECT DISTINCT
				DT_INICIO_REALIZACAO, DT_TERMINO_REALIZACAO, HR_INICIO_REALIZACAO, HR_TERMINO_REALIZACAO, DURACAO_PROVA, c.CD_PROVA, DS_PROVA, IN_SENHA_MESTRA, CD_CICLO
				 FROM col_prova c INNER JOIN col_prova_chamada pc ON pc.CD_PROVA = c.CD_PROVA INNER JOIN col_prova_vinculo pv ON pv.CD_PROVA = c.CD_PROVA LEFT JOIN col_prova_cargo prc ON prc.CD_PROVA = c.CD_PROVA
				  LEFT JOIN col_prova_aplicada pa on pa.CD_PROVA = c.CD_PROVA AND pa.CD_USUARIO = $usuario
				  WHERE (pv.CD_VINCULO = $empresaID AND IN_TIPO_VINCULO = 1) AND c.IN_ATIVO = 1 AND (prc.NM_CARGO_FUNCAO = '{$_SESSION["cargofuncao"]}' or prc.NM_CARGO_FUNCAO is null or prc.NM_CARGO_FUNCAO = '')
				AND CONCAT(DATE_FORMAT(DT_INICIO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_INICIO_REALIZACAO, '%H%i')) <= DATE_FORMAT(sysdate(), '%Y%m%d%H%i') AND CONCAT(DATE_FORMAT(DT_TERMINO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_TERMINO_REALIZACAO, '%H%i')) >= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
				AND ID_PASTA_INDIVIDUAL = $idPastaIndividual AND (($sqlWhereTipoProva))
				ORDER BY DS_PROVA, IN_PRIMEIRA_CHAMADA, DT_INICIO_REALIZACAO, HR_INICIO_REALIZACAO LIMIT 0,1";

        //$resultado = DaoEngine::getInstance()->executeQuery($sql);
    //echo $sql;
    return $sql;

}

function dataGridProva($usuario, $idPaginaIndividual, $inProva = 0, $idPasta = "", $idCapitulo = ""){

    $tipo = $_SESSION["tipo"];
    $empresaID = $_SESSION["empresaID"];

    if ($inProva == 0)
        $sql = getSqlSimulado($tipo, $empresaID, $usuario, $idPaginaIndividual, $idPasta, $idCapitulo);
    else{
        $sql = getSqlProva(1, $empresaID, $usuario, $idPaginaIndividual);
    }

    $resultado = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

    if(mysql_num_rows($resultado) == 0){

        $sql = getSqlProva(2, $empresaID, $usuario, $idPaginaIndividual);
        //echo $sql;

        $resultado = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());

        if(mysql_num_rows($resultado) == 0){

            return;

        }
    }

    $complementoQuestoes = "";
    $tituloProva = "<TD class=\"title\">T�tulo</TD>";
    $colspan = 6;
    $complementoQuestoes = "";
    $teste = "Teste Final do Curso";

    if($inProva==0){

        $complementoQuestoes = '<TD class="title" align="center">Banco</TD>';
        $colspan = 7;
        $teste = "Simulado";
    }




    $cabecalhoTitulo = '                        <TABLE cellpadding="3" cellspacing="0" border="0" width="722px" >
                <tr class="tarjaItens">
                    <td colspan="' . $colspan . '" class="title" style="text-align:center; border-radius: 3px; font-size: 12px;background-color: #FFCE5A; border: 1px solid #E9B93F;">' . $teste . '</td>

                </tr>
                <TR class="tarjaItens">' .
                    $tituloProva .
                    '<TD class="title" align="center">Quest�es</TD>
                    ' . $complementoQuestoes . '
                    <TD class="title" align="center">Tempo</TD>
                    <TD class="title" align="center">In�cio</TD>
                    <TD class="title" align="center">T�rmino</TD>

                    <TD class="title">&nbsp;</TD>
                </TR>';

                //<TD class="title" align="center">Hora In�cio</TD>
                //<TD class="title" align="center">Hora T�rmino</TD>

                while ($linha = mysql_fetch_array($resultado)){

                    $data = page::formataPropriedade($linha["DT_INICIO_REALIZACAO"], DaoEngine::DATA);
                    $dataFim = page::formataPropriedade($linha["DT_TERMINO_REALIZACAO"], DaoEngine::DATA);
                    $inicio = page::formataPropriedade($linha["HR_INICIO_REALIZACAO"], DaoEngine::DATA_HORA_MINUTO);
                    $fim = page::formataPropriedade($linha["HR_TERMINO_REALIZACAO"], DaoEngine::DATA_HORA_MINUTO);
                    $duracao = $linha["DURACAO_PROVA"];

                    //$sqlQuestoes = "SELECT SUM(NR_QTD_PERGUNTAS_NIVEL_1) + SUM(NR_QTD_PERGUNTAS_NIVEL_2) + SUM(NR_QTD_PERGUNTAS_NIVEL_3) as Total
                    //FROM col_prova_disciplina WHERE CD_PROVA = {$linha["CD_PROVA"]}";

                    if ($inProva == 0){
                        if ($idPasta == ""){
                            if($idCapitulo == ""){
                                $sqlQuestoes = "SELECT
                                            COUNT(1) as Total
                                        FROM
                                            col_prova_disciplina pd
                                            INNER JOIN col_perguntas p ON p.CD_DISCIPLINA = pd.CD_DISCIPLINA
                                        WHERE
                                            p.IN_ATIVO = 1
                                        AND pd.CD_PROVA = {$linha["CD_PROVA"]}";

                            }else{
                                $sqlQuestoes = "SELECT
                                            COUNT(DISTINCT pe.CD_PERGUNTAS) as Total
                                        FROM
                                            tb_paginasindividuais pi
                                            INNER JOIN col_disciplina d ON pi.ID = d.ID_PAGINA_INDIVIDUAL
                                            INNER JOIN col_perguntas pe ON pe.CD_DISCIPLINA = d.CD_DISCIPLINA
                                        WHERE
                                            pi.CD_CAPITULO = $idCapitulo
                                        AND pi.`Status` = 1
                                        AND pe.IN_ATIVO = 1;";

                            }

                        }else{
                            $sqlQuestoes = "SELECT
                                            COUNT(DISTINCT pe.CD_PERGUNTAS) as Total
                                        FROM
                                            tb_paginasindividuais pi
                                            INNER JOIN col_disciplina d ON pi.ID = d.ID_PAGINA_INDIVIDUAL
                                            INNER JOIN col_perguntas pe ON pe.CD_DISCIPLINA = d.CD_DISCIPLINA
                                        WHERE
                                            pi.PastaID = $idPasta
                                        AND pi.`Status` = 1
                                        AND pe.IN_ATIVO = 1;";


                        }
                    }else{
                        $sqlQuestoes = "SELECT
                                            COUNT(DISTINCT pi.ID) as Total
                                        FROM
                                            tb_paginasindividuais pi
                                            INNER JOIN col_disciplina d ON pi.ID = d.ID_PAGINA_INDIVIDUAL
                                            INNER JOIN col_perguntas pe ON pe.CD_DISCIPLINA = d.CD_DISCIPLINA
                                        WHERE
                                            pi.PastaID = $idPaginaIndividual
                                        AND pi.`Status` = 1
                                        AND pe.IN_ATIVO = 1;";

                    }
                    //echo $sqlQuestoes;
                    $resultadoQuestoes = mysql_query($sqlQuestoes);

                    //$quest�es = $linha["QTD_PERGUNTAS"];

                    if ($linhaQuestoes = mysql_fetch_row($resultadoQuestoes))
                    {
                        $quest�es = $linhaQuestoes[0];
                    }

                    //$linkProva = "<a href='javascript:abrirSimulado({$linha["CD_PROVA"]})'>Iniciar</a>";
                    //$hrefProva = "<a href='javascript:abrirSimulado({$linha["CD_PROVA"]})'>{$linha["DS_PROVA"]}</a>";

                    $multi = "";

                    if($idCapitulo != ""){
                        $multi = "&multi=2";
                    }

                    if ($idPasta != ""){
                      $multi = "&multi=1";
                    }

                    $target = "target='prova'";

                    $modoAbertura = "";

                    if($inProva == 1){
                        $target = "target='_top'";
                        $modoAbertura = "&mode=1";
                    }

                    $linkProva = "<a href='canal/certificacao/prova.php?id={$linha["CD_PROVA"]}&all=1$multi$modoAbertura' $target><img height='20' src='../botao_simulado.png'></img></a>";
                    $hrefProva = "<a href='canal/certificacao/prova.php?id={$linha["CD_PROVA"]}&all=1$multi$modoAbertura' $target>{$linha["DS_PROVA"]}</a>";

                    $textoTituloProva = "";

                    //if($inProva != 0){

                    $textoTituloProva = "<TD class='textblk' style='text-align:left;'>$hrefProva</TD>";

                    //}
                    $banco = $quest�es;
                    if($inProva == 0 && ($idPasta != "" || $idCapitulo != "")){
                        if($quest�es > 20) $quest�es = 20;
                    }

                    $complementoQuestoes = "";
                    if($inProva==0){
                        $complementoQuestoes = "<TD class='textblk' style='text-align:center;'>{$banco}</TD>";
                    }

                    if ($banco == 0)
                        return;

                    echo $cabecalhoTitulo;

                    echo "<TR style='height: 20px;'>
        $textoTituloProva
        <TD class='textblk' style='text-align:center;'>{$quest�es}</TD>
        $complementoQuestoes
        <TD class='textblk' style='text-align:center;'>{$duracao} minutos</TD>
        <TD class='textblk' style='text-align:center;'>{$data}</TD>
        <TD class='textblk' style='text-align:center;'>{$dataFim}</TD>
        <TD class='textblk' style='text-align:left;'>$linkProva</TD>
        </TR>";

                    // <TD class='textblk' style='text-align:center;'>{$inicio}</TD>
                    //<TD class='textblk' style='text-align:center;'>{$fim}</TD>

                }


                echo '						</TABLE><br/><br/>';

}


?>