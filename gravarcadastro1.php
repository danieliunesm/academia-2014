<?
//include "../include/security.php";
include "include/defines.php";
include "include/dbconnection.php";
include "include/genericfunctions.php";
?>
<?
function getDocumentRoot()
{
	$fullpath 	 = str_replace("\\","/",strtolower($_SERVER["SCRIPT_FILENAME"]));
	$thispath 	 = '/gravarcadastro1.php';
	$doc_rootpos = strpos(str_replace("\\","/",$fullpath),$thispath);
//	$doc_root	 = '/home/colaborae/public_html'.substr($fullpath,0,$doc_rootpos);
	$doc_root	 = substr($fullpath,0,$doc_rootpos);
	
	return $doc_root;
}

reset($_POST);
/*
$tipo					= 1;
$grupo					= 1;
$permissao				= "";
$nome					= "";
$cd_usuario 			= 0;

$UPLOAD_BASE_URL 		= "/usuarios/";
$UPLOAD_BASE_DIR 		= getDocumentRoot().$UPLOAD_BASE_URL;

$nome					= $_POST["nome"];
$sobrenome				= $_POST["sobrenome"];
$datanasc				= '';//$_POST["datanasc"];
$naturalidade			= '';//$_POST["naturalidade"];
$uf						= '';//$_POST["uf"];
$nacionalidade			= '';//$_POST["nacionalidade"];
$enderecores			= '';//$_POST["enderecores"];
$cepres					= '';//$_POST["cepres"];
$dddtelres				= '';//$_POST["dddtelres"];
$telres					= '';//$_POST["telres"];
$dddcel					= '';//$_POST["dddcel"];
$cel					= '';//$_POST["cel"];
$email					= $_POST["email"];
$cd_empresa				= $_POST["empresa"];
$cd_lotacao				= $_POST["lotacao"];
$cargofuncao			= $_POST["cargofuncao"];
$enderecocom			= $_POST["enderecocom"];
$cepcom					= $_POST["cepcom"];
$dddtelcom				= $_POST["dddtelcom"];
$telcom					= $_POST["telcom"];
$ramal					= $_POST["ramal"];
$experiencia			= '';//$_POST["experiencia"];
$outrosexperiencia		= '';//$_POST["outrosexperiencia"];
$especializacao			= '';//$_POST["especializacao"];
$outrosespecializacao	= '';//$_POST["outrosespecializacao"];
$fotofile				= '';
$cvfile					= '';
$newsletteragreement	= isset($_POST["newsletteragreement"]) ? $_POST["newsletteragreement"] : "0";
$policyagreement		= isset($_POST["policyagreement"]) ? $_POST["policyagreement"] : "0";

//$experiencia			= implode(",", $experiencia);
//$especializacao		= implode(",", $especializacao);
$newsletteragreement	= $newsletteragreement == "on" ? "1" : "0";
$policyagreement		= $policyagreement == "on" ? "1" : "0";

$uploadfotoerror = 0;
$uploadcverror   = 0;
*/

$tipo					= 1;
$grupo					= 1;
$permissao				= "";
$nome					= "";
$cd_usuario 			= 0;

$UPLOAD_BASE_DOC_DIR	= "/usuarios/";
$UPLOAD_BASE_DOC 		= getDocumentRoot().$UPLOAD_BASE_DOC_DIR;

$UPLOAD_BASE_IMG_DIR	= "/images/colaboradores/";
$UPLOAD_BASE_IMG 		= getDocumentRoot().$UPLOAD_BASE_IMG_DIR;

$nome					= $_POST["nome"];
$sobrenome				= $_POST["sobrenome"];
$datanasc				= isset($_POST["datanasc"]) ? $_POST["datanasc"] : '' ;
$naturalidade			= isset($_POST["naturalidade"]) ? $_POST["naturalidade"] : '' ;
$uf						= isset($_POST["uf"]) ? $_POST["uf"] : '' ;
$nacionalidade			= isset($_POST["nacionalidade"]) ? $_POST["nacionalidade"] : '' ;
$enderecores			= isset($_POST["enderecores"]) ? $_POST["enderecores"] : '' ;
$cepres					= isset($_POST["cepres"]) ? $_POST["cepres"] : '' ;
$dddtelres				= isset($_POST["dddtelres"]) ? $_POST["dddtelres"] : '' ;
$telres					= isset($_POST["telres"]) ? $_POST["telres"] : '' ;
$dddcel					= isset($_POST["dddcel"]) ? $_POST["dddcel"] : '' ;
$cel					= isset($_POST["cel"]) ? $_POST["cel"] : '' ;
$email					= $_POST["email"];
$cd_empresa				= $_POST["empresa"];
$cd_lotacao				= $_POST["lotacao"];
$cargofuncao			= $_POST["cargofuncao"];
$enderecocom			= $_POST["enderecocom"];
$cepcom					= $_POST["cepcom"];
$dddtelcom				= $_POST["dddtelcom"];
$telcom					= $_POST["telcom"];
$ramal					= $_POST["ramal"];
$experiencia			= isset($_POST["experiencia"]) ? implode(",", $_POST["experiencia"]) : '' ;
$outrosexperiencia		= isset($_POST["outrosexperiencia"]) ? $_POST["outrosexperiencia"] : '' ;
$especializacao			= isset($_POST["especializacao"]) ? implode(",", $_POST["especializacao"]) : '' ;
$outrosespecializacao	= isset($_POST["outrosespecializacao"]) ? $_POST["outrosespecializacao"] : '' ;
$fotofile				= '';
$cvfile					= '';
$newsletteragreement	= isset($_POST["newsletteragreement"]) ? $_POST["newsletteragreement"] : "0";
$policyagreement		= isset($_POST["policyagreement"]) ? $_POST["policyagreement"] : "0";

$newsletteragreement	= $newsletteragreement == "on" ? "1" : "0";
$policyagreement		= $policyagreement == "on" ? "1" : "0";

$uploadfotoerror = 0;
$uploadcverror   = 0;

if(is_uploaded_file($_FILES['foto']['tmp_name'])){
	$savefile = $UPLOAD_BASE_IMG.$_FILES['foto']['name'];
	if(move_uploaded_file($_FILES['foto']['tmp_name'], $savefile)){
		chmod($savefile, 0666);
		$fotofile = $_FILES['foto']['name'];
	}
}
else{
	$uploadfotoerror = (Integer)$_FILES['foto']['error'] + 1;
}

if(is_uploaded_file($_FILES['cv']['tmp_name'])){
	$savefile = $UPLOAD_BASE_DOC.$_FILES['cv']['name'];
	if(move_uploaded_file($_FILES['cv']['tmp_name'], $savefile)){
		chmod($savefile, 0666);
		$cvfile = $_FILES['cv']['name'];
	}
}
else{
	$uploadcverror = (Integer)$_FILES['cv']['error'] + 1;
}

mysql_query('START TRANSACTION');

$sql  = "INSERT INTO col_usuario(";
$sql .= "tipo,";
$sql .= "grupo,";
$sql .= "permissao,";
$sql .= "NM_USUARIO,";
$sql .= "NM_SOBRENOME,";
$sql .= "datanasc,";
$sql .= "naturalidade,";
$sql .= "uf,";
$sql .= "nacionalidade,";
$sql .= "enderecores,";
$sql .= "cepres,";
$sql .= "dddtelres,";
$sql .= "telres,";
$sql .= "dddcel,";
$sql .= "cel,";
$sql .= "email,";
$sql .= "empresa,";
$sql .= "lotacao,";
$sql .= "cargofuncao,";
$sql .= "enderecocom,";
$sql .= "cepcom,";
$sql .= "dddtelcom,";
$sql .= "telcom,";
$sql .= "ramal,";
$sql .= "experiencia,";
$sql .= "outrosexperiencia,";
$sql .= "especializacao,";
$sql .= "outrosespecializacao,";
$sql .= "fotofile,";
$sql .= "cvfile,";
$sql .= "cv,";
$sql .= "newsletteragreement,";
$sql .= "policyagreement,";
$sql .= "datacadastro,";
$sql .= "Status";
$sql .= ") VALUES (";
$sql .= "'"  . $tipo . "'";
$sql .= ",'" . $grupo . "'";
$sql .= ",'" . $permissao . "'";
$sql .= ",'" . $nome . "'";
$sql .= ",'" . $sobrenome . "'";
$sql .= ",'" . $datanasc . "'";
$sql .= ",'" . $naturalidade . "'";
$sql .= ",'" . $uf . "'";
$sql .= ",'" . $nacionalidade . "'";
$sql .= ",'" . $enderecores . "'";
$sql .= ",'" . $cepres . "'";
$sql .= ",'" . $dddtelres . "'";
$sql .= ",'" . $telres . "'";
$sql .= ",'" . $dddcel . "'";
$sql .= ",'" . $cel . "'";
$sql .= ",'" . $email . "'";
$sql .= ",'" . $cd_empresa . "'";
$sql .= ",'" . $cd_lotacao . "'";
$sql .= ",'" . $cargofuncao . "'";
$sql .= ",'" . $enderecocom . "'";
$sql .= ",'" . $cepcom . "'";
$sql .= ",'" . $dddtelcom . "'";
$sql .= ",'" . $telcom . "'";
$sql .= ",'" . $ramal . "'";
$sql .= ",'" . $experiencia . "'";
$sql .= ",'" . $outrosexperiencia . "'";
$sql .= ",'" . $especializacao . "'";
$sql .= ",'" . $outrosespecializacao . "'";
$sql .= ",'" . $fotofile . "'";
$sql .= ",'" . $cvfile . "'";
$sql .= ",''";								// CV
$sql .= ",'" . $newsletteragreement . "'";
$sql .= ",'" . $policyagreement . "'";
$sql .= ",NOW()";							// NOW()
$sql .= ",'1'";								// STATUS
$sql .= ")";

if(!($RS_query = mysql_query($sql))){
	header('Location:retornocadastro.php?erro=1&up1='.$uploadfotoerror.'&up2='.$uploadcverror);
}
if($RS_query = mysql_query('SELECT MAX(CD_USUARIO) FROM col_usuario')){
	if($oRs = mysql_fetch_row($RS_query)){
		session_start();
		$_SESSION["cd_usuario"] = $oRs[0];
		$_SESSION["cd_empresa"] = $cd_empresa;
	}
}
else{
	header('Location:retornocadastro.php?erro=2&up1='.$uploadfotoerror.'&up2='.$uploadcverror);
}

mysql_query('COMMIT');

$resultado = mysql_query("SELECT CD_PROVA FROM col_prova_vinculo WHERE (CD_VINCULO = '$cd_empresa' AND IN_TIPO_VINCULO = 1) OR (CD_VINCULO = '$cd_lotacao' AND IN_TIPO_VINCULO = 2)");

mysql_num_rows($resultado);

while($linhaProva = mysql_fetch_array($resultado)){
	$sql = "INSERT INTO col_prova_aplicada (CD_PROVA, CD_USUARIO) VALUES ({$linhaProva["CD_PROVA"]}, {$_SESSION["cd_usuario"]})";
	mysql_query($sql);
}

mysql_close();

header('Location:cadastro2.php');
?>