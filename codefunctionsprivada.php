<?
function writetopcode(){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<link rel="stylesheet" type="text/css" media="all" href="/include/css/colaborae.css">
<link rel="stylesheet" type="text/css" media="print" href="/include/css/pcolaborae.css">
<link rel="stylesheet" type="text/css" media="screen" href="/include/css/caroussel.css">

<style type="text/css">

    html, body {height: 100%;}

    ul#topnav li:hover{background: #ccc;}

</style>

<script type="text/javascript" src="/include/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="/include/js/formfunctions.js"></script>
<script type="text/javascript" src="/include/js/formatadados.js"></script>
<script type="text/javascript">

    function closeMenu(){

        var atual = document.getElementById("tdMenu").style.display;
        document.getElementById("tdMenu").style.display = (atual=="none"? "block" : "none");
        document.getElementById("tdMenu").style.width = "190px";

        var idImagem = (atual=="none"? "" : "2");
        document.getElementById("imgOpenClose").src = "images/layout/btOpenClose" + idImagem + ".gif";

    }

    function redirecionaHistorico(obj){

        window.location = 'paginaindividualprivada.php?id=' + obj.value;
    }

</script>

<script type="text/javascript" src="/include/js/colaboraeprivada.js"></script>
<?
}

function writeheadercode($id, $cdUsuario, $cdEmpresa){

    $sql = "SELECT a.ID_PAGINA_INDIVIDUAL, p.TITULO FROM col_acesso_pagina a INNER JOIN tb_paginasindividuais p ON p.ID = ID_PAGINA_INDIVIDUAL
            WHERE a.CD_USUARIO = $cdUsuario AND p.CD_EMPRESA = $cdEmpresa ORDER BY DT_ACESSO_PAGINA DESC LIMIT 0,10";

    $resultado = mysql_query($sql);


?>
	<div style="float: right; position: relative; padding-top: 6px;">

        <select style="color: navy; background: transparent;" onchange="javascript:redirecionaHistorico(this);">
            <option>Acessos recentes</option>

            <?
            while($linha = mysql_fetch_row($resultado)){

                echo "<option value=\"{$linha[0]}\">{$linha[1]}</option>";

            }

            ?>

        </select>

	</div>

<?php

    $sql = "INSERT INTO col_acesso_pagina (DT_ACESSO_PAGINA, ID_PAGINA_INDIVIDUAL, CD_USUARIO) VALUES
                                            (sysdate(), $id, $cdUsuario)";
    mysql_query($sql);


}

function writeloggedcode(){
	$user = isset($_SESSION["nome"])?$_SESSION["nome"]:'';
	if($user){
		if(isValidUser()){
			echo '<p><strong>'.$user.'</strong> est� logado.&nbsp;-&nbsp;&nbsp;<a href="logout.php" title="   logout   ">Logout</a></p>';
		}
		else{
			echo '<p><span><strong>'.$user.'</strong></span></p>';
			unset($_SESSION['usuario']);
			unset($_SESSION['nome']);
			unset($_SESSION['empresaID']);
			unset($_SESSION['email']);
		}
	}
}

function writemenucode($pasta, $nomePagina="paginaindividualprivada"){
	echo '<div id="menu"><ul id="topnav">';
    $sql1 = "SELECT pa.ID, pa.Titulo, (SELECT ID FROM tb_paginasindividuais WHERE NR_ORDEM = MIN(pi.NR_ORDEM) AND PastaID = pa.ID and Status = 1 LIMIT 0,1) AS MIN_ID FROM tb_pastasindividuais pa INNER JOIN tb_paginasindividuais pi ON pa.ID = pi.PastaID WHERE pi.Status = 1 AND pi.CD_EMPRESA = {$_SESSION["empresaID"]} GROUP BY pa.NR_ORDEM, pa.ID, pa.Titulo ORDER BY pa.NR_ORDEM";

	$query1 = mysql_query($sql1) or die(ERROR_MSG_SQLQUERY . mysql_error());
	while($oRs1 = mysql_fetch_row($query1)){

        $relevo = "";
        if ($pasta == $oRs1[0]) $relevo = "background-color: #aaa;";

		echo '<li><a style="color: #000; '. $relevo .'" href="' . $nomePagina . '.php?id=' . $oRs1[2] . '&pasta=' . $oRs1[0] . '">' . $oRs1[1] . '</a>';

        $sql2 = "SELECT c.CD_CAPITULO, NM_CAPITULO, (SELECT ID FROM tb_paginasindividuais WHERE CD_CAPITULO = c.CD_CAPITULO AND Status = 1 AND NR_ORDEM = MIN(pi.NR_ORDEM) LIMIT 0,1) AS MIN_ID FROM col_capitulo c LEFT JOIN tb_paginasindividuais pi ON pi.CD_CAPITULO = c.CD_CAPITULO WHERE IN_ATIVO = 1 AND CD_PASTA = {$oRs1[0]} GROUP BY c.CD_CAPITULO, NM_CAPITULO, NR_ORDEM_CAPITULO ORDER BY NR_ORDEM_CAPITULO";
        //echo $sql2;
        $query2 = mysql_query($sql2) or die(ERROR_MSG_SQLQUERY . mysql_error());

        if (mysql_num_rows($query2) > 0){

            echo "<span style='background: #ddd; color: #000;'>";
            while($oRs2 = mysql_fetch_row($query2)){

                echo "<a style='color: #000; font-weight: bold;' href='$nomePagina.php?id={$oRs2[2]}&pasta={$oRs1[0]}'>{$oRs2[1]}</a>";

            }
            echo "</span>";
        }
		echo '</li>';
	}
	mysql_free_result($query1);
	
	echo '</ul>';
	echo '</div>';
	echo '<p class="clearboth"></p>';
}

function writenavcode($oNav){
    $id = isset($_GET["id"])?$_GET["id"]:0;
    $sql = "SELECT ID, NR_ORDEM, PastaID, CD_EMPRESA FROM tb_paginasindividuais WHERE ID = $id;";
    $query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
    $linha = mysql_fetch_array($query);
    $pasta = $linha["PastaID"];
    $ordem = $linha["NR_ORDEM"];
    mysql_free_result($query);

    if(is_numeric($ordem)){

        $sql = "SELECT ID FROM tb_paginasindividuais WHERE Status = 1 AND PastaID = $pasta AND NR_ORDEM > $ordem ORDER BY NR_ORDEM LIMIT 0,1";
        $query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
        $linha = mysql_fetch_row($query);
        $idProximo = $linha[0];
        mysql_free_result($query);

        $sql = "SELECT * FROM tb_paginasindividuais WHERE Status = 1 AND PastaID = $pasta AND NR_ORDEM < $ordem ORDER BY NR_ORDEM DESC LIMIT 0,1";
        $query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
        $linha = mysql_fetch_row($query);
        $idAnterior = $linha[0];
        mysql_free_result($query);

        //Verifica se � o

    }
	if($oNav == 1){

?>
			<div id="navtop">
                <? if ($idProximo != ""){ ?>
                <a href="paginaindividualprivada.php?id=<?echo $idProximo ?>" title="   p�gina posterior  "><img src="images/layout/btfoward.png" /></a>
                <?}
                if ($idAnterior != ""){ ?>
                <a href="paginaindividualprivada.php?id=<?echo $idAnterior ?>" title="   p�gina anterior  "><img src="images/layout/btback.png" /></a>
                <?} ?>
				<a href="#" onclick="if(window.print())window.print();return false;return false" title="   imprimir p�gina   "><img src="images/layout/btprint.png" /></a>
			</div>
<?php
	}
	if($oNav == 2){
?>
		<div id="navbottom">
             <? if ($idProximo != ""){ ?>
            <a href="paginaindividualprivada.php?id=<?echo $idProximo ?>" title="   p�gina posterior  "><img src="images/layout/btfoward2.png" /></a>
            <?}
            if ($idAnterior != ""){ ?>
            <a href="paginaindividualprivada.php?id=<?echo $idAnterior ?>" title="   p�gina anterior  "><img src="images/layout/btback2.png" /></a>
            <?} ?>
		</div>
<?
	}
}

function writebottomcode($navigation=true){
?>
	<div id="rodape">
		<ul>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_facebook.png" />Facebook</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_twitter.png" />Twitter</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_linkedin.png" />LinkedIn</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_podcast2.png" />Podcasts</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_newsletter.png" />Newsletters</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_youtube.png" />YouTube</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_rss.png" />RSS</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_blogs.png" />Blogs</a> --></li>
		<li><!-- <a href="#"><img src="images/layout/footer_icons_movies.png" />V�deos</a> --></li>
		</ul>
<?php if($navigation) writenavcode(2);?>
	</div>
	<div id="copyright">

		<span>&copy; Colabor&aelig; <?php echo date("Y");?></span>
	</div>
<?php
}


function dataGridProva($usuario, $idPaginaIndividual){

    $tipo = $_SESSION["tipo"];
    $empresaID = $_SESSION["empresaID"];


    $sql =          "SELECT
                        DT_INICIO_REALIZACAO,
                        DT_TERMINO_REALIZACAO,
                        HR_INICIO_REALIZACAO,
                        HR_TERMINO_REALIZACAO,
                        DURACAO_PROVA,
                        c.CD_PROVA,
                        DS_PROVA,
                        IN_SENHA_MESTRA,
                        CD_CICLO,
                        COUNT(DISTINCT pe.CD_PERGUNTAS) AS QTD_PERGUNTAS
                    FROM
                        col_prova c
                    INNER JOIN col_prova_aplicada pa ON pa.CD_PROVA = c.CD_PROVA
                    INNER JOIN col_prova_chamada pc ON pc.CD_PROVA = c.CD_PROVA
                    INNER JOIN col_prova_disciplina pd ON pd.CD_PROVA = c.CD_PROVA
                    INNER JOIN col_disciplina d ON d.CD_DISCIPLINA = pd.CD_DISCIPLINA
                    LEFT JOIN col_perguntas pe ON pe.CD_DISCIPLINA = pd.CD_DISCIPLINA
                    INNER JOIN col_prova_vinculo pv ON pv.CD_PROVA = c.CD_PROVA
                    LEFT JOIN col_prova_cargo prc ON prc.CD_PROVA = c.CD_PROVA
                    WHERE
                        (pa.CD_USUARIO = $usuario OR $tipo > 3)
                    AND(
                        pv.CD_VINCULO = $empresaID
                    AND IN_TIPO_VINCULO = 1
                    )
                    AND c.IN_ATIVO = 1
                    AND(
                        prc.NM_CARGO_FUNCAO = '{$_SESSION["cargofuncao"]}'
                    OR prc.NM_CARGO_FUNCAO IS NULL
                    OR prc.NM_CARGO_FUNCAO = ''
                    )
                    AND CONCAT(
                        DATE_FORMAT(
                            DT_INICIO_REALIZACAO,
                            '%Y%m%d'
                        ),
                        DATE_FORMAT(
                            HR_INICIO_REALIZACAO,
                            '%H%i'
                        )
                    )<= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
                    AND CONCAT(
                        DATE_FORMAT(
                            DT_TERMINO_REALIZACAO,
                            '%Y%m%d'
                        ),
                        DATE_FORMAT(
                            HR_TERMINO_REALIZACAO,
                            '%H%i'
                        )
                    )>= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
                    AND d.ID_PAGINA_INDIVIDUAL = $idPaginaIndividual
                    AND(
                        (
                            c.IN_PROVA = 0
                        AND c.NR_TENTATIVA > pa.NR_TENTATIVA_USUARIO

                        )
                    )
                    GROUP BY
                        DT_INICIO_REALIZACAO,
                        DT_TERMINO_REALIZACAO,
                        HR_INICIO_REALIZACAO,
                        HR_TERMINO_REALIZACAO,
                        DURACAO_PROVA,
                        c.CD_PROVA,
                        DS_PROVA,
                        IN_SENHA_MESTRA,
                        CD_CICLO
                    ORDER BY
                        DS_PROVA,
                        DT_INICIO_REALIZACAO,
                        HR_INICIO_REALIZACAO
                    ";






    /*$sqlWhereTipoProva = "c.IN_PROVA = 0 AND c.NR_TENTATIVA > pa.NR_TENTATIVA_USUARIO";
    $sqlWhereTipoProva = "$sqlWhereTipoProva AND c.CD_PROVA IN (SELECT
                                                        pd.CD_PROVA
                                                    FROM
                                                        col_prova_disciplina pd
                                                        INNER JOIN col_disciplina d ON d.CD_DISCIPLINA = pd.CD_DISCIPLINA
                                                        LEFT JOIN col_perguntas p ON p.CD_DISCIPLINA = d.CD_DISCIPLINA
                                                    WHERE
                                                        d.ID_PAGINA_INDIVIDUAL = $idPaginaIndividual
                                                    GROUP BY
                                                        pd.CD_PROVA
                                                    HAVING COUNT(p.CD_PERGUNTAS) > 4)";

    $sql = "SELECT CD_PROVA FROM col_prova_disciplina";

    $sql = "SELECT DISTINCT
				DT_INICIO_REALIZACAO, DT_TERMINO_REALIZACAO, HR_INICIO_REALIZACAO, HR_TERMINO_REALIZACAO, DURACAO_PROVA, c.CD_PROVA, DS_PROVA, IN_SENHA_MESTRA, CD_CICLO
				 FROM col_prova c INNER JOIN col_prova_aplicada pa on pa.CD_PROVA = c.CD_PROVA INNER JOIN col_prova_chamada pc ON pc.CD_PROVA = c.CD_PROVA INNER JOIN col_prova_vinculo pv ON pv.CD_PROVA = c.CD_PROVA LEFT JOIN col_prova_cargo prc ON prc.CD_PROVA = c.CD_PROVA WHERE (pa.CD_USUARIO = $usuario or $tipo > 3) AND (pv.CD_VINCULO = $empresaID AND IN_TIPO_VINCULO = 1) AND c.IN_ATIVO = 1 AND (prc.NM_CARGO_FUNCAO = '{$_SESSION["cargofuncao"]}' or prc.NM_CARGO_FUNCAO is null or prc.NM_CARGO_FUNCAO = '')
				AND CONCAT(DATE_FORMAT(DT_INICIO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_INICIO_REALIZACAO, '%H%i')) <= DATE_FORMAT(sysdate(), '%Y%m%d%H%i') AND CONCAT(DATE_FORMAT(DT_TERMINO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_TERMINO_REALIZACAO, '%H%i')) >= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
				AND (($sqlWhereTipoProva))
				ORDER BY DS_PROVA, DT_INICIO_REALIZACAO, HR_INICIO_REALIZACAO";
    */
    //echo "<!--$sql-->";
    //exit();
    $resultado = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());;



    if(mysql_num_rows($resultado) == 0){

        return;
    }

    echo '                        <TABLE cellpadding="0" cellspacing="0" border="0" width="98%">
                <tr class="tarjaItens">
                    <td colspan="8" class="title" style="text-align:center; background-color: #ccc;">Simulado</td>

                </tr>
                <TR class="tarjaItens">
                    <TD class="title">T�tulo</TD>
                    <TD class="title" align="center">In�cio</TD>
                    <TD class="title" align="center">T�rmino</TD>
                    <TD class="title" align="center">Hora In�cio</TD>
                    <TD class="title" align="center">Hora T�rmino</TD>
                    <TD class="title" align="center">Quest�es</TD>
                    <TD class="title" align="center">Tempo</TD>
                    <TD class="title">&nbsp;</TD>
                </TR>';

                while ($linha = mysql_fetch_array($resultado)){

                    $data = page::formataPropriedade($linha["DT_INICIO_REALIZACAO"], DaoEngine::DATA);
                    $dataFim = page::formataPropriedade($linha["DT_TERMINO_REALIZACAO"], DaoEngine::DATA);
                    $inicio = page::formataPropriedade($linha["HR_INICIO_REALIZACAO"], DaoEngine::DATA_HORA_MINUTO);
                    $fim = page::formataPropriedade($linha["HR_TERMINO_REALIZACAO"], DaoEngine::DATA_HORA_MINUTO);
                    $duracao = $linha["DURACAO_PROVA"];

                    $sqlQuestoes = "SELECT SUM(NR_QTD_PERGUNTAS_NIVEL_1) + SUM(NR_QTD_PERGUNTAS_NIVEL_2) + SUM(NR_QTD_PERGUNTAS_NIVEL_3) as Total
                    FROM col_prova_disciplina WHERE CD_PROVA = {$linha["CD_PROVA"]}";

                    $resultadoQuestoes = mysql_query($sqlQuestoes);

                    $quest�es = 0;

                    if ($linhaQuestoes = mysql_fetch_row($resultadoQuestoes))
                    {
                        $quest�es = $linhaQuestoes[0];
                    }

                    $linkProva = "<a href='canal/certificacao/prova.php?id={$linha["CD_PROVA"]}' target='_top'>Iniciar</a>";
                    $hrefProva = "<a href='canal/certificacao/prova.php?id={$linha["CD_PROVA"]}' target='_top'>{$linha["DS_PROVA"]}</a>";

                    echo "<TR style='height: 20px;'>
        <TD class='textblk' style='text-align:left;'>$hrefProva</TD>
        <TD class='textblk' style='text-align:center;'>{$data}</TD>
        <TD class='textblk' style='text-align:center;'>{$dataFim}</TD>
        <TD class='textblk' style='text-align:center;'>{$inicio}</TD>
        <TD class='textblk' style='text-align:center;'>{$fim}</TD>
        <TD class='textblk' style='text-align:center;'>{$quest�es}</TD>
        <TD class='textblk' style='text-align:center;'>{$duracao}</TD>
        <TD class='textblk' style='text-align:left;'>$linkProva</TD>
        </TR>";

                }


                echo '						</TABLE>';

}


?>