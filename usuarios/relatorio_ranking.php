<?php

//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');

$tituloTotal = "";
$tituloRelatorio = "Ranking";

$POST = obterPost();

//Obter Parametros
$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];

$codigoEmpresa = $POST['cboEmpresa'];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$dataAtual = $dataInicial;

// Fim obter parametros
$campoSelect = "DS_LOTACAO";
$tituloCampo = "Lota��o";

switch ($tipoRelatorio)
{
	case "L":
		$campoSelect = "DS_LOTACAO";
		$tituloCampo = "Lota��o";
		break;
	case "U":
		$campoSelect = "login";
		$tituloCampo = "Participante";
		break;
}

//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa


$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);


//COUNT(DISTINCT a.CD_USUARIO, IF(DS_PAGINA_ACESSO = 'FORUM' ,DS_PAGINA_ACESSO ,null)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_PARTICIPACAO_FORUM
$sqlAcesso = "
SELECT
  $campoSelect,
  COUNT(DISTINCT(IF(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery',a.CD_USUARIO,null))) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_PARTICIPACAO,
  COUNT(DISTINCT a.DS_ID_SESSAO, IF(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery',a.CD_USUARIO,null)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_ACESSO,
  COUNT(DISTINCT a.CD_USUARIO, IF(DS_PAGINA_ACESSO LIKE 'DOWNLOAD%' AND (DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery'),DS_PAGINA_ACESSO ,null)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_DOWNLOAD
FROM
  col_usuario u
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  LEFT JOIN col_acesso a ON a.CD_USUARIO = u.CD_USUARIO
  LEFT JOIN col_pagina_acesso pa ON pa.CD_PAGINA_ACESSO = a.CD_PAGINA_ACESSO
WHERE
  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
  AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
GROUP BY
  $campoSelect
ORDER BY
  $campoSelect;";

$resultadoAcesso = DaoEngine::getInstance()->executeQuery($sqlAcesso,true);

$sqlAvaliacao = "
SELECT
  $campoSelect,
  COUNT(IF(p.IN_PROVA = 1 AND pa.VL_MEDIA IS NOT NULL AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), u.CD_USUARIO, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_PARTICIPAO_AVALIACAO,
  COUNT(IF(p.IN_PROVA = 0 AND pa.VL_MEDIA IS NOT NULL AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), u.CD_USUARIO, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_PARTICIPAO_SIMULADO,
  COALESCE(SUM(IF(p.IN_PROVA = 0 AND pa.VL_MEDIA IS NOT NULL AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), pa.NR_TENTATIVA_USUARIO, NULL)),0) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_REALIZACAO_SIMULADO,
  COALESCE(AVG(IF(p.IN_PROVA = 1 AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), pa.VL_MEDIA, NULL)),0) AS MEDIA_AVALIACAO,
  COALESCE(AVG(IF(p.IN_PROVA = 0 AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), pa.VL_MEDIA, NULL)),0) AS MEDIA_SIMULADO
FROM
  col_usuario u
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  LEFT JOIN col_prova_aplicada pa ON pa.CD_USUARIO = u.CD_USUARIO
  LEFT JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
WHERE
  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
  AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
GROUP BY
  $campoSelect
ORDER BY
  $campoSelect;";

$resultadoAvaliacao = DaoEngine::getInstance()->executeQuery($sqlAvaliacao,true);


$sqlForum = "
SELECT
  $campoSelect,
  COUNT(DISTINCT(IF(DATE_FORMAT(fo.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(fo.data, '%Y%m%d') <= '$dataFinalQuery' ,fo.name, NULL))) / COUNT(DISTINCT(u.CD_USUARIO)) as FATOR_PARTICIPACAO_FORUM,
  COUNT(IF(DATE_FORMAT(fo.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(fo.data, '%Y%m%d') <= '$dataFinalQuery' ,fo.id, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_CONTRIBUICAO_FORUM
FROM
  col_usuario u
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  LEFT JOIN col_foruns f ON f.CD_EMPRESA = u.empresa
  LEFT JOIN tb_forum fo ON fo.cd_empresa = u.empresa AND f.CD_FORUM = fo.CD_FORUM AND u.login = fo.name
WHERE
  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
  AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
GROUP BY
  $campoSelect
ORDER BY
  $campoSelect;";

$resultadoForum = DaoEngine::getInstance()->executeQuery($sqlForum,true);

$linhas = array();


$pesoParticipacao = 1;
$pesoAcesso = 1;
$pesoDownload = 1;
$pesoParticipacaoAvaliacao = 1;
$pesoParticipacaoSimulado = 1;
$pesoRealizacaoSimulado = 1;
$pesoMediaAvaliacao = 1;
$pesoMediaSimulado = 1;
$pesoParticipacaoForum = 1;
$pesoContribuicaoForum = 1;


while ($linhaAcesso = mysql_fetch_array($resultadoAcesso)) {
	
	$linhaAvaliacao = mysql_fetch_array($resultadoAvaliacao);
	$linhaForum = mysql_fetch_array($resultadoForum);
	
	$linhaAcesso["FATOR_PARTICIPAO_AVALIACAO"] = $linhaAvaliacao["FATOR_PARTICIPAO_AVALIACAO"];
	$linhaAcesso["FATOR_PARTICIPAO_SIMULADO"] = $linhaAvaliacao["FATOR_PARTICIPAO_SIMULADO"];
	$linhaAcesso["FATOR_REALIZACAO_SIMULADO"] = $linhaAvaliacao["FATOR_REALIZACAO_SIMULADO"];
	$linhaAcesso["MEDIA_AVALIACAO"] = $linhaAvaliacao["MEDIA_AVALIACAO"];
	$linhaAcesso["MEDIA_SIMULADO"] = $linhaAvaliacao["MEDIA_SIMULADO"];
	
	$linhaAcesso["FATOR_PARTICIPACAO_FORUM"] = $linhaForum["FATOR_PARTICIPACAO_FORUM"];
	$linhaAcesso["FATOR_CONTRIBUICAO_FORUM"] = $linhaForum["FATOR_CONTRIBUICAO_FORUM"];
	
	$pontos = ($linhaAcesso["FATOR_PARTICIPACAO"] * $pesoParticipacao) +
			  ($linhaAcesso["FATOR_ACESSO"] * $pesoAcesso) +
			  ($linhaAcesso["FATOR_DOWNLOAD"] * $pesoDownload) +
			  ($linhaAcesso["FATOR_PARTICIPAO_AVALIACAO"] * $pesoParticipacaoAvaliacao) +
			  ($linhaAcesso["FATOR_PARTICIPAO_SIMULADO"] * $pesoParticipacaoSimulado) +
			  ($linhaAcesso["FATOR_REALIZACAO_SIMULADO"] * $pesoRealizacaoSimulado) +
			  ($linhaAcesso["MEDIA_AVALIACAO"] * $pesoMediaAvaliacao) +
			  ($linhaAcesso["MEDIA_SIMULADO"] * $pesoMediaSimulado) +
			  ($linhaAcesso["FATOR_PARTICIPACAO_FORUM"] * $pesoParticipacaoForum) +
			  ($linhaAcesso["FATOR_CONTRIBUICAO_FORUM"] * $pesoContribuicaoForum);
			  
	$linhaAcesso["TOTAL_PONTUACAO"] = $pontos;
	
	$linhas[] = $linhaAcesso;
	
}

usort($linhas, "ordenarParticipacao");
$linhas = ranquearItens($linhas, "FATOR_PARTICIPACAO","RANK_PARTICIPACAO");
usort($linhas, "ordenarAcesso");
$linhas = ranquearItens($linhas, "FATOR_ACESSO","RANK_ACESSO");
usort($linhas, "ordenarDownload");
$linhas = ranquearItens($linhas, "FATOR_DOWNLOAD","RANK_DOWNLOAD");
usort($linhas, "ordenarParticipacaoAvaliacao");
$linhas = ranquearItens($linhas, "FATOR_PARTICIPAO_AVALIACAO","RANK_PARTICIPAO_AVALIACAO");
usort($linhas, "ordenarParticipacaoSimulado");
$linhas = ranquearItens($linhas, "FATOR_PARTICIPAO_SIMULADO","RANK_PARTICIPAO_SIMULADO");
usort($linhas, "ordenarRealizacaoSimulado");
$linhas = ranquearItens($linhas, "FATOR_REALIZACAO_SIMULADO","RANK_REALIZACAO_SIMULADO");
usort($linhas, "ordenarMediaAvaliacao");
$linhas = ranquearItens($linhas, "MEDIA_AVALIACAO","RANK_MEDIA_AVALIACAO");
usort($linhas, "ordenarMediaSimulado");
$linhas = ranquearItens($linhas, "MEDIA_SIMULADO","RANK_MEDIA_SIMULADO");
usort($linhas, "ordenarParticipacaoForum");
$linhas = ranquearItens($linhas, "FATOR_PARTICIPACAO_FORUM","RANK_PARTICIPACAO_FORUM");
usort($linhas, "ordenarContribuicaoForum");
$linhas = ranquearItens($linhas, "FATOR_CONTRIBUICAO_FORUM","RANK_CONTRIBUICAO_FORUM");
usort($linhas, "ordenarTotalGeral");
$linhas = ranquearItens($linhas, "TOTAL_PONTUACAO","RANK_TOTAL_PONTUACAO");

function conveterDataParaYMD($data)
{
	$aData = split("/", $data);
	$data = "{$aData[2]}{$aData[1]}{$aData[0]}";
	
	return $data;
	
}

function ranquearItens($linhas, $campo, $campoRank)
{
	$posicaoAtual = 1;
	$ultimoValor = -1;
	
	
	
	//foreach ($linhas as $linha)
	for ($i=0; $i < count($linhas); $i++)
	{
		
		$linhas[$i][$campoRank] = $posicaoAtual;
		
		if ($linhas[$i][$campo] != $ultimoValor)
		{
			$posicaoAtual++;
		}
		
		//echo $linhas[$i][$campoRank];
		
		$ultimoValor = $linha[$campo];
		
	}
	
	return $linhas;
	
}

function ordenarParticipacao($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPACAO");
}

function ordenarAcesso($a, $b)
{
	return ordenarDados($a, $b, "FATOR_ACESSO");
}

function ordenarDownload($a, $b)
{
	return ordenarDados($a, $b, "FATOR_DOWNLOAD");
}

function ordenarParticipacaoAvaliacao($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPAO_AVALIACAO");
}

function ordenarParticipacaoSimulado($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPAO_SIMULADO");
}

function ordenarRealizacaoSimulado($a, $b)
{
	return ordenarDados($a, $b, "FATOR_REALIZACAO_SIMULADO");
}

function ordenarMediaAvaliacao($a, $b)
{
	return ordenarDados($a, $b, "MEDIA_AVALIACAO");
}

function ordenarMediaSimulado($a, $b)
{
	return ordenarDados($a, $b, "MEDIA_SIMULADO");
}

function ordenarParticipacaoForum($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPACAO_FORUM");
}

function ordenarContribuicaoForum($a, $b)
{
	return ordenarDados($a, $b, "FATOR_CONTRIBUICAO_FORUM");
}

function ordenarTotalGeral($a, $b)
{
	return ordenarDados($a, $b, "TOTAL_PONTUACAO");
}

function ordenarDados($a, $b, $campo)
{
	if ($a[$campo] == $b[$campo])
		return 0;
		
	return ($a[$campo] > $b[$campo]) ? -1:+1;
}

?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
	</style>
	
</head>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk'>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<p><?php echo date("d/m/Y") ?></p>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr class="textblk">
		<td width="100%">
			Per�odo:&nbsp;
							<?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0" style="border: 1px solid black;">
	<tr class='textblk'>
		<td class="bdLat" ><?php echo $tituloCampo; ?></td>
		<td class='bdLat' width="30" align="center">RP</td>
		<td class='bdLat' width="30" align="center">RA</td>
		<td class='bdLat' width="30" align="center">RD</td>
		<td class='bdLat' width="30" align="center">RPS</td>
		<td class='bdLat' width="30" align="center">RQS</td>
		<td class='bdLat' width="30" align="center">RNS</td>
		<td class='bdLat' width="30" align="center">RPF</td>
		<td class='bdLat' width="30" align="center">RCF</td>
		<td class='bdLat' width="30" align="center">RPA</td>
		<td class='bdLat' width="30" align="center">RNA</td>
		<td align="center" width="30">RT</td>
	</tr>

	<?php
	
		foreach ($linhas as $linha)
		{
			
			echo "<tr class='textblk'>
					<td class='titRelatD'>{$linha[$campoSelect]}</td>
					<td class='titRelat'>{$linha["RANK_PARTICIPACAO"]}</td>
					<td class='titRelat'>{$linha["RANK_ACESSO"]}</td>
					<td class='titRelat'>{$linha["RANK_DOWNLOAD"]}</td>
					<td class='titRelat'>{$linha["RANK_PARTICIPAO_SIMULADO"]}</td>
					<td class='titRelat'>{$linha["RANK_REALIZACAO_SIMULADO"]}</td>
					<td class='titRelat'>{$linha["RANK_MEDIA_SIMULADO"]}</td>
					<td class='titRelat'>{$linha["RANK_PARTICIPACAO_FORUM"]}</td>
					<td class='titRelat'>{$linha["RANK_CONTRIBUICAO_FORUM"]}</td>
					<td class='titRelat'>{$linha["RANK_PARTICIPAO_AVALIACAO"]}</td>
					<td class='titRelat'>{$linha["RANK_MEDIA_AVALIACAO"]}</td>
					<td class='titRelatTop'>{$linha["RANK_TOTAL_PONTUACAO"]}</td>
				  </tr>
			";
			
			
			
			
			
		}
	
	?>
	
</table>
<p align="center">
	<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
	<p class="textoInformativo" align="center">Favor definir o formato paisagem para fins de impress�o</p>
</p>	
</body>
</html>