<?php
include "include/security2.php";
include "include/defines.php";
include "include/dbconnection.php";
include "include/genericfunctions.php";
include "include/codefunctions.php";

$erro = isset($_GET['erro']) ? $_GET['erro'] : 1;
$up1  = isset($_GET['up1']) ? $_GET['up1'] : -1;
$up2  = isset($_GET['up2']) ? $_GET['up2'] : -1;
?>
<?php writetopcode();?>
</head>
<body>
<div id="global">
<div id="globalpadding">
<?php writeheadercode();?>
<?php writemenucode();?>
	<div id="conteudo">
<?php
$titulo = "Cadastro";
?>
		<h1><?php echo $titulo; writenavcode(1);?></h1>
<?php
	$msg  = '';
	switch($erro){
		case 0:	$msg = "<strong>Cadastro realizado com sucesso!</strong>";
				break;
		case 1: $msg = "<span class='erro'>Houve um erro ao tentar gravar seus dados!</span><br>Favor tentar novamente.";
				break;
		case 2: $msg = "<span class='erro'>N�o foi poss�vel completar a grava��o de seus dados!</span><br>Favor tentar novamente.";
				break;
		case 3: $msg = "<span class='erro'>O Login escolhido j� est� em uso!</span><br>Favor escolher outro.";
				break;
		case 4: $msg = "<span class='erro'>Esta sess�o foi interrompida inesperadamente!</span><br>Favor cadastrar-se novamente.";
				break;
	}
	echo '<p>'.$msg.'</p>';
	$msg  = '';
	if($erro < 3){
		switch($up1){
			case 0: // no error;
					$msg = "<strong>Arquivo anexado de foto gravado com sucesso!</strong>";
					break;
			case 1: // unknown error; possible file attack
					$msg = "<span class='erro'>Houve um erro n�o especificado com o upload do arquivo de foto.</span>";
					break;
			case 2: // uploaded file exceeds the upload_max_filesize directive in php.ini
					$msg = "<span class='erro'>O arquivo de foto que voc� est� tentando fazer upload � muito grande.</span>";
					break;
			case 3: // uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
					$msg = "<span class='erro'>O arquivo de foto que voc� est� tentando fazer upload � muito grande.</span>";
					break;
			case 4: // uploaded file was only partially uploaded
					$msg = "<span class='erro'>O arquivo de foto que voc� est� tentando fazer upload n�o foi transferido integralmente.</span>";
					break;
		}
		echo '<p>'.$msg.'</p>';
		$msg  = '';
		switch($up2){
			case 0: // no error;
					$msg = "<strong>Arquivo anexado de Curriculum gravado com sucesso!</strong>";
					break;
			case 1: // unknown error; possible file attack
					$msg = "<span class='erro'>Houve um erro n�o especificado com o upload do arquivo de Curriculum.</span>";
					break;
			case 2: // uploaded file exceeds the upload_max_filesize directive in php.ini
					$msg = "<span class='erro'>O arquivo de Curriculum que voc� est� tentando fazer upload � muito grande.</span>";
					break;
			case 3: // uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
					$msg = "<span class='erro'>O arquivo de Curriculum que voc� est� tentando fazer upload � muito grande.</span>";
					break;
			case 4: // uploaded file was only partially uploaded
					$msg = "<span class='erro'>O arquivo de Curriculum que voc� est� tentando fazer upload n�o foi transferido integralmente.</span>";
					break;
		}
		echo '<p>'.$msg.'</p>';
	}
	if($erro == 1 || $erro == 2 || $erro == 3){
		echo '<p>&nbsp;</p>';
		echo '<p><input class="buttonsty8" style="width:150px" type="button" value="Voltar" onclick="history.back()"></p>';
	}
	if($erro == 4){
		echo '<p>&nbsp;</p>';
		echo '<p><input class="buttonsty8" style="width:150px" type="button" value="Novo cadastro" onclick="document.location.href=\'cadastro1.php\'"></p>';
	}
?>
	</div>
<?php writebottomcode(); ?>
</div>
</div>
</body>
</html>
<?php
mysql_close();
?>