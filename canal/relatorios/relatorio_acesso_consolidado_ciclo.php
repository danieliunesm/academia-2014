<?php
//Includes

include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include('relatorio_acesso_util.php');

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

$tituloTotal = "";
$tituloRelatorio = "Radar Consolidado";


//Obter parametros
$POST = obterPost();
$codigoEmpresa = $POST['cboEmpresa'];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$codigoCiclo = $_REQUEST["hdnCiclo"];
$nomeCiclo = obterNomeCiclo($codigoCiclo);

$usaData = true;

if ($dataInicial == "" && $dataFinal == ""){

	$usaData = false;

}


if ($dataInicial==""){

	$dataInicial = '01/01/2008';

}

if ($dataFinal=="") {

	$dataFinal = date("d/m/Y");

}

$dataAtual = $dataInicial;
// Fim obter parametros


//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa

//Obter Lotacao Hierarquia



//Acertar as p�ginas a serem exibidas
$paginaRn = new col_pagina_acesso();
$paginaRn->prepararRelatorios($dataInicial, $dataFinal, $codigoEmpresa);


if ($_POST["btnExcel"] != "") {

	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=RadarConsolidado.xls");

}

$dataInicialQuery = conveterDataParaYMD($dataInicial);

$dataFinalQuery = conveterDataParaYMD($dataFinal);

//Obter o total de participantes do filtro

$sqlTotalGeral = "SELECT

		  COUNT(DISTINCT(u.CD_USUARIO)) AS QD_TOTAL

		FROM

		  col_acesso a

		  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
		  LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao

		WHERE

		  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)

		  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')

		  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')

		  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))

		  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";



$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);

$linha = mysql_fetch_row($resultadoTotalGeral);

$totalParticipantesGrupo = $linha[0];


ob_start();

	
?>



<html>

<head>

	<title>Colaborae Consultoria e Educa��o Corporativa</title>

	

	<?php

	

	if ($_POST["btnExcel"] == "") {



	?>

	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">		

	<?php

	}

	?>

	

	

	<style type="text/css">



		@media print {

			.buttonsty {

			  display: none;

			}

			.textoInformativo{

				display: none;

			}

		}

		

		.textoInformativo{

			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						

		}

	

		.titRelat, .titRelatD

		{

			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;

		}

		

		.titRelatD

		{

			text-align: left;

		}

		

		.titRelatTop

		{

			border-top: 1px solid #000000; text-align: center;

		}

		

		.titRelatEsq

		{

			border-right: 1px solid #000000; border-top: 1px solid #000000;

			border-left: 1px solid #000000;

		}

		

		.titRelatRod

		{

			border-right: 1px solid #000000; border-top: 1px solid #000000;

			border-bottom: 1px solid #000000;

		}

		

		.titRelatEsqRod

		{

			border-right: 1px solid #000000; border-top: 1px solid #000000;

			border-bottom: 1px solid #000000;

			border-left: 1px solid #000000;

		}

		

		.bdLat

		{

			border-right: 1px solid #000000;

		}

		

		.bdLatEsq

		{

			border-right: 1px solid #000000;

			border-left: 1px solid #000000;

		}

		

		.bdEsq

		{

			border-left: 1px solid #000000;

		}



		.bdRod

		{

			border-bottom: 1px solid #000000;

		}
		
		.bdTop
		{
			border-top: 1px solid #000000;
		}

		body{overflow:auto}
	</style>

	

</head>



<body>



<br>



<?php



	$colSpan = 0;

	$colunasMaximo = 22;

	$colunasAparecer = 0;

	$colunasTotal = 0;

	$ciclos = obterNomeCiclos(true);

	$colSpan = count($ciclos);


?>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">

	<tr style="height: 30px">

		<td class='textblk'>

			<p><b><?php echo $nomeEmpresa; ?></b></p>

		</td>

		<td width="65%" class="textblk" colspan="<?php echo $colSpan + 1; ?>" align="center">

				<?php echo $tituloRelatorio; ?>

			</b>

		</td>

		<td style="text-align: right" class='textblk'>

			<p><?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?></p>

		</td>

	</tr>

</table>





<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">

	<?php
		if ($POST["nomeFiltro"] != "")
		{
			linhaCabecalho("<b>Nome do Grupo Gerencial","{$POST["nomeFiltro"]}</b>");
		}elseif ($POST["nomeLotacao"] != "")
		{
			if ($POST["nomeHierarquia"] == "") $POST["nomeHierarquia"] = "Nome da Lota��o";
			linhaCabecalho("<b>{$POST["nomeHierarquia"]}","{$POST["nomeLotacao"]}</b>");
		}
		
		linhaCabecalho("<b>Ciclo","$nomeCiclo</b>");
	?>


	<tr class="textblk">

		<td width="100%" colspan="<?php echo $colSpan + 3; ?>">

			<b>Per�odo:&nbsp; <?php

												if($dataInicioTexto!=""){

													echo "de $dataInicioTexto ";

													if($dataTerminoTexto!=""){

														echo "a $dataTerminoTexto";

													}

												}else{

													echo "Todos ";

												}

							?>

		</b></td>

	</tr>

	<?php

	

		$totalUsuariosEmpresa = obterTotalUsuarios($codigoEmpresa);

		$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);


?>

	

</table>



<table cellpadding="1" cellspacing="0" style="border: 1px solid black;" align="center" width="95%">


	<tr class='textblk'>

		<td class="bdLat" colspan="2">

			<b>Ades�o</b>

		</td>
		
	
<?php


	foreach ($ciclos as $ciclo)
	{
		
		echo "<td class='bdLat' width='16%' align='center'>

			&nbsp;$ciclo&nbsp;

		</td>";
		
	}

?>

		<td align="center" width="10%">

			Total

		</td>

	</tr>



<?php
	
	$totalUsuariosAcumuladoCiclo = obterTotaisUsuariosAcumuladoCiclo();
	
	$totalAcumulado = 0;
	$totalNaoParticipantes = 0;
	$totalCadastrado = 0;
	$totalParticipacao = 0;
	
	$trCadastrados = "";
	$trParticipantes = "";
	$trNaoParticipantes = "";
	$trParticipacao = "";
	
	foreach ($ciclos as $ciclo)
	{

		if ($totalUsuariosAcumuladoCiclo[$ciclo]["QD_PARTICIPANTE"] == null)
			$totalUsuariosAcumuladoCiclo[$ciclo]["QD_PARTICIPANTE"] = 0;
			
		if ($totalUsuariosAcumuladoCiclo[$ciclo]["QD_CADASTRADO"] == null)
			$totalUsuariosAcumuladoCiclo[$ciclo]["QD_CADASTRADO"] = 0;
			

		$totalAcumulado = $totalUsuariosAcumuladoCiclo[$ciclo]["QD_PARTICIPANTE"];
		$totalCadastrado = $totalUsuariosAcumuladoCiclo[$ciclo]["QD_CADASTRADO"];
		$totalNaoParticipantes = $totalCadastrado - $totalAcumulado;

		$trParticipantes = "$trParticipantes <td class='titRelat'>

			$totalAcumulado

		</td>";
			
		$trCadastrados = "$trCadastrados <td class='titRelat'>

			$totalCadastrado

		</td>";
			
		$totalNaoParticipantes = $totalCadastrado - $totalParticipantesGrupo;
		
		$trNaoParticipantes = "$trNaoParticipantes
								<td class='titRelat'>
					
									$totalNaoParticipantes
					
								</td>";
									
		$totalParticipacao = $totalAcumulado/$totalCadastrado;
		$totalParticipacao = number_format(round($totalParticipacao, 2),2,",",".");
		
		$trParticipacao = "$trParticipacao
									<td class='titRelat'>
										$totalParticipacao
									</td>";


	}
	
	$trParticipantes = "$trParticipantes <td class='titRelatTop'>

				$totalParticipantesGrupo

			</td>";

	
				
	$trCadastrados = "$trCadastrados <td class='titRelatTop'>

			$totalCadastrado

		</td>";
	
	$trNaoParticipantes = "$trNaoParticipantes
									<td class='titRelatTop'>
						
										$totalNaoParticipantes
						
									</td>";

										
	$totalParticipacao = $totalParticipantesGrupo/$totalCadastrado;
	$totalParticipacao = number_format(round($totalParticipacao, 2),2,",",".");
	$trParticipacao = "$trParticipacao
									<td class='titRelatTop'>
										$totalParticipacao
									</td>";

	
?>	
	
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Cadastrados
		</td>
	
		<?php
			echo $trCadastrados;
			
		?>
	
	</tr>
	
	
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Participantes
		</td>
	
		<?php
			echo $trParticipantes;
			
		?>
	
	</tr>

	
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			N�o Participantes
		</td>
		
		<?php
			echo $trNaoParticipantes;
			
		?>
		
	</tr>
	
	<tr class='textblk'>
		<td colspan="2" class="titRelatD">
			Participantes/Cadastrados
		</td>
		
		<?php
			echo $trParticipacao;
		?>
		
	
	</tr>

	
	<tr>
		<td class='titRelatTop' colspan="<?php echo $colSpan + 3; ?>">&nbsp;</td>
	</tr>
		
	
	<tr class='textblk'>

		<td class="bdTop" width="100%" colspan="<?php echo $colSpan + 3; ?>">

			<b>Acessos</b>

		</td>

	</tr>
	<tr>

<?php



	$totalizadorDias = array();

	$totalGeral = 0;

	$acessoUsuarios = array();
	
	$acessosCiclo = array();

	$sqlTotalCiclo = "SELECT

			  c.NM_CICLO,

			  COUNT(DISTINCT DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'), a.CD_USUARIO) AS QD_TOTAL

			FROM

			  col_acesso a

			  INNER JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
			  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
			  INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND DATE_FORMAT(c.DT_INICIO,'%Y%m%d') <= DATE_FORMAT(DT_ACESSO,'%Y%m%d') AND DATE_FORMAT(DT_TERMINO,'%Y%m%d') >= DATE_FORMAT(DT_ACESSO,'%Y%m%d')
			WHERE

			  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)

			  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')

			  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')

			  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))

			  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')

			  GROUP BY

			  	c.NM_CICLO";

	

	$resultadoTotalCiclo = DaoEngine::getInstance()->executeQuery($sqlTotalCiclo,true);

	while ($linha = mysql_fetch_row($resultadoTotalCiclo)) {

			//echo "aaa";

		$acessosCiclo[$linha[0]] = $linha[1];

	}


echo "<tr class='textblk'>

			<td class='titRelatD' colspan='2'>

				Total

			</td>

			";

		$totalGeralTodos = 0;

		foreach ($ciclos as $ciclo)
		{


			if ($acessosCiclo[$ciclo] == null)
				$acessosCiclo[$ciclo] = 0;
				

			$totalGeralTodos = $totalGeralTodos + $acessosCiclo[$ciclo];
			echo "<td class='titRelat'>

					{$acessosCiclo[$ciclo] }

			</td>";

		}

	



	

	echo "<td class='titRelatTop'>

			$totalGeralTodos

		</td>";

	

	echo "</tr>";
	
	
	$nomesPaginas = obterNomePaginas();
	
	$acessosPorCiclo = obterTotalAcessosCiclo();
	$excluiBoasVindas = true;
	
	foreach($nomesPaginas as $chave => $valor)
	{
		
		if ($excluiBoasVindas)
		{
			$excluiBoasVindas = false;
			continue;
		}
		
		$paginaAcesso = $valor;
		$indiceAcesso = $chave;
		
		echo "<tr class='textblk'>";

		echo "<td class='titRelatD' colspan='2'>$paginaAcesso</td>";

		$totalPagina = 0;
		
		
		foreach ($ciclos as $ciclo)
		{

			$indice = "$indiceAcesso$ciclo";
			
			$acessosPagina = 0;
			
			if ($acessosPorCiclo[$indice] != null){

				$acessosPagina = $acessosPorCiclo[$indice];

			}

			$totalPagina = $totalPagina + $acessosPagina;

			echo "<td class='titRelat'>

					$acessosPagina

				</td>";
		
		}
		
			echo "<td class='titRelatTop'>

				$totalPagina

			</td>";	

	echo "</tr>";
	}

?>

	<tr>
		<td class='titRelatTop' colspan="<?php echo $colSpan + 3; ?>">&nbsp;</td>
	</tr>

	<tr class='textblk'>

		<td class="bdTop" width="100%" colspan="<?php echo $colSpan + 3; ?>">

			<b><?php echo $labelAvaliacao; ?></b>

		</td>

	</tr>

	
	
	


<?php

	$mediaGeralProvasCalculada = 0;

	$mediaProvas = obterProvasMediaCiclo();
	$mediaGeralProvas = obterProvasMediasGeral();
	
	
	
	$quantidadeTotalAvaliacoes = 0;
	$quantidadeTotalAvaliacoeAcimaSete = 0;
	
	$trAvaliacoes = "";
	$trQuantidadeAvaliacoes = "";
	$trQuantidadeAvaliacoesAcimaSete = "";
	
	foreach ($ciclos as $ciclo)
	{


		$indiceAvaliacao = $ciclo;

		if ($mediaProvas[$indiceAvaliacao] != null)
		{
			$quantidadeTotalAvaliacoes = $quantidadeTotalAvaliacoes + $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"];
			$quantidadeTotalAvaliacoeAcimaSete = $quantidadeTotalAvaliacoeAcimaSete + $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"];
			
		}
		
		if ($mediaProvas[$indiceAvaliacao] == null)
		{
			$mediaProvas[$indiceAvaliacao]["VL_MEDIA"] = "-";
			$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"] = "-";
			$percentualAprovacao = "-";
		}
		else
		{
			$percentualAprovacao = $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO_SETE"] / $mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"];
			$percentualAprovacao = number_format(round($percentualAprovacao, 2),2,",",".");
		}
		
		$trQuantidadeAvaliacoes = "$trQuantidadeAvaliacoes <td class='titRelat'>

				{$mediaProvas[$indiceAvaliacao]["QTD_AVALIACAO"]}
	
			</td>";
			
		$trQuantidadeAvaliacoesAcimaSete = "$trQuantidadeAvaliacoesAcimaSete <td class='titRelat'>
	
				$percentualAprovacao
	
			</td>";
				
		$trAvaliacoes = "$trAvaliacoes <td class='titRelat'>
	
				{$mediaProvas[$indiceAvaliacao]["VL_MEDIA"]}
	
			</td>";

	}
	
	//TODO: Acertar para pegar os valores corretamente
	if ($mediaGeralProvas["1"] == null)
		$mediaGeralProvasCalculada ="-"; //$mediaGeralProvas["1"] = "-";
		

	$trAvaliacoes = "$trAvaliacoes <td class='titRelatTop'>

		{$mediaGeralProvasCalculada}

			</td>";
		
	$trQuantidadeAvaliacoes = "$trQuantidadeAvaliacoes <td class='titRelatTop'>

		$quantidadeTotalAvaliacoes

			</td>";
	
	$percentualAprovacao = $quantidadeTotalAvaliacoeAcimaSete / $quantidadeTotalAvaliacoes;
	$percentualAprovacao = number_format(round($percentualAprovacao, 2),2,",",".");
		
	$trQuantidadeAvaliacoesAcimaSete = "$trQuantidadeAvaliacoesAcimaSete <td class='titRelatTop'>

		$percentualAprovacao

			</td>";	
	

?>
	
		<tr class="textblk">
		<td colspan="2" class="titRelatD">
			Qtde de <?php echo $labelAvaliacao; ?>
		</td>	

	<?php echo $trQuantidadeAvaliacoes;?>
	
	</tr>
	
	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			% Aprova��o
		</td>	

	<?php echo $trQuantidadeAvaliacoesAcimaSete;?>
	
	</tr>


	<tr class="textblk">
		<td colspan="2" class="titRelatD">
			M�dia das <?php echo $labelAvaliacao; ?>
		</td>	

	<?php echo $trAvaliacoes;?>
	
	</tr>
	
	

</table>



	<?php

		if ($_POST["btnExcel"] == "") {

	?>

	<div style="width:100%;text-align:center">

		<br />

		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']; ?>">

		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">

		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">

		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>

		</form>	

	</div>

	<?php

		}

	?>

	

</body>

</html>



<?php



	ob_end_flush();



	function obterTextoMes($numero)

	{

		switch ($numero)

		{

			case 1:

				return "Janeiro";

				break;

			case 2:

				return "Fevereiro";

				break;

			case 3:

				return "Mar�o";

				break;

			case 4:

				return "Abril";

				break;

			case 5:

				return "Maio";

				break;

			case 6:

				return "Junho";

				break;

			case 7:

				return "Julho";

				break;

			case 8:

				return "Agosto";

				break;

			case 9:

				return "Setembro";

				break;

			case 10:

				return "Outubro";

				break;

			case 11:

				return "Novembro";

				break;

			case 12:

				return "Dezembro";

				break;

		}

	}

	

	

	function obterTextoDiaSemana($numero)

	{



		switch ($numero) {

			case 0:

				return 'dom';

				break;

			case 1:

				return 'seg';

				break;

			case 2:

				return 'ter';

				break;

			case 3:

				return 'qua';

				break;

			case 4:

				return 'qui';

				break;

			case 5:

				return 'sex';

				break;

			case 6:

				return 'sab';

				break;



		}

		

	}



	function linhaCabecalho($titulo, $valor)

	{

		

		global $colSpan;

		

		$colunasCabecalhoTitulo = "";

		$colunasCabecalhoValor = "";

		

		if ($_POST["btnExcel"] != "")

		{

			$colunasCabecalhoValor = $colSpan + 2;

			$colunasCabecalhoValor = "</td><td align=\"left\" width=\"100%\" colspan=\"$colunasCabecalhoValor\">" ;

		}

		else 

		{

			$colunasCabecalhoTitulo = $colSpan + 3;

			$colunasCabecalhoTitulo = "colspan=\"$colunasCabecalhoTitulo\"";

		}

		

		

		echo "<tr class=\"textblk\">

				<td $colunasCabecalhoTitulo>

					$titulo:

				$colunasCabecalhoValor

					$valor

				</td>

			</tr>";

		

	}

		

?>