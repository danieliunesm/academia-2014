<?php

//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');

$tituloTotal = "";
$tituloRelatorio = "";

$POST = obterPost();

//Obter Parametros
$acessoUnico = $_REQUEST["hdnTipoRelatorio"];

$codigoEmpresa = $POST['cboEmpresa'];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$tipoAcessoContado = "";
if ($acessoUnico == "H"){
	$tipoAcessoContado = "COUNT(1)";
	$tituloTotal = "Total de &nbsp;Acessos&nbsp;de&nbsp;Usu�rios&nbsp; no Per�odo&nbsp;";
	
}
elseif ($acessoUnico == "D")
{
	$tipoAcessoContado = "COUNT(DISTINCT(u.CD_USUARIO))";
	$tituloTotal = "Total&nbsp;de Participantes";
	
	if ($_GET["cboEmpresa"])
	{
		$tituloRelatorio = "Radar de Participantes da Empresa";
	}
	else
	{
		$tituloRelatorio = "Radar de Participantes do Grupo: {$POST["nomeFiltro"]}";
	}
	
}
elseif ($acessoUnico == "S")
{
	$tipoAcessoContado = "COUNT(DISTINCT DS_ID_SESSAO, u.CD_USUARIO)";
	$tituloTotal = "Total de Acessos&nbsp;no&nbsp;Per�odo";
	
	if ($_GET["cboEmpresa"])
	{
		$tituloRelatorio = "Radar de Acessos da Empresa";
	}
	else
	{
		$tituloRelatorio = "Radar de Acessos do Grupo: {$POST["nomeFiltro"]}";
	}
	
}

$dataAtual = $dataInicial;

// Fim obter parametros


//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa

//Acertar as p�ginas a serem exibidas
$paginaRn = new col_pagina_acesso();
$paginaRn->prepararRelatorios($dataInicial, $dataFinal, $codigoEmpresa);


//Obter as p�ginas a serem exibidas
$sql = "SELECT CD_PAGINA_ACESSO, DS_PAGINA_ACESSO FROM col_pagina_acesso WHERE NOT IN_ORDENAR = -1 AND NOT IN_ORDENAR = 999 ORDER BY IN_ORDENAR";
$colunas = DaoEngine::getInstance()->executeQuery($sql,true);
$qtdColunas = mysql_num_rows($colunas);
$codigosPaginas = array();


//Obter os acessos di�rios por p�gina
$sql = "SELECT
			  CD_PAGINA_ACESSO, DATE_FORMAT(DT_ACESSO, '%Y%m%d') AS DT_ACESSO,
			  $tipoAcessoContado AS QD_TOTAL
			FROM
			  col_acesso a
			  INNER JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
			WHERE
			  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
			  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
			  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
			GROUP BY
			  CD_PAGINA_ACESSO, DATE_FORMAT(DT_ACESSO, '%Y%m%d')";

//				  OR (a.CD_PAGINA_ACESSO = 10)		

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

$resultados = array();
	
while ($linha = mysql_fetch_array($resultado))
{
	$indice = "{$linha["CD_PAGINA_ACESSO"]}{$linha["DT_ACESSO"]}";
	$resultados[$indice] = $linha["QD_TOTAL"];
}

if ($_POST["btnExcel"] != "") {
	header("Content-Type: application/vnd.ms-excel; name='excel'");
	header("Content-disposition:  attachment; filename=RelatorioConsolidadoAcesso.xls");
}


$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

//Obter o total de participantes da empresa
$sqlTotalGeral = "SELECT
		  $tipoAcessoContado AS QD_TOTAL
		FROM
		  col_acesso a
		  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
		WHERE
		  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
		  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";

$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);
$linha = mysql_fetch_row($resultadoTotalGeral);
$totalGeralEmpresa = $linha[0];

//Obter o total de participantes do filtro
$sqlTotalGeral = "SELECT
		  $tipoAcessoContado AS QD_TOTAL
		FROM
		  col_acesso a
		  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
		WHERE
		  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
		  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";

$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);
$linha = mysql_fetch_row($resultadoTotalGeral);
$totalGeralTodos = $linha[0];


$totalParticipantesEmpresa = 0;
$totalParticipantesGrupo = 0;
if ($acessoUnico == "S")
{
	
	//Obter o total de participantes da empresa
	$sqlTotalGeral = "SELECT
			  COUNT(DISTINCT(u.CD_USUARIO)) AS QD_TOTAL
			FROM
			  col_acesso a
			  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
			WHERE
			  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";
	
	$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);
	$linha = mysql_fetch_row($resultadoTotalGeral);
	$totalParticipantesEmpresa = $linha[0];
	
	//Obter o total de participantes do filtro
	$sqlTotalGeral = "SELECT
			  COUNT(DISTINCT(u.CD_USUARIO)) AS QD_TOTAL
			FROM
			  col_acesso a
			  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
			WHERE
			  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
			  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
			  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
			  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";
	
	$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);
	$linha = mysql_fetch_row($resultadoTotalGeral);
	$totalParticipantesGrupo = $linha[0];
	
}



ob_start();
	
?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	
	<?php
	
	if ($_POST["btnExcel"] == "") {

	?>
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">		
	<?php
	}
	?>
	
	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
	</style>
	
</head>

<body>

<br>

<?php

	$colSpan = 0;
	$colunasMaximo = 24;
	$colunasAparecer = 0;
	$colunasTotal = 0;
	
	if ($usaData)
	{

		$dataAtualOriginal = $dataAtual;
		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal))) {

			if ($colunasAparecer < $colunasMaximo)
			{
				$colSpan++;
				$colunasAparecer++;
			}
			
			$colunasTotal++;
			$dataAtual = somarUmDia($dataAtual);
		}
		
		
		$dataAtual = $dataAtualOriginal;
		
		
	}
	
	$colunaInicial = 1;
	if ($colunasTotal > $colunasAparecer)
	{
		$colunaInicial = $colunasTotal - $colunasAparecer + 1;
	}


?>


<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk'>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" colspan="<?php echo $colSpan + 1; ?>" align="center">
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<p><?php echo date("d/m/Y") ?></p>
		</td>
	</tr>
</table>


<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr class="textblk">
		<td width="100%" colspan="<?php echo $colSpan + 3; ?>">
			Per�odo:&nbsp; <?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
		</td>
	</tr>
	<?php
	
		$totalUsuariosEmpresa = obterTotalUsuarios($codigoEmpresa);
		$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);
	
	
		switch ($acessoUnico)
		{
			case "D":
				linhaCabecalho("Cadastrados da Empresa", $totalUsuariosEmpresa);
				linhaCabecalho("Participantes da Empresa", $totalGeralEmpresa);
				$percentualParticipacaoEmpresa = str_ireplace(".",",",round($totalGeralEmpresa/$totalUsuariosEmpresa,2));
				linhaCabecalho("Participantes/Cadastrados da Empresa", $percentualParticipacaoEmpresa);
				if (!isset($_GET["cboEmpresa"]))
				{
					linhaCabecalho("Cadastrados do Grupo", $totalUsuarioGrupo);
					linhaCabecalho("Participantes do Grupo", $totalGeralTodos);
					$percentualParticipacaoGrupo = str_ireplace(".",",",round($totalGeralTodos/$totalUsuarioGrupo,2));
					linhaCabecalho("Participantes/Cadastrados do Grupo", "$percentualParticipacaoGrupo");
				}
				break;
			case "S":
				linhaCabecalho("Cadastrados na Empresa", $totalUsuariosEmpresa);
				linhaCabecalho("Participantes da Empresa", $totalParticipantesEmpresa);
				linhaCabecalho("Acessos de Participantes da Empresa", $totalGeralEmpresa);
				$percentualParticipacaoEmpresa = str_ireplace(".",",",round($totalGeralEmpresa/$totalParticipantesEmpresa,2));
				linhaCabecalho("Acessos de Participantes/Participantes da Empresa", $percentualParticipacaoEmpresa);
				if (!isset($_GET["cboEmpresa"]))
				{
					linhaCabecalho("Cadastrados do Grupo", $totalUsuarioGrupo);
					linhaCabecalho("Participantes do Grupo", $totalParticipantesGrupo);
					linhaCabecalho("Acessos de Participantes do Grupo", $totalGeralTodos);
					$percentualParticipacaoGrupo = str_ireplace(".",",",round($totalGeralTodos/$totalParticipantesGrupo,2));
					linhaCabecalho("Acessos de Participantes do Grupo/Participantes do Grupo", "$percentualParticipacaoGrupo");
				}
				
				break;
			
		}

	?>
	
</table>

<table cellpadding="1" cellspacing="0" style="border: 1px solid black;" align="center" width="95%">
	<tr class='textblk'>
		<td  rowspan="2" class="bdLat" width="100%" colspan="2">
			Funcionalidades
		</td>

<?php

	$dataAtualOriginal = $dataAtual;

	$trDiasMeses = "";
	if ($usaData)
	{
		
		$meses = array();
		$contador = 1;
		
		//Define os meses envolvidos e a quantidade de dias de cada um at� a data limite
		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal))) {

			if (!($colunaInicial > $contador))
			{
			
				$aData = split("/", $dataAtual);
				//$dia = date("w", strtotime("{$aData[1]}/{$aData[0]}/{$aData[2]}"));
				
				$mesAtual = $aData[1];
				
				if ($meses[$mesAtual] == null)
				{
					$meses[$mesAtual] = 0;
				}
				
				$meses[$mesAtual] = $meses[$mesAtual] + 1;
				
				$trDiasMeses =  "$trDiasMeses
									<td class='titRelat' >
										{$aData[0]}
									</td>";
			}
						
			$dataAtual = somarUmDia($dataAtual);
			
			$contador++;
			
		}
	
	
		foreach($meses as $chave => $valor)
		{
			$nomeMes = obterTextoMes($chave);
			echo "	<td class='bdLat' colspan=\"$valor\" align=\"center\">
						$nomeMes
					</td>";

		}
	
	}
	
?>
		<td align="center" rowspan="2">
			<?php echo $tituloTotal; ?>
		</td>
	</tr>
	<tr class='textblk'>
		
<?php
	if ($usaData)
	{
		//Percorre os dias at� a data limite
		echo $trDiasMeses;
		
	}

?>
		
	</tr>
	
<?php

	$totalizadorDias = array();
	$totalGeral = 0;
	$maximoGeral = 0;
	$mediaGeral = 0;
	
	$acessoUsuarios = array();
	
	/*if ($acessoUnico == "D")
	{
		
		$sql = "SELECT
					  CD_PAGINA_ACESSO,
					  $tipoAcessoContado AS QD_TOTAL
					FROM
					  col_acesso a
					  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
					WHERE
					  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
					  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
					  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
					  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
					GROUP BY
					  CD_PAGINA_ACESSO";
		
		$resultadoUsuario = DaoEngine::getInstance()->executeQuery($sql,true);
		while ($linha = mysql_fetch_row($resultadoUsuario)) {
			//echo "aaa";
			$acessoUsuarios[$linha[0]] = $linha[1];
		}
		
	}*/
	
	$acessosFuncionalidades = array();

	//Obter o total de participantes por p�gina
	$sql = "SELECT
			  CD_PAGINA_ACESSO, $tipoAcessoContado AS QD_TOTAL
			FROM
			  col_acesso a
			  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
			WHERE
			  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
			  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
			  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
			  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
			GROUP BY
			  CD_PAGINA_ACESSO";
	
	$resultadoFuncionalidades = DaoEngine::getInstance()->executeQuery($sql,true);
	while ($linha = mysql_fetch_row($resultadoFuncionalidades)) {
			//echo "aaa";
		$acessosFuncionalidades[$linha[0]] = $linha[1];
	}
	
	

	
	$acessosDia = array();
	
	$sqlTotalDia = "SELECT
			  DATE_FORMAT(DT_ACESSO, '%Y%m%d') AS DT_ACESSO,
			  $tipoAcessoContado AS QD_TOTAL
			FROM
			  col_acesso a
			  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO
			WHERE
			  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
			  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
			  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
			  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
			  GROUP BY
			  	DATE_FORMAT(DT_ACESSO, '%Y%m%d')";
	
	$resultadoTotalDiario = DaoEngine::getInstance()->executeQuery($sqlTotalDia,true);
	while ($linha = mysql_fetch_row($resultadoTotalDiario)) {
			//echo "aaa";
		$acessosDia[$linha[0]] = $linha[1];
	}

	while ($linha = mysql_fetch_array($colunas)) {
		
		$paginaAcesso = $linha["DS_PAGINA_ACESSO"];
		$paginaAcesso = str_ireplace("Download ", "", $paginaAcesso);
		
		if ($linha["DS_PAGINA_ACESSO"] != $paginaAcesso)
		{
			$paginaAcesso = str_ireplace("/docs/", "", $paginaAcesso);
			
			$tamanho = strlen($paginaAcesso);
			
			for ($x = 0; $x < $tamanho; $x++)
			{
				if (is_numeric($paginaAcesso{$x}))
				{
					$paginaAcesso = substr($paginaAcesso, 0, $x);
					break;
				}
			}

		}
		
		echo "<tr class='textblk'>";
			echo "<td class='titRelatD' colspan='2'>$paginaAcesso</td>";

		$dataAtual = $dataAtualOriginal;
		
		$quantidadesPagina = $resultados;
		$maximoPagina = 0;
		$totalAcessosPagina = 0;
		$diasTotais = 0;
		$contador = 1;
		//Define os meses envolvidos e a quantidade de dias de cada um at� a data limite
		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal))) {

			$aData = split("/", $dataAtual);

			$indice = "{$linha["CD_PAGINA_ACESSO"]}{$aData[2]}{$aData[1]}{$aData[0]}";
			$acessosPagina = 0;
			
			if ($quantidadesPagina[$indice] != null){
				$acessosPagina = $quantidadesPagina[$indice];
			}
			
			$maximoPagina = max($maximoPagina, $acessosPagina);
			
			$totalAcessosPagina = $totalAcessosPagina + $acessosPagina;
			
			if (!($colunaInicial > $contador))
			{
				if ($usaData)
				{
					echo "<td class='titRelat'>
							$acessosPagina
						</td>";
				}
			}
			$dataAtual = somarUmDia($dataAtual);
			
			if ($totalizadorDias[$diasTotais] == null)
			{
				$totalizadorDias[$diasTotais] = 0;
			}
			
			$totalizadorDias[$diasTotais] = $totalizadorDias[$diasTotais] + $acessosPagina;
			
			$diasTotais++;
			$contador++;
		}
		
		$media = $media = round($totalAcessosPagina / $diasTotais);
		
		/*if ($acessoUnico == "D")
		{
			if ($acessoUsuarios[$linha["CD_PAGINA_ACESSO"]] == null)
			{
				$totalAcessosPagina = 0;
			}
			else 
			{
		*/		$totalAcessosPagina = $acessosFuncionalidades[$linha["CD_PAGINA_ACESSO"]];	
			/*}
			
		}*/
		
		$totalGeral = $totalGeral + $totalAcessosPagina;
		$mediaGeral = $mediaGeral + $media;
		$maximoGeral = $maximoGeral + $maximoPagina;
		
		if ($totalAcessosPagina == "")
			$totalAcessosPagina = 0;
		
		echo "<td class='titRelatTop'>
				$totalAcessosPagina
			</td>";
		echo "</tr>";
		
	}
	
	echo "<tr class='textblk'>
			<td class='titRelatD' colspan='2'>
				$tituloTotal
			</td>
			";
	
	if ($usaData)
	{
		
		$contador = 1;
		$dataAtual = $dataAtualOriginal;
		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))
		{
			if (!($colunaInicial > $contador))
			{
				if ($acessosDia[conveterDataParaYMD($dataAtual)] == null)
					$acessosDia[conveterDataParaYMD($dataAtual)] = 0;
				
				echo "<td class='titRelat'>
						{$acessosDia[conveterDataParaYMD($dataAtual)]}
				</td>";
			}
			$contador++;			
			$dataAtual = somarUmDia($dataAtual);
		}
		
		//for ($diaAtual = 0; $diaAtual < $diasTotais; $diaAtual++)
		//{
		//	echo "<td class='titRelat'>
		//			{$acessosDia[$diaAtual]}
		//		  </td>";
		//}
	}
	
	if ($totalGeralTodos == "")
		$totalGeralTodos = 0;
	
	echo "<td class='titRelatTop'>
			$totalGeralTodos
		</td>";
	
	echo "</tr>";

?>
	
</table>

	<?php
		if ($_POST["btnExcel"] == "") {
	?>
	<div style="width:100%;text-align:center">
		<br />
		<form method="POST">
		
			<?php
				escreveHiddensPost($POST);
				//<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">
			?>
		
		
		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
		</form>	
	</div>
	<?php
		}
	?>
	
</body>
</html>

<?php

	ob_end_flush();

	function obterTextoMes($numero)
	{
		switch ($numero)
		{
			case 1:
				return "Janeiro";
				break;
			case 2:
				return "Fevereiro";
				break;
			case 3:
				return "Mar�o";
				break;
			case 4:
				return "Abril";
				break;
			case 5:
				return "Maio";
				break;
			case 6:
				return "Junho";
				break;
			case 7:
				return "Julho";
				break;
			case 8:
				return "Agosto";
				break;
			case 9:
				return "Setembro";
				break;
			case 10:
				return "Outubro";
				break;
			case 11:
				return "Novembro";
				break;
			case 12:
				return "Dezembro";
				break;
		}
	}
	
	
	function obterTextoDiaSemana($numero)
	{

		switch ($numero) {
			case 0:
				return 'dom';
				break;
			case 1:
				return 'seg';
				break;
			case 2:
				return 'ter';
				break;
			case 3:
				return 'qua';
				break;
			case 4:
				return 'qui';
				break;
			case 5:
				return 'sex';
				break;
			case 6:
				return 'sab';
				break;

		}
		
	}

	function conveterDataParaYMD($data)
	{
		$aData = split("/", $data);
		$data = "{$aData[2]}{$aData[1]}{$aData[0]}";
		
		return $data;
		
	}
	
	
	
	function somarUmDia($data){
		$aData = split("/", $data);
		$data = date("d/m/Y", strtotime("{$aData[1]}/{$aData[0]}/{$aData[2]}") + 90000);
		
		return  $data;
	}
	
	function linhaCabecalho($titulo, $valor)
	{
		
		global $colSpan;
		
		$colunasCabecalhoTitulo = "";
		$colunasCabecalhoValor = "";
		
		if ($_POST["btnExcel"] != "")
		{
			$colunasCabecalhoValor = $colSpan + 2;
			$colunasCabecalhoValor = "</td><td align=\"left\" width=\"100%\" colspan=\"$colunasCabecalhoValor\">" ;
		}
		else 
		{
			$colunasCabecalhoTitulo = $colSpan + 3;
			$colunasCabecalhoTitulo = "colspan=\"$colunasCabecalhoTitulo\"";
		}
		
		
		echo "<tr class=\"textblk\">
				<td $colunasCabecalhoTitulo>
					$titulo:
				$colunasCabecalhoValor
					$valor
				</td>
			</tr>";
		
	}
		
?>