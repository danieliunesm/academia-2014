<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1); 
//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include ('relatorio_ranking_util.php');

$assessment=true;

$rowSpanCabecalho = 12;

if ($assessment){
	$rowSpanCabecalho = 3;
}

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

$tituloTotal = "";
$tituloRelatorio = "Gest�o";

$POST = obterPost();

//Obter Parametros
$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];

$codigoEmpresa = $POST['cboEmpresa'];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$hierarquia = isset($_REQUEST["hdnHierarquico"])? $_REQUEST["hdnHierarquico"]: 0; 
$codigoCiclo = $_REQUEST["hdnCiclo"];
$localidade = $_REQUEST['cboLocalidade'];

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$dataAtual = $dataInicial;

// Fim obter parametros
$campoSelect = "DS_LOTACAO";
$tituloCampo = "Lota��o";
$tabelaExtra = "";
$whereExtra = "";
$tabelaCiclo = "";
$complementoNotaAvaliacaoSelect = "";
$complementoNotaSimuladoSelect = "";

$complementoTitulo = "";
if ($_GET["cboEmpresa"])
{
	$complementoTitulo = "da Empresa";
}
else 
{
	$complementoTitulo = "do Grupo: {$POST["nomeFiltro"]}";
}



switch ($tipoRelatorio)
{
	case "L":
		$campoSelect = "DS_LOTACAO";
		$tituloCampo = "Lota��o";
		$tituloRelatorio = "Desempenho de Lota��es";
		break;
	case "U":
		$campoSelect = "login, NM_USUARIO, NM_SOBRENOME, DS_LOTACAO, cargofuncao";
		$tituloCampo = "Participante";
		$tituloRelatorio = "Desempenho de Participantes";
		
		if ($codigoCiclo == -1)
		{

			$sql = "SELECT COUNT(1) as QT_CICLO FROM col_ciclo WHERE CD_EMPRESA = $codigoEmpresa";
			$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
			$linhaCiclo = mysql_fetch_array($resultado);
			$quantidadeCiclo = $linhaCiclo["QT_CICLO"];
			
			if (!($quantidadeCiclo > 0))
				$quantidadeCiclo = 1;
		
			$tabelaCiclo = " LEFT JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND DATE_FORMAT(c.DT_INICIO, '%Y%m%d') <= DATE_FORMAT(pa.DT_INICIO, '%Y%m%d')
	            			AND DATE_FORMAT(c.DT_TERMINO, '%Y%m%d') >= DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') ";
			
			$complementoNotaAvaliacaoSelect = " * COUNT(DISTINCT IF(p.IN_PROVA = 1 AND pa.VL_MEDIA IS NOT NULL, c.CD_CICLO, NULL)) / $quantidadeCiclo ";
			$complementoNotaSimuladoSelect = " * COUNT(DISTINCT IF(p.IN_PROVA = 0 AND pa.VL_MEDIA IS NOT NULL, c.CD_CICLO, NULL)) / $quantidadeCiclo ";
		
		}
		
		
		break;
	case "G":
		$campoSelect = "NM_FILTRO";
		$tituloCampo = "Grupo";
		$tituloRelatorio = "Desempenho de Grupos Gerenciais";
		$tabelaExtra = "INNER JOIN col_filtro_lotacao filtro_lotacao ON filtro_lotacao.CD_LOTACAO = l.CD_LOTACAO
  						INNER JOIN col_filtro filtro ON filtro.CD_FILTRO = filtro_lotacao.CD_FILTRO";
		$whereExtra = "AND filtro.IN_FILTRO_RANKING = 1 AND filtro.IN_ATIVO = 1";
		break;
}

//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa

$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

$quantidadeCiclo = 1;
$nomeCiclo = "Todos os Ciclos";
if ($codigoCiclo != -1)
{
	//Obter o nome do Ciclo
	$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaCiclo = mysql_fetch_array($resultado);
	$nomeCiclo = $linhaCiclo["NM_CICLO"];
	//Fim obter o nome do Ciclo
}
else
{
	$sql = "SELECT COUNT(1) as TOTAL FROM col_ciclo WHERE CD_EMPRESA = $codigoEmpresa AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaCiclo = mysql_fetch_array($resultado);
	$quantidadeCiclo = $linhaCiclo["TOTAL"];
}

$totalUsuariosEmpresa = obterTotalUsuariosGrupamento($codigoEmpresa, -1, -1, -1, $dataFinalQuery);
$quantidadeCadastradosEmpresa = $totalUsuariosEmpresa;
$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataFinalQuery);

$sqlSuperusuario = ' AND (u.tipo > 0 AND u.tipo < 4) ';
//$sqlWhere = $sqlSuperusuario;


$whereExtra = "$whereExtra $sqlSuperusuario ";

$codigoFiltroUsuario = $_REQUEST["id"];

$quantidadeCiclosAvaliacoes = 1;
if ($quantidadeCiclo > 1)
	$quantidadeCiclosAvaliacoes = obterQuantidadeCiclosAvaliacoes();

if (!(ehEmpresa()))
{
	$fatoresGrupo = "";

	if (ehLotacao()) // Obtem os fatores da lota��o
	{
		$codigoLotacao = $POST["cboLotacao"][0];
		$fatoresGrupo = obterFatoresLotacao($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoLotacao, $quantidadeCiclosAvaliacoes);
	}
	elseif (ehFiltro() > 0)
	{
		$fatoresGrupo = obterFatoresGrupo($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoFiltroUsuario, $quantidadeCiclosAvaliacoes);
	}

	//$fatoresGrupo = obterFatoresGrupo($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoFiltroUsuario);
	
	$fatorGrupoMediaAvaliacaoOriginal = $fatoresGrupo["VALOR_MEDIA_AVALIACAO"] * 10;
	$fatorGrupoMediaAvaliacao = tratarDividirZero($fatoresGrupo["VALOR_MEDIA_AVALIACAO"] * 10);
	
	$fatorGrupoMediaAssessmentOriginal = $fatoresGrupo["VALOR_MEDIA_ASSESSMENT"] * 10;
	$fatorGrupoParticipacaoAssessmentOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_DIAGNOSTIVO"];
	
	$fatorGrupoParticipacaoAvaliacaoOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_AVALIACAO"];
	$fatorGrupoParticipacaoAvaliacao = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_AVALIACAO"]);
	
	$fatorGrupoAcessoOriginal = $fatoresGrupo["VALOR_ACESSO"];
	$fatorGrupoAcesso = tratarDividirZero($fatoresGrupo["VALOR_ACESSO"]);
	
	$fatorGrupoDownloadOriginal = $fatoresGrupo["VALOR_DOWNLOAD"];
	$fatorGrupoDownload = tratarDividirZero($fatoresGrupo["VALOR_DOWNLOAD"]);
	
	$fatorGrupoParticipacaoSimuladoOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_SIMULADO"];
	$fatorGrupoParticipacaoSimulado = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_SIMULADO"]);
	
	$fatorGrupoRealizacaoSimuladoOriginal = $fatoresGrupo["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
	$fatorGrupoRealizacaoSimulado = tratarDividirZero($fatoresGrupo["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);
	
	$fatorGrupoParticipacaoForumOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_FORUM"];
	$fatorGrupoParticipacaoForum = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_FORUM"]);
	
	$fatorGrupoContribuicaoForumOriginal = $fatoresGrupo["VALOR_CONTRIBUICAO_FORUM"];
	$fatorGrupoContribuicaoForum = tratarDividirZero($fatoresGrupo["VALOR_CONTRIBUICAO_FORUM"]);

	
	
	$fatorGrupoParticipacaoOriginal = 0;
	$fatorGrupoParticipacao = tratarDividirZero(0);
	
	$fatorGrupoMediaSimuladoOriginal = 0;
	$fatorGrupoMediaSimulado = tratarDividirZero(0);
	

	
	//Fim fatores do grupo

}


$fatoresEmpresa = obterFatoresEmpresa($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $quantidadeCiclosAvaliacoes);

$fatorEmpresaMediaAvaliacaoOriginal = $fatoresEmpresa["VALOR_MEDIA_AVALIACAO"] * 10;
$fatorEmpresaMediaAvaliacao = tratarDividirZero($fatoresEmpresa["VALOR_MEDIA_AVALIACAO"] * 10);

$fatorEmpresaMediaAssessmentOriginal = $fatoresEmpresa["VALOR_MEDIA_ASSESSMENT"] * 10;
$fatorEmpresaParticipacaoAssessmentOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_DIAGNOSTIVO"];

$fatorEmpresaParticipacaoAvaliacaoOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_AVALIACAO"];
$fatorEmpresaParticipacaoAvaliacao = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO_AVALIACAO"]);

$fatorEmpresaAcessoOriginal = $fatoresEmpresa["VALOR_ACESSO"];
$fatorEmpresaAcesso = tratarDividirZero($fatoresEmpresa["VALOR_ACESSO"]);

$fatorEmpresaDownloadOriginal = $fatoresEmpresa["VALOR_DOWNLOAD"];
$fatorEmpresaDownload = tratarDividirZero($fatoresEmpresa["VALOR_DOWNLOAD"]);

$fatorEmpresaParticipacaoSimuladoOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_SIMULADO"];
$fatorEmpresaParticipacaoSimulado = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO_SIMULADO"]);

$fatorEmpresaRealizacaoSimuladoOriginal = $fatoresEmpresa["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
$fatorEmpresaRealizacaoSimulado = tratarDividirZero($fatoresEmpresa["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);


$fatorEmpresaParticipacaoOriginal = $fatoresEmpresa["FATOR_PARTICIPACAO"];
$fatorEmpresaParticipacao = tratarDividirZero($fatoresEmpresa["FATOR_PARTICIPACAO"]);

$fatorEmpresaParticipacaoForumOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_FORUM"];
$fatorEmpresaParticipacaoForum = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO_FORUM"]);

$fatorEmpresaContribuicaoForumOriginal = $fatoresEmpresa["VALOR_CONTRIBUICAO_FORUM"];
$fatorEmpresaContribuicaoForum = tratarDividirZero($fatoresEmpresa["VALOR_CONTRIBUICAO_FORUM"]);

$fatorEmpresaMediaSimuladoOriginal = 0;
$fatorEmpresaMediaSimulado = tratarDividirZero(0);

$fatorParticipacaoEmpresa = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO"]);

if (exibeTabela())
{

	
	$linhas = array();
	
	//obter pesos
	$sqlFarol = "SELECT * FROM col_farol WHERE CD_EMPRESA = $codigoEmpresa";
	
	//echo $sqlFarol;
	
	$resultadoFarol = DaoEngine::getInstance()->executeQuery($sqlFarol,true);
	
	$linhaFarol = mysql_fetch_array($resultadoFarol);
	
	$exibirParticipacao = $linhaFarol["IN_PESO_PARTICIPACAO"];
	$exibirAcesso = $linhaFarol["IN_PESO_ACESSO"];
	$exibirDownload = $linhaFarol["IN_PESO_DOWNLOAD"];
	$exibirParticipacaoAvaliacao = $linhaFarol["IN_PESO_PARTICIPACAO_AVALIACAO"];
	$exibirParticipacaoSimulado = $linhaFarol["IN_PESO_PARTICIPACAO_SIMULADO"];
	$exibirRealizacaoSimulado = $linhaFarol["IN_PESO_QUANTIDADE_SIMULADO"];
	$exibirMediaAvaliacao = $linhaFarol["IN_PESO_NOTA_AVALIACAO"];
	$exibirMediaSimulado = $linhaFarol["IN_PESO_NOTA_SIMULADO"];
	$exibirParticipacaoForum = $linhaFarol["IN_PESO_PARTICIPACAO_FORUM"];
	$exibirContribuicaoForum = $linhaFarol["IN_PESO_CONTRIBUICAO_FORUM"];
	
	$pesoParticipacao = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO"], $exibirParticipacao);
	$pesoAcesso = nullPara0Ranking($linhaFarol["NR_PESO_ACESSO"], $exibirAcesso);
	$pesoDownload = nullPara0Ranking($linhaFarol["NR_PESO_DOWNLOAD"], $exibirDownload);
	$pesoParticipacaoAvaliacao = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO_AVALIACAO"], $exibirParticipacaoAvaliacao);
	$pesoParticipacaoSimulado = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO_SIMULADO"], $exibirParticipacaoSimulado);
	$pesoRealizacaoSimulado = nullPara0Ranking($linhaFarol["NR_PESO_QUANTIDADE_SIMULADO"], $exibirRealizacaoSimulado);
	$pesoMediaAvaliacao = nullPara0Ranking($linhaFarol["NR_PESO_NOTA_AVALIACAO"], $exibirMediaAvaliacao);
	$pesoMediaSimulado = nullPara0Ranking($linhaFarol["NR_PESO_NOTA_SIMULADO"], $exibirMediaSimulado);
	$pesoParticipacaoForum = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO_FORUM"], $exibirParticipacaoForum);
	$pesoContribuicaoForum = nullPara0Ranking($linhaFarol["NR_PESO_CONTRIBUICAO_FORUM"], $exibirContribuicaoForum);
	
	$limitarResultado = $linhaFarol["NR_LIMITE"];
	
	$quantidadeRankeados = 0;
	$quantidadeRankeadosFiltro = 0;
	
	$quantidadeSuperusuarios = 0;
	$quantidadeParticipantesSuperusuario = 0;
	
	$quantidadeLotacoesCadastradas = 0;
	
	$fatoresParticipantes = obterFatoresParticipantes($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoLotacao, $cargo, $codigoUsuario, $tipoRelatorio, false, false, $quantidadeCiclosAvaliacoes);
	
	while ($linhaAcesso = mysql_fetch_array($fatoresParticipantes)) {
		//$memoria = memory_get_peak_usage(1);
		//$linhaAcesso = verificarRanking($linhaAcesso, "FATOR_PARTICIPACAO", 0);
		verificarRanking($linhaAcesso, "VALOR_ACESSO", 0);
		verificarRanking($linhaAcesso, "VALOR_DOWNLOAD", 0);
		verificarRanking($linhaAcesso, "VALOR_PARTICIPACAO_AVALIACAO", 0);
		verificarRanking($linhaAcesso, "VALOR_PARTICIPACAO_SIMULADO", 0);
		verificarRanking($linhaAcesso, "VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA", null);
		verificarRanking($linhaAcesso, "VALOR_MEDIA_AVALIACAO", null);
		//$linhaAcesso = verificarRanking($linhaAcesso, "MEDIA_SIMULADO", null);
		verificarRanking($linhaAcesso, "VALOR_PARTICIPACAO_FORUM", 0);
		verificarRanking($linhaAcesso, "VALOR_CONTRIBUICAO_FORUM", 0);
		
		
		//($linhaAcesso["FATOR_PARTICIPACAO"] * $pesoParticipacao) +
		// ($linhaAcesso["MEDIA_SIMULADO"] * $pesoMediaSimulado) +
		
		$pontos = (($linhaAcesso["VALOR_ACESSO"] / $fatorEmpresaAcesso) * $pesoAcesso) +
				  (($linhaAcesso["VALOR_DOWNLOAD"] / $fatorEmpresaDownload) * $pesoDownload) +
				  (($linhaAcesso["VALOR_PARTICIPACAO_AVALIACAO"] / $fatorEmpresaParticipacaoAvaliacao) * $pesoParticipacaoAvaliacao) +
				  (($linhaAcesso["VALOR_PARTICIPACAO_SIMULADO"] /$fatorEmpresaParticipacaoSimulado) * $pesoParticipacaoSimulado) +
				  (($linhaAcesso["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"] / $fatorEmpresaRealizacaoSimulado) * $pesoRealizacaoSimulado) +
				  (($linhaAcesso["VALOR_MEDIA_AVALIACAO"] * 10) * $pesoMediaAvaliacao) +
				  (($linhaAcesso["VALOR_PARTICIPACAO_FORUM"] / ($fatorEmpresaParticipacaoForum / $fatorParticipacaoEmpresa)) * $pesoParticipacaoForum) +
				  (($linhaAcesso["VALOR_CONTRIBUICAO_FORUM"] / ($fatorEmpresaContribuicaoForum / $fatorParticipacaoEmpresa)) * $pesoContribuicaoForum);
				  
		if (ehSuperusuario($linhaAcesso)) //Se for superusu�rio
		{
			if($linhaAcesso["FILTRADO"] == 1){
				$quantidadeSuperusuarios++;
				if ($linhaAcesso["VALOR_ACESSO"] > 0 || $pontos > 0 || $linhaAcesso["VALOR_PARTICIPACAO_AVALIACAO"] > 0) $quantidadeParticipantesSuperusuario++;
			}
			$pontos = -10000;
		}	
		
		
		$linhaAcesso["TOTAL_PONTUACAO"] = $pontos;
		
		$linhaAcesso["PARTICIPOU"] = $linhaAcesso["VALOR_ACESSO"] > 0 || $pontos > 0 || $linhaAcesso["VALOR_PARTICIPACAO_AVALIACAO"] > 0;
		
		if($linhaAcesso["PARTICIPOU"])
		{
			$quantidadeRankeados++;
		}
		elseif(!(ehSuperusuario($linhaAcesso)))
		{
			if ($linhaAcesso["ELEGIVEL"] == 1)
			{
				$linhaAcesso["TOTAL_PONTUACAO"] = -1;
			}	
			else 
			{
				$linhaAcesso["TOTAL_PONTUACAO"] = -5;
			}	
		}
		
		$linhas[] = $linhaAcesso;
		
		if($linhaAcesso["FILTRADO"] == 1)
		{
			$quantidadeLotacoesCadastradas++;
		}
		
		if($linhaAcesso["PARTICIPOU"] && $linhaAcesso["FILTRADO"] == 1)
			$quantidadeRankeadosFiltro++;
		
	}
	
	$quantidadeNaoRankeados = $totalUsuariosEmpresa - $quantidadeRankeados;
	
	if ($_REQUEST["id"] != "")
	{
		$quantidadeRankeados = $quantidadeRankeadosFiltro;
		$quantidadeNaoRankeados = $totalUsuarioGrupo - $quantidadeRankeados;
	}
	/*
	usort($linhas, "ordenarParticipacao");
	$linhas = ranquearItens($linhas, "FATOR_PARTICIPACAO","RANK_PARTICIPACAO");
	usort($linhas, "ordenarAcesso");
	$linhas = ranquearItens($linhas, "FATOR_ACESSO","RANK_ACESSO");
	usort($linhas, "ordenarDownload");
	$linhas = ranquearItens($linhas, "FATOR_DOWNLOAD","RANK_DOWNLOAD");
	usort($linhas, "ordenarParticipacaoAvaliacao");
	$linhas = ranquearItens($linhas, "FATOR_PARTICIPAO_AVALIACAO","RANK_PARTICIPAO_AVALIACAO");
	usort($linhas, "ordenarParticipacaoSimulado");
	$linhas = ranquearItens($linhas, "FATOR_PARTICIPAO_SIMULADO","RANK_PARTICIPAO_SIMULADO");
	usort($linhas, "ordenarRealizacaoSimulado");
	$linhas = ranquearItens($linhas, "FATOR_REALIZACAO_SIMULADO","RANK_REALIZACAO_SIMULADO");
	usort($linhas, "ordenarMediaAvaliacao");
	$linhas = ranquearItens($linhas, "MEDIA_AVALIACAO","RANK_MEDIA_AVALIACAO");
	usort($linhas, "ordenarMediaSimulado");
	$linhas = ranquearItens($linhas, "MEDIA_SIMULADO","RANK_MEDIA_SIMULADO");
	usort($linhas, "ordenarParticipacaoForum");
	$linhas = ranquearItens($linhas, "FATOR_PARTICIPACAO_FORUM","RANK_PARTICIPACAO_FORUM");
	usort($linhas, "ordenarContribuicaoForum");
	$linhas = ranquearItens($linhas, "FATOR_CONTRIBUICAO_FORUM","RANK_CONTRIBUICAO_FORUM");
	*/
	

	usort($linhas, "ordenarTotalGeral");
	//$posicoes = array();
	//$linhas = ranquearItens($linhas, "TOTAL_PONTUACAO","RANK_TOTAL_PONTUACAO");
	//$linhas = ranquearItens(&$linhas, "TOTAL_PONTUACAO","RANK_TOTAL_PONTUACAO");
	ranquearItens($linhas, "TOTAL_PONTUACAO","RANK_TOTAL_PONTUACAO");

}

function verificarRanking(&$linha, $campo, $valorSemAcesso, $substituirValor = 0)
{
	//$nomeCampoFlagAcesso = $campo . "_ACESSO";
	
	//$linha[$nomeCampoFlagAcesso] = true;
	
	if ($linha[$campo] == $valorSemAcesso)
	{
		$linha[$campo] = $substituirValor;
		//$linha[$nomeCampoFlagAcesso] = false;
	}
	
	//return $linha;
}

function ranquearItens(&$linhas, $campo, $campoRank)
{
	$posicaoAtual = 0;
	$ultimoValor = -1;
	
	//foreach ($linhas as $linha)
	for ($i=0; $i < count($linhas); $i++)
	{
		
		if ($linhas[$i][$campo] != $ultimoValor)
		{
			$posicaoAtual = $i + 1;
		}

		$linhas[$i][$campoRank] = $posicaoAtual;
		//$teste[] = $posicaoAtual;
		//echo $linhas[$i][$campoRank];
		
		$ultimoValor = $linhas[$i][$campo];
		
	}
	
	//return $linhas;
	
}

function ordenarParticipacao($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPACAO");
}

function ordenarAcesso($a, $b)
{
	return ordenarDados($a, $b, "FATOR_ACESSO");
}

function ordenarDownload($a, $b)
{
	return ordenarDados($a, $b, "FATOR_DOWNLOAD");
}

function ordenarParticipacaoAvaliacao($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPAO_AVALIACAO");
}

function ordenarParticipacaoSimulado($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPAO_SIMULADO");
}

function ordenarRealizacaoSimulado($a, $b)
{
	return ordenarDados($a, $b, "FATOR_REALIZACAO_SIMULADO");
}

function ordenarMediaAvaliacao($a, $b)
{
	return ordenarDados($a, $b, "MEDIA_AVALIACAO");
}

function ordenarMediaSimulado($a, $b)
{
	return ordenarDados($a, $b, "MEDIA_SIMULADO");
}

function ordenarParticipacaoForum($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPACAO_FORUM");
}

function ordenarContribuicaoForum($a, $b)
{
	return ordenarDados($a, $b, "FATOR_CONTRIBUICAO_FORUM");
}

function ordenarTotalGeral(&$a, &$b)
{
	return ordenarDados($a, $b, "TOTAL_PONTUACAO");
}

function ordenarDados($a, $b, $campo)
{
	if ($a[$campo] == $b[$campo])
		return 0;
		
	return ($a[$campo] > $b[$campo]) ? -1:+1;
}

function exibirGrupo()
{

	//global $tipoRelatorio;
	
	//return (($tipoRelatorio=="U" || $tipoRelatorio=="L") && (isset($_GET["id"]) && $_GET["id"] != ""));
	return (!(ehEmpresa()));

}

function tratarDividirZero($valor)
{
	if ($valor == 0)
	{
		return 1;
	}
	else 
	{
		return $valor;
	}
}

if ($_POST["btnExcel"] != "") {

	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=RelatorioDesempenho.xls");

}

?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	<?php

	

	if ($_POST["btnExcel"] == "") {



	?>

	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">		

	<?php

	}

	?>
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		
		.tabelaRanking td
		{
			padding: 1px 3px 1px 3px;
		}
	</style>
	
</head>
<body>
<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk' nowrap>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">

	<?php
	if (!(ehEmpresa()))
	{
	?>

	<tr class="textblk">
		<td>
			<b>
			<?php
				if (ehFiltro()){ 
					echo "Nome do Grupo Gerencial: {$POST["nomeFiltro"]}";
				}elseif (ehLotacao()) {
					$labelLotacao = "Nome da Lota��o";
					if ($POST["nomeHierarquia"] != "") $labelLotacao = $POST["nomeHierarquia"];
					echo "$labelLotacao: {$POST["nomeLotacao"]}";
				}
			?>
			</b>
		</td>
	</tr>
	<?php
	}
	?>
	<tr class="textblk">
		<td>
			<b>Ciclo: <?php echo $nomeCiclo; ?></b>
		</td>
	</tr>
	<tr class="textblk">
		<td width="100%">
			<b>Per�odo:&nbsp;
							<?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
		</b></td>
	</tr>
	
	<?php if($codigoLotacao > -1) {
	 ?>
	<tr class="textblk">
		<td width="100%">Grupo de Gest�o: <?php echo $POST["nomeLotacao"];?>
		</td>
	</tr>
	<?php 
	}
	?>
	<?php
	if (ehEmpresa())
	{
	?>
	<tr class="textblk">
		<td>
			<?php 
				if ($tipoRelatorio == "L")
					echo "Lota��es Cadastradas na Empresa: $quantidadeLotacoesCadastradas";
				else 
					echo "Cadastrados da Empresa: $totalUsuariosEmpresa";	
				
			?>
			
		</td>
	</tr>
	<?php
		
	}
	else
	{
	?>
	<tr class="textblk">
		<td>
			<?php 
				if ($tipoRelatorio == "L")
				{	
					if (ehFiltro())
					{
						echo "Lota��es Cadastradas no Grupo: $quantidadeLotacoesCadastradas";	
					}
					
				}
				elseif (ehFiltro() || ehLotacao())
				{ 
					
					$labelCadastrados = "Cadastrados no Grupo";
					if (ehLotacao())
					{
						$labelCadastrados = "Cadastrados no Grupo de Gest�o";
						if ($POST["nomeHierarquia"] != "") $labelCadastrados = "Cadastrados no(a) {$POST["nomeHierarquia"]}";
					}
					
					echo "$labelCadastrados: $totalUsuarioGrupo";
				}	
				
			?>
		</td>
	</tr>
	<?php
	}
	if ($codigoEmpresa == 34)
	{
	?>
	<tr class="textblk">
		<td>
			Cadastrados como Participantes: <?php echo $totalUsuarioGrupo - $quantidadeSuperusuarios; ?>
		</td>
	</tr>
	<tr class="textblk">
		<td>
			Cadastrados como Superusu�rios (Gerentes): <?php echo $quantidadeSuperusuarios; ?>
		</td>
	</tr>		
	<?php 
	$quantidadeRankeados = $quantidadeRankeados - $quantidadeParticipantesSuperusuario;
	$quantidadeNaoRankeados = $totalUsuarioGrupo - $quantidadeSuperusuarios - $quantidadeRankeados;
	
	}
	if (exibeTabela())
	{
		$quantidadeRankeados = $quantidadeRankeados - $quantidadeParticipantesSuperusuario;
		$quantidadeNaoRankeados = $totalUsuarioGrupo - $quantidadeSuperusuarios - $quantidadeRankeados;
	?>
	<tr class="textblk">
		<td>
			<?php 
				if ($tipoRelatorio == "L")
				{
					if ($_REQUEST["id"] > -1)
					{
						echo "Quantidade de Lota��es Participantes: $quantidadeRankeados";
					}
				}
				else
					echo "Quantidade de Participantes: $quantidadeRankeados";
			?>
				<!--  : <?php //echo $quantidadeRankeados; ?>-->
		</td>
	</tr>
	<?php 
/*	<tr class="textblk">
		<td>
			<?php 
				echo "Quantidade de Superusu�rios Participantes: $quantidadeParticipantesSuperusuario";
			
			?>
		</td>
	</tr>
*/
?>
		<?php
			if($codigoEmpresa == 34)
			{
				$linhaElegiveis = obterQuantidadeCadastradosElegiveis($codigoEmpresa, $codigoLotacao, $codigoCiclo);
				$totalCadastradosElegiveis = $linhaElegiveis[0];
				
				$linhaElegiveis = obterQuantidadeParticipantesElegiveis($codigoEmpresa, $codigoLotacao, $codigoCiclo);
				$totalElegiveis = $linhaElegiveis[0];
		?>
	<tr>
		<td class="textblk">
			Eleg�veis a Certifica��o: <?php echo $totalCadastradosElegiveis;?>
		</td>
	</tr>
		
	<tr class="textblk">
		<td>
			Quantidade de Participantes Eleg�veis: <?php echo $totalElegiveis; ?>
		</td>
	</tr>	

	<tr class="textblk">
		<td>
		<?php 
			$quantidadeParticipantesNaoElegiveis = $quantidadeRankeados - $totalElegiveis;
			$quantidadeParticipantesNaoElegiveis = ($quantidadeParticipantesNaoElegiveis < 0? 0 : $quantidadeParticipantesNaoElegiveis);
		?>
			Quantidade de Participantes N�o Eleg�veis: <?php echo $quantidadeParticipantesNaoElegiveis; ?>
		</td>
	</tr>

	<tr class="textblk">
		<td>
			Quantidade de Participantes Eleg�veis sem Certifica��o: <?php echo $totalElegiveis - $linhaElegiveis[1]; ?>
		</td>
	</tr>
		
		<?php 	
			}
		 ?>
	
		<?php if (1==2) {?>
		<tr class="textblk">
			<td>
				<?php 
					if ($tipoRelatorio == "L")
					{
						$quantidadeLotacoesNaoRankeadas = $quantidadeLotacoesCadastradas - $quantidadeRankeados;
						if ($_REQUEST["id"] > -1)
						{
							echo "Quantidade de Lota��es N�o Participantes: $quantidadeLotacoesNaoRankeadas";
						}
					}
					else
					{
						echo "Quantidade de N�o Participantes: $quantidadeNaoRankeados";
					}	
				?>
				
			</td>
		</tr>
		<?php }?>
	<?php
	}
	?>	

</table>

<table cellspacing="0" border="0" style="margin-left: 2.5%; margin-right: 2.5%;" width="94%">
	<tr class="textblk">
		<td style="width:100px"><!-- <b>Indicadores</b> --></td>
		<td style="width:100px;" align="center"><!-- <b>Perfil Empresarial</b> --></td>
		<td rowspan="<?php echo $rowSpanCabecalho;?>" >&nbsp;&nbsp;</td>
		<?php
			if (!(ehEmpresa()))
			{
				$labelPefilColuna = "Perfil Grupo";
				if (ehLotacao())
				{
					$labelPefilColuna = "Perfil Lota��o";
					if ($POST["nomeHierarquia"] != "") $labelPefilColuna = "Perfil {$POST["nomeHierarquia"]}";
				}
		?>
				
		<td style="width:100px" align="center"><b><?php echo $labelPefilColuna;?></b></td>
		<?php

			}
		?>
		<td rowspan="<?php echo $rowSpanCabecalho;?>" >&nbsp;</td>
		<td width="100%" rowspan="<?php echo $rowSpanCabecalho + 2;?>"  valign="top" align="right">
			<table cellpadding="1" cellspacing="0" border="0" style="border: 1px solid #000000;" >
				<tr class="textblk"><td>RT - Ranking Total</td></tr>
				<tr class="textblk"><td>RG - Ranking no Grupo</td></tr>
				<?php if(!$assessment){?>
				<tr class="textblk"><td>NR - N�o Ranqueado</td></tr>
				<tr class="textblk"><td>"-" - N�o Eleg�veis</td></tr>
				<?php }?>
				
				<?php
				if($codigoEmpresa == 34)
				{
				?>
					<tr class="textblk"><td>E - Eleg�vel</td></tr>
					<tr class="textblk"><td>NE - N�o Eleg�vel</td></tr>
					<tr class="textblk"><td>* - Teste de Certifica��o</td></tr>
				<?php 
				}
				
				
				?>
				
			</table>
		</td>
	</tr>
	
	<?php if($exibirMediaAvaliacao!=2)
	{
	?>
<!--	<tr class="textblk">
		<td nowrap="nowrap">MD - M�dia dos Testes <?php //echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo formatarValores($fatorEmpresaMediaAvaliacaoOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoMediaAvaliacaoOriginal,2); ?></td>
		<?php
			}
		?>
	</tr> -->
	
	<?php
	}
	$exibirParticipacaoAvaliacao = 1;
	if($exibirParticipacaoAvaliacao!=2)
	{
	?>
<!--	<tr class="textblk" >
		<td nowrap>Quantidade de Testes<?php //echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo formatarValores($fatorEmpresaParticipacaoAvaliacaoOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoParticipacaoAvaliacaoOriginal,2); ?></td>
		<?php
			}
		?>
	</tr> -->
	<?php
	}
	if(!$assessment){
	?>
	<tr class="textblk">
		<td>MD - M�dia dos Diagn�sticos <?php //echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo formatarValores($fatorEmpresaMediaAssessmentOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoMediaAssessmentOriginal,2); ?></td>
		<?php
			}
		?>
	</tr>
	<tr class="textblk" >
		<td nowrap>QD - Quantidade de Diagn�sticos<?php //echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo formatarValores($fatorEmpresaParticipacaoAssessmentOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoParticipacaoAssessmentOriginal,2); ?></td>
		<?php
			}
		?>
	</tr>
	
	<?php 
	$exibirAcesso = 2;
	if($exibirAcesso!=2)
	{
	?>
	<tr class="textblk">
		<td>AP - Acessos por Participantes</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaAcessoOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoAcessoOriginal,2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	$exibirDownload = 2;
	if($exibirDownload!=2)
	{
	?>
	<tr class="textblk">
		<td nowrap>DP - Downloads por Participantes</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaDownloadOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoDownloadOriginal,2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	$exibirParticipacaoSimulado = 1;
	if($exibirParticipacaoSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td nowrap>SP - Simulados por Participantes</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaParticipacaoSimuladoOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoParticipacaoSimuladoOriginal,2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	$exibirRealizacaoSimulado = 1;	
	if($exibirRealizacaoSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td nowrap>SA - Simulados com Aprova��o</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaRealizacaoSimuladoOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoRealizacaoSimuladoOriginal,2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	$exibirParticipacaoForum = 1;
	if($exibirParticipacaoForum!=2)
	{
	?>
	<tr class="textblk">
		<td>PF - Participa��o em F�runs</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaParticipacaoForumOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoParticipacaoForumOriginal,2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	$exibirContribuicaoForum = 1;
	if($exibirContribuicaoForum!=2)
	{
	?>
	<tr class="textblk">
		<td>CF - Contribui��o em F�runs</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaContribuicaoForumOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoContribuicaoForumOriginal,2); ?></td>
		<?php
			}
		?>
	</tr>	
	<?php
	}
	$exibirMediaSimulado = 2;
	if($exibirMediaSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td>Nota em Simulado</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaMediaSimuladoOriginal,2); ?></td>
		<?php
			if (!(ehEmpresa()))
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoMediaSimuladoOriginal,2); ?></td>
		<?php
			}
		?>
	</tr>	
	<?php
	}
	$exibirParticipacao = 2;
	if($exibirParticipacao!=2)
	{
	?>	
		<tr class="textblk">
			<td>Percentual de Participa��o</td>
			<td align="center"><?php echo formatarValores($fatorEmpresaParticipacaoOriginal,2); ?></td>
			<?php
			if (!(ehEmpresa()))
			{
			?>
			<td align="center"><?php echo formatarValores($fatorGrupoParticipacaoOriginal,2); ?></td>
			<?php
			}
			?>
		</tr>
	<?php
	}
		
	}
	if ($_POST["btnExcel"] == ""){
		?>
		<tr class="textblk"><td>&nbsp;</td>
		<tr class="textblk"><td>&nbsp;</td>
		<?php 
	}
	?>
</table>
<?php 
if ($_POST["btnExcel"] == ""){
?>
<br />
<?php 
}

if (exibeTabela())
{
?>

<table class="tabelaRanking" cellspacing="0" align="center" width="95%" border="0" style="border: 1px solid black;">
	<tr class='textblk' style="font-weight: bold;">
		<td class="bdLat">#</td>
		<?php
			$labelLotacao = "Lota��o";
			if ($POST["nomeHierarquia"] != "") $labelLotacao = "Respons�vel";
			switch ($tipoRelatorio)
			{
				case "U":
					echo "<td class=\"bdLat\" >Participante</td>
						 <td class=\"bdLat\" >Login</td>
						 <td class=\"bdLat\" >Cargo/Fun��o</td>
						 <td class=\"bdLat\" >$labelLotacao</td>";
					break;
				default:
					echo "<td class=\"bdLat\" width='100%'>$tituloCampo</td>";
					break;
			}
		
		?>
		
		<td class='bdLat' align="center" width="30" bgcolor="#cccccc">RT</td>
		
		<?php
		
		if(exibirGrupo())
		{
			echo "<td class='bdLat' align='center' width='30' bgcolor='#cccccc'>RG</td>";
		}
		?>
		
		<?php
		if($exibirMediaAvaliacao!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">MD</td>
		<?php
		}
		$exibirParticipacaoAvaliacao=2;
		if($exibirParticipacaoAvaliacao!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">QA</td>
		<?php
		
		?>
		<td class='bdLat' width="30" align="center">MD</td>
		<td class='bdLat' width="30" align="center">QD</td>
		<?php 
		}
		$exibirAcesso=2;
		if($exibirAcesso!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">AP</td>
		<?php
		}
		$exibirDownload=2;
		if($exibirDownload!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">DP</td>
		<?php
		}
		$exibirParticipacaoSimulado=2;
		if($exibirParticipacaoSimulado!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">SP</td>
		<?php
		}
		$exibirRealizacaoSimulado=2;
		if($exibirRealizacaoSimulado!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">SA</td>
		<?php
		}
		$exibirParticipacaoForum=2;
		if($exibirParticipacaoForum!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">PF</td>
		<?php
		}
		$exibirContribuicaoForum=2;
		if($exibirContribuicaoForum!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">CF</td>
		<?php
		}
		
		$exibirMediaSimulado=2;
		if($exibirMediaSimulado!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RNS</td>
		<?php
		}
		$exibirParticipacao=2;
		if($exibirParticipacao!=2)
		{
		?>
		<td width="30" align="center">RP</td>
		<?php
		}
		?>
	</tr>

	<?php
		$ranking_Grupo = 0;
		$rankingGrupoReal = 0;
		$ranking_geral_Anterior = 0;
		$contadorLinhas = 0;
		foreach ($linhas as $linha)
		{
			if ($linha["FILTRADO"] == 0)
				continue;
			
			if ($tipoRelatorio == "G" && $_GET["id"] != "")
			{
				if ($linha[$campoSelect] != $POST["nomeFiltro"])
					continue;
			}
				
			if(is_numeric($limitarResultado) && $limitarResultado > 0)
			{
				if ($linha["RANK_TOTAL_PONTUACAO"] > $limitarResultado)
				{
					break;
				}
			}
				
			echo "<tr class='textblk'>";
			
			$contadorLinhas++;
			echo "<td class=\"titRelatD\" >$contadorLinhas</td>";
			
			switch ($tipoRelatorio)
			{
				case "U":
					echo "<td class=\"titRelat\" style='text-align: left;'>{$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}</td>
						 <td class=\"titRelat\" style='text-align: left;' >{$linha["login"]}</td>
						 <td class=\"titRelat\" style='text-align: left;'>{$linha["cargofuncao"]}</td>
						 <td class=\"titRelat\" style='text-align: left;'>{$linha["DS_LOTACAO"]}</td>";
					break;
				default:
					echo "<td class='titRelat'>{$linha[$campoSelect]}</td>";
					break;
			}
			
			if (ehSuperusuario($linha))
			{
				$linha["RANK_TOTAL_PONTUACAO"] = "-";
				if ($codigoEmpresa != 34)
				{
					foreach ($linha as &$valor)
					{
						$valor = "-";
					}
					
				}
			}
			$elegivel = $linha["ELEGIVEL"];
			$tipoUsuario = $linha["tipo"];
			if (!$linha["PARTICIPOU"])
			{
				$valorSubstituto = "NR";
				if (ehSuperusuario($linha) && $codigoEmpresa == 34)
				{
					$valorSubstituto = "-";
				}
				foreach ($linha as &$valor)
				{
					//$valorAvaliacao = ($linha["ELEGIVEL"] == 1 ? "E" : "NE");
					$valor = $valorSubstituto;
				}
			}
					
			echo 	"<td class='titRelat' bgcolor=\"#cccccc\"><b>{$linha["RANK_TOTAL_PONTUACAO"]}</b></td>";
			
			if (exibirGrupo())
			{
				if ($ranking_geral_Anterior != $linha["RANK_TOTAL_PONTUACAO"])
				{
					$ranking_geral_Anterior = $linha["RANK_TOTAL_PONTUACAO"];
					$ranking_Grupo = $rankingGrupoReal + 1;
					//$ranking_Grupo++;
				}
				$ExibicaoRankingGrupo = $ranking_Grupo;
				$rankingGrupoReal++;
				
				if ($linha["RANK_TOTAL_PONTUACAO"] == "NR")
				{
					$ExibicaoRankingGrupo = "NR";
				}
				
				if ($linha["RANK_TOTAL_PONTUACAO"] == "-")
				{
					$ExibicaoRankingGrupo = "-";
				}
				
				echo 	"<td class='titRelat' bgcolor=\"#cccccc\">$ExibicaoRankingGrupo</td>";
				
			}
			
			if($exibirMediaAvaliacao!=2)
			{
				$mediaAvaliacao = "-";
				if ($linha["VALOR_MEDIA_AVALIACAO"] != null)
				{
					$mediaAvaliacao = formatarValores($linha["VALOR_MEDIA_AVALIACAO"] * 10,2);
					
					if ($mediaAvaliacao == "-" && $linha["VALOR_PARTICIPACAO_AVALIACAO"] > 0)
						$mediaAvaliacao = "0";
					
				}
				
				if ($mediaAvaliacao == "-" && $codigoEmpresa == 34)
				{
					if ($elegivel == 1 && $tipoUsuario == 1) //Se � usu�rio comum e pode fazer avalia��o
					{
						$mediaAvaliacao = "E";
					}
					else
					{
						$mediaAvaliacao = "NE";
					}
				}
				
				if ($tipoUsuario != 1 && $codigoEmpresa == 34)
				{
					if($linha["VALOR_PARTICIPACAO_AVALIACAO"] > 0)
					{
						$mediaAvaliacao = "*";
					}else
					{
						$mediaAvaliacao = "-";
					}	
				}
				
				echo 	"<td class='titRelat'>$mediaAvaliacao</td>";
			}
			if($exibirParticipacaoAvaliacao!=2)
			{
				//$linhaAcesso["TOTAL_PONTUACAO"]
				echo 	"<td class='titRelat'>{$linha["VALOR_PARTICIPACAO_AVALIACAO"]}</td>";
			
			
				$mediaDiagnostico = "-";
				$quantidadeDiagnostico = $linha["VALOR_PARTICIPACAO_DIAGNOSTIVO"];
				if ($quantidadeDiagnostico == null) {
					$quantidadeDiagnostico = 0;
				}
				if ($linha["VALOR_MEDIA_ASSESSMENT"] != null)
				{
					$mediaDiagnostico = formatarValores($linha["VALOR_MEDIA_ASSESSMENT"] * 10,2);
					
					if ($mediaDiagnostico == "-" && $quantidadeDiagnostico > 0)
						$mediaDiagnostico = "0";
					
				}
				
				echo "<td class='titRelat'>$mediaDiagnostico</td>";
				echo "<td class='titRelat'>{$quantidadeDiagnostico}</td>";
			}
			if($exibirAcesso!=2)
				echo 	"<td class='titRelat'>".formatarValores($linha["VALOR_ACESSO"],2)."</td>";
			if($exibirDownload!=2)
				echo 	"<td class='titRelat'>".formatarValores($linha["VALOR_DOWNLOAD"],2)."</td>";
			if($exibirParticipacaoSimulado!=2)
				echo 	"<td class='titRelat'>".formatarValores($linha["VALOR_PARTICIPACAO_SIMULADO"],2)."</td>";
			if($exibirRealizacaoSimulado!=2)
				echo 	"<td class='titRelat'>".formatarValores($linha["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"],2)."</td>";
			if($exibirParticipacaoForum!=2)
				echo 	"<td class='titRelat'>{$linha["VALOR_PARTICIPACAO_FORUM"]}</td>";				
			if($exibirContribuicaoForum!=2)
				echo 	"<td class='titRelat'>{$linha["VALOR_CONTRIBUICAO_FORUM"]}</td>";

			if($exibirMediaSimulado!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_MEDIA_SIMULADO"]}</td>";


			
			
			if($exibirParticipacao!=2)
				echo 	"<td class='titRelatTop'>{$linha["RANK_PARTICIPACAO"]}</td>";
				
			echo 	"</tr>";
			
			
			/*echo "<tr class='textblk'>
					<td class='titRelatD'>{$linha[$campoSelect]}</td>
					<td class='titRelat'>{$linha["FATOR_PARTICIPACAO"]}</td>
					<td class='titRelat'>{$linha["FATOR_ACESSO"]}</td>
					<td class='titRelat'>{$linha["FATOR_DOWNLOAD"]}</td>
					<td class='titRelat'>{$linha["FATOR_PARTICIPAO_SIMULADO"]}</td>
					<td class='titRelat'>{$linha["FATOR_REALIZACAO_SIMULADO"]}</td>
					<td class='titRelat'>{$linha["MEDIA_SIMULADO"]}</td>
					<td class='titRelat'>{$linha["FATOR_PARTICIPACAO_FORUM"]}</td>
					<td class='titRelat'>{$linha["RANK_CONTRIBUICAO_FORUM"]}</td>
					<td class='titRelat'>{$linha["FATOR_PARTICIPAO_AVALIACAO"]}</td>
					<td class='titRelat'>{$linha["MEDIA_AVALIACAO"]}</td>
					<td class='titRelatTop'>{$linha["RANK_TOTAL_PONTUACAO"]}</td>
				  </tr>
			";*/
			
			//
			
			
			
		}

		
	?>
	
</table>
<?php 
}


function ehSuperusuario(&$linha)
{
	return ($linha["tipo"] != null && $linha["tipo"] < 0);
}

function formatarValores($valor, $decimal = 2, $seVazio = "-")
{
	if ($valor == "" || $valor == null || $valor == "-")
	{
		return $seVazio;
	}
	
	if (round($valor,0) == $valor)
	{
		return round($valor,0);
	}
	
	return number_format(round($valor,$decimal), $decimal, ",", "");		
	
}

function aplicarNaoRanqueado($linha, $campoRanking, $campoFator)
{
	$campoFator = $campoFator . "_ACESSO";
	
	if (!$linha[$campoFator])
		return "NR";
		
	return $linha[$campoRanking];
	
}

function exibeTabela()
{
	global $tipoRelatorio, $codigoFiltroUsuario;
	
	$retorno = true;
	
	if ($tipoRelatorio == "G" && ehFiltro()) //Se for relat�rio de grupo e o codigo do grupo for passado ent�o n�o exibe a tabela
		$retorno = false;
	
	if ($tipoRelatorio == "L" && ehLotacao()) //Se for relat�rio de grupo e o codigo do grupo for passado ent�o n�o exibe a tabela
		$retorno = false;
		
	return $retorno;
	
}

function ehFiltro()
{
	return (isset($_REQUEST["id"]) && $_REQUEST["id"] > 0);
}

function ehLotacao()
{
	return (isset($_REQUEST["hdnLotacao"]) && $_REQUEST["hdnLotacao"] > 0);
}

function ehEmpresa()
{
	return (!(ehLotacao() || ehFiltro()));
}

?>

<?php

		if ($_POST["btnExcel"] == "") {

	?>

	<div style="width:100%;text-align:center">

		<br />

		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']; ?>">

		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">

		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">

		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>

		</form>	

	</div>

	<?php

		}

	?>

</body>
</html>