<?php

include('../../admin/filtrocomponente.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/controles.php');
include('../../admin/datagrid.php');

obterSessaoEmpresa();

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

function instanciarRN(){
	return new col_disciplina();
}

$paginaEdicao = "relatorio_prova.php";
$paginaEdicaoAltura = 600;
$paginaEdicaoLargura = 900;

$obterParametros = "'cboEmpresa=' + document.getElementById('cboEmpresa').value + '&nomeEmpresa=' + document.getElementById('cboEmpresa').options[document.getElementById('cboEmpresa').selectedIndex].text + 
					'&cboLotacao=' + document.getElementById('cboLotacao').value + '&nomeLotacao=' + escape(document.getElementById('cboLotacao').options[document.getElementById('cboLotacao').selectedIndex].text) + 
					'&cboCargo=' + escape(document.getElementById('cboCargo').value) +
					'&cboUsuario=' + document.getElementById('cboUsuario').value + '&nomeUsuario=' + document.getElementById('cboUsuario').options[document.getElementById('cboUsuario').selectedIndex].text + 
					'&cboProva=' + document.getElementById('cboProva').value + '&nomeProva=' + escape(document.getElementById('cboProva').options[document.getElementById('cboProva').selectedIndex].text) +
					'&txtDe=' + document.getElementById('txtDe').value + '&txtAte=' + document.getElementById('txtAte').value +
					'&rdoTipoRelatorio=' + (document.getElementById('rdoTipoRelatorio').checked?1:0)" ;

define(TITULO_PAGINA, "Relat�rio de ". $labelAvaliacao . " Realizadas");

include('relatorio_cabecalho.php');

obterTextoComentario(basename($PHP_SELF,".php"));

exibirParametros();

include('relatorio_rodape.php');

acertarSessaoEmpresa();

function exibirParametros(){
	
	echo '<table cellpadding="0" cellspacing="0" border="0" align="center">';
	
		ExibirFiltroConsulta(true, false, false, false, true, true, false, $_SESSION["empresaID"]);
	
	?>
		<tr><td colspan="2">&nbsp;</tr>
		<tr>
			<td class="textblk">
				Per�odo: &nbsp;
			</td>
		</tr>
		<tr>
			<td class="textblk">
				De: <?php textbox("txtDe", "10", $_GET["txtDe"], "75px", "textbox3",false, Mascaras::Data); ?>
				At�: <?php textbox("txtAte", "10", $_GET["txtAte"], "75px", "textbox3",false, Mascaras::Data); ?>
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</tr>
	</table>
	
	<?php
	
}


?>