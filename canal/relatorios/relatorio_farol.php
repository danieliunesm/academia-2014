<?php

include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

$codigoAgrupamento = "";
$limiteResultado = "";

$POST = null;
if (isset($_GET["id"]))
{
	
	$farol = new col_farol();
	$farol->CD_FAROL = $_GET["id"];
	$farol = $farol->obter();
	
	$POST = array();
	$POST["txtNomeRelatorio"] = $farol->NM_FAROL;
	$POST["txtPesoParticipanteAvaliacao"] = $farol->NR_PESO_PARTICIPANTE_AVALIACAO;
	$POST["txtPesoMediaAvaliacao"] = $farol->NR_PESO_MEDIA_AVALIACAO;
	$POST["txtPesoParticipanteSimulado"] = $farol->NR_PESO_PARTICIPANTE_SIMULADO;
	$POST["txtPesoMediaSimulado"] = $farol->NR_PESO_MEDIA_SIMULADO;
	$POST["txtPesoContribuicaoForum"] = $farol->NR_PESO_CONTRIBUICAO_FORUM;
	$POST["txtPesoVelocidadeProva"] = $farol->NR_PESO_VELOCIDADE_PROVA;
	$POST["txtPesoQuantidadeDownload"] = $farol->NR_PESO_QUANTIDADE_DOWNLOAD;
	$POST["txtPesoQuantidadeHits"] = $farol->NR_PESO_QUANTIDADE_HITS;
	$POST["txtPesoQuantidadeSessao"] = $farol->NR_PESO_QUANTIDADE_SESSAO;
	$POST["txtPesoQuantidadeUsuario"] = $farol->NR_PESO_QUANTIDADE_USUARIO;
	
	$POST["cboEmpresa"] = $farol->filtro->CD_EMPRESA;
	$POST["cboUsuario"] = tranformaArrayVazioMenos1(obterValoresUsuario($farol->filtro->usuarios));
	$POST["cboProva"] = tranformaArrayVazioMenos1(obterValoresProva($farol->filtro->provas));
	$POST["cboCargo"] = tranformaArrayVazioMenos1(obterValoresCargo($farol->filtro->cargos));
	$POST["cboLotacao"] = tranformaArrayVazioMenos1(obterValoresLotacao($farol->filtro->lotacoes));
	$POST["cboForum"] = tranformaArrayVazioMenos1(obterValoresForum($farol->filtro->foruns));
	
	$POST["txtDe"] = $farol->DT_INICIO;
	$POST["txtAte"] = $farol->DT_TERMINO;
	
	$limiteResultado = $farol->NR_LIMITE;
	$codigoAgrupamento = split(";", $farol->IN_TIPO_AGRUPAMENTO);
	
	$POST["chktxtPesoParticipanteAvaliacao"] = $farol->IN_PESO_PARTICIPANTE_AVALIACAO;
	$POST["chktxtPesoMediaAvaliacao"] = $farol->IN_PESO_MEDIA_AVALIACAO;
	$POST["chktxtPesoParticipanteSimulado"] = $farol->IN_PESO_PARTICIPANTE_SIMULADO;
	$POST["chktxtPesoMediaSimulado"] = $farol->IN_PESO_MEDIA_SIMULADO;
	$POST["chktxtPesoContribuicaoForum"] = $farol->IN_PESO_CONTRIBUICAO_FORUM;
	$POST["chktxtPesoVelocidadeProva"] = $farol->IN_PESO_VELOCIDADE_PROVA;
	$POST["chktxtPesoQuantidadeDownload"] = $farol->IN_PESO_QUANTIDADE_DOWNLOAD;
	$POST["chktxtPesoQuantidadeHits"] = $farol->IN_PESO_QUANTIDADE_HITS;
	$POST["chktxtPesoQuantidadeSessao"] = $farol->IN_PESO_QUANTIDADE_SESSAO;
	$POST["chktxtPesoQuantidadeUsuario"] = $farol->IN_PESO_QUANTIDADE_USUARIO;
	
}
else
{
	$POST = $_POST;
	
	$limiteResultado = $_REQUEST["txtLimiteResultado"];
	
	if (is_array($_REQUEST["cboAgrupamento"]))
	{
		$codigoAgrupamento  = $_REQUEST["cboAgrupamento"];	
	}
	
}

$nomeRelatorio = $POST["txtNomeRelatorio"];

if ($nomeRelatorio == "")
{
	$nomeRelatorio = "Relat�rio Farol";
}

$pesoParticipanteAvaliacao = ZeraVazio("txtPesoParticipanteAvaliacao");
$PesoMediaAvaliacao = ZeraVazio("txtPesoMediaAvaliacao");
$PesoParticipanteSimulado = ZeraVazio("txtPesoParticipanteSimulado");
$PesoMediaSimulado = ZeraVazio("txtPesoMediaSimulado");
$PesoContribuicaoForum = ZeraVazio("txtPesoContribuicaoForum");
$PesoVelocidadeProva = ZeraVazio("txtPesoVelocidadeProva");
$PesoQuantidadeDownload = ZeraVazio("txtPesoQuantidadeDownload");
$PesoQuantidadeHits = ZeraVazio("txtPesoQuantidadeHits");
$PesoQuantidadeSessao = ZeraVazio("txtPesoQuantidadeSessao");
$PesoQuantidadeUsuario = ZeraVazio("txtPesoQuantidadeUsuario");

$inpesoParticipanteAvaliacao = TratarCheckFarol($POST["chktxtPesoParticipanteAvaliacao"]);
$inPesoMediaAvaliacao = TratarCheckFarol($POST["chktxtPesoMediaAvaliacao"]);
$inPesoParticipanteSimulado = TratarCheckFarol($POST["chktxtPesoParticipanteSimulado"]);
$inPesoMediaSimulado = TratarCheckFarol($POST["chktxtPesoMediaSimulado"]);
$inPesoContribuicaoForum = TratarCheckFarol($POST["chktxtPesoContribuicaoForum"]);
$inPesoVelocidadeProva = TratarCheckFarol($POST["chktxtPesoVelocidadeProva"]);
$inPesoQuantidadeDownload = TratarCheckFarol($POST["chktxtPesoQuantidadeDownload"]);
$inPesoQuantidadeHits = TratarCheckFarol($POST["chktxtPesoQuantidadeHits"]);
$inPesoQuantidadeSessao = TratarCheckFarol($POST["chktxtPesoQuantidadeSessao"]);
$inPesoQuantidadeUsuario = TratarCheckFarol($POST["chktxtPesoQuantidadeUsuario"]);

$codigoEmpresa = $POST['cboEmpresa'];
$codigoUsuario = joinArray($POST['cboUsuario']);
$codigoProva = joinArray($POST['cboProva']);
$cargo = joinArray($POST['cboCargo'],"','");
$cargoCorrigido = corrigePlic($cargo);
$codigoLotacao = joinArray($POST['cboLotacao']);
$codigoForum = joinArray($POST['cboForum']);

$dataInicio = $POST["txtDe"];
$dataTermino = $POST["txtAte"];

if ($dataInicio == "" && $dataTermino == "")
{
	$dataTermino = date("d/m/Y");
}

$dataInicioTexto = $dataInicio;
$dataTerminoTexto = $dataTermino;

$dataTerminoAnterior = "";
$dataTerminoAnteriorTexto = "";

$sqlDataProva = "";
$sqlDataForum = "";
$sqlDataAcesso = "";
$sqlDataAnteriorProva = "";
$sqlDataAnteriorForum = "";
$sqlDataAnteriorAcesso = "";

if ($dataInicio != ""){
	$dataInicio = substr($dataInicio, 6, 4) . substr($dataInicio, 3, 2) . substr($dataInicio, 0, 2);
	
	$sqlDataProva = " $sqlData AND  DATE_FORMAT(DT_INICIO, '%Y%m%d') >= '$dataInicio' ";
	$sqlDataForum = " $sqlData AND  DATE_FORMAT(data, '%Y%m%d') >= '$dataInicio' ";
	$sqlDataAcesso = " $sqlData AND  DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicio' ";
}

if ($dataTermino != ""){
	$ano = substr($dataTermino, 6, 4);
	$mes = substr($dataTermino, 3, 2);
	$dia = substr($dataTermino, 0, 2);
	$dataTermino = $ano . $mes . $dia;
	
	$dataTerminoAnteriorTexto = strftime("%d/%m/%Y" , mktime(0, 0, 0, $mes, $dia - 1, $ano));
	$dataTerminoAnterior = substr($dataTerminoAnteriorTexto, 6, 4) . substr($dataTerminoAnteriorTexto, 3, 2) . substr($dataTerminoAnteriorTexto, 0, 2);
		
	$sqlDataProva = " $sqlData AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= '$dataTermino' ";
	$sqlDataForum = " $sqlData AND DATE_FORMAT(data, '%Y%m%d') <= '$dataTermino' ";
	$sqlDataAcesso = " $sqlData AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataTermino' ";
	
	$sqlDataAnteriorProva = " $sqlData AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= '$dataTerminoAnterior' ";
	$sqlDataAnteriorForum = " $sqlData AND DATE_FORMAT(data, '%Y%m%d') <= '$dataTerminoAnterior' ";
	$sqlDataAnteriorAcesso = " $sqlData AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataTerminoAnterior' ";
}

$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];

$colspanTitulo = 2;

//MontarAgrupamento
$arrayAgrupamento = $codigoAgrupamento;

$codigoAgrupamento = montarAgrupamento($codigoAgrupamento);

if ($codigoAgrupamento == "")
{

$resultadoTotalProva = executarQueryTotalAvaliacao($sqlDataProva, 1);
$resultadoTotalProvaAnterior = executarQueryTotalAvaliacao($sqlDataAnteriorProva, 1);

$resultadoTotalSimulado = executarQueryTotalAvaliacao($sqlDataProva, 0);
$resultadoTotalSimuladoAnterior = executarQueryTotalAvaliacao($sqlDataAnteriorProva, 0);

$resultadoMediaProva = executarQueryTotalAvaliacao($sqlDataProva, 1, 2);
$resultadoMediaProvaAnterior = executarQueryTotalAvaliacao($sqlDataAnteriorProva, 1, 2);

$resultadoMediaSimulado = executarQueryTotalAvaliacao($sqlDataProva, 0, 2);
$resultadoMediaSimuladoAnterior = executarQueryTotalAvaliacao($sqlDataAnteriorProva, 0, 2);

$resultadoTempoProva = executarQueryTotalAvaliacao($sqlDataProva, 1, 3);
$resultadoTempoProvaAnterior = executarQueryTotalAvaliacao($sqlDataAnteriorProva, 1, 3);

$resultadoForum = executarQueryTotalForum($sqlDataForum);
$resultadoForumAnterior = executarQueryTotalForum($sqlDataAnteriorForum);

$resultadoDownload = executarQueryAcesso($sqlDataAcesso, 1, true);
$resultadoDownloadAnterior = executarQueryAcesso($sqlDataAnteriorAcesso, 1, true);

$resultadoHits = executarQueryAcesso($sqlDataAcesso);
$resultadoHitsAnterior = executarQueryAcesso($sqlDataAnteriorAcesso);

$resultadoSessao = executarQueryAcesso($sqlDataAcesso, 2);
$resultadoSessaoAnterior = executarQueryAcesso($sqlDataAnteriorAcesso, 2);

$resultadoUsuario = executarQueryAcesso($sqlDataAcesso, 3);
$resultadoUsuarioAnterior = executarQueryAcesso($sqlDataAnteriorAcesso, 3);
}
else
{
	$resultadoAgrupado = executarQueryAcessoGeral($sqlDataProva, $sqlDataAcesso, $sqlDataForum);
	$colspanTitulo = count($arrayAgrupamento);
}

	if ($_POST["btnExcel"] != "") {
		header("Content-Type: application/vnd.ms-excel; name='excel'");
		header("Content-disposition:  attachment; filename=RelatorioAvaliacao.xls");
	}
	
	ob_start();
?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	
	<?php
	
	if ($_POST["btnExcel"] == "") {

	?>
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">
	<?php
	}
	?>
	
	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
	</style>
	
</head>

<body>

<table cellpadding="0" cellspacing="0" border="0" width="95%" align="center">
	<tr style="height: 30px">
			<td class='textblk'>
				<p><b><?php echo $nomeEmpresa; ?></b></p>
			</td>
			<td align="center" class="textblk" colspan="<?php echo $colspanTitulo ?>">
				<b>
					<?php echo $nomeRelatorio; ?>
				</b>
			</td>
			<td style="text-align: right" class='textblk'>
				<p><?php echo date("d/m/Y") ?></p>
			</td>
	</tr>
	
	<tr>
		<td class='textblk' width="80">
			Participantes:
		</td>
		<td align="left" class="textblk" colspan="<?php echo $colspanTitulo ?>">
					<?php echo obterTotalUsuarios($codigoEmpresa); ?>
		</td>
		<td style="text-align: right" class='textblk'>
			&nbsp;
		</td>
	</tr>

	<?php if ($codigoAgrupamento == "")
	{
		
	?>
	
	
	<tr align="center" class="textblk" style="font-weight: bold">
		<td class="titRelatEsq" align="left">
			Itens Analisados
		</td>
		<td class="titRelat" >
			At� <?php echo $dataTerminoAnteriorTexto; ?>
		</td>
		<td class="titRelat" >
			At� <?php echo $dataTerminoTexto; ?>
		</td>
		<td class="titRelat" >
			<?php echo $dataTerminoTexto; ?>
		</td>
	</tr>
	
	<?php
		$somatorioDataAnterior = 0;
		$somatorioDataAteAtual = 0;
		$somatorioDataAtual = 0;
	
		$itensRelatorio = array(
									array('Participantes nas ' . $labelAvaliacao, $pesoParticipanteAvaliacao, $resultadoTotalProva, $resultadoTotalProvaAnterior, $inpesoParticipanteAvaliacao),
									array('M�dia nas ' . $labelAvaliacao, $PesoMediaAvaliacao, $resultadoMediaProva, $resultadoMediaProvaAnterior, $inPesoMediaAvaliacao),
									array('Participantes nos Simulados', $PesoParticipanteSimulado, $resultadoTotalSimulado, $resultadoTotalSimuladoAnterior, $inPesoParticipanteSimulado),
									array('M�dia nos Simulados', $PesoMediaSimulado, $resultadoMediaSimulado, $resultadoMediaSimuladoAnterior, $inPesoMediaSimulado),
									array('Velocidade na Prova', $PesoVelocidadeProva, $resultadoTempoProva, $resultadoTempoProvaAnterior, $inPesoVelocidadeProva),
									array('Contribui��es no F�rum', $PesoContribuicaoForum, $resultadoForum, $resultadoForumAnterior, $inPesoContribuicaoForum),
									array('Quantidade de Download', $PesoQuantidadeDownload, $resultadoDownload, $resultadoDownloadAnterior, $inPesoQuantidadeDownload),
									array('Quantidade de Hits', $PesoQuantidadeHits, $resultadoHits, $resultadoHitsAnterior, $inPesoQuantidadeHits),
									array('Quantidade de Sess�es', $PesoQuantidadeSessao, $resultadoSessao, $resultadoSessaoAnterior, $inPesoQuantidadeSessao),
									array('Quantidade de Acessos de Usu�rios', $PesoQuantidadeUsuario, $resultadoUsuario, $resultadoUsuarioAnterior, $inPesoQuantidadeUsuario),
								);
	
		foreach($itensRelatorio as $item)
		{
			
			if ($item[4] != 1)
			{
				continue;
			}
			
			$titulo = $item[0];
			$peso = $item[1];
			
			$resultadoItemDataAnterior = $item[3];
			$linha = mysql_fetch_row($resultadoItemDataAnterior);
			$valorDataAnterior = $linha[0];
			if ($valorDataAnterior == "")
			{
				$valorDataAnterior = 0;
			}
			
			$resultadoItemDataAteAtual = $item[2];
			$linha = mysql_fetch_row($resultadoItemDataAteAtual);
			$valorDataAteAtual = $linha[0];
			if ($valorDataAteAtual == "") {
				$valorDataAteAtual = 0;
			}
			
			$valorDataAtual = $valorDataAteAtual - $valorDataAnterior;
			
			$somatorioDataAnterior = $somatorioDataAnterior + ($peso * $valorDataAnterior);
			$somatorioDataAteAtual = $somatorioDataAteAtual + ($peso * $valorDataAteAtual);
			$somatorioDataAtual = $somatorioDataAtual + ($peso * $valorDataAtual);
			
			echo "<tr class=\"textblk\" align=\"center\">
					<td class=\"titRelatEsq\" align=\"left\">
						$titulo
					</td>
					<td class=\"titRelat\">$valorDataAnterior</td>
					<td class=\"titRelat\">$valorDataAteAtual</td>
					<td class=\"titRelat\">$valorDataAtual</td>
				  </tr>";
		}
		
		
		echo "<tr class=\"textblk\" align=\"center\">
				<td class=\"titRelatEsqRod\" align=\"left\" width=\"25%\">
					<b>Pontua��o Radar</b>
				</td>
				<td class=\"titRelatRod\" width=\"25%\"><b>$somatorioDataAnterior</b></td>
				<td class=\"titRelatRod\" width=\"25%\"><b>$somatorioDataAteAtual</b></td>
				<td class=\"titRelatRod\" width=\"25%\"><b>$somatorioDataAtual</b></td>
		  	  </tr>";
	}
	else 
	{


		?>
		
		<tr align="center" class="textblk" style="font-weight: bold">
			<td class="titRelatEsq" align="center">
				Ranking
			</td>
			
			<?php
			
				foreach ($arrayAgrupamento as $itemRelatorio)
				{
					
					$largura = "";
					$texto = "";
					
					switch ($itemRelatorio)
					{
						case "1":
							$texto = "Lota��o";
							break;
						case "2":
							$texto = "Cargo/Fun��o";
							break;
							
						case "3":
							$texto = "Participante";
							if (count($arrayAgrupamento) > 1)
								$largura = "width=\"100\"";
							break;
					}
					
					echo "	<td align=\"left\" class=\"titRelat\" $largura >
								$texto
							</td>";
					
				}
			
			?>
			
			
			<td class="titRelat" width="80">
				Pontos
			</td>
		</tr>
		
		<?php
		
		$ranking = 1;
		$qtdResultados = mysql_num_rows($resultadoAgrupado);
		
		while ($linha = mysql_fetch_array($resultadoAgrupado))
		{
			
			$cssClassEsquerda = "titRelatEsq";
			$cssClassPadrao = "titRelat";
			
			if ($ranking == $qtdResultados)
			{
				$cssClassEsquerda = "titRelatEsqRod";
				$cssClassPadrao = "titRelatRod";
			}
			
			echo "<tr class=\"textblk\" align=\"center\">
					<td class=\"$cssClassEsquerda\" align=\"center\">
						$ranking
					</td>";
					
			foreach ($arrayAgrupamento as $itemRelatorio)
			{
				
				$texto = "";
				
				switch ($itemRelatorio)
				{
					case "1":
						$texto = "DS_LOTACAO";
						break;
					case "2":
						$texto = "cargofuncao";
						break;
						
					case "3":
						$texto = "login";
						break;
				}
				
				echo "	<td class=\"$cssClassPadrao\" align=\"left\">
							{$linha[$texto]}&nbsp;
						</td>";
				
			}
					
			echo "	<td class=\"$cssClassPadrao\">{$linha["TOTAL"]}</td>
				  </tr>";
			
			$ranking++;
		}
		
	}
	?>
	
	
	
	</table>
<?php

	function ZeraVazio($nomeCampo)
	{
		
		global $POST;
		
		if (is_numeric($POST[$nomeCampo]))
		{
			return $POST[$nomeCampo];
		}
		else
		{
			return 0;
		}
		
	}

	
	function executarQueryTotalAvaliacao($sqlDataProva, $inProva, $inTotalMediaTempo = 1)
	{
		global 	$codigoEmpresa, $codigoUsuario, $codigoProva, $cargo, $cargoCorrigido,
				$codigoLotacao, $codigoForum, $codigoAgrupamento, $limiteResultado;
		
		$count = "";
		
		if ($inTotalMediaTempo == 1)
		{
			$count = "COUNT(p.CD_PROVA) AS TOTAL";
		}
		else if ($inTotalMediaTempo == 2)
		{
			$count = "ROUND(AVG(pa.VL_MEDIA),2) AS TOTAL";
		}
		else 
		{
			$count = "ROUND(AVG(p.DURACAO_PROVA * 60 / TIME_TO_SEC(TIMEDIFF(pa.DT_TERMINO, pa.DT_INICIO))),2) AS TOTAL";
		}
		
		$ordenacao = "";
		$agrupamento = "";
		$limite = "";
		
		if ($codigoAgrupamento != "")
		{
			$count = "$codigoAgrupamento $count";
			
			$codigoAgrupamentoInterno = substr($codigoAgrupamento, 0, strrpos($codigoAgrupamento,','));
			
			$ordenacao = "ORDER BY $codigoAgrupamentoInterno";
			$agrupamento = "GROUP BY $codigoAgrupamentoInterno";
			
			if (is_numeric($limite))
				$limite = "LIMIT 0, $limite";
		}

		
		$sqlTotalAvaliacao = "
			SELECT
			  $count
			FROM
				col_empresa e
				LEFT JOIN col_lotacao l on l.CD_EMPRESA = e.CD_EMPRESA
				LEFT JOIN col_usuario u ON u.lotacao = l.CD_LOTACAO
				LEFT JOIN col_prova_aplicada pa ON pa.CD_USUARIO = u.CD_USUARIO
				LEFT JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
			WHERE
			  	pa.VL_MEDIA IS NOT NULL
			AND	p.IN_PROVA = $inProva
			AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
			AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
			AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
			AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
			AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
			$sqlDataProva
			$agrupamento
			$ordenacao
			$limite";
		
		//echo $sqlTotalAvaliacao;
		
		return DaoEngine::getInstance()->executeQuery($sqlTotalAvaliacao, true);
				
	}
	
	function executarQueryTotalForum($sqlDataForum)
	{
		
		global 	$codigoEmpresa, $codigoUsuario, $codigoProva, $cargo, $cargoCorrigido,
				$codigoLotacao, $codigoForum, $codigoAgrupamento, $limiteResultado;
		
		
		$count = "";	
		$ordenacao = "";
		$agrupamento = "";
		$limite = "";
		
		if ($codigoAgrupamento != "")
		{
			$count = "$codigoAgrupamento $count";
			
			$codigoAgrupamentoInterno = substr($codigoAgrupamento, 0, strrpos($codigoAgrupamento,','));
			
			$ordenacao = "ORDER BY $codigoAgrupamentoInterno";
			$agrupamento = "GROUP BY $codigoAgrupamentoInterno";
			
			if (is_numeric($limite))
				$limite = "LIMIT 0, $limite";

		}		
		
		$sqlTotalForum = "
			SELECT
				$count COUNT(tf.id) AS TOTAL
			FROM
				col_empresa e
				LEFT JOIN col_lotacao l on l.CD_EMPRESA = e.CD_EMPRESA
				LEFT JOIN col_usuario u ON u.lotacao = l.CD_LOTACAO
				LEFT JOIN col_foruns f ON f.CD_EMPRESA = e.CD_EMPRESA
				LEFT JOIN tb_forum tf ON u.login = tf.name
									AND tf.CD_FORUM = f.CD_FORUM
			WHERE 1=1 
			AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
			AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
			AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
			AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
			AND	(f.CD_FORUM in ($codigoForum) OR '$codigoForum' = '-1')
			$sqlDataForum
			$agrupamento
			$ordenacao
			$limite";
		
		//echo $sqlTotalForum;
		
		return DaoEngine::getInstance()->executeQuery($sqlTotalForum, true);
		
	}
	
	function executarQueryAcesso($sqlDataAcesso, $inHitsSessaoUsuario = 1, $inDownload = false)
	{
		
			global 	$codigoEmpresa, $codigoUsuario, $codigoProva, $cargo, $cargoCorrigido,
					$codigoLotacao, $codigoForum, $codigoAgrupamento, $limiteResultado;
		
					
			$usuario = false;
					
			if ($inHitsSessaoUsuario == 1)
			{
				$inHitsSessaoUsuario = "count(a.CD_USUARIO) AS TOTAL";
			}
			else if ($inHitsSessaoUsuario == 2)
			{
				
				$inHitsSessaoUsuario = "COUNT(DISTINCT(DS_ID_SESSAO)) AS TOTAL";
			}
			else 
			{
				$inHitsSessaoUsuario = "count(CD_USUARIO) AS TOTAL
										FROM
										(
										SELECT
										DISTINCT a.CD_USUARIO, DATE_FORMAT(a.DT_ACESSO, '%Y%m%d')";
				
				$sqlDataAcesso = "$sqlDataAcesso ) AS TABELA";
				
				$usuario = true;
			}
			
			if ($inDownload)
			{
				$inDownload = "AND	DS_PAGINA_ACESSO LIKE 'Download %'";
			}
			else
			{
				$inDownload = "";
			}
			
			$countb = "";
			$count = "";
			$ordenacao = "";
			$agrupamento = "";
			$limite = "";
			
			if ($codigoAgrupamento != "")
			{
				$count = "$codigoAgrupamento $count";
				
				$codigoAgrupamentoInterno = substr($codigoAgrupamento, 0, strrpos($codigoAgrupamento,','));
				
				$ordenacao = "ORDER BY $codigoAgrupamentoInterno";
				$agrupamento = "GROUP BY $codigoAgrupamentoInterno";
				
				if (is_numeric($limite))
					$limite = "LIMIT 0, $limite";
				
				$countb = "";
					
				if ($usuario)
				{
					$countb = ", $count";
					$countb = str_ireplace(", u.CD_USUARIO,", "", $countb);
					$agrupamento = str_ireplace("u.","",$agrupamento);
					$agrupamento = str_ireplace("l.","",$agrupamento);
					$ordenacao = str_ireplace("u.","",$ordenacao);
					$ordenacao = str_ireplace("l.","",$ordenacao);
					$count = str_ireplace("u.","",$count);
					$count = str_ireplace("l.","",$count);
				}
					
	
			}	
					
			$sqlTotalAcesso = "
			SELECT
				$count $inHitsSessaoUsuario $countb
			FROM
				col_empresa e
				LEFT JOIN col_lotacao l ON l.CD_EMPRESA = e.CD_EMPRESA
				LEFT JOIN col_usuario u ON u.lotacao = l.CD_LOTACAO
				LEFT JOIN col_acesso a ON u.CD_USUARIO = a.CD_USUARIO
				LEFT JOIN col_pagina_acesso pa ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
			WHERE
				NOT pa.IN_ORDENAR = -1
				$inDownload
			AND	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
			AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
			AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
			AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
			$sqlDataAcesso
			$agrupamento
			$ordenacao
			$limite";
			
			//echo $sqlTotalAcesso;
			
			return DaoEngine::getInstance()->executeQuery($sqlTotalAcesso, true);
	}
	
	function executarQueryAcessoGeral($sqlDataProva, $sqlDataAcesso, $sqlDataForum)
	{
		global 	$codigoEmpresa, $codigoUsuario, $codigoProva, $cargo, $cargoCorrigido,
				$codigoLotacao, $codigoForum, $codigoAgrupamento, $limiteResultado,
				$pesoParticipanteAvaliacao, $PesoMediaAvaliacao, $PesoParticipanteSimulado,
				$PesoMediaSimulado, $PesoContribuicaoForum, $PesoVelocidadeProva, $PesoQuantidadeDownload,
				$PesoQuantidadeHits, $PesoQuantidadeSessao, $PesoQuantidadeUsuario;
				

		$count = "";	
		$ordenacao = "";
		$agrupamento = "";
		$limite = "";
		
		if ($codigoAgrupamento != "")
		{
			$count = "$codigoAgrupamento $count";
			
			$codigoAgrupamentoInterno = substr($codigoAgrupamento, 0, strrpos($codigoAgrupamento,','));
			
			$ordenacao = "ORDER BY TOTAL DESC, $codigoAgrupamentoInterno";
			$agrupamento = "GROUP BY $codigoAgrupamentoInterno";
			
			if (is_numeric($limiteResultado))
				$limite = "LIMIT 0, $limiteResultado";

		}	
				
				
		$query ="SELECT
					$count

					sum((	SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
						 	WHERE
								pa.CD_USUARIO = u.CD_USUARIO
							AND pa.VL_MEDIA IS NOT NULL
							AND	p.IN_PROVA = 1
							AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
							$sqlDataProva)) * $pesoParticipanteAvaliacao +
				
				
					sum((	SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 0
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)) * $PesoParticipanteSimulado +
				
				
					COALESCE(ROUND(
						SUM((SELECT SUM(pa.VL_MEDIA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							 WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 1
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)) /
				
					sum((	SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 1
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)),2),0) * $PesoMediaAvaliacao +
				
				
				
					COALESCE(ROUND(
						SUM((SELECT SUM(pa.VL_MEDIA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							 WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 0
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)) /
				
					sum((	SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 0
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)),2),0) * $PesoMediaSimulado +
				
				
				
					COALESCE(ROUND(
						SUM((SELECT SUM(p.DURACAO_PROVA * 60 / TIME_TO_SEC(TIMEDIFF(pa.DT_TERMINO, pa.DT_INICIO))) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							 WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 1
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)) /
				
						sum((SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							 WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 1
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)),2),0) * $PesoVelocidadeProva +
				
				
					SUM((	SELECT COUNT(tf.id) FROM col_foruns f INNER JOIN
								tb_forum tf ON tf.CD_FORUM = f.CD_FORUM
							WHERE
								u.login = tf.name
								AND	(f.CD_FORUM in ($codigoForum) OR '$codigoForum' = '-1')
								$sqlDataForum)) * $PesoContribuicaoForum +
				
					SUM((	SELECT count(a.CD_USUARIO) AS TOTAL FROM col_acesso a
								LEFT JOIN col_pagina_acesso pa ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
							WHERE
								NOT pa.IN_ORDENAR = -1
								AND u.CD_USUARIO = a.CD_USUARIO
								AND	DS_PAGINA_ACESSO LIKE 'Download %'
								$sqlDataAcesso)) * $PesoQuantidadeDownload +
				
					SUM((	SELECT count(a.CD_USUARIO) AS TOTAL FROM col_acesso a
								LEFT JOIN col_pagina_acesso pa ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
							WHERE
								NOT pa.IN_ORDENAR = -1
								AND u.CD_USUARIO = a.CD_USUARIO
								$sqlDataAcesso)) * $PesoQuantidadeHits +
				
					SUM((	SELECT COUNT(DISTINCT(DS_ID_SESSAO)) FROM col_acesso a
								LEFT JOIN col_pagina_acesso pa ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
							WHERE
								NOT pa.IN_ORDENAR = -1
								AND u.CD_USUARIO = a.CD_USUARIO
								$sqlDataAcesso)) * $PesoQuantidadeSessao +
				
					SUM((	SELECT COUNT(DISTINCT a.CD_USUARIO + DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'))
							FROM col_acesso a
								LEFT JOIN col_pagina_acesso pa ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
							WHERE
								NOT pa.IN_ORDENAR = -1
								AND u.CD_USUARIO = a.CD_USUARIO
								$sqlDataAcesso)) * $PesoQuantidadeUsuario  AS TOTAL
				
				FROM
					col_empresa e
					LEFT JOIN col_lotacao l on l.CD_EMPRESA = e.CD_EMPRESA
					LEFT JOIN col_usuario u ON u.lotacao = l.CD_LOTACAO
				WHERE
					(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
				AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
				AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
				AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
				$agrupamento
				$ordenacao
				$limite";

				//echo $query;
		
				return DaoEngine::getInstance()->executeQuery($query, true);
		
	}
	
	if ($_POST["btnExcel"] == "") {
	?>
	<div style="width:100%;text-align:center">
		<br />
		<form method="POST">
		
			<?php
				escreveHiddensPost();
			?>
		
			<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">
			<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
			<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
		</form>	
	</div>
	<?php
		}
	?>
	
</body>
</html>

<?php

	ob_end_flush();
	

	
	function montarAgrupamento($array)
	{
		if (!is_array($array))
		{
			return $array;
		}
		
		$agrupador = "";
		
		foreach ($array as $item)
		{
			
			switch ($item)
			{
				case "1":
					$agrupador = "$agrupador l.DS_LOTACAO, l.CD_LOTACAO, ";
					break;
					
				case "2":
					$agrupador = "$agrupador u.cargofuncao, ";
					break;
				case "3":
					$agrupador = "$agrupador u.NM_USUARIO, u.NM_SOBRENOME, u.login, u.CD_USUARIO, ";
					break;
			}
			
		}
		
		return $agrupador;
	}

?>