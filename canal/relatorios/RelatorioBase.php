

<?php

	//Includes
	include('../../admin/page.php');
	include("../../include/defines.php");
	include('../../admin/framework/crud.php');
	include('../../admin/relatorio_util.php');
	include('../../admin/controles.php');

	
	$codigoEmpresa = $_SESSION["empresaID"];
	
	exibirCabecalhoPadraoRanking("Radar do Participante");

	function exibirCabecalhoPadrao($tituloRelatorio)
	{
		
		global $codigoEmpresa;
		$nomeEmpresa = obterNomeEmpresa($codigoEmpresa);
		
?>
		
			<html>
			<head>
				<title>Colaborae Consultoria e Educa��o Corporativa</title>
				<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">	
				<style type="text/css">
			
					@media print {
						.buttonsty {
						  display: none;
						}
						.textoInformativo{
							display: none;
						}
					}
					
					.textoInformativo{
						FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
					}
				
					.titRelat, .titRelatD
					{
						border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
					}
					
					.titRelatD
					{
						text-align: left;
					}
					
					.titRelatTop
					{
						border-top: 1px solid #000000; text-align: center;
					}
					
					.titRelatEsq
					{
						border-right: 1px solid #000000; border-top: 1px solid #000000;
						border-left: 1px solid #000000;
					}
					
					.titRelatRod
					{
						border-right: 1px solid #000000; border-top: 1px solid #000000;
						border-bottom: 1px solid #000000;
					}
					
					.titRelatEsqRod
					{
						border-right: 1px solid #000000; border-top: 1px solid #000000;
						border-bottom: 1px solid #000000;
						border-left: 1px solid #000000;
					}
					
					.bdLat
					{
						border-right: 1px solid #000000;
					}
					
					.bdLatEsq
					{
						border-right: 1px solid #000000;
						border-left: 1px solid #000000;
					}
					
					.bdEsq
					{
						border-left: 1px solid #000000;
					}
			
					.bdRod
					{
						border-bottom: 1px solid #000000;
					}
					
					.tabelaRanking td
					{
						padding: 1px 3px 1px 3px;
					}
				</style>
				
			</head>
			
			<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
				<tr style="height: 30px">
					<td class='textblk'>
						<p><b><?php echo $nomeEmpresa; ?></b></p>
					</td>
					<td width="65%" class="textblk" align="center">
						<b>
							<?php echo $tituloRelatorio; ?>
						</b>
					</td>
					<td style="text-align: right" class='textblk'>
						<p><?php echo date("d/m/Y") ?></p>
					</td>
				</tr>
			</table>
			
<?php
		
	}
	
	function obterNomeEmpresa($codigoEmpresa)
	{
		
		$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		$linhaEmpresa = mysql_fetch_array($resultado);
		$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
		return  $nomeEmpresa;
		
	}
	
	function obterNomeCiclo()
	{
		
		$codigoCiclo = $_REQUEST["hdnCiclo"];
		
		$nomeCiclo = "Todos os Ciclos";
		if ($codigoCiclo != -1 && $codigoCiclo != "")
		{
			//Obter o nome do Ciclo
			$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
			$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
			$linhaCiclo = mysql_fetch_array($resultado);
			$nomeCiclo = $linhaCiclo["NM_CICLO"];
			//Fim obter o nome do Ciclo
		}
		
		return $nomeCiclo;
		
	}

	function exibirCabecalhoPadraoRanking($tituloRelatorio)
	{
		
		global $codigoEmpresa;
		
		exibirCabecalhoPadrao($tituloRelatorio);
		
		$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];
		
		$totalUsuariosEmpresa = obterTotalUsuarios($codigoEmpresa);
		
		if ($tipoRelatorio == "G")
			$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);
		
		
		
		
?>
		
		<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
			<tr class="textblk">
				<td class="textblk">
					<b>Nome do Grupo Gerencial: <?php echo $nomeFiltro; ?></b>
				</td>
			</tr>
			<tr class="textblk">
				<td>
					<b>Ciclo: <?php echo obterNomeCiclo(); ?></b>
				</td>
			</tr>
			<tr class="textblk">
				<td width="100%">
					<b>Per�odo:&nbsp;
									<?php
														if($dataInicioTexto!=""){
															echo "de $dataInicioTexto ";
															if($dataTerminoTexto!=""){
																echo "a $dataTerminoTexto";
															}
														}else{
															echo "Todos ";
														}
									?>
					</b>
				</td>
			</tr>
			<tr class="textblk">
				<td>
					Cadastrados da Empresa: <?php echo $totalUsuariosEmpresa; ?>
				</td>
			</tr>
			<?php
			if ($tipoRelatorio == "G")
			{
			?>
			<tr class="textblk">
				<td>
					Cadastrados do Grupo: <?php echo $totalUsuarioGrupo; ?>
				</td>
			</tr>
			<?php
			}
			?>
			<tr class="textblk">
				<td>
					Quantidade de Participantes Ranqueados: <?php echo $quantidadeRankeados; ?>
				</td>
			</tr>
			<tr class="textblk">
				<td>
					Quantidade de Participantes N�o Ranqueados: <?php echo $quantidadeNaoRankeados; ?>
				</td>
			</tr>	
		</table>
		
<?php
	}
	
?>