<?php 
//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include ('relatorio_ranking_util.php');
include('relatorio_acesso_util.php');

$tituloRelatorio = "Resultados de Pesquisa";

$codigoFiltro = ($_GET["id"]==""?0:$_GET["id"]);
$POST = obterPost();

$codigoEmpresa = $_SESSION["empresaID"];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$cargo = $POST['cboCargo'];
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$codigoCiclo = $_REQUEST["hdnCiclo"];
//$codigoPesquisa = $_REQUEST["cboPesquisa"];

$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

//Obter os c�digos das pesquisas
$sql = "SELECT
            *
        FROM
            col_pesquisa
        WHERE
            CD_EMPRESA = $codigoEmpresa
        AND IN_ATIVO = 1
        AND DATE_FORMAT(DT_INICIO_RELATORIO, '%Y%m%d') <= DATE_FORMAT(SYSDATE(), '%Y%m%d')
        AND DATE_FORMAT(DT_TERMINO_RELATORIO, '%Y%m%d') >= DATE_FORMAT(SYSDATE(), '%Y%m%d')";

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

$codigoPesquisa = '0';

while($linha = mysql_fetch_array($resultado)){

    if($codigoPesquisa == '0'){
        $codigoPesquisa = $linha["CD_PESQUISA"];
    }else{
        $codigoPesquisa = $codigoPesquisa . "," . $linha["CD_PESQUISA"];
    }


}

//echo $codigoPesquisa;

$nomeLotacao = "{$POST["nomeHierarquia"]}: {$POST["nomeLotacao"]}";
if ($POST["nomeLotacao"] == "")
{
	$nomeLotacao = "&nbsp;";
}

//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa
mysql_free_result($resultado);


$sql = "SELECT
			COUNT(1) AS TOTAL_RESPOSTAS
		FROM
			col_usuario_pesquisa up
			INNER JOIN col_usuario u ON u.CD_USUARIO = up.CD_USUARIO
			INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
		WHERE
			up.CD_EMPRESA = $codigoEmpresa AND up.CD_PESQUISA IN ($codigoPesquisa)
			AND (l.CD_LOTACAO IN ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao%')
			AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
			";


$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaPesquisa = mysql_fetch_array($resultado);
$totalRespostas = $linhaPesquisa["TOTAL_RESPOSTAS"];
//Fim obter nome da empresa
mysql_free_result($resultado);

if ($_POST["btnExcel"] != "") {

	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=RelatorioDesempenho.xls");

}

?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	
<?php
if ($_POST["btnExcel"] == "") {
?>
	
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">	

<?php 
}
?>	

	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		
		.tabelaRanking td
		{
			padding: 1px 3px 1px 3px;
		}
	</style>
	
</head>
<body>
<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk' nowrap>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	
	<?php 
	if(exibeGrupo())
	{
		$sql = "SELECT NM_FILTRO FROM col_filtro WHERE CD_FILTRO = $codigoFiltro";
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		$linhaGrupo = mysql_fetch_array($resultado);
		$nomeGrupo = $linhaGrupo["NM_FILTRO"];
	?>
	<tr class="textblk">
		<td width="100%">
			<b>Grupo Gerencial:&nbsp;<?php echo $nomeGrupo;?>
			</b>
		</td>
	</tr>
	<?php 
	}
	?>
	<tr class="textblk">
		<td width="100%">
			<b>Quantidade de Respostas:&nbsp;<?php echo $totalRespostas;?>
			</b>
		</td>
	</tr>
	
	
	<td class="textblk">
		<b><?php echo $nomeLotacao; ?></b>
	</td>
	</tr>
</table>

<?php 

//OBTEM MEDIA DA EMPRESA
$sqlEmpresa = "
		SELECT
  			DS_PERGUNTA, SUM(MEDIA_EMPRESA) AS MEDIA_EMPRESA, SUM(MEDIA_GRUPO) AS MEDIA_GRUPO, SUM(MEDIA_LOTACAO) AS MEDIA_LOTACAO
		FROM
		
		(

		SELECT
			pp.NR_ORDEM, pp.DS_PERGUNTA, AVG(if(VL_RESPOSTA>1,VL_RESPOSTA,null)) AS MEDIA_EMPRESA, NULL AS MEDIA_GRUPO, NULL AS MEDIA_LOTACAO
		FROM
			col_pergunta_pesquisa pp
			LEFT JOIN col_resposta_pesquisa rp ON pp.CD_PERGUNTA_PESQUISA = rp.CD_PERGUNTA_PESQUISA
		WHERE
			pp.CD_EMPRESA = $codigoEmpresa AND pp.CD_PESQUISA IN ($codigoPesquisa)
		GROUP BY
			pp.CD_PERGUNTA_PESQUISA, pp.DS_PERGUNTA, pp.NR_ORDEM";

			
$sqlGrupo = "UNION
				
		SELECT
			pp.NR_ORDEM, pp.DS_PERGUNTA, NULL, AVG(if(VL_RESPOSTA>1,VL_RESPOSTA,null)), NULL
		FROM
			col_pergunta_pesquisa pp
			INNER JOIN col_resposta_pesquisa rp ON pp.CD_PERGUNTA_PESQUISA = rp.CD_PERGUNTA_PESQUISA
			INNER JOIN col_usuario u ON u.CD_USUARIO = rp.CD_USUARIO
			INNER JOIN col_filtro_lotacao filtro_lotacao ON filtro_lotacao.CD_LOTACAO = u.lotacao
			INNER JOIN col_filtro filtro ON filtro.CD_FILTRO = filtro_lotacao.CD_FILTRO
		WHERE
			pp.CD_EMPRESA = $codigoEmpresa AND pp.CD_PESQUISA IN ($codigoPesquisa)
			AND filtro.CD_FILTRO = $codigoFiltro
		GROUP BY
  			pp.CD_PERGUNTA_PESQUISA, pp.DS_PERGUNTA, pp.NR_ORDEM";
		
$sqlLotacao = "UNION
		
		SELECT
			pp.NR_ORDEM, pp.DS_PERGUNTA, NULL, NULL, AVG(if(VL_RESPOSTA>1,VL_RESPOSTA,null))
		FROM
			col_pergunta_pesquisa pp
			INNER JOIN col_resposta_pesquisa rp ON pp.CD_PERGUNTA_PESQUISA = rp.CD_PERGUNTA_PESQUISA
			INNER JOIN col_usuario u ON u.CD_USUARIO = rp.CD_USUARIO
			INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
		WHERE
			pp.CD_EMPRESA = $codigoEmpresa AND pp.CD_PESQUISA IN ($codigoPesquisa)
  			AND (l.CD_LOTACAO = $codigoLotacao OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao%')
  			AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		GROUP BY
  			pp.CD_PERGUNTA_PESQUISA, pp.DS_PERGUNTA, pp.NR_ORDEM";
  			
$sqlFinal = ")
  			AS TABELA
		GROUP BY
  			DS_PERGUNTA, NR_ORDEM
		ORDER BY NR_ORDEM
			";

$sql = $sqlEmpresa;

if (exibeGrupo())
{
	$sql = "$sql $sqlGrupo";
}

if (exibeLotacao())
{
	$sql = "$sql $sqlLotacao";
}

$sql = "$sql $sqlFinal";

$resultadoEmpresa = DaoEngine::getInstance()->executeQuery($sql,true);

?>

<table cellpadding="1" cellspacing="0" border="0" style="margin-left: 2.5%; margin-right: 2.5%;">
	<tr class="textblk" >
		<td style="width:155px" style="border-bottom: 1px solid #000;"><b>Quest�es Avaliadas</b></td>
		<td width="128" align="center" style="border-bottom: 1px solid #000;"><b>Empresa</b></td>
		<?php 
		if (exibeGrupo())
		{
		?>
		<td width="128" align="center" style="border-bottom: 1px solid #000;"><b>Grupo Gerencial</b></td>
		<?php 
		}
		if (exibeLotacao())
		{
		?>
		<td width="128" align="center" style="border-bottom: 1px solid #000;"><b>Lota��o</b></td>
		<?php 
		}
		?>
		
	</tr>
	
<?php 
	$somaNotas = 0;
	$somaNotasGrupo = 0;
	$somaNotasLotacao = 0;
	$totalLinhas = 0;
	while($linha = mysql_fetch_array($resultadoEmpresa))
	{
		$totalLinhas++;
		$somaNotas = $somaNotas + $linha["MEDIA_EMPRESA"];
		$somaNotasGrupo = $somaNotasGrupo + $linha["MEDIA_GRUPO"];
		$somaNotasLotacao = $somaNotasLotacao + $linha["MEDIA_LOTACAO"];
?>		
		<tr class="textblk" nowrap>
			<td><?php echo $linha["DS_PERGUNTA"];?></td>
			<td align="center"><?php echo formatarValores($linha["MEDIA_EMPRESA"],2) ;?></td>
			<?php 
			if (exibeGrupo())
			{
			?>
			<td align="center"><?php echo formatarValores($linha["MEDIA_GRUPO"],2) ;?></td>
			<?php
			} 
			if (exibeLotacao())
			{
			?>
			<td align="center"><?php echo formatarValores($linha["MEDIA_LOTACAO"],2) ;?></td>
			<?php 
			}
			?>
		</tr>
<?php 
	}

?>
	
		<tr class="textblk" >
		<td style="width:155px" style="border-top: 1px solid #000; border-bottom: 1px solid #000;"><b>M�dia</b></td>
		<td width="128" align="center" style="border-top: 1px solid #000; border-bottom: 1px solid #000;"><b><?php echo formatarValores($somaNotas / $totalLinhas,2);?></b></td>
		<?php 
		if (exibeGrupo())
		{
		?>
		<td width="128" align="center" style="border-top: 1px solid #000; border-bottom: 1px solid #000;"><b><?php echo formatarValores($somaNotasGrupo / $totalLinhas,2);?></b></td>
		<?php
		} 
		if (exibeLotacao())
		{
		?>
		<td width="128" align="center" style="border-top: 1px solid #000; border-bottom: 1px solid #000;"><b><?php echo formatarValores($somaNotasLotacao / $totalLinhas,2);?></b></td>
		<?php 
		}
		?>
	</tr>
	
</table>
<br /><br />
<table cellpadding="1" cellspacing="0" border="0" style="margin-left: 2.5%; margin-right: 2.5%;">
	<tr class="textblk" >
		<td><b>Sugest�es de Melhorias (at� 150 caracteres)</b></td>
	</tr>
	
<?php 
$sql = "SELECT
  			u.NM_USUARIO, up.DS_COMENTARIO
		FROM
  			col_usuario u
  			INNER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
  			INNER JOIN col_usuario_pesquisa up ON up.CD_USUARIO = u.CD_USUARIO
		WHERE
			up.DS_COMENTARIO IS NOT NULL AND up.DS_COMENTARIO != ''
			AND up.CD_EMPRESA = $codigoEmpresa AND up.CD_PESQUISA IN ($codigoPesquisa)
  			AND (l.CD_LOTACAO IN ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao%')
  			AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
  			";

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

?>
	<tr class="textblk" >
		<td>Total de Registros: <?php echo mysql_num_rows($resultado); ?></td>
	</tr>

	<tr>&nbsp;</tr>

<?php

	while($linha=mysql_fetch_array($resultado))
	{
	?>
		<tr class="textblk" >
			<td style="border: 1px dotted #000000; padding: 4px;"><?php echo "{$linha["DS_COMENTARIO"]} ({$linha["NM_USUARIO"]})"; ?></td>
		</tr>
	<?php 
	}

?>
	
</table>

<?php

if ($_POST["btnExcel"] == "") {

?>

<div style="width:100%;text-align:center">

		<br />

		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']; ?>">

		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">

		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">

		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>

		</form>	

</div>

</body>
</html>
<?php 
}


function exibeLotacao()
{
	return ($_GET["hdnLotacao"] != "" && $_GET["hdnLotacao"] > 0);
}	


function exibeGrupo()
{
	return ($_GET["id"] != "" && $_GET["id"] > 0);
}

function formatarValores($valor, $decimal = 2, $seVazio = "-")
{
	if ($valor == "")
	{
		return $seVazio;
	}
	
	return number_format(round($valor,$decimal), $decimal, ",", "");
	
}
?>