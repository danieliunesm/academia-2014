<?php
//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include ('relatorio_ranking_util.php');
include('relatorio_acesso_util.php');

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

$tituloTotal = "";
$tituloRelatorio = "Eleg�veis para Segunda Chamada";

$POST = obterPost();

//Obter Parametros
$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];

if ($tipoRelatorio == "B")
	$tituloRelatorio = "Avalia��es e Segundas Chamadas";

$codigoEmpresa = $POST['cboEmpresa'];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$hierarquia = isset($_REQUEST["hdnHierarquico"])? $_REQUEST["hdnHierarquico"]: 0; 
$codigoCiclo = $_REQUEST["hdnCiclo"];

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$dataAtual = $dataInicial;

// Fim obter parametros



//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa

$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

$quantidadeCiclo = 1;
$nomeCiclo = "Todos os Ciclos";
$ciclos=array();
$sql = "";
if ($codigoCiclo != -1)
{
	$sql = "SELECT CD_CICLO, NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
}
else
{
	$sql = "SELECT CD_CICLO, NM_CICLO FROM col_ciclo WHERE CD_EMPRESA = $codigoEmpresa ORDER BY DT_INICIO";
}

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

while ($linha = mysql_fetch_array($resultado))
{
	$ciclos[$linha["CD_CICLO"]] = array();
	$ciclos[$linha["CD_CICLO"]]["NM_CICLO"] = $linha["NM_CICLO"];
	$nomeCiclo = $linha["NM_CICLO"];
}

$sql = "SELECT CD_PROVA, DS_PROVA, p.CD_CICLO FROM col_prova p INNER JOIN col_ciclo c ON c.CD_CICLO = p.CD_CICLO WHERE c.CD_EMPRESA = $codigoEmpresa AND IN_PROVA = 1 ORDER BY DS_PROVA";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

while ($linha = mysql_fetch_array($resultado))
{
	if ($ciclos[$linha["CD_CICLO"]]["PROVAS"] == null)
		$ciclos[$linha["CD_CICLO"]]["PROVAS"] = array();
		
	
	 $ciclos[$linha["CD_CICLO"]]["PROVAS"][] = $linha;
}


$totalUsuariosEmpresa = obterTotalUsuariosGrupamento($codigoEmpresa, -1, -1, -1, $dataFinalQuery);
$quantidadeCadastradosEmpresa = $totalUsuariosEmpresa;
$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataFinalQuery);


$mediaGeralProvasCalculada = 0;
$quantidadeTotalAvaliacoesMaisSegundaChamada = 0;
obterProvasMediaCiclo();

if ($_POST["btnExcel"] != "") {

	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=RelatorioDesempenho.xls");

}

?>

<html>
<head>
	<meta equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	<?php

	

	if ($_POST["btnExcel"] == "") {



	?>

	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">		

	<?php

	}

	?>
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		
		.tabelaRanking td
		{
			padding: 1px 3px 1px 3px;
		}
	</style>
	
</head>
<body>
<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk'>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">

	<?php
	if (!(ehEmpresa()))
	{
	?>

	<tr class="textblk">
		<td>
			<b>
			<?php
				if (ehFiltro()){ 
					echo "Nome do Grupo Gerencial: {$POST["nomeFiltro"]}";
				}elseif (ehLotacao()) {
					$labelLotacao = "Nome da Lota��o";
					if ($POST["nomeHierarquia"] != "") $labelLotacao = $POST["nomeHierarquia"];
					echo "$labelLotacao: {$POST["nomeLotacao"]}";
				}
			?>
			</b>
		</td>
	</tr>
	<?php
	}
	?>
	<tr class="textblk">
		<td>
			<b>Ciclo: <?php echo $nomeCiclo; ?></b>
		</td>
	</tr>
	<tr class="textblk">
		<td width="100%">
			<b>Per�odo:&nbsp;
							<?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
		</b></td>
	</tr>
	<?php
	if (ehEmpresa())
	{
	?>
	<tr class="textblk">
		<td>
			<?php 
					echo "Cadastrados da Empresa: $totalUsuariosEmpresa";	
				
			?>
			
		</td>
	</tr>
	<?php
		
	}
	else
	{
	?>
	<tr class="textblk">
		<td>
			<?php 
					$labelCadastrados = "Cadastrados no Grupo";
					if (ehLotacao())
					{
						$labelCadastrados = "Cadastrados na Lota��o";
						if ($POST["nomeHierarquia"] != "") $labelCadastrados = "Cadastrados no(a) {$POST["nomeHierarquia"]}";
					}
					
					echo "$labelCadastrados: $totalUsuarioGrupo";
			?>
		</td>
	</tr>
	<?php
	}
	?>
	<tr class="textblk">
		<td>
			Quantidade de Avalia��es: <?php echo $quantidadeTotalAvaliacoesMaisSegundaChamada;?>
		</td>
	</tr>
</table>





<br />

<?php 

if (exibeTabela())
{
?>

<table class="tabelaRanking" cellspacing="0" align="center" width="95%" border="0" style="border: 1px solid black;">
	<tr class='textblk' style="font-weight: bold;">
		<?php
			$labelLotacao = "Lota��o";
			if ($POST["nomeHierarquia"] != "") $labelLotacao = "Respons�vel";
			echo "<td class=\"bdLat\" rowspan=\"2\">Participante</td>
				 <td class=\"bdLat\" rowspan=\"2\">Login</td>
				 <td class=\"bdLat\" rowspan=\"2\">Cargo/Fun��o</td>
				 <td class=\"bdLat\" rowspan=\"2\">$labelLotacao</td>";
		
			
			
			$tdProvas = "";
			foreach($ciclos as $ciclo)
			{

				if ($ciclo["NM_CICLO"] == null) continue;
				
				$colspan = count($ciclo["PROVAS"]);
				echo "<td class='bdLat' align=\"center\" colspan=\"$colspan\">{$ciclo["NM_CICLO"]}</td>";
				
				foreach ($ciclo["PROVAS"] as $prova)
				{
					$tdProvas = "$tdProvas<td class='bdLat' align=\"center\" >{$prova["DS_PROVA"]}</td>";
				}
			}
			
		?>
		
	</tr>
	<tr class='textblk' style="font-weight: bold;">
		<?php echo $tdProvas; ?>
	</tr>

	<?php
		
		$linhas = obterDadosSegundaChamada($codigoEmpresa, -1, $codigoLotacao, $codigoCiclo, $tipoRelatorio != "B");
		
		$codigoUsuarioAnterior = 0;
		$provas=null;
		$aprovacao = null;
		while ($linha = mysql_fetch_array($linhas))
		{
			
			if ($codigoUsuarioAnterior != $linha["CD_USUARIO"])
			{
				
				if ($codigoUsuarioAnterior != 0)
				{
					foreach($ciclos as $ciclo)
					{
		
						if ($ciclo["NM_CICLO"] == null) continue;
						
						foreach ($ciclo["PROVAS"] as $prova)
						{
							$codigoProva = $prova["CD_PROVA"];
							 if ($provas[$codigoProva] == null)
							 	$provas[$codigoProva] = "&nbsp;";
							 $aprovado = $aprovacao[$codigoProva];
							 
							 $vermelho = "";
							 if ($aprovado == 0) $vermelho = "color: red";
							 
							 echo "<td class=\"titRelat\" style='text-align: center; $vermelho'>{$provas[$codigoProva]}</td>";
						}
					}
					echo 	"</tr>";
				}
				
				$provas = array();
				$aprovacao = array();
				echo "<tr  class='textblk'>";
				
				echo "<td class=\"titRelatD\" >{$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}</td>
					 <td class=\"titRelat\" style='text-align: left;' >{$linha["login"]}</td>
					 <td class=\"titRelat\" style='text-align: left;'>{$linha["cargofuncao"]}</td>
					 <td class=\"titRelat\" style='text-align: left;'>{$linha["DS_LOTACAO"]}</td>";
				
				$codigoUsuarioAnterior = $linha["CD_USUARIO"];
			}
			
			if ($linha["VL_MEDIA"] == null)
			{
				if ($linha["APROVADO"] == 0)
					$provas[$linha["CD_PROVA"]] = "-";
				else
					$provas[$linha["CD_PROVA"]] = "&nbsp;";
			}
			else
			{
				$provas[$linha["CD_PROVA"]] = formatarValores($linha["VL_MEDIA"] * 10, 2, "0");
			}
				
			$aprovacao[$linha["CD_PROVA"]] = $linha["APROVADO"];

			
			
		}

		foreach($ciclos as $ciclo)
		{

			if ($ciclo["NM_CICLO"] == null) continue;
			
			foreach ($ciclo["PROVAS"] as $prova)
			{
				$codigoProva = $prova["CD_PROVA"];
				 if ($provas[$codigoProva] == null)
				 	$provas[$codigoProva] = "&nbsp;";
				 $aprovado = $aprovacao[$codigoProva];
							 
				 $vermelho = "";
				 if ($aprovado == 0) $vermelho = "color: red";
				 	
				 echo "<td class=\"titRelat\" style='text-align: center; $vermelho'>{$provas[$codigoProva]}</td>";
			}
		}
		echo 	"</tr>";
	?>
	
</table>
<?php 
}


function ehSuperusuario($linha)
{
	return ($linha["tipo"] != null && $linha["tipo"] < 0);
}

function formatarValores($valor, $decimal = 2, $seVazio = "-")
{
	if ($valor == "" || $valor == null || $valor == "-")
	{
		return $seVazio;
	}
	
	if (round($valor,0) == $valor)
	{
		return round($valor,0);
	}
	
	return number_format(round($valor,$decimal), $decimal, ",", "");		
	
}

function aplicarNaoRanqueado($linha, $campoRanking, $campoFator)
{
	$campoFator = $campoFator . "_ACESSO";
	
	if (!$linha[$campoFator])
		return "NR";
		
	return $linha[$campoRanking];
	
}

function exibeTabela()
{
	$retorno = true;
	return $retorno;
	
}

function ehFiltro()
{
	return (isset($_REQUEST["id"]) && $_REQUEST["id"] > 0);
}

function ehLotacao()
{
	return (isset($_REQUEST["hdnLotacao"]) && $_REQUEST["hdnLotacao"] > 0);
}

function ehEmpresa()
{
	return (!(ehLotacao() || ehFiltro()));
}

?>

<?php

		if ($_POST["btnExcel"] == "") {

	?>

	<div style="width:100%;text-align:center">

		<br />

		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']; ?>">

		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">

		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">

		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>

		</form>	

	</div>

	<?php

		}

	?>

</body>
</html>