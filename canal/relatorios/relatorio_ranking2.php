<?php

//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��o";
}
else
{
	$labelAvaliacao = "Avalia��o";
}

$tituloTotal = "";
$tituloRelatorio = "Ranking";

$POST = obterPost();

//Obter Parametros
$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];

$codigoEmpresa = $POST['cboEmpresa'];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;

$codigoCiclo = $_REQUEST["hdnCiclo"];

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$dataAtual = $dataInicial;

// Fim obter parametros
$campoSelect = "DS_LOTACAO";
$tituloCampo = "Lota��o";
$tabelaExtra = "";
$whereExtra = "";

$complementoTitulo = "";
if ($_GET["cboEmpresa"])
{
	$complementoTitulo = "da Empresa";
}
else 
{
	$complementoTitulo = "do Grupo: {$POST["nomeFiltro"]}";
}


switch ($tipoRelatorio)
{
	case "L":
		$campoSelect = "DS_LOTACAO";
		$tituloCampo = "Lota��o";
		$tituloRelatorio = "Ranking de Lota��es $complementoTitulo";
		break;
	case "U":
		$campoSelect = "login, NM_USUARIO, NM_SOBRENOME, DS_LOTACAO, cargofuncao";
		$tituloCampo = "Participante";
		$tituloRelatorio = "Ranking de Participantes $complementoTitulo";
		break;
	case "G":
		$campoSelect = "NM_FILTRO";
		$tituloCampo = "Grupo";
		$tituloRelatorio = "Ranking de Grupos da Empresa";
		$tabelaExtra = "INNER JOIN col_filtro_lotacao filtro_lotacao ON filtro_lotacao.CD_LOTACAO = l.CD_LOTACAO
  						INNER JOIN col_filtro filtro ON filtro.CD_FILTRO = filtro_lotacao.CD_FILTRO";
		$whereExtra = "AND filtro.IN_FILTRO_RANKING = 1 AND filtro.IN_ATIVO = 1";
		break;
}

//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa


$nomeCiclo = "Todos os Ciclos";
if ($codigoCiclo != -1)
{
	//Obter o nome do Ciclo
	$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaCiclo = mysql_fetch_array($resultado);
	$nomeCiclo = $linhaCiclo["NM_CICLO"];
	//Fim obter o nome do Ciclo
}


$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

$totalUsuariosEmpresa = obterTotalUsuarios($codigoEmpresa);
$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);

//ObterFatoresDasEmpresa
$sqlAcesso = "
SELECT
  COUNT(DISTINCT IF(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery',a.CD_USUARIO, null), (SELECT CD_CICLO FROM col_ciclo c WHERE DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= DATE_FORMAT(c.DT_INICIO, '%Y%m%d') AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= DATE_FORMAT(c.DT_TERMINO, '%Y%m%d'))) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_PARTICIPACAO,
  COUNT(DISTINCT a.DS_ID_SESSAO, IF(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery',a.CD_USUARIO,null)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_ACESSO,
  COUNT(DISTINCT a.CD_USUARIO, IF(DS_PAGINA_ACESSO LIKE 'DOWNLOAD%' AND (DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery'),DS_PAGINA_ACESSO ,null)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_DOWNLOAD
FROM
  col_usuario u
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  LEFT JOIN col_acesso a ON a.CD_USUARIO = u.CD_USUARIO
  LEFT JOIN col_pagina_acesso pa ON pa.CD_PAGINA_ACESSO = a.CD_PAGINA_ACESSO
WHERE
  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1);";

$resultadoAcesso = DaoEngine::getInstance()->executeQuery($sqlAcesso,true);
$linhaEmpresaAcesso = mysql_fetch_array($resultadoAcesso);
$fatorEmpresaParticipacao = tratarDividirZero($linhaEmpresaAcesso["FATOR_PARTICIPACAO"]);
$fatorEmpresaAcesso = tratarDividirZero($linhaEmpresaAcesso["FATOR_ACESSO"]);
$fatorEmpresaDownload = tratarDividirZero($linhaEmpresaAcesso["FATOR_DOWNLOAD"]);


$sqlAvaliacao = "
SELECT
  COUNT(IF(p.IN_PROVA = 1 AND pa.VL_MEDIA IS NOT NULL AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), u.CD_USUARIO, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_PARTICIPAO_AVALIACAO,
  COUNT(IF(p.IN_PROVA = 0 AND pa.VL_MEDIA IS NOT NULL AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), u.CD_USUARIO, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_PARTICIPAO_SIMULADO,
  COALESCE(SUM(IF(p.IN_PROVA = 0 AND pa.VL_MEDIA IS NOT NULL AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), pa.NR_TENTATIVA_USUARIO, NULL)),0) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_REALIZACAO_SIMULADO,
  COALESCE(AVG(IF(p.IN_PROVA = 1 AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), pa.VL_MEDIA, NULL)),0) AS MEDIA_AVALIACAO,
  COALESCE(AVG(IF(p.IN_PROVA = 0 AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), pa.VL_MEDIA, NULL)),0) AS MEDIA_SIMULADO
FROM
  col_usuario u
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  LEFT JOIN col_prova_aplicada pa ON pa.CD_USUARIO = u.CD_USUARIO
  LEFT JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
WHERE
  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1);";

$resultadoAvaliacao = DaoEngine::getInstance()->executeQuery($sqlAvaliacao,true);
$linhaEmpresaAvaliacao = mysql_fetch_array($resultadoAvaliacao);
$fatorEmpresaParticipacaoAvaliacao = tratarDividirZero($linhaEmpresaAvaliacao["FATOR_PARTICIPAO_AVALIACAO"]);
$fatorEmpresaParticipacaoSimulado = tratarDividirZero($linhaEmpresaAvaliacao["FATOR_PARTICIPAO_SIMULADO"]);
$fatorEmpresaRealizacaoSimulado = tratarDividirZero($linhaEmpresaAvaliacao["FATOR_REALIZACAO_SIMULADO"]);
$fatorEmpresaMediaAvaliacao = tratarDividirZero($linhaEmpresaAvaliacao["MEDIA_AVALIACAO"]);
$fatorEmpresaMediaSimulado = tratarDividirZero($linhaEmpresaAvaliacao["MEDIA_SIMULADO"]);

$sqlForum = "
SELECT
  COUNT(DISTINCT IF(DATE_FORMAT(fo.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(fo.data, '%Y%m%d') <= '$dataFinalQuery' ,fo.name, NULL), fo.CD_FORUM) / COUNT(DISTINCT(u.CD_USUARIO)) as FATOR_PARTICIPACAO_FORUM,
  COUNT(IF(DATE_FORMAT(fo.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(fo.data, '%Y%m%d') <= '$dataFinalQuery' ,fo.id, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_CONTRIBUICAO_FORUM
FROM
  col_usuario u
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  LEFT JOIN col_foruns f ON f.CD_EMPRESA = u.empresa
  LEFT JOIN tb_forum fo ON fo.cd_empresa = u.empresa AND f.CD_FORUM = fo.CD_FORUM AND u.login = fo.name
WHERE
  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1);";

$resultadoForum = DaoEngine::getInstance()->executeQuery($sqlForum,true);
$linhaEmpresaForum = mysql_fetch_array($resultadoForum);
$fatorEmpresaParticipacaoForum = tratarDividirZero($linhaEmpresaForum["FATOR_PARTICIPACAO_FORUM"]);
$fatorEmpresaContribuicaoForum = tratarDividirZero($linhaEmpresaForum["FATOR_CONTRIBUICAO_FORUM"]);


//COUNT(DISTINCT a.CD_USUARIO, IF(DS_PAGINA_ACESSO = 'FORUM' ,DS_PAGINA_ACESSO ,null)) / COUNT(DISTINCT(u.CD_USUARIO)) AS FATOR_PARTICIPACAO_FORUM
$sqlAcesso = "
SELECT
  $campoSelect,
  COUNT(DISTINCT IF(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery',a.CD_USUARIO,null), (SELECT CD_CICLO FROM col_ciclo c WHERE DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= DATE_FORMAT(c.DT_INICIO, '%Y%m%d') AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= DATE_FORMAT(c.DT_TERMINO, '%Y%m%d'))) / COUNT(DISTINCT(u.CD_USUARIO)) / $fatorEmpresaParticipacao AS FATOR_PARTICIPACAO,
  COUNT(DISTINCT a.DS_ID_SESSAO, IF(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery',a.CD_USUARIO,null)) / COUNT(DISTINCT(u.CD_USUARIO)) / $fatorEmpresaAcesso AS FATOR_ACESSO,
  COUNT(DISTINCT a.CD_USUARIO, IF(DS_PAGINA_ACESSO LIKE 'DOWNLOAD%' AND (DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery'),DS_PAGINA_ACESSO ,null)) / COUNT(DISTINCT(u.CD_USUARIO)) / $fatorEmpresaDownload AS FATOR_DOWNLOAD,
  IF((u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1') AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1') AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo')), 1, 0) AS FILTRADO
FROM
  col_usuario u
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  $tabelaExtra
  LEFT JOIN col_acesso a ON a.CD_USUARIO = u.CD_USUARIO
  LEFT JOIN col_pagina_acesso pa ON pa.CD_PAGINA_ACESSO = a.CD_PAGINA_ACESSO
WHERE
  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
  $whereExtra
GROUP BY
  $campoSelect
ORDER BY
  $campoSelect;";

$resultadoAcesso = DaoEngine::getInstance()->executeQuery($sqlAcesso,true);

$sqlAvaliacao = "
SELECT
  $campoSelect,
  COUNT(IF(p.IN_PROVA = 1 AND pa.VL_MEDIA IS NOT NULL AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), u.CD_USUARIO, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) / $fatorEmpresaParticipacaoAvaliacao AS FATOR_PARTICIPAO_AVALIACAO,
  COUNT(IF(p.IN_PROVA = 0 AND pa.VL_MEDIA IS NOT NULL AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), u.CD_USUARIO, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) / $fatorEmpresaParticipacaoSimulado AS FATOR_PARTICIPAO_SIMULADO,
  SUM(IF(p.IN_PROVA = 0 AND pa.VL_MEDIA IS NOT NULL AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), pa.NR_TENTATIVA_USUARIO, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) / $fatorEmpresaRealizacaoSimulado AS FATOR_REALIZACAO_SIMULADO,
  AVG(IF(p.IN_PROVA = 1 AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), pa.VL_MEDIA, NULL)) / $fatorEmpresaMediaAvaliacao AS MEDIA_AVALIACAO,
  AVG(IF(p.IN_PROVA = 0 AND (DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'), pa.VL_MEDIA, NULL)) / $fatorEmpresaMediaSimulado AS MEDIA_SIMULADO
FROM
  col_usuario u
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  $tabelaExtra
  LEFT JOIN col_prova_aplicada pa ON pa.CD_USUARIO = u.CD_USUARIO
  LEFT JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
WHERE
  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
  $whereExtra
GROUP BY
  $campoSelect
ORDER BY
  $campoSelect;";

$resultadoAvaliacao = DaoEngine::getInstance()->executeQuery($sqlAvaliacao,true);

$sqlForum = "
SELECT
  $campoSelect,
  COUNT(DISTINCT IF(DATE_FORMAT(fo.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(fo.data, '%Y%m%d') <= '$dataFinalQuery' ,fo.name, NULL), fo.CD_FORUM) / COUNT(DISTINCT(u.CD_USUARIO)) / $fatorEmpresaParticipacaoForum as FATOR_PARTICIPACAO_FORUM,
  COUNT(IF(DATE_FORMAT(fo.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(fo.data, '%Y%m%d') <= '$dataFinalQuery' ,fo.id, NULL)) / COUNT(DISTINCT(u.CD_USUARIO)) / $fatorEmpresaContribuicaoForum AS FATOR_CONTRIBUICAO_FORUM
FROM
  col_usuario u
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  $tabelaExtra
  LEFT JOIN col_foruns f ON f.CD_EMPRESA = u.empresa
  LEFT JOIN tb_forum fo ON fo.cd_empresa = u.empresa AND f.CD_FORUM = fo.CD_FORUM AND u.login = fo.name
WHERE
  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
  $whereExtra
GROUP BY
  $campoSelect
ORDER BY
  $campoSelect;";

$resultadoForum = DaoEngine::getInstance()->executeQuery($sqlForum,true);

$linhas = array();



//obter pesos
$sqlFarol = "SELECT * FROM col_farol WHERE CD_EMPRESA = $codigoEmpresa";

//echo $sqlFarol;

$resultadoFarol = DaoEngine::getInstance()->executeQuery($sqlFarol,true);

$linhaFarol = mysql_fetch_array($resultadoFarol);

$exibirParticipacao = $linhaFarol["IN_PESO_PARTICIPACAO"];
$exibirAcesso = $linhaFarol["IN_PESO_ACESSO"];
$exibirDownload = $linhaFarol["IN_PESO_DOWNLOAD"];
$exibirParticipacaoAvaliacao = $linhaFarol["IN_PESO_PARTICIPACAO_AVALIACAO"];
$exibirParticipacaoSimulado = $linhaFarol["IN_PESO_PARTICIPACAO_SIMULADO"];
$exibirRealizacaoSimulado = $linhaFarol["IN_PESO_QUANTIDADE_SIMULADO"];
$exibirMediaAvaliacao = $linhaFarol["IN_PESO_NOTA_AVALIACAO"];
$exibirMediaSimulado = $linhaFarol["IN_PESO_NOTA_SIMULADO"];
$exibirParticipacaoForum = $linhaFarol["IN_PESO_PARTICIPACAO_FORUM"];
$exibirContribuicaoForum = $linhaFarol["IN_PESO_CONTRIBUICAO_FORUM"];

$pesoParticipacao = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO"], $exibirParticipacao);
$pesoAcesso = nullPara0Ranking($linhaFarol["NR_PESO_ACESSO"], $exibirAcesso);
$pesoDownload = nullPara0Ranking($linhaFarol["NR_PESO_DOWNLOAD"], $exibirDownload);
$pesoParticipacaoAvaliacao = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO_AVALIACAO"], $exibirParticipacaoAvaliacao);
$pesoParticipacaoSimulado = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO_SIMULADO"], $exibirParticipacaoSimulado);
$pesoRealizacaoSimulado = nullPara0Ranking($linhaFarol["NR_PESO_QUANTIDADE_SIMULADO"], $exibirRealizacaoSimulado);
$pesoMediaAvaliacao = nullPara0Ranking($linhaFarol["NR_PESO_NOTA_AVALIACAO"], $exibirMediaAvaliacao);
$pesoMediaSimulado = nullPara0Ranking($linhaFarol["NR_PESO_NOTA_SIMULADO"], $exibirMediaSimulado);
$pesoParticipacaoForum = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO_FORUM"], $exibirParticipacaoForum);
$pesoContribuicaoForum = nullPara0Ranking($linhaFarol["NR_PESO_CONTRIBUICAO_FORUM"], $exibirContribuicaoForum);

$limitarResultado = $linhaFarol["NR_LIMITE"];

$quantidadeRankeados = 0;

while ($linhaAcesso = mysql_fetch_array($resultadoAcesso)) {
	
	$linhaAvaliacao = mysql_fetch_array($resultadoAvaliacao);
	$linhaForum = mysql_fetch_array($resultadoForum);
	
	$linhaAcesso["FATOR_PARTICIPAO_AVALIACAO"] = $linhaAvaliacao["FATOR_PARTICIPAO_AVALIACAO"];
	$linhaAcesso["FATOR_PARTICIPAO_SIMULADO"] = $linhaAvaliacao["FATOR_PARTICIPAO_SIMULADO"];
	$linhaAcesso["FATOR_REALIZACAO_SIMULADO"] = $linhaAvaliacao["FATOR_REALIZACAO_SIMULADO"];
	$linhaAcesso["MEDIA_AVALIACAO"] = $linhaAvaliacao["MEDIA_AVALIACAO"];
	$linhaAcesso["MEDIA_SIMULADO"] = $linhaAvaliacao["MEDIA_SIMULADO"];
	
	$linhaAcesso["FATOR_PARTICIPACAO_FORUM"] = $linhaForum["FATOR_PARTICIPACAO_FORUM"];
	$linhaAcesso["FATOR_CONTRIBUICAO_FORUM"] = $linhaForum["FATOR_CONTRIBUICAO_FORUM"];
	
	$linhaAcesso = verificarRanking($linhaAcesso, "FATOR_PARTICIPACAO", 0);
	$linhaAcesso = verificarRanking($linhaAcesso, "FATOR_ACESSO", 0);
	$linhaAcesso = verificarRanking($linhaAcesso, "FATOR_DOWNLOAD", 0);
	$linhaAcesso = verificarRanking($linhaAcesso, "FATOR_PARTICIPAO_AVALIACAO", 0);
	$linhaAcesso = verificarRanking($linhaAcesso, "FATOR_PARTICIPAO_SIMULADO", 0);
	$linhaAcesso = verificarRanking($linhaAcesso, "FATOR_REALIZACAO_SIMULADO", null);
	$linhaAcesso = verificarRanking($linhaAcesso, "MEDIA_AVALIACAO", null);
	$linhaAcesso = verificarRanking($linhaAcesso, "MEDIA_SIMULADO", null);
	$linhaAcesso = verificarRanking($linhaAcesso, "FATOR_PARTICIPACAO_FORUM", 0);
	$linhaAcesso = verificarRanking($linhaAcesso, "FATOR_CONTRIBUICAO_FORUM", 0);
	
	
	
	
	$pontos = ($linhaAcesso["FATOR_PARTICIPACAO"] * $pesoParticipacao) +
			  ($linhaAcesso["FATOR_ACESSO"] * $pesoAcesso) +
			  ($linhaAcesso["FATOR_DOWNLOAD"] * $pesoDownload) +
			  ($linhaAcesso["FATOR_PARTICIPAO_AVALIACAO"] * $pesoParticipacaoAvaliacao) +
			  ($linhaAcesso["FATOR_PARTICIPAO_SIMULADO"] * $pesoParticipacaoSimulado) +
			  ($linhaAcesso["FATOR_REALIZACAO_SIMULADO"] * $pesoRealizacaoSimulado) +
			  ($linhaAcesso["MEDIA_AVALIACAO"] * $pesoMediaAvaliacao) +
			  ($linhaAcesso["MEDIA_SIMULADO"] * $pesoMediaSimulado) +
			  ($linhaAcesso["FATOR_PARTICIPACAO_FORUM"] * $pesoParticipacaoForum) +
			  ($linhaAcesso["FATOR_CONTRIBUICAO_FORUM"] * $pesoContribuicaoForum);
			  
	$linhaAcesso["TOTAL_PONTUACAO"] = $pontos;
	
	$linhaAcesso["PARTICIPOU"] = $linhaAcesso["FATOR_PARTICIPACAO"] > 0;
	
	if($linhaAcesso["PARTICIPOU"])
		$quantidadeRankeados++;
	
	$linhas[] = $linhaAcesso;
	
}

$quantidadeNaoRankeados = $totalUsuariosEmpresa - $quantidadeRankeados;

usort($linhas, "ordenarParticipacao");
$linhas = ranquearItens($linhas, "FATOR_PARTICIPACAO","RANK_PARTICIPACAO");
usort($linhas, "ordenarAcesso");
$linhas = ranquearItens($linhas, "FATOR_ACESSO","RANK_ACESSO");
usort($linhas, "ordenarDownload");
$linhas = ranquearItens($linhas, "FATOR_DOWNLOAD","RANK_DOWNLOAD");
usort($linhas, "ordenarParticipacaoAvaliacao");
$linhas = ranquearItens($linhas, "FATOR_PARTICIPAO_AVALIACAO","RANK_PARTICIPAO_AVALIACAO");
usort($linhas, "ordenarParticipacaoSimulado");
$linhas = ranquearItens($linhas, "FATOR_PARTICIPAO_SIMULADO","RANK_PARTICIPAO_SIMULADO");
usort($linhas, "ordenarRealizacaoSimulado");
$linhas = ranquearItens($linhas, "FATOR_REALIZACAO_SIMULADO","RANK_REALIZACAO_SIMULADO");
usort($linhas, "ordenarMediaAvaliacao");
$linhas = ranquearItens($linhas, "MEDIA_AVALIACAO","RANK_MEDIA_AVALIACAO");
usort($linhas, "ordenarMediaSimulado");
$linhas = ranquearItens($linhas, "MEDIA_SIMULADO","RANK_MEDIA_SIMULADO");
usort($linhas, "ordenarParticipacaoForum");
$linhas = ranquearItens($linhas, "FATOR_PARTICIPACAO_FORUM","RANK_PARTICIPACAO_FORUM");
usort($linhas, "ordenarContribuicaoForum");
$linhas = ranquearItens($linhas, "FATOR_CONTRIBUICAO_FORUM","RANK_CONTRIBUICAO_FORUM");
usort($linhas, "ordenarTotalGeral");
$linhas = ranquearItens($linhas, "TOTAL_PONTUACAO","RANK_TOTAL_PONTUACAO");

function verificarRanking($linha, $campo, $valorSemAcesso, $substituirValor = 0)
{
	$nomeCampoFlagAcesso = $campo . "_ACESSO";
	
	$linha[$nomeCampoFlagAcesso] = true;
	
	if ($linha[$campo] == $valorSemAcesso)
	{
		$linha[$campo] = $substituirValor;
		$linha[$nomeCampoFlagAcesso] = false;
	}
	
	return $linha;
}

function conveterDataParaYMD($data)
{
	$aData = split("/", $data);
	$data = "{$aData[2]}{$aData[1]}{$aData[0]}";
	
	return $data;
	
}

function ranquearItens($linhas, $campo, $campoRank)
{
	$posicaoAtual = 0;
	$ultimoValor = -1;
	
	//foreach ($linhas as $linha)
	for ($i=0; $i < count($linhas); $i++)
	{
		
		if ($linhas[$i][$campo] != $ultimoValor)
		{
			$posicaoAtual = $i + 1;
		}

		$linhas[$i][$campoRank] = $posicaoAtual;
		//echo $linhas[$i][$campoRank];
		
		$ultimoValor = $linhas[$i][$campo];
		
	}
	
	return $linhas;
	
}

function ordenarParticipacao($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPACAO");
}

function ordenarAcesso($a, $b)
{
	return ordenarDados($a, $b, "FATOR_ACESSO");
}

function ordenarDownload($a, $b)
{
	return ordenarDados($a, $b, "FATOR_DOWNLOAD");
}

function ordenarParticipacaoAvaliacao($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPAO_AVALIACAO");
}

function ordenarParticipacaoSimulado($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPAO_SIMULADO");
}

function ordenarRealizacaoSimulado($a, $b)
{
	return ordenarDados($a, $b, "FATOR_REALIZACAO_SIMULADO");
}

function ordenarMediaAvaliacao($a, $b)
{
	return ordenarDados($a, $b, "MEDIA_AVALIACAO");
}

function ordenarMediaSimulado($a, $b)
{
	return ordenarDados($a, $b, "MEDIA_SIMULADO");
}

function ordenarParticipacaoForum($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPACAO_FORUM");
}

function ordenarContribuicaoForum($a, $b)
{
	return ordenarDados($a, $b, "FATOR_CONTRIBUICAO_FORUM");
}

function ordenarTotalGeral($a, $b)
{
	return ordenarDados($a, $b, "TOTAL_PONTUACAO");
}

function ordenarDados($a, $b, $campo)
{
	if ($a[$campo] == $b[$campo])
		return 0;
		
	return ($a[$campo] > $b[$campo]) ? -1:+1;
}

function tratarDividirZero($valor)
{
	if ($valor == 0)
	{
		return 1;
	}
	else 
	{
		return $valor;
	}
}

?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		
		.tabelaRanking td
		{
			padding: 1px 3px 1px 3px;
		}
	</style>
	
</head>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk'>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<p><?php echo date("d/m/Y") ?></p>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr class="textblk">
		<td>
			Ciclo: <?php echo $nomeCiclo; ?>
		</td>
	</tr>
	<tr class="textblk">
		<td width="100%">
			Per�odo:&nbsp;
							<?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
		</td>
	</tr>
	<tr class="textblk">
		<td>
			Cadastrados da Empresa: <?php echo $totalUsuariosEmpresa; ?>
		</td>
	</tr>
	<?php
	if (!isset($_GET["cboEmpresa"]))
	{
	?>
	<tr class="textblk">
		<td>
			Cadastrados do Grupo: <?php echo $totalUsuarioGrupo; ?>
		</td>
	</tr>
	<?php
	}
	?>
	<tr class="textblk">
		<td>
			Quantidade de Participantes Raqueados: <?php echo $quantidadeRankeados; ?>
		</td>
	</tr>
	<tr class="textblk">
		<td>
			Quantidade de Participantes N�o Raqueados: <?php echo $quantidadeNaoRankeados; ?>
		</td>
	</tr>	
</table>

<table cellpadding="1" cellspacing="0" border="0" style="margin-left: 2.5%">
	<tr class="textblk">
		<td><b>Legenda</b></td>
		<td align="center"><b>Peso</b></td>
	</tr>
	<tr class="textblk">
		<td>RT - Ranking Total</td>
		<td align="center">-</td>
	</tr>

	
	<?php if($exibirMediaAvaliacao!=2)
	{
	?>
	<tr class="textblk">
		<td>RNA - Ranking de Nota em <?php echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo $pesoMediaAvaliacao; ?></td>
	</tr>
	
	<?php
	}
	if($exibirParticipacaoAvaliacao!=2)
	{
	?>
	<tr class="textblk" nowrap>
		<td>RPA - Ranking de Participa��o em <?php echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo $pesoParticipacaoAvaliacao; ?></td>
	</tr>
	<?php
	}
	if($exibirContribuicaoForum!=2)
	{
	?>
	<tr class="textblk">
		<td>RCF - Ranking de Contribui��o em F�rum</td>
		<td align="center"><?php echo $pesoContribuicaoForum; ?></td>
	</tr>	
	<?php
	}
	if($exibirParticipacaoForum!=2)
	{
	?>
	<tr class="textblk">
		<td>RPF - Ranking de Participa��o em F�runs</td>
		<td align="center"><?php echo $pesoParticipacaoForum; ?></td>
	</tr>
	<?php
	}
	if($exibirMediaSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td>RNS - Ranking de Nota em Simulado</td>
		<td align="center"><?php echo $pesoMediaSimulado; ?></td>
	</tr>	
	<?php
	}
	if($exibirRealizacaoSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td>RQS - Ranking de Quantidade de Simulado</td>
		<td align="center"><?php echo $pesoRealizacaoSimulado; ?></td>
	</tr>
	<?php
	}
	if($exibirParticipacaoSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td>RPS - Ranking de Participa��o em Simulado</td>
		<td align="center"><?php echo $pesoParticipacaoSimulado; ?></td>
	</tr>
	<?php
	}
	if($exibirDownload!=2)
	{
	?>
	<tr class="textblk">
		<td>RD - Ranking de Download</td>
		<td align="center"><?php echo $pesoDownload; ?></td>
	</tr>
	<?php
	}
	if($exibirAcesso!=2)
	{
	?>
	<tr class="textblk">
		<td>RA - Ranking de Acesso</td>
		<td align="center"><?php echo $pesoAcesso; ?></td>
	</tr>
	<?php
	}
	if($exibirParticipacao!=2)
	{
	?>	
		<tr class="textblk">
			<td>RP - Ranking de Participa�ao</td>
			<td align="center"><?php echo $pesoParticipacao; ?></td>
		</tr>
	<?php
	}
	?>
</table>

<br />

<table class="tabelaRanking" cellspacing="0" align="center" width="95%" border="0" style="border: 1px solid black;">
	<tr class='textblk' style="font-weight: bold;">
		<?php
			switch ($tipoRelatorio)
			{
				case "U":
					echo "<td class=\"bdLat\" >Participante</td>
						 <td class=\"bdLat\" >Login</td>
						 <td class=\"bdLat\" >Cargo/Fun��o</td>
						 <td class=\"bdLat\" >Lota�ao</td>";
					break;
				default:
					echo "<td class=\"bdLat\" width='100%'>$tituloCampo</td>";
					break;
			}
		
		?>
		
		<td class='bdLat' align="center" width="30" bgcolor="#cccccc">RT</td>
		
		<?php
		if($exibirMediaAvaliacao!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RNA</td>
		<?php
		}
		if($exibirParticipacaoAvaliacao!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RPA</td>
		<?php
		}
		if($exibirContribuicaoForum!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RCF</td>
		<?php
		}
		if($exibirParticipacaoForum!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RPF</td>
		<?php
		}
		if($exibirMediaSimulado!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RNS</td>
		<?php
		}
		if($exibirRealizacaoSimulado!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RQS</td>
		<?php
		}
		if($exibirParticipacaoSimulado!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RPS</td>
		<?php
		}
		if($exibirDownload!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RD</td>
		<?php
		}
		if($exibirAcesso!=2)
		{
		?>
		<td class='bdLat' width="30" align="center">RA</td>
		<?php
		}
		if($exibirParticipacao!=2)
		{
		?>
		<td width="30" align="center">RP</td>
		<?php
		}
		?>
	</tr>

	<?php
	
		foreach ($linhas as $linha)
		{
			if ($linha["FILTRADO"] == 0)
				continue;
			
			if(is_numeric($limitarResultado) && $limitarResultado > 0)
			{
				if ($linha["RANK_TOTAL_PONTUACAO"] > $limitarResultado)
				{
					break;
				}
			}
				
			echo "<tr class='textblk'>";
			
			switch ($tipoRelatorio)
			{
				case "U":
					echo "<td class=\"titRelatD\" >{$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}</td>
						 <td class=\"titRelat\" style='text-align: left;' >{$linha["login"]}</td>
						 <td class=\"titRelat\" style='text-align: left;'>{$linha["cargofuncao"]}</td>
						 <td class=\"titRelat\" style='text-align: left;'>{$linha["DS_LOTACAO"]}</td>";
					break;
				default:
					echo "<td class='titRelatD'>{$linha[$campoSelect]}</td>";
					break;
			}
			
			if (!$linha["PARTICIPOU"])
			{
				foreach ($linha as &$valor)
				{
					$valor = "NR";
				}
			}
			else 
			{
				$linha["RANK_ACESSO"] = aplicarNaoRanqueado($linha,"RANK_ACESSO", "FATOR_ACESSO");
				$linha["RANK_DOWNLOAD"] = aplicarNaoRanqueado($linha,"RANK_DOWNLOAD", "FATOR_DOWNLOAD");
				$linha["RANK_PARTICIPAO_SIMULADO"] = aplicarNaoRanqueado($linha,"RANK_PARTICIPAO_SIMULADO", "FATOR_PARTICIPAO_SIMULADO");
				$linha["RANK_REALIZACAO_SIMULADO"] = aplicarNaoRanqueado($linha,"RANK_REALIZACAO_SIMULADO", "FATOR_REALIZACAO_SIMULADO");
				$linha["RANK_MEDIA_SIMULADO"] = aplicarNaoRanqueado($linha,"RANK_MEDIA_SIMULADO", "MEDIA_SIMULADO");
				$linha["RANK_PARTICIPACAO_FORUM"] = aplicarNaoRanqueado($linha,"RANK_PARTICIPACAO_FORUM", "FATOR_PARTICIPACAO_FORUM");
				$linha["RANK_CONTRIBUICAO_FORUM"] = aplicarNaoRanqueado($linha,"RANK_CONTRIBUICAO_FORUM", "FATOR_CONTRIBUICAO_FORUM");
				$linha["RANK_PARTICIPAO_AVALIACAO"] = aplicarNaoRanqueado($linha,"RANK_PARTICIPAO_AVALIACAO", "FATOR_PARTICIPAO_AVALIACAO");
				$linha["RANK_MEDIA_AVALIACAO"] = aplicarNaoRanqueado($linha,"RANK_MEDIA_AVALIACAO", "MEDIA_AVALIACAO");
			}
					
			echo 	"<td class='titRelat' bgcolor=\"#cccccc\"><b>{$linha["RANK_TOTAL_PONTUACAO"]}</b></td>";
			
			if($exibirMediaAvaliacao!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_MEDIA_AVALIACAO"]}</td>";
			if($exibirParticipacaoAvaliacao!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_PARTICIPAO_AVALIACAO"]}</td>";
			if($exibirContribuicaoForum!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_CONTRIBUICAO_FORUM"]}</td>";
			if($exibirParticipacaoForum!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_PARTICIPACAO_FORUM"]}</td>";
			if($exibirMediaSimulado!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_MEDIA_SIMULADO"]}</td>";
			if($exibirRealizacaoSimulado!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_REALIZACAO_SIMULADO"]}</td>";
			if($exibirParticipacaoSimulado!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_PARTICIPAO_SIMULADO"]}</td>";
			if($exibirDownload!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_DOWNLOAD"]}</td>";
			if($exibirAcesso!=2)
				echo 	"<td class='titRelat'>{$linha["RANK_ACESSO"]}</td>";
			if($exibirParticipacao!=2)
				echo 	"<td class='titRelatTop'>{$linha["RANK_PARTICIPACAO"]}</td>";
				
			echo 	"</tr>";
			
			
			/*echo "<tr class='textblk'>
					<td class='titRelatD'>{$linha[$campoSelect]}</td>
					<td class='titRelat'>{$linha["FATOR_PARTICIPACAO"]}</td>
					<td class='titRelat'>{$linha["FATOR_ACESSO"]}</td>
					<td class='titRelat'>{$linha["FATOR_DOWNLOAD"]}</td>
					<td class='titRelat'>{$linha["FATOR_PARTICIPAO_SIMULADO"]}</td>
					<td class='titRelat'>{$linha["FATOR_REALIZACAO_SIMULADO"]}</td>
					<td class='titRelat'>{$linha["MEDIA_SIMULADO"]}</td>
					<td class='titRelat'>{$linha["FATOR_PARTICIPACAO_FORUM"]}</td>
					<td class='titRelat'>{$linha["RANK_CONTRIBUICAO_FORUM"]}</td>
					<td class='titRelat'>{$linha["FATOR_PARTICIPAO_AVALIACAO"]}</td>
					<td class='titRelat'>{$linha["MEDIA_AVALIACAO"]}</td>
					<td class='titRelatTop'>{$linha["RANK_TOTAL_PONTUACAO"]}</td>
				  </tr>
			";*/
			
			//
			
			
			
		}
	
		
		function aplicarNaoRanqueado($linha, $campoRanking, $campoFator)
		{
			$campoFator = $campoFator . "_ACESSO";
			
			if (!$linha[$campoFator])
				return "NR";
				
			return $linha[$campoRanking];
			
		}
		
	?>
	
</table>
<div style="width:100%;text-align:center">
	<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
	<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
</div>	
</body>
</html>