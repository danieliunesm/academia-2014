<?php
include "../../include/security.php";
include "../../include/genericfunctions.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Colabor� - Consultoria e Treinamento em Telecom</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META http-equiv="pragma" content="no-cache">
		<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">
		<script type="text/javascript" src="../../admin/include/js/functions.js"></script>
		<script language="JavaScript" src="../../include/js/ranking.js"></script>
		<script language="javascript">

            function ShowDialog(pagePath, args, width, height, left, top){
                return showModalDialog(pagePath, args, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;" + (left?("dialogLeft:" + left + "px;"):"") + (top?("dialogTop:" + top + "px;"):"") + "help:no;scroll:no;status:no");
            }
            
             var oVsession = "";
             var inativos = 0;
        
		     function viewControl(vSession){
                oVsession = vSession;
                if (document.getElementById("hdnRemovidos").value != ""){
                    inativos = document.getElementById("hdnRemovidos").value;
                }
                
                var html = ShowDialog("modoVisualizacao.htm", window, 420, 210);
                if(html!=null){
                    document.getElementById("hdnRemovidos").value = html;
                    document.forms[0].submit();
                }
            }
        
			var paginaEdicao = '<?php echo $paginaEdicao; ?>';
			
			<?php
				if (!isset($paginaEdicaoLargura)){
					$paginaEdicaoLargura = 450;
				}
				
				if (!isset($paginaEdicaoAltura)){
					$paginaEdicaoAltura = 280;
				}
			?>
		
			function noFocus(obj){if(obj.blur())obj.blur()}
		
			function openWindow(){
				
				if (document.getElementById('cboProva') != null){
					if (document.getElementById('cboProva').selectedIndex == 0){
						alert("Selecione uma avalia��o");
						document.getElementById('cboProva').focus();
						return;
					}
				}

				if (document.getElementById('cboForum') != null){
					if (document.getElementById('cboForum').selectedIndex == 0){
						alert("Selecione um f�rum");
						document.getElementById('cboForum').focus();
						return;
					}
				}
				
				if (document.getElementById('cboProva[]') != null)
				{
					if (document.getElementById('cboProva[]').selectedIndex == -1){
						alert("Selecione ao menos uma avalia��o");
						document.getElementById('cboProva[]').focus();
						return;
					}
				}
				
				if (document.getElementById('cboForum[]') != null)
				{
					if (document.getElementById('cboForum[]').selectedIndex == -1){
						alert("Selecione ao menos um f�rum");
						document.getElementById('cboForum[]').focus();
						return;
					}
				}
				
				newWin=null;
				var w=<?php echo $paginaEdicaoLargura; ?>;
				var h=<?php echo $paginaEdicaoAltura; ?>;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;


				if (document.getElementById("cboFiltro") == null)
				{
					url = obterParametros();
					newWin=window.open(url,'relatorio','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
				}
				else
				{
					newWin=window.open('','relatorio','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
					document.forms[0].target = "relatorio";
					document.forms[0].action = '<?php echo $paginaEdicao ?>';
					document.forms[0].method = "post";
					document.forms[0].submit();
					
					document.forms[0].target = '';
					document.forms[0].action = '';
					document.forms[0].method = "get";
					
					
				}
				//if(newWin!=null)setTimeout('newWin.focus()',100);
			}
			
			function getURL(){
				getURL(null);
			}
			
			function getURL(id){
				if(id != null){
					id = "?id=" + id;
				}else{
					id = "";
				}
				
				openWindow(paginaEdicao + id);
				
			}
		
			function obterParametros(){
				
				url = '<?php echo $paginaEdicao ?>'
				
				url = url + '?' + <?php echo $obterParametros ?>
				
				return url;
				
			}
		</script>		
	</HEAD>
	
	<BODY>
	<!-- Inicio do T�tulo -->
		<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
			<TR>
				<TD><IMG height="2" src="../../admin/images/blank.gif" width="100%"/></TD>
			</TR>
			<TR>
				<TD background="../../admin/images/bg_logo_admin.png">
					<TABLE cellpadding="0" cellspacing="0" width="663" background="images/logo_admin.png" border="0">
						<TR>
							<TD><IMG src="../../admin/images/blank.gif" height="32" width="1"/></TD>
							<TD class="data" align="right"><?php echo getServerDate(); ?></TD>
						</TR>
					
					</TABLE>
				
				</TD>
			</TR>
			<TR>
				<TD><IMG src="../../admin/images/blank.gif" width="100%" height="2" /></TD>
			</TR>
			<TR>
				<TD bgcolor="#cccccc"><IMG src="../../admin/images/blank.gif" height="3" width="100%"/></TD>
			</TR>
		</TABLE>
		<!-- Fim do T�tulo -->
		
		<!-- In�cio Cabe�alho -->
		<TABLE cellspacing="0" cellpadding="0" width="756" align="center" border="0">
			<TR>
				<TD width="1%"><IMG height="20" src="../../admin/images/blank.gif" width="289"/></TD>
				<TD></TD>
				<TD></TD>
			</TR>
			<TR valign="top">
				<TD class="textblk"><SPAN class="title">USU�RIO: </SPAN><?php echo $_SESSION["alias"]; ?></TD>
				<TD width="1%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.replace('/logout.php')"
									type="button" value="Logout" /></TD>
				<TD align="right" width="98%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href='../index.php'"
							type="button" value="Voltar"></TD>
			</TR>
			<TR>
				<TD><IMG src="../../admin/images/blank.gif" height="4" width="1" /></TD>
			</TR>
			<TR>
				<TD><IMG height="1" src="../../admin/images/blank.gif" width="280"></TD>
			</TR>
		</TABLE>
		<BR>
		<!-- Fim Cabe�alho -->
		
		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="GET" onsubmit="javascript:return false;">
			
            <input type="hidden" name="hdnRemovidos" value="<?php echo $_POST["hdnRemovidos"] ?>" />
            		
			<TABLE cellpadding="0" cellspacing="0" width="756" align="center" border="0">
				<TR class="tarjaTitulo">
						<TD align="middle" height="20"><?php echo TITULO_PAGINA; ?></TD>
				</TR>
				<TR>
					<TD>
						&nbsp;
					</TD>
				</TR>
				<TR>
					<TD><IMG src="../../admin/images/blank.gif" height="1" width="10"/></TD>
				</TR>
				<TR>
					<TD width="100%">
