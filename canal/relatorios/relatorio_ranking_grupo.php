<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1); 
//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include ('relatorio_ranking_util.php');

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

$tituloTotal = "";
$tituloRelatorio = "Radar";

$POST = obterPost();

//Obter Parametros
$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];

$codigoEmpresa = $POST['cboEmpresa'];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$hierarquia = isset($_REQUEST["hdnHierarquico"])? $_REQUEST["hdnHierarquico"]: 0; 
$codigoCiclo = $_REQUEST["hdnCiclo"];

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$dataAtual = $dataInicial;

// Fim obter parametros
$campoSelect = "DS_LOTACAO";
$tituloCampo = "Lota��o";
$tabelaExtra = "";
$whereExtra = "";
$tabelaCiclo = "";
$complementoNotaAvaliacaoSelect = "";
$complementoNotaSimuladoSelect = "";

$complementoTitulo = "";
if ($_GET["cboEmpresa"])
{
	$complementoTitulo = "da Empresa";
}
else 
{
	$complementoTitulo = "do Grupo: {$POST["nomeFiltro"]}";
}



switch ($tipoRelatorio)
{
	case "L":
		$campoSelect = "DS_LOTACAO";
		$tituloCampo = "Grupo Gerencial";
		$tituloRelatorio = "Desempenho de Grupos Gerenciais";
		break;
	case "U":
		$campoSelect = "login, NM_USUARIO, NM_SOBRENOME, DS_LOTACAO, cargofuncao";
		$tituloCampo = "Participante";
		$tituloRelatorio = "Desempenho de Participantes";
		
		if ($codigoCiclo == -1)
		{

			$sql = "SELECT COUNT(1) as QT_CICLO FROM col_ciclo WHERE CD_EMPRESA = $codigoEmpresa";
			$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
			$linhaCiclo = mysql_fetch_array($resultado);
			$quantidadeCiclo = $linhaCiclo["QT_CICLO"];
			
			if (!($quantidadeCiclo > 0))
				$quantidadeCiclo = 1;
		
			$tabelaCiclo = " LEFT JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND DATE_FORMAT(c.DT_INICIO, '%Y%m%d') <= DATE_FORMAT(pa.DT_INICIO, '%Y%m%d')
	            			AND DATE_FORMAT(c.DT_TERMINO, '%Y%m%d') >= DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') ";
			
			$complementoNotaAvaliacaoSelect = " * COUNT(DISTINCT IF(p.IN_PROVA = 1 AND pa.VL_MEDIA IS NOT NULL, c.CD_CICLO, NULL)) / $quantidadeCiclo ";
			$complementoNotaSimuladoSelect = " * COUNT(DISTINCT IF(p.IN_PROVA = 0 AND pa.VL_MEDIA IS NOT NULL, c.CD_CICLO, NULL)) / $quantidadeCiclo ";
		
		}
		
		
		break;
	case "G":
		$campoSelect = "NM_FILTRO";
		$tituloCampo = "Grupo";
		$tituloRelatorio = "Desempenho de Grupos Gerenciais";
		$tabelaExtra = "INNER JOIN col_filtro_lotacao filtro_lotacao ON filtro_lotacao.CD_LOTACAO = l.CD_LOTACAO
  						INNER JOIN col_filtro filtro ON filtro.CD_FILTRO = filtro_lotacao.CD_FILTRO";
		$whereExtra = "AND filtro.IN_FILTRO_RANKING = 1 AND filtro.IN_ATIVO = 1";
		break;
}

//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa

$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

$quantidadeCiclo = 1;
$nomeCiclo = "Todos os Ciclos";
if ($codigoCiclo != -1)
{
	//Obter o nome do Ciclo
	$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaCiclo = mysql_fetch_array($resultado);
	$nomeCiclo = $linhaCiclo["NM_CICLO"];
	//Fim obter o nome do Ciclo
}
else
{
	$sql = "SELECT COUNT(1) as TOTAL FROM col_ciclo WHERE CD_EMPRESA = $codigoEmpresa AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaCiclo = mysql_fetch_array($resultado);
	$quantidadeCiclo = $linhaCiclo["TOTAL"];
}

$totalUsuariosEmpresa = obterTotalUsuariosGrupamento($codigoEmpresa, -1, -1, -1, $dataFinalQuery);
$quantidadeCadastradosEmpresa = $totalUsuariosEmpresa;
$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataFinalQuery);

$sqlSuperusuario = ' AND (u.tipo > 0 AND u.tipo < 4) ';
//$sqlWhere = $sqlSuperusuario;


$whereExtra = "$whereExtra $sqlSuperusuario ";

$codigoFiltroUsuario = $_REQUEST["id"];

$quantidadeCiclosAvaliacoes = 1;
if ($quantidadeCiclo > 1)
	$quantidadeCiclosAvaliacoes = obterQuantidadeCiclosAvaliacoes();

if (!(ehEmpresa()))
{
	$fatoresGrupo = "";

	if (ehLotacao()) // Obtem os fatores da lota��o
	{
		$codigoLotacao = $POST["cboLotacao"][0];
		$fatoresGrupo = obterFatoresLotacao($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoLotacao, $quantidadeCiclosAvaliacoes);
	}
	elseif (ehFiltro() > 0)
	{
		$fatoresGrupo = obterFatoresGrupo($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoFiltroUsuario, $quantidadeCiclosAvaliacoes);
	}

	//$fatoresGrupo = obterFatoresGrupo($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoFiltroUsuario);
	
	$fatorGrupoMediaAvaliacaoOriginal = $fatoresGrupo["VALOR_MEDIA_AVALIACAO"] * 10;
	$fatorGrupoMediaAvaliacao = tratarDividirZero($fatoresGrupo["VALOR_MEDIA_AVALIACAO"] * 10);
	
	$fatorGrupoMediaAssessmentOriginal = $fatoresGrupo["VALOR_MEDIA_ASSESSMENT"] * 10;
	$fatorGrupoParticipacaoAssessmentOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_DIAGNOSTIVO"];
	
	$fatorGrupoParticipacaoAvaliacaoOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_AVALIACAO"];
	$fatorGrupoParticipacaoAvaliacao = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_AVALIACAO"]);
	
	$fatorGrupoAcessoOriginal = $fatoresGrupo["VALOR_ACESSO"];
	$fatorGrupoAcesso = tratarDividirZero($fatoresGrupo["VALOR_ACESSO"]);
	
	$fatorGrupoDownloadOriginal = $fatoresGrupo["VALOR_DOWNLOAD"];
	$fatorGrupoDownload = tratarDividirZero($fatoresGrupo["VALOR_DOWNLOAD"]);
	
	$fatorGrupoParticipacaoSimuladoOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_SIMULADO"];
	$fatorGrupoParticipacaoSimulado = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_SIMULADO"]);
	
	$fatorGrupoRealizacaoSimuladoOriginal = $fatoresGrupo["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
	$fatorGrupoRealizacaoSimulado = tratarDividirZero($fatoresGrupo["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);
	
	$fatorGrupoParticipacaoForumOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_FORUM"];
	$fatorGrupoParticipacaoForum = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_FORUM"]);
	
	$fatorGrupoContribuicaoForumOriginal = $fatoresGrupo["VALOR_CONTRIBUICAO_FORUM"];
	$fatorGrupoContribuicaoForum = tratarDividirZero($fatoresGrupo["VALOR_CONTRIBUICAO_FORUM"]);

	
	
	$fatorGrupoParticipacaoOriginal = 0;
	$fatorGrupoParticipacao = tratarDividirZero(0);
	
	$fatorGrupoMediaSimuladoOriginal = 0;
	$fatorGrupoMediaSimulado = tratarDividirZero(0);
	

	
	//Fim fatores do grupo

}

$fatoresEmpresa = obterFatoresEmpresaGrupoGerencial($codigoEmpresa, $dataInicialQuery, $dataFinalQuery);

$cadastradosEmpresa = $fatoresEmpresa["TOTAL_CADASTRADO"];
$participantesEmpresa = $fatoresEmpresa["TOTAL_PARTICIPANTE"];
$acessosEmpresa = $fatoresEmpresa["TOTAL_ACESSO"];
$simuladosEmpresa = $fatoresEmpresa["TOTAL_SIMULADO"];
$avaliacoesEmpresa = $fatoresEmpresa["TOTAL_AVALIACAO"];
$participantesAvaliacoesEmpresa = $fatoresEmpresa["TOTAL_PARTICIPANTE_AVALIACAO"];
$mediaEmpresa = $fatoresEmpresa["MEDIA_AVALIACAO"];
$percentualAprovacaoEmpresa = $fatoresEmpresa["PERCENTUAL_APROVACAO"];


if (exibeTabela())
{

	$linhas = array();
	
	//obter pesos
	$sqlFarol = "SELECT * FROM col_farol WHERE CD_EMPRESA = $codigoEmpresa";
	
	//echo $sqlFarol;
	
	$resultadoFarol = DaoEngine::getInstance()->executeQuery($sqlFarol,true);
	
	$linhaFarol = mysql_fetch_array($resultadoFarol);
	
	$exibirParticipacao = $linhaFarol["IN_PESO_PARTICIPACAO"];
	$exibirAcesso = $linhaFarol["IN_PESO_ACESSO"];
	$exibirDownload = $linhaFarol["IN_PESO_DOWNLOAD"];
	$exibirParticipacaoAvaliacao = $linhaFarol["IN_PESO_PARTICIPACAO_AVALIACAO"];
	$exibirParticipacaoSimulado = $linhaFarol["IN_PESO_PARTICIPACAO_SIMULADO"];
	$exibirRealizacaoSimulado = $linhaFarol["IN_PESO_QUANTIDADE_SIMULADO"];
	$exibirMediaAvaliacao = $linhaFarol["IN_PESO_NOTA_AVALIACAO"];
	$exibirMediaSimulado = $linhaFarol["IN_PESO_NOTA_SIMULADO"];
	$exibirParticipacaoForum = $linhaFarol["IN_PESO_PARTICIPACAO_FORUM"];
	$exibirContribuicaoForum = $linhaFarol["IN_PESO_CONTRIBUICAO_FORUM"];
	
	$pesoParticipacao = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO"], $exibirParticipacao);
	$pesoAcesso = nullPara0Ranking($linhaFarol["NR_PESO_ACESSO"], $exibirAcesso);
	$pesoDownload = nullPara0Ranking($linhaFarol["NR_PESO_DOWNLOAD"], $exibirDownload);
	$pesoParticipacaoAvaliacao = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO_AVALIACAO"], $exibirParticipacaoAvaliacao);
	$pesoParticipacaoSimulado = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO_SIMULADO"], $exibirParticipacaoSimulado);
	$pesoRealizacaoSimulado = nullPara0Ranking($linhaFarol["NR_PESO_QUANTIDADE_SIMULADO"], $exibirRealizacaoSimulado);
	$pesoMediaAvaliacao = nullPara0Ranking($linhaFarol["NR_PESO_NOTA_AVALIACAO"], $exibirMediaAvaliacao);
	$pesoMediaSimulado = nullPara0Ranking($linhaFarol["NR_PESO_NOTA_SIMULADO"], $exibirMediaSimulado);
	$pesoParticipacaoForum = nullPara0Ranking($linhaFarol["NR_PESO_PARTICIPACAO_FORUM"], $exibirParticipacaoForum);
	$pesoContribuicaoForum = nullPara0Ranking($linhaFarol["NR_PESO_CONTRIBUICAO_FORUM"], $exibirContribuicaoForum);
	
	$limitarResultado = $linhaFarol["NR_LIMITE"];
	
	$quantidadeRankeados = 0;
	$quantidadeRankeadosFiltro = 0;
	
	$quantidadeSuperusuarios = 0;
	$quantidadeParticipantesSuperusuario = 0;
	
	$quantidadeLotacoesCadastradas = 0;
	
	//$fatoresParticipantes = obterFatoresParticipantes($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoLotacao, $cargo, $codigoUsuario, $tipoRelatorio, false, false, $quantidadeCiclosAvaliacoes);
	
	$fatoresGrupoGerencial = obterFatoresGrupoGerencial($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoLotacao);
	
	$fatorGrupoEspec�fico = null;
	if ($codigoLotacao > 0) {
		$fatorGrupoEspecifico = obterFatoresGrupoGerencialEspecifico($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoLotacao);
	}
	
	$quantidadeLotacoesCadastradas = mysql_num_rows($fatoresGrupoGerencial);
	
}

function verificarRanking(&$linha, $campo, $valorSemAcesso, $substituirValor = 0)
{
	//$nomeCampoFlagAcesso = $campo . "_ACESSO";
	
	//$linha[$nomeCampoFlagAcesso] = true;
	
	if ($linha[$campo] == $valorSemAcesso)
	{
		$linha[$campo] = $substituirValor;
		//$linha[$nomeCampoFlagAcesso] = false;
	}
	
	//return $linha;
}

function ranquearItens(&$linhas, $campo, $campoRank)
{
	$posicaoAtual = 0;
	$ultimoValor = -1;
	
	//foreach ($linhas as $linha)
	for ($i=0; $i < count($linhas); $i++)
	{
		
		if ($linhas[$i][$campo] != $ultimoValor)
		{
			$posicaoAtual = $i + 1;
		}

		$linhas[$i][$campoRank] = $posicaoAtual;
		//$teste[] = $posicaoAtual;
		//echo $linhas[$i][$campoRank];
		
		$ultimoValor = $linhas[$i][$campo];
		
	}
	
	//return $linhas;
	
}

function ordenarParticipacao($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPACAO");
}

function ordenarAcesso($a, $b)
{
	return ordenarDados($a, $b, "FATOR_ACESSO");
}

function ordenarDownload($a, $b)
{
	return ordenarDados($a, $b, "FATOR_DOWNLOAD");
}

function ordenarParticipacaoAvaliacao($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPAO_AVALIACAO");
}

function ordenarParticipacaoSimulado($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPAO_SIMULADO");
}

function ordenarRealizacaoSimulado($a, $b)
{
	return ordenarDados($a, $b, "FATOR_REALIZACAO_SIMULADO");
}

function ordenarMediaAvaliacao($a, $b)
{
	return ordenarDados($a, $b, "MEDIA_AVALIACAO");
}

function ordenarMediaSimulado($a, $b)
{
	return ordenarDados($a, $b, "MEDIA_SIMULADO");
}

function ordenarParticipacaoForum($a, $b)
{
	return ordenarDados($a, $b, "FATOR_PARTICIPACAO_FORUM");
}

function ordenarContribuicaoForum($a, $b)
{
	return ordenarDados($a, $b, "FATOR_CONTRIBUICAO_FORUM");
}

function ordenarTotalGeral(&$a, &$b)
{
	return ordenarDados($a, $b, "TOTAL_PONTUACAO");
}

function ordenarDados($a, $b, $campo)
{
	if ($a[$campo] == $b[$campo])
		return 0;
		
	return ($a[$campo] > $b[$campo]) ? -1:+1;
}

function exibirGrupo()
{

	//global $tipoRelatorio;
	
	//return (($tipoRelatorio=="U" || $tipoRelatorio=="L") && (isset($_GET["id"]) && $_GET["id"] != ""));
	return false;
	return (!(ehEmpresa()));

}

function tratarDividirZero($valor)
{
	if ($valor == 0)
	{
		return 1;
	}
	else 
	{
		return $valor;
	}
}

if ($_POST["btnExcel"] != "") {

	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=RelatorioDesempenho.xls");

}

?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	<?php

	

	if ($_POST["btnExcel"] == "") {



	?>

	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">		

	<?php

	}

	?>
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		
		.tabelaRanking td
		{
			padding: 1px 3px 1px 3px;
		}
	</style>
	
</head>
<body>
<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk' nowrap>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">

	<?php
	if (!(ehEmpresa()))
	{
	?>

	<tr class="textblk">
		<td>
			<b>
			<?php
				if (ehFiltro()){ 
					echo "Nome do Grupo Gerencial: {$POST["nomeFiltro"]}";
				}elseif (ehLotacao()) {
					$labelLotacao = "Nome da Lota��o";
					if ($POST["nomeHierarquia"] != "") $labelLotacao = $POST["nomeHierarquia"];
					echo "$labelLotacao: {$POST["nomeLotacao"]}";
				}
			?>
			</b>
		</td>
	</tr>
	<?php
	}
	?>
	<tr class="textblk">
		<td>
			<b>Ciclo: <?php echo $nomeCiclo; ?></b>
		</td>
	</tr>
	<tr class="textblk">
		<td width="100%">
			<b>Per�odo:&nbsp;
							<?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
		</b></td>
	</tr>
	
	<?php if($codigoLotacao > -1) {
	 ?>
	<tr class="textblk">
		<td width="100%">Grupo de Gest�o: <?php echo $POST["nomeLotacao"];?>
		</td>
	</tr>
	<?php 
	}
	?>
	<?php
	if (ehEmpresa())
	{
	?>
	<tr class="textblk">
		<td>
			<?php 
				if ($tipoRelatorio == "L")
					echo "Grupos Gerenciais: $quantidadeLotacoesCadastradas";
				else 
					echo "Cadastrados da Empresa: $totalUsuariosEmpresa";	
				
			?>
			
		</td>
	</tr>
	<?php
		
	}
	else
	{
	?>
	<tr class="textblk">
		<td>
			<?php 
				if ($tipoRelatorio == "L")
				{	
					if (ehFiltro())
					{
						echo "Lota��es Cadastradas no Grupo: $quantidadeLotacoesCadastradas";	
					}
					
				}
				elseif (ehFiltro() || ehLotacao())
				{ 
					
					$labelCadastrados = "Cadastrados no Grupo";
					if (ehLotacao())
					{
						$labelCadastrados = "Cadastrados no Grupo de Gest�o";
						if ($POST["nomeHierarquia"] != "") $labelCadastrados = "Cadastrados no(a) {$POST["nomeHierarquia"]}";
					}
					
					echo "$labelCadastrados: $totalUsuarioGrupo";
				}	
				
			?>
		</td>
	</tr>
	<?php
	}
	if ($codigoEmpresa == 34)
	{
	?>
	<tr class="textblk">
		<td>
			Cadastrados como Participantes: <?php echo $totalUsuarioGrupo - $quantidadeSuperusuarios; ?>
		</td>
	</tr>
	<tr class="textblk">
		<td>
			Cadastrados como Superusu�rios (Gerentes): <?php echo $quantidadeSuperusuarios; ?>
		</td>
	</tr>		
	<?php 
	$quantidadeRankeados = $quantidadeRankeados - $quantidadeParticipantesSuperusuario;
	$quantidadeNaoRankeados = $totalUsuarioGrupo - $quantidadeSuperusuarios - $quantidadeRankeados;
	
	}
	if (exibeTabela())
	{
		$quantidadeRankeados = $quantidadeRankeados - $quantidadeParticipantesSuperusuario;
		$quantidadeNaoRankeados = $totalUsuarioGrupo - $quantidadeSuperusuarios - $quantidadeRankeados;
	?>
	<tr class="textblk">
		<td>
			<?php 
				if ($tipoRelatorio == "L")
				{
					if ($_REQUEST["id"] > -1)
					{
						echo "Quantidade de Lota��es Participantes: $quantidadeRankeados";
					}
				}
				else
					echo "Quantidade de Participantes: $quantidadeRankeados";
			?>
				<!--  : <?php //echo $quantidadeRankeados; ?>-->
		</td>
	</tr>
	<?php 
/*	<tr class="textblk">
		<td>
			<?php 
				echo "Quantidade de Superusu�rios Participantes: $quantidadeParticipantesSuperusuario";
			
			?>
		</td>
	</tr>
*/
?>
		<?php
			if($codigoEmpresa == 34)
			{
				$linhaElegiveis = obterQuantidadeCadastradosElegiveis($codigoEmpresa, $codigoLotacao, $codigoCiclo);
				$totalCadastradosElegiveis = $linhaElegiveis[0];
				
				$linhaElegiveis = obterQuantidadeParticipantesElegiveis($codigoEmpresa, $codigoLotacao, $codigoCiclo);
				$totalElegiveis = $linhaElegiveis[0];
		?>
	<tr>
		<td class="textblk">
			Eleg�veis a Certifica��o: <?php echo $totalCadastradosElegiveis;?>
		</td>
	</tr>
		
	<tr class="textblk">
		<td>
			Quantidade de Participantes Eleg�veis: <?php echo $totalElegiveis; ?>
		</td>
	</tr>	

	<tr class="textblk">
		<td>
		<?php 
			$quantidadeParticipantesNaoElegiveis = $quantidadeRankeados - $totalElegiveis;
			$quantidadeParticipantesNaoElegiveis = ($quantidadeParticipantesNaoElegiveis < 0? 0 : $quantidadeParticipantesNaoElegiveis);
		?>
			Quantidade de Participantes N�o Eleg�veis: <?php echo $quantidadeParticipantesNaoElegiveis; ?>
		</td>
	</tr>

	<tr class="textblk">
		<td>
			Quantidade de Participantes Eleg�veis sem Certifica��o: <?php echo $totalElegiveis - $linhaElegiveis[1]; ?>
		</td>
	</tr>
		
		<?php 	
			}
		 ?>
	
		<?php if (1==2) {?>
		<tr class="textblk">
			<td>
				<?php 
					if ($tipoRelatorio == "L")
					{
						$quantidadeLotacoesNaoRankeadas = $quantidadeLotacoesCadastradas - $quantidadeRankeados;
						if ($_REQUEST["id"] > -1)
						{
							echo "Quantidade de Lota��es N�o Participantes: $quantidadeLotacoesNaoRankeadas";
						}
					}
					else
					{
						echo "Quantidade de N�o Participantes: $quantidadeNaoRankeados";
					}	
				?>
				
			</td>
		</tr>
		<?php }?>
	<?php
	}
	?>	

</table>

<table cellspacing="0" border="0" style="margin-left: 2.5%; margin-right: 2.5%;" width="94%">
	<tr class="textblk">
		<td style="width:100px"><b>Indicadores</b></td>
		<td style="width:100px;" align="center"><b>Perfil Empresarial</b></td>
		<td rowspan="12" >&nbsp;&nbsp;</td>
		<?php
			if ($fatorGrupoEspecifico != null)
			{
				$labelPefilColuna = "Perfil Grupo";
				if (ehLotacao())
				{
					//$labelPefilColuna = "Perfil Lota��o";
					//if ($POST["nomeHierarquia"] != "") $labelPefilColuna = "Perfil {$POST["nomeHierarquia"]}";
				}
		?>
				
		<td style="width:100px" align="center"><b><?php echo $labelPefilColuna;?></b></td>
		<?php

			}
		?>
		<td rowspan="12" >&nbsp;</td>
		<td width="100%" rowspan="14"  valign="top" align="right">
			<table cellpadding="1" cellspacing="0" border="0" style="border: 1px solid #000000;" >
				<tr class="textblk"><td>RT - Ranking Total</td></tr>
				
				
				<?php
				if($codigoEmpresa == 34)
				{
				?>
					<tr class="textblk"><td>E - Eleg�vel</td></tr>
					<tr class="textblk"><td>NE - N�o Eleg�vel</td></tr>
					<tr class="textblk"><td>* - Teste de Certifica��o</td></tr>
				<?php 
				}
				
				
				?>
				
			</table>
		</td>
	</tr>
	
	<?php if($exibirMediaAvaliacao!=2)
	{
	?>
	<tr class="textblk">
		<td>Cadastrados <?php //echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo formatarValores($cadastradosEmpresa,2); ?></td>
		<?php
			if ($fatorGrupoEspecifico != null)
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoEspecifico["TOTAL_CADASTRADO"],2); ?></td>
		<?php
			}
		?>
	</tr>
	
	<?php
	}
	$exibirParticipacaoAvaliacao = 1;
	if($exibirParticipacaoAvaliacao!=2)
	{
	?>
	<tr class="textblk" >
		<td nowrap>Participantes<?php //echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo formatarValores($participantesEmpresa,2); ?></td>
		<?php
			if ($fatorGrupoEspecifico != null)
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoEspecifico["TOTAL_PARTICIPANTE"],2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	?>
	<tr class="textblk">
		<td>Acessos <?php //echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo formatarValores($acessosEmpresa,2); ?></td>
		<?php
			if ($fatorGrupoEspecifico != null)
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoEspecifico["TOTAL_ACESSO"],2); ?></td>
		<?php
			}
		?>
	</tr>
	<tr class="textblk" >
		<td nowrap>Simulados<?php //echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo formatarValores($simuladosEmpresa,2); ?></td>
		<?php
			if ($fatorGrupoEspecifico != null)
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoEspecifico["TOTAL_SIMULADO"],2); ?></td>
		<?php
			}
		?>
	</tr>
	
	<?php 
	$exibirAcesso = 1;
	if($exibirAcesso!=2)
	{
	?>
	<tr class="textblk">
		<td>Avalia��es</td>
		<td align="center"><?php echo formatarValores($avaliacoesEmpresa,2); ?></td>
		<?php
			if ($fatorGrupoEspecifico != null)
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoEspecifico["TOTAL_AVALIACAO"],2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	$exibirDownload = 1;
	if($exibirDownload!=2)
	{
	?>
	<tr class="textblk">
		<td nowrap>Participantes em Avalia��es</td>
		<td align="center"><?php echo formatarValores($participantesAvaliacoesEmpresa,2); ?></td>
		<?php
			if ($fatorGrupoEspecifico != null)
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoEspecifico["TOTAL_PARTICIPANTE_AVALIACAO"],2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	$exibirParticipacaoSimulado = 1;
	if($exibirParticipacaoSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td nowrap>M�dia</td>
		<td align="center"><?php echo formatarValores($mediaEmpresa * 10,2); ?></td>
		<?php
			if ($fatorGrupoEspecifico != null)
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoEspecifico["MEDIA_AVALIACAO"],2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	$exibirRealizacaoSimulado = 1;	
	if($exibirRealizacaoSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td nowrap>% de Aprova��o</td>
		<td align="center"><?php echo formatarValores($percentualAprovacaoEmpresa,2); ?></td>
		<?php
			if ($fatorGrupoEspecifico != null)
			{
		?>
		<td align="center"><?php echo formatarValores($fatorGrupoEspecifico["PERCENTUAL_APROVACAO"],2); ?></td>
		<?php
			}
		?>
	</tr>
	<?php
	}
	

	?>
		<tr class="textblk"><td>&nbsp;</td></tr>
		<tr class="textblk"><td>&nbsp;</td></tr>
	
</table>



<br />

<?php 

if (exibeTabela())
{
?>

<table class="tabelaRanking" cellspacing="0" align="center" width="95%" border="0" style="border: 1px solid black;">
	<tr class='textblk' style="font-weight: bold;">
		<td class="bdLat">&nbsp;</td>
		<?php
			$labelLotacao = "Lota��o";
			if ($POST["nomeHierarquia"] != "") $labelLotacao = "Respons�vel";
			switch ($tipoRelatorio)
			{
				case "U":
					echo "<td class=\"bdLat\" >Participante</td>
						 <td class=\"bdLat\" >Login</td>
						 <td class=\"bdLat\" >Cargo/Fun��o</td>
						 <td class=\"bdLat\" >$labelLotacao</td>";
					break;
				default:
					echo "<td class=\"bdLat\" width='100%' align='left'>$tituloCampo</td>";
					break;
			}
		
		?>
		
		<td class='bdLat' align="center" width="30" bgcolor="#cccccc">RT</td>
		
		<?php
		
		if(exibirGrupo())
		{
			echo "<td class='bdLat' align='center' width='30' bgcolor='#cccccc'>RG</td>";
		}
		?>
		
		<td class='bdLat' width="30" align="center">Cadastrados</td>
		<td class='bdLat' width="30" align="center">Participantes</td>
		<td class='bdLat' width="30" align="center">Acessos</td>
		<td class='bdLat' width="30" align="center">Simulados</td>
		<td class='bdLat' width="30" align="center">Avalia��es</td>
		<td class='bdLat' width="30" align="center">Participantes em&nbsp;Avalia��es</td>
		<td class='bdLat' width="30" align="center">M�dia</td>
		<td class='bdLat' align="center" nowrap>% de Aprova��o</td>
	</tr>

	<?php
	
		mysql_data_seek($fatoresGrupoGerencial, 0);
	
		$ranking_Grupo = 0;
		$rankingGrupoReal = 0;
		$ranking_geral_Anterior = 0;
		$contadorLinhas = 0;
		while ($linha = mysql_fetch_array($fatoresGrupoGerencial))
		{
			verificarRanking($linha, "TOTAL_CADASTRADO", null);
			verificarRanking($linha, "TOTAL_PARTICIPANTE", null);
			verificarRanking($linha, "TOTAL_ACESSO", null);
			verificarRanking($linha, "TOTAL_SIMULADO", null);
			verificarRanking($linha, "TOTAL_AVALIACAO", null);
			verificarRanking($linha, "TOTAL_PARTICIPANTE_AVALIACAO", null);
			verificarRanking($linha, "MEDIA_AVALIACAO", null, "-");
			verificarRanking($linha, "PERCENTUAL_APROVACAO", "", 0);
			
			echo "<tr class='textblk'>";
			
			$contadorLinhas++;
			echo "<td class=\"titRelatD\" >$contadorLinhas</td>";
			
			switch ($tipoRelatorio)
			{
				case "U":
					echo "<td class=\"titRelat\" >{$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}</td>
						 <td class=\"titRelat\" style='text-align: left;' >{$linha["login"]}</td>
						 <td class=\"titRelat\" style='text-align: left;'>{$linha["cargofuncao"]}</td>
						 <td class=\"titRelat\" style='text-align: left;'>{$linha["DS_LOTACAO"]}</td>";
					break;
				default:
					echo "<td class='titRelat' style='text-align: left;'>{$linha[$campoSelect]}</td>";
					break;
			}
			
			$rankingGrupoReal++;
			
			
			$linha["PERCENTUAL_APROVACAO"] = formatarValores($linha["PERCENTUAL_APROVACAO"], 2, 0);
			$linha["MEDIA_AVALIACAO"] = formatarValores($linha["MEDIA_AVALIACAO"] * 10, 2);
			
			echo 	"<td class='titRelat' bgcolor=\"#cccccc\"><b>{$rankingGrupoReal}</b></td>";

			echo 	"<td class='titRelat'>{$linha["TOTAL_CADASTRADO"]}</td>";
			echo 	"<td class='titRelat'>{$linha["TOTAL_PARTICIPANTE"]}</td>";
			echo 	"<td class='titRelat'>{$linha["TOTAL_ACESSO"]}</td>";
			echo 	"<td class='titRelat'>{$linha["TOTAL_SIMULADO"]}</td>";
			echo 	"<td class='titRelat'>{$linha["TOTAL_AVALIACAO"]}</td>";
			echo 	"<td class='titRelat'>{$linha["TOTAL_PARTICIPANTE_AVALIACAO"]}</td>";
			echo 	"<td class='titRelat'>{$linha["MEDIA_AVALIACAO"]}</td>";
			echo 	"<td class='titRelat'>{$linha["PERCENTUAL_APROVACAO"]}</td>";

			echo 	"</tr>";
			
			

			
			
			
		}

		
	?>
	
</table>
<?php 
}


function ehSuperusuario(&$linha)
{
	return ($linha["tipo"] != null && $linha["tipo"] < 0);
}

function formatarValores($valor, $decimal = 2, $seVazio = "-")
{
	if ($valor == "" || $valor == null || $valor == "-")
	{
		return $seVazio;
	}
	
	if (round($valor,0) == $valor)
	{
		return round($valor,0);
	}
	
	return number_format(round($valor,$decimal), $decimal, ",", "");		
	
}

function aplicarNaoRanqueado($linha, $campoRanking, $campoFator)
{
	$campoFator = $campoFator . "_ACESSO";
	
	if (!$linha[$campoFator])
		return "NR";
		
	return $linha[$campoRanking];
	
}

function exibeTabela()
{
	
	return true;
	
	global $tipoRelatorio, $codigoFiltroUsuario;
	
	$retorno = true;
	
	if ($tipoRelatorio == "G" && ehFiltro()) //Se for relat�rio de grupo e o codigo do grupo for passado ent�o n�o exibe a tabela
		$retorno = false;
	
	if ($tipoRelatorio == "L" && ehLotacao()) //Se for relat�rio de grupo e o codigo do grupo for passado ent�o n�o exibe a tabela
		$retorno = false;
		
	return $retorno;
	
}

function ehFiltro()
{
	return (isset($_REQUEST["id"]) && $_REQUEST["id"] > 0);
}

function ehLotacao()
{
	return (isset($_REQUEST["hdnLotacao"]) && $_REQUEST["hdnLotacao"] > 0);
}

function ehEmpresa()
{
	return true;
	return (!(ehLotacao() || ehFiltro()));
}

?>

<?php

		if ($_POST["btnExcel"] == "") {

	?>

	<div style="width:100%;text-align:center">

		<br />

		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']; ?>">

		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">

		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">

		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>

		</form>	

	</div>

	<?php

		}

	?>

</body>
</html>