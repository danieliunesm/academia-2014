<?php

//include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');

finalizarProvas();

$POST = obterPost();

$inTipoRelatorio = -1;
if (isset($_GET["tipo"]))
{
	$inTipoRelatorio = $_GET["tipo"];
}

$codigoEmpresa = $POST['cboEmpresa'];
$codigoUsuario = joinArray($POST['cboUsuario']);
$codigoProva = joinArray($POST['cboProva']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);

$dataInicio = $POST["txtDe"];
$dataInicioTexto = $dataInicio;
$dataTermino = $POST["txtAte"];
$dataTerminoTexto = $dataTermino;

$nomeEmpresa = 'Todas';
$nomeUsuario = 'Todos';
$nomeProva = '';
$lotacao = "";

$tipoRelatorio = "";

$sqlData = "";

if ($dataInicio != ""){
	$dataInicio = substr($dataInicio, 6, 4) . substr($dataInicio, 3, 2) . substr($dataInicio, 0, 2);
	
	$sqlData = " $sqlData AND  DATE_FORMAT(DT_INICIO, '%Y%m%d') >= '$dataInicio' ";
}

if ($dataTermino != ""){
	$dataTermino = substr($dataTermino, 6, 4) . substr($dataTermino, 3, 2) . substr($dataTermino, 0, 2);
		
	$sqlData = " $sqlData AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= '$dataTermino' ";
}

$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];

if (is_array($POST['cboProva']))
{
	if (count($POST['cboProva']) == 1)
	{
		$sql = "SELECT DS_PROVA, IN_PROVA FROM col_prova WHERE CD_PROVA = $codigoProva";
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		$linhaProva = mysql_fetch_array($resultado);
		$nomeProva = $linhaProva["DS_PROVA"];
		$tipoRelatorio = $linhaProva["IN_PROVA"];
	}
}

if($tipoRelatorio==1){
	$nomeTipoRelatorio = "Avalia��o de Aprendizado - ";	
}else if ($tipoRelatorio!="") {
	$nomeTipoRelatorio = "Avalia��o de Conhecimento - ";
}else {
	$nomeTipoRelatorio = "Certifica��es";
}

$sql = "
SELECT
  u.CD_USUARIO, u.NM_USUARIO, u.NM_SOBRENOME, u.empresa, e.DS_EMPRESA, p.IN_PROVA, u.login, p.DURACAO_PROVA,
  p.NR_QTD_PERGUNTAS_TOTAL, p.DS_PROVA, pa.VL_MEDIA, l.DS_LOTACAO, l.CD_LOTACAO,
  DATE_FORMAT(pa.DT_INICIO,'%d/%m/%Y') as DT_INICIO, DATE_FORMAT(pa.DT_INICIO,'%H:%i:%S') as HR_INICIO,
  TIMEDIFF(DT_TERMINO, DT_INICIO) AS TEMPO, pa.NR_TENTATIVA_USUARIO,
  (SELECT COUNT(1) FROM col_perguntas_aplicadas ipa INNER JOIN col_respostas ir
    ON ipa.CD_RESPOSTA = ir.CD_RESPOSTAS AND ipa.CD_PERGUNTAS = ir.CD_PERGUNTAS
    WHERE ir.IN_CERTA = 1 AND ipa.CD_USUARIO = u.CD_USUARIO AND ipa.CD_PROVA = p.CD_PROVA
 ) AS CORRETAS
 FROM
	col_empresa e
	inner join col_usuario u on e.CD_EMPRESA = u.empresa
	inner join col_prova_aplicada pa on pa.CD_USUARIO = u.CD_USUARIO
	inner join col_prova p on pa.CD_PROVA = p.CD_PROVA
	inner join col_lotacao l on l.CD_LOTACAO = u.lotacao
WHERE
	(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
AND (p.IN_PROVA = $inTipoRelatorio OR $inTipoRelatorio = -1)
AND (pa.VL_MEDIA is not null)

$sqlData
ORDER BY
	e.DS_EMPRESA, e.CD_EMPRESA, l.DS_LOTACAO, l.CD_LOTACAO, u.NM_USUARIO, u.CD_USUARIO, p.DS_PROVA, p.CD_PROVA, p.DT_REALIZACAO";

//AND	(p.IN_PROVA = $tipoRelatorio)

//echo $sql;
//exit();

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

//if (!mysql_num_rows($resultado) > 0){
//	echo '<script>alert("N�o h� dados para exibi��o do relat�rio.");self.close();</script>';
//	exit();
//}

$sqlParticipantes = "SELECT
						lotacao, COUNT(DISTINCT(u.CD_USUARIO)) AS TOTAL
					 FROM
					 	col_usuario u
					 WHERE
					 	u.empresa = $codigoEmpresa
					 GROUP BY
					 	u.lotacao";


$resultadosParticipantes = DaoEngine::getInstance()->executeQuery($sqlParticipantes,true);

$totalGrupamento = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);

renderRelatorio($resultado, $nomeEmpresa, $nomeUsuario, $nomeProva, $nomeTipoRelatorio, $dataInicioTexto, $dataTerminoTexto, $cargo, $lotacao, $resultadosParticipantes);

function converteHoraParaSegundo($tempo){
	
	$hora = $tempo{0} . $tempo{1};
	$minuto = $tempo{3} . $tempo{4};
	$segundo = $tempo{6} . $tempo{7};
	
	$segundos = $segundo + ($minuto * 60) + ($hora *60 *60);
	
	return $segundos;
	
}

function converteSegundoParaHora($segundo){
	
	$hora = floor($segundo / 3600);
	$minuto = $segundo % 3600;
	$minuto = floor($minuto / 60);
	$segundo = $segundo - ($hora * 3600) - ($minuto * 60);
	
	if ($hora < 10){
		$hora = "0$hora";
	}
	
	if ($minuto < 10){
		$minuto = "0$minuto";
	}
	
	if ($segundo < 10) {
		$segundo = "0$segundo";
	}
	
	return "$hora:$minuto:$segundo";
	
}

function renderRelatorio($resultado, $nomeEmpresa, $nomeUsuario, $nomeProva, $nomeTipoRelatorio, $dataInicio, $dataFim, $cargo, $lotacao, $resultadosParticipantes){

	global $POST;
	
	if (isset($_POST["btnExcel"]) && $_POST["btnExcel"] != "") {
		header("Content-Type: application/vnd.ms-excel; name='excel'");
		header("Content-disposition:  attachment; filename=RelatorioAvaliacao.xls");
	}
	
ob_start();

$tipoRelatorio = "";
if (isset($_GET["rdoTipoRelatorio"]))
	$tipoRelatorio = $_GET["rdoTipoRelatorio"];

?>

<html>
<head>
	<title>Colabor� Consultoria e Educa��o Corporativa</title>
	
	<?php
	
		if (isset($_POST["btnExcel"]) && $_POST["btnExcel"] != "") {
			formataExcel();
		}else {
			
			?>
			
				<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">
	
				<style type="text/css">
			
					@media print {
						.buttonsty {
						  display: none;
						}
						.textoInformativo{
							display: none;
						}
					}
					
					.textoInformativo{
						FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
					}
			
				</style>

			
			<?php
			
		}
	
	?>
	
	
</head>

<body>
	<table cellpadding="1" cellspacing="0" border="0" width="95%" align="center">
		<tr style="height: 30px">
			<td colspan="1" style="width: 100px" class='textblk' style="border-bottom: 1px solid #cccccc" nowrap>
				<!--<p>Colabor� Consultoria e Educa��o Corporativa</p>-->
				<p><b><?php echo $nomeEmpresa; ?></b></p>
			</td>
			<td colspan="<?php if($tipoRelatorio == 1){echo "5";}else{echo "6";} ?>" width="100%" align="center" class="textblk" style="border-bottom: 1px solid #cccccc">
				<b>Relat�rio de <?php echo "$nomeTipoRelatorio $nomeProva"; ?></b>
			</td>
			<td colspan="1" style="width: 100px; text-align: right" class='textblk' style="border-bottom: 1px solid #cccccc">
				<!--<p>&nbsp;</p>-->
				<p><?php echo date("d/m/Y") ?></p>
			</td>
		</tr>
		
	<?php
		
		$participantes = array();
		$totalParticipantes = 0;
	
		while ($linhaParticipante = mysql_fetch_array($resultadosParticipantes)) {
			
			$participantes[$linhaParticipante["lotacao"]] = $linhaParticipante["TOTAL"];
			$totalParticipantes = $totalParticipantes + $linhaParticipante["TOTAL"];
			
		}
	
	
		$relatorio = "";
		$empresaAnterior = 0;
		$usuarioAnterior = 0;
		$lotacaoAnterior = 0;
		$usuarioAnterior = 0;
		
		$qtdUsuario = 0;
		$usuarios = array();
		$mediaUsuario = 0;
		$mediasUsuarios = array();
		
		
		$qtdEmpresa = 0;
		$qtdLotacao = 0;
		$qtdAvaliacaoEmpresa = 0;
		$qtdAvaliacaoLotacao = 0;
		$empresas = array();
		$avaliacoesEmpresa = array();
		$lotacoes = array();
		$avaliacoesLotacao = array();
		$mediasEmpresas = array();
		$mediaEmpresa = 0;
		$mediasLotacoes = array();
		$mediaLotacao = 0;
		
		$tempoTotalLotacao = 0;
		$tempoTotal = 0;
		$temposLotacoes = array();
		
		$qtdPerguntasProva = 0;
		
		$duracaoProva = 0;
		
		while ($linha = mysql_fetch_array($resultado)) {
			
			$qtdPerguntasProva = $linha["NR_QTD_PERGUNTAS_TOTAL"];
			$duracaoProva = $linha["DURACAO_PROVA"];
			
			if ($empresaAnterior != $linha["empresa"] && $empresaAnterior > 0){
				$empresas[] = $qtdEmpresa;
				$avaliacoesEmpresa[] = $qtdAvaliacaoEmpresa;
				$mediasEmpresas[] = $mediaEmpresa;
				$qtdEmpresa = 0;
				$qtdAvaliacaoEmpresa = 0;
				$mediaEmpresa = 0;
			}
			
			if ($lotacaoAnterior != $linha["CD_LOTACAO"] && $lotacaoAnterior > 0){
				$lotacoes[] = $qtdLotacao;
				$avaliacoesLotacao[] = $qtdAvaliacaoLotacao;
				$mediasLotacoes[] = $mediaLotacao;
				$temposLotacoes[] = $tempoTotalLotacao;
				$qtdLotacao = 0;
				$qtdAvaliacaoLotacao = 0;
				$mediaLotacao = 0;
				$tempoTotalLotacao = 0;
			}
			
			
			
			$mediaEmpresa = $mediaEmpresa + $linha["VL_MEDIA"];
			$mediaLotacao = $mediaLotacao + $linha["VL_MEDIA"];
			$tempoTotalLotacao = $tempoTotalLotacao + converteHoraParaSegundo($linha["TEMPO"]);
			$tempoTotal = $tempoTotal + converteHoraParaSegundo($linha["TEMPO"]);
			
			if ($usuarioAnterior != $linha["CD_USUARIO"])
			{
				$qtdEmpresa++;
				$qtdLotacao++;
			}

			$qtdAvaliacaoLotacao++;
			$qtdAvaliacaoEmpresa++;
			$empresaAnterior = $linha["empresa"];
			$lotacaoAnterior = $linha["CD_LOTACAO"];
			$usuarioAnterior = $linha["CD_USUARIO"];
			
			if (!isset($POST['cboProva']) || !is_array($POST['cboProva']) || count($POST['cboProva']) != 1)
			{
				
				if (!isset($usuarios[$linha["CD_USUARIO"]]) || $usuarios[$linha["CD_USUARIO"]] == null)
				{
					$usuarios[$linha["CD_USUARIO"]] = array();
				}
				
				$usuarios[$linha["CD_USUARIO"]][] = $linha;
				
			}
			

		}
		
		$empresas[] = $qtdEmpresa;
		$avaliacoesEmpresa[] = $qtdAvaliacaoEmpresa;
		$lotacoes[] = $qtdLotacao;
		$avaliacoesLotacao[] = $qtdAvaliacaoLotacao;
		$mediasEmpresas[] = $mediaEmpresa;
		$mediasLotacoes[] = $mediaLotacao;
		$temposLotacoes[] = $tempoTotalLotacao;
		
		$qtdEmpresa = 0;
		$qtdLotacao = 0;
		
		if (mysql_num_rows($resultado) > 0)
		{
			mysql_data_seek($resultado, 0);
		}
		
	?>
		
		<tr style="height: 40px" >
			<td colspan="7" class="textblk" >
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr class="textblk">
						<td colspan="4" width="30%">Per�odo:&nbsp; <?php
												if($dataInicio!=""){
													echo "de $dataInicio ";
													if($dataFim!=""){
														echo "a $dataFim";
													}
												}else{
													echo "Todos ";
												}
						echo "</td><td align='right'>";
												echo "Total Perguntas: $qtdPerguntasProva ";
						echo "</td><td align='right'>";		
												
												if ($tempoTotal != 0)
													$tempoTotal = converteSegundoParaHora(round($tempoTotal / $avaliacoesEmpresa[0],0));
												else 
													$tempoTotal = "00:00:00";
													
												echo "Tempo M�dio: $tempoTotal";
												
												if ($mediasEmpresas[0] != 0)
													$mediaFinalEmpresa = round($mediasEmpresas[0] / $avaliacoesEmpresa[0], 2);
												else 
													$mediaFinalEmpresa = 0;
													
												$mediaFinalEmpresa = number_format($mediaFinalEmpresa, 2, ",", ".");
						echo "</td><td align='right'>";		
												echo "M�dia Geral: $mediaFinalEmpresa";
											?>
						</td>
					</tr>
					<tr>
					<?php
						if ((!isset($_POST["btnExcel"])) || $_POST["btnExcel"] == "")
						{
							?>
								<td colspan="7" class="textblk">Tempo-limite realiza��o da Avalia��o: <?php echo converteSegundoParaHora($duracaoProva * 60); ?></td>
							<?
						}
						else 
						{
							?>
					
						<td colspan="2" class="textblk">Tempo-limite realiza��o da Avalia��o:</td>
						<td colspan="5" class="textblk"><?php echo converteSegundoParaHora($duracaoProva * 60); ?></td>
							<?php
						}
							?>
					</tr>
					<tr class="textblk">
						<?php
							if ((!isset($_POST["btnExcel"])) || $_POST["btnExcel"] == "")
							{
						?>
					
						<td colspan="7">Total de cadastrados: <?php echo $totalParticipantes; ?></td>
						
						<?php
							}
							else 
							{
								echo "<td>Total de cadastrados: </td>";
								echo "<td colspan=\"2\">$totalParticipantes</td>";
							}
						?>
						
					</tr>
					<tr class="textblk">
						<?php
							global $totalGrupamento;
						
							if ((!isset($_POST["btnExcel"])) || $_POST["btnExcel"] == "")
							{
						?>
					
						<td colspan="7">Total do grupamento: <?php echo $totalGrupamento; ?></td>
						
						<?php
							}
							else 
							{
								echo "<td>Total do grupamento: </td>";
								echo "<td colspan=\"2\">$totalGrupamento</td>";
							}
						?>
						
					</tr>
					<tr class="textblk">
						<?php
							
						
							if ((!isset($_POST["btnExcel"])) || $_POST["btnExcel"] == "")
							{
						?>
					
						<td colspan="7">Total de Participantes: <?php echo $empresas[0]; ?></td>
						
						<?php
							}
							else 
							{
								echo "<td>Total de Participantes: </td>";
								echo "<td colspan=\"2\">{$empresas[0]}</td>";
							}
						?>
						
					</tr>
					<tr class="textblk">
						<?php
							$participacao = $empresas[0] / $totalGrupamento * 100;
							$participacao = number_format($participacao,2,",",".");
						
							if ((!isset($_POST["btnExcel"])) || $_POST["btnExcel"] == "")
							{
						?>
					
						<td colspan="7">Participa��o do grupamento: <?php echo "$participacao%"; ?></td>
						
						<?php
							}
							else 
							{
								echo "<td>Participa��o do grupamento: </td>";
								echo "<td colspan=\"2\">$participacao%</td>";
							}
						?>
						
					</tr>
					
				</table>

			</td>
		</tr>
	</table>
	<br />
	<table cellpadding="0" cellspacing="0" border="0" width="95%" align="center">
	
		
	<?php
		
		$lotacaoAnterior = 0;
		$usuarioAnterior = 0;
		$colspan = "";
		if($tipoRelatorio == 1)
		{
			$colspan = "5";
		}
		else
		{
			$colspan = "6";
		}
	
		while ($linha = mysql_fetch_array($resultado)) {
			
			if ($lotacaoAnterior != $linha["CD_LOTACAO"]){
				$quantidade = $lotacoes[$qtdLotacao];
				$media = round($mediasLotacoes[$qtdLotacao] / $avaliacoesLotacao[$qtdLotacao], 2);
				$tempoTotalLotacao = converteSegundoParaHora(round($temposLotacoes[$qtdLotacao] / $avaliacoesLotacao[$qtdLotacao],0));
				$qtdLotacao++;
				
				if ($lotacaoAnterior != 0){
								
						echo   "</table>
							</td>
						</tr>
						<tr>
							<td>&nbsp;
							</td>
						</tr>";
				}
				
				$media = number_format($media,2,",",".");
				
				$percentualParticipacao = $quantidade/$participantes[$linha["CD_LOTACAO"]] * 100;
				
				$percentualParticipacao = number_format($percentualParticipacao,2,",",".");
				
				echo "<tr><td colspan='4' class='textblk' style='border-bottom: 1px solid #cccccc'><b>Lota��o: {$linha["DS_LOTACAO"]}</b></td></tr>";
				echo "<tr height='30'><td class='textblk' width='30%'>T. Cadastrados por lota��o: {$participantes[$linha["CD_LOTACAO"]]}</td>
							<td class='textblk' align='right'>Q. Participantes: $quantidade</td>
							<td class='textblk' align='right'>Tempo M�dio Lota��o: $tempoTotalLotacao</td>
							<td class='textblk' align='right'>M�dia Lota��o: $media</td>
						</tr>
						<tr>
							<td colspan='4' class='textblk'>Q. Participantes/Total Cadastrados: $percentualParticipacao% </td>
						</tr>
						<tr>
							<td colspan='4' class='textblk'>
								<table cellpadding='0' cellspacing='0' border='0' width='100%'>
									<tr class='textblk' height='20' valign='top'>
										<td width='40%'>Login / Nome Participante</span>";
				
				global $inTipoRelatorio;
				
				if ($inTipoRelatorio!=1){
					echo "					<td align='center' width='8%' class='centralizado'>Q.&nbsp;Certifica��es</td>";
				}
					
				echo "					<td align='center' width='10%' class='centralizado'>Data</td>
										<td align='center' width='10%' class='centralizado'>Hora</td>
										<td align='center' width='10%' class='centralizado'>Tempo</td>
										<td align='center' width='10%' class='centralizado'>Certas</td>
										<td align='center' width='10%' class='centralizado'>Erradas</td>
										<td align='center' width='10%' class='centralizado'>Nota</td>
									</tr>";

				
				$lotacaoAnterior = $linha["CD_LOTACAO"];
			}
			
			
			//if ($usuarioAnterior != $linha["CD_USUARIO"]){
			//	
			//	echo "<tr><td colspan='4'  class='textblk'>Usu�rio: {$linha["NM_USUARIO"]}</td></tr>";		
			//	
			//	$usuarioAnterior = $linha["CD_USUARIO"];
			//}
	
			//$tipoExame = "Av. de Conhecimento";
			//if ($linha["IN_PROVA"] == 1){
			//	$tipoExame = "Av. de Aprendizado";
			//}
			
			$tipoExame = "";
			
			$corretas = $linha["CORRETAS"];
			
			if ($corretas > $linha["NR_QTD_PERGUNTAS_TOTAL"])
				$corretas = $linha["NR_QTD_PERGUNTAS_TOTAL"];
			
			$erradas = $linha["NR_QTD_PERGUNTAS_TOTAL"] - $corretas;

			$corretas = $linha["VL_MEDIA"] * $linha["NR_QTD_PERGUNTAS_TOTAL"] / 10;
			$erradas = $linha["NR_QTD_PERGUNTAS_TOTAL"] - $corretas;
			
			$vlMedia = number_format($linha["VL_MEDIA"], 2, ",", ".");
			

			
			
			if (count($usuarios) > 0)
			{
				if ($usuarioAnterior != $linha["CD_USUARIO"])
				{
					echo "					<tr class='textblk'>
												<td colspan='$colspan'>&nbsp;</td>";
					
					echo "					<tr class='textblk'>
						<td class='texto' colspan='$colspan'>{$linha["login"]} / {$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}</td>
						<tr class='textblk'>
							<td class='texto'><span style='margin-left: 30px;'>{$linha["DS_PROVA"]}</span></td>";

					$usuarioAnterior = $linha["CD_USUARIO"];
				}
				else
				{
					echo "<tr class='textblk'>
							<td class='texto'><span style='margin-left: 30px;'>{$linha["DS_PROVA"]}</span></td>";
				}
			}
			else
			{
				echo "					<tr class='textblk'>
											<td class='texto'>{$linha["login"]} / {$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}</td>";

			}

			

			if ($inTipoRelatorio!=1){
				echo "					<td align='center' class='centralizado'>{$linha["NR_TENTATIVA_USUARIO"]}</td>";
			}
										

			echo "						<td align='center' class='dataExcel'>{$linha["DT_INICIO"]}</td>
										<td align='center' class='hora'>{$linha["HR_INICIO"]}</td>
										<td align='center' class='hora'>{$linha["TEMPO"]}</td>
										<td align='center' class='centralizado'>$corretas</td>
										<td align='center' class='centralizado'>$erradas</td>
										<td align='center' class='fixado'>$vlMedia</td>
									</tr>";
			
			

		}
		
		echo   "</table>
			</td>
		</tr>";
	
	?>
		
		<!--<tr>
			<td colspan="2">Quest�es: <br />
				<blockquote>
					Quest�o 1 Quest�o 1 Quest�o 1 Quest�o 1 <br />
						<blockquote style="margin-top: -2px">
							<span style="width: 80%;">Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1Resposta 1 Resposta 1 Resposta 1 </span><br />
							<span style="width: 80%;">Resposta 2 Resposta 2 Resposta 2</span><br /> 
						</blockquote>
				</blockquote>
			
			</td>
		</tr>-->
		
	</table>
	
	<?php
		if ((!isset($_POST["btnExcel"])) || $_POST["btnExcel"] == "") {
	?>
	<div style="width:100%;text-align:center">
		<br />
		<form method="POST">
			<input type="hidden" value="<?php echo $POST["cboEmpresa"]?>" id="cboEmpresa" name="cboEmpresa" />
			
			<?php
				
				if (isset($POST["cboProva"]) && is_array($POST["cboProva"]))
				{
					$itens = $POST["cboProva"];
					foreach ($itens as $item)
					{
						echo "<input type=\"hidden\" value=\"$item\" id=\"cboProva[]\" name=\"cboProva[]\" />";
					}

				}
				
				if (isset($POST['cboUsuario']) && is_array($POST['cboUsuario']))
				{
					$itens = $POST['cboUsuario'];
					foreach ($itens as $item)
					{
						echo "<input type=\"hidden\" value=\"$item\" id=\"cboUsuario[]\" name=\"cboUsuario[]\" />";
					}
				}
				
				if (isset($POST['cboCargo']) && is_array($POST['cboCargo']))
				{			
					$itens = $POST['cboCargo'];
					foreach ($itens as $item)
					{
						echo "<input type=\"hidden\" value=\"$item\" id=\"cboCargo[]\" name=\"cboCargo[]\" />";
					}
				}
				
				if (isset($POST['cboLotacao']) && is_array($POST['cboLotacao']))
				{
					$itens = $POST['cboLotacao'];
					foreach ($itens as $item)
					{
						echo "<input type=\"hidden\" value=\"$item\" id=\"cboLotacao[]\" name=\"cboLotacao[]\" />";
					}
				}
				
				if (isset($POST["txtDe"]))
				{
					echo "<input type=\"hidden\" value=\"{$POST["txtDe"]}\" id=\"txtDe\" name=\"txtDe\" />";
				}

				if (isset($POST["txtAte"]))
				{
					echo "<input type=\"hidden\" value=\"{$POST["txtAte"]}\" id=\"txtAte\" name=\"txtAte\" />";
				}
			
			?>
			
			<!--<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">-->
			<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
			<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
		</form>
	</div>
	<?php
		}
	?>
	
</body>
</html>

<?php

ob_end_flush();

}

function formataExcel(){
	
?>
	
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Petrobras</o:Author>
  <o:LastAuthor>Petrobras</o:LastAuthor>
  <o:Created>2008-02-25T20:58:45Z</o:Created>
  <o:LastSaved>2008-02-25T21:00:08Z</o:LastSaved>
  <o:Company>Petrobras</o:Company>
  <o:Version>11.8132</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<style>

<!--table
	{mso-displayed-decimal-separator:"\,";
	mso-displayed-thousand-separator:"\.";}
@page
	{mso-header-data:"&LColabor� Consultoria e Educa��o Corporativa&RP�gina &P de &N";
	margin:.98in .24in .98in .24in;
	mso-header-margin:.49in;
	mso-footer-margin:.49in;
	mso-page-orientation:landscape;}
tr
	{mso-height-source:auto;}
col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
.textblk
	{mso-style-parent:style0;
	mso-number-format:"\@";}
	
.texto
	{mso-style-parent:style0;
	white-space:normal;}
	
.dataExcel
	{mso-style-parent:style0;
	mso-number-format:"dd\/mm\/yyyy";
	text-align:center;}
	
.hora
	{mso-style-parent:style0;
	mso-number-format:"hh\:mm\:ss";
	text-align:center;}
	
.centralizado
	{mso-style-parent:style0;
	text-align:center;}
	
.fixado
	{mso-style-parent:style0;
	mso-number-format:Fixed;
	text-align:center;}

td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>Relat�rio de Certifica��es</x:Name>
    <x:WorksheetOptions>
     <x:Selected/>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>11895</x:WindowHeight>
  <x:WindowWidth>15180</x:WindowWidth>
  <x:WindowTopX>360</x:WindowTopX>
  <x:WindowTopY>15</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
	
<?php
	
}


function finalizarProvas(){
	
	$sql = "SELECT pa.CD_PROVA, pa.CD_USUARIO,  pa.NR_TENTATIVA_USUARIO, p.NR_QTD_PERGUNTAS_TOTAL,
			DT_INICIO, DATE_ADD( DT_INICIO, INTERVAL DURACAO_PROVA
			MINUTE ) AS DATAFIM
			FROM col_prova_aplicada pa
			INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
			WHERE DT_TERMINO IS NULL 
			AND DT_INICIO IS NOT NULL 
			AND DATE_ADD( DT_INICIO, INTERVAL DURACAO_PROVA
			MINUTE ) < sysdate( ) ";
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	
	while ($linha = mysql_fetch_array($resultado)) {

		$usuario = $linha["CD_USUARIO"];
		$prova = $linha["CD_PROVA"];
		$DataTerminoIdeal = $linha["DATAFIM"];
		$TentativasUsuario = $linha["NR_TENTATIVA_USUARIO"];
		$total = $linha["NR_QTD_PERGUNTAS_TOTAL"];
		
		$sql = "SELECT COUNT(1) AS CERTAS FROM col_perguntas_aplicadas P INNER JOIN col_respostas R ON P.CD_RESPOSTA = R.CD_RESPOSTAS AND P.CD_PERGUNTAS = R.CD_PERGUNTAS WHERE P.CD_USUARIO = $usuario AND P.CD_PROVA = $prova AND R.IN_CERTA = 1";
		$resultadoCorretas = DaoEngine::getInstance()->executeQuery($sql,true);
		$linhaCorretas = mysql_fetch_array($resultadoCorretas);
		$corretas = $linhaCorretas["CERTAS"];
		
		$media = $corretas / $total * 1000;
		
		$media = round($media) / 100;
		
		$sql = "UPDATE col_prova_aplicada SET DT_TERMINO = '$DataTerminoIdeal', VL_MEDIA = $media, NR_TENTATIVA_USUARIO = NR_TENTATIVA_USUARIO + 1 WHERE CD_USUARIO = $usuario AND CD_PROVA = $prova";
		DaoEngine::getInstance()->executeQuery($sql,true);
		
		$sql = "UPDATE col_perguntas_aplicadas SET IN_STATUS_PERGUNTA = 0 WHERE CD_PROVA = $prova AND CD_USUARIO = $usuario";
		DaoEngine::getInstance()->executeQuery($sql,true);
		
	}
	
	
}

?>