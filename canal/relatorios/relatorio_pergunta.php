<?php
//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include ('relatorio_ranking_util.php');
include('relatorio_acesso_util.php');

$notaCentesimal = false;

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

if (isset($_POST["btnExcel"]) && $_POST["btnExcel"] != "") {

	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=RadarAssessment.xls");

}

error_reporting(E_ALL);
ini_set('display_errors', '1');
$tituloTotal = "";
$tituloRelatorio = "Ranking";

$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];
$codigoFiltro = 0;
$POST = obterPost($codigoFiltro);
$classificacaoRelatorio = $_REQUEST["tipo"];

if ($POST["nomeHierarquia"] == "") {
	$POST["nomeHierarquia"] = "Grupo de Gest�o";
}


$nomeLotacao = "{$POST["nomeHierarquia"]}: {$POST["nomeLotacao"]}";
if ($POST["nomeLotacao"] == "")
{
	$nomeLotacao = "&nbsp;";
}

$codigoLotacaoUsuario = $_REQUEST["hdnLotacao"];

$codigoEmpresa = $_SESSION["empresaID"];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$cargo = $POST['cboCargo'];
$codigoUsuario = (isset($_REQUEST["cboUsuario"])?$_REQUEST["cboUsuario"]:-1);
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$codigoCiclo = $_REQUEST["hdnCiclo"];
$nomeUsuario = null;
$localidade = $_REQUEST['cboLocalidade'];

if (isset($_GET["cboUsuario"]) || ($_SESSION["tipo"] >=4 || $_SESSION["tipo"] == -1 || $_SESSION["tipo"] == -2 || $_SESSION["tipo"] == -3))
{
	
}
else
{
	$codigoUsuario = $_SESSION["cd_usu"];
}

if ($codigoUsuario > -1) {
	$sql = "SELECT u.lotacao, u.NM_USUARIO, l.DS_LOTACAO FROM col_usuario u LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao WHERE CD_USUARIO = $codigoUsuario";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaLotacao = mysql_fetch_array($resultado);
	
	if ($classificacaoRelatorio == "C") {
		$codigoLotacao = $linhaLotacao["lotacao"];
	}
	
	$nomeUsuario = $linhaLotacao["NM_USUARIO"];
	$nomeLotacao = "{$POST["nomeHierarquia"]}: {$linhaLotacao["DS_LOTACAO"]}";
}

if (isset($_REQUEST["operacao"]) && $_REQUEST["operacao"] ==765)
{
	
	$codigoEmpresa = 5;
	$dataInicial = "";
	$dataFinal = "";
	$cargo = joinArray($POST['cboCargo'],"','");
	$codigoUsuario = joinArray($POST['cboUsuario']);
	$codigoLotacao = joinArray($POST['cboLotacao']);
	$dataInicioTexto = $dataInicial;
	$dataTerminoTexto = $dataFinal;
	$codigoCiclo = -1;
	$codigoFiltroUsuario = 1;
}

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$dataAtual = $dataInicial;

// Fim obter parametros
$campoSelect = "login, NM_USUARIO, NM_SOBRENOME, DS_LOTACAO, cargofuncao";
$tituloCampo = "Participante";
$tituloRelatorio = "Radar de Aprendizado";

if ($classificacaoRelatorio == "C") {
	$tituloRelatorio = "Relat�rio de Perguntas";
}

//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa
mysql_free_result($resultado);

$nomeCiclo = "Todos os Ciclos";
if ($codigoCiclo != -1)
{
	//Obter o nome do Ciclo
	$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaCiclo = mysql_fetch_array($resultado);
	$nomeCiclo = $linhaCiclo["NM_CICLO"];
	//Fim obter o nome do Ciclo
}


$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

function tratarDividirZero($valor)
{
	if ($valor == 0)
	{
		return 1;
	}
	else 
	{
		return $valor;
	}
}

function formatarValores($valor, $decimal = 2, $seVazio = "-")
{
	if ($valor == "")
	{
		return $seVazio;
	}
	
	global $notaCentesimal;
	
	
	return number_format(round($valor,$decimal), $decimal, ",", "");
	
}

function letraResposta($numero){
	
	switch ($numero){
		case 1:
			return "a";
		case 2:
			return "b";
		case 3:
			return "c";
		case 4:
			return "d";
	}
	
}

?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	
	<?php

	
	$estiloColunaCompetenciaExcel = "";
	if (!isset($_POST["btnExcel"]) || $_POST["btnExcel"] == "") {



	?>

	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">		

	<?php

	}else{
		$estiloColunaCompetenciaExcel = ".colunaCompetencia{
			width: 550px;
		}";
	}

	?>
	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		
		.tabelaRanking td
		{
			padding: 1px 3px 1px 3px;
		}
		body{overflow:auto}
		
		<?php echo $estiloColunaCompetenciaExcel;?>
		
}
	</style>
	
</head>
<body>
<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk' colspan="2" nowrap>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td colspan="2" width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<!-- <tr class="textblk">
		<td>
			<b>Ciclo: <?php echo $nomeCiclo; ?></b>
		</td>
	</tr>
	<tr class="textblk">
		<td width="100%">
			<b>Per�odo:&nbsp;
							<?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
			</b>
		</td>
	</tr>-->
	<tr class="textblk">
	<td class="textblk" colspan="2">
		<b><?php echo $nomeLotacao; ?></b>
	</td>
	</tr>
	
	<?php 
	if ($codigoUsuario > -1) {
	?>
	<tr class="textblk">
		<td class="textblk">
			<b>Nome: <?php echo $nomeUsuario; ?></b>
		</td>
	</tr>
	<?php 
	}
	?>
	
	
</table>

<br />
<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr>
		<td class="textblk" colspan="5">
			<?php obterTextoComentario(basename("/relatorio_pergunta",".php"));?>
		</td>
	</tr>
</table>

<?php 
$dadosPerguntas = obterPerguntasStatus($codigoEmpresa, $codigoLotacao, $cargo, $localidade, $codigoCiclo);

?>

<table cellpadding="2" cellspacing="0" align="center" width="95%" border="0" style="margin-left: 10px; margin-right: 10px;">

	<?php 
	$cargoAnterior = " ";
	$grupoDisciplinaAnterior = "";
	$contadorGrupoDisciplina = 0;
	$contadorPerguntas = 0;
	$contadorCargos = 0;
	$totalOcorrenciasGrupoDisciplina = 0;
	$totalAcertosGrupoDisciplina = 0;
		while($linha = mysql_fetch_array($dadosPerguntas)){
			
			//Cargo
			if (strtoupper($cargoAnterior) != ""){
				$cargoAnterior = "";
				$grupoDisciplinaAnterior = "";
				$contadorCargos++;
				$contadorGrupoDisciplina = 0;
				$estilo = "style=\"border-top: 1px solid #000;\"";
				if ($contadorCargos == 1){
					$estilo = "";
				}else
				{
					?>
					
					<tr class="textblk" style="background: #ccc; font-weight: bold;">
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" nowrap colspan="3"><b>Quantidade de ocorr�ncias</b></td>
						<td style="text-align: center; border-top: 1px solid #000; width: 100px;" nowrap><b><?php echo $totalOcorrenciasGrupoDisciplina;?></b></td>
						<td style="text-align: center; border-top: 1px solid #000; width: 100px; border-right: 1px solid #000;" nowrap><b><?php echo formatarValores($totalAcertosGrupoDisciplina / $totalOcorrenciasGrupoDisciplina * 10, 2, "0,00");?></b></td>
					</tr>
					
					<?php 
				}
				$totalOcorrenciasGrupoDisciplina = 0;
				$totalAcertosGrupoDisciplina = 0;
				?>
				<tr class="textblk" style="font-weight: bold;">
					<td nowrap colspan="5" <?php echo $estilo;?>><br /><?php echo " ";?></td>
				</tr>
				<?php 
			}
			
			//GrupoDisciplina
			if (strtoupper($grupoDisciplinaAnterior) != strtoupper($linha["NM_GRUPO_DISCIPLINA"])){
				$grupoDisciplinaAnterior = $linha["NM_GRUPO_DISCIPLINA"];
				$contadorGrupoDisciplina++;
				$contadorPerguntas = 0;
			?>
				<tr class="textblk" style="background: #ccc; font-weight: bold;">
					<td style="text-align: left; border-top: 1px solid #000; border-left: 1px solid #000;"><b><?php echo $contadorGrupoDisciplina;?></b></td>
					<td colspan="2" style="border-top: 1px solid #000;" nowrap class="colunaCompetencia"><b><?php echo $linha["NM_GRUPO_DISCIPLINA"];?></b></td>
					<td style="text-align: center; border-top: 1px solid #000; width: 100px;" nowrap><b>Ocorr�ncias</b></td>
					<td style="text-align: center; border-top: 1px solid #000; width: 100px; border-right: 1px solid #000;" nowrap><b>M�dia</b></td>
				</tr>
			<?php 
			}
			$contadorPerguntas++;
			$totalOcorrenciasGrupoDisciplina = $totalOcorrenciasGrupoDisciplina + $linha["TOTAL_OCORRENCIA"];
			$totalAcertosGrupoDisciplina = $totalAcertosGrupoDisciplina + $linha["TOTAL_ACERTO"];
			?>
				<tr class="textblk" style="font-weight: bold; font-style: italic">
					<td style="text-align: left; border-top: 1px solid #000; border-left: 1px solid #000;"><?php echo $contadorPerguntas;?></td>
					<td colspan="2" style="border-top: 1px solid #000;"><?php echo str_replace("\n","<br />", $linha["DS_PERGUNTAS"]);?></td>
					<td style="text-align: center; border-top: 1px solid #000;" nowrap><?php echo $linha["TOTAL_OCORRENCIA"];?></td>
					<td style="text-align: center; border-top: 1px solid #000; border-right: 1px solid #000;" nowrap><?php echo formatarValores($linha["MEDIA_ACERTO"] * 10, 2, "0,00");?></td>
				</tr>
				<?php 
					for($i = 1; $i < 5; $i++){
						if ($linha["RESPOSTA_$i"] == "")
							continue;
						
						$letra = letraResposta($i);
						$mediaResposta = $linha["TOTAL_RESPOSTA_$i"] / $linha["TOTAL_OCORRENCIA"];
						$mediaResposta = formatarValores($mediaResposta, 2, "0,00");
						$estiloCerta = "";
						if($linha["RESPOSTA_CERTA"] == $i){
							$estiloCerta = "background: #ccc;";
						}
						
						echo "<tr class=\"textblk\">
								<td style=\"border-top: 1px solid #000; border-left: 1px solid #000; $estiloCerta \">$letra</td>
								<td colspan=\"2\" style=\"border-top: 1px solid #000;\">{$linha["RESPOSTA_$i"]}</td>
								<td style=\"text-align: center; border-top: 1px solid #000;\">{$linha["TOTAL_RESPOSTA_$i"]}</td>
								<td style=\"text-align: center; border-top: 1px solid #000; border-right: 1px solid #000;\">$mediaResposta</td>
							</tr>";
						
					}
				
				?>
				
			<?php 
		}
	
	if ($contadorCargos > 0){
	?>

	<tr class="textblk" style="background: #ccc; font-weight: bold;">
		<td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000;" nowrap colspan="3"><b>Quantidade de ocorr�ncias</b></td>
		<td style="text-align: center; border-top: 1px solid #000; width: 100px; border-bottom: 1px solid #000;" nowrap><b><?php echo $totalOcorrenciasGrupoDisciplina;?></b></td>
		<td style="text-align: center; border-top: 1px solid #000; width: 100px; border-right: 1px solid #000; border-bottom: 1px solid #000;" nowrap><b><?php echo formatarValores($totalAcertosGrupoDisciplina / $totalOcorrenciasGrupoDisciplina * 10, 2, "0,00");?></b></td>
	</tr>
	
	<?php 
	}
	?>
	
</table>
<div>&nbsp;</div>
	<?php

		if (!isset($_POST["btnExcel"]) || $_POST["btnExcel"] == "") {

	?>
<div style="width:100%;text-align:center">
	<form method="POST" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']; ?>">
		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">
		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
	</form>
</div>	
	<?php

		}

	?>
</body>
</html>