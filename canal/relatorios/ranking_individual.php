<html>
	<TITLE>Colabor� - Consultoria e Educa&ccedil;&atilde;o Corporativa</TITLE>
	<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<META http-equiv="pragma" content="no-cache">
	<LINK rel="stylesheet" type="text/css" href="../../include/css/admincolaborae.css">
<body>

<?php


include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');

$codigoAgrupamento = "";
$limiteResultado = "";

$POST = null;
if (isset($_GET["id"]))
{
	
	$farol = new col_farol();
	$farol->CD_FAROL = $_GET["id"];
	$farol = $farol->obter();
	
	$POST = array();
	$POST["txtNomeRelatorio"] = $farol->NM_FAROL;
	$POST["txtPesoParticipanteAvaliacao"] = $farol->NR_PESO_PARTICIPANTE_AVALIACAO;
	$POST["txtPesoMediaAvaliacao"] = $farol->NR_PESO_MEDIA_AVALIACAO;
	$POST["txtPesoParticipanteSimulado"] = $farol->NR_PESO_PARTICIPANTE_SIMULADO;
	$POST["txtPesoMediaSimulado"] = $farol->NR_PESO_MEDIA_SIMULADO;
	$POST["txtPesoContribuicaoForum"] = $farol->NR_PESO_CONTRIBUICAO_FORUM;
	$POST["txtPesoVelocidadeProva"] = $farol->NR_PESO_VELOCIDADE_PROVA;
	$POST["txtPesoQuantidadeDownload"] = $farol->NR_PESO_QUANTIDADE_DOWNLOAD;
	$POST["txtPesoQuantidadeHits"] = $farol->NR_PESO_QUANTIDADE_HITS;
	$POST["txtPesoQuantidadeSessao"] = $farol->NR_PESO_QUANTIDADE_SESSAO;
	$POST["txtPesoQuantidadeUsuario"] = $farol->NR_PESO_QUANTIDADE_USUARIO;
	
	$POST["cboEmpresa"] = $farol->filtro->CD_EMPRESA;
	$POST["cboUsuario"] = tranformaArrayVazioMenos1(obterValoresUsuario($farol->filtro->usuarios));
	$POST["cboProva"] = tranformaArrayVazioMenos1(obterValoresProva($farol->filtro->provas));
	$POST["cboCargo"] = tranformaArrayVazioMenos1(obterValoresCargo($farol->filtro->cargos));
	$POST["cboLotacao"] = tranformaArrayVazioMenos1(obterValoresLotacao($farol->filtro->lotacoes));
	$POST["cboForum"] = tranformaArrayVazioMenos1(obterValoresForum($farol->filtro->foruns));
	
	$POST["txtDe"] = $farol->DT_INICIO;
	$POST["txtAte"] = $farol->DT_TERMINO;
	
	$limiteResultado = $farol->NR_LIMITE;
	$codigoAgrupamento = split(";", $farol->IN_TIPO_AGRUPAMENTO);
	
	$POST["chktxtPesoParticipanteAvaliacao"] = $farol->IN_PESO_PARTICIPANTE_AVALIACAO;
	$POST["chktxtPesoMediaAvaliacao"] = $farol->IN_PESO_MEDIA_AVALIACAO;
	$POST["chktxtPesoParticipanteSimulado"] = $farol->IN_PESO_PARTICIPANTE_SIMULADO;
	$POST["chktxtPesoMediaSimulado"] = $farol->IN_PESO_MEDIA_SIMULADO;
	$POST["chktxtPesoContribuicaoForum"] = $farol->IN_PESO_CONTRIBUICAO_FORUM;
	$POST["chktxtPesoVelocidadeProva"] = $farol->IN_PESO_VELOCIDADE_PROVA;
	$POST["chktxtPesoQuantidadeDownload"] = $farol->IN_PESO_QUANTIDADE_DOWNLOAD;
	$POST["chktxtPesoQuantidadeHits"] = $farol->IN_PESO_QUANTIDADE_HITS;
	$POST["chktxtPesoQuantidadeSessao"] = $farol->IN_PESO_QUANTIDADE_SESSAO;
	$POST["chktxtPesoQuantidadeUsuario"] = $farol->IN_PESO_QUANTIDADE_USUARIO;
	
}
else
{
	$POST = $_POST;
	
	$limiteResultado = $_REQUEST["txtLimiteResultado"];
	
	if (is_array($_REQUEST["cboAgrupamento"]))
	{
		$codigoAgrupamento  = $_REQUEST["cboAgrupamento"];	
	}
	
}

$codigoUsuarioAtivo = $_SESSION["cd_usu"];

$nomeRelatorio = $POST["txtNomeRelatorio"];

if ($nomeRelatorio == "")
{
	$nomeRelatorio = "Relat�rio Farol";
}

$pesoParticipanteAvaliacao = ZeraVazio("txtPesoParticipanteAvaliacao");
$PesoMediaAvaliacao = ZeraVazio("txtPesoMediaAvaliacao");
$PesoParticipanteSimulado = ZeraVazio("txtPesoParticipanteSimulado");
$PesoMediaSimulado = ZeraVazio("txtPesoMediaSimulado");
$PesoContribuicaoForum = ZeraVazio("txtPesoContribuicaoForum");
$PesoVelocidadeProva = ZeraVazio("txtPesoVelocidadeProva");
$PesoQuantidadeDownload = ZeraVazio("txtPesoQuantidadeDownload");
$PesoQuantidadeHits = ZeraVazio("txtPesoQuantidadeHits");
$PesoQuantidadeSessao = ZeraVazio("txtPesoQuantidadeSessao");
$PesoQuantidadeUsuario = ZeraVazio("txtPesoQuantidadeUsuario");

$inpesoParticipanteAvaliacao = TratarCheckFarol($POST["chktxtPesoParticipanteAvaliacao"]);
$inPesoMediaAvaliacao = TratarCheckFarol($POST["chktxtPesoMediaAvaliacao"]);
$inPesoParticipanteSimulado = TratarCheckFarol($POST["chktxtPesoParticipanteSimulado"]);
$inPesoMediaSimulado = TratarCheckFarol($POST["chktxtPesoMediaSimulado"]);
$inPesoContribuicaoForum = TratarCheckFarol($POST["chktxtPesoContribuicaoForum"]);
$inPesoVelocidadeProva = TratarCheckFarol($POST["chktxtPesoVelocidadeProva"]);
$inPesoQuantidadeDownload = TratarCheckFarol($POST["chktxtPesoQuantidadeDownload"]);
$inPesoQuantidadeHits = TratarCheckFarol($POST["chktxtPesoQuantidadeHits"]);
$inPesoQuantidadeSessao = TratarCheckFarol($POST["chktxtPesoQuantidadeSessao"]);
$inPesoQuantidadeUsuario = TratarCheckFarol($POST["chktxtPesoQuantidadeUsuario"]);

$codigoEmpresa = $POST['cboEmpresa'];
$codigoUsuario = joinArray($POST['cboUsuario']);
$codigoProva = joinArray($POST['cboProva']);
$cargo = joinArray($POST['cboCargo'],"','");
$cargoCorrigido = corrigePlic($cargo);
$codigoLotacao = joinArray($POST['cboLotacao']);
$codigoForum = joinArray($POST['cboForum']);

$dataInicio = $POST["txtDe"];
$dataTermino = $POST["txtAte"];

if ($dataInicio == "" && $dataTermino == "")
{
	$dataTermino = date("d/m/Y");
}

$dataInicioTexto = $dataInicio;
$dataTerminoTexto = $dataTermino;

$dataTerminoAnterior = "";
$dataTerminoAnteriorTexto = "";

$sqlDataProva = "";
$sqlDataForum = "";
$sqlDataAcesso = "";
$sqlDataAnteriorProva = "";
$sqlDataAnteriorForum = "";
$sqlDataAnteriorAcesso = "";

if ($dataInicio != ""){
	$dataInicio = substr($dataInicio, 6, 4) . substr($dataInicio, 3, 2) . substr($dataInicio, 0, 2);
	
	$sqlDataProva = " $sqlData AND  DATE_FORMAT(DT_INICIO, '%Y%m%d') >= '$dataInicio' ";
	$sqlDataForum = " $sqlData AND  DATE_FORMAT(data, '%Y%m%d') >= '$dataInicio' ";
	$sqlDataAcesso = " $sqlData AND  DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicio' ";
}

if ($dataTermino != ""){
	$ano = substr($dataTermino, 6, 4);
	$mes = substr($dataTermino, 3, 2);
	$dia = substr($dataTermino, 0, 2);
	$dataTermino = $ano . $mes . $dia;
	
	$dataTerminoAnteriorTexto = strftime("%d/%m/%Y" , mktime(0, 0, 0, $mes, $dia - 1, $ano));
	$dataTerminoAnterior = substr($dataTerminoAnteriorTexto, 6, 4) . substr($dataTerminoAnteriorTexto, 3, 2) . substr($dataTerminoAnteriorTexto, 0, 2);
		
	$sqlDataProva = " $sqlData AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= '$dataTermino' ";
	$sqlDataForum = " $sqlData AND DATE_FORMAT(data, '%Y%m%d') <= '$dataTermino' ";
	$sqlDataAcesso = " $sqlData AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataTermino' ";
	
	$sqlDataAnteriorProva = " $sqlData AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= '$dataTerminoAnterior' ";
	$sqlDataAnteriorForum = " $sqlData AND DATE_FORMAT(data, '%Y%m%d') <= '$dataTerminoAnterior' ";
	$sqlDataAnteriorAcesso = " $sqlData AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataTerminoAnterior' ";
}


//Montar Individual
$limiteResultado = "";
$texto = "";

$sqlUsuario = "SELECT u.cargofuncao, u.lotacao, l.DS_LOTACAO, e.DS_EMPRESA
				FROM
					col_usuario u 
					LEFT JOIN col_lotacao l ON u.lotacao = l.CD_LOTACAO
					LEFT JOIN col_empresa e ON u.empresa = e.CD_EMPRESA
				WHERE
					u.CD_USUARIO = $codigoUsuarioAtivo";

$resultadoUsuario = DaoEngine::getInstance()->executeQuery($sqlUsuario, true);

$dadosUsuario = mysql_fetch_array($resultadoUsuario);

if (is_array($codigoAgrupamento))
{

	foreach ($codigoAgrupamento as $item)
	{
		
		switch ($item)
		{
			case "1":
				$texto = "$texto da Lota��o {$dadosUsuario["DS_LOTACAO"]}";
				//$codigoLotacao = $dadosUsuario["lotacao"];
				break;
				
			case "2":
				$texto = "$texto do Cargo/Fun��o {$dadosUsuario["cargofuncao"]}";
				//$cargo = $dadosUsuario["cargofuncao"];
				break;
		}
		
	}
}
	
echo $agrupador;


//MontarAgrupamento
$arrayAgrupamento = $codigoAgrupamento;

$codigoAgrupamento = " u.NM_USUARIO, u.NM_SOBRENOME, u.login, u.CD_USUARIO, l.DS_LOTACAO, l.CD_LOTACAO, u.cargofuncao, ";

$resultadoAgrupado = executarQueryAcessoGeral($sqlDataProva, $sqlDataAcesso, $sqlDataForum);

$pontuacaoAnterior = -1;
$posicao = 1;
$ranking = 1;
$qtdResultados = mysql_num_rows($resultadoAgrupado);
$existeNoRanking = false;
$posicaoLotacao = 1;
$posicaoCargo = 1;
	
while ($linha = mysql_fetch_array($resultadoAgrupado))
{
	
	if ($pontuacaoAnterior != $linha["TOTAL"])
	{
		$posicao = $ranking;
	}
	
	if ($_SESSION["cd_usu"] == $linha["CD_USUARIO"])
	{
		$existeNoRanking = true;
		break;
	}
	
	if ($linha["CD_LOTACAO"] == $dadosUsuario["lotacao"])
	{
		$posicaoLotacao++;
	}

	if ($linha["cargofuncao"] == $dadosUsuario["cargofuncao"])
	{
		$posicaoCargo++;
	}

	
	$ranking++;
}

?>

<table cellpadding="1" cellspacing="0" border="0" width="100%" align="center">
	<tr>
		<td colspan="2" class="textblk" align="center" bgcolor="#000000" ><span class="title" style="color:#ffffff"><?php echo  $nomeRelatorio; ?></span></td>
	</tr>
	<td>
		<td colspan="2">&nbsp;</td>
	</td>
	<tr>
		<td width="108" rowspan="2"><img width="104" height="151" src="../images/trofeu.jpg" alt="Ranking"/></td>
		<td valign="bottom" class="textblk" width="100%" style="border-bottom: 1px solid #000000">
			<b>Ranking</b>
		</td>
	</tr>
	<tr valign="top">
		<td class="textblk" width="100%">
			Empresa: <?php echo $posicao ?> <br />
			Lota��o: <?php echo $posicaoLotacao ?><br />
			Cargo/Fun��o: <?php echo $posicaoCargo ?>
		</td>
	</tr>
</table>


	
<?php

	function ZeraVazio($nomeCampo)
	{
		
		global $POST;
		
		if (is_numeric($POST[$nomeCampo]))
		{
			return $POST[$nomeCampo];
		}
		else
		{
			return 0;
		}
		
	}

	function executarQueryAcessoGeral($sqlDataProva, $sqlDataAcesso, $sqlDataForum)
	{
		global 	$codigoEmpresa, $codigoUsuario, $codigoProva, $cargo, $cargoCorrigido,
				$codigoLotacao, $codigoForum, $codigoAgrupamento, $limiteResultado,
				$pesoParticipanteAvaliacao, $PesoMediaAvaliacao, $PesoParticipanteSimulado,
				$PesoMediaSimulado, $PesoContribuicaoForum, $PesoVelocidadeProva, $PesoQuantidadeDownload,
				$PesoQuantidadeHits, $PesoQuantidadeSessao, $PesoQuantidadeUsuario;
				

		$count = "";	
		$ordenacao = "";
		$agrupamento = "";
		$limite = "";
		
		if ($codigoAgrupamento != "")
		{
			$count = "$codigoAgrupamento $count";
			
			$codigoAgrupamentoInterno = substr($codigoAgrupamento, 0, strrpos($codigoAgrupamento,','));
			
			$ordenacao = "ORDER BY TOTAL DESC, $codigoAgrupamentoInterno";
			$agrupamento = "GROUP BY $codigoAgrupamentoInterno";
			
			if (is_numeric($limiteResultado))
				$limite = "LIMIT 0, $limiteResultado";

		}	
				
				
		$query ="SELECT
					$count

					sum((	SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
						 	WHERE
								pa.CD_USUARIO = u.CD_USUARIO
							AND pa.VL_MEDIA IS NOT NULL
							AND	p.IN_PROVA = 1
							AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
							$sqlDataProva)) * $pesoParticipanteAvaliacao +
				
				
					sum((	SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 0
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)) * $PesoParticipanteSimulado +
				
				
					COALESCE(ROUND(
						SUM((SELECT SUM(pa.VL_MEDIA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							 WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 1
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)) /
				
					sum((	SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 1
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)),2),0) * $PesoMediaAvaliacao +
				
				
				
					COALESCE(ROUND(
						SUM((SELECT SUM(pa.VL_MEDIA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							 WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 0
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)) /
				
					sum((	SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 0
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)),2),0) * $PesoMediaSimulado +
				
				
				
					COALESCE(ROUND(
						SUM((SELECT SUM(p.DURACAO_PROVA * 60 / TIME_TO_SEC(TIMEDIFF(pa.DT_TERMINO, pa.DT_INICIO))) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							 WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 1
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)) /
				
						sum((SELECT COUNT(p.CD_PROVA) FROM col_prova p INNER JOIN
								col_prova_aplicada pa ON p.CD_PROVA = pa.CD_PROVA
							 WHERE
								pa.CD_USUARIO = u.CD_USUARIO
								AND pa.VL_MEDIA IS NOT NULL
								AND	p.IN_PROVA = 1
								AND	(p.CD_PROVA in ($codigoProva) OR '$codigoProva' = '-1')
								$sqlDataProva)),2),0) * $PesoVelocidadeProva +
				
				
					SUM((	SELECT COUNT(tf.id) FROM col_foruns f INNER JOIN
								tb_forum tf ON tf.CD_FORUM = f.CD_FORUM
							WHERE
								u.login = tf.name
								AND	(f.CD_FORUM in ($codigoForum) OR '$codigoForum' = '-1')
								$sqlDataForum)) * $PesoContribuicaoForum +
				
					SUM((	SELECT count(a.CD_USUARIO) AS TOTAL FROM col_acesso a
								LEFT JOIN col_pagina_acesso pa ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
							WHERE
								NOT pa.IN_ORDENAR = -1
								AND u.CD_USUARIO = a.CD_USUARIO
								AND	DS_PAGINA_ACESSO LIKE 'Download %'
								$sqlDataAcesso)) * $PesoQuantidadeDownload +
				
					SUM((	SELECT count(a.CD_USUARIO) AS TOTAL FROM col_acesso a
								LEFT JOIN col_pagina_acesso pa ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
							WHERE
								NOT pa.IN_ORDENAR = -1
								AND u.CD_USUARIO = a.CD_USUARIO
								$sqlDataAcesso)) * $PesoQuantidadeHits +
				
					SUM((	SELECT COUNT(DISTINCT(DS_ID_SESSAO)) FROM col_acesso a
								LEFT JOIN col_pagina_acesso pa ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
							WHERE
								NOT pa.IN_ORDENAR = -1
								AND u.CD_USUARIO = a.CD_USUARIO
								$sqlDataAcesso)) * $PesoQuantidadeSessao +
				
					SUM((	SELECT COUNT(DISTINCT a.CD_USUARIO + DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'))
							FROM col_acesso a
								LEFT JOIN col_pagina_acesso pa ON a.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
							WHERE
								NOT pa.IN_ORDENAR = -1
								AND u.CD_USUARIO = a.CD_USUARIO
								$sqlDataAcesso)) * $PesoQuantidadeUsuario  AS TOTAL
				
				FROM
					col_empresa e
					LEFT JOIN col_lotacao l on l.CD_EMPRESA = e.CD_EMPRESA
					LEFT JOIN col_usuario u ON u.lotacao = l.CD_LOTACAO
				WHERE
					(e.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
				AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
				AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
				AND (u.cargofuncao in ('$cargo') OR '$cargoCorrigido' = '-1')
				$agrupamento
				$ordenacao
				$limite";

				//echo $query;
		
				return DaoEngine::getInstance()->executeQuery($query, true);
		
	}
?>
	
</body>
</html>

<?php

	ob_end_flush();
	
	function tranformaArrayVazioMenos1($array)
	{
		if (count($array) == 0)
		{
			return "-1";
		}
		
		return $array;
		
	}
	
	function montarAgrupamento($array)
	{
		if (!is_array($array))
		{
			return $array;
		}
		
		$agrupador = "";
		
		foreach ($array as $item)
		{
			
			switch ($item)
			{
				case "1":
					$agrupador = "$agrupador l.DS_LOTACAO, l.CD_LOTACAO, ";
					break;
					
				case "2":
					$agrupador = "$agrupador u.cargofuncao, ";
					break;
				default:
					$agrupador = "$agrupador u.NM_USUARIO, u.NM_SOBRENOME, u.login, u.CD_USUARIO, ";
					break;
			}
			
		}
		
		return $agrupador;
	}

?>