<?php
//Includes

include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include('relatorio_acesso_util.php');

$tituloTotal = "";
$tituloRelatorio = "Radar Consolidado";


//Obter parametros
$POST = obterPost();
$codigoEmpresa = $POST['cboEmpresa'];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$codigoCiclo = $_REQUEST["hdnCiclo"];
$nomeCiclo = obterNomeCiclo($codigoCiclo);

$usaData = true;

if ($dataInicial == "" && $dataFinal == ""){

	$usaData = false;

}


if ($dataInicial==""){

	$dataInicial = '01/01/2008';

}

if ($dataFinal=="") {

	$dataFinal = date("d/m/Y");

}

$dataAtual = $dataInicial;
// Fim obter parametros


//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa



//Acertar as p�ginas a serem exibidas
$paginaRn = new col_pagina_acesso();
$paginaRn->prepararRelatorios($dataInicial, $dataFinal, $codigoEmpresa);


if ($_POST["btnExcel"] != "") {

	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=RadarConsolidado.xls");

}

$dataInicialQuery = conveterDataParaYMD($dataInicial);

$dataFinalQuery = conveterDataParaYMD($dataFinal);


//Obter o total de participantes da empresa
$sqlTotalGeral = "SELECT

		  COUNT(DISTINCT DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'), a.CD_USUARIO) AS QD_TOTAL

		FROM

		  col_acesso a

		  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO

		WHERE

		  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)

		  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";



$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);

$linha = mysql_fetch_row($resultadoTotalGeral);

$totalGeralEmpresa = $linha[0];



//Obter o total de participantes do filtro

$sqlTotalGeral = "SELECT

		  COUNT(DISTINCT DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'), a.CD_USUARIO) AS QD_TOTAL

		FROM

		  col_acesso a

		  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO

		WHERE

		  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)

		  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')

		  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')

		  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))

		  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";



$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);

$linha = mysql_fetch_row($resultadoTotalGeral);

$totalGeralTodos = $linha[0];





$totalParticipantesEmpresa = 0;

$totalParticipantesGrupo = 0;



	

//Obter o total de participantes da empresa

$sqlTotalGeral = "SELECT

		  COUNT(DISTINCT(u.CD_USUARIO)) AS QD_TOTAL

		FROM

		  col_acesso a

		  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO

		WHERE

		  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)

		  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";



$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);

$linha = mysql_fetch_row($resultadoTotalGeral);

$totalParticipantesEmpresa = $linha[0];

	

//Obter o total de participantes do filtro

$sqlTotalGeral = "SELECT

		  COUNT(DISTINCT(u.CD_USUARIO)) AS QD_TOTAL

		FROM

		  col_acesso a

		  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO

		WHERE

		  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)

		  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')

		  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')

		  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))

		  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')";



$resultadoTotalGeral = DaoEngine::getInstance()->executeQuery($sqlTotalGeral,true);

$linha = mysql_fetch_row($resultadoTotalGeral);

$totalParticipantesGrupo = $linha[0];

	



ob_start();

	
?>



<html>

<head>

	<title>Colaborae Consultoria e Educa��o Corporativa</title>

	

	<?php

	

	if ($_POST["btnExcel"] == "") {



	?>

	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">		

	<?php

	}

	?>

	

	

	<style type="text/css">



		@media print {

			.buttonsty {

			  display: none;

			}

			.textoInformativo{

				display: none;

			}

		}

		

		.textoInformativo{

			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						

		}

	

		.titRelat, .titRelatD

		{

			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;

		}

		

		.titRelatD

		{

			text-align: left;

		}

		

		.titRelatTop

		{

			border-top: 1px solid #000000; text-align: center;

		}

		

		.titRelatEsq

		{

			border-right: 1px solid #000000; border-top: 1px solid #000000;

			border-left: 1px solid #000000;

		}

		

		.titRelatRod

		{

			border-right: 1px solid #000000; border-top: 1px solid #000000;

			border-bottom: 1px solid #000000;

		}

		

		.titRelatEsqRod

		{

			border-right: 1px solid #000000; border-top: 1px solid #000000;

			border-bottom: 1px solid #000000;

			border-left: 1px solid #000000;

		}

		

		.bdLat

		{

			border-right: 1px solid #000000;

		}

		

		.bdLatEsq

		{

			border-right: 1px solid #000000;

			border-left: 1px solid #000000;

		}

		

		.bdEsq

		{

			border-left: 1px solid #000000;

		}



		.bdRod

		{

			border-bottom: 1px solid #000000;

		}
		
		.bdTop
		{
			border-top: 1px solid #000000;
		}

	</style>

	

</head>



<body>



<br>



<?php



	$colSpan = 0;

	$colunasMaximo = 22;

	$colunasAparecer = 0;

	$colunasTotal = 0;

	

	if ($usaData)

	{

		$dataAtualOriginal = $dataAtual;

		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal))) {



			if ($colunasAparecer < $colunasMaximo)

			{

				$colSpan++;

				$colunasAparecer++;

			}

			

			$colunasTotal++;

			$dataAtual = somarUmDia($dataAtual);

		}


		$dataAtual = $dataAtualOriginal;

		

	}

	

	$colunaInicial = 1;

	if ($colunasTotal > $colunasAparecer)

	{

		$colunaInicial = $colunasTotal - $colunasAparecer + 1;

	}





?>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">

	<tr style="height: 30px">

		<td class='textblk'>

			<p><b><?php echo $nomeEmpresa; ?></b></p>

		</td>

		<td width="65%" class="textblk" colspan="<?php echo $colSpan + 1; ?>" align="center">

				<?php echo $tituloRelatorio; ?>

			</b>

		</td>

		<td style="text-align: right" class='textblk'>

			<p><?php echo date("d/m/Y") ?></p>

		</td>

	</tr>

</table>





<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">

	<?php
		if ($POST["nomeFiltro"] != "")
		{
			linhaCabecalho("<b>Nome do Grupo Gerencial","{$POST["nomeFiltro"]}</b>");
		}
		
		linhaCabecalho("<b>Ciclo","$nomeCiclo</b>");
	?>


	<tr class="textblk">

		<td width="100%" colspan="<?php echo $colSpan + 3; ?>">

			<b>Per�odo:&nbsp; <?php

												if($dataInicioTexto!=""){

													echo "de $dataInicioTexto ";

													if($dataTerminoTexto!=""){

														echo "a $dataTerminoTexto";

													}

												}else{

													echo "Todos ";

												}

							?>

		</b></td>

	</tr>

	<?php

	

		$totalUsuariosEmpresa = obterTotalUsuarios($codigoEmpresa);

		$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);

		

	


linhaCabecalho("Cadastrados na Empresa", $totalUsuariosEmpresa);

linhaCabecalho("Participantes da Empresa", $totalParticipantesEmpresa);

linhaCabecalho("Acessos de Participantes da Empresa", $totalGeralEmpresa);

$percentualParticipacaoEmpresa = str_ireplace(".",",",round($totalGeralEmpresa/$totalParticipantesEmpresa,2));

linhaCabecalho("Acessos de Participantes/Participantes da Empresa", $percentualParticipacaoEmpresa);

if (!isset($_GET["cboEmpresa"]))

{

	linhaCabecalho("Cadastrados do Grupo", $totalUsuarioGrupo);

	linhaCabecalho("Participantes do Grupo", $totalParticipantesGrupo);

	linhaCabecalho("Acessos de Participantes do Grupo", $totalGeralTodos);

	$percentualParticipacaoGrupo = str_ireplace(".",",",round($totalGeralTodos/$totalParticipantesGrupo,2));

	linhaCabecalho("Acessos de Participantes do Grupo/Participantes do Grupo", "$percentualParticipacaoGrupo");

}

?>

	

</table>



<table cellpadding="1" cellspacing="0" style="border: 1px solid black;" align="center" width="95%">


	<tr class='textblk'>

		<td  rowspan="2" class="bdLat" width="100%" colspan="2">

			Participantes (Acumulado)

		</td>
		
	
<?php



	$dataAtualOriginal = $dataAtual;

	$trDiasMeses = "";

	if ($usaData)

	{

		$meses = array();

		$contador = 1;

		

		//Define os meses envolvidos e a quantidade de dias de cada um at� a data limite

		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal))) {



			if (!($colunaInicial > $contador))

			{

				$aData = split("/", $dataAtual);

				$mesAtual = $aData[1];

				if ($meses[$mesAtual] == null)

				{

					$meses[$mesAtual] = 0;

				}

				$meses[$mesAtual] = $meses[$mesAtual] + 1;
				

				$trDiasMeses =  "$trDiasMeses

									<td class='titRelat' >

										{$aData[0]}

									</td>";

			}


			$dataAtual = somarUmDia($dataAtual);

			$contador++;

			

		}

	

	

		foreach($meses as $chave => $valor)

		{

			$nomeMes = obterTextoMes($chave);

			echo "	<td class='bdLat' colspan=\"$valor\" align=\"center\">

						$nomeMes

					</td>";



		}

	

	}

	

?>

		<td align="center" rowspan="2">

			Total

		</td>

	</tr>

	<tr class='textblk'>

		

<?php

	if ($usaData)

	{

		//Percorre os dias at� a data limite

		echo $trDiasMeses;

		

	}



?>


	</tr>
	<tr class='textblk' bgcolor='#cccccc'>
		<td colspan="2" class="titRelatD">
			Novos Participantes
		</td>

<?php
	
	$totalUsuariosAcumuladoDia = obterTotaisUsuariosAcumuladoDia();
	
	
	$contador = 1;

	$dataAtual = $dataAtualOriginal;
	
	$totalAcumulado = 0;
	$totalNaoParticipantes = 0;
	$jaContou = false;
	
	$trNaoParticipantes = "";
	
	while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))

	{

		if (!($colunaInicial > $contador))

		{
			
			if ($jaContou == false)
			{
				$jaContou = true;
				
					$dataAtualConvertida = conveterDataParaYMD($dataAtual);
	
					foreach($totalUsuariosAcumuladoDia as $chave => $valor)
					{
						if ($dataAtualConvertida > $chave)
						{
							$totalAcumulado = $totalAcumulado + $valor;
						}
					
					}
				
			}
			

			if ($totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)] == null)

				$totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)] = 0;

			$totalAcumulado = $totalAcumulado + $totalUsuariosAcumuladoDia[conveterDataParaYMD($dataAtual)];

			echo "<td class='titRelat'>

				$totalAcumulado

			</td>";
			
			
			
			if (!isset($_GET["cboEmpresa"]))
			{
				$totalNaoParticipantes = $totalUsuarioGrupo - $totalAcumulado;
			}
			else 
			{
				$totalNaoParticipantes = $totalUsuariosEmpresa - $totalAcumulado;
			}
			
			
			
			$trNaoParticipantes = "$trNaoParticipantes
									<td class='titRelat'>
						
										$totalNaoParticipantes
						
									</td>";

		}

		$contador++;			

		$dataAtual = somarUmDia($dataAtual);

	}

	echo "<td class='titRelatTop'>

				$totalAcumulado

			</td>";
	
	$trNaoParticipantes = "$trNaoParticipantes
									<td class='titRelatTop'>
						
										$totalNaoParticipantes
						
									</td>";

	
?>	
	




	</tr>

	
	<tr class='textblk' bgcolor='#cccccc'>
		<td colspan="2" class="titRelatD">
			N�o Participantes
		</td>
		
		<?php
			echo $trNaoParticipantes;
		?>
		
	
	</tr>

	
	<tr>
		<td class='titRelatTop' colspan="<?php echo $colunasAparecer + 3; ?>">&nbsp;</td>
	</tr>
		
	
	<tr class='textblk'>

		<td class="bdTop" width="100%" colspan="<?php echo $colunasAparecer + 3; ?>">

			Acessos (Di�rio)

		</td>

	</tr>
	<tr>

<?php



	$totalizadorDias = array();

	$totalGeral = 0;

	$acessoUsuarios = array();
	
	$acessosDia = array();

	$sqlTotalDia = "SELECT

			  DATE_FORMAT(DT_ACESSO, '%Y%m%d') AS DT_ACESSO,

			  COUNT(DISTINCT DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'), a.CD_USUARIO) AS QD_TOTAL

			FROM

			  col_acesso a

			  LEFT JOIN col_usuario u ON a.CD_USUARIO = u.CD_USUARIO

			WHERE

			  (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)

			  AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')

			  AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')

			  AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))

			  AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')

			  GROUP BY

			  	DATE_FORMAT(DT_ACESSO, '%Y%m%d')";

	

	$resultadoTotalDiario = DaoEngine::getInstance()->executeQuery($sqlTotalDia,true);

	while ($linha = mysql_fetch_row($resultadoTotalDiario)) {

			//echo "aaa";

		$acessosDia[$linha[0]] = $linha[1];

	}


echo "<tr class='textblk' bgcolor='#cccccc'>

			<td class='titRelatD' colspan='2'>

				Total

			</td>

			";

	

	if ($usaData)

	{

		

		$contador = 1;

		$dataAtual = $dataAtualOriginal;

		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))

		{

			if (!($colunaInicial > $contador))

			{

				if ($acessosDia[conveterDataParaYMD($dataAtual)] == null)

					$acessosDia[conveterDataParaYMD($dataAtual)] = 0;

				

				echo "<td class='titRelat'>

						{$acessosDia[conveterDataParaYMD($dataAtual)]}

				</td>";

			}

			$contador++;			

			$dataAtual = somarUmDia($dataAtual);

		}

	}

	

	if ($totalGeralTodos == "")

		$totalGeralTodos = 0;

	

	echo "<td class='titRelatTop'>

			$totalGeralTodos

		</td>";

	

	echo "</tr>";
	
	
	
	$nomesPaginas = obterNomePaginas();
	
	$acessosPorDia = obterTotalAcessosDia();
	
	foreach($nomesPaginas as $chave => $valor)
	{
		
		
		$paginaAcesso = $valor;
		$indiceAcesso = $chave;
		
		echo "<tr class='textblk'>";

		echo "<td class='titRelatD' colspan='2'>$paginaAcesso</td>";

		$dataAtual = $dataAtualOriginal;
		$totalPagina = 0;
		
		$contador = 1;
		
		while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))
		{



			$aData = split("/", $dataAtual);

			$indice = "$indiceAcesso{$aData[2]}{$aData[1]}{$aData[0]}";
			
			$acessosPagina = 0;
			
			if ($acessosPorDia[$indice] != null){

				$acessosPagina = $acessosPorDia[$indice];

			}

			$totalPagina = $totalPagina + $acessosPagina;
			
			if (!($colunaInicial > $contador))

			{

				if ($usaData)

				{

					echo "<td class='titRelat'>

							$acessosPagina

						</td>";

				}

			}

			$dataAtual = somarUmDia($dataAtual);
			$contador++;
		
		}
		
			echo "<td class='titRelatTop'>

				$totalPagina

			</td>";	

	echo "</tr>";
	}

?>

	<tr>
		<td class='titRelatTop' colspan="<?php echo $colunasAparecer + 3; ?>">&nbsp;</td>
	</tr>
		
	
	<tr class='textblk'>

		<td class="bdTop" width="100%" colspan="<?php echo $colunasAparecer + 3; ?>">

			M�dias das Certifica��es

		</td>

	</tr>
	
	<tr class="textblk" bgcolor="#cccccc">
		<td colspan="2" class="titRelatD">
			Certifica��es
		</td>	
	
	


<?php

	$mediaProvas = obterProvasMediasDia();
	$mediaGeralProvas = obterProvasMediasGeral();
	
	$contador = 1;

	$dataAtual = $dataAtualOriginal;
	
	$trAvaliacoes = "";
	
	while (!(conveterDataParaYMD($dataAtual) > conveterDataParaYMD($dataFinal)))

	{

		if (!($colunaInicial > $contador))

		{
			$indiceAvaliacao = conveterDataParaYMD($dataAtual);
			$indiceAvaliacao = "1$indiceAvaliacao";

			if ($mediaProvas[$indiceAvaliacao] == null)

				$mediaProvas[$indiceAvaliacao] = "-";

			echo "<td class='titRelat'>

				{$mediaProvas[$indiceAvaliacao]}

			</td>";
			
			
			
			$trAvaliacoes = "$trAvaliacoes
									<td class='titRelat'>
						
										{$mediaProvas[$indiceAvaliacao]}
						
									</td>";

		}

		$contador++;			

		$dataAtual = somarUmDia($dataAtual);

	}

	echo "<td class='titRelatTop'>

		{$mediaGeralProvas["1"]}

			</td>";
	
	$trAvaliacoes = "$trAvaliacoes
									<td class='titRelatTop'>
						
										100
						
									</td>";

?>
	
	</tr>

</table>



	<?php

		if ($_POST["btnExcel"] == "") {

	?>

	<div style="width:100%;text-align:center">

		<br />

		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']; ?>">

		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">

		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">

		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>

		</form>	

	</div>

	<?php

		}

	?>

	

</body>

</html>



<?php



	ob_end_flush();



	function obterTextoMes($numero)

	{

		switch ($numero)

		{

			case 1:

				return "Janeiro";

				break;

			case 2:

				return "Fevereiro";

				break;

			case 3:

				return "Mar�o";

				break;

			case 4:

				return "Abril";

				break;

			case 5:

				return "Maio";

				break;

			case 6:

				return "Junho";

				break;

			case 7:

				return "Julho";

				break;

			case 8:

				return "Agosto";

				break;

			case 9:

				return "Setembro";

				break;

			case 10:

				return "Outubro";

				break;

			case 11:

				return "Novembro";

				break;

			case 12:

				return "Dezembro";

				break;

		}

	}

	

	

	function obterTextoDiaSemana($numero)

	{



		switch ($numero) {

			case 0:

				return 'dom';

				break;

			case 1:

				return 'seg';

				break;

			case 2:

				return 'ter';

				break;

			case 3:

				return 'qua';

				break;

			case 4:

				return 'qui';

				break;

			case 5:

				return 'sex';

				break;

			case 6:

				return 'sab';

				break;



		}

		

	}



	function conveterDataParaYMD($data)

	{

		$aData = split("/", $data);

		$data = "{$aData[2]}{$aData[1]}{$aData[0]}";

		

		return $data;

		

	}

	

	

	

	function somarUmDia($data){

		$aData = split("/", $data);

		$data = date("d/m/Y", strtotime("{$aData[1]}/{$aData[0]}/{$aData[2]}") + 90000);

		

		return  $data;

	}

	

	function linhaCabecalho($titulo, $valor)

	{

		

		global $colSpan;

		

		$colunasCabecalhoTitulo = "";

		$colunasCabecalhoValor = "";

		

		if ($_POST["btnExcel"] != "")

		{

			$colunasCabecalhoValor = $colSpan + 2;

			$colunasCabecalhoValor = "</td><td align=\"left\" width=\"100%\" colspan=\"$colunasCabecalhoValor\">" ;

		}

		else 

		{

			$colunasCabecalhoTitulo = $colSpan + 3;

			$colunasCabecalhoTitulo = "colspan=\"$colunasCabecalhoTitulo\"";

		}

		

		

		echo "<tr class=\"textblk\">

				<td $colunasCabecalhoTitulo>

					$titulo:

				$colunasCabecalhoValor

					$valor

				</td>

			</tr>";

		

	}

		

?>