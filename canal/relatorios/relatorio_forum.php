<?php

include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');

class relatorio_forum{
	
	static $quantidadeRespostasTema;
	static $sqlData;
	
}

$POST = obterPost();

$codigoEmpresa = $POST['cboEmpresa'];
$codigoUsuario = joinArray($POST['cboUsuario']);
$codigoForum = joinArray($POST['cboForum']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);

$dataInicio = $POST["txtDe"];
$dataTermino = $POST["txtAte"];
$dataInicioTexto = $dataInicio;
$dataTerminoTexto = $dataTermino;

$nomeEmpresa = 'Todas';
$nomeUsuario = 'Todos';
$nomeForum = 'Todos';
$lotacao = "";

$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];


if (is_array($POST['cboForum']))
{
	if (count($POST['cboForum']) == 1)
	{
		$sql = "SELECT DS_FORUM FROM col_foruns WHERE CD_FORUM = $codigoForum";
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		$linhaForum = mysql_fetch_array($resultado);
		$nomeForum = $linhaForum["DS_FORUM"];
	}
}

$sqlData = "";

if ($dataInicio != ""){
	$dataInicio = substr($dataInicio, 6, 4) . substr($dataInicio, 3, 2) . substr($dataInicio, 0, 2);
	
	$sqlData = " $sqlData AND  DATE_FORMAT(data, '%Y%m%d') >= '$dataInicio' ";
}

if ($dataTermino != ""){
	$dataTermino = substr($dataTermino, 6, 4) . substr($dataTermino, 3, 2) . substr($dataTermino, 0, 2);
		
	$sqlData = " $sqlData AND DATE_FORMAT(data, '%Y%m%d') <= '$dataTermino' ";
}

relatorio_forum::$sqlData = $sqlData;

$sql = "
SELECT
	cf.CD_FORUM, cf.DS_FORUM,
	f.id, f.title,
	e.CD_EMPRESA, e.DS_EMPRESA,
	u.CD_USUARIO, u.NM_USUARIO, u.NM_SOBRENOME,
	DATE_FORMAT(f.data,'%d/%m/%Y %H:%i') AS data
FROM
	tb_forum f
	INNER JOIN col_foruns cf on cf.CD_FORUM = f.CD_FORUM
	INNER JOIN col_usuario u on cf.CD_USUARIO_MEDIADOR = u.CD_USUARIO
	INNER JOIN col_empresa e on e.CD_EMPRESA = u.empresa

WHERE
	(cf.CD_EMPRESA = $codigoEmpresa OR $codigoEmpresa = -1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
AND (u.cargofuncao = ('$cargo') OR '$cargo' = '-1')
AND	(f.CD_FORUM in ($codigoForum) or '$codigoForum' = '-1')
AND (f.parent_id = 0)

ORDER BY
	e.DS_EMPRESA, e.CD_EMPRESA, cf.CD_FORUM, f.id desc";

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);


if (!mysql_num_rows($resultado) > 0){
	echo '<script>alert("N�o h� dados para exibi��o do relat�rio.");self.close();</script>';
	exit();
}



if ($codigoUsuario > -1){
	//filtrar usuario
	$nomeUsuario = $_GET['nomeUsuario'];
}

if ($codigoLotacao > -1){
	
	$lotacao = $_GET['nomeLotacao'];
}

if ($_GET["excel"] == 1) {
	header("Content-Type: application/vnd.ms-excel; name='excel'");
	header("Content-disposition:  attachment; filename=RelatorioAcessos.xls");
}


renderRelatorio($resultado, $nomeEmpresa, $nomeUsuario, $nomeForum, $dataInicioTexto, $dataTerminoTexto, $cargo, $lotacao);

function renderRelatorio($resultado, $nomeEmpresa, $nomeUsuario, $nomeForum, $dataInicio, $dataFim, $cargo, $lotacao){

?>
<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Relat�rio do F�rum <?php echo "$nomeForum"; ?> - <?php echo date("d/m/Y") ?></title>
	
	<?php
	
	if ($_GET["excel"] != 1) {

	?>
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">
	<?php
	}
	?>
	
	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			
			.textoInformativo{
				display: none;
			}
			
			.sumirImpressao{
				display:none;
			}
			
			.aImp{
				display:block;
			}
			
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
		
		.sumirImpressao{
			height: 30px;
		}
		
		.aImp{
			display: none;
		}
		
		.impressao{
			margin-top: -23px;
		}


		
	</style>
	
</head>

<body>
	<table cellpadding="2" cellspacing="0" border="0" width="95%" align="center" class="impressao">
		<tr style="height: 1px; font: 1px">
			<td class="textblk" width="50%" colspan="2">&nbsp;</td>
			<td width="50%" align="left" class="textblk" colspan="2">
				&nbsp;
			</td>
			<td class="textblk" style="width: 1%">
				&nbsp;
			</td>
		</tr>
		<tr class="sumirImpressao">
			<td class="textblk" width="50%" colspan="2"><b>Colaborae Consultoria e Educa��o Corporativa</b></td>
			<td width="50%" align="left" class="textblk" colspan="2">
				<b>Relat�rio do F�rum <?php echo "$nomeForum"; ?></b>
			</td>
			<td class="textblk" style="width: 1%; text-align: right;">
				<?php echo date("d/m/Y") ?>
			</td>
		</tr>
		
		<?php
		
		$relatorio = "";
		$empresaAnterior = 0;
		//$usuarioAnterior = 0;
		$lotacaoAnterior = 0;
		$forumAnterior = 0;
		
		$qtdEmpresa = 0;
		$qtdLotacao = 0;
		$empresas = array();
		$lotacoes = array();
		$respostasEmpresas = array();
		$qtdRespostasEmpresa = 0;
		$respostasLotacoes = array();
		$qtdRespostasLotacao = 0;
		$usuariosEmpresas = array();
		$qtdusuariosEmpresas = array();		
		$usuariosLotacoes = array();
		$qtdusuariosLotacao = array();
		
		$qtdForum = 0;
		$foruns = array();
		$respostasForuns = array();
		$qtdRespostasForum = 0;
		$usuariosForuns = array();
		$qtdUsuariosForuns = array();
		
		$moderador = "";
		
//$quantidadeUsuarios = count($usuariosRespostas)		
		while ($linha = mysql_fetch_array($resultado)) {
			
			$moderador = "{$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}";
			
			if ($empresaAnterior != $linha["empresa"] && $empresaAnterior > 0){
				$empresas[] = $qtdEmpresa;
				$respostasEmpresas[] = $qtdRespostasEmpresa;
				$usuariosEmpresas[] = $qtdusuariosEmpresas;
				$qtdEmpresa = 0;
				$qtdRespostasEmpresa = 0;
				$qtdusuariosEmpresas = null;
				$qtdusuariosEmpresas = array();
			}
			
			if ($forumAnterior != $linha["id"] && $forumAnterior > 0){
				$foruns[] = $qtdForum;
				$respostasForuns[] = $qtdRespostasForum;
				$usuariosForuns[] = $qtdUsuariosForuns;
				$qtdForum = 0;
				$qtdRespostasForum = 0;
				$qtdUsuariosForuns = null;
				$qtdUsuariosForuns = array();
			}
			
//			if ($lotacaoAnterior != $linha["CD_LOTACAO"] && $lotacaoAnterior > 0){
//				$lotacoes[] = $qtdLotacao;
//				$respostasLotacoes[] = $qtdRespostasLotacao;
//				$usuariosLotacoes[] = $qtdusuariosLotacao;
//				$qtdLotacao = 0;
//				$qtdRespostasLotacao = 0;
//				$qtdusuariosLotacao = null;
//				$qtdusuariosLotacao = array();
//			}
			

			$mediaLotacao = $mediaLotacao + $linha["VL_MEDIA"];
			
			$usuariosParaTema = listarUsuarioRespostas($linha["id"], null, null);
			
			foreach ($usuariosParaTema as $usuarioTema){
				$existeEmpresa = false;
				$existeLotacao = false;
				$existeForum = false;
				foreach ($qtdusuariosEmpresas as $usuarioEmpresa){
					if ($linha["CD_USUARIO"] == $usuarioEmpresa["CD_USUARIO"]){
						$existeEmpresa = true;
						break;
					}
				}

//				foreach ($qtdusuariosLotacao as $usuarioLotacao){
//					if ($linha["CD_USUARIO"] == $usuarioLotacao["CD_USUARIO"]){
//						$existeLotacao = true;
//						break;
//					}
//				}
				
				foreach ($qtdUsuariosForuns as $usuarioForum){
					if ($linha["CD_USUARIO"] == $usuarioForum["CD_USUARIO"]) {
						$existeForum = true;
						break;
					}
				}

//				if (!$existeEmpresa){
					$qtdusuariosEmpresas[] = $usuarioTema;
//				}
				
//				if (!$existeLotacao){
//					$qtdusuariosLotacao[] = $usuarioTema;
//				}
				
				//if ($existeForum) {
					$qtdUsuariosForuns[] = $usuarioTema;
				//}
				
			}
			
			$qtdRespostasLotacao = $qtdRespostasLotacao + relatorio_forum::$quantidadeRespostasTema + 1;
			$qtdRespostasEmpresa = $qtdRespostasEmpresa + relatorio_forum::$quantidadeRespostasTema + 1;
			$qtdRespostasForum = $qtdRespostasForum + relatorio_forum::$quantidadeRespostasTema + 1;
			relatorio_forum::$quantidadeRespostasTema = 0;
			
			$qtdEmpresa++;
			$qtdLotacao++;
			$qtdForum++;
			
//			$lotacaoAnterior = $linha["CD_LOTACAO"];
			$forumAnterior = $linha["id"];
			$empresaAnterior = $linha["empresa"];
			
		}
		
		$empresas[] = $qtdEmpresa;
		$lotacoes[] = $qtdLotacao;
		$respostasEmpresas[] = $qtdRespostasEmpresa;
		$respostasLotacoes[] = $qtdRespostasLotacao;
		$usuariosEmpresas[] = $qtdusuariosEmpresas;
		$usuariosLotacoes[] = $qtdusuariosLotacao;
		$foruns[] = $qtdForum;
		$respostasForuns[] = $qtdRespostasForum;
		$usuariosForuns[] = $qtdUsuariosForuns;
		
		$qtdEmpresa = 0;
		$qtdLotacao = 0;
		$qtdForum = 0;
		
		mysql_data_seek($resultado, 0);
		
		
		?>
		
		
		<tr class="textblk">
			<td width="100%" colspan="5">Per�odo:&nbsp; <?php
												if($dataInicio!=""){
													echo "de $dataInicio ";
													if($dataFim!=""){
														echo "a $dataFim";
													}
												}else{
													echo "Todos ";
												}
						echo "</td></tr>";
						
						if ($lotacao != "" || $cargo != "-1")
						{
							echo "<tr>";
							
							echo "<td class='textblk' colspan='5'>";
							
							if ($lotacao != "")
							{
								echo "Lota��o: $lotacao";
							}
							
							if ($lotacao != "" && $cargo != "-1")
							{
								echo "</td><tr><td colspan='5' class='textblk'>";
							}

							if ($cargo != "-1")
							{
								echo "Cargo/Fun��o: $cargo";
							}
								
							echo "</td>";
							
							echo "</tr>";
							
						}
						
						
						echo "<tr class='textblk' style='height: 25px' valign='top'><td align='left' style='width:65%' colspan='2'>Mediador: $moderador</td>";
						echo "<td style='width: 35%'>Login/Nome Participante</td><td style='width: 1%' align='right' style='width:1%'>QT&nbsp;de&nbsp;Participantes:</td><td align='right; width:1%'>&nbsp;&nbsp;QT&nbsp;de&nbsp;Contribui��es:</td></tr>";
						echo "<tr class='textblk'><td colspan='3'  style='border-top: 1px solid black'><b>$nomeEmpresa</b></td>";
												$todosParticipantes = array();
												foreach ($usuariosEmpresas[0] as $participantes){
													
													$jaMostrou = false;
													foreach ($todosParticipantes as $participante){
														if ($participante["CD_USUARIO"] == $participantes["CD_USUARIO"]) {
															$jaMostrou = true;
															break;
														}
													}
													
													if (!$jaMostrou) {
														$todosParticipantes[] = $participantes;
													}
													
												}
						
												$quantidadeUsuariosEmpresaImprime = count($todosParticipantes);
												echo "<td style='border-top: 1px solid black' align='right'>$quantidadeUsuariosEmpresaImprime</td><td style='border-top: 1px solid black' align='right'>{$respostasEmpresas[0]}</td>";
											?>
		</tr>
	
	<?php
		
		$forumAnterior = 0;
		$existeRelatorio = false;
		
		while ($linha = mysql_fetch_array($resultado)) {
			
/*			if ($empresaAnterior != $linha["CD_EMPRESA"]){
				
				$quantidade = $empresas[$qtdEmpresa];
				$quantidadeRespostas = $respostasEmpresas[$qtdEmpresa];
				$quantidadeUsuariosEmpresa = count($usuariosEmpresas[$qtdEmpresa]);
				$qtdEmpresa++;
				//echo "<tr><td style='border-bottom: 1px solid black' colspan='4' bgcolor='#dddddd' class='textblk'>Empresa: <b>{$linha["DS_EMPRESA"]}</b></td></tr>";
				echo "<tr><td colspan='4' class='textblk'><span style='width: 55%'><b>{$linha["DS_EMPRESA"]}</b></span> T�tulos: $quantidade &nbsp; Postagens: $quantidadeRespostas &nbsp; Participantes: $quantidadeUsuariosEmpresa</td></tr>";
				$empresaAnterior = $linha["CD_EMPRESA"];
			}
*/
/*			
			if ($forumAnterior != $linha["id"]){
				$quantidade = $foruns[$qtdForum];
				$quantidadeRespostas = $respostasForuns[$qtdForum];
				$quantidadeUsuariosForum = count($usuariosForuns[$qtdForum]);
				$qtdForum++;
				if ($quantidadeRespostas > 0) {
					
					if ($forumAnterior > 0) {
						echo "<tr><td colspan='5'>&nbsp;</td></tr>";	
					}
					
					$existeRelatorio = true;
					echo "<tr class='textblk'><td colspan='3' style='border-bottom: 1px solid #000000'><b>T�pico: {$linha["title"]}</b></td><td style='border-bottom: 1px solid #000000' align='right'>$quantidade</td><td align='right' style='border-bottom: 1px solid #000000'>$quantidadeRespostas</td></tr>";	
				}
				
				$forumAnterior = $linha["id"];
			}				
*/		
			relatorio_forum::$quantidadeRespostasTema = 0;
			$usuariosRespostas = listarUsuarioRespostas($linha["id"],null, $respostasTema);
			$quantidadeUsuarios = count($usuariosRespostas);
			
			$respostasTema = relatorio_forum::$quantidadeRespostasTema;
			
			$lotacoesTema = array();
			
			foreach ($usuariosRespostas as $usuarioRespostaLinha){
				
				$existeLotacaoTema = false;
				
				foreach ($lotacoesTema as $lotacaoTema){
					
					if ($lotacaoTema == $usuarioRespostaLinha["DS_LOTACAO"]){
						$existeLotacaoTema = true;
						break;
					}
				}
				
				if (!$existeLotacaoTema) {
					$lotacoesTema[] = $usuarioRespostaLinha["DS_LOTACAO"];
				}
				
			}
			
			sort($lotacoesTema);

			$lotacoesUsuario = 0;
			$lotacoesUsuarios = array();
			$lotacoesContribuicao = 0;
			$lotacoesContribuicoes = array();
			$usuariosLotacao = array();
			
			$listaUsuarios = array();
			
			$totalUsuariosTopico = 0;
			
			foreach ($lotacoesTema as $lotacaoTemaImprimir){
				$lotacoesContribuicao = 0;
				$lotacoesUsuario = 0;
				foreach ($usuariosRespostas as $usuarioRespostaLinha){
					
					if ($lotacaoTemaImprimir == $usuarioRespostaLinha["DS_LOTACAO"]){
						$jaContou = false;
						foreach ($usuariosLotacao as $usuario){
							if ($usuario == $usuarioRespostaLinha["CD_USUARIO"]){
								$jaContou = true;
								break;
							}
								
						}
							
						if(!$jaContou){
							$lotacoesUsuario++;
							$usuariosLotacao[] = $usuarioRespostaLinha["CD_USUARIO"];
							$listaUsuarios[$usuarioRespostaLinha["NM_USUARIO"].$usuarioRespostaLinha["NM_SOBRENOME"]] = $usuarioRespostaLinha;
						}
						
						$lotacoesContribuicao++;
						
					}
					
				}

				$lotacoesContribuicoes[] = $lotacoesContribuicao;
				$lotacoesUsuarios[] = $lotacoesUsuario;
				$totalUsuariosTopico = $totalUsuariosTopico + $lotacoesUsuario;
				
			}
			
		
			if ($forumAnterior != $linha["id"]){
				$quantidade = $foruns[$qtdForum];
				$quantidadeRespostas = $respostasForuns[$qtdForum];
				$quantidadeUsuariosForum = count($usuariosForuns[$qtdForum]);
				$qtdForum++;
				if ($quantidadeRespostas > 0) {
					
					if ($forumAnterior > 0) {
						echo "<tr><td colspan='5'>&nbsp;</td></tr>";	
					}
					
					$existeRelatorio = true;
					echo "<tr class='textblk'><td colspan='3' style='border-bottom: 1px solid #000000'><b>T�pico: {$linha["title"]}</b></td><td style='border-bottom: 1px solid #000000' align='right'>$totalUsuariosTopico</td><td align='right' style='border-bottom: 1px solid #000000'>$quantidadeRespostas</td></tr>";	
				}
				
				$forumAnterior = $linha["id"];
			}	
						
			$x = 0;
			
			ksort($listaUsuarios);
			$teste = count($listaUsuarios);
			
			foreach ($lotacoesTema as $lotacaoTemaImprimir){
				echo "<tr height='40' class='textblk'>
						<td colspan='3' class='textblk'><b>Lota��o: $lotacaoTemaImprimir</b></td>
							<td align='right'>{$lotacoesUsuarios[$x]}</td>
							<td align='right'>{$lotacoesContribuicoes[$x]}</td>
						</tr>";
								
				$usuariosImpressos = array();
				

				foreach ($listaUsuarios as $usuarioRespostaLinha){

					$usuariosQuantidades = array();
					$usuariosQuantidade = 0;
					$usuarioAnterior = 0;
					
					if ($lotacaoTemaImprimir == $usuarioRespostaLinha["DS_LOTACAO"]){
						
						foreach ($usuariosRespostas as $usuarioTotal){

							if ($usuarioTotal["CD_USUARIO"] == $usuarioRespostaLinha["CD_USUARIO"]) {
								$usuariosQuantidade++;
							}
						}
						

									echo "<tr class='textblk'>
										<td colspan='2'></td>
										<td colspan='2'>{$usuarioRespostaLinha["login"]} / {$usuarioRespostaLinha["NM_USUARIO"]} {$usuarioRespostaLinha["NM_SOBRENOME"]}</td>
										<td align='right'>$usuariosQuantidade</td>
										</tr>";
						
					}
				
				}				
				
				$x++;
				
			}
			



		}
		
		if (!$existeRelatorio) {
			echo '<script>alert("N�o h� dados para exibi��o do relat�rio.");self.close();</script>';
			exit();
		}
	
	?>
		
		<tr><td colspan="5" style="border-bottom: 1px solid #000000">&nbsp;</td></tr>
	</table>
	
	<?php
		if ($_GET["excel"] != 1) {
	?>
	
	<div style="width:100%;text-align:center">
		<br />
		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
	</div>
	<?php
		}
	?>
	
</body>
</html>

<?php
}

function listarUsuarioRespostas($codigoItemForum, $usuarioRespostas, $respostasTema){
	
	$sqlData = relatorio_forum::$sqlData;
	
	if (!is_array($usuarioRespostas)){
		$usuarioRespostas = array();
		
		$sql = "SELECT
				f.id, u.CD_USUARIO, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO, u.login
			FROM
				tb_forum f
				INNER JOIN col_usuario u ON f.name = u.login
				INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				INNER JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
			WHERE
				f.id = $codigoItemForum
				"; //$sqlData
	
		$resultadoUsuario = DaoEngine::getInstance()->executeQuery($sql,true);
		
		$linha = mysql_fetch_array($resultadoUsuario);
		
		$usuarioRespostas[] = $linha;
		
	}
	
	$sql = "SELECT
				f.id, u.CD_USUARIO, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO, u.login
			FROM
				tb_forum f
				INNER JOIN col_usuario u ON f.name = u.login
				INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
				INNER JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
			WHERE
				f.parent_id = $codigoItemForum
				AND e.IN_ATIVO = 1
				$sqlData";
	
	$resultadoUsuario = DaoEngine::getInstance()->executeQuery($sql,true);
	
	while ($linha = mysql_fetch_array($resultadoUsuario)) {
		relatorio_forum::$quantidadeRespostasTema++;
		//$jaExiste = false;
	
		//foreach ($usuarioRespostas as $linhaGravada){
		//	if ($linhaGravada["CD_USUARIO"] == $linha["CD_USUARIO"]){
		//		$jaExiste = true;
		//		break;
		//	}
		//}
	
		//if (!$jaExiste){
			$usuarioRespostas[] = $linha;
		//}
		
		$usuarioRespostas = listarUsuarioRespostas($linha["id"], $usuarioRespostas, $respostasTema);
		
	}
	
	return $usuarioRespostas;
	
}

?>