<?php
//Includes
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include ('relatorio_ranking_util.php');
include('relatorio_acesso_util.php');

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

if ($_POST["btnExcel"] != "") {

	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=RadarAssessment.xls");

}

//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$tituloTotal = "";
$tituloRelatorio = "Ranking";

$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];
$codigoFiltro = 0;
$POST = obterPost($codigoFiltro);
$classificacaoRelatorio = $_REQUEST["tipo"];

if ($POST["nomeHierarquia"] == "") {
	$POST["nomeHierarquia"] = "Grupo de Gest�o";
}


$nomeLotacao = "{$POST["nomeHierarquia"]}: {$POST["nomeLotacao"]}";
if ($POST["nomeLotacao"] == "")
{
	$nomeLotacao = "&nbsp;";
}

$codigoLotacaoUsuario = $_REQUEST["hdnLotacao"];

$codigoEmpresa = $_SESSION["empresaID"];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$cargo = joinArray($POST['cboCargo'],"','");
$codigoUsuario = (isset($_REQUEST["cboUsuario"])?$_REQUEST["cboUsuario"]:-1);
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$codigoCiclo = $_REQUEST["hdnCiclo"];
$nomeUsuario = null;

if (isset($_GET["cboUsuario"]) || ($_SESSION["tipo"] >=4 || $_SESSION["tipo"] == -1 || $_SESSION["tipo"] == -2 || $_SESSION["tipo"] == -3))
{
	
}
else
{
	$codigoUsuario = $_SESSION["cd_usu"];
}

if ($codigoUsuario > -1) {
	$sql = "SELECT u.lotacao, u.NM_USUARIO, l.DS_LOTACAO FROM col_usuario u LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao WHERE CD_USUARIO = $codigoUsuario";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaLotacao = mysql_fetch_array($resultado);
	
	if ($classificacaoRelatorio == "C") {
		$codigoLotacao = $linhaLotacao["lotacao"];
	}
	
	$nomeUsuario = $linhaLotacao["NM_USUARIO"];
	$nomeLotacao = "{$POST["nomeHierarquia"]}: {$linhaLotacao["DS_LOTACAO"]}";
}

if ($_REQUEST["operacao"] ==765)
{
	
	$codigoEmpresa = 5;
	$dataInicial = "";
	$dataFinal = "";
	$cargo = joinArray($POST['cboCargo'],"','");
	$codigoUsuario = joinArray($POST['cboUsuario']);
	$codigoLotacao = joinArray($POST['cboLotacao']);
	$dataInicioTexto = $dataInicial;
	$dataTerminoTexto = $dataFinal;
	$codigoCiclo = -1;
	$codigoFiltroUsuario = 1;
}

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$dataAtual = $dataInicial;

// Fim obter parametros
$campoSelect = "login, NM_USUARIO, NM_SOBRENOME, DS_LOTACAO, cargofuncao";
$tituloCampo = "Participante";
$tituloRelatorio = "Radar de Aprendizado";

if ($classificacaoRelatorio == "C") {
	$tituloRelatorio = "Radar Boletim";
}

//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa
mysql_free_result($resultado);

$nomeCiclo = "Todos os Ciclos";
if ($codigoCiclo != -1)
{
	//Obter o nome do Ciclo
	$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaCiclo = mysql_fetch_array($resultado);
	$nomeCiclo = $linhaCiclo["NM_CICLO"];
	//Fim obter o nome do Ciclo
}


$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

$totalUsuarioLotacao = obterTotalUsuariosGrupamento($codigoEmpresa, -1, $codigoLotacaoUsuario, -1, $dataFinalQuery);


$mediaGeralProvasCalculada = 0;
$quantidadeTotalAvaliacoesMaisSegundaChamada = 0;
obterProvasMediaCiclo();

$dadosAssessment = null;
$mediasAssessment = null;

$dadosAssessment = obterDadosAssessmentUsuarios($codigoEmpresa, $codigoLotacao, $codigoUsuario);
$mediasAssessment = obterMediaHierarquia($codigoEmpresa, $codigoLotacao, $codigoUsuario);

//$dadosAssessment = obterDadosAssessmentEmpresa($codigoLotacaoUsuario);


function tratarDividirZero($valor)
{
	if ($valor == 0)
	{
		return 1;
	}
	else 
	{
		return $valor;
	}
}

$totalQuestoes = 0;

$grupos = obterGruposDisciplinaAssessmentEmpresa($codigoEmpresa);
//$grupos[1]["nome"] = "Produtos e Servi�os";
//$grupos[2]["nome"] = "Servi�os VAS";
//$grupos[3]["nome"] = "Intelig�ncia Competitiva";
//$grupos[4]["nome"] = "Processos";
//$grupos[5]["nome"] = "TIM Evolu��o Tecnol�gica";

foreach ($grupos as $codigoGrupo => $nomeGrupo) {
	
	$grupos[$codigoGrupo]["total"] = 0;
	$grupos[$codigoGrupo]["certas"] = 0;
	$grupos[$codigoGrupo]["itens"] = array();
	$grupos[$codigoGrupo]["perguntas"] = 0;

}

//for($i = 1; $i < 5; $i++)
//{
//	$grupos[$i]["total"] = 0;
//	$grupos[$i]["certas"] = 0;
//	$grupos[$i]["itens"] = array();
//	$grupos[$i]["perguntas"] = 0;
//}


$usuarios = array();
while($linhaAssessment = mysql_fetch_array($dadosAssessment))
{
	$codigoGrupo = $linhaAssessment["CD_GRUPO_DISCIPLINA"];
	$codigoParticipante = $linhaAssessment["CD_USUARIO"];
	if ($codigoParticipante == "") $codigoParticipante = -1;
	
	if ($usuarios[$codigoParticipante] == null){
		$usuarios[$codigoParticipante] = array();
		$usuarios[$codigoParticipante]["grupos"] = $grupos;
	}
	
	$usuarios[$codigoParticipante]["login"] = $linhaAssessment["login"];
	$usuarios[$codigoParticipante]["NM_USUARIO"] = $linhaAssessment["NM_USUARIO"];
	
	
	$usuarios[$codigoParticipante]["grupos"][$codigoGrupo]["total"] = $usuarios[$codigoParticipante]["grupos"][$codigoGrupo]["total"] + $linhaAssessment["QD_TOTAL"];
	$usuarios[$codigoParticipante]["grupos"][$codigoGrupo]["certas"] = $usuarios[$codigoParticipante]["grupos"][$codigoGrupo]["certas"] + $linhaAssessment["QD_CERTAS"];
	
	//$grupos[$codigoGrupo]["perguntas"] = $grupos[$codigoGrupo]["perguntas"] + $linhaAssessment["NR_QTD_PERGUNTA_TOTAL"];
	$usuarios[$codigoParticipante]["grupos"][$codigoGrupo]["itens"][] = $linhaAssessment;
	//$usuarios[$codigoParticipante]["grupos"] = $grupos;
	
	//$totalQuestoes = $totalQuestoes + $linhaAssessment["NR_QTD_PERGUNTA_TOTAL"];
}




?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	
	<?php

	

	if ($_POST["btnExcel"] == "") {



	?>

	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">		

	<?php

	}

	?>
	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		
		.tabelaRanking td
		{
			padding: 1px 3px 1px 3px;
		}
	</style>
	
</head>
<body style="overflow-x: scroll; overflow:scroll" >
<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk' nowrap>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr class="textblk">
		<td>
			<b>Ciclo: <?php echo $nomeCiclo; ?></b>
		</td>
	</tr>
	<tr class="textblk">
		<td width="100%">
			<b>Per�odo:&nbsp;
							<?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
			</b>
		</td>
	</tr>
	<tr class="textblk">
	<td class="textblk">
		<b><?php echo $nomeLotacao; ?></b>
	</td>
	</tr>
	
	<?php 
	if ($codigoUsuario > -1) {
	?>
	<tr class="textblk">
		<td class="textblk">
			<b>Nome: <?php echo $nomeUsuario; ?></b>
		</td>
	</tr>
	<?php 
	}
	?>
	
	
</table>


<table cellpadding="1" cellspacing="0" align="center" width="95%" border="1">

	<?php

		$linhaSelecionada = null;

		foreach ($linhas as $linha)
		{
			if ($linha["FILTRADO"] == 0)
				continue;

			//Aqui vinha a tabela de informacoes que passou para baixo	
			$linhaSelecionada = $linha;
			
			//Forcao nao aparecer o que nao deve
			$exibirParticipacao = 2;
			$exibirParticipacaoForum = 2;
	
		}
	
		
		function formatarValores($valor, $decimal = 2, $seVazio = "-")
		{
			if ($valor == "")
			{
				return $seVazio;
			}
			
			return number_format(round($valor,$decimal), $decimal, ",", "");
			
		}
		
		function aplicarNaoRanqueado($linha, $campoRanking, $campoFator)
		{
			$campoFator = $campoFator . "_ACESSO";
			
			if (!$linha[$campoFator])
				return "NR";
				
			return $linha[$campoRanking];
			
		}
		
		
		
	?>

<?php
	$linha = $linhaSelecionada;
?>
	

<?php 
	$totalGeral = 0;
	$totalCertaGeral = 0;
	$totalPerguntasGeral = 0;
	$colunas = array();
	
	foreach($usuarios as $usuario)
	{
		?>
		<tr class="textblk">
		<?php 
		if ($usuario["login"] == ""){ //Se � o cabe�alho
			
		?>
		
	
		<td>Participantes</td>
		
		<?php 
		foreach($usuario["grupos"] as $grupo){
			 $colunas[] = $grupo["nome"] . "G";
		?>
			<td align="center"><b><?php echo $grupo["nome"];?></b></td>
			
		<?php
			foreach($grupo["itens"] as $linhaAssessment){
				$colunas[] = $linhaAssessment["DS_DISCIPLINA"] . "D";
		?>
				<td align="center"><?php echo $linhaAssessment["DS_DISCIPLINA"];?></td>
		<?php 
			}
		 }?>
	
		
		<?php 
		}else {	//Se n�o � o cabe�alho

			echo "<td nowrap>{$usuario["NM_USUARIO"]} - {$usuario["login"]}</td>";
			$i = 0;
			foreach($usuario["grupos"] as $grupo){
				$totalGeral = $totalGeral + $grupo["total"];
				$totalCertaGeral = $totalCertaGeral + $grupo["certas"];
				$percentualGrupo = $grupo["certas"] / $grupo["total"] * 100;
				$totalPerguntas = $grupo["perguntas"];
				$totalPerguntasGeral = $totalPerguntasGeral + $totalPerguntas;
				escreveLinha($percentualGrupo, $i, $grupo["nome"] . "G", $colunas);
				$i++;
				foreach($grupo["itens"] as $linhaAssessment){
					$percentualAssesment = $linhaAssessment["QD_CERTAS"] / $linhaAssessment["QD_TOTAL"] * 100;
					escreveLinha($percentualAssesment, $i, $linhaAssessment["DS_DISCIPLINA"] . "D", $colunas);
					$i++;
				}
				
			}
			while ($i < count($colunas)){
				echo "<td align=\"center\">0,00</td>";
				$i++;
			}
				
		}
		
		?>
		</tr>
		<?php 
	}
	
	function escreveLinha($valorEscrito, &$codigoLinha, $nomeProcurado,&$colunas){
		while($colunas[$codigoLinha] != $nomeProcurado){
		?>
			<td align="center">0,00</td>
		<?php 
			$codigoLinha++;
		}
	?>
		<td align="center"><?php echo formatarValores($valorEscrito,2,"0,00"); ?></td>
	<?php 
	}
	
	?>
</table>
<div>&nbsp;</div>
	<?php

		if ($_POST["btnExcel"] == "") {

	?>
<div style="width:100%;text-align:center">
	<form method="POST" action="<?php echo $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']; ?>">
		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">
		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
	</form>
</div>	
	<?php

		}

	?>
</body>
</html>