<?php
//Includes
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include ('relatorio_ranking_util.php');

function gerarRadarParticipante($codEmpresa, $codCiclo, $codUsuario)
{
	global $codigoEmpresa, $codigoCiclo, $codigoUsuarioIndividual,$quantidadeCadastradosEmpresa;
	$codigoEmpresa = $codEmpresa;
	$codigoCiclo = $codCiclo;
	$codigoUsuarioIndividual = $codUsuario;
	
	if (isset($codigoEmpresa) && $codigoEmpresa == 34)
	{
		$labelAvaliacao = "Certifica&ccedil;&otilde;es";
	}
	else
	{
		$labelAvaliacao = "Avalia&ccedil;&otilde;es";
	}
	
	$tituloTotal = "";
	$tituloRelatorio = "Ranking";
	
	$codigoFiltro = 0;
	
	//Obter Informa��es do Grupo do Usu�rio
	$sql = "SELECT
			f.CD_FILTRO, f.NM_FILTRO, u.lotacao, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO
	FROM
			col_usuario u
			LEFT JOIN col_filtro_lotacao fl ON fl.CD_LOTACAO = u.lotacao
			LEFT JOIN col_filtro f ON f.CD_FILTRO = fl.CD_FILTRO
			LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
	WHERE
			u.CD_USUARIO = $codigoUsuarioIndividual
	AND  	f.IN_ATIVO = 1
	AND 	f.IN_FILTRO_RANKING = 1";
	
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	
	if (mysql_num_rows($resultado) == 0)
	{
		$sql = "SELECT
			f.CD_FILTRO, f.NM_FILTRO, u.lotacao, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO
	FROM
			col_usuario u
			LEFT JOIN col_filtro_lotacao fl ON fl.CD_LOTACAO = u.lotacao
			LEFT JOIN col_filtro f ON f.CD_FILTRO = fl.CD_FILTRO
			LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
	WHERE
			u.CD_USUARIO = $codigoUsuarioIndividual
	ORDER BY f.IN_ATIVO = 1 DESC, f.IN_FILTRO_RANKING DESC";
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		
	}
	
	$linha = mysql_fetch_row($resultado);
	
	$codigoFiltroUsuario = $linha[0];
	$nomeFiltro = $linha[1];
	$codigoLotacaoUsuario = $linha[2];
	$nomeUsuario = "{$linha[3]} {$linha[4]}";
	$nomeLotacao = $linha[5];
	
	mysql_free_result($resultado);
	
	$complementoTitulo = "";
	
	$siglaTotalizadorRanking = 'RT';
	$descricaoTotalizadorRanking = 'Ranking Total';
	
	$codigoFiltro = $codigoFiltroUsuario;
	$complementoTitulo = "do Grupo: $nomeFiltro";
	$siglaTotalizadorRanking = 'RG';
	$descricaoTotalizadorRanking = 'Ranking no Grupo';
	
	
	$POST = obterPost($codigoFiltro);
	//Obter Parametros
	
//	$dataInicial = $POST["txtDe"];
//	$dataFinal = $POST["txtAte"];
//	$cargo = joinArray($POST['cboCargo'],"','");
	$codigoUsuario = $codigoUsuarioIndividual;
//	$codigoLotacao = joinArray($POST['cboLotacao']);
//	$dataInicioTexto = $dataInicial;
//	$dataTerminoTexto = $dataFinal;
	
	
//	if ($_REQUEST["operacao"] ==765)
//	{
//		
//		$codigoEmpresa = 5;
//		$dataInicial = "";
//		$dataFinal = "";
//		$cargo = joinArray($POST['cboCargo'],"','");
//		$codigoUsuario = $codigoUsuarioIndividual;
//		$codigoLotacao = joinArray($POST['cboLotacao']);
//		$dataInicioTexto = $dataInicial;
//		$dataTerminoTexto = $dataFinal;
//		$codigoCiclo = -1;
//		$codigoFiltroUsuario = 1;
//	}
	
	$usaData = true;
	if ($dataInicial == "" && $dataFinal == ""){
		$usaData = false;
	}
	
	if ($dataInicial==""){
		$dataInicial = '01/01/2008';
	}
	if ($dataFinal=="") {
		$dataFinal = date("d/m/Y");
	}
	
	$dataAtual = $dataInicial;
	
	// Fim obter parametros
	$campoSelect = "login, NM_USUARIO, NM_SOBRENOME, DS_LOTACAO, cargofuncao";
	$tituloCampo = "Participante";
	$tituloRelatorio = "Radar do Participante";
	
	
	//Obter o nome da empresa
	$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaEmpresa = mysql_fetch_array($resultado);
	$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
	//Fim obter nome da empresa
	mysql_free_result($resultado);
	
	$nomeCiclo = "Todos os Ciclos";
	if ($codigoCiclo != -1)
	{
		//Obter o nome do Ciclo
		$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		$linhaCiclo = mysql_fetch_array($resultado);
		$nomeCiclo = $linhaCiclo["NM_CICLO"];
		//Fim obter o nome do Ciclo
	}
	
	
	$dataInicialQuery = conveterDataParaYMD($dataInicial);
	$dataFinalQuery = conveterDataParaYMD($dataFinal);
	
	$totalUsuariosEmpresa = obterTotalUsuariosGrupamento($codigoEmpresa, -1, -1, -1, $dataFinalQuery);
	$quantidadeCadastradosEmpresa = $totalUsuariosEmpresa;
	$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataFinalQuery);
	$totalUsuarioLotacao = obterTotalUsuariosGrupamento($codigoEmpresa, -1, $codigoLotacaoUsuario, -1, $dataFinalQuery);
	
	
	$quantidadeCiclosAvaliacoes = 1;
	if ($codigoCiclo == -1)
		$quantidadeCiclosAvaliacoes = obterQuantidadeCiclosAvaliacoes();
	
	$fatoresEmpresa = obterFatoresEmpresa($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $quantidadeCiclosAvaliacoes);
	//Fatores da Empresa
	$fatorEmpresaMediaAvaliacaoOriginal = $fatoresEmpresa["VALOR_MEDIA_AVALIACAO"] * 10;
	$fatorEmpresaMediaAvaliacao = tratarDividirZero($fatoresEmpresa["VALOR_MEDIA_AVALIACAO"] * 10);
	
	$fatorEmpresaParticipacaoAvaliacaoOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_AVALIACAO"];
	$fatorEmpresaParticipacaoAvaliacao = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO_AVALIACAO"]);
	
	$fatorEmpresaAcessoOriginal = $fatoresEmpresa["VALOR_ACESSO"];
	$fatorEmpresaAcesso = tratarDividirZero($fatoresEmpresa["VALOR_ACESSO"]);
	
	$fatorEmpresaDownloadOriginal = $fatoresEmpresa["VALOR_DOWNLOAD"];
	$fatorEmpresaDownload = tratarDividirZero($fatoresEmpresa["VALOR_DOWNLOAD"]);
	
	$fatorEmpresaParticipacaoSimuladoOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_SIMULADO"];
	$fatorEmpresaParticipacaoSimulado = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO_SIMULADO"]);
	
	$fatorEmpresaRealizacaoSimuladoOriginal = $fatoresEmpresa["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
	$fatorEmpresaRealizacaoSimulado = tratarDividirZero($fatoresEmpresa["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);
	
	
	$fatorEmpresaParticipacaoOriginal = $fatoresEmpresa["FATOR_PARTICIPACAO"];
	$fatorEmpresaParticipacao = tratarDividirZero($fatoresEmpresa["FATOR_PARTICIPACAO"]);
	
	$fatorEmpresaParticipacaoForumOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_FORUM"];
	$fatorEmpresaParticipacaoForum = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO_FORUM"]);
	
	$fatorEmpresaContribuicaoForumOriginal = $fatoresEmpresa["VALOR_CONTRIBUICAO_FORUM"];
	$fatorEmpresaContribuicaoForum = tratarDividirZero($fatoresEmpresa["VALOR_CONTRIBUICAO_FORUM"]);
	
	$fatorEmpresaMediaSimuladoOriginal = 0;
	$fatorEmpresaMediaSimulado = tratarDividirZero(0);
	
	$fatorParticipacaoEmpresa = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO"]);
	
	
	////Fatores do Grupo
	if ($codigoEmpresa != 34)
	{
	$fatoresGrupo = obterFatoresGrupo($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoFiltroUsuario, $quantidadeCiclosAvaliacoes);
	$fatorGrupoMediaAvaliacaoOriginal = $fatoresGrupo["VALOR_MEDIA_AVALIACAO"] * 10;
	$fatorGrupoMediaAvaliacao = tratarDividirZero($fatoresGrupo["VALOR_MEDIA_AVALIACAO"] * 10);
	
	$fatorGrupoParticipacaoAvaliacaoOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_AVALIACAO"];
	$fatorGrupoParticipacaoAvaliacao = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_AVALIACAO"]);
	
	$fatorGrupoAcessoOriginal = $fatoresGrupo["VALOR_ACESSO"];
	$fatorGrupoAcesso = tratarDividirZero($fatoresGrupo["VALOR_ACESSO"]);
	
	$fatorGrupoDownloadOriginal = $fatoresGrupo["VALOR_DOWNLOAD"];
	$fatorGrupoDownload = tratarDividirZero($fatoresGrupo["VALOR_DOWNLOAD"]);
	
	$fatorGrupoParticipacaoSimuladoOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_SIMULADO"];
	$fatorGrupoParticipacaoSimulado = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_SIMULADO"]);
	
	$fatorGrupoRealizacaoSimuladoOriginal = $fatoresGrupo["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
	$fatorGrupoRealizacaoSimulado = tratarDividirZero($fatoresGrupo["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);
	
	$fatorGrupoParticipacaoForumOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_FORUM"];
	$fatorGrupoParticipacaoForum = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_FORUM"]);
	
	$fatorGrupoContribuicaoForumOriginal = $fatoresGrupo["VALOR_CONTRIBUICAO_FORUM"];
	$fatorGrupoContribuicaoForum = tratarDividirZero($fatoresGrupo["VALOR_CONTRIBUICAO_FORUM"]);
	
	$fatorParticipacaoGrupo = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO"]);
	}
	//Fim fatores do grupo
	
	//Fatores da Lota��o
	$fatoresLotacao = obterFatoresLotacao($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoLotacaoUsuario, $quantidadeCiclosAvaliacoes);
			
	$fatorLotacaoMediaAvaliacaoOriginal = $fatoresLotacao["VALOR_MEDIA_AVALIACAO"] * 10;
	$fatorLotacaoMediaAvaliacao = tratarDividirZero($fatoresLotacao["VALOR_MEDIA_AVALIACAO"] * 10);
	
	$fatorLotacaoParticipacaoAvaliacaoOriginal = $fatoresLotacao["VALOR_PARTICIPACAO_AVALIACAO"];
	$fatorLotacaoParticipacaoAvaliacao = tratarDividirZero($fatoresLotacao["VALOR_PARTICIPACAO_AVALIACAO"]);
	
	$fatorLotacaoAcessoOriginal = $fatoresLotacao["VALOR_ACESSO"];
	$fatorLotacaoAcesso = tratarDividirZero($fatoresLotacao["VALOR_ACESSO"]);
	
	$fatorLotacaoDownloadOriginal = $fatoresLotacao["VALOR_DOWNLOAD"];
	$fatorLotacaoDownload = tratarDividirZero($fatoresLotacao["VALOR_DOWNLOAD"]);
	
	$fatorLotacaoParticipacaoSimuladoOriginal = $fatoresLotacao["VALOR_PARTICIPACAO_SIMULADO"];
	$fatorLotacaoParticipacaoSimulado = tratarDividirZero($fatoresLotacao["VALOR_PARTICIPACAO_SIMULADO"]);
	
	$fatorLotacaoRealizacaoSimuladoOriginal = $fatoresLotacao["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
	$fatorLotacaoRealizacaoSimulado = tratarDividirZero($fatoresLotacao["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);
	
	$fatorLotacaoParticipacaoForumOriginal = $fatoresLotacao["VALOR_PARTICIPACAO_FORUM"];
	$fatorLotacaoParticipacaoForum = tratarDividirZero($fatoresLotacao["VALOR_PARTICIPACAO_FORUM"]);
	
	$fatorLotacaoContribuicaoForumOriginal = $fatoresLotacao["VALOR_CONTRIBUICAO_FORUM"];
	$fatorLotacaoContribuicaoForum = tratarDividirZero($fatoresLotacao["VALOR_CONTRIBUICAO_FORUM"]);
	
	$fatorParticipacaoLotacao = tratarDividirZero($fatoresLotacao["VALOR_PARTICIPACAO"]);
	
	//Fim fatores da Lota��o
	
	
	//Fatores do Usu�rio
	$fatoresParticipante = obterFatoresParticipantes($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, -1, -1 , $codigoUsuarioIndividual, "U", true, true, $quantidadeCiclosAvaliacoes);
	$fatoresParticipante = mysql_fetch_array($fatoresParticipante);
			
	$fatorParticipanteMediaAvaliacaoOriginal = $fatoresParticipante["VALOR_MEDIA_AVALIACAO"] * 10;
	$fatorParticipanteMediaAvaliacao = tratarDividirZero($fatoresParticipante["VALOR_MEDIA_AVALIACAO"] * 10);
	
	$fatorParticipanteParticipacaoAvaliacaoOriginal = $fatoresParticipante["VALOR_PARTICIPACAO_AVALIACAO"];
	$fatorParticipanteParticipacaoAvaliacao = tratarDividirZero($fatoresParticipante["VALOR_PARTICIPACAO_AVALIACAO"]);
	
	$fatorParticipanteAcessoOriginal = $fatoresParticipante["VALOR_ACESSO"];
	$fatorParticipanteAcesso = tratarDividirZero($fatoresParticipante["VALOR_ACESSO"]);
	
	$fatorParticipanteDownloadOriginal = $fatoresParticipante["VALOR_DOWNLOAD"];
	$fatorParticipanteDownload = tratarDividirZero($fatoresParticipante["VALOR_DOWNLOAD"]);
	
	$fatorParticipanteParticipacaoSimuladoOriginal = $fatoresParticipante["VALOR_PARTICIPACAO_SIMULADO"];
	$fatorParticipanteParticipacaoSimulado = tratarDividirZero($fatoresParticipante["VALOR_PARTICIPACAO_SIMULADO"]);
	
	$fatorParticipanteRealizacaoSimuladoOriginal = $fatoresParticipante["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
	$fatorParticipanteRealizacaoSimulado = tratarDividirZero($fatoresParticipante["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);
	
	$fatorParticipanteParticipacaoForumOriginal = $fatoresParticipante["VALOR_PARTICIPACAO_FORUM"];
	$fatorParticipanteParticipacaoForum = tratarDividirZero($fatoresParticipante["VALOR_PARTICIPACAO_FORUM"]);
	
	$fatorParticipanteContribuicaoForumOriginal = $fatoresParticipante["VALOR_CONTRIBUICAO_FORUM"];
	$fatorParticipanteContribuicaoForum = tratarDividirZero($fatoresParticipante["VALOR_CONTRIBUICAO_FORUM"]);
	
	$fatorParticipacaoParticipante = tratarDividirZero($fatoresParticipante["VALOR_PARTICIPACAO"]);
	
	//*************************************** Fim Altera��o
	
	$linhaSelecionada = null;
	global $linhas;
	foreach ($linhas as $linha)
	{
		if ($linha["FILTRADO"] == 0)
			continue;

		//Aqui vinha a tabela de informacoes que passou para baixo	
		$linhaSelecionada = $linha;
		
		//Forcao nao aparecer o que nao deve
		$exibirParticipacao = 2;
		$exibirParticipacaoForum = 2;

	}
		
	$linha = $linhaSelecionada;

	
	$htmlGerado = '
		<table cellpadding="1" cellspacing="0" border="0" style="margin-left: 2.5%; margin-right: 2.5%;">
		<tr class="textblk" >
			<td width="155" style="border-bottom: 1px solid #000;"><b>Indicadores</b></td>
			<td width="128" align="center" style="border-bottom: 1px solid #000;"><b>Perfil Empresarial</b></td> ';
			if ($codigoEmpresa != 34)
			{
				$htmlGerado .= '<td width="115" align="center" style="border-bottom: 1px solid #000;"><b>Perfil Grupo</b></td>';
			}
			$htmlGerado .= '<td width="115" align="center" style="border-bottom: 1px solid #000;"><b>Perfil Lota&ccedil;&atilde;o</b></td>
			<td width="150" align="center" style="border-bottom: 1px solid #000;"><b>Perfil do <br /> Participante</b></td>
			<td rowspan="14" >&nbsp;</td>
		</tr>';
		global $exibirMediaAvaliacao;
		if($exibirMediaAvaliacao!=2)
		{
		$htmlGerado .= '<tr class="textblk">
			<td>M&eacute;dia das '. $labelAvaliacao.'</td>
			<td align="center">'. formatarValores($fatorEmpresaMediaAvaliacaoOriginal,2).'</td>';
			if ($codigoEmpresa != 34)
			{
				$htmlGerado .= '<td align="center">'. formatarValores($fatorGrupoMediaAvaliacaoOriginal,2).'</td>';
			}
			$htmlGerado .= '<td align="center">'. formatarValores($fatorLotacaoMediaAvaliacaoOriginal,2).'</td>
			<td align="center">'. formatarValores($fatorParticipanteMediaAvaliacaoOriginal,2, 0).'</td>
		</tr>
		
		<tr class="textblk" nowrap style="border-bottom: 1px solid #000;">
			<td style="border-bottom: 1px solid #000;">Quantidade de '. $labelAvaliacao.'</td>
			<td align="center" style="border-bottom: 1px solid #000;">'. formatarValores($fatorEmpresaParticipacaoAvaliacaoOriginal,0, 0).'</td>';
			if ($codigoEmpresa != 34)
			{
				$htmlGerado .= '<td align="center" style="border-bottom: 1px solid #000;">'. formatarValores($fatorGrupoParticipacaoAvaliacaoOriginal,0, 0).'</td>';
			
			}
			$htmlGerado .= '<td align="center" style="border-bottom: 1px solid #000;">'. formatarValores($fatorLotacaoParticipacaoAvaliacaoOriginal,0, 0).'</td>
			<td align="center" style="border-bottom: 1px solid #000;">'. formatarValores($fatorParticipanteParticipacaoAvaliacaoOriginal,0).'</td>
		</tr>';
		}
		global $exibirAcesso;
		if($exibirAcesso!=2)
		{
		$htmlGerado .= '<tr class="textblk">
			<td>Acessos por Participante</td>
			<td align="center">'. formatarValores($fatorEmpresaAcessoOriginal,2).'</td>';
			if ($codigoEmpresa != 34)
			{
			$htmlGerado .= '<td align="center">'. formatarValores($fatorGrupoAcessoOriginal,2).'</td>';
			
			}
			$htmlGerado .= '<td align="center">'. formatarValores($fatorLotacaoAcessoOriginal,2).'</td>
			<td align="center">'. formatarValores($fatorParticipanteAcessoOriginal,0).'</td>
		</tr>';
		}
		global $exibirDownload;
		if($exibirDownload!=2)
		{
		$htmlGerado .= '<tr class="textblk">
			<td>Downloads por Participante</td>
			<td align="center">'. formatarValores($fatorEmpresaDownloadOriginal,2).'</td>';
			if ($codigoEmpresa != 34)
			{
				$htmlGerado .= '<td align="center">'. formatarValores($fatorGrupoDownloadOriginal,2).'</td>';
			
			}
			$htmlGerado .= '<td align="center">'. formatarValores($fatorLotacaoDownloadOriginal,2).'</td>
			<td align="center">'. formatarValores($fatorParticipanteDownloadOriginal,0).'</td>
		</tr>';
		}

//		if($exibirRealizacaoSimulado!=2 || 1==1){
		$htmlGerado .= '<tr class="textblk">
			<td>Simulados por Participante</td>
			<td align="center">'. formatarValores($fatorEmpresaParticipacaoSimuladoOriginal,2).'</td>';
			if ($codigoEmpresa != 34)
			{
			$htmlGerado .= '<td align="center">'. formatarValores($fatorGrupoParticipacaoSimuladoOriginal,2).'</td>';
			
			}
			$htmlGerado .= '<td align="center">'. formatarValores($fatorLotacaoParticipacaoSimuladoOriginal,2).'</td>
			<td align="center">'. formatarValores($fatorParticipanteParticipacaoSimuladoOriginal,0).'</td>
		</tr>
		<tr class="textblk">
			<td>Simulados com Aprova&ccedil;&atilde;o</td>
			<td align="center">'. formatarValores($fatorEmpresaRealizacaoSimuladoOriginal, 2).'</td>';
			if ($codigoEmpresa != 34)
			{
				$htmlGerado .= '<td align="center">'. formatarValores($fatorGrupoRealizacaoSimuladoOriginal, 2).'</td>';
			
			}
			$htmlGerado .= '<td align="center">'. formatarValores($fatorLotacaoRealizacaoSimuladoOriginal, 2).'</td>
			<td align="center">'. formatarValores($fatorParticipanteRealizacaoSimuladoOriginal, 0).'</td>
		</tr>';
//		}
		$exibirParticipacaoSimulado = 2;
		if($exibirParticipacaoSimulado!=2)
		{
		$htmlGerado .= '<tr class="textblk">
			<td>Participa&ccedil;&atilde;o em Simulado</td>
			<td align="center">'. formatarValores($fatorEmpresaParticipacaoSimuladoOriginal,2).'</td>';
			if ($codigoEmpresa != 34)
			{
			$htmlGerado .= '<td align="center">'. formatarValores($fatorGrupoParticipacaoSimuladoOriginal,2).'</td>';
			
			}
			
			$htmlGerado .= '<td align="center">'. formatarValores($fatorLotacaoParticipacaoSimuladoOriginal,2).'</td>
			<td align="center">'. formatarValores($fatorParticipanteParticipacaoSimuladoOriginal,0).'</td>
		</tr>';
		}
		$exibirParticipacaoForum = 1;
		if($exibirParticipacaoForum!=2)
		{
		$htmlGerado .= '<tr class="textblk">
			<td>Participa&ccedil;&atilde;o em F&oacute;runs</td>
			<td align="center">'. round($fatorEmpresaParticipacaoForumOriginal,2).'</td>';
			if ($codigoEmpresa != 34)
			{
				$htmlGerado .= '<td align="center">'. round($fatorGrupoParticipacaoForumOriginal,2).'</td>';
			
			}
			$htmlGerado .= '<td align="center">'. round($fatorLotacaoParticipacaoForumOriginal,2).'</td>
			<td align="center">'. round($fatorParticipanteParticipacaoForumOriginal,0).'</td>
		</tr>';
		}
//		if($exibirContribuicaoForum!=2)
//		{
		$htmlGerado .= '<tr class="textblk">
			<td>Contribui&ccedil;&otilde;es em F&oacute;rum</td>
			<td align="center">'. formatarValores($fatorEmpresaContribuicaoForumOriginal,0).'</td>';
			if ($codigoEmpresa != 34)
			{
				$htmlGerado .= '<td align="center">'. formatarValores($fatorGrupoContribuicaoForumOriginal,0).'</td>';
			
			}
			$htmlGerado .= '<td align="center">'. formatarValores($fatorLotacaoContribuicaoForumOriginal,0).'</td>
			<td align="center">'. formatarValores($fatorParticipanteContribuicaoForumOriginal,0).'</td>
		</tr>';
//		}
		$exibir3ultimasLinhas = false;
		if($exibir3ultimasLinhas)
		{
		$htmlGerado .= '<tr class="textblk">
			<td style="border-top: 1px solid #000;">Cadastrados</td>
			<td style="border-top: 1px solid #000;" align="center">'. $totalUsuariosEmpresa.'</td>';
			if ($codigoEmpresa != 34)
			{
			
			$htmlGerado .= '<td style="border-top: 1px solid #000;" align="center">'. $totalUsuarioGrupo.'</td>';
			
			}
			
			$htmlGerado .= '<td style="border-top: 1px solid #000;" align="center">'. $totalUsuarioLotacao.'</td>
			<td style="border-top: 1px solid #000;" align="center"> - </td>
		</tr>
		<tr class="textblk">
			<td>Participantes</td>
			<td align="center">'. $fatorParticipacaoEmpresa.'</td>';
			if ($codigoEmpresa != 34)
			{
			$htmlGerado .= '<td align="center">'. $fatorParticipacaoGrupo.'</td>';
			}
			
			$htmlGerado .= '<td align="center">'. $fatorParticipacaoLotacao.'</td>
			<td align="center"> - </td>
		</tr>
		<tr class="textblk">
			<td>% de Participa&cecdil;&atilde;o</td>
			<td align="center">'. formatarValores($fatorParticipacaoEmpresa / $totalUsuariosEmpresa * 100,2).'%</td>';
			if ($codigoEmpresa != 34)
			{
			$htmlGerado .= '<td align="center">'. str_ireplace(".", ",", round($fatorParticipacaoGrupo / $totalUsuarioGrupo * 100,2)).'%</td>';
			}
			$htmlGerado .= '<td align="center">'. str_ireplace(".", ",", round($fatorParticipacaoLotacao / $totalUsuarioLotacao * 100,2)).'%</td>
			<td align="center">'. formatarValores($fatorParticipacaoParticipante * 100,2).'%</td>
		</tr>';
		}
		$exibirMediaSimulado = 2;
		if($exibirMediaSimulado!=2)
		{
		$htmlGerado .= '<tr class="textblk">
			<td>Nota em Simulado</td>
			<td align="center">'. round($fatorEmpresaMediaSimuladoOriginal,2).'</td>
			<td align="center">'. round($fatorGrupoMediaSimuladoOriginal,2).'</td>
			<td align="center">'. round($linha["MEDIA_SIMULADO_INDIVIDUAL"],2).'</td>
		</tr>';
		}
		$exibirParticipacao = 2;
		if($exibirParticipacao!=2)
		{
			$htmlGerado .= '<tr class="textblk">
				<td>Participa&cecdil;&atilde;o</td>
				<td align="center">'. round($fatorEmpresaParticipacao,2).'</td>
				<td align="center">'. round($fatorGrupoParticipacaoOriginal,2).'</td>
				<td align="center">'. round($linha["FATOR_PARTICIPACAO_INDIVIDUAL"],0).'</td>
			</tr>';
		}
//			$htmlGerado .= '<tr class="textblk"><td>&nbsp;</td></tr>
//			<tr class="textblk"><td>&nbsp;</td></tr>
	$htmlGerado .='</table>';
			
	return $htmlGerado;
}

function formatarValores($valor, $decimal = 2, $seVazio = "-")
{
	if ($valor == "")
	{
		return $seVazio;
	}
	
	return number_format(round($valor,$decimal), $decimal, ",", "");
	
}

function aplicarNaoRanqueado($linha, $campoRanking, $campoFator)
{
	$campoFator = $campoFator . "_ACESSO";
	
	if (!$linha[$campoFator])
		return "NR";
		
	return $linha[$campoRanking];
	
}

function tratarDividirZero($valor)
{
	if ($valor == 0)
	{
		return 1;
	}
	else 
	{
		return $valor;
	}
}

?>