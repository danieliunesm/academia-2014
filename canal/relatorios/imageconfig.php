<?php

include 'phpplot/phplot.php';

function str_replace_once($str_pattern, $str_replacement, $string, $offset = null){ 

        

	if (strpos($string, $str_pattern, $offset) !== false){ 

		$occurrence = strpos($string, $str_pattern, $offset); 

		return substr_replace($string, $str_replacement, strpos($string, $str_pattern, $offset), strlen($str_pattern)); 

	}

        

	return $string; 

} 

$itensColunas = 3;

if (isset($_REQUEST["colunas"])){
    $itensColunas = $_REQUEST["colunas"];
}

$arrayValores = split(";", $_REQUEST["parametro"]);



$example_data = array();

$colunas = 0;

for($i=1; $i < count($arrayValores);$i++){

	$titulo = $arrayValores[$i];

	if (strlen($titulo) < 40){

		$titulo = str_replace_once(" ", "\n", $titulo);

	}else{

		$titulo = str_replace_once(" ", "\n", $titulo, 17);

		$titulo = str_replace_once(" ", "\n", $titulo, 34);

	}


    if ($itensColunas == 1){
        $example_data[] = array($titulo, $arrayValores[$i + 1]);
    }elseif($itensColunas == 2){
        $example_data[] = array($titulo, $arrayValores[$i + 1], $arrayValores[$i + 2]);
    }else{
        $example_data[] = array($titulo, $arrayValores[$i + 1], $arrayValores[$i + 2], $arrayValores[$i + 3]);
    }


	$i++;

    //if ($itensColunas > 1)
    	$i++;

    //if ($itensColunas > 2)
        $i++;

	$colunas++;

}





$arrayTexto = array('Nota do Participante','Nota M�dia do Grupo','Nota M�dia da Empresa');



if (isset($_REQUEST["texto"])){

	$arraySubstituiTexto = split(";", $_REQUEST["texto"]);
    $arrayTexto = null;
    $arrayTexto = array();
    for ($i=0;$i < $itensColunas; $i++){
        $arrayTexto[] = $arraySubstituiTexto[$i];
    }

}



/*

$example_data = array(

	array('Desenvolvimento 

e Implementa��o 

das Arquiteturas

de Canais',30,40),

	array('Portf�lio 

de Produtos',50,''), 

	array('Gest�o de Vendas',70,20),

	array('Sistemas 

de Suporte',80,10),

	array('Trade Marketing',20,40),

	array('Negocia��o',60,40),

	array('Planejamento

Comercial',70,20),

	array('M�dia Geral',70,20)

);

*/

//$example_data = array();



//$example_data[] = array('Desenvolvimento 

//e Implementa��o 

//das Arquiteturas

//de Canais',30,40);

$width = 650;

if(isset($_REQUEST["width"])){
    $width = $_REQUEST["width"];
}

$graph =& new PHPlot($width,450);

$title = "";

if(isset($_REQUEST["title"])){
   $title =  $_REQUEST["title"];
}

$graph->SetTitle($title);										// T�tulo do Gr�fico

$xtitle = "Disciplinas";

if(isset($_REQUEST["xtitle"])){
   $xtitle = $_REQUEST["xtitle"];
}

$graph->SetXTitle($xtitle);											// Label Eixo X

$ytitle = "Notas";

if(isset($_REQUEST["ytitle"])){
    $ytitle = $_REQUEST["ytitle"];
}

$graph->SetYTitle($ytitle);													// Label Eixo Y

$graph->SetLegend($arrayTexto);		// Legenda de cada grupo de colunas

$yMax = 15;
if(isset($_REQUEST["ymax"])){
  $yMax = $_REQUEST["ymax"];
}

$graph->SetPlotAreaWorld(0,0,$colunas,$yMax);										// 0,0, Max grupos de colunas, Max Y

$graph->SetPrintImage(0); 

$graph->SetDataType("text-data"); 

$graph->SetDataValues($example_data);

$graph->SetFileFormat("PNG");

$tickIncrement = 1;
if(isset($_REQUEST["tickincrement"])){
   $tickIncrement = $_REQUEST["tickincrement"];
}

$graph->SetYTickIncrement($tickIncrement);

$graph->SetPlotType("bars");

$graph->SetXLabelAngle(90); 

$graph->SetXTickPos('none');

$graph->SetXTickLabelPos('none');

$graph->SetNewPlotAreaPixels(50,50,$width - 20,300);	// Janela do gr�fico dentro da imagem

$graph->SetBackgroundColor('#ffffff');

$graph->SetLineStyles('solid');

$graph->DrawGraph();

$graph->PrintImage();

?>

 

