<?php

include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');


$POST = obterPost();

$acessoUnico = $_REQUEST["hdnTipoRelatorio"];

$codigoEmpresa = $POST['cboEmpresa'];

$codigoUsuario = joinArray($POST['cboUsuario']);
$cargo = joinArray($POST['cboCargo'],"','");
$codigoLotacao = joinArray($POST['cboLotacao']);

$dataInicio = $POST["txtDe"];
$dataTermino = $POST["txtAte"];
$dataInicioTexto = $dataInicio;
$dataTerminoTexto = $dataTermino;

$nomeEmpresa = 'Todas';
$nomeUsuario = 'Todos';

$sqlData = "";

if ($dataInicio != ""){
	$dataInicio = substr($dataInicio, 6, 4) . substr($dataInicio, 3, 2) . substr($dataInicio, 0, 2);
	
	$sqlData = " $sqlData AND  DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicio' ";
}

if ($dataTermino != ""){
	$dataTermino = substr($dataTermino, 6, 4) . substr($dataTermino, 3, 2) . substr($dataTermino, 0, 2);
		
	$sqlData = " $sqlData AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataTermino' ";
}


$paginaRn = new col_pagina_acesso();
$paginaRn->prepararRelatorios($dataInicio, $dataTermino, $codigoEmpresa);

$tipoAcessoContado = "";
if ($acessoUnico == "H"){
	$tipoAcessoContado = "COUNT(1)";
}
elseif ($acessoUnico == "D")
{
	//$tipoAcessoContado = "COUNT(DISTINCT(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d')))";
	$tipoAcessoContado = "COUNT(DISTINCT(u.CD_USUARIO))";
}
elseif ($acessoUnico == "S")
{
	//$tipoAcessoContado = "COUNT(DISTINCT DS_ID_SESSAO, u.CD_USUARIO)";
	$tipoAcessoContado = "COUNT(DISTINCT DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'), a.CD_USUARIO)";
}




$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];


$sql = "SELECT
  			l.DS_LOTACAO,
  			COUNT(DISTINCT(u.CD_USUARIO)) AS QD_ACESSO,
  			(SELECT COUNT(DISTINCT u1.CD_USUARIO) FROM col_usuario u1 WHERE u1.lotacao = l.CD_LOTACAO) AS TOTAL_CADASTRADO,
  			h.DS_HIERARQUIA
FROM
  col_usuario u
  INNER JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  LEFT JOIN col_acesso a1 ON u.CD_USUARIO = a1.CD_USUARIO
  LEFT JOIN col_pagina_acesso pa ON a1.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
  LEFT JOIN col_hierarquia h ON h.CD_NIVEL_HIERARQUICO = l.CD_NIVEL_HIERARQUICO AND h.CD_EMPRESA = l.CD_EMPRESA
WHERE
	((NOT pa.IN_ORDENAR = -1 AND NOT pa.IN_ORDENAR = 999) OR pa.IN_ORDENAR IS NULL)
AND	u.empresa = $codigoEmpresa
AND (u.Status = 1 AND u.tipo = 1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
AND DATE_FORMAT(u.datacadastro, '%Y%m%d') <= '$dataTermino'
AND u.CD_USUARIO NOT IN
    (SELECT a.CD_USUARIO FROM col_acesso a WHERE 1=1 $sqlData)
GROUP BY
	l.DS_LOTACAO
ORDER BY
	l.DS_LOTACAO"; //QD_ACESSO DESC,


//echo $sql;
$resultadoLotacao = DaoEngine::getInstance()->executeQuery($sql,true);


$sql = "
SELECT
  u.login, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO,
  COUNT(DISTINCT(a1.CD_USUARIO)) AS QD_ACESSO
FROM
  col_usuario u
  INNER JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  LEFT JOIN col_acesso a1 ON u.CD_USUARIO = a1.CD_USUARIO
  LEFT JOIN col_pagina_acesso pa ON a1.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
WHERE
	u.empresa = $codigoEmpresa
AND (u.Status = 1 AND u.tipo = 1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
AND DATE_FORMAT(u.datacadastro, '%Y%m%d') <= '$dataTermino'
AND u.CD_USUARIO NOT IN
    (SELECT a.CD_USUARIO FROM col_acesso a WHERE NOT pa.IN_ORDENAR = -1 AND NOT pa.IN_ORDENAR = 999 $sqlData)
GROUP BY
  u.login, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO
ORDER BY
	u.NM_USUARIO, u.NM_SOBRENOME, u.login"; //QD_ACESSO DESC,

//echo $sql;

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

//if (!mysql_num_rows($resultado) > 0){
//	echo '<script>alert("N�o h� dados para exibi��o do relat�rio.");self.close();</script>';
//	exit();
//}

if ($acessoUnico == "D")
{
	$tipoAcessoContado = "COUNT(DISTINCT(u.CD_USUARIO))";
}

$sql = "
SELECT
  COUNT(DISTINCT(u.CD_USUARIO)) AS QD_ACESSO
FROM
  col_usuario u
  INNER JOIN col_empresa e ON e.CD_EMPRESA = u.empresa
  INNER JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
  LEFT JOIN col_acesso a1 ON u.CD_USUARIO = a1.CD_USUARIO
  LEFT JOIN col_pagina_acesso pa ON a1.CD_PAGINA_ACESSO = pa.CD_PAGINA_ACESSO
WHERE
	u.empresa = $codigoEmpresa
AND (u.Status = 1 AND u.tipo = 1)
AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$codigoLotacao.%')
AND   (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
AND DATE_FORMAT(u.datacadastro, '%Y%m%d') <= '$dataTermino'
AND u.CD_USUARIO NOT IN
    (SELECT a.CD_USUARIO FROM col_acesso a WHERE NOT pa.IN_ORDENAR = -1 AND NOT pa.IN_ORDENAR = 999 $sqlData)
";
//echo $sql;
$resultadoAcessosEmpresa = DaoEngine::getInstance()->executeQuery($sql,true);
$linha = mysql_fetch_row($resultadoAcessosEmpresa);
$totalEmpresa = $linha[0];
$totalParticipanteEmpresa = $linha[0];

if ($_POST["btnExcel"] != "") {
	header("Content-Type: application/vnd.ms-excel; name='excel'");
	header("Content-disposition:  attachment; filename=RelatorioAcessos.xls");
}

$codigoCiclo = $_REQUEST["hdnCiclo"];
$nomeCiclo = obterNomeCiclo($codigoCiclo);

?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Relat�rio de Acessos<?php if ($acessoUnico == "D") echo ' - Quantidade de Acessos de Usu�rios'; elseif ($acessoUnico == "S") echo ' - Quantidade de Sess�es Abertas por Usu�rios'; elseif ($acessoUnico == "H") echo ' - Quantidade de Hits de todas as Sess�es e Usu�rios'; ?> - <?php echo date("d/m/Y") ?></title>
	
	<?php
	
	if ($_POST["btnExcel"] == "") {

	?>
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">	
	<?php
	}
	?>
	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			
			.textoInformativo{
				display: none;
			}
			
			.sumirImpressao{
				display:none;
			}
			
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
		
		.impressao{
			margin-top: -23px;
		}
		
		.sumirImpressao{
			height: 30px;
		}

	</style>
	
</head>

<body>
	<table cellpadding="2" cellspacing="0" border="0" width="95%" align="center" class="impressao">
		<tr style="height: 1px; font: 1px">
			<td class="textblk" width="50%" colspan="2">&nbsp;</td>
			<td width="50%" align="left" class="textblk" colspan="2">
				&nbsp;
			</td>
			<td class="textblk" style="width: 1%">
				&nbsp;
			</td>
		</tr>
		<tr class="sumirImpressao">
			<td class="textblk" width="50%" colspan="2"><b><?php echo $nomeEmpresa;?></b></td>
			<td width="50%" align="left" class="textblk" colspan="2">
				<b>Lista de n�o Participantes</b>
			</td>
			<td class="textblk" style="width: 1%; text-align: right;">
				<?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?>
			</td>
		</tr>
		
<?php

$lotacoes = array();

while ($linha = mysql_fetch_array($resultado)) {
	
	$lotacao = $linha["DS_LOTACAO"];
	
	if ($lotacoes[$lotacao] == null)
	{
		$lotacoes[$lotacao] = array();
	}
	
	$lotacoes[$lotacao][] = $linha;
	
}


?>
			<?
			if ($_REQUEST["id"] != "")
			{
			?>
			
			<tr class="textblk">
				<td colspan="5">
					<b>
					<?php
						if ($_REQUEST["id"] > 0){ 
							echo "Nome do Grupo Gerencial: {$POST["nomeFiltro"]}";
						}elseif ($POST["nomeLotacao"] != "") {
							if ($POST["nomeHierarquia"] == "") $POST["nomeHierarquia"] = "Nome da Lota��o";
							echo "{$POST["nomeHierarquia"]}: {$POST["nomeLotacao"]}";
						}
					?>
					</b>
				</td>
			
			</tr>
			
			<?
			}
			?>

			<tr class="textblk">
				<td colspan="5"><b>Ciclo: <?php echo $nomeCiclo; ?></b></td>
			</tr>


			<?php //<tr class="textblk">
					//<td width="100%" colspan="5"> ?>
			<tr class='textblk' valign='top'><td align='left' style='width:65%' colspan='5'>
											<b>Per�odo:&nbsp; <?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
			echo '</b></td>';
			
			echo "</tr>";
			/*
			if ($_REQUEST["id"] == "")
			{
				echo "<tr class='textblk' valign='top'>";
				echo "<td align='left'>Cadastrados na Empresa: ";
				
				if ($_POST["btnExcel"] == "")
				{
					echo obterTotalUsuarios($codigoEmpresa);
				}
				else{
					echo "</td><td align='center'>";
					echo obterTotalUsuarios($codigoEmpresa);
				}
				
				echo "</td>";
				
				
				echo "</tr>";
			}
			echo "<tr class='textblk' style='height: 25px' valign='top'>";
			
				
			if ($_POST["btnExcel"] == "")
			{
				echo "<td align='left' style='width:65%' colspan='2'>";
				if ($_REQUEST["id"] != "")
				{

					if ($_REQUEST["id"] > 0) {
						echo "Cadastrados no Grupo: ";
					}else {
						echo "Cadastrados na Lota��o: ";
					}
					
					echo obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);
				}
				echo "&nbsp;</td>";
			}
			else 
			{
				if ($_REQUEST["id"] != "")
				{

					$labelCadastro = "Cadastrados na Lota��o";
					if ($_REQUEST["id"] > 0) {
						$labelCadastro = "Cadastrados no Grupo";
					}
					
					echo "<td align='left' style='width:65%'>$labelCadastro: </td><td align='center'>";
					echo obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo);
				}
					echo "<td align='left' style='width:65%' colspan='2'>&nbsp;</td>";			
			}
			*/
			echo "<tr class='textblk' style='height: 25px' valign='top'>";
			echo "<td align='left' style='width:30%'>&nbsp;</td>";
			//echo "<td align='left' style='width:1%'>Qtd&nbsp;de&nbsp;Cadastrados:</td>";
			echo "<td align='left' style='width:1%'>&nbsp;</td>";
			
			
			$login = "Login/";
			if ($codigoEmpresa == 34) $login = "";
			//echo "</td></tr>";
			//echo "<tr class='textblk' style='height: 25px' valign='top'><td align='left' style='width:65%' colspan='2'>&nbsp;</td>";
			echo "<td style='width: 35%' colspan='2' >{$login}Nome Participante</td>
			<td align='right; width:1%'>&nbsp;&nbsp;Qtde&nbsp;de&nbsp;n�o&nbsp;Participantes:</td>
			</tr>";
			echo "<tr class='textblk'><td style='border-top: 1px solid black'><b>Total</b></td>";
			echo "<td style='border-top: 1px solid black' align='right'>";
			//echo obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataTermino);
			echo "&nbsp;";
			echo "</td>";
			echo "<td style='border-top: 1px solid black' colspan='2'>&nbsp;</td>";
			echo "<td align='right' class='textblk' style='border-top: 1px solid black'>$totalParticipanteEmpresa";
			?>

			</td>
		</tr>

<?php

	while ($linha = mysql_fetch_array($resultadoLotacao)) {
		//{$linha["TOTAL_CADASTRADO"]}
		$labelLotacao = "Lota��o";
		if ($linha["DS_HIERARQUIA"] != "")
		{
			$labelLotacao = $linha["DS_HIERARQUIA"];
		}
		
		echo "<tr height='40' class='textblk' >
				<td class='textblk' style='border-top: 1px solid black'><b>$labelLotacao: {$linha["DS_LOTACAO"]}</b></td>
					<td align='right' style='border-top: 1px solid black' >&nbsp;</td>
					<td  align='right'  style='border-top: 1px solid black' colspan='2' >&nbsp;</td>
					<td align='right' width='50%' style='border-top: 1px solid black' >{$linha["QD_ACESSO"]}</td>
				</tr>
			";
			
		$codigoLotacao = $linha["DS_LOTACAO"];

		foreach ($lotacoes[$codigoLotacao] as $item)
		{
			$qdAcesso = "&nbsp;";
		
			if ($acessoUnico != "D")
			{
				$qdAcesso = $item["QD_ACESSO"];
			}
			
			$login = "{$item["login"]} /";
			if ($codigoEmpresa == 34) $login = "";
			
			echo "<tr class='textblk'>
					<td colspan='2'>&nbsp;</td>
					<td colspan='3'>$login {$item["NM_USUARIO"]} {$item["NM_SOBRENOME"]}</td>
					
					</tr>";

		}
		
	}



?>
	<tr><td colspan="5" style="border-bottom: 1px solid #000000">&nbsp;</td></tr>

</table>

	<?php
		if ($_POST["btnExcel"] == "") {
	?>

	<div style="width:100%;text-align:center">
		<br />
		<form method="POST">
		
			<?php
				escreveHiddensPost($POST);
			?>
		<INPUT class="buttonsty" type="submit" value="Excel" id="btnExcel" name="btnExcel">
		<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
		<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
		</form>	
	</div>
	<?php
		}
	?>
	
</body>
</html>