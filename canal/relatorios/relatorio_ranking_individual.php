<?php
//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include ('relatorio_ranking_util.php');

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$tituloTotal = "";
$tituloRelatorio = "Ranking";

$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];
$codigoFiltro = 0;
//S� permite sele��o do usu�rio se for administrador
$codigoUsuarioIndividual = 0;
if (isset($_GET["cboUsuario"]) && ($_SESSION["tipo"] >=4 || $_SESSION["tipo"] == -1 || $_SESSION["tipo"] == -2 || $_SESSION["tipo"] == -3))
{
	$codigoUsuarioIndividual = $_GET["cboUsuario"];
}
else
{
	$codigoUsuarioIndividual = $_SESSION["cd_usu"];
}


//Obter Informa��es do Grupo do Usu�rio
$sql = "SELECT
		f.CD_FILTRO, f.NM_FILTRO, u.lotacao, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO
FROM
		col_usuario u
		LEFT JOIN col_filtro_lotacao fl ON fl.CD_LOTACAO = u.lotacao
		LEFT JOIN col_filtro f ON f.CD_FILTRO = fl.CD_FILTRO
		LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
WHERE
		u.CD_USUARIO = $codigoUsuarioIndividual
AND  	f.IN_ATIVO = 1
AND 	f.IN_FILTRO_RANKING = 1";


$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

if (mysql_num_rows($resultado) == 0)
{
	$sql = "SELECT
		f.CD_FILTRO, f.NM_FILTRO, u.lotacao, u.NM_USUARIO, u.NM_SOBRENOME, l.DS_LOTACAO
FROM
		col_usuario u
		LEFT JOIN col_filtro_lotacao fl ON fl.CD_LOTACAO = u.lotacao
		LEFT JOIN col_filtro f ON f.CD_FILTRO = fl.CD_FILTRO
		LEFT JOIN col_lotacao l ON l.CD_LOTACAO = u.lotacao
WHERE
		u.CD_USUARIO = $codigoUsuarioIndividual
ORDER BY f.IN_ATIVO = 1 DESC, f.IN_FILTRO_RANKING DESC";
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	
}

$linha = mysql_fetch_row($resultado);

$codigoFiltroUsuario = $linha[0];
$nomeFiltro = $linha[1];
$codigoLotacaoUsuario = $linha[2];
$nomeUsuario = "{$linha[3]} {$linha[4]}";
$nomeLotacao = $linha[5];

mysql_free_result($resultado);

$complementoTitulo = "";

$siglaTotalizadorRanking = 'RT';
$descricaoTotalizadorRanking = 'Ranking Total';

switch ($tipoRelatorio)
{
	case "E":
		$complementoTitulo = "da Empresa";
		break;
	case "G":
		$codigoFiltro = $codigoFiltroUsuario;
		$complementoTitulo = "do Grupo: $nomeFiltro";
		$siglaTotalizadorRanking = 'RG';
		$descricaoTotalizadorRanking = 'Ranking no Grupo';
}	


$POST = obterPost($codigoFiltro);
//Obter Parametros

$codigoEmpresa = $_SESSION["empresaID"];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$cargo = joinArray($POST['cboCargo'],"','");
$codigoUsuario = joinArray($POST['cboUsuario']);
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$codigoCiclo = $_REQUEST["hdnCiclo"];


if ($_REQUEST["operacao"] ==765)
{
	
	$codigoEmpresa = 5;
	$dataInicial = "";
	$dataFinal = "";
	$cargo = joinArray($POST['cboCargo'],"','");
	$codigoUsuario = joinArray($POST['cboUsuario']);
	$codigoLotacao = joinArray($POST['cboLotacao']);
	$dataInicioTexto = $dataInicial;
	$dataTerminoTexto = $dataFinal;
	$codigoCiclo = -1;
	$codigoFiltroUsuario = 1;
}

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$dataAtual = $dataInicial;

// Fim obter parametros
$campoSelect = "login, NM_USUARIO, NM_SOBRENOME, DS_LOTACAO, cargofuncao";
$tituloCampo = "Participante";
$tituloRelatorio = "Radar de Desempenho";


//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa
mysql_free_result($resultado);

$nomeCiclo = "Todos os Ciclos";
if ($codigoCiclo != -1)
{
	//Obter o nome do Ciclo
	$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaCiclo = mysql_fetch_array($resultado);
	$nomeCiclo = $linhaCiclo["NM_CICLO"];
	//Fim obter o nome do Ciclo
}


$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

$totalUsuariosEmpresa = obterTotalUsuariosGrupamento($codigoEmpresa, -1, -1, -1, $dataFinalQuery);
$quantidadeCadastradosEmpresa = $totalUsuariosEmpresa;
$totalUsuarioGrupo = obterTotalUsuariosGrupamento($codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataFinalQuery);
$totalUsuarioLotacao = obterTotalUsuariosGrupamento($codigoEmpresa, -1, $codigoLotacaoUsuario, -1, $dataFinalQuery);


$quantidadeCiclosAvaliacoes = 1;
if ($codigoCiclo == -1)
	$quantidadeCiclosAvaliacoes = obterQuantidadeCiclosAvaliacoes();

$fatoresEmpresa = obterFatoresEmpresa($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $quantidadeCiclosAvaliacoes);
//Fatores da Empresa
$fatorEmpresaMediaAvaliacaoOriginal = $fatoresEmpresa["VALOR_MEDIA_AVALIACAO"] * 10;
$fatorEmpresaMediaAvaliacao = tratarDividirZero($fatoresEmpresa["VALOR_MEDIA_AVALIACAO"] * 10);

$fatorEmpresaMediaAssessmentOriginal = $fatoresEmpresa["VALOR_MEDIA_ASSESSMENT"] * 10;
$fatorEmpresaParticipacaoAssessmentOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_DIAGNOSTIVO"];

$fatorEmpresaParticipacaoAvaliacaoOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_AVALIACAO"];
$fatorEmpresaParticipacaoAvaliacao = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO_AVALIACAO"]);

$fatorEmpresaAcessoOriginal = $fatoresEmpresa["VALOR_ACESSO"];
$fatorEmpresaAcesso = tratarDividirZero($fatoresEmpresa["VALOR_ACESSO"]);

$fatorEmpresaDownloadOriginal = $fatoresEmpresa["VALOR_DOWNLOAD"];
$fatorEmpresaDownload = tratarDividirZero($fatoresEmpresa["VALOR_DOWNLOAD"]);

$fatorEmpresaParticipacaoSimuladoOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_SIMULADO"];
$fatorEmpresaParticipacaoSimulado = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO_SIMULADO"]);

$fatorEmpresaRealizacaoSimuladoOriginal = $fatoresEmpresa["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
$fatorEmpresaRealizacaoSimulado = tratarDividirZero($fatoresEmpresa["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);


$fatorEmpresaParticipacaoOriginal = $fatoresEmpresa["FATOR_PARTICIPACAO"];
$fatorEmpresaParticipacao = tratarDividirZero($fatoresEmpresa["FATOR_PARTICIPACAO"]);

$fatorEmpresaParticipacaoForumOriginal = $fatoresEmpresa["VALOR_PARTICIPACAO_FORUM"];
$fatorEmpresaParticipacaoForum = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO_FORUM"]);

$fatorEmpresaContribuicaoForumOriginal = $fatoresEmpresa["VALOR_CONTRIBUICAO_FORUM"];
$fatorEmpresaContribuicaoForum = tratarDividirZero($fatoresEmpresa["VALOR_CONTRIBUICAO_FORUM"]);

$fatorEmpresaMediaSimuladoOriginal = 0;
$fatorEmpresaMediaSimulado = tratarDividirZero(0);

$fatorParticipacaoEmpresa = tratarDividirZero($fatoresEmpresa["VALOR_PARTICIPACAO"]);


//////Fatores do Grupo
//if ($_SESSION["empresaID"] != 34)
//{
//$fatoresGrupo = obterFatoresGrupo($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoFiltroUsuario, $quantidadeCiclosAvaliacoes);
//$fatorGrupoMediaAvaliacaoOriginal = $fatoresGrupo["VALOR_MEDIA_AVALIACAO"] * 10;
//$fatorGrupoMediaAvaliacao = tratarDividirZero($fatoresGrupo["VALOR_MEDIA_AVALIACAO"] * 10);
//
//$fatorGrupoParticipacaoAvaliacaoOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_AVALIACAO"];
//$fatorGrupoParticipacaoAvaliacao = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_AVALIACAO"]);
//
//$fatorGrupoAcessoOriginal = $fatoresGrupo["VALOR_ACESSO"];
//$fatorGrupoAcesso = tratarDividirZero($fatoresGrupo["VALOR_ACESSO"]);
//
//$fatorGrupoDownloadOriginal = $fatoresGrupo["VALOR_DOWNLOAD"];
//$fatorGrupoDownload = tratarDividirZero($fatoresGrupo["VALOR_DOWNLOAD"]);
//
//$fatorGrupoParticipacaoSimuladoOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_SIMULADO"];
//$fatorGrupoParticipacaoSimulado = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_SIMULADO"]);
//
//$fatorGrupoRealizacaoSimuladoOriginal = $fatoresGrupo["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
//$fatorGrupoRealizacaoSimulado = tratarDividirZero($fatoresGrupo["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);
//
//$fatorGrupoParticipacaoForumOriginal = $fatoresGrupo["VALOR_PARTICIPACAO_FORUM"];
//$fatorGrupoParticipacaoForum = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO_FORUM"]);
//
//$fatorGrupoContribuicaoForumOriginal = $fatoresGrupo["VALOR_CONTRIBUICAO_FORUM"];
//$fatorGrupoContribuicaoForum = tratarDividirZero($fatoresGrupo["VALOR_CONTRIBUICAO_FORUM"]);
//
//$fatorParticipacaoGrupo = tratarDividirZero($fatoresGrupo["VALOR_PARTICIPACAO"]);
//}
////Fim fatores do grupo

//Fatores da Lota��o
$fatoresLotacao = obterFatoresLotacao($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, $codigoLotacaoUsuario, $quantidadeCiclosAvaliacoes);
		
$fatorLotacaoMediaAvaliacaoOriginal = $fatoresLotacao["VALOR_MEDIA_AVALIACAO"] * 10;
$fatorLotacaoMediaAvaliacao = tratarDividirZero($fatoresLotacao["VALOR_MEDIA_AVALIACAO"] * 10);

$fatorLotacaoMediaAssessmentOriginal = $fatoresLotacao["VALOR_MEDIA_ASSESSMENT"] * 10;
$fatorLotacaoParticipacaoAssessmentOriginal = $fatoresLotacao["VALOR_PARTICIPACAO_DIAGNOSTIVO"];

$fatorLotacaoParticipacaoAvaliacaoOriginal = $fatoresLotacao["VALOR_PARTICIPACAO_AVALIACAO"];
$fatorLotacaoParticipacaoAvaliacao = tratarDividirZero($fatoresLotacao["VALOR_PARTICIPACAO_AVALIACAO"]);

$fatorLotacaoAcessoOriginal = $fatoresLotacao["VALOR_ACESSO"];
$fatorLotacaoAcesso = tratarDividirZero($fatoresLotacao["VALOR_ACESSO"]);

$fatorLotacaoDownloadOriginal = $fatoresLotacao["VALOR_DOWNLOAD"];
$fatorLotacaoDownload = tratarDividirZero($fatoresLotacao["VALOR_DOWNLOAD"]);

$fatorLotacaoParticipacaoSimuladoOriginal = $fatoresLotacao["VALOR_PARTICIPACAO_SIMULADO"];
$fatorLotacaoParticipacaoSimulado = tratarDividirZero($fatoresLotacao["VALOR_PARTICIPACAO_SIMULADO"]);

$fatorLotacaoRealizacaoSimuladoOriginal = $fatoresLotacao["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
$fatorLotacaoRealizacaoSimulado = tratarDividirZero($fatoresLotacao["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);

$fatorLotacaoParticipacaoForumOriginal = $fatoresLotacao["VALOR_PARTICIPACAO_FORUM"];
$fatorLotacaoParticipacaoForum = tratarDividirZero($fatoresLotacao["VALOR_PARTICIPACAO_FORUM"]);

$fatorLotacaoContribuicaoForumOriginal = $fatoresLotacao["VALOR_CONTRIBUICAO_FORUM"];
$fatorLotacaoContribuicaoForum = tratarDividirZero($fatoresLotacao["VALOR_CONTRIBUICAO_FORUM"]);

$fatorParticipacaoLotacao = tratarDividirZero($fatoresLotacao["VALOR_PARTICIPACAO"]);

//Fim fatores da Lota��o


//Fatores do Usu�rio
$fatoresParticipante = obterFatoresParticipantes($codigoEmpresa, $dataInicialQuery, $dataFinalQuery, -1, -1 , $codigoUsuarioIndividual, "U", true, true, $quantidadeCiclosAvaliacoes);
$fatoresParticipante = mysql_fetch_array($fatoresParticipante);
		
$fatorParticipanteMediaAvaliacaoOriginal = $fatoresParticipante["VALOR_MEDIA_AVALIACAO"] * 10;
$fatorParticipanteMediaAvaliacao = tratarDividirZero($fatoresParticipante["VALOR_MEDIA_AVALIACAO"] * 10);

$fatorParticipanteMediaAssessmentOriginal = $fatoresParticipante["VALOR_MEDIA_ASSESSMENT"] * 10;
$fatorParticipanteParticipacaoAssessmentOriginal = $fatoresParticipante["VALOR_PARTICIPACAO_DIAGNOSTIVO"];


$fatorParticipanteParticipacaoAvaliacaoOriginal = $fatoresParticipante["VALOR_PARTICIPACAO_AVALIACAO"];
$fatorParticipanteParticipacaoAvaliacao = tratarDividirZero($fatoresParticipante["VALOR_PARTICIPACAO_AVALIACAO"]);

$fatorParticipanteAcessoOriginal = $fatoresParticipante["VALOR_ACESSO"];
$fatorParticipanteAcesso = tratarDividirZero($fatoresParticipante["VALOR_ACESSO"]);

$fatorParticipanteDownloadOriginal = $fatoresParticipante["VALOR_DOWNLOAD"];
$fatorParticipanteDownload = tratarDividirZero($fatoresParticipante["VALOR_DOWNLOAD"]);

$fatorParticipanteParticipacaoSimuladoOriginal = $fatoresParticipante["VALOR_PARTICIPACAO_SIMULADO"];
$fatorParticipanteParticipacaoSimulado = tratarDividirZero($fatoresParticipante["VALOR_PARTICIPACAO_SIMULADO"]);

$fatorParticipanteRealizacaoSimuladoOriginal = $fatoresParticipante["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"];
$fatorParticipanteRealizacaoSimulado = tratarDividirZero($fatoresParticipante["VALOR_REALIZACAO_SIMULADO_ACIMA_MEDIA"]);

$fatorParticipanteParticipacaoForumOriginal = $fatoresParticipante["VALOR_PARTICIPACAO_FORUM"];
$fatorParticipanteParticipacaoForum = tratarDividirZero($fatoresParticipante["VALOR_PARTICIPACAO_FORUM"]);

$fatorParticipanteContribuicaoForumOriginal = $fatoresParticipante["VALOR_CONTRIBUICAO_FORUM"];
$fatorParticipanteContribuicaoForum = tratarDividirZero($fatoresParticipante["VALOR_CONTRIBUICAO_FORUM"]);

$fatorParticipacaoParticipante = tratarDividirZero($fatoresParticipante["VALOR_PARTICIPACAO"]);

//*************************************** Fim Altera��o

function tratarDividirZero($valor)
{
	if ($valor == 0)
	{
		return 1;
	}
	else 
	{
		return $valor;
	}
}

?>

<html>
<head>
	<title>Colaborae Consultoria e Educa��o Corporativa</title>
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		
		.tabelaRanking td
		{
			padding: 1px 3px 1px 3px;
		}
	</style>
	
</head>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk' nowrap>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr class="textblk">
		<td>
			<b>Ciclo: <?php echo $nomeCiclo; ?></b>
		</td>
	</tr>
	<tr class="textblk">
		<td width="100%">
			<b>Per�odo:&nbsp;
							<?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
			</b>
		</td>
	</tr>
		<?php
//		 if ($_SESSION["empresaID"] != 34)
//		{
//		?>
	<!--<tr class="textblk">
		<td class="textblk">
			<b>Nome do Grupo Gerencial: <?php //echo $nomeFiltro; ?></b>
		</td>
	</tr>
		<?php 
//		}
		?>-->
	<tr class="textblk">
		<td class="textblk">
			<b>Grupo de Gest�o: <?php echo $nomeLotacao; ?></b>
		</td>
	</tr>
	<tr class="textblk">
		<td style="color: navy;">
			<b>Nome: <?php echo $nomeUsuario; ?></b>
		</td>
	</tr>
	
	<!-- 
	
	<tr class="textblk">
		<td>
			Cadastrados da Empresa: <?php echo $totalUsuariosEmpresa; ?>
		</td>
	</tr>
	<?php
	if ($tipoRelatorio == "G")
	{
	?>
	<tr class="textblk">
		<td>
			Cadastrados do Grupo: <?php echo $totalUsuarioGrupo; ?>
		</td>
	</tr>
	<tr class="textblk">
		<td>
			Quantidade de Participantes no Grupo: <?php echo $quantidadeRankeados; ?>
		</td>
	</tr>

	 -->
	<?php
	}
	?>
</table>

<?php

		$linhaSelecionada = null;

		foreach ($linhas as $linha)
		{
			if ($linha["FILTRADO"] == 0)
				continue;

			//Aqui vinha a tabela de informacoes que passou para baixo	
			$linhaSelecionada = $linha;
			
			//Forcao nao aparecer o que nao deve
			$exibirParticipacao = 2;
			$exibirParticipacaoForum = 2;
	
		}
	
		
		function formatarValores($valor, $decimal = 2, $seVazio = "-")
		{
			if ($valor == "")
			{
				return $seVazio;
			}
			
			return number_format(round($valor,$decimal), $decimal, ",", "");
			
		}
		
		function aplicarNaoRanqueado($linha, $campoRanking, $campoFator)
		{
			$campoFator = $campoFator . "_ACESSO";
			
			if (!$linha[$campoFator])
				return "NR";
				
			return $linha[$campoRanking];
			
		}
		
		
		
	?>
	
</table>

<br />

<?php
	$linha = $linhaSelecionada;
?>
<table cellpadding="1" cellspacing="0" border="0" style="margin-left: 2.5%; margin-right: 2.5%;">
	<tr class="textblk" >
		<td width="155" style="border-bottom: 1px solid #000;"><b>Indicadores</b></td>
		<td width="128" align="center" style="border-bottom: 1px solid #000;"><b>Perfil Empresarial</b></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td width="115" align="center" style="border-bottom: 1px solid #000;"><b>Perfil Grupo</b></td>
		<?php 
		//}
		?>
		--><td width="115" align="center" style="border-bottom: 1px solid #000;"><b>Perfil Lota��o</b></td>
		<td width="150" align="center" style="border-bottom: 1px solid #000;"><b>Perfil do <br /> Participante</b></td>
		<td rowspan="14" >&nbsp;</td>
	</tr>
	<?php 
	if($exibirMediaAvaliacao!=2)
	{
	?>
	<tr class="textblk">
		<td>M�dia do Diagn�stico <?php //echo $labelAvaliacao; ?></td>
		<td align="center"><?php echo formatarValores($fatorEmpresaMediaAssessmentOriginal,2); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center"><?php //echo formatarValores($fatorGrupoMediaAvaliacaoOriginal,2); ?></td>
		<?php 
		//}
		?>
		--><td align="center"><?php echo formatarValores($fatorLotacaoMediaAssessmentOriginal,2); ?></td>
		<td align="center"><?php echo formatarValores($fatorParticipanteMediaAssessmentOriginal,2, 0); ?></td>
	</tr>
	
	<?php
	//}
	//if($exibirParticipacaoAvaliacao!=2)
	//{
	?>
	<tr class="textblk" nowrap style="border-bottom: 1px solid #000;">
		<td style="border-bottom: 1px solid #000;">Quantidade de Diagn�sticos<?php //echo $labelAvaliacao; ?></td>
		<td align="center" style="border-bottom: 1px solid #000;"><?php echo formatarValores($fatorEmpresaParticipacaoAssessmentOriginal,0, 0); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center" style="border-bottom: 1px solid #000;"><?php //echo formatarValores($fatorGrupoParticipacaoAvaliacaoOriginal,0, 0); ?></td>
		<?php 
		//}
		?>
		--><td align="center" style="border-bottom: 1px solid #000;"><?php echo formatarValores($fatorLotacaoParticipacaoAssessmentOriginal,0, 0); ?></td>
		<td align="center" style="border-bottom: 1px solid #000;"><?php echo formatarValores($fatorParticipanteParticipacaoAssessmentOriginal,0); ?></td>
	</tr>
	<?php
	}
	if($exibirAcesso!=2)
	{
	?>
	<tr class="textblk">
		<td>Acessos por Participante</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaAcessoOriginal,2); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center"><?php //echo formatarValores($fatorGrupoAcessoOriginal,2); ?></td>
		<?php 
		//}
		?>
		--><td align="center"><?php echo formatarValores($fatorLotacaoAcessoOriginal,2); ?></td>
		<td align="center"><?php echo formatarValores($fatorParticipanteAcessoOriginal,0); ?></td>
	</tr>
	<?php
	}
	$exibirDownload = 2;
	if($exibirDownload!=2)
	{
	?>
	<tr class="textblk">
		<td>Downloads por Participante</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaDownloadOriginal,2); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center"><?php //echo formatarValores($fatorGrupoDownloadOriginal,2); ?></td>
		<?php 
		//}
		?>
		--><td align="center"><?php echo formatarValores($fatorLotacaoDownloadOriginal,2); ?></td>
		<td align="center"><?php echo formatarValores($fatorParticipanteDownloadOriginal,0); ?></td>
	</tr>
	<?php
	}
	if($exibirRealizacaoSimulado!=2 || 1==1)
	{
	?>
	<tr class="textblk">
		<td>Simulados por Participante</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaParticipacaoSimuladoOriginal,2); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center"><?php //echo formatarValores($fatorGrupoParticipacaoSimuladoOriginal,2); ?></td>
		<?php 
		//}
		?>
		--><td align="center"><?php echo formatarValores($fatorLotacaoParticipacaoSimuladoOriginal,2); ?></td>
		<td align="center"><?php echo formatarValores($fatorParticipanteParticipacaoSimuladoOriginal,0); ?></td>
	</tr>
	<tr class="textblk">
		<td>Simulados com Aprova��o</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaRealizacaoSimuladoOriginal, 2); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center"><?php //echo formatarValores($fatorGrupoRealizacaoSimuladoOriginal, 2); ?></td>
		<?php 
		//}
		?>
		--><td align="center"><?php echo formatarValores($fatorLotacaoRealizacaoSimuladoOriginal, 2); ?></td>
		<td align="center"><?php echo formatarValores($fatorParticipanteRealizacaoSimuladoOriginal, 0); ?></td>
	</tr>
	<?php
	}
	
	
	
	$exibirParticipacaoSimulado = 2;
	if($exibirParticipacaoSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td>Participa��o em Simulado</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaParticipacaoSimuladoOriginal,2); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center"><?php //echo formatarValores($fatorGrupoParticipacaoSimuladoOriginal,2); ?></td>
		<?php 
		//}
		?>
		--><td align="center"><?php echo formatarValores($fatorLotacaoParticipacaoSimuladoOriginal,2); ?></td>
		<td align="center"><?php echo formatarValores($fatorParticipanteParticipacaoSimuladoOriginal,0); ?></td>
	</tr>
	<?php
	}
	$exibirParticipacaoForum = 1;
	if($exibirParticipacaoForum!=2)
	{
	?>
	<tr class="textblk">
		<td>Participa��o em F�runs</td>
		<td align="center"><?php echo round($fatorEmpresaParticipacaoForumOriginal,2); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center"><?php //echo round($fatorGrupoParticipacaoForumOriginal,2); ?></td>
		<?php 
		//}
		?>
		--><td align="center"><?php echo round($fatorLotacaoParticipacaoForumOriginal,2); ?></td>
		<td align="center"><?php echo round($fatorParticipanteParticipacaoForumOriginal,0); ?></td>
	</tr>
	<?php
	}
	if($exibirContribuicaoForum!=2)
	{
	?>
	<tr class="textblk">
		<td>Contribui��es em F�rum</td>
		<td align="center"><?php echo formatarValores($fatorEmpresaContribuicaoForumOriginal,0); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center"><?php //echo formatarValores($fatorGrupoContribuicaoForumOriginal,0); ?></td>
		<?php 
		//}
		?>
		--><td align="center"><?php echo formatarValores($fatorLotacaoContribuicaoForumOriginal,0); ?></td>
		<td align="center"><?php echo formatarValores($fatorParticipanteContribuicaoForumOriginal,0); ?></td>
	</tr>	
	<?php
	}
	?>
	<?php
	$exibir3ultimasLinhas = false;
	if($exibir3ultimasLinhas)
	{
	?>	
	<tr class="textblk">
		<td style="border-top: 1px solid #000;">Cadastrados</td>
		<td style="border-top: 1px solid #000;" align="center"><?php echo $totalUsuariosEmpresa; ?></td>
		<?php if ($_SESSION["empresaID"] != 34)
		{
		?>
		<td style="border-top: 1px solid #000;" align="center"><?php echo $totalUsuarioGrupo; ?></td>
		<?php 
		}
		?>
		<td style="border-top: 1px solid #000;" align="center"><?php echo $totalUsuarioLotacao; ?></td>
		<td style="border-top: 1px solid #000;" align="center"> - </td>
	</tr>
	<tr class="textblk">
		<td>Participantes</td>
		<td align="center"><?php echo $fatorParticipacaoEmpresa; ?></td>
		<?php if ($_SESSION["empresaID"] != 34)
		{
		?>
		<td align="center"><?php echo $fatorParticipacaoGrupo; ?></td>
		<?php 
		}
		?>
		<td align="center"><?php echo $fatorParticipacaoLotacao; ?></td>
		<td align="center"> - </td>
	</tr>
	<tr class="textblk">
		<td>% de Participa��o</td>
		<td align="center"><?php echo formatarValores($fatorParticipacaoEmpresa / $totalUsuariosEmpresa * 100,2); ?>%</td>
		<?php if ($_SESSION["empresaID"] != 34)
		{
		?>
		<td align="center"><?php echo str_ireplace(".", ",", round($fatorParticipacaoGrupo / $totalUsuarioGrupo * 100,2)); ?>%</td>
		<?php 
		}
		?>
		<td align="center"><?php echo str_ireplace(".", ",", round($fatorParticipacaoLotacao / $totalUsuarioLotacao * 100,2)); ?>%</td>
		<td align="center"><?php echo formatarValores($fatorParticipacaoParticipante * 100,2); ?>%</td>
	</tr>
	<?php
	}
	?>
	<?php
	$exibirMediaSimulado = 2;
	if($exibirMediaSimulado!=2)
	{
	?>
	<tr class="textblk">
		<td>Nota em Simulado</td>
		<td align="center"><?php echo round($fatorEmpresaMediaSimuladoOriginal,2); ?></td>
		<td align="center"><?php echo round($fatorGrupoMediaSimuladoOriginal,2); ?></td>
		<td align="center"><?php echo round($linha["MEDIA_SIMULADO_INDIVIDUAL"],2); ?></td>
	</tr>	
	<?php
	}
	$exibirParticipacao = 2;
	if($exibirParticipacao!=2)
	{
	?>	
		<tr class="textblk">
			<td>Participa��o</td>
			<td align="center"><?php echo round($fatorEmpresaParticipacao,2); ?></td>
			<td align="center"><?php echo round($fatorGrupoParticipacaoOriginal,2); ?></td>
			<td align="center"><?php echo round($linha["FATOR_PARTICIPACAO_INDIVIDUAL"],0); ?></td>
		</tr>
	<?php
	}
	?>
	
	<tr class="textblk" style="border-top: 1px solid #000;">
		<td style="border-top: 1px solid #000;">M�dia das <?php echo $labelAvaliacao; ?></td>
		<td align="center" style="border-top: 1px solid #000;"><?php echo formatarValores($fatorEmpresaMediaAvaliacaoOriginal,2); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center"><?php //echo formatarValores($fatorGrupoMediaAvaliacaoOriginal,2); ?></td>
		<?php 
		//}
		?>
		--><td align="center" style="border-top: 1px solid #000;"><?php echo formatarValores($fatorLotacaoMediaAvaliacaoOriginal,2); ?></td>
		<td align="center" style="border-top: 1px solid #000;"><?php echo formatarValores($fatorParticipanteMediaAvaliacaoOriginal,2, 0); ?></td>
	</tr>
	
	<tr class="textblk" nowrap style="border-bottom: 1px solid #000;">
		<td style="border-bottom: 1px solid #000;">Quantidade de <?php echo $labelAvaliacao; ?></td>
		<td align="center" style="border-bottom: 1px solid #000;"><?php echo formatarValores($fatorEmpresaParticipacaoAvaliacaoOriginal,0, 0); ?></td>
		<!--<?php //if ($_SESSION["empresaID"] != 34)
		//{
		?>
		<td align="center" style="border-bottom: 1px solid #000;"><?php //echo formatarValores($fatorGrupoParticipacaoAvaliacaoOriginal,0, 0); ?></td>
		<?php 
		//}
		?>
		--><td align="center" style="border-bottom: 1px solid #000;"><?php echo formatarValores($fatorLotacaoParticipacaoAvaliacaoOriginal,0, 0); ?></td>
		<td align="center" style="border-bottom: 1px solid #000;"><?php echo formatarValores($fatorParticipanteParticipacaoAvaliacaoOriginal,0); ?></td>
	</tr>
	
		<tr class="textblk"><td>&nbsp;</td>
		<tr class="textblk"><td>&nbsp;</td>
	
</table>

<p>



</p>


<div style="width:100%;text-align:center">
	<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
	<p class="textoInformativo">Favor definir o formato paisagem para fins de impress�o</p>
</div>	
</body>
</html>