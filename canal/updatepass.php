<?
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";
include "../include/accesscounter.php";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<script language="JavaScript" src="/include/js/utilities.js"></script>
<script language="JavaScript">
function validarFormulario()
{
	if ((document.cadastroForm.userpass1.value == '')||(document.cadastroForm.userpass1.value.length < 6))
	{
		alert('   A senha atual est� vazia ou com menos de 6 caracteres!   ');
		return false;
	}
	if (document.cadastroForm.userpass2.value != document.cadastroForm.userpass3.value)
	{
		alert('   A confirma��o de senha n�o foi digitada corretamente!   ');
		return false;
	}
	document.cadastroForm.submit();
}
</script>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr>
<td background="/images/layout/bg_logo_admin.png">
	<table border="0" cellpadding="0" cellspacing="0" width="663" background="/canal/images/logo_canal.png">
	<tr>
	<td><img src="/images/layout/blank.gif" width="1" height="32"></td>
	<td align="right" class="data"><? echo getServerDate(); ?></td>
	</tr>
	</table>	
</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="100" height="2"></td></tr>
<tr><td bgcolor="#cccccc"><img src="/images/layout/blank.gif" width="100" height="3"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<tr>
<td width="1%"><img src="/images/layout/blank.gif" width="289" height="20"></td>
<td></td>
<td></td>
</tr>
<tr valign="top">
<td class="textblk"><span class="title">USU�RIO:&nbsp;</span><?php echo $_SESSION["alias"]; ?></td><td width="1%"><input type="button" class="buttonsty" value="Logout" onclick="document.location.replace('/logout.php')" onfocus="noFocus(this)"></td><td width="98%" align="right"><input type="button" class="buttonsty" value="Voltar" onclick="document.location.href='index.php'" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="280" height="11"></td></tr>
<tr>
<td colspan="3" align="center" valign="bottom">
<?
$erro = empty($_GET["erro"])?"":$_GET["erro"];

if	  ($erro==1)  echo "<font class=\"alert\">ERRO: A senha atual est� incorreta. Digite novamente.<br><br></font>";
elseif($erro==2)  echo "<font class=\"alert\">ERRO: Senhas n�o coincidem. Digite novamente.<br><br></font>";
else 			  echo "<img src=\"/images/layout/blank.gif\" width=\"280\" height=\"28\">";
?>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="756" align="center">
<form name="cadastroForm" action="update.php" method="post">
<tr class="tarjaTitulo">
	<td height="20" colspan="2" align="center">ALTERA��O DE SENHA DE USU�RIO</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="289" height="20"></td></tr>
<tr>
	<td class="title" align="right" width="1%">Senha atual:&nbsp;</td>
	<td><input name="userpass1" type="password" class="textbox2" maxlength="8" tabIndex="3"></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
	<td class="title" align="right" width="1%">Senha&nbsp;nova:&nbsp;</td>
	<td class="textblk"><input name="userpass2" type="password" class="textbox2" maxlength="8" tabIndex="4"> (min 6 caracteres, max 8 caracteres)</td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="5"></td></tr>
<tr>
	<td class="title" align="right" width="1%">Confirmar&nbsp;Senha:&nbsp;</td>
	<td><input name="userpass3" type="password" class="textbox2" maxlength="8" tabIndex="5"></td>
</tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="20"></td></tr>
<tr><td></td><td><input type="button" class="buttonsty" value="Alterar" onclick="validarFormulario()" onfocus="noFocus(this)"></td></tr>
<tr><td><img src="/images/layout/blank.gif" width="1" height="10"></td></tr>
</form>
</table>
<?
mysql_close();
?>
</body>
</html>
