<?php

$ciclos = obterCiclos($empresaID);

$complementoCiclo = "sumario";

$usuario = $_SESSION["cd_usu"];

$ciclosLiberados = obterCiclosLiberados($usuario);
$cicloAtual = obterCicloAtual($empresaID);

obterTextoComentario(basename("/sumario_artigo_video",".php"));

//Verifica se existem comentários para os ciclos
$sql = "SELECT count(1) FROM col_comentario WHERE NM_PAGINA = 'sumario_artigo_video.php' AND IN_ATIVO = 1 AND CD_EMPRESA = $empresaID AND (CD_CICLO IS NOT NULL AND CD_CICLO > 0)";
$resultado = DaoEngine::getInstance()->executeQuery($sql);
$linha = mysql_fetch_row($resultado);

//echo "<!-- $sql -->";

if($empresaID == 44 && $tipo == 1){
    foreach($ciclos as $ciclo){
         $ciclosLiberados[$ciclo["CD_CICLO"]] = $ciclo["CD_CICLO"];
    }
}

if ($linha[0] > 0){

?>

<div class="productDetails" style="width: 100%">

    <ul class="tabs" id="<?php echo $complementoCiclo; ?>" style="font-size: 11px">

        <?php
        if($cicloAtual == 0)
            $cicloAtual = $ciclos[0]["CD_CICLO"];

        if (!in_array($cicloAtual, $ciclosLiberados) && $obrigaDiagnostico == 1 && $tipo == 1){
            $chaves = array_keys($ciclosLiberados);
            $cicloAtual = $ciclosLiberados[$chaves[0]];
        }

        foreach($ciclos as $ciclo){

            if(!in_array($ciclo["CD_CICLO"],$ciclosLiberados) && $obrigaDiagnostico == 1 && $tipo == 1){
                continue;
            }

            $selecionado = "";
            if($cicloAtual == $ciclo["CD_CICLO"]){
                $selecionado = " class=\"selected\"";
            }

            echo "<li $selecionado onclick=\"javascript:trocaAba(this,'$complementoCiclo', {$ciclo["CD_CICLO"]});\"><a>{$ciclo["NM_CICLO"]}<br />&nbsp;</a></li>";
        }

        ?>

    </ul>

    <div class="boxR1">

        <div class="boxR12">

            <div class="boxR13">

                <div class="boxR14">

                    <div class="activeTabs">

                        <div class="contTabs" id="<?php echo "tabs$complementoCiclo"; ?>" style=" margin-left:10px; margin-right:10px">


                            <?php

                            foreach($ciclos as $ciclo){

                                if(!in_array($ciclo["CD_CICLO"],$ciclosLiberados) && $obrigaDiagnostico == 1 && $tipo == 1){
                                    continue;
                                }

                                $exibir = "";
                                if($cicloAtual != $ciclo["CD_CICLO"]){
                                    $exibir = "style=\"display:none;\"";
                                }

                                echo "<div id=\"$complementoCiclo{$ciclo["CD_CICLO"]}\" $exibir class=\"conteudo\">";

                                    obterTextoComentario(basename("/sumario_artigo_video",".php"), true, $ciclo["CD_CICLO"]);

                                echo "</div>";

                            }


                            ?>


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



</div>
<?php

}

?>