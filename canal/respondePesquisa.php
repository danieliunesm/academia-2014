<?php
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";
include "../include/accesscounter.php";
include('../admin/framework/crud.php');
include('../admin/controles.php');


$codigoEmpresa = $_SESSION["empresaID"];
$codigoUsuario = $_SESSION["cd_usu"];


if (isset($_POST["btnEnviar"]))
{
	
	$codigoPesquisa = $_POST["hdnPesquisa"];
	$comentario = str_ireplace("'", "\\'",$_POST["txtComentario"]);
	
	$sql = "INSERT INTO col_usuario_pesquisa
				(CD_USUARIO, CD_PESQUISA, CD_EMPRESA, DT_RESPOSTA, DS_COMENTARIO)
			VALUES
				($codigoUsuario, $codigoPesquisa, $codigoEmpresa, sysdate(), '$comentario')";
	
	DaoEngine::getInstance()->executeQuery($sql, true);
	
	foreach($_POST as $chave => $valor)
	{
		if (substr($chave,0, 12) == "cboResposta_")
		{
			$indice = substr($chave, 12);
			$sql = "INSERT INTO col_resposta_pesquisa
						(CD_PERGUNTA_PESQUISA, CD_USUARIO, VL_RESPOSTA)
					VALUES
						($indice, $codigoUsuario, $valor)";
			
			DaoEngine::getInstance()->executeQuery($sql, true);
		}
	}
	
}


?>

<html>
	<head>
	
	<script language="javascript">
		function fechaJanela()
		{
			alert("Obrigado por participar da nossa pesquisa");
			self.close();
		}
	
	</script>
	
	</head>

	<body onload="fechaJanela();">
	
	</body>

</html>