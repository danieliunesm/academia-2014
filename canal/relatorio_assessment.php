<?php
//Includes
include('../../admin/page.php');
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/relatorio_util.php');
include('../../admin/controles.php');
include ('relatorio_ranking_util.php');
include('relatorio_acesso_util.php');

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certificações";
}
else
{
	$labelAvaliacao = "Avaliações";
}

//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$tituloTotal = "";
$tituloRelatorio = "Ranking";

$tipoRelatorio = $_REQUEST["hdnTipoRelatorio"];
$codigoFiltro = 0;
$POST = obterPost($codigoFiltro);
$classificacaoRelatorio = $_REQUEST["tipo"];

$nomeLotacao = "{$POST["nomeHierarquia"]}: {$POST["nomeLotacao"]}";
if ($POST["nomeLotacao"] == "")
{
	$nomeLotacao = "&nbsp;";
}

$codigoLotacaoUsuario = $_REQUEST["hdnLotacao"];

$codigoEmpresa = $_SESSION["empresaID"];
$dataInicial = $POST["txtDe"];
$dataFinal = $POST["txtAte"];
$cargo = joinArray($POST['cboCargo'],"','");
$codigoUsuario = (isset($_REQUEST["cboUsuario"])?$_REQUEST["cboUsuario"]:-1);
$codigoLotacao = joinArray($POST['cboLotacao']);
$dataInicioTexto = $dataInicial;
$dataTerminoTexto = $dataFinal;
$codigoCiclo = $_REQUEST["hdnCiclo"];


if (isset($_GET["cboUsuario"]) || ($_SESSION["tipo"] >=4 || $_SESSION["tipo"] == -1 || $_SESSION["tipo"] == -2 || $_SESSION["tipo"] == -3))
{
	
}
else
{
	$codigoUsuario = $_SESSION["cd_usu"];
}

if ($codigoUsuario > -1 && $classificacaoRelatorio == "C") {
	$sql = "SELECT lotacao FROM col_usuario WHERE CD_USUARIO = $codigoUsuario";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaLotacao = mysql_fetch_array($resultado);
	$codigoLotacao = $linhaLotacao["lotacao"];
}

if ($_REQUEST["operacao"] ==765)
{
	
	$codigoEmpresa = 5;
	$dataInicial = "";
	$dataFinal = "";
	$cargo = joinArray($POST['cboCargo'],"','");
	$codigoUsuario = joinArray($POST['cboUsuario']);
	$codigoLotacao = joinArray($POST['cboLotacao']);
	$dataInicioTexto = $dataInicial;
	$dataTerminoTexto = $dataFinal;
	$codigoCiclo = -1;
	$codigoFiltroUsuario = 1;
}

$usaData = true;
if ($dataInicial == "" && $dataFinal == ""){
	$usaData = false;
}

if ($dataInicial==""){
	$dataInicial = '01/01/2008';
}
if ($dataFinal=="") {
	$dataFinal = date("d/m/Y");
}

$dataAtual = $dataInicial;

// Fim obter parametros
$campoSelect = "login, NM_USUARIO, NM_SOBRENOME, DS_LOTACAO, cargofuncao";
$tituloCampo = "Participante";
$tituloRelatorio = "Radar de Aprendizado";

if ($classificacaoRelatorio == "C") {
	$tituloRelatorio = "Radar de Conhecimento";
}

//Obter o nome da empresa
$sql = "SELECT DS_EMPRESA FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$linhaEmpresa = mysql_fetch_array($resultado);
$nomeEmpresa = $linhaEmpresa["DS_EMPRESA"];
//Fim obter nome da empresa
mysql_free_result($resultado);

$nomeCiclo = "Todos os Ciclos";
if ($codigoCiclo != -1)
{
	//Obter o nome do Ciclo
	$sql = "SELECT NM_CICLO FROM col_ciclo WHERE CD_CICLO = $codigoCiclo";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linhaCiclo = mysql_fetch_array($resultado);
	$nomeCiclo = $linhaCiclo["NM_CICLO"];
	//Fim obter o nome do Ciclo
}


$dataInicialQuery = conveterDataParaYMD($dataInicial);
$dataFinalQuery = conveterDataParaYMD($dataFinal);

$totalUsuarioLotacao = obterTotalUsuariosGrupamento($codigoEmpresa, -1, $codigoLotacaoUsuario, -1, $dataFinalQuery);


$mediaGeralProvasCalculada = 0;
$quantidadeTotalAvaliacoesMaisSegundaChamada = 0;
obterProvasMediaCiclo();

$dadosAssessment = null;

if ($classificacaoRelatorio == "C") {
	$dadosAssessment = obterDadosAssessmentHierarquia($codigoEmpresa, $codigoLotacao, $codigoUsuario);
} else {
	$dadosAssessment = obterDadosAssessment($codigoEmpresa, $codigoLotacao, $codigoUsuario);
}

//$dadosAssessment = obterDadosAssessmentEmpresa($codigoLotacaoUsuario);


function tratarDividirZero($valor)
{
	if ($valor == 0)
	{
		return 1;
	}
	else 
	{
		return $valor;
	}
}

$totalQuestoes = 0;

$grupos = obterGruposDisciplinaAssessmentEmpresa($codigoEmpresa);
//$grupos[1]["nome"] = "Produtos e Serviços";
//$grupos[2]["nome"] = "Serviços VAS";
//$grupos[3]["nome"] = "Inteligência Competitiva";
//$grupos[4]["nome"] = "Processos";
//$grupos[5]["nome"] = "TIM Evolução Tecnológica";

foreach ($grupos as $codigoGrupo => $nomeGrupo) {
	
	$grupos[$codigoGrupo]["total1"] = 0;
	$grupos[$codigoGrupo]["certas1"] = 0;
	$grupos[$codigoGrupo]["total2"] = 0;
	$grupos[$codigoGrupo]["certas2"] = 0;
	$grupos[$codigoGrupo]["total3"] = 0;
	$grupos[$codigoGrupo]["certas3"] = 0;
	$grupos[$codigoGrupo]["itens"] = array();
	$grupos[$codigoGrupo]["perguntas"] = 0;

}

//for($i = 1; $i < 5; $i++)
//{
//	$grupos[$i]["total"] = 0;
//	$grupos[$i]["certas"] = 0;
//	$grupos[$i]["itens"] = array();
//	$grupos[$i]["perguntas"] = 0;
//}

while($linhaAssessment = mysql_fetch_array($dadosAssessment))
{
	$codigoGrupo = $linhaAssessment["CD_GRUPO_DISCIPLINA"];
	$grupos[$codigoGrupo]["total1"] = $grupos[$codigoGrupo]["total1"] + $linhaAssessment["QD_TOTAL_1"];
	$grupos[$codigoGrupo]["certas1"] = $grupos[$codigoGrupo]["certas1"] + $linhaAssessment["QD_CERTAS_1"];
	$grupos[$codigoGrupo]["total2"] = $grupos[$codigoGrupo]["total2"] + $linhaAssessment["QD_TOTAL_2"];
	$grupos[$codigoGrupo]["certas2"] = $grupos[$codigoGrupo]["certas2"] + $linhaAssessment["QD_CERTAS_2"];
	$grupos[$codigoGrupo]["total3"] = $grupos[$codigoGrupo]["total3"] + $linhaAssessment["QD_TOTAL_3"];
	$grupos[$codigoGrupo]["certas3"] = $grupos[$codigoGrupo]["certas3"] + $linhaAssessment["QD_CERTAS_3"];
	//$grupos[$codigoGrupo]["perguntas"] = $grupos[$codigoGrupo]["perguntas"] + $linhaAssessment["NR_QTD_PERGUNTA_TOTAL"];
	$grupos[$codigoGrupo]["itens"][] = $linhaAssessment;
	//$totalQuestoes = $totalQuestoes + $linhaAssessment["NR_QTD_PERGUNTA_TOTAL"];
}




?>

<html>
<head>
	<title>Colaborae Consultoria e Educação Corporativa</title>
	<LINK rel="stylesheet" type="text/css" href="../../admin/include/css/admincolaborae.css">	
	<style type="text/css">

		@media print {
			.buttonsty {
			  display: none;
			}
			.textoInformativo{
				display: none;
			}
		}
		
		.textoInformativo{
			FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #000; LINE-HEIGHT: 1.4em; FONT-FAMILY: "verdana","arial","helvetica",sans-serif						
		}
	
		.titRelat, .titRelatD
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatD
		{
			text-align: left;
		}
		
		.titRelatTop
		{
			border-top: 1px solid #000000; text-align: center;
		}
		
		.titRelatEsq
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.titRelatRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
		}
		
		.titRelatEsqRod
		{
			border-right: 1px solid #000000; border-top: 1px solid #000000;
			border-bottom: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdLat
		{
			border-right: 1px solid #000000;
		}
		
		.bdLatEsq
		{
			border-right: 1px solid #000000;
			border-left: 1px solid #000000;
		}
		
		.bdEsq
		{
			border-left: 1px solid #000000;
		}

		.bdRod
		{
			border-bottom: 1px solid #000000;
		}
		
		.tabelaRanking td
		{
			padding: 1px 3px 1px 3px;
		}
	</style>
	
</head>
<body>
<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr style="height: 30px">
		<td class='textblk'>
			<p><b><?php echo $nomeEmpresa; ?></b></p>
		</td>
		<td width="65%" class="textblk" align="center">
			<b>
				<?php echo $tituloRelatorio; ?>
			</b>
		</td>
		<td style="text-align: right" class='textblk'>
			<?php echo date("d/m/Y"); ?><br /><?php echo date("H:i:s"); ?>
		</td>
	</tr>
</table>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr class="textblk">
		<td>
			<b>Ciclo: <?php echo $nomeCiclo; ?></b>
		</td>
	</tr>
	<tr class="textblk">
		<td width="100%">
			<b>Período:&nbsp;
							<?php
												if($dataInicioTexto!=""){
													echo "de $dataInicioTexto ";
													if($dataTerminoTexto!=""){
														echo "a $dataTerminoTexto";
													}
												}else{
													echo "Todos ";
												}
							?>
			</b>
		</td>
	</tr>
	<tr class="textblk">
	<td class="textblk">
		<b><?php echo $nomeLotacao; ?></b>
	</td>
	</tr>
</table>

<?php 

$tituloColuna1 = "Diagnóstico";
$tituloColuna2 = "Aprendizado";
$tituloColuna3 = "Avaliação";

if ($classificacaoRelatorio == "C") {
	
	$tituloColuna1 = "Empresa";
	$tituloColuna2 = "Grupo de Gestão";
	$tituloColuna3 = "Participante";
	
}

?>

<table cellpadding="1" cellspacing="0" align="center" width="95%" border="0">
	<tr class="textblk">
		<td>&nbsp;</td>
		<td align="center"><b><?php echo $tituloColuna1;?></b></td>
		
		<?php if(!($classificacaoRelatorio == "C" && $codigoLotacao == "-1")) {?>
		<td align="center"><b><?php echo $tituloColuna2;?></b></td>
		<?php }
			  if(!($classificacaoRelatorio == "C" && $codigoUsuario == "-1")) {?>
		<td align="center"><b><?php echo $tituloColuna3;?></b></td>
		<?php }?>
	</tr>
<!--  	<tr class="textblk">
		<td>Cadastrados</td>
		<td><?php echo $totalUsuarioEmpresa; ?></td>
		<td><?php echo $totalUsuarioLotacao; ?></td>
		<td> - </td>
	</tr>
	<tr class="textblk">
		<td>Participantes</td>
		<td><?php echo $totalParticipantesEmpresa; ?></td>
		<td><?php echo $totalParticipantesLotação; ?></td>
		<td> - </td>
	</tr>
	<tr class="textblk">
		<td>Nível de Conhecimento</td>
		<td><?php echo $totalParticipantesEmpresa; ?></td>
		<td><?php echo $totalParticipantesLotação; ?></td>
		<td> - </td>
	</tr>
-->
	
	<?php

		$linhaSelecionada = null;

		foreach ($linhas as $linha)
		{
			if ($linha["FILTRADO"] == 0)
				continue;

			//Aqui vinha a tabela de informacoes que passou para baixo	
			$linhaSelecionada = $linha;
			
			//Forcao nao aparecer o que nao deve
			$exibirParticipacao = 2;
			$exibirParticipacaoForum = 2;
	
		}
	
		
		function formatarValores($valor, $decimal = 2, $seVazio = "-")
		{
			if ($valor == "")
			{
				return $seVazio;
			}
			
			return number_format(round($valor,$decimal), $decimal, ",", "");
			
		}
		
		function aplicarNaoRanqueado($linha, $campoRanking, $campoFator)
		{
			$campoFator = $campoFator . "_ACESSO";
			
			if (!$linha[$campoFator])
				return "NR";
				
			return $linha[$campoRanking];
			
		}
		
		
		
	?>

<br />

<?php
	$linha = $linhaSelecionada;
?>
	<tr class="textblk">
		<td nowrap><b>Nível de Conhecimento</b></td>
		<td>&nbsp;</td>
		<?php if(!($classificacaoRelatorio == "C" && $codigoLotacao == "-1")) {?>
		<td>&nbsp;</td>
		<?php }
			  if(!($classificacaoRelatorio == "C" && $codigoUsuario == "-1")) {?>
		<td>&nbsp;</td>
		<?php }?>
	</tr>

<?php 
	$totalGeral1 = 0;
	$totalGeral2 - 0;
	$totalGeral3 = 0;
	$totalCertaGeral1 = 0;
	$totalCertaGeral2 = 0;
	$totalCertaGeral3 = 0;
	$totalPerguntasGeral = 0;
	
	foreach($grupos as $grupo)
	{
		$totalGeral1 = $totalGeral1 + $grupo["total1"];
		$totalGeral2 = $totalGeral2 + $grupo["total2"];
		$totalGeral3 = $totalGeral3 + $grupo["total3"];
		$totalCertaGeral1 = $totalCertaGeral1 + $grupo["certas1"];
		$totalCertaGeral2 = $totalCertaGeral2 + $grupo["certas2"];
		$totalCertaGeral3 = $totalCertaGeral3 + $grupo["certas3"];
		$percentualGrupo1 = $grupo["certas1"] / $grupo["total1"] * 100;
		$percentualGrupo2 = $grupo["certas2"] / $grupo["total2"] * 100;
		$percentualGrupo3 = $grupo["certas3"] / $grupo["total3"] * 100;
		$nomeGrupo = $grupo["nome"];
		$totalPerguntas = $grupo["perguntas"];
		$totalPerguntasGeral = $totalPerguntasGeral + $totalPerguntas;
		
		if ($classificacaoRelatorio == "C") {
			if ($codigoLotacao == "-1") {
				$percentualGrupo3 = "&nbsp;";
			}
			if ($codigoUsuario == "-1") {
				$percentualGrupo2 = "&nbsp;";
			}
		}
		
?>

	<tr class="textblk" nowrap style="background: #ccc; font-weight: bold">
		<td style="border-top: 1px solid #000;"><b><?php echo $nomeGrupo;?></b></td>
		<td style="border-top: 1px solid #000;" align="center"><?php echo formatarValores($percentualGrupo1,2,"0,00"); ?></td>
		<?php if(!($classificacaoRelatorio == "C" && $codigoLotacao == "-1")) {?>
		<td style="border-top: 1px solid #000;" align="center"><?php echo formatarValores($percentualGrupo3,2,"0,00"); ?></td>
		<?php }
			  if(!($classificacaoRelatorio == "C" && $codigoUsuario == "-1")) {?>
		<td style="border-top: 1px solid #000;" align="center"><?php echo formatarValores($percentualGrupo2,2,"0,00"); ?></td>
		<?php }?>
	</tr>

<?php 
		if (count($grupo["itens"]) == 1) continue;
		foreach($grupo["itens"] as $linhaAssessment)
		{
			$percentualAssesment1 = $linhaAssessment["QD_CERTAS_1"] / $linhaAssessment["QD_TOTAL_1"] * 100;
			$percentualAssesment2 = $linhaAssessment["QD_CERTAS_2"] / $linhaAssessment["QD_TOTAL_2"] * 100;
			$percentualAssesment3 = $linhaAssessment["QD_CERTAS_3"] / $linhaAssessment["QD_TOTAL_3"] * 100;
			$nomeDisciplina = $linhaAssessment["DS_DISCIPLINA"];
?>	
	
		<tr class="textblk" nowrap>
			<td><?php echo $nomeDisciplina;?></td>
			<td align="center"><?php echo formatarValores($percentualAssesment1,2,"0,00"); ?></td>
			<?php if(!($classificacaoRelatorio == "C" && $codigoLotacao == "-1")) {?>
			<td align="center"><?php echo formatarValores($percentualAssesment3,2,"0,00"); ?></td>
			<?php }
				  if(!($classificacaoRelatorio == "C" && $codigoUsuario == "-1")) {?>
			<td align="center"><?php echo formatarValores($percentualAssesment2,2,"0,00"); ?></td>
			<?php }?>
		</tr>

<?php 
		}
	}


?>
<!--  	
	<tr class="textblk" nowrap style="background: #ccc; font-weight: bold">
		<td style="border-top: 1px solid #000;"><b>Nível de Conhecimento Geral</b></td>
		<td style="border-top: 1px solid #000;" align="center"><?php echo $totalPerguntasGeral;?></td>
		<td style="border-top: 1px solid #000;" align="center"><?php echo $mediaGeralProvasCalculada; ?></td>
	</tr>
-->	
</table>
<div>&nbsp;</div>

<div style="width:100%;text-align:center">
	<INPUT class="buttonsty" onclick="javascript:self.print()" type="button" value="Imprimir">
	<p class="textoInformativo">Favor definir o formato paisagem para fins de impressão</p>
</div>	
</body>
</html>