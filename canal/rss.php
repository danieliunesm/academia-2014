<?php
//include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";
include "../include/accesscounter.php";
include "../admin/framework/crud.php";
include "../admin/controles.php";

require "../include/inc.rss2array.php";

//obterSessaoEmpresa();
//$empresaID = isset($_SESSION["empresaID"])? $_SESSION["empresaID"] : 0;

$empresaID = isset($_GET['id']) ? $_GET['id'] : 0;
$programa  = isset($_GET['p'])  ? $_GET['p']  : '';

$sortby	   = isset($_GET['s'])  ? $_GET['s']  : '0';	// '0' -> none; '1' -> by title; '2' -> by date
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head><title>Canal de Comunica��o Colabor&aelig;</title>
<meta http-equiv=pragma content=no-cache>
<meta http-equiv="refresh" content="300">
<title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<style type="text/css">
body{
	margin:0;
	padding:0px;
	padding-right:214px;
	background:#f1f1f1;
}
#feedBody{
	width:auto;
	margin:0 10px 10px 10px;
	padding:20px 20px 20px 20px;
	border:1px solid #ccc;
	font-family:tahoma,arial,helvetica,sans-serif;
	background:#fff;
}
.entry{
	width:100%;
	font-size:11px;
	border:1px solid transparent;
}
.entry h3{
	font-size:14px;
	margin:10px 0 0 0;
}
.entry a{
	color:#069;
	text-decoration:none;
}
.entry a:hover{
	color:#000;
}
.lastUpdated{
	color:#999;
}
.lastUpdatedSort{
	display:none;
}
.feedEntryContent{
	margin:5px 0 0 0;
	line-height:1.4em;
	padding-bottom:10px;
	border-bottom:1px solid #ddd;
}
.last{
	border-bottom:none;
}
.feedSubtitleText{
	width:auto;
	font-size:18px;
	font-weight:bold;
	margin:10px 0 10px 0;
	border-bottom:2px solid #666;
}
#controlbox{
	position:fixed;
	width:200px;
	height:150px;
	border: 2px solid #666;
	background:#ffffee;
	right:10px;
	font-family:tahoma,arial,helvetica,sans-serif;
	font-size:11px;
}
#controlbox a{
	color: #000;
	text-decoration:none;
}
#controlbox p{
	width:auto;
	margin:10px 0 0 11px;
}
#controlbox div{
	width:auto;
	height:10px;
	margin:10px 0 0 10px;
	clear:both;
}
#controlbox div img{
	float:left;
	margin:3px 6px 0 0;
	border:none;
}
#controlbox div span{
	float:left;
}
#filtertext{
	 float:left;width:175px;height:23px;border:1px solid #069;margin:5px 5px 20px 10px;padding-top:4px;
}
#searchimg{
	 float:left;margin-top:7px;width:13px;height:7px;
}
#program{
	font-family:tahoma,arial,helvetica,sans-serif;
	font-size:16px;
	font-weight:bold;
}
#program span{
	float:left;
	margin-top:20px;
	padding-left:20px;
}
</style>
<script type="text/javascript" src="../include/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="../include/js/jquery.sort.js"></script>
<script type="text/javascript">
function filter(selector, query){   
	query  = $.trim(query);   
	query  = query.replace(/ /gi, '|');
	$itens = $(selector);
	$itens.show().addClass('visible');
	$itens.each(function(index){
		if(($('.feedEntryContent', $(this)).text() + $('.feedEntryTitle', $(this)).text()).search(new RegExp(query, "i")) < 0){
			$(this).hide().removeClass('visible');
		}
		else{
			$(this).show().addClass('visible');
		}
//		($(this).text().search(new RegExp(query, "i")) < 0) ? $(this).hide().removeClass('visible') : $(this).show().addClass('visible');
	});   
}

function sort(stype){
	switch(stype){
		case 1:
			$feeds.each(function(){
				$('.entry',this).sortElements(function(a, b){
					return $(a).find('a').text() > $(b).find('a').text() ? order1 : -order1;									// BY TITLE
				});
			});
			$('#bydate').css('font-weight', 'normal');
			$('#bytitle').css('font-weight', 'bold');
			$('#bytitle').find('img').attr('src', (order1 == 1 ? '../admin/images/arrow_up.gif' : '../admin/images/arrow_dw.gif'));
			order1 = -order1;
			break;
		case 2:
			$feeds.each(function(){
				$('.entry',this).sortElements(function(a, b){
					return $(a).find('.lastUpdatedSort').text() > $(b).find('.lastUpdatedSort').text() ? -order2 : order2;		// BY DATE DESC
				});
			});
			$('#bytitle').css('font-weight', 'normal');
			$('#bydate').css('font-weight', 'bold');
			$('#bydate').find('img').attr('src', (order2 == -1 ? '../admin/images/arrow_up.gif' : '../admin/images/arrow_dw.gif'));
			order2 = -order2;
			break;
	}
}

window.onload = function(){
	$('a').focus(function(){$(this).blur();});
	$feeds = $('.feedContent');
}
order1 = 1;
order2 = 1;
</script>
</head>
<body>
<div id="program">
<img src="images/empresas/boasvindas<?php echo $empresaID; ?>.png" height="60" style="margin:0 0 0 10px;float:left" />
<span>Programa: <?php echo $programa; ?></span>
<p style="margin:0;clear:both"></p>
</div>
<div id="controlbox">
	<p>Filtrar:</p>
	<input id="filtertext" type="text" onkeyup="filter('.entry', this.value)" /><!-- <img id="searchimg" src="../admin/images/bt_preview.gif" /> -->
	<p>Ordenar:</p>
	<div id="bytitle"><a href="javascript:void(0)" onclick="sort(1)"><img src="../admin/images/arrow_up.gif" /><span>Por T�tulo</span></a></div>
	<div id="bydate"><a href="javascript:void(0)" onclick="sort(2)"><img src="../admin/images/arrow_dw.gif" /><span>Por Data</span></a></div>
</div>

<div id="feedBody">
<?php
setlocale (LC_ALL, 'pt_BR');

function array_orderby(){
    $args = func_get_args();
    $data = array_shift($args);
    foreach($args as $n => $field){
        if(is_string($field)) {
            $tmp = array();
            foreach($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

function has_keywords($haystack, $wordlist){
	$found = false;
	foreach($wordlist as $w){
		if(stripos($haystack, $w) !== false){
			$found = true;
		}
	}
	return $found;
}

function rss_filter($rss, $keywords){
	$news = array();
	for($i = 0; $i < count($rss); $i++){
		if(has_keywords($rss[$i]['title'], $keywords) || has_keywords($rss[$i]['description'], $keywords)){
			$news[] = array(
				"title" => $rss[$i]['title'],
				"description" => $rss[$i]['description'],
				"link" => $rss[$i]['link'],
				"pubdate" => $rss[$i]['pubdate']
			);
		}
	}
	return $news;
}

$sql = "SELECT * FROM col_rss WHERE IN_ATIVO = 1 AND TRIM(url) <> '' AND CD_EMPRESA = $empresaID";
$query = mysql_query($sql) or die(ERROR_MSG_SQLQUERY . mysql_error());
while($oRs = mysql_fetch_assoc($query)){
	$url = $oRs['url'];
	$rss_array = rss2array($url);
	
	if(count($rss_array['channel']) == 0) continue;
	
	$rss_array['channel']['title'] = $oRs['titulo'];
	
	if(trim($oRs['pre_filtro']) != ''){
		$words = explode(",", $oRs['pre_filtro']);	// pre-filtro
	}
	else $words = array(" ");						// ALL
	
	$rss_filtered = rss_filter($rss_array['items'], $words);
	
	$rss_sorted = $rss_filtered;
	
	switch($sortby){
		case '1':	$rss_sorted = array_orderby($rss_filtered, 'title', SORT_ASC);					// by TITLE
					break;
		case '2':	$rss_sorted = array_orderby($rss_filtered, 'pubdate', SORT_DESC, SORT_NUMERIC);	// by DATE
					break;
	}
	
//	print "<pre>";
//	print_r($rss_sorted);
//	print "</pre>";
	
	echo "\t<div class=\"feedTitle\">\n";
	echo "\t\t<div class=\"feedTitleContainer\">\n";
//	echo "\t\t\t<h1 class=\"feedSubtitleText\">" . utf8_decode($rss_array['channel']['title']) . "</h1>\n";
	echo "\t\t\t<h1 class=\"feedSubtitleText\">" . $rss_array['channel']['title'] . "</h1>\n";
	echo "\t\t</div>\n";
	echo "\t</div>\n";
	
	echo "\t<div class=\"feedContent\">\n";
	
	for($i = 0; $i < count($rss_sorted); $i++){
		echo "\t\t<div class=\"entry\">\n";
		echo "\t\t\t<h3>\n";
		echo "\t\t\t\t<a href=\"" . $rss_sorted[$i]['link'] . "\" target=\"_blank\"><span class=\"feedEntryTitle\">" . utf8_decode($rss_sorted[$i]['title']) . "</span></a>\n";
		echo "\t\t\t</h3>\n";
		echo "\t\t\t\t<div class=\"lastUpdatedSort\">" . $rss_sorted[$i]['pubdate'] . "</div>\n";
		echo "\t\t\t\t<div class=\"lastUpdated\">" . date('D, d M Y H:i:s', $rss_sorted[$i]['pubdate']) . "</div>\n";
		$description = preg_replace("/<img[^>]+\>/i", " ", utf8_decode($rss_sorted[$i]['description']));
		$description = str_replace('<br/><br/>', '', $description);
		echo "\t\t\t<div xml:base=\"" . $url . "\" class=\"feedEntryContent" . ($i == (count($rss_sorted) - 1) ? ' last' : '') . "\">" . $description . "</div>\n";
		echo "\t\t</div>\n";
		echo "\t\t<div style=\"clear: both;\"></div>\n";
	}
	echo "\t</div>\n";
}

mysql_close();
?>
</div>
</body>
</html>
