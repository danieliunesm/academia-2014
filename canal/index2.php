<?php
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";
include "../include/accesscounter.php";
include('../admin/framework/crud.php');
include('../admin/controles.php');
include "include/codefunctions.php";

if ($_SESSION["urlOrigin"] != null){
	$cod = 'index2.php?'.$_SESSION["urlOrigin"];
	$_SESSION["urlOrigin"] = null;
	echo "<meta http-equiv=\"refresh\" content=\"0;URL=/canal/$cod\">";
	exit();
}

obterSessaoEmpresa();

$empresaID = isset($_SESSION["empresaID"])? $_SESSION["empresaID"] : 0;
$tipo = $_SESSION["tipo"];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Canal de Comunica��o Colabor&aelig;</title>
<meta http-equiv=pragma content=no-cache>
<link type="text/css" rel="stylesheet" media="all" href="abas.css">
<!--[if lte IE 8]><link type="text/css" rel="stylesheet" media="all" href="abasIE.css"><![endif]-->
<link rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">

<style type="text/css">
#global{width:980px;padding-bottom:1px;margin-left:auto;margin-right:auto;background:#fff}
.infoUsu{color:#fff;text-align:left; padding-left: 10px;}
.conteudo, div, td {
	font-size: 11px;
	font-family: tahoma,arial,helvetica,sans-serif;
	color: #333;

}
.conteudo .text{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.conteudo .textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.conteudo .textblk{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000;text-align:right;}
.conteudo .rodape{font-family:"tahoma","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#666666}
.conteudo .legenda{font-family:"arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.conteudo .legendablk{font-family:"arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.conteudo .assinatura{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;font-style:italic;color:#000000}
.conteudo .title{font-family:"tahoma","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}

.conteudo a{font-family:tahoma,arial,helvetica,sans-serif;font-size:11px;font-weight:normal;color:#000066;text-decoration:none}
.conteudo a:hover{color:#000066}
</style>

<script type="text/javascript" src="include/js/browser_faster.js"></script>
<script type="text/javascript" src="include/js/functions.js"></script>
<script type="text/javascript" src="include/js/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="include/js/jquery.iframe-auto-height.plugin.js"></script>
<script language="javascript" type="text/javascript">
function mudaAba(reference)
{
	
	var itens = document.getElementById("productTabs").childNodes;
	var qtdItens = itens.length;
	
	
	for(var i = 0; i < qtdItens; i++)
	{

		if (itens[i].tagName == "LI")
		{
			itens[i].className = "";
		}
		
	}
	
	this.className = "selected";
		
	itens = document.getElementById("tabsContent").childNodes;
	qtdItens = itens.length;
	
	for(i = 0; i < qtdItens; i++)
	{
		if (itens[i].style)
		{
			itens[i].style.display = "none";
		}
	}
	try{
		document.getElementById(reference).style.display = "block";
	}
	catch(e){}
}


var callingURL = document.URL;
if (callingURL.indexOf('?') > 0 ){
	var cgiString = callingURL.substring(callingURL.indexOf('?')+1,callingURL.length);
	if (cgiString.indexOf('#')!=-1){ 
	    cgiString=cgiString.slice(0,cgiString.indexOf('#')); 
	} 
	var arrayParams=cgiString.split('&'); 
	for (var i=0;i<arrayParams.length;i++){ 
	    eval(arrayParams[i].substring(0,arrayParams[i].indexOf('=')+1)+"\""+ 
	    arrayParams[i].substring(arrayParams[i].indexOf('=')+1,arrayParams
	     [i].length)+"\""); 
	}
	var cont = decode64(arrayParams[0]);
	t=setTimeout("mudaAba('"+cont+"')",1000);
}

</script>

</head>
<body style="background: #eaeaea; text-align:center;">
<div id="global">
	<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
	<tr style="background: #eaeaea;">
		<td width="50%" style="height: 51px;"><a href="index2.php"><img src="images/empresas/logocertificacaoTIM.png" height="51"></a></td>
		<td width="50%" style="text-align:right;"><?php echo getServerDate(); ?></td>
	</tr>
	<tr valign="middle" style="background: #2a5693;">
		<td width="50%" class="infoUsu">Usu�rio &raquo; <strong><?php echo ($_SESSION["empresaID"] == 34? $_SESSION['nome']: strtoupper($_SESSION['alias'])); ?></strong>
			<br />
			<strong>Programa: <?php echo $_SESSION['nomeEmpresa'];?></strong>
		</td>
		<td width="50%" style="text-align:right; padding: 9px; border-bottom:1px solid #999; width: 100%;">
			<a href="/logout.php" style="font-size:12px;font-weight:bold;font-family:arial,helvetica,sans-serif;margin-top:0;padding: 9px 15px;color: #fff;text-decoration: none;">Logout</a>
			<a href="'updatepass.php" style="font-size:12px;font-weight:bold;font-family:arial,helvetica,sans-serif;margin-top:0;padding: 9px 15px;color: #fff;text-decoration: none;">Alterar senha</a>
			<?php 
				if (($tipo > 4 || $tipo < 0) && $_SESSION["empresaID"] == 34)
				{
			?>
			<a href="cadastro.php" style="font-size:12px;font-weight:bold;font-family:arial,helvetica,sans-serif;color:#333;margin-top:0;padding: 9px 15px;color: #fff;text-decoration: none;">Cadastro R�pido</a>
			<?php 
				}
			?>
		</td>
	</tr>
	<tr valign="middle" style="padding: 10px;">
		<?php
		if($empresaID == 34){
		?>
			<td colspan="2" class="conteudo">
				<img src="images/empresas/logocertificacaoTIM.png" style="float:left;margin:0 20px 10px 0" />
				<?php
				obterTextoComentario(basename("/index",".php"));
				?>
			</td>
		<?php
		}else{
		?>
			<td colspan="2" class="conteudo" style="padding-left:60px">
				<?php
				obterTextoComentario(basename("/index",".php"));
				?>
			</td>
		<?php
		}
		?>
	</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" width="99%" align="center" style="background:transparent url(images/empresas/logo<?php echo $empresaID; ?>.png) no-repeat fixed center; margin-left:auto;margin-right:auto">
	<tr>
		<td >
			
			<div class="productDetails" style="width: 100%">
				<ul class="tabs" id="productTabs" style="font-size: 11px">
					<li onclick="javascript:mudaAba('tabCont01')" class="selected"><a>Descri��o<br /> do Programa</a></li>
					<li onclick="javascript:mudaAba('tabCont08')"><a>Conte�dos<br />&nbsp;</a></li>
					<li onclick="javascript:mudaAba('tabCont11')"><a>Blog<br />&nbsp;</a></li>
					<?php
						$existeForum = 0;
						$sql = "SELECT COUNT(1) AS TOTAL FROM col_foruns WHERE CD_EMPRESA = $empresaID AND IN_ATIVO = 1";
						$RS_query = DaoEngine::getInstance()->executeQuery($sql, true);
						if($oRs = mysql_fetch_row($RS_query))
						{
							$existeForum = $oRs[0];
						}
						mysql_free_result($RS_query);
						if ($existeForum > 0)
						{
					?>
					<li onclick="javascript:mudaAba('tabCont02')"><a>F�rum<br />&nbsp;</a></li>
					<?php
						}
					?>
					<li onclick="javascript:mudaAba('tabCont04')"><a>Simulados<br />&nbsp;</a></li>
					
					<?php
						$textoAvaliacoes = "Avalia��es"; 
						if ($empresaID == 34)
						{
							$textoAvaliacoes = "Certifica��o";
						}
					?>
					
					<li onclick="javascript:mudaAba('tabCont03')"><a><?php echo $textoAvaliacoes; ?><br />&nbsp;</a></li>
					
					<?php
						if ($tipo >= 4 || $tipo == -1) //Se administrador ou RH
						{
					?>
					<li onclick="javascript:mudaAba('tabCont05')"><a>Ger�ncia do<br />Programa</a></li>
					<?php
						}
						if ($tipo >= 4 || $tipo == -2) //Se administrador ou Gerente de Grupo
						{
					?>
					<li onclick="javascript:mudaAba('tabCont06')"><a>Radar do<br />Gerente</a></li>
					<?php
						}
						if ($tipo >= 4 || $tipo == -3) //Se administrador ou Gerente de Lota��o
						{
					?>
					<li onclick="javascript:mudaAba('tabCont10')"><a>Radar do <br />Gerente</a></li>
					<?php
						}
						
					?>
					<li onclick="javascript:mudaAba('tabCont07')"><a>Radar do<br />Participante</a></li>
					<li onclick="javascript:mudaAba('tabCont09')"><a>Fale Conosco<br />&nbsp;</a></li>
					
				</ul>
				<div class="boxR1">
					<div class="boxR12">
						<div class="boxR13">
							<div class="boxR14">
								<div class="activeTabs">
									<div class="contTabs" id="tabsContent" style=" margin-left:10px; margin-right:10px">
										<div id="tabCont01" class="conteudo">
											<?php
											include("descricao_programa.php");
											?>
										</div>
										<div id="tabCont08" style="display:none;" class="conteudo">
											<?php
											obterTextoComentario(basename("/sumario_artigo_video",".php"));;
											?>
										</div>
										<div id="tabCont02" style="display:none;" class="conteudo">
											<?php
												include("forum/lista_forum.php");
											?>
										</div>
										<div id="tabCont03" style="display:none;" class="conteudo">
											<?php
												include("certificacao/lista_prova.php");
											?>
										</div>
										<div id="tabCont04" style="display:none;" class="conteudo">
											<?php
												include("certificacao/lista_simulado.php");
											?>
										</div>
										<?php
											if ($tipo >= 4 || $tipo == -1) //Se administrador ou RH
											{
										?>
										<div id="tabCont05" style="display:none;" class="conteudo">
											<?php
												include("gestao_programa_rh.php");
											?>
										</div>
										<?php
											}
											if ($tipo >= 4 || $tipo == -2) //Se administrador ou Gerente de Grupo
											{
										?>
										<div id="tabCont06" style="display:none;" class="conteudo">
											<?php
												include("gestao_programa_gerente.php");
											?>
										</div>
										<?php
											}
											if ($tipo >= 4 || $tipo == -3) //Se administrador ou Gerente de Lota��o
											{
										?>
										<div id="tabCont10" style="display:none;" class="conteudo">
											<?php
												include("gestao_programa_gerente_lotacao.php");
											?>
										</div>
										<?php
											}
										?>
										<div id="tabCont07" style="display:none;" class="conteudo">
											<?php
												include("gestao_programa_participante.php");
											?>
										</div>
										<div id="tabCont09" style="display:none;" class="conteudo">
											<?php
												include("abafaleconosco.php");
											//obterTextoComentario(basename("/fale_conosco",".php"));;
											?>
										</div>
										<div id="tabCont11" style="display:none; width: 100%; height:100%; overflow: auto; background:transparent;" class="conteudo">
											<iframe allowtransparency="true" frameborder="0" style="background:transparent;"
												width="100%" height="1000"
												src="<?php echo "../blog/".$_SESSION['blogPrograma']?>"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</td>
	</tr>
	</table>
</div>

	<?php
	//verifica se o usu�rio � admin
	if ($tipo >= 4 || $_SESSION["tipoReal"] > 4)
	{
	?>
	<div style="width:100%;text-align:center"><input type="button" class="buttonsty" style="width:150px" value="Configura��es de Acesso" onclick="document.location.href='selecionarempresa.php'" onfocus="noFocus(this)"></div>
	<?php
	}
	?>
	
	<div id="footer" style="position:relative;top:0px;margin:20px auto 20px auto">
	<div class="hrblue"></div>
	<p>&copy; Colabor&aelig; <?php echo date("Y");?></p>
	</div>

</body>
</html>
<?
acertarSessaoEmpresa();
?>
