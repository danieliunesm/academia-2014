<?php
//include "../include/security.php";
//include "../include/genericfunctions.php";
//include("../include/defines.php");
//include('../admin/framework/crud.php');
//include('../admin/controles.php');
//include('../admin/page.php');
//include "../include/accesscounter.php";

$dataAtual = date("Ymd");
$dataFinal = date("d/m/Y");
$empresa = $_SESSION["empresaID"];
$dataInicio = "";
$dataInicial = $dataInicio;
$cicloSelecionado = (isset($_GET["id"])? $_GET["id"]:"");
$dataInicioTodosCiclos = "";
$dataTerminoTodosCiclos = "";

if ($cicloSelecionado == "")
	$cicloSelecionado = -1;
	
$codigoUsuario = $_SESSION["cd_usu"];

function formatarValoresAmericano($valor, $decimal = 2, $seVazio = "0.00")
{

    if ($valor == "")

    {

        return $seVazio;

    }

    $notaCentesimal = false;

    if (!$notaCentesimal) {

        $valor = $valor / 10;

    }

    return number_format(round($valor,$decimal), $decimal, ".", "");



}

?>
		<script language="javascript">
			function abreRelatorioPA()
			{

				cboUsuario = "<?php echo $codigoUsuario ?>";
				
				cboCiclo = document.getElementById("cboCicloPA");
				
				hdnCiclo = cboCiclo.value;
				dataInicio = cboCiclo.options[cboCiclo.selectedIndex].getAttribute("dataInicial");
				dataTermino = cboCiclo.options[cboCiclo.selectedIndex].getAttribute("dataFinal");
				filtro = "";
				cboEmpresa = <?php echo $empresa; ?>;
				
				
				if (document.getElementById("hdnParticipante") != null && document.getElementById("hdnParticipante").value > 0)
				{
					cboUsuario = document.getElementById("hdnParticipante").value;
				}

				if (document.getElementById("hdnParticipante") != null && !document.getElementById("hdnParticipante").value > 0)
				{
                    if (document.getElementById("cboTipoRelatorioPA").value.indexOf("relatorio_pesquisa.php") == -1){
                        alert("Selecione um participante para emiss�o do relat�rio.");
                        return;
                    }
				}
				
				rel = document.getElementById("cboTipoRelatorioPA").value;

				url = "relatorios/" + rel + "&txtDe=" + dataInicio + "&txtAte=" + dataTermino + "&cboUsuario=" + cboUsuario + "&hdnCiclo=" + hdnCiclo + "&cboEmpresa=" + cboEmpresa + "&id=-1";
				
				newWin=null;
				var w= 900;
				var h= 600;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				
				//alert(url);
				
				newWin=window.open(url,'relatorio','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
				
			}
			
			function procurarParticipante()
			{
				
				var participante = document.getElementById("txtParticipante").value;
				document.getElementById("hdnParticipante").value = "";
				document.getElementById("frmPesquisarUsuario").src = "selecao_participante.php?txtParticipante=" + participante;
				
			}
		
		</script>
			
		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="post">
					
			<TABLE cellpadding="0" cellspacing="0" width="100%" align="center" border="0">
				<TR>
					<TD width="100%" >

					<?php
						obterTextoComentario(basename("/gestao_programa_participante",".php"));
					
					?>

					<table cellpadding="0" cellspacing="0" width="100%" border="0">
						<tr>
							<td width="50%">
								Ciclos do Programa:
							</td>
					<?php
						if ($tipo >= 4 || $tipo == -2 || $tipo == -1 || $tipo == -3) //Se administrador ou Gerente ou RH (1==3)//
						{
					?>
							<td rowspan="10" valign="middle" style="padding-left: 13px; width: 50%">
								<table cellpadding="0" cellspacing="0" width="320">
									<tr>
										<td>Selecione o Participante:</td>
									</tr>
									<tr>
										<td>
											<input type="text" name="txtParticipante" id="txtParticipante" class="textbox3" style="width: 243px;" />
											<input type="button" class="buttonsty" style="width: 70px;" value="Procurar" onclick="javascript:procurarParticipante();" />
											<input type="hidden" name="hdnParticipante"	id="hdnParticipante" value="" />
										</td>
									</tr>
								</table>
								<iframe style="border: 1px solid #000000;" frameborder="0" width="320px" height="100px" id="frmPesquisarUsuario" name="frmPesquisarUsuario"></iframe>
							</td>
					<?php
						}else{

                            ?>
                            <td rowspan="10" valign="middle" style="padding-left: 13px; width: 50%">

                                <?
                                include('relatorios/relatorio_ranking_util.php');

                                $codigoCiclo = "-1";
                                $grupos = obterGruposDisciplinaAssessmentEmpresa($empresa);

                                foreach ($grupos as $codigoGrupo => $nomeGrupo) {

                                    $grupos[$codigoGrupo]["total1"] = 0;
                                    $grupos[$codigoGrupo]["certas1"] = 0;
                                    $grupos[$codigoGrupo]["total2"] = 0;
                                    $grupos[$codigoGrupo]["certas2"] = 0;
                                    $grupos[$codigoGrupo]["total3"] = 0;
                                    $grupos[$codigoGrupo]["certas3"] = 0;
                                    $grupos[$codigoGrupo]["itens"] = array();
                                    $grupos[$codigoGrupo]["perguntas"] = 0;

                                }

                                $dadosAssessment = obterDadosAssessmentHierarquia($empresa, $_SESSION["lotacaoID"], $codigoUsuario);

                                while($linhaAssessment = mysql_fetch_array($dadosAssessment))
                                {
                                    $codigoGrupo = $linhaAssessment["CD_GRUPO_DISCIPLINA"];
                                    $grupos[$codigoGrupo]["total1"] = $grupos[$codigoGrupo]["total1"] + $linhaAssessment["QD_TOTAL_1"];
                                    $grupos[$codigoGrupo]["certas1"] = $grupos[$codigoGrupo]["certas1"] + $linhaAssessment["QD_CERTAS_1"];
                                    $grupos[$codigoGrupo]["total2"] = $grupos[$codigoGrupo]["total2"] + $linhaAssessment["QD_TOTAL_2"];
                                    $grupos[$codigoGrupo]["certas2"] = $grupos[$codigoGrupo]["certas2"] + $linhaAssessment["QD_CERTAS_2"];
                                    $grupos[$codigoGrupo]["total3"] = $grupos[$codigoGrupo]["total3"] + $linhaAssessment["QD_TOTAL_3"];
                                    $grupos[$codigoGrupo]["certas3"] = $grupos[$codigoGrupo]["certas3"] + $linhaAssessment["QD_CERTAS_3"];
                                    //$grupos[$codigoGrupo]["perguntas"] = $grupos[$codigoGrupo]["perguntas"] + $linhaAssessment["NR_QTD_PERGUNTA_TOTAL"];
                                    $grupos[$codigoGrupo]["itens"][] = $linhaAssessment;
                                    //$totalQuestoes = $totalQuestoes + $linhaAssessment["NR_QTD_PERGUNTA_TOTAL"];
                                }

                                $stringGrafico = "";

                                foreach($grupos as $grupo)
                                {


                                    $percentualGrupo1 = $grupo["certas1"] / $grupo["total1"] * 100;
                                    $percentualGrupo2 = $grupo["certas2"] / $grupo["total2"] * 100;
                                    $percentualGrupo3 = $grupo["certas3"] / $grupo["total3"] * 100;
                                    $nomeGrupo = $grupo["nome"];

                                    if($nomeGrupo=="") continue;

                                    $valorIndividual = formatarValoresAmericano($percentualGrupo2);
                                    $valorGrupo = formatarValoresAmericano($percentualGrupo3);
                                    $valorEmpresa = formatarValoresAmericano($percentualGrupo1);
                                    $stringGrafico = "$stringGrafico;$nomeGrupo;$valorIndividual;$valorGrupo;$valorEmpresa";

                                }

                                //echo $stringGrafico;
                                $stringGrafico = urlencode($stringGrafico);

                                ?>

                                <img src="relatorios/imageconfig.php?width=550&title=Plano de Aprendizado&parametro=<?php echo $stringGrafico;?>&ymax=11" />

                            </td>

                    <?php
                        }
					?>
						</tr>
						<tr>
							<td>
								<?php
									comboboxCiclo("cboCicloPA", -1, $empresa)								
								?>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								Tipos de Relat�rios:
							</td>
						</tr>
						<tr>
							<td>
								<?php
									if ($empresa == 34)
									{
										comboboxTipoRelatorioIndividualTim("cboTipoRelatorioPA", "");
									}else{
										comboboxTipoRelatorioIndividual("cboTipoRelatorioPA", "", $somenteAssessment);
									}
								
								
									
								?>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<input type="button" class="buttonsty" value="Visualizar Relat�rio" onclick="javascript:abreRelatorioPA();" />
							</td>
						</tr>
					</table>


					</TD>
				</TR>

				
			</TABLE>
		</FORM>
		
