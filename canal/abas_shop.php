<link href="abas.css" type="text/css" rel="stylesheet" media="all">


<!-- Inicio do componente ABAS -->
<div class="productDetails">
	<ul class="list4" id="productTabs">
				<li><a href="#tabCont01" rel="tabCont01">Descri��o Geral</a></li>
			<li><a href="#tabCont02" rel="tabCont02">Informa��es Adicionais</a></li>
		</ul>
	
	<div class="box1">
		<div class="activeTabs">
			<ul class="contTabs" id="tabsContent">
					<li id="tabCont01">
					<table>
					  <thead>
					    <tr>
					      <th>
						   <h2 class="title1"><span>Descri��o Geral</span></h2>
					      </th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>					      				      
					     							 <p><strong>TV 47" Led Full HD - 47SL90QD - (1.920x1.080 pixels) - com Conversor Digital Integrado, 3 Entradas HDMI e Entrada USB - LG + TV 26" LCD 26LH20R com 2 entradas HDMI, HDTV Ready - LG </strong><br><br>

Com essas TVs de 47 e 26 polegadas voc� n�o ir� perder seus programas preferidos! Voc� pode coloc�-las em diferentes c�modos da sua casa. Mais entreterimento e informa��o para voc� com a garantia de qualidade dos produtos LG. Confira!<br>


<strong>TV 47":</strong><br>
A nova linha de TVs LIVE BORDERLESS traz para a realidade o sonho de uma TV �nica. O design sem moldura externa garante uma experi�ncia �tima com imagens perfeitas e v�o revolucionar o seu jeito de assistir TV. A nova Live Borderless ainda vem com Bluetooth e fun��o Time Machine Ready para voc� gravar em um HD externo.<br><br>

A revolucion�ria tecnologia de ilumina��o LED traz a impressionante taxa de contraste de 3.000.000:1. S�o cores mais reais e tons de preto muito mais profundos que proporcionam uma imagem �nica. Muito mais rapidez no acendimento das l�mpadas (processo de dimming) iluminando somente os pixels que ir�o formar a imagem.<br><br>

<strong>Diferenciais:</strong><br>
- TruMotion (120Hz): freq��ncia de 120hz, o dobro da frequ�ncia dos televisores convencionais.
S�o 120 quadros por segundo, o que garante melhor qualidade nas cenas de velocidade.<br>
- USB 2.0: reproduz v�deos em alta defini��o fotos e m�sicas.<br>
- Divx HD: reproduz DivX em Alta Defini��o atrav�s de um USB ou HD externo.<br>
- DTV: Conversor Digital integrado(ISDB-TB)<br>
- Energy Saving: Fun��o que proporciona economia de energia atrav�s do ajuste do n�vel do brilho da TV.<br>
- Intelligent Sensor: ajuste autom�tico da imagem da TV conforme a luz do ambiente. Permite economizar at� 69,5% de energia.<br>
- Tecnologia IPS: tecnologia que permite um maior �ngulo de vis�o, al�m de garantir melhor qualidade em cenas de velocidade.<br>
- 3 Conex�es HDMI<br><br>

<strong>TV 26":</strong><br>
Tecnologia e precis�o nos detalhes, design surpreendente! Com essa TV LCD 26" voc� usufrui da qualidade HDTV para imagens muito mais reais, megacontraste para ver todos os detalhes nos seus programas favoritos e outro detalhe que faz a diferen�a: o invisible speaker. S�o alto-falantes invis�veis projetados pelo renomado especialista em som Mark Levinson.<br>
Com a fun��o HDMI voc� pode conectar a TV com seus aparelhos favoritos usando apenas um controle remoto. E ainda pode escolher quanto de energia quer economizar, alterando as configura��es de imagem. Muito mais conectividade e economia na sua divers�o, aproveite!<br><br>

<strong>Energy Saving</strong><br>
Fun��o que proporciona economia de energia atrav�s do ajuste do n�vel do brilho da TV<br><br>

<strong>Tecnologia IPS</strong><br>
Tecnologia que permite um maior �ngulo de vis�o, al�m de garantir melhor qualidade em cenas de velocidade<br><br>

<strong>Quick Menu</strong><br>
Menu de acesso r�pido que possibilita ajuste: Formato de tela/Ajuste de imagem/Closed caption/Sleep Timer/Canais favoritos/Energy saving<br><br>

<strong>Energy Saving</strong><br>
Com essa tecnologia � poss�vel controlar o consumo de energia atrav�s com o ajuste do n�vel de brilho. Em ambientes escuros, por exemplo, � poss�vel reduzir o brilho e garantir redu��o do consumo. O uso da fun��o � muito simples, basta acionar o bot�o "verde" no controle remoto<br><br>

<strong>Tecnologia IPS</strong><br>
Os televisores da LG usam painel IPS. O painel � o principal componente dos televisores de tela fina (� a tela da TV). As principais vantagens do painel IPS s�o: melhor reprodu��o e fidelidade das
cores em qualquer �ngulo de vis�o; baixo tempo de resposta, ideal para cenas em movimento, al�m da
confiabilidade do painel que � mais resistente em fun��o do alinhamento horizontal dos cristais.<br><br>

<strong>Quick Menu</strong><br>
Menu de acesso r�pido que possibilita ajuste: Formato de tela/Ajuste de imagem/Closed caption/Sleep Timer/Canais favoritos/Energy saving<br><br><br>


</p>
					     					      </td>
					    </tr>
					  </tbody>
					</table>
				</li>
					<li id="tabCont02">
					<table>
					  <thead>
					    <tr>
					      <th>
						   <h2 class="title1"><span>Informa��es Adicionais</span></h2>
					      </th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>					      				      
					     					     									<p>Observa��es: A Shoptime n�o se responsabiliza pela montagem/ instala��o dos produtos. Verifique com os fabricantes do produto e de seus componentes eventuais limita��es � utiliza��o de todos os recursos e funcionalidades</p>
														
			                					     	<ul class="listProductTabs">
					     							     			<li>
  			                		<h3 class="title8">TV 47" Led Full HD - 47SL90QD - (1.920x1.080 pixels) - com Conversor  Digital Integrado, 3 Entradas HDMI e Entrada USB - LG</h3>
  			                						                		<ul class="listProductDetails">
  			                							                           <li>Marca: LG</li>
			                							                           <li>Modelo: 47SL90QD</li>
			                							                           <li>Tipo de Televisor: LCD</li>
			                							                           <li>Tamanho da Tela  (polegadas): 47"</li>
			                							                           <li>Diagonal Visual Aproximada (cm): 119,3cm</li>
			                							                           <li>Conex�es: 2 entradas v�deo componente, 2 entradas �udio e v�deo (1 lateral), 1 entrada USB 2.0 PLUS Compat�vel com DivX HD, 3 entradas HDMI (1 lateral), 1 sa�da digital (�ptica), 1 entrada RGB (15 pinos), 1 entrada de �udio PC, 1 entrada RS-232 1 entrada para TV a Cabo e 1 entrada RF para TV aberta (Digital e Anal�gico)</li>
			                							                           <li>Voltagem: Bivolt</li>
			                							                           <li>Consumo: 175 W / 1W em stand by</li>
			                							                           <li>Conte�do da Embalagem: TV, controle, cabo de for�a e manual</li>
			                							                           <li>Dimens�es aproximadas do produto (cm) - AxLxP: Com base: 75,7x111,9x28,6cm / Sem base: 69,1x111,9x2,9cm</li>
			                							                           <li>Peso l�q. aproximado do produto (kg): Com base: 28,5kg / Sem base 24kg</li>
			                							                           <li>Garantia do Fornecedor: 12 meses</li>
			                							                           <li>Fornecedor: LG</li>
			                							                           <li>Controle remoto: Sim</li>
			                							                           <li>Idiomas do Menu: Portugu�s, ingl�s e espanhol</li>
			                							                           <li>Timer On/Off: Sim</li>
			                							                           <li>Sleeptimer: Sim</li>
			                							                           <li>Canais: 181</li>
			                							                           <li>Bloqueio de Canais: Sim</li>
			                							                           <li>Sistema de cores: PAL-M, N, NTSC e ISDB-TB</li>
			                							                           <li>Resolu��o: 1920x1080p</li>
			                							                           <li>Contraste: 3.000.000:1</li>
			                							                           <li>HDTV: Sim</li>
			                							                           <li>HDMI: Sim</li>
			                							                           <li>Full HD: Sim</li>
			                							                           <li>Progressive Scan: Sim</li>
			                							                           <li>Zoom: Sim</li>
			                							                           <li>Pot�ncia de �udio: 20W RMS</li>
			                							                           <li>Som est�reo: Sim</li>
			                							                           <li>SAP: Sim</li>
			                							                           <li>Closed Caption: Sim</li>
			                							                           <li>SAC: 4004-5400 (Capitais e Regi�es Metropolitanas) / 0800 707-5454 (Demais Localidades)</li>
			                							                		</ul>
			                											</li>
			                						     			<li>
  			                		<h3 class="title8">TV 26" LCD 26LH20R com 2 entradas HDMI, HDTV Ready - LG</h3>
  			                						                		<ul class="listProductDetails">
  			                							                           <li>Marca: LG</li>
			                							                           <li>Modelo: LCD 26LH20R</li>
			                							                           <li>Tipo de Televisor: LCD</li>
			                							                           <li>Tamanho da Tela  (polegadas): 26"</li>
			                							                           <li>Diagonal Visual Aproximada (cm): 66,04 cm</li>
			                							                           <li>Conex�es: 1 Entrada v�deo componente; 1 Entrada �udio e v�deo; 1 Sa�da �udio e v�deo; 2 Entradas HDMI; 1 Entrada RGB (para PC); 1 Entrada de �udio PC; 1 Entrada USB (service): entrada exclusiva para atualiza��o de software; 1 Entrada RS-232C; 1 Antena RF</li>
			                							                           <li>Voltagem: Bivolt</li>
			                							                           <li>Consumo: Stand-by: 1W</li>
			                							                           <li>Conte�do da Embalagem: TV LCD, Controle remoto, Pilhas, Cabo de for�a, Manual em portugu�s e Flanela</li>
			                							                           <li>Dimens�es aproximadas do produto (cm) - AxLxP: Sem base: 44,3x67x8cm / Com base: 50,3x67x20cm</li>
			                							                           <li>Peso l�q. aproximado do produto (kg): Sem base: 7,5Kg / Com base: 8,6Kg</li>
			                							                           <li>Garantia do Fornecedor: 12 meses</li>
			                							                           <li>Fornecedor: LG</li>
			                							                           <li>Controle remoto: Sim</li>
			                							                           <li>Idiomas do Menu: portugu�s; ingl�s; espanhol</li>
			                							                           <li>Timer On/Off: Sim</li>
			                							                           <li>Sleeptimer: Sim</li>
			                							                           <li>Canais: 181 Canais</li>
			                							                           <li>Bloqueio de Canais: Sim</li>
			                							                           <li>Modo Hotel: N�o</li>
			                							                           <li>Sistema de cores: PAL-M / N / NTSC</li>
			                							                           <li>Resolu��o: 1366 x 768 pixels</li>
			                							                           <li>Contraste: 60.000:1</li>
			                							                           <li>Brilho: 500 cd/m2</li>
			                							                           <li>HDTV: Sim</li>
			                							                           <li>HDMI: Sim</li>
			                							                           <li>Full HD: N�o</li>
			                							                           <li>Progressive Scan: Sim</li>
			                							                           <li>Zoom: Sim</li>
			                							                           <li>Pot�ncia de �udio: 14 W RMS</li>
			                							                           <li>Som est�reo: Sim</li>
			                							                           <li>SAP: Sim</li>
			                							                           <li>Closed Caption: Sim</li>
			                							                           <li>SAC: 4004-5400 (Capitais e Regi�es Metropolitanas) / 0800 707-5454 (Demais Localidades)</li>
			                							                		</ul>
			                											</li>
			                								</ul>
					     						     					      </td>
					    </tr>
					  </tbody>
					</table>
				</li>
				</ul>
		</div>
	</div>
</div>
<!-- Fim do componente ABAS  -->
