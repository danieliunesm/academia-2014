<?php 
include "../../include/security.php";
include "../../include/cripto.php";
include("../../include/defines.php");
include('../../admin/framework/crud.php');

$mensagem = "";

function autorizar()
{
	
	global $mensagem;
	$login = $_POST["txtLogin"];
	$senha = $_POST["txtSenha"];
	$empresa = $_SESSION["empresaID"];
	$codigoUsuarioLogado = $_SESSION["cd_usu"];
	$codigoAutorizador = 'null';
	
	$senhaGenerica = obterSenhaGenerica($empresa);
	$usuarioAutorizadoGenerico = false;
	
	$sql = "SELECT CD_USUARIO, lotacao, IN_SENHA_ALTERADA FROM col_usuario WHERE (tipo < 0 || tipo > 3) AND login = '$login' AND senha = '" . Cripto($senha,"C") . "' AND empresa = $empresa";  
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	
	if (mysql_num_rows($resultado) == 0)
	{
		//Verifica se usu�rio utilizou senha gen�rica
		if ($senhaGenerica == $senha)
		{
			//Verifica se usu�rio n�o � participante
			$sql = "SELECT CD_USUARIO FROM col_usuario WHERE empresa = $empresa AND login = '$login' AND tipo = 1";
			$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
			if (mysql_num_rows($resultado) > 0) //Se � participante, bloqueia
			{
				$mensagem = "Usu�rio e Senha inv�lidos para autoriza��o";
				return;
			}
			
			$usuarioAutorizadoGenerico = true;
			
		}
		else
		{
			$mensagem = "Usu�rio e Senha inv�lidos para autoriza��o";
			return;
			
		}
		
	}
	
	if (!$usuarioAutorizadoGenerico)
	{

		$linha = mysql_fetch_array($resultado);
		
		$codigoAutorizador = $linha["CD_USUARIO"];
		
		//if ($linha["IN_SENHA_ALTERADA"] == 0)
		//{
		//	$mensagem = "� necess�rio que o autorizador altere a senha padr�o antes de autorizar";
		//	return;
		//}
		
		$lotacaoSuper = $linha["lotacao"];
		
		$sql = "SELECT CD_USUARIO FROM col_usuario WHERE lotacao IN (SELECT CD_LOTACAO FROM col_lotacao WHERE DS_LOTACAO_HIERARQUIA LIKE '%$lotacaoSuper%') AND CD_USUARIO = $codigoUsuarioLogado";
	
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		
		if (mysql_num_rows($resultado) == 0)
		{
			$mensagem = "O autorizador n�o possui permiss�o para o usu�rio solicitado";
			return;
		}
		
	}
	
	$inSenhaGenerica = ($usuarioAutorizadoGenerico?1:0);
	
	$sql = "INSERT INTO col_autorizacao_prova 	(CD_USUARIO_AUTORIZADOR, CD_USUARIO_AUTORIZADO, CD_PROVA, DT_AUTORIZACAO, CD_LOGIN_AUTORIZADOR, IN_SENHA_GENERICA)
				VALUES							($codigoAutorizador, $codigoUsuarioLogado, {$_REQUEST["id"]}, sysdate(), '$login', $inSenhaGenerica)";
	
	DaoEngine::getInstance()->executeQuery($sql,true);
	
	//Deixa autorizar
	$_SESSION["avaliacaoAutorizada"] = $_REQUEST["id"];
	
	//Colocar para gravar na tabela col_autorizacao_prova;
	
}

function obterSenhaGenerica($codigoEmpresa)
{
	return "timestra";
}

function scriptAutorizar()
{
	
	?>
	<script type="text/javascript">
		window.returnValue = <?php echo $_SESSION["avaliacaoAutorizada"]; ?>;
		window.close();
	</script>
	<?php 
	
}

if (count($_POST) > 0)
{
	autorizar();
}

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Canal de Comunica��o Colabor&aelig;</title>
<meta http-equiv=pragma content=no-cache>
<link rel="stylesheet" type="text/css" href="../include/css/admincolaborae.css">

<style type="text/css">
.conteudo, div, td {
	font-size: 11px;
	font-family: tahoma,arial,helvetica,sans-serif;
	color: #333;
	text-align: center;

}
.conteudo .text{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.conteudo .textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.conteudo .textblk{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.conteudo .rodape{font-family:"tahoma","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#666666}
.conteudo .legenda{font-family:"arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.conteudo .legendablk{font-family:"arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.conteudo .assinatura{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;font-style:italic;color:#000000}
.conteudo .title{font-family:"tahoma","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}

.conteudo a{font-family:tahoma,arial,helvetica,sans-serif;font-size:11px;font-weight:normal;color:#000066;text-decoration:none}
.conteudo a:hover{color:#000066}

</style>
<script type="text/javascript" src="../include/js/functions.js"></script>
<?php 
if ($_SESSION["avaliacaoAutorizada"] == $_REQUEST["id"])
{
	scriptAutorizar();
}

?>

<base target="_self" />
</head>
<body>

<form name="frmAutorizacao" method="post">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td class="textblk" style="text-align: center">
				Para iniciar este diagn�stico � necess�rio que seja autorizado.
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Login/CPF do Autorizador:</td>
		</tr>
		<tr>
			<td><input type="text" maxlength="20" name="txtLogin" id="txtLogin" value="<?php echo $_POST["txtLogin"]?>" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Senha do Autorizador:</td>
		</tr>
		<tr>
			<td style="text-align: center">
				<input type="password" maxlength="8" name="txtSenha" id="txtSenha" />
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align: center">
				<input class="buttonsty" type="submit" value="Autorizar Diagn�stico" />
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="textblk" style="color: red">
				<?php echo $mensagem;?>
			</td>
		</tr>
	</table>
</form>
</body>
</html>