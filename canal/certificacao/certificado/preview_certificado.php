<?php
require('fpdf.php');

$pdf=new FPDF("L","mm","A4");
$pdf->AddFont('E111Viva','','e111viva.php');
$pdf->AddPage();

$logo1				= isset($_POST['logo1'])?(trim($_POST['logo1'])!=''?$_POST['logo1']:"images/layout/blank.png"):"images/layout/blank.png";
$logo1Dx			= isset($_POST['logo1Dx'])?$_POST['logo1Dx']:0;
$logo1Dy			= isset($_POST['logo1Dy'])?$_POST['logo1Dy']:0;
$logo1W				= isset($_POST['logo1W'])?$_POST['logo1W']:0;
$logo1H				= isset($_POST['logo1H'])?$_POST['logo1H']:0;

$logo2				= isset($_POST['logo2'])?(trim($_POST['logo2'])!=''?$_POST['logo2']:"images/layout/blank.png"):"images/layout/blank.png";
$logo2Dx			= isset($_POST['logo2Dx'])?$_POST['logo2Dx']:0;
$logo2Dy			= isset($_POST['logo2Dy'])?$_POST['logo2Dy']:0;
$logo2W				= isset($_POST['logo2W'])?$_POST['logo2W']:0;
$logo2H				= isset($_POST['logo2H'])?$_POST['logo2H']:0;

$certificadoDy		= isset($_POST['certificadoDy'])?$_POST['certificadoDy']:0;

$marcadagua			= isset($_POST['marcadagua'])?(trim($_POST['marcadagua'])!=''?$_POST['marcadagua']:"images/layout/blank.png"):"images/layout/blank.png";
$marcadaguaDx		= isset($_POST['marcadaguaDx'])?$_POST['marcadaguaDx']:0;
$marcadaguaDy		= isset($_POST['marcadaguaDy'])?$_POST['marcadaguaDy']:0;
$marcadaguaW		= isset($_POST['marcadaguaW'])?$_POST['marcadaguaW']:0;
$marcadaguaH		= isset($_POST['marcadaguaH'])?$_POST['marcadaguaH']:0;

$txt1				= isset($_POST['txt1'])?($_POST['txt1']):"";
$txt1Dy				= isset($_POST['txt1Dy'])?$_POST['txt1Dy']:0;

$linhaDy			= isset($_POST['linhaDy'])?$_POST['linhaDy']:0;
$nome				= isset($_POST['nome'])?$_POST['nome']:"";

$txt2				= isset($_POST['txt2'])?($_POST['txt2']):"";
$txt2Dy				= isset($_POST['txt2Dy'])?$_POST['txt2Dy']:0;

$programa			= isset($_POST['programa'])?($_POST['programa']):"";
$programaDy			= isset($_POST['programaDy'])?$_POST['programaDy']:0;

$curso				= isset($_POST['curso'])?($_POST['curso']):"";
$cursoDy			= isset($_POST['cursoDy'])?$_POST['cursoDy']:0;

$txt3				= isset($_POST['txt3'])?($_POST['txt3']):"";
$txt3Dy				= isset($_POST['txt3Dy'])?$_POST['txt3Dy']:0;

$data				= isset($_POST['data'])?$_POST['data']:"";
$dataDy				= isset($_POST['dataDy'])?$_POST['dataDy']:0;

$medalhaDx			= isset($_POST['medalhaDx'])?$_POST['medalhaDx']:0;
$medalhaDy			= isset($_POST['medalhaDy'])?$_POST['medalhaDy']:0;
$medalhaStatus		= isset($_POST['medalhaStatus'])?$_POST['medalhaStatus']:0;

$conjunto1Dx		= isset($_POST['conjunto1Dx'])?$_POST['conjunto1Dx']:0;
$conjunto1Dy		= isset($_POST['conjunto1Dy'])?$_POST['conjunto1Dy']:0;
$assinatura1		= isset($_POST['assinatura1'])?(trim($_POST['assinatura1'])!=''?$_POST['assinatura1']:"images/layout/blank.png"):"images/layout/blank.png";
$assinatura1Dx		= isset($_POST['assinatura1Dx'])?$_POST['assinatura1Dx']:0;
$assinatura1Dy		= isset($_POST['assinatura1Dy'])?$_POST['assinatura1Dy']:0;
$assinatura1W		= isset($_POST['assinatura1W'])?$_POST['assinatura1W']:0;
$assinatura1H		= isset($_POST['assinatura1H'])?$_POST['assinatura1H']:0;
$nome1				= isset($_POST['nome1'])?$_POST['nome1']:"";
$cargo1				= isset($_POST['cargo1'])?$_POST['cargo1']:"";

$conjunto2Dx		= isset($_POST['conjunto2Dx'])?$_POST['conjunto2Dx']:0;
$conjunto2Dy		= isset($_POST['conjunto2Dy'])?$_POST['conjunto2Dy']:0;
$assinatura2		= isset($_POST['assinatura2'])?(trim($_POST['assinatura2'])!=''?$_POST['assinatura2']:"images/layout/blank.png"):"images/layout/blank.png";
$assinatura2Dx		= isset($_POST['assinatura2Dx'])?$_POST['assinatura2Dx']:0;
$assinatura2Dy		= isset($_POST['assinatura2Dy'])?$_POST['assinatura2Dy']:0;
$assinatura2W		= isset($_POST['assinatura2W'])?$_POST['assinatura2W']:0;
$assinatura2H		= isset($_POST['assinatura2H'])?$_POST['assinatura2H']:0;
$nome2				= isset($_POST['nome2'])?$_POST['nome2']:"";
$cargo2				= isset($_POST['cargo2'])?$_POST['cargo2']:"";

$nu_certificado		= isset($_POST['nu_certificado'])?$_POST['nu_certificado']:"";

$programa = stripcslashes($programa);
$curso	  = stripcslashes($curso);
$txt1	  = stripcslashes($txt1);
$txt2	  = stripcslashes($txt2);
$txt3	  = stripcslashes($txt3);

$pdf->Image("images/layout/frameborder.png",12,10,272.8,188.81);
$pdf->Image($logo1,30+$logo1Dx,30+$logo1Dy,$logo1W,$logo1H);
$pdf->Image($logo2,218+$logo2Dx,28+$logo2Dy,$logo2W,$logo2H);
$pdf->SetFont("Times","B",30);
$pdf->SetXY(148,42+$certificadoDy);
$pdf->Cell(1,10,"CERTIFICADO",0,0,"C");

$pdf->Image($marcadagua,60+$marcadaguaDx,87+$marcadaguaDy,$marcadaguaW,$marcadaguaH);

$pdf->SetFont("Times","I",18);
$pdf->SetXY(10,57+$txt1Dy);
$pdf->MultiCell(0,9,$txt1,0,"C");

$pdf->SetDrawColor(204);
$pdf->SetLineWidth(0.2);
$pdf->Line(78, 83+$linhaDy, 218, 83+$linhaDy);

$pdf->SetFont("E111Viva","",38);
$pdf->SetXY(148,72+$linhaDy);
$pdf->Cell(1,10,$nome,0,0,"C");

$pdf->SetFont("Times","I",18);
$pdf->SetXY(10,87+$txt2Dy);
$pdf->MultiCell(0,9,$txt2,0,"C");

$pdf->SetFont("Times","BI",20);
$pdf->SetXY(10,101+$programaDy);
$pdf->MultiCell(0,8,$programa,0,"C");

$pdf->SetFont("Times","BI",22);
$pdf->SetXY(10,120+$cursoDy);
$pdf->MultiCell(0,9,$curso,0,"C");

$pdf->SetFont("Times","I",16);
$pdf->SetXY(10,134+$txt3Dy);
$pdf->MultiCell(0,6,$txt3,0,"C");

$pdf->SetFont("Times","I",16);
$pdf->SetXY(10,156+$dataDy);
$pdf->MultiCell(0,6,$data,0,"C");

if($medalhaStatus)
	$pdf->Image("images/layout/medalha_comlouvor.png",235+$medalhaDx,90+$medalhaDy,25.82,49.45);

$pdf->Image($assinatura1,42+$assinatura1Dx+$conjunto1Dx,158+$assinatura1Dy+$conjunto1Dy,$assinatura1W,$assinatura1H);
$pdf->SetXY(68+$conjunto1Dx,172+$conjunto1Dy);
$pdf->SetFont("Times","I",12);
$pdf->Cell(1,4,$nome1,0,0,"C");
$pdf->Ln();
$pdf->SetX(68+$conjunto1Dx);
$pdf->SetFont("Times","",12);
$pdf->Cell(1,4,$cargo1,0,0,"C");

$pdf->Image($assinatura2,202+$assinatura2Dx+$conjunto2Dx,158+$assinatura2Dy+$conjunto2Dy,$assinatura2W,$assinatura2H);
$pdf->SetXY(228+$conjunto2Dx,172+$conjunto2Dy);
$pdf->SetFont("Times","I",12);
$pdf->Cell(1,4,$nome2,0,0,"C");
$pdf->Ln();
$pdf->SetX(228+$conjunto2Dx);
$pdf->SetFont("Times","",12);
$pdf->Cell(1,4,$cargo2,0,0,"C");

$pdf->SetFont("Times","",12);
$pdf->SetXY(148,184);
$pdf->Cell(1,4,$nu_certificado,0,0,"C");

$pdf->Output();

//echo "$txt1<br>$txt2<br>$txt3<br>$programa<br>$curso";
?>