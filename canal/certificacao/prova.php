<?php

include "../../include/security.php";
//page::$usuario = $_SESSION("CD_USU");
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/controles.php');
include('../../admin/page.php');

class parametrosProva{
	
	static $col_prova;
	static $visualizar;
	static $nota;
	static $somenteInformacao = "";
	static $labelAvaliacao = "Avalia��o";
	
}

$_SESSION["avaliacaoAutorizada"] = "";

$usuario = page::$usuario;

if ($_SESSION["empresaID"] == 34)
{
	parametrosProva::$labelAvaliacao = "Certifica��o";
}

parametrosProva::$labelAvaliacao = "Teste";

$sqlWhereTipoProva = "((((pa.DT_TERMINO IS NULL AND (DT_INICIO IS NULL OR DATE_ADD(DT_INICIO, INTERVAL c.DURACAO_PROVA *(60) + 5 SECOND) >= sysdate()))
								OR (VL_MEDIA_APROVACAO > VL_MEDIA AND NR_TENTATIVA_USUARIO = 1 AND IN_PRIMEIRA_CHAMADA = 2)
								) AND c.IN_PROVA = 1
								  AND NOT EXISTS (SELECT 1 FROM col_prova_aplicada pa1 INNER JOIN col_prova p1 ON p1.CD_PROVA = pa1.CD_PROVA WHERE pa1.CD_USUARIO = u.CD_USUARIO AND pa1.CD_PROVA = c.CD_PROVA_SUPERIOR AND COALESCE(pa1.VL_MEDIA,0) >= p1.VL_MEDIA_APROVACAO)
								))";


$sql = "SELECT DISTINCT
				IN_PROVA, DT_TERMINO, DATE_ADD(DT_INICIO, INTERVAL c.DURACAO_PROVA *(60) + 5 SECOND) >= sysdate() as RESTA_TEMPO, IN_SENHA_MESTRA
				 FROM col_prova c INNER JOIN col_prova_chamada pc ON pc.CD_PROVA = c.CD_PROVA INNER JOIN col_prova_vinculo pv ON pv.CD_PROVA = c.CD_PROVA INNER JOIN col_usuario u ON u.empresa = pv.CD_VINCULO AND pv.IN_TIPO_VINCULO = 1 LEFT JOIN col_prova_aplicada pa on pa.CD_PROVA = c.CD_PROVA AND pa.CD_USUARIO = u.CD_USUARIO WHERE (u.CD_USUARIO = $usuario ) AND c.IN_ATIVO = 1 AND c.CD_PROVA = {$_GET["id"]}
				AND CONCAT(DATE_FORMAT(DT_INICIO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_INICIO_REALIZACAO, '%H%i')) <= DATE_FORMAT(sysdate(), '%Y%m%d%H%i') AND CONCAT(DATE_FORMAT(DT_TERMINO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_TERMINO_REALIZACAO, '%H%i')) >= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
				AND (($sqlWhereTipoProva) OR (c.IN_PROVA = 0 AND (c.NR_TENTATIVA > pa.NR_TENTATIVA_USUARIO OR pa.NR_TENTATIVA_USUARIO IS NULL)))
				ORDER BY DS_PROVA, DT_INICIO_REALIZACAO, HR_INICIO_REALIZACAO";

/*
$sql = "SELECT IN_PROVA, DT_TERMINO, DATE_ADD(DT_INICIO, INTERVAL c.DURACAO_PROVA *(60) + 5 SECOND) >= sysdate() as RESTA_TEMPO
		FROM col_prova c INNER JOIN col_prova_aplicada pa on pa.CD_PROVA = c.CD_PROVA INNER JOIN col_prova_chamada pc ON pc.CD_PROVA = c.CD_PROVA WHERE pa.CD_USUARIO = $usuario AND c.IN_ATIVO = 1
		AND CONCAT(DATE_FORMAT(DT_INICIO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_INICIO_REALIZACAO, '%H%i')) <= DATE_FORMAT(sysdate(), '%Y%m%d%H%i') AND CONCAT(DATE_FORMAT(DT_TERMINO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_TERMINO_REALIZACAO, '%H%i')) >= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
		AND c.CD_PROVA = {$_GET["id"]}
		AND ((DT_TERMINO IS NULL AND (DT_INICIO IS NULL OR DATE_ADD(DT_INICIO, INTERVAL c.DURACAO_PROVA *(60) + 5 SECOND) >= sysdate()))
		OR (c.IN_PROVA = 0 AND c.NR_TENTATIVA > pa.NR_TENTATIVA_USUARIO))";
*/
//$sql = "SELECT count(1) as Total FROM col_prova WHERE CD_PROVA = {$_GET["id"]}"; //selecionar por data e usu�rio e se j� fez a prova

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

//echo $sql;

$total = mysql_num_rows($resultado);

if (!(isset($_POST["btnFinalizar"]) || $_POST["hdnFinalizar"] == "1")){
	if ($total == 0){
		$sqlInformacao = "SELECT DATE_FORMAT(DT_INICIO,'%d/%m/%Y %H:%i') AS DT_INICIO, IN_PROVA FROM col_prova_aplicada pa
							inner join col_prova p on p.CD_PROVA = pa.CD_PROVA 
							WHERE p.CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario AND DT_INICIO IS NOT NULL";
		$resultadoInformacao = DaoEngine::getInstance()->executeQuery($sqlInformacao,true);
		if (mysql_num_rows($resultadoInformacao) == 0){
			$sqlInformacao = "SELECT DATE_FORMAT(DT_INICIO_REALIZACAO, '%d/%m/%Y') AS DT_INICIO, IN_PROVA FROM col_prova p INNER JOIN col_prova_chamada pc ON pc.CD_PROVA = p.CD_PROVA WHERE p.CD_PROVA = {$_GET["id"]}";
			$resultadoInformacao = DaoEngine::getInstance()->executeQuery($sqlInformacao,true);
			
			if (mysql_num_rows($resultadoInformacao) == 0) {
				parametrosProva::$somenteInformacao = "Avalia��o n�o dispon�vel";
			}
			
			
		}
		
		if (parametrosProva::$somenteInformacao == "") {

			$dadoInformacao = mysql_fetch_array($resultadoInformacao);
			if($dadoInformacao["IN_PROVA"] == 1){
				parametrosProva::$somenteInformacao = "Avalia��o de aprendizado j� realizada";
			} else {
				parametrosProva::$somenteInformacao = "Avalia��o de conhecimento j� realizada";
			}
			
			parametrosProva::$somenteInformacao = parametrosProva::$somenteInformacao . " em {$dadoInformacao["DT_INICIO"]}";
			
		}
		
	}
	else{
		$linha = mysql_fetch_array($resultado);
		if($linha["IN_PROVA"] == 0 && (!is_null($linha["DT_TERMINO"]) || $linha["RESTA_TEMPO"] == 0)){
			$usuario = page::$usuario;
			$sql = "insert into col_perguntas_aplicadas_hist (`DT_CADASTRO`,`CD_PERGUNTAS`,`CD_USUARIO`,`CD_PROVA`,`CD_RESPOSTA`,`IN_STATUS_PERGUNTA`) select sysdate(),`CD_PERGUNTAS`,`CD_USUARIO`,`CD_PROVA`,`CD_RESPOSTA`,`IN_STATUS_PERGUNTA` from col_perguntas_aplicadas where CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
			DaoEngine::getInstance()->executeQuery($sql,true);
			$sql = "DELETE FROM col_perguntas_aplicadas WHERE CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
			DaoEngine::getInstance()->executeQuery($sql,true);
			$sql = "insert into col_prova_aplicada_hist(`DT_CADASTRO`, `CD_PROVA`,`CD_USUARIO`,`VL_MEDIA`,`DT_INICIO`,`DT_TERMINO`,`CD_EMPRESA`,`NR_TENTATIVA_USUARIO`) select sysdate(), `CD_PROVA`,`CD_USUARIO`,`VL_MEDIA`,`DT_INICIO`,`DT_TERMINO`,`CD_EMPRESA`,`NR_TENTATIVA_USUARIO` from col_prova_aplicada where CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
			DaoEngine::getInstance()->executeQuery($sql,true);
			$sql = "UPDATE col_prova_aplicada SET DT_INICIO = NULL, DT_TERMINO = NULL WHERE DT_INICIO IS NOT NULL AND CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
			DaoEngine::getInstance()->executeQuery($sql,true);
		}
		elseif($linha["IN_PROVA"] == 1 && (!is_null($linha["DT_TERMINO"]) || $linha["RESTA_TEMPO"] == 0))
		{
			$usuario = page::$usuario;
			$sql = "insert into col_perguntas_aplicadas_hist_prova (`DT_CADASTRO`,`CD_PERGUNTAS`,`CD_USUARIO`,`CD_PROVA`,`CD_RESPOSTA`,`IN_STATUS_PERGUNTA`) select sysdate(),`CD_PERGUNTAS`,`CD_USUARIO`,`CD_PROVA`,`CD_RESPOSTA`,`IN_STATUS_PERGUNTA` from col_perguntas_aplicadas where CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
			DaoEngine::getInstance()->executeQuery($sql,true);
			$sql = "DELETE FROM col_perguntas_aplicadas WHERE CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
			DaoEngine::getInstance()->executeQuery($sql,true);
			$sql = "insert into col_prova_aplicada_hist_prova(`DT_CADASTRO`, `CD_PROVA`,`CD_USUARIO`,`VL_MEDIA`,`DT_INICIO`,`DT_TERMINO`,`CD_EMPRESA`,`NR_TENTATIVA_USUARIO`) select sysdate(), `CD_PROVA`,`CD_USUARIO`,`VL_MEDIA`,`DT_INICIO`,`DT_TERMINO`,`CD_EMPRESA`,`NR_TENTATIVA_USUARIO` from col_prova_aplicada where CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
			DaoEngine::getInstance()->executeQuery($sql,true);
			$sql = "UPDATE col_prova_aplicada SET DT_INICIO = NULL, DT_TERMINO = NULL WHERE DT_INICIO IS NOT NULL AND CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
			DaoEngine::getInstance()->executeQuery($sql,true);
		}
		
	}
}

parametrosProva::$col_prova = new col_prova();
parametrosProva::$col_prova->CD_PROVA = $_GET["id"];
parametrosProva::$col_prova->obter();

define(TITULO_PAGINA, parametrosProva::$col_prova->DS_PROVA);

//monta a prova se ela j� n�o existir
page::$isPostBack = count($_POST) > 0 ? true : false;

if (parametrosProva::$somenteInformacao != ""){
	render();
	exit();
}

if (page::$isPostBack){
	if (isset($_POST["btnFinalizar"]) || $_POST["hdnFinalizar"] == "1"){
		finalizarProva();
	}
	
	if (isset($_POST["btnGravar"])) {
		carregarRN();
	}
	
	if (isset($_POST["btnVisualizar"])) {
		carregarRN();
	}
	
}else
{

	$arquivoGravar = $PHP_SELF;
	$codigoProvaGravar = $_GET["id"];
	$arquivoGravar = "$arquivoGravar?$codigoProvaGravar";
	$paginaGravar = parametrosProva::$col_prova->DS_PROVA;
	include "../../include/accesscounter.php";
	
}

montarProva();	

if (!$_POST["salvarRespostas"] == 1)
{
	render();
}

function montarProva(){

    //colocar os disciplinas no filtro da prova
    $usuario = page::$usuario;

    //Adiciona em col_prova_aplicada se n�o existir
    $sql = "INSERT INTO col_prova_aplicada (CD_PROVA, CD_USUARIO)
            SELECT
                {$_GET["id"]}, $usuario
            FROM DUAL
            WHERE
            NOT EXISTS(SELECT 1 FROM col_prova_aplicada WHERE CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario)";

    DaoEngine::getInstance()->executeQuery($sql,true);

    //Verifica se pode fazer a prova
    if (!(verificarPaRealizado($usuario, $_GET["id"], $_SESSION["empresaID"]))){
        $resultados = true;
        include('cabecalho_prova_principal.php');
        echo '<div class="textblk" style="width:100%;text-align:center"><b>� necess�rio fazer o PA antes de fazer o AA.
				</b></div>';
        rodape(true);
        exit();

    }

	//$sql = "SELECT COUNT(1) AS TOTAL FROM col_perguntas_aplicadas WHERE CD_USUARIO = $usuario AND CD_PROVA = {$_GET["id"]}";
	$sql = "SELECT count(1) as TOTAL FROM col_prova_aplicada WHERE DT_INICIO IS NOT NULL AND CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linha = mysql_fetch_array($resultado);
	if ($linha["TOTAL"] > 0){
		return;
	}
	
	$sql = "DELETE FROM col_perguntas_aplicadas WHERE CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
	if (!DaoEngine::getInstance()->executeQuery($sql,true)){
		echo "Erro durante a cria��o da avalia��o";
		exit();
	}
	
	//$codigo_disciplina = parametrosProva::$col_prova->CD_DISCIPLINA;

	$disciplinas = parametrosProva::$col_prova->disciplinas;
	$resultadosPerguntas = array();
	
	foreach ($disciplinas as $disciplina){
		
		$codigo_disciplina = $disciplina->CD_DISCIPLINA;

        if (isset($_GET["all"]) && $_GET["all"] == 1 ){
            //Se � simulado
            if (parametrosProva::$col_prova->IN_PROVA == 0 && !(isset($_GET["multi"]) && ($_GET["multi"] == 1 || $_GET["multi"] == 2))){
                //echo "passou aqui 1";
                    $resultadosPerguntas[] = obterTodasPerguntasProva($codigo_disciplina, 1);
            }          //echo "passou aqui 2";

        }else{

        //Monta as perguntas por peso
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_1, 1);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_2, 2);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_3, 3);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_4, 4);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_5, 5);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_6, 6);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_7, 7);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_8, 8);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_9, 9);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_10, 10);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_11, 11);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_12, 12);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_13, 13);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_14, 14);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_15, 15);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_16, 16);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_17, 17);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_18, 18);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_19, 19);
            $resultadosPerguntas[] = obterPerguntasProva($codigo_disciplina, $disciplina->NR_QTD_PERGUNTAS_NIVEL_20, 20);

            //echo "passou aqui 1";

            //Se for para trazer todas as perguntas

        }

        //exit();

//		//******** Monta PESO 1 *********************************//
//		$quantidade_perguntas = $disciplina->NR_QTD_PERGUNTAS_NIVEL_1;
//
//		$sql = "SELECT * FROM col_perguntas P
//				WHERE P.CD_DISCIPLINA = $codigo_disciplina AND P.IN_PESO = 1 AND P.IN_ATIVO = 1
//				ORDER BY RAND() LIMIT 0,$quantidade_perguntas";
//
//
//		$resultado1 = DaoEngine::getInstance()->executeQuery($sql,true);
//
//		verificarQuantidadePerguntas($quantidade_perguntas, $resultado1);
//
//		//******** Monta PESO 2 *********************************//
//		$quantidade_perguntas = $disciplina->NR_QTD_PERGUNTAS_NIVEL_2;
//
//		$sql = "SELECT * FROM col_perguntas P
//				WHERE P.CD_DISCIPLINA = $codigo_disciplina AND P.IN_PESO = 2 AND P.IN_ATIVO = 1
//				ORDER BY RAND() LIMIT 0,$quantidade_perguntas";
//
//		$resultado2 = DaoEngine::getInstance()->executeQuery($sql,true);
//
//		verificarQuantidadePerguntas($quantidade_perguntas, $resultado2);
//
//		//********* Monta PESO 3 ********************************//
///		$quantidade_perguntas = $disciplina->NR_QTD_PERGUNTAS_NIVEL_3;
//
///		$sql = "SELECT * FROM col_perguntas P
//				WHERE P.CD_DISCIPLINA = $codigo_disciplina AND P.IN_PESO = 3 AND P.IN_ATIVO = 1
//				ORDER BY RAND() LIMIT 0,$quantidade_perguntas";
//
//		$resultado3 = DaoEngine::getInstance()->executeQuery($sql,true);
//
//		verificarQuantidadePerguntas($quantidade_perguntas, $resultado3);
//
//		$resultadosPerguntas[] = $resultado1;
//		$resultadosPerguntas[] = $resultado2;
//		$resultadosPerguntas[] = $resultado3;
		
	}

    //Se � prova e � para exibir a regra de uma pergunta por disciplina
    //echo parametrosProva::$col_prova->IN_PROVA;
    if (isset($_GET["all"]) && $_GET["all"] == 1 && (parametrosProva::$col_prova->IN_PROVA == 1)){
        $resultadosPerguntas[] = obterPerguntasProvaDisciplina(parametrosProva::$col_prova->CD_PROVA);
    }
    elseif (isset($_GET["all"]) && $_GET["all"] == 1 && (parametrosProva::$col_prova->IN_PROVA == 0 && isset($_GET["multi"]) && $_GET["multi"] == 1)){
        $resultadosPerguntas[] = obterTodasPerguntasProvaPasta($_GET["id"], 1);
    }elseif (isset($_GET["all"]) && $_GET["all"] == 1 && (parametrosProva::$col_prova->IN_PROVA == 0 && isset($_GET["multi"]) && $_GET["multi"] == 2)){
        $resultadosPerguntas[] = obterTodasPerguntasProvaCapitulo($_GET["id"], 1);
    }

		
	$arrayPerguntas=Array();
	
	foreach ($resultadosPerguntas as $resultadoPergunta){
		
		while ($linha = mysql_fetch_array($resultadoPergunta)) {
			$arrayPerguntas[] = $linha;
		}
	}
	
	shuffle($arrayPerguntas);

    //echo count($arrayPerguntas);

	inserirPerguntasProva($arrayPerguntas, $usuario);
	
	//inserirPerguntasProva($resultado1, $usuario);
	//inserirPerguntasProva($resultado2, $usuario);
	//inserirPerguntasProva($resultado3, $usuario);
	
	$sql = "UPDATE col_prova_aplicada SET DT_INICIO = sysdate() WHERE CD_USUARIO = $usuario AND CD_PROVA = {$_GET["id"]}";
	DaoEngine::getInstance()->executeQuery($sql,true);
	
}

function verificarPaRealizado($codigoUsuario, $codigoProva, $codigoEmpresa){

    //Verifica se o PA � obriogat�rio
    $obrigaDiagnostico = 0;
    $sql = "SELECT IN_OBRIGA_DIAGNOSTICO FROM col_empresa WHERE CD_EMPRESA = $codigoEmpresa";
    $RS_query = DaoEngine::getInstance()->executeQuery($sql,true);
    if($oRs = mysql_fetch_row($RS_query))
    {
        $obrigaDiagnostico = $oRs[0];
    }

    if($obrigaDiagnostico == 0) return true;

    //Verifica se � uma prova. Se n�o for, retorna true
    $sql = "SELECT IN_PROVA, ID_PASTA_INDIVIDUAL FROM col_prova WHERE CD_PROVA = $codigoProva";
    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);
    $linha = mysql_fetch_row($resultado);
    if($linha[0] == 0) return true;

    $idPasta = $linha[1];

    //Verifica se � o PA. Se for retorna true
    $sql = "SELECT 1 FROM col_assessment WHERE CD_PROVA = $codigoProva AND CD_EMPRESA = $codigoEmpresa";
    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);
    if(mysql_num_rows($resultado) > 0) return true;

    //Verifica se fez o PA
    $sql = "SELECT
                1
            FROM
                col_prova p
                INNER JOIN col_assessment a ON p.CD_PROVA = a.CD_PROVA
                INNER JOIN col_prova_aplicada pa ON pa.CD_PROVA = p.CD_PROVA
            WHERE
                p.ID_PASTA_INDIVIDUAL = $idPasta
            AND pa.CD_USUARIO = $codigoUsuario
            AND pa.VL_MEDIA IS NOT NULL";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);
    if(mysql_num_rows($resultado) > 0) return true;

    return false;

}


function obterPerguntasProva($codigo_disciplina, $quantidade_perguntas, $peso){

    $sql = "SELECT * FROM col_perguntas P
				WHERE P.CD_DISCIPLINA = $codigo_disciplina AND P.IN_PESO = $peso AND P.IN_ATIVO = 1
				ORDER BY RAND() LIMIT 0,$quantidade_perguntas";


    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    verificarQuantidadePerguntas($quantidade_perguntas, $resultado);

    return $resultado;

}

function obterPerguntasProvaDisciplina($idProva){



    $sql = "SELECT
                d.CD_DISCIPLINA, (SELECT pe1.CD_PERGUNTAS FROM col_perguntas pe1 WHERE pe1.CD_DISCIPLINA = d.CD_DISCIPLINA AND pe1.IN_ATIVO = 1 ORDER BY RAND() LIMIT 0,1) as CD_PERGUNTAS
            FROM
                tb_paginasindividuais pi
                INNER JOIN col_disciplina d ON pi.ID = d.ID_PAGINA_INDIVIDUAL
                INNER JOIN col_perguntas pe ON pe.CD_DISCIPLINA = d.CD_DISCIPLINA
                INNER JOIN col_prova p ON p.ID_PASTA_INDIVIDUAL = pi.pastaID
            WHERE
              pi.`Status` = 1
            AND pe.IN_ATIVO = 1
            AND p.CD_PROVA = $idProva
            group by d.CD_DISCIPLINA";

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    //echo $sql;

    verificarQuantidadePerguntas(1, $resultado);

    return $resultado;

}

function obterTodasPerguntasProva($codigo_disciplina, $quantidade_minima){

    $sql = "SELECT * FROM col_perguntas P
				WHERE P.CD_DISCIPLINA = $codigo_disciplina AND P.IN_ATIVO = 1
				ORDER BY RAND()";

    //echo $sql;
    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    verificarQuantidadePerguntas($quantidade_minima, $resultado);

    return $resultado;

}

function obterTodasPerguntasProvaPasta($codigo_prova, $quantidade_minima){

    $sql = "SELECT
                pe.*
            FROM
                    tb_paginasindividuais pi
                    INNER JOIN col_disciplina d ON pi.ID = d.ID_PAGINA_INDIVIDUAL
                    INNER JOIN col_perguntas pe ON pe.CD_DISCIPLINA = d.CD_DISCIPLINA
                    INNER JOIN col_prova p ON p.ID_PASTA_INDIVIDUAL = pi.pastaID
            WHERE
                pi.`Status` = 1
            AND pe.IN_ATIVO = 1
            AND p.CD_PROVA = $codigo_prova
				ORDER BY RAND() LIMIT 0,20";
    //echo $sql;

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    verificarQuantidadePerguntas($quantidade_minima, $resultado);

    return $resultado;

}

function obterTodasPerguntasProvaCapitulo($codigo_prova, $quantidade_minima){

    $sql = "SELECT
                pe.*
            FROM
                    tb_paginasindividuais pi
                    INNER JOIN col_disciplina d ON pi.ID = d.ID_PAGINA_INDIVIDUAL
                    INNER JOIN col_perguntas pe ON pe.CD_DISCIPLINA = d.CD_DISCIPLINA
                    INNER JOIN col_prova p ON p.CD_CAPITULO = pi.CD_CAPITULO
            WHERE
                pi.`Status` = 1
            AND pe.IN_ATIVO = 1
            AND p.CD_PROVA = $codigo_prova
				ORDER BY RAND() LIMIT 0,20";
    //echo $sql;

    $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

    verificarQuantidadePerguntas($quantidade_minima, $resultado);

    return $resultado;

}

function verificarQuantidadePerguntas($quantidade_perguntas, &$resultado){
	
	if ($quantidade_perguntas > mysql_num_rows($resultado)){
		$resultados = false;
		include('cabecalho_prova_principal.php');
		echo '<div class="textblk" style="width:100%;text-align:center"><b>N�o foi poss�vel iniciar execu��o da avalia��o.<br />
				Por favor, notifique-nos atrav�s do link <a
									href="http://'.$_SERVER["SERVER_NAME"].'/canal/index.php?m'.base64_encode("tabCont09").'&title=Problemas em '.TITULO_PAGINA.'"><span
									style="color: rgb(59, 89, 152); text-decoration: none;">Fale
								Conosco</span></a>.</b></div>';
		rodape(true);
		exit();
	}
	
}

function inserirPerguntasProva($resultado, $usuario){
	
	foreach ($resultado as $linha) {
		
		//Inserir no banco
		$sql = "INSERT INTO col_perguntas_aplicadas (CD_PERGUNTAS, CD_USUARIO, CD_PROVA) VALUES ({$linha["CD_PERGUNTAS"]}, $usuario, {$_GET["id"]})";
		DaoEngine::getInstance()->executeQuery($sql,true);

	}
	
}

function render($resultados = false){

    $header = true;

    if (isset($_GET["header"])){

        $header = $_GET["header"];

    }
    if (parametrosProva::$somenteInformacao != ""){
        $resultados = true;
    }

	include('cabecalho_prova_principal.php');

	if (parametrosProva::$somenteInformacao == ""){
		dataGridPerguntas(page::$usuario, $_GET["id"], $resultados);	
	}else {
		echo "<div class='textblk' style='width:100%;text-align:center'><br />" . parametrosProva::$somenteInformacao . "</div>";
		exit();
	}
	
	
	rodape($resultados);
}
	
	function dataGridPerguntas($usuario, $prova, $resultados){
		
		if ($resultados){
			
			$sql = "SELECT COUNT(1) AS CERTAS FROM col_perguntas_aplicadas P INNER JOIN col_respostas R ON P.CD_RESPOSTA = R.CD_RESPOSTAS AND P.CD_PERGUNTAS = R.CD_PERGUNTAS WHERE P.CD_USUARIO = $usuario AND P.CD_PROVA = $prova AND R.IN_CERTA = 1";
			$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
			$linha = mysql_fetch_array($resultado);
			$corretas = $linha["CERTAS"];
			
			$sql = "SELECT DATE_FORMAT(DT_INICIO,'%d/%m/%Y %H:%i:%S') AS DT_INICIO, TIMEDIFF(DT_TERMINO, DT_INICIO) AS TEMPO, NR_TENTATIVA_USUARIO,  DATE_FORMAT(DT_TERMINO,'%d/%m/%Y %H:%i:%S') AS DT_TERMINO FROM col_prova_aplicada WHERE CD_USUARIO = $usuario AND CD_PROVA = $prova";
			$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
			$linha = mysql_fetch_array($resultado);
			
			$inicio = $linha["DT_INICIO"];
			$tempoProva = $linha["TEMPO"];
			$tentativaUsuario = $linha["NR_TENTATIVA_USUARIO"];
			$termino = $linha["DT_TERMINO"];

            $sql = "SELECT
                        COUNT(1)AS TOTAL
                    FROM
                        col_perguntas_aplicadas P
                    WHERE
                        P.CD_USUARIO = $usuario
                    AND P.CD_PROVA = $prova;
                    ";
            $resultado = DaoEngine::getInstance()->executeQuery($sql,true);
            $linha = mysql_fetch_array($resultado);
            $total = $linha["TOTAL"];


			$erradas = $total - $corretas;
			//$total = parametrosProva::$col_prova->NR_QTD_PERGUNTAS_TOTAL;
			
			
			echo "<span class='textblk'>Nota: " . parametrosProva::$nota;
			echo "<br>Respostas Corretas: $corretas";
			echo "<br>Respostas Erradas: $erradas";
			echo "<br>Total de Perguntas: $total";
			echo "<br>In�cio da " . parametrosProva::$labelAvaliacao . ": $inicio";
			echo "<br>T�rmino da " . parametrosProva::$labelAvaliacao . ": $termino";
			echo "<br>Tempo de " . parametrosProva::$labelAvaliacao . ": $tempoProva";
			if (parametrosProva::$col_prova->IN_PROVA != 1){
				echo "<br>Tentativas: $tentativaUsuario";
			}

			echo "</span>";

			if ($_POST["hdnFinalizar"] == "1"){
				echo "<div class='textblk' style='width:100%;text-align:center'><b>A " . parametrosProva::$labelAvaliacao . " foi encerrada por decurso de prazo</b></div>";	
			}
			
				
			if (parametrosProva::$col_prova->IN_PROVA == 1 && parametrosProva::$col_prova->IN_GABARITO == 0) {
				return;
			}
		}
		
		if (!$resultados) {
			timer();	
		}
		
		parametrosProva::$visualizar = "";

		if(isset($_POST["hdnVisualizar"])){
			parametrosProva::$visualizar = $_POST["hdnVisualizar"];
		}
		
		$where = "";
		if(parametrosProva::$visualizar!="" && !$resultados){
			$where = " AND (IN_STATUS_PERGUNTA = 1 OR A.CD_RESPOSTA = 0) ";
		}
		
		
		$sql = "SELECT P.CD_PERGUNTAS, P.DS_PERGUNTAS, R.CD_RESPOSTAS, R.DS_RESPOSTAS, A.CD_RESPOSTA AS RESPOSTA_DADA, A.IN_STATUS_PERGUNTA, R.IN_CERTA, P.TX_OBSERVACAO, P.DS_LINK_SUMARIO FROM
					col_perguntas P
					INNER JOIN col_perguntas_aplicadas A ON P.CD_PERGUNTAS = A.CD_PERGUNTAS
					INNER JOIN col_respostas R ON R.CD_PERGUNTAS = P.CD_PERGUNTAS
					WHERE A.CD_PROVA = $prova AND CD_USUARIO = $usuario $where ORDER BY A.NR_ORDEM_PERGUNTA";
		
		//echo $sql;
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		//echo '						<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">';
		
		$perguntaAnterior = 0;
		
		$contador = 0;
		$quantidade_perguntas = parametrosProva::$col_prova->NR_QTD_PERGUNTAS_TOTAL;
		$perguntasPagina = parametrosProva::$col_prova->NR_QTD_PERGUNTAS_PAGINA;
		$novaPagina = false;
		$display = "";
		$aba = 0;
		
		if (isset($_POST["hdnAba"])){
			$aba = $_POST["hdnAba"];
		}
		
		if (isset($_POST["hdnVisualizar"])) {
			$aba = 0;
		}
		
		if (!$resultados || isset($_GET["header"])) {
			echo "<div style='height:350px; overflow : auto;'>";
		}else{
			echo "<div width='100%' id='divDados'>";
		}
		
		if (mysql_num_rows($resultado) == 0){
			echo "<div class='textblk' style='width:100%;text-align:center'><br />N�o h� quest�es pendentes e/ou respostas em branco</div>";
		}
		
		$numeroPergunta = 1;
		$letraResposta = "";
		
		while ($linha = mysql_fetch_array($resultado)) {
			
			
			if ($perguntaAnterior != $linha["CD_PERGUNTAS"]){
				
				$novaPagina = true;
				$letraResposta = "";
				
				$pendente = "";
				if ($linha["IN_STATUS_PERGUNTA"] > 0){
					$pendente = "checked";
				}
				
				if ($contador % $perguntasPagina == 0 ){
					
					if ($contador > 0){
						echo "</TABLE>\n";
					}
					
					$idTabela = $contador / $perguntasPagina;
					
					
					
					if ($idTabela != $aba){
						
						$display = "style='display: none;'";
					}else {
						$display = "";
					}
					
					
					
					echo "<TABLE cellpadding='0' cellspacing='0' border='0' width='100%' $display id='tbl$idTabela'>\n";
				}

                $textoPergunta = str_ireplace("
","<br />", $linha["DS_PERGUNTAS"]);

				echo "<TR><TD colspan='2'>&nbsp;</TD></TR>
					<TR class='tarjaItens'>
						<TD class='title' colspan='2'><span style='width: 85%'><!--<pre  class='title' style=\"font-weight:normal\">-->$numeroPergunta - $textoPergunta<!--</pre>--></span>
						<span >&nbsp;</span>
						<span width='15%' align='right'>";
				
				$numeroPergunta++;
				
				if (!$resultados){
					if (34 != 34)
					{
						echo "<INPUT type='checkbox' name='chk{$linha["CD_PERGUNTAS"]}' value='1' $pendente/>Pendente";	
					}
				}else{
					
					$observacao = $linha["TX_OBSERVACAO"];
					
					if (parametrosProva::$col_prova->IN_OBS == 0){
						$observacao = "";
					}
					
					if (trim($linha["DS_LINK_SUMARIO"]) != ""){
						echo "<a href='{$linha["DS_LINK_SUMARIO"]}' target='_blank'>Sum�rio</a>";
					}
					
					if (trim($linha["DS_LINK_SUMARIO"]) != "" && trim($observacao) != ""){
						//echo " - ";
					}
					
					if (trim($observacao) != ""){
						//echo "<a href='javascript:abreObs({$linha["CD_PERGUNTAS"]})'>Obs</a>";
					}
					
					//echo "<a href='{$linha["DS_LINK_SUMARIO"]}' target='_blank'>Sum�rio</a> - 
					//	<a href='javascript:abreObs({$linha["CD_PERGUNTAS"]})'>Obs</a>";
					
				}
						
				echo "		</span></TD>
					</TR>\n";
					
				$perguntaAnterior = $linha["CD_PERGUNTAS"];
				
			}
			
				$checked = "";
				$cor = "";
				
				if ($linha["RESPOSTA_DADA"] == $linha["CD_RESPOSTAS"]) {
					$checked = "checked";
					//$cor = "style='background-color: FF7373'";
				}
				
				
				$disabled = "";
				if($resultados){
					if ($linha["IN_CERTA"] == 1){
						$cor = "style='background-color: lightblue; color: darkblue'";
					}
					if ($checked == "") {
						$disabled = "disabled";
					}
					
				}
				
				switch ($letraResposta)
				{
					case "":
						$letraResposta = "a";
						break;
					case "a":
						$letraResposta = "b";
						break;
					case "b":
						$letraResposta = "c";
						break;
					case "c":
						$letraResposta = "d";
						break;
					case "d":
						$letraResposta = "e";
						break;
				}
				
				echo "<TR class='textblk' style='vertical-align:top'><TD><INPUT $disabled type='radio' name='rdo{$linha["CD_PERGUNTAS"]}' value='{$linha["CD_RESPOSTAS"]}' onclick='salvarRespostas();' $checked /></TD><TD $cor style='width:100%'>$letraResposta) {$linha["DS_RESPOSTAS"]}</TD></TR>\n";
			
//			if (($contador + 1) % $perguntasPagina == 0 || ($contador + 1) == $quantidade_perguntas){
//				echo "</TABLE>\n";
//			}
			
			if ($novaPagina){
				$contador++;
				$novaPagina = false;
			}
			
		}
		
		
		echo '</TABLE>';
		
		echo "</div>";
		
		$navegador = $contador / $perguntasPagina;

		if ($contador % $perguntasPagina > 0 && $contador / $perguntasPagina > 1){
			$navegador++;
		}
		
		if ($navegador <= 1){
			return;
		}
		
		$habilitaAnterior = "";
		$habilitaProximo = "";
		
		if ($aba == 0){
			$habilitaAnterior = "disabled";
		}
		
		if ($aba + 1 == floor($navegador)){
			$habilitaProximo = "disabled";
		}
		
		echo "<TABLE cellpading='0' border='0' cellspacing='0'>
				<TR><TD>&nbsp;</TD></TR>
				<TR>
					
					<TD><BUTTON id='navAnterior' onclick='javascript:navegaPerguntas(-1)' $habilitaAnterior><<- Anterior</BUTTON>
					<BUTTON id='navProxima' onclick='javascript:navegaPerguntas(1)' $habilitaProximo>Pr�xima ->></BUTTON>
					</TD>
				</TR>
			  </TABLE>
			  <INPUT type='hidden' value='$aba' name='hdnAba' />
			  <SCRIPT language='javascript'>

			   var abaAtual = $aba;
			  
			  	function navegaPerguntas(item){

			  		for (i = 0; i < 1000; i++){
			  			var aba = document.getElementById('tbl' + i);
			  			if (aba == null){
			  				break;
			  			}
			  			
			  			aba.style.display = 'none';
			  		}
			  	
			  		var novaAba = abaAtual + item;
			  		
			  		document.getElementById('tbl' + novaAba).style.display = 'block'
			  		
			  		abaAtual = novaAba;
			  		
			  		document.getElementById('hdnAba').value = abaAtual;
			  		
			  		novaAba = abaAtual + item;
			  		
			  		if (document.getElementById('tbl' + novaAba) == null){
						if (item > 0){
							document.getElementById('navAnterior').disabled = false;
							document.getElementById('navProxima').disabled = true;
						}else{
							document.getElementById('navAnterior').disabled = true;
							document.getElementById('navProxima').disabled = false;
						}
			  		}else {
						document.getElementById('navAnterior').disabled = false;
						document.getElementById('navProxima').disabled = false;
			  		}
			  		
			  	}
			  </SCRIPT>
			  ";
		
	}

function timer(){
	
		$usuario = page::$usuario;
		
		$sql = "SELECT HOUR(sysdate()) AS HORA_ATUAL, MINUTE(sysdate()) AS MINUTO_ATUAL, SECOND(sysdate()) AS SEGUNDO_ATUAL, HOUR(DT_INICIO) AS HORA, MINUTE(DT_INICIO) AS MINUTO, SECOND(DT_INICIO) AS SEGUNDO from col_prova_aplicada WHERE CD_USUARIO = $usuario AND CD_PROVA = {$_GET["id"]}";
		$horaResultado = DaoEngine::getInstance()->executeQuery($sql,true);
		
		$linhaResultado = mysql_fetch_array($horaResultado);
		
		$horaInicio = $linhaResultado["HORA"];
		$minutoInicio = $linhaResultado["MINUTO"];
		$segundoInicio = $linhaResultado["SEGUNDO"];
		$tempoProva = parametrosProva::$col_prova->DURACAO_PROVA;
		
		$diferencaHora = $linhaResultado["HORA_ATUAL"];
		$diferencaMinuto = $linhaResultado["MINUTO_ATUAL"] ;
		$diferencaSegundo = $linhaResultado["SEGUNDO_ATUAL"];
		
		$horaProva = floor($tempoProva / 60);
		$minutoProva = $tempoProva % 60;
		$segundoProva = $segundoInicio;
		
		$horaProva = $horaInicio + $horaProva;
		$minutoProva = $minutoInicio + $minutoProva;
		
		$horaInicio = $diferencaHora;
		$minutoInicio = $diferencaMinuto;
		$segundoInicio = $diferencaSegundo;
		
?>

<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript">
<!-- 

function salvarRespostas()
{

	get(document);
	
}

var http_request = false;
function makePOSTRequest(url, parameters) {
   http_request = false;
   if (window.XMLHttpRequest) { // Mozilla, Safari,...
      http_request = new XMLHttpRequest();
      if (http_request.overrideMimeType) {
      	// set type accordingly to anticipated content type
         //http_request.overrideMimeType('text/xml');
         http_request.overrideMimeType('text/html');
      }
   } else if (window.ActiveXObject) { // IE
      try {
         http_request = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
         try {
            http_request = new ActiveXObject("Microsoft.XMLHTTP");
         } catch (e) {}
      }
   }
   if (!http_request) {
      alert('Cannot create XMLHTTP instance');
      return false;
   }
   
   http_request.onreadystatechange = alertContents;
   http_request.open('POST', url, true);
   http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   http_request.setRequestHeader("Content-length", parameters.length);
   http_request.setRequestHeader("Connection", "close");
   http_request.send(parameters);
}

function alertContents() {
   if (http_request.readyState == 4) {
      if (http_request.status == 200) {
         //alert(http_request.responseText);
         result = http_request.responseText;
         //alert(http_request.responseText);
         //document.getElementById('myspan').innerHTML = result;            
      } else {
         //alert('Ocorreu um poss�vel erro durante a grava��o da resposta.');
      }
   }
}

function checarPendencias()
{

	var obj = document;

	qtdElementos = obj.getElementsByTagName("input").length;
	
	for (i=0; i<qtdElementos; i++) {
		var objRadio = obj.getElementsByTagName("input")[i];
		if (objRadio.type == "radio") {
			var nomeObjeto = objRadio.name;
			var radios = obj.getElementsByName(nomeObjeto);
			var qtdRadios = radios.length;
			var respostaMarcada = false;
			for(x=0;x<qtdRadios;x++) {
				if (radios[x].checked)
				{
					respostaMarcada = true;
					break;
				}
			}

			if(!(respostaMarcada))
			{
				return confirm('Existem perguntas n�o respondidas.\nTem certeza que deseja finalizar a <?php echo parametrosProva::$labelAvaliacao; ?>?');
			}
		}
	}

	
	return confirm('Tem certeza que deseja finalizar a <?php echo parametrosProva::$labelAvaliacao;?>?');

}

function get(obj) {
  var getstr = "?";
  for (i=0; i<obj.getElementsByTagName("input").length; i++) {
        if (obj.getElementsByTagName("input")[i].type == "text") {
           getstr += obj.getElementsByTagName("input")[i].name + "=" + 
                   obj.getElementsByTagName("input")[i].value + "&";
        }
        if (obj.getElementsByTagName("input")[i].type == "checkbox") {
           if (obj.getElementsByTagName("input")[i].checked) {
              getstr += obj.getElementsByTagName("input")[i].name + "=" + 
                   obj.getElementsByTagName("input")[i].value + "&";
           } else {
              //getstr += obj.getElementsByTagName("input")[i].name + "=&";
           }
        }
        if (obj.getElementsByTagName("input")[i].type == "radio") {
           if (obj.getElementsByTagName("input")[i].checked) {
              getstr += obj.getElementsByTagName("input")[i].name + "=" + 
                   obj.getElementsByTagName("input")[i].value + "&";
           }
     	}  
	     if (obj.getElementsByTagName("input")[i].tagName == "SELECT") {
	        var sel = obj.getElementsByTagName("input")[i];
	        getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
	     }
        if (obj.getElementsByTagName("input")[i].type == "hidden") {
            getstr += obj.getElementsByTagName("input")[i].name + "=" + 
                    obj.getElementsByTagName("input")[i].value + "&";
         }
     
  }

  getstr += "salvarRespostas=1&btnGravar=Salvar&";
  
  makePOSTRequest('<?php echo $_SERVER["REQUEST_URI"];?>', getstr);
  
}



//		     (yyyy,MM - 1,dd,hh,mm,ss)
<?php echo "var dateFuture = new Date(2007,10,07,$horaProva,$minutoProva,$segundoProva);";?>
<?php echo "var dataInicio = new Date(2007,10,07,$horaInicio,$minutoInicio,$segundoInicio);";?>
var restante = dateFuture.getTime() - dataInicio.getTime();

function GetCount(i){

	restante = restante - i;

	amount = restante;

	if(amount < 0){
		//document.getElementById('countbox').innerHTML="Terminou!";
		document.getElementById("hdnFinalizar").value = 1;
		document.forms[0].submit();
		return;
	}
	else{
		days=0;hours=0;mins=0;secs=0;out="";

		amount = Math.floor(amount/1000);
		days=Math.floor(amount/86400);
		amount=amount%86400;

		hours=Math.floor(amount/3600);
		amount=amount%3600;

		mins=Math.floor(amount/60);
		amount=amount%60;

		secs=Math.floor(amount);

		//if(days != 0){out += days +" dia"+((days!=1)?"s":"")+", ";}
		//if(days != 0 || hours != 0){out += hours +" hora"+((hours!=1)?"s":"")+", ";}
		//if(days != 0 || hours != 0 || mins != 0){out += mins +" minuto"+((mins!=1)?"s":"")+", ";}
		//out += secs +" segundos";

		if (secs < 10){
			secs = "0" + secs;
		}
		
		if(days != 0){out += days +" dia"+((days!=1)?"s":"")+"";}
		if(days != 0 || hours != 0){out += hours +":"+((hours!=1)?"":"")+"";}
		if(days != 0 || hours != 0 || mins != 0){out += mins +":"+((mins!=1)?"":"")+"";}
		out += secs +"";

		document.getElementById('countbox').innerHTML='Tempo restante: ' + out;

		setTimeout("GetCount(1000)", 1000);
	}
}

window.onload=function(){GetCount(0);}//call when everything has loaded




//-->
</script>
<div id="countbox" class='textblk' style='display:block;text-align:right;width:100%;height:23px;font-weight:bold;font-size:14px'></div>

<?php
	
}
	
function finalizarProva(){
	
	carregarRN(true);
	render(true);

    echo "<script>
           window.opener.location.reload();
        </script>";

	exit();
	
}

function carregarRN($finalizar = false){
	
	$prova = $_GET["id"];
	$usuario = page::$usuario;
	
	$respostas = array();

	//zera as pendentes
	$sql = "UPDATE col_perguntas_aplicadas SET IN_STATUS_PERGUNTA = 0 WHERE CD_PROVA = $prova AND CD_USUARIO = $usuario";
	DaoEngine::getInstance()->executeQuery($sql,true);

	
	foreach($_POST as $key => $valor){
		//echo "$key $valor \n";
		if (strlen($key) > 3){
			if (substr($key,0,3) == "rdo"){
				$pergunta = substr($key,3, strlen($key));
				$resposta = $valor;
				
				$pendente = 0;
				if (isset($_POST["chk$pergunta"])){
					$pendente = $_POST["chk$pergunta"];
				}
				
				$sql = "UPDATE col_perguntas_aplicadas SET CD_RESPOSTA = $resposta, IN_STATUS_PERGUNTA = $pendente WHERE CD_PROVA = $prova AND CD_USUARIO = $usuario AND CD_PERGUNTAS = $pergunta";
				DaoEngine::getInstance()->executeQuery($sql,true);
				//echo $sql;
			}elseif (substr($key,0,3) == "chk"){
				
				$pendente = 0;
				$pergunta = substr($key,3, strlen($key));
				if (isset($_POST["chk$pergunta"])){
					$pendente = $_POST["chk$pergunta"];
				}
				
				$sql = "UPDATE col_perguntas_aplicadas SET IN_STATUS_PERGUNTA = $pendente WHERE CD_PROVA = $prova AND CD_USUARIO = $usuario AND CD_PERGUNTAS = $pergunta";
				DaoEngine::getInstance()->executeQuery($sql,true);
				
			}
		}
	}
		
	if ($finalizar){
		
		$sql = "SELECT
                    COUNT(1)AS TOTAL,
                    COUNT(IF(R.IN_CERTA = 1,1,NULL)) AS CERTAS
                FROM
                    col_perguntas_aplicadas P
                LEFT JOIN col_respostas R ON P.CD_RESPOSTA = R.CD_RESPOSTAS
                AND P.CD_PERGUNTAS = R.CD_PERGUNTAS
                WHERE P.CD_USUARIO = $usuario AND P.CD_PROVA = $prova";

		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		
		$linha = mysql_fetch_array($resultado);
		
		$corretas = $linha["CERTAS"];
        $total = $linha["TOTAL"];

		$media = $corretas / $total * 1000;
		
		$media = round($media) / 100;

		
		$sql = "UPDATE col_prova_aplicada SET DT_TERMINO = sysdate(), VL_MEDIA = $media, NR_TENTATIVA_USUARIO = NR_TENTATIVA_USUARIO + 1 WHERE CD_USUARIO = $usuario AND CD_PROVA = $prova";
		DaoEngine::getInstance()->executeQuery($sql,true);
		
		$sql = "UPDATE col_perguntas_aplicadas SET IN_STATUS_PERGUNTA = 0 WHERE CD_PROVA = $prova AND CD_USUARIO = $usuario";
		DaoEngine::getInstance()->executeQuery($sql,true);
		
		parametrosProva::$nota = formataNotaAvaliacao($media);
		
		render(true);

        if(parametrosProva::$col_prova->IN_PROVA == 1){

            //Verifica se existe nota anterior maior que a atual e se sim, substitui a prova uma pela outra
            $sql = "SELECT
                        COUNT(1)
                    FROM
                        col_prova_aplicada_hist_prova ph
                        INNER JOIN col_prova_aplicada pa ON ph.CD_PROVA = pa.CD_PROVA AND ph.CD_USUARIO = pa.CD_USUARIO
                    WHERE
                        pa.CD_USUARIO = $usuario AND pa.CD_PROVA = $prova AND ph.VL_MEDIA > pa.VL_MEDIA;";

            $resultado = DaoEngine::getInstance()->executeQuery($sql,true);
            $linha = mysql_fetch_row($resultado);

            if($linha[0] > 0){ //Se tem nota maior, substitui
                //Insere a prova atual na tabela de hist�rico
                $sql = "insert into col_perguntas_aplicadas_hist_prova (`DT_CADASTRO`,`CD_PERGUNTAS`,`CD_USUARIO`,`CD_PROVA`,`CD_RESPOSTA`,`IN_STATUS_PERGUNTA`) select sysdate(),`CD_PERGUNTAS`,`CD_USUARIO`,`CD_PROVA`,`CD_RESPOSTA`,`IN_STATUS_PERGUNTA` from col_perguntas_aplicadas where CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
                DaoEngine::getInstance()->executeQuery($sql,true);

                $sql = "insert into col_prova_aplicada_hist_prova(`DT_CADASTRO`, `CD_PROVA`,`CD_USUARIO`,`VL_MEDIA`,`DT_INICIO`,`DT_TERMINO`,`CD_EMPRESA`,`NR_TENTATIVA_USUARIO`) select sysdate(), `CD_PROVA`,`CD_USUARIO`,`VL_MEDIA`,`DT_INICIO`,`DT_TERMINO`,`CD_EMPRESA`,`NR_TENTATIVA_USUARIO` from col_prova_aplicada where CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
                DaoEngine::getInstance()->executeQuery($sql,true);

                $sql = "DELETE FROM col_prova_aplicada WHERE CD_USUARIO = $usuario AND CD_PROVA = $prova";
                DaoEngine::getInstance()->executeQuery($sql,true);

                $sql = "INSERT INTO col_prova_aplicada (DT_INICIO, DT_TERMINO, CD_PROVA, CD_USUARIO, VL_MEDIA, NR_TENTATIVA_USUARIO)
                        SELECT
                            ph.DT_INICIO, ph.DT_TERMINO, ph.CD_PROVA, ph.CD_USUARIO, ph.VL_MEDIA, 2
                        FROM
                            col_prova_aplicada_hist_prova ph
                        WHERE
                            ph.CD_USUARIO = $usuario AND ph.CD_PROVA = $prova
                        ORDER BY
                            ph.VL_MEDIA DESC,
                            ph.DT_CADASTRO ASC
                        LIMIT 0,1";
                DaoEngine::getInstance()->executeQuery($sql,true);

                //insere a prova antiga na tabela de prova
                $sql = "DELETE FROM col_perguntas_aplicadas WHERE CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
                DaoEngine::getInstance()->executeQuery($sql,true);

                $sql = "INSERT INTO col_perguntas_aplicadas (CD_PERGUNTAS, CD_USUARIO, CD_PROVA, CD_RESPOSTA, IN_STATUS_PERGUNTA)
                        SELECT
                            pa.CD_PERGUNTAS, pa.CD_USUARIO, pa.CD_PROVA, pa.CD_RESPOSTA, pa.IN_STATUS_PERGUNTA
                        FROM
                            col_prova_aplicada_hist_prova ph
                            INNER JOIN col_perguntas_aplicadas_hist_prova pa ON ph.CD_PROVA = pa.CD_PROVA AND ph.CD_USUARIO = pa.CD_USUARIO AND pa.DT_CADASTRO = ph.DT_CADASTRO
                        WHERE
                            pa.CD_USUARIO = $usuario AND pa.CD_PROVA = $prova
                            AND ph.DT_CADASTRO = (SELECT MIN(ph1.DT_CADASTRO) FROM col_prova_aplicada_hist_prova ph1 WHERE ph1.CD_USUARIO = ph.CD_USUARIO AND ph1.CD_PROVA = ph.CD_PROVA AND
                            ph1.VL_MEDIA = (SELECT MAX(ph2.VL_MEDIA) FROM col_prova_aplicada_hist_prova ph2 WHERE ph1.CD_USUARIO = ph2.CD_USUARIO AND ph1.CD_PROVA = ph2.CD_PROVA))
                        ORDER BY
                            ph.VL_MEDIA,
                            ph.DT_CADASTRO";


                DaoEngine::getInstance()->executeQuery($sql,true);




            }

            //Define o curso como default se fez PA
            $sql = "SELECT
                            p.ID_PASTA_INDIVIDUAL
                        FROM
                            col_assessment a
                            INNER JOIN col_prova p ON a.CD_PROVA = p.CD_PROVA
                        WHERE
                            p.CD_PROVA = {$_GET["id"]}";

            $resultado = DaoEngine::getInstance()->executeQuery($sql,true);

            if(mysql_num_rows($resultado) > 0){
                $linha = mysql_fetch_row($resultado);
                $_SESSION["PASTA"] = $linha[0];
                $_SESSION["ABA"] = "CONTEUDO";
            }

        }



		exit();
		
	}
	
}
	
function rodape($resultados = false){
	
?>	
					</TD>
				</TR>
				<TR class="tarjaTitulo">
						<TD colSpan="4"><IMG height="1" src="images/blank.gif" width="100" style="visibility: hidden;"></TD>
				</TR>
				
			</TABLE>

			<?php if(!$resultados){ ?>
			
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD><IMG height="40" src="images/blank.gif" width="1"></TD>
					</TR>
					<TR>
						<TD align="middle">
							

						
							<input type="hidden" id="hdnVisualizar" name="hdnVisualizar" value="<?php echo parametrosProva::$visualizar; ?>" />
							<input type="hidden" id="hdnFinalizar" name="hdnFinalizar" value="0" />
							<?php
							
								$itensVisualizar = "Pendentes e Brancos";
								if(parametrosProva::$visualizar != ""){
									$itensVisualizar = "Todos";
								}
							
								$valorVisualizar = "1";
								if (parametrosProva::$visualizar!=""){
									$valorVisualizar = "";
								}
								
							$empresa = $_SESSION["empresaID"];
							
							
							
							if (34 != 34)
							{
							?>
							<INPUT class="buttonsty" name="btnVisualizar" style="width: 210px"
								type="submit" value="Visualizar <?php echo $itensVisualizar; ?>" onclick="javascript:document.getElementById('hdnVisualizar').value = '<?php echo $valorVisualizar; ?>'; return true;"/>
								<IMG height="1" src="images/blank.gif" width="20"/>
							<?php 
							}
							?>
							<!--  <INPUT class="buttonsty" name="btnGravar"
								type="submit" value="Salvar Respostas" title="Salvar as perguntas j� respondidas"/><IMG height="1" src="images/blank.gif" width="20"/>-->
							
							<INPUT class="buttonsty" name="btnFinalizar"
								type="submit" value="Finalizar <?php echo parametrosProva::$labelAvaliacao; ?>" onclick="javascript:return checarPendencias();"/><BR>
						</TD>
					</TR>
			</TABLE>
			<?php } ?>
	
		</FORM>
		
		
		<iframe style="display:none" src="mantemsessao.php"></iframe>
		
	</BODY>
	
</HTML>

<?php
}


?>