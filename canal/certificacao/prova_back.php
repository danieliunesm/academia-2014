<?php

include "../../include/security.php";
//page::$usuario = $_SESSION("CD_USU");
include("../../include/defines.php");
include('../../admin/framework/crud.php');
include('../../admin/controles.php');
include('../../admin/page.php');

class parametrosProva{
	
	static $col_prova;
	static $visualizar;
	static $nota;
	static $somenteInformacao = "";
	
}

$usuario = page::$usuario;


$sql = "SELECT IN_PROVA, DT_TERMINO FROM col_prova c INNER JOIN col_prova_aplicada pa on pa.CD_PROVA = c.CD_PROVA WHERE pa.CD_USUARIO = $usuario AND c.IN_ATIVO = 1
		AND CONCAT(DATE_FORMAT(DT_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HORARIO_INICIO_DISPONIVEL, '%H%i')) <= DATE_FORMAT(sysdate(), '%Y%m%d%H%i') AND CONCAT(DATE_FORMAT(DT_FIM_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HORARIO_FIM_DISPONIVEL, '%H%i')) >= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
		AND c.CD_PROVA = {$_GET["id"]} AND DT_TERMINO IS NULL AND (DT_INICIO IS NULL OR DATE_ADD(DT_INICIO, INTERVAL c.DURACAO_PROVA *(60) + 5 SECOND) >= sysdate())";


//$sql = "SELECT count(1) as Total FROM col_prova WHERE CD_PROVA = {$_GET["id"]}"; //selecionar por data e usu�rio e se j� fez a prova

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

$total = mysql_num_rows($resultado);

if (!(isset($_POST["btnFinalizar"]) || $_POST["hdnFinalizar"] == "1")){
	if ($total == 0){
		$sqlInformacao = "SELECT DATE_FORMAT(DT_INICIO,'%d/%m/%Y %H:%i') AS DT_INICIO, IN_PROVA FROM col_prova_aplicada pa
							inner join col_prova p on p.CD_PROVA = pa.CD_PROVA
							WHERE p.CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
		$resultadoInformacao = DaoEngine::getInstance()->executeQuery($sqlInformacao,true);
		if (mysql_num_rows($resultadoInformacao) == 0){
			parametrosProva::$somenteInformacao = "Prova n�o dispon�vel";
		}else {
			$dadoInformacao = mysql_fetch_array($resultadoInformacao);
			if($dadoInformacao["IN_PROVA"] == 1){
				parametrosProva::$somenteInformacao = "Prova j� realizada";
			} else {
				parametrosProva::$somenteInformacao = "Simulado j� realizado";
			}
			
			parametrosProva::$somenteInformacao = parametrosProva::$somenteInformacao . " em {$dadoInformacao["DT_INICIO"]}";
			
		}
		
		
		//echo "Prova n�o dispon�vel";
		//exit();
		
	}
}
/*else{
	$linha = mysql_fetch_array($resultado);
	if($linha["IN_PROVA"] == 1 || is_null($linha["DT_TERMINO"])){
		echo "Prova n�o dispon�vel";
		exit();
	}
}*/

parametrosProva::$col_prova = new col_prova();
parametrosProva::$col_prova->CD_PROVA = $_GET["id"];
parametrosProva::$col_prova->obter();

define(TITULO_PAGINA, parametrosProva::$col_prova->DS_PROVA);

//monta a prova se ela j� n�o existir
page::$isPostBack = count($_POST) > 0 ? true : false;


if (parametrosProva::$somenteInformacao != ""){
	render();
	exit();
}


if (page::$isPostBack){
	if (isset($_POST["btnFinalizar"]) || $_POST["hdnFinalizar"] == "1"){
		finalizarProva();
	}
	
	if (isset($_POST["btnGravar"])) {
		carregarRN();
	}
	
	if (isset($_POST["btnVisualizar"])) {
		carregarRN();
	}
	
}

montarProva();	

render();

function montarProva(){
	
	//colocar os disciplinas no filtro da prova
	$usuario = page::$usuario;
	//$sql = "SELECT COUNT(1) AS TOTAL FROM col_perguntas_aplicadas WHERE CD_USUARIO = $usuario AND CD_PROVA = {$_GET["id"]}";
	$sql = "SELECT count(1) as TOTAL FROM col_prova_aplicada WHERE DT_INICIO IS NOT NULL AND CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	$linha = mysql_fetch_array($resultado);
	if ($linha["TOTAL"] > 0){
		return;
	}
	
	$sql = "DELETE FROM col_perguntas_aplicadas WHERE CD_PROVA = {$_GET["id"]} AND CD_USUARIO = $usuario";
	if (!DaoEngine::getInstance()->executeQuery($sql,true)){
		echo "Erro durante a cria��o da prova";
		exit();
	}
	
	$quantidade_perguntas = parametrosProva::$col_prova->NR_QTD_PERGUNTAS_TOTAL;
	$codigo_disciplina = parametrosProva::$col_prova->CD_DISCIPLINA;
	$peso_prova = parametrosProva::$col_prova->IN_DIFICULDADE;
	
	$sql = "SELECT * FROM col_perguntas P
			WHERE P.CD_DISCIPLINA = $codigo_disciplina AND P.IN_PESO <= $peso_prova AND P.IN_ATIVO = 1
			ORDER BY P.IN_PESO DESC, RAND() LIMIT 0,$quantidade_perguntas";
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	
	if ($quantidade_perguntas > mysql_num_rows($resultado)){
		include('cabecalho_prova.php');
		echo '<div class="textblk" style="width:100%;text-align:center"><b>N�o foi poss�vel iniciar execu��o da prova.<br />
				Por favor, notifique o usu�rio Jo�o Paulo pelo telefone (21) 8112-4832.</b></div>';
		rodape(true);
		exit();
	}
	
	while ($linha = mysql_fetch_array($resultado)) {
		
		//Inserir no banco
		$sql = "INSERT INTO col_perguntas_aplicadas (CD_PERGUNTAS, CD_USUARIO, CD_PROVA) VALUES ({$linha["CD_PERGUNTAS"]}, $usuario, {$_GET["id"]})";
		DaoEngine::getInstance()->executeQuery($sql,true);
		
	}
	
	$sql = "UPDATE col_prova_aplicada SET DT_INICIO = sysdate() WHERE CD_USUARIO = $usuario AND CD_PROVA = {$_GET["id"]}";
	DaoEngine::getInstance()->executeQuery($sql,true);
	
}


function render($resultados = false){


	include('cabecalho_prova.php');

	if (parametrosProva::$somenteInformacao == ""){
		dataGridPerguntas(page::$usuario, $_GET["id"], $resultados);	
	}else {
		echo "<div align='center' class='textblk'><br />" . parametrosProva::$somenteInformacao . "</div>";
		exit();
	}
	
	
	rodape($resultados);
}
	
	function dataGridPerguntas($usuario, $prova, $resultados){
		
		if ($resultados){
			
			$sql = "SELECT COUNT(1) AS CERTAS FROM col_perguntas_aplicadas P INNER JOIN col_respostas R ON P.CD_RESPOSTA = R.CD_RESPOSTAS AND P.CD_PERGUNTAS = R.CD_PERGUNTAS WHERE P.CD_USUARIO = $usuario AND P.CD_PROVA = $prova AND R.IN_CERTA = 1";
			$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
			$linha = mysql_fetch_array($resultado);
			$corretas = $linha["CERTAS"];
			
			$sql = "SELECT DATE_FORMAT(DT_INICIO,'%d/%m/%Y %H:%i:%S') AS DT_INICIO, TIMEDIFF(DT_TERMINO, DT_INICIO) AS TEMPO FROM col_prova_aplicada WHERE CD_USUARIO = $usuario AND CD_PROVA = $prova";
			$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
			$linha = mysql_fetch_array($resultado);
			
			$inicio = $linha["DT_INICIO"];
			$tempoProva = $linha["TEMPO"];

			$erradas = parametrosProva::$col_prova->NR_QTD_PERGUNTAS_TOTAL - $corretas;
			$total = parametrosProva::$col_prova->NR_QTD_PERGUNTAS_TOTAL;
			
			
			echo "<span class='textblk'>Nota: " . parametrosProva::$nota;
			echo "<br>Respostas Corretas: $corretas";
			echo "<br>Respostas Erradas: $erradas";
			echo "<br>Total de Perguntas: $total";
			echo "<br>In�cio da Prova: $inicio";
			echo "<br>Tempo de Prova: $tempoProva </span>";
			if ($_POST["hdnFinalizar"] == "1"){
				echo "<div class='textblk' style='width:100%;text-align:center'><b>A prova foi encerrada por decurso de prazo</b></div>";	
			}
			
				
			if (parametrosProva::$col_prova->IN_PROVA == 1) {
				return;
			}
		}
		
		if (!$resultados) {
			timer();	
		}
		
		parametrosProva::$visualizar = "";

		if(isset($_POST["hdnVisualizar"])){
			parametrosProva::$visualizar = $_POST["hdnVisualizar"];
		}
		
		$where = "";
		if(parametrosProva::$visualizar!="" && !$resultados){
			$where = " AND (IN_STATUS_PERGUNTA = 1 OR A.CD_RESPOSTA = 0) ";
		}
		
		
		$sql = "SELECT P.CD_PERGUNTAS, P.DS_PERGUNTAS, R.CD_RESPOSTAS, R.DS_RESPOSTAS, A.CD_RESPOSTA AS RESPOSTA_DADA, A.IN_STATUS_PERGUNTA, R.IN_CERTA, P.TX_OBSERVACAO, P.DS_LINK_SUMARIO FROM
					col_perguntas P
					INNER JOIN col_perguntas_aplicadas A ON P.CD_PERGUNTAS = A.CD_PERGUNTAS
					INNER JOIN col_respostas R ON R.CD_PERGUNTAS = P.CD_PERGUNTAS
					WHERE A.CD_PROVA = $prova AND CD_USUARIO = $usuario $where ORDER BY P.CD_PERGUNTAS";
		
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		
		//echo '						<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">';
		
		$perguntaAnterior = 0;
		
		$contador = 0;
		$quantidade_perguntas = parametrosProva::$col_prova->NR_QTD_PERGUNTAS_TOTAL;
		$perguntasPagina = parametrosProva::$col_prova->NR_QTD_PERGUNTAS_PAGINA;
		$novaPagina = false;
		$display = "";
		$aba = 0;
		
		if (isset($_POST["hdnAba"])){
			$aba = $_POST["hdnAba"];
		}
		
		if (isset($_POST["hdnVisualizar"])) {
			$aba = 0;
		}
		
		echo "<div style='height:350px; overflow : auto;'>";
		
		if (mysql_num_rows($resultado) == 0){
			echo "<div align='center' class='textblk'><br />N�o h� pendentes ou respostas em branco</div>";
		}
		
		while ($linha = mysql_fetch_array($resultado)) {
			
			
			if ($perguntaAnterior != $linha["CD_PERGUNTAS"]){
				
				$novaPagina = true;
				
				$pendente = "";
				if ($linha["IN_STATUS_PERGUNTA"] > 0){
					$pendente = "checked";
				}
				
				if ($contador % $perguntasPagina == 0 ){
					
					if ($contador > 0){
						echo "</TABLE>\n";
					}
					
					$idTabela = $contador / $perguntasPagina;
					
					
					
					if ($idTabela != $aba){
						
						$display = "style='display: none;'";
					}else {
						$display = "";
					}
					
					
					
					echo "<TABLE cellpadding='0' cellspacing='0' border='0' width='100%' $display id='tbl$idTabela'>\n";
				}
				
				echo "<TR><TD colspan='2'>&nbsp;</TD></TR>
					<TR class='tarjaItens'>
						<TD class='title' colspan='2'><span style='width: 85%'>{$linha["DS_PERGUNTAS"]}</span>
						<span >&nbsp;</span>
						<span width='15%' align='right'>";
				
				if (!$resultados){
					echo "<INPUT type='checkbox' name='chk{$linha["CD_PERGUNTAS"]}' value='1' $pendente/>Pendente";
				}else{
					
					$observacao = $linha["TX_OBSERVACAO"];
					
					if (trim($linha["DS_LINK_SUMARIO"]) != ""){
						echo "<a href='{$linha["DS_LINK_SUMARIO"]}' target='_blank'>Sum�rio</a>";
					}
					
					if (trim($linha["DS_LINK_SUMARIO"]) != "" && trim($observacao) != ""){
						echo " - ";
					}
					
					if (trim($observacao) != ""){
						echo "<a href='javascript:abreObs({$linha["CD_PERGUNTAS"]})'>Obs</a>";
					}
					
					//echo "<a href='{$linha["DS_LINK_SUMARIO"]}' target='_blank'>Sum�rio</a> - 
					//	<a href='javascript:abreObs({$linha["CD_PERGUNTAS"]})'>Obs</a>";
					
				}
						
				echo "		</span></TD>
					</TR>\n";
					
				$perguntaAnterior = $linha["CD_PERGUNTAS"];
				
			}
			
				$checked = "";
				if ($linha["RESPOSTA_DADA"] == $linha["CD_RESPOSTAS"]) {
					$checked = "checked";
				}
				
				$cor = "";
				if($resultados){
					if ($linha["IN_CERTA"] == 1){
						$cor = "style='background-color: lightblue'";
					}
					
				}
				
				echo "<TR class='textblk'><TD ><INPUT type='radio' name='rdo{$linha["CD_PERGUNTAS"]}' value='{$linha["CD_RESPOSTAS"]}' $checked/></TD><TD $cor style='width:100%'>{$linha["DS_RESPOSTAS"]}</TD></TR>\n";
			
//			if (($contador + 1) % $perguntasPagina == 0 || ($contador + 1) == $quantidade_perguntas){
//				echo "</TABLE>\n";
//			}
			
			if ($novaPagina){
				$contador++;
				$novaPagina = false;
			}
			
		}
		
		
		echo '</TABLE>';
		
		echo "</div>";
		
		$navegador = $contador / $perguntasPagina;
		if ($contador % $perguntasPagina > 0){
			$navegador++;
		}
		
		if ($navegador <= 1){
			return;
		}
		
		$habilitaAnterior = "";
		$habilitaProximo = "";
		
		if ($aba == 0){
			$habilitaAnterior = "disabled";
		}
		
		if ($aba + 1 == floor($navegador)){
			$habilitaProximo = "disabled";
		}
		
		echo "<TABLE cellpading='0' border='0' cellspacing='0'>
				<TR><TD>&nbsp;</TD></TR>
				<TR>
					
					<TD><BUTTON id='navAnterior' onclick='javascript:navegaPerguntas(-1)' $habilitaAnterior><<- Anterior</BUTTON>
					<BUTTON id='navProxima' onclick='javascript:navegaPerguntas(1)' $habilitaProximo>Pr�xima ->></BUTTON>
					</TD>
				</TR>
			  </TABLE>
			  <INPUT type='hidden' value='$aba' name='hdnAba' />
			  <SCRIPT language='javascript'>

			   var abaAtual = $aba;
			  
			  	function navegaPerguntas(item){

			  		for (i = 0; i < 1000; i++){
			  			var aba = document.getElementById('tbl' + i);
			  			if (aba == null){
			  				break;
			  			}
			  			
			  			aba.style.display = 'none';
			  		}
			  	
			  		var novaAba = abaAtual + item;
			  		
			  		document.getElementById('tbl' + novaAba).style.display = 'block'
			  		
			  		abaAtual = novaAba;
			  		
			  		document.getElementById('hdnAba').value = abaAtual;
			  		
			  		novaAba = abaAtual + item;
			  		
			  		if (document.getElementById('tbl' + novaAba) == null){
						if (item > 0){
							document.getElementById('navAnterior').disabled = false;
							document.getElementById('navProxima').disabled = true;
						}else{
							document.getElementById('navAnterior').disabled = true;
							document.getElementById('navProxima').disabled = false;
						}
			  		}else {
						document.getElementById('navAnterior').disabled = false;
						document.getElementById('navProxima').disabled = false;
			  		}
			  		
			  	}
			  </SCRIPT>
			  ";
		
	}

function timer(){
	
		$usuario = page::$usuario;
		
		$sql = "SELECT HOUR(sysdate()) AS HORA_ATUAL, MINUTE(sysdate()) AS MINUTO_ATUAL, SECOND(sysdate()) AS SEGUNDO_ATUAL, HOUR(DT_INICIO) AS HORA, MINUTE(DT_INICIO) AS MINUTO, SECOND(DT_INICIO) AS SEGUNDO from col_prova_aplicada WHERE CD_USUARIO = $usuario AND CD_PROVA = {$_GET["id"]}";
		$horaResultado = DaoEngine::getInstance()->executeQuery($sql,true);
		
		$linhaResultado = mysql_fetch_array($horaResultado);
		
		$horaInicio = $linhaResultado["HORA"];
		$minutoInicio = $linhaResultado["MINUTO"];
		$segundoInicio = $linhaResultado["SEGUNDO"];
		$tempoProva = parametrosProva::$col_prova->DURACAO_PROVA;
		
		$diferencaHora = $linhaResultado["HORA_ATUAL"];
		$diferencaMinuto = $linhaResultado["MINUTO_ATUAL"] ;
		$diferencaSegundo = $linhaResultado["SEGUNDO_ATUAL"];
		
		$horaProva = floor($tempoProva / 60);
		$minutoProva = $tempoProva % 60;
		$segundoProva = $segundoInicio;
		
		$horaProva = $horaInicio + $horaProva;
		$minutoProva = $minutoInicio + $minutoProva;
		
		$horaInicio = $diferencaHora;
		$minutoInicio = $diferencaMinuto;
		$segundoInicio = $diferencaSegundo;
		
?>

<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript">
<!-- 

//		     (yyyy,MM - 1,dd,hh,mm,ss)
<?php echo "var dateFuture = new Date(2007,10,07,$horaProva,$minutoProva,$segundoProva);";?>
<?php echo "var dataInicio = new Date(2007,10,07,$horaInicio,$minutoInicio,$segundoInicio);";?>
var restante = dateFuture.getTime() - dataInicio.getTime();

function GetCount(i){

	restante = restante - i;

	amount = restante;

	if(amount < 0){
		//document.getElementById('countbox').innerHTML="Terminou!";
		document.getElementById("hdnFinalizar").value = 1;
		document.forms[0].submit();
		return;
	}
	else{
		days=0;hours=0;mins=0;secs=0;out="";

		amount = Math.floor(amount/1000);
		days=Math.floor(amount/86400);
		amount=amount%86400;

		hours=Math.floor(amount/3600);
		amount=amount%3600;

		mins=Math.floor(amount/60);
		amount=amount%60;

		secs=Math.floor(amount);

		//if(days != 0){out += days +" dia"+((days!=1)?"s":"")+", ";}
		//if(days != 0 || hours != 0){out += hours +" hora"+((hours!=1)?"s":"")+", ";}
		//if(days != 0 || hours != 0 || mins != 0){out += mins +" minuto"+((mins!=1)?"s":"")+", ";}
		//out += secs +" segundos";

		if (secs < 10){
			secs = "0" + secs;
		}
		
		if(days != 0){out += days +" dia"+((days!=1)?"s":"")+"";}
		if(days != 0 || hours != 0){out += hours +":"+((hours!=1)?"":"")+"";}
		if(days != 0 || hours != 0 || mins != 0){out += mins +":"+((mins!=1)?"":"")+"";}
		out += secs +"";

		document.getElementById('countbox').innerHTML='Tempo restante: ' + out;

		setTimeout("GetCount(1000)", 1000);
	}
}

window.onload=function(){GetCount(0);}//call when everything has loaded

//-->
</script>
<div id="countbox" class='textblk'></div>

<?php
	
}
	
function finalizarProva(){
	
	carregarRN(true);
	render(true);
	exit();
	
}

function carregarRN($finalizar = false){
	
	$prova = $_GET["id"];
	$usuario = page::$usuario;
	
	$respostas = array();

	//zera as pendentes
	$sql = "UPDATE col_perguntas_aplicadas SET IN_STATUS_PERGUNTA = 0 WHERE CD_PROVA = $prova AND CD_USUARIO = $usuario";
	DaoEngine::getInstance()->executeQuery($sql,true);

	
	foreach($_POST as $key => $valor){
		//echo "$key $valor \n";
		if (strlen($key) > 3){
			if (substr($key,0,3) == "rdo"){
				$pergunta = substr($key,3, strlen($key));
				$resposta = $valor;
				
				$pendente = 0;
				if (isset($_POST["chk$pergunta"])){
					$pendente = $_POST["chk$pergunta"];
				}
				
				$sql = "UPDATE col_perguntas_aplicadas SET CD_RESPOSTA = $resposta, IN_STATUS_PERGUNTA = $pendente WHERE CD_PROVA = $prova AND CD_USUARIO = $usuario AND CD_PERGUNTAS = $pergunta";
				DaoEngine::getInstance()->executeQuery($sql,true);

			}elseif (substr($key,0,3) == "chk"){
				
				$pendente = 0;
				$pergunta = substr($key,3, strlen($key));
				if (isset($_POST["chk$pergunta"])){
					$pendente = $_POST["chk$pergunta"];
				}
				
				$sql = "UPDATE col_perguntas_aplicadas SET IN_STATUS_PERGUNTA = $pendente WHERE CD_PROVA = $prova AND CD_USUARIO = $usuario AND CD_PERGUNTAS = $pergunta";
				DaoEngine::getInstance()->executeQuery($sql,true);
				
			}
		}
	}
		
	if ($finalizar){
		
		$sql = "SELECT COUNT(1) AS CERTAS FROM col_perguntas_aplicadas P INNER JOIN col_respostas R ON P.CD_RESPOSTA = R.CD_RESPOSTAS AND P.CD_PERGUNTAS = R.CD_PERGUNTAS WHERE P.CD_USUARIO = $usuario AND P.CD_PROVA = $prova AND R.IN_CERTA = 1";
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
		
		$linha = mysql_fetch_array($resultado);
		
		$corretas = $linha["CERTAS"];
		
		$media = $corretas / parametrosProva::$col_prova->NR_QTD_PERGUNTAS_TOTAL * 1000;
		
		$media = round($media) / 100;

		
		$sql = "UPDATE col_prova_aplicada SET DT_TERMINO = sysdate(), VL_MEDIA = $media WHERE CD_USUARIO = $usuario AND CD_PROVA = $prova";
		DaoEngine::getInstance()->executeQuery($sql,true);
		
		$sql = "UPDATE col_perguntas_aplicadas SET IN_STATUS_PERGUNTA = 0 WHERE CD_PROVA = $prova AND CD_USUARIO = $usuario";
		DaoEngine::getInstance()->executeQuery($sql,true);
		
		parametrosProva::$nota = $media;
		
		render(true);
		exit();
		
	}
	
}
	
function rodape($resultados = false){
	
?>	
					</TD>
				</TR>
				<TR class="tarjaTitulo">
						<TD colSpan="4"><IMG height="1" src="images/blank.gif" width="100"></TD>
				</TR>
				
			</TABLE>

			<?php if(!$resultados){ ?>
			
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD><IMG height="40" src="images/blank.gif" width="1"></TD>
					</TR>
					<TR>
						<TD align="middle">
							

						
							<input type="hidden" name="hdnVisualizar" value="<?php echo parametrosProva::$visualizar; ?>" />
							<input type="hidden" name="hdnFinalizar" value="0" />
							<?php
							
								$itensVisualizar = "Pendentes e Brancos";
								if(parametrosProva::$visualizar != ""){
									$itensVisualizar = "Todos";
								}
							
								$valorVisualizar = "1";
								if (parametrosProva::$visualizar!=""){
									$valorVisualizar = "";
								}
								
							?>
							
							
							<INPUT class="buttonsty" name="btnVisualizar" style="width: 210px"
								type="submit" value="Visualizar <?php echo $itensVisualizar; ?>" onclick="javascript:document.getElementById('hdnVisualizar').value = '<?php echo $valorVisualizar; ?>'; return true;"/>
								<IMG height="1" src="images/blank.gif" width="20"/>
						
							<INPUT class="buttonsty" name="btnGravar"
								type="submit" value="Salvar Respostas" title="Salvar as perguntas j� respondidas"/><IMG height="1" src="images/blank.gif" width="20"/>
							
							<INPUT class="buttonsty" name="btnFinalizar"
								type="submit" value="Finalizar prova" onclick="javascript:return confirm('Tem certeza que deseja finalizar a prova?')"/><BR>
						</TD>
					</TR>
			</TABLE>
			<?php } ?>
	
		</FORM>
		
	</BODY>
	
</HTML>

<?php
}


?>