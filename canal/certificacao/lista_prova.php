<?php

//include "../../include/security.php";
//include("../../include/defines.php");
//include('../../admin/framework/crud.php');
//include('../../admin/controles.php');
include('../admin/page.php');
//include "../../include/accesscounter.php";

obterSessaoEmpresa();

define(TITULO_PAGINA, "Avalia��es de Aprendizado");

include('certificacao/cabecalho_prova.php');

obterTextoComentario(basename("/lista_prova",".php"));

?>

<script language="javascript" type="text/javascript">

    function trocaAba(obj, prefixo, codigo)
    {

        //alert("");
        var itens = document.getElementById(prefixo).childNodes;

        var qtdItens = itens.length;

        for(var i = 0; i < qtdItens; i++)

        {

            if (itens[i].tagName == "LI")

            {

                itens[i].className = "";

            }

        }



        itens = document.getElementById("tabs" + prefixo).childNodes;

        qtdItens = itens.length;

        for(i = 0; i < qtdItens; i++)

        {

            if (itens[i].style)

            {

                itens[i].style.display = "none";

            }

        }



        try

        {

            obj.className = "selected";

            //alert(obj.reference);

            document.getElementById(prefixo + codigo).style.display = "block";

        }

        catch(e){}

    }


    function exibeAba(codigoAba, prefixo, obj){

        var divs = document.getElementsByTagName("div");

        for(var i = 0; i < divs.length; i++){
            //alert(divs[i].id.substr(0,5));
            var id = divs[i].id.substr(0,5);
            if (id == prefixo){
            //    alert(id);
                divs[i].style.display = "none";
            }
        }

        //alert(document.getElementById("ciclo" + codigoAba));
        document.getElementById(prefixo + codigoAba).style.display = "block";

        //document.getElementById("ciclo321").style.display = "inline";
        //document.getElementById("ciclo310").style.display = "none";
        //document.getElementById("ciclo326").style.display = "none";



        //alert(document.getElementById("ciclo" + codigoAba).style.display);

    }


</script>

<?php

dataGridProva(1);

include('certificacao/rodape_prova.php');

acertarSessaoEmpresa();

	function dataGridProva($usuario, $listaSimulados = false){
		
		global $tipo;
		global $empresaID;

        $usuario = page::$usuario;

        $ciclosLiberados = obterCiclosLiberados($usuario);
        $cicloAtual = obterCicloAtual($empresaID);
		
		$sqlWhereTipoProva = "((((pa.DT_TERMINO IS NULL AND (DT_INICIO IS NULL OR DATE_ADD(DT_INICIO, INTERVAL c.DURACAO_PROVA *(60) + 5 SECOND) >= sysdate())) OR $tipo > 3  
								OR (VL_MEDIA_APROVACAO > VL_MEDIA AND NOT (CONCAT(DATE_FORMAT(DT_INICIO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_INICIO_REALIZACAO, '%H%i')) <= DATE_FORMAT(DT_INICIO, '%Y%m%d%H%i') AND CONCAT(DATE_FORMAT(DT_TERMINO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_TERMINO_REALIZACAO, '%H%i')) >= DATE_FORMAT(DT_INICIO, '%Y%m%d%H%i')))
								) AND c.IN_PROVA = 1
								  AND NOT EXISTS (SELECT 1 FROM col_prova_aplicada pa1 INNER JOIN col_prova p1 ON p1.CD_PROVA = pa1.CD_PROVA WHERE pa1.CD_USUARIO = pa.CD_USUARIO AND pa1.CD_PROVA = c.CD_PROVA_SUPERIOR AND COALESCE(pa1.VL_MEDIA,0) >= p1.VL_MEDIA_APROVACAO)
								))";
		
		if ($listaSimulados)
		{
			$sqlWhereTipoProva = "c.IN_PROVA = 0 AND c.NR_TENTATIVA > pa.NR_TENTATIVA_USUARIO";

            //if($empresaID == 37){
                if(count($ciclosLiberados) > 0){
                    $ciclosSql = join(",", $ciclosLiberados);
                    $sqlWhereTipoProva = "$sqlWhereTipoProva AND c.CD_CICLO IN ($ciclosSql)";
                }
            //}

		}else{

            $obrigaDiagnostico = 0;
            $sql = "SELECT IN_OBRIGA_DIAGNOSTICO FROM col_empresa WHERE CD_EMPRESA = $empresaID";
            $RS_query = DaoEngine::getInstance()->executeQuery($sql, true);
            if($oRs = mysql_fetch_row($RS_query))
            {
                $obrigaDiagnostico = $oRs[0];
            }

            if ($obrigaDiagnostico == 1 && $tipo == 1){
                if(1==1){ // $empresaID == 37 OR $empresaID = 44){

                    //Obter as provas que podem ser exibidas:
                    $codigosPAs = obterPAs($empresaID);

                    if($codigosPAs == ""){
                        $codigosPAs = "0";
                    }

                    $ciclosSql = join(",", $ciclosLiberados);

                    if($ciclosSql == ""){
                        $ciclosSql = "0";
                    }

                    $sqlWhereTipoProva = " $sqlWhereTipoProva AND (c.CD_PROVA in ($codigosPAs) OR c.CD_CICLO IN ($ciclosSql)) ";

                }else{
                    $sql = "SELECT a.CD_PROVA FROM col_assessment a INNER JOIN col_prova_aplicada pa ON pa.CD_PROVA = a.CD_PROVA
                            WHERE pa.CD_USUARIO = $usuario AND pa.VL_MEDIA IS NULL";

                    //echo $sql;

                    $RS_query = DaoEngine::getInstance()->executeQuery($sql, true);
                    if($oRs = mysql_fetch_row($RS_query))
                    {
                        $sqlWhereTipoProva = " $sqlWhereTipoProva AND c.CD_PROVA = {$oRs[0]} ";
                    }

                }

            }

        }
	
		$rn = new col_prova();

		$sql = "SELECT DISTINCT
				DT_INICIO_REALIZACAO, DT_TERMINO_REALIZACAO, HR_INICIO_REALIZACAO, HR_TERMINO_REALIZACAO, DURACAO_PROVA, c.CD_PROVA, DS_PROVA, IN_SENHA_MESTRA, CD_CICLO
				 FROM col_prova c INNER JOIN col_prova_aplicada pa on pa.CD_PROVA = c.CD_PROVA INNER JOIN col_prova_chamada pc ON pc.CD_PROVA = c.CD_PROVA INNER JOIN col_prova_vinculo pv ON pv.CD_PROVA = c.CD_PROVA LEFT JOIN col_prova_cargo prc ON prc.CD_PROVA = c.CD_PROVA WHERE (pa.CD_USUARIO = $usuario or $tipo > 3) AND (pv.CD_VINCULO = $empresaID AND IN_TIPO_VINCULO = 1) AND c.IN_ATIVO = 1 AND (prc.NM_CARGO_FUNCAO = '{$_SESSION["cargofuncao"]}' or prc.NM_CARGO_FUNCAO is null or prc.NM_CARGO_FUNCAO = '')
				AND CONCAT(DATE_FORMAT(DT_INICIO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_INICIO_REALIZACAO, '%H%i')) <= DATE_FORMAT(sysdate(), '%Y%m%d%H%i') AND CONCAT(DATE_FORMAT(DT_TERMINO_REALIZACAO, '%Y%m%d') , DATE_FORMAT(HR_TERMINO_REALIZACAO, '%H%i')) >= DATE_FORMAT(sysdate(), '%Y%m%d%H%i')
				AND (($sqlWhereTipoProva))		
				ORDER BY DS_PROVA, DT_INICIO_REALIZACAO, HR_INICIO_REALIZACAO";
		//AND ((pa.DT_TERMINO IS NULL AND c.IN_PROVA = 1) OR (c.IN_PROVA = 0 AND c.NR_TENTATIVA > pa.NR_TENTATIVA_USUARIO))		
		//$sql = "SELECT * FROM col_prova c INNER JOIN col_prova_aplicada pa on pa.CD_PROVA = c.CD_PROVA WHERE pa.CD_USUARIO = $usuario AND c.IN_ATIVO = 1 ORDER BY DT_REALIZACAO, HORARIO_INICIO_DISPONIVEL";
		//echo "<!--$sql-->";
		$resultado = DaoEngine::getInstance()->executeQuery($sql);
        //echo $sql;

        $ciclos = obterCiclos($empresaID);

        $complementoCiclo = "prova";
        if ($listaSimulados){
            $complementoCiclo = "simul";
        }


        //Implementa��o para Programa vendas
        if($empresaID == 44 && $tipo == 1){
            $sqlUsuario = "SELECT DATE_FORMAT(datacadastro,'%Y%m%d') FROM col_usuario WHERE CD_USUARIO = $usuario";
            $resultadoUsuario = DaoEngine::getInstance()->executeQuery($sqlUsuario);
            $linhaUsuario = mysql_fetch_row($resultadoUsuario);
            if($linhaUsuario[0] < "20120904"){ //Se for cadastrado no ciclo 1
                $cicloAtual = 0;
                unset($ciclos[1]);
            }else{ //Sen�o, � ciclo 2
                unset($ciclos[0]);
            }

        }


?>

    <div class="productDetails" style="width: 100%">

        <ul class="tabs" id="<?php echo $complementoCiclo; ?>" style="font-size: 11px">

            <?php
                if($cicloAtual == 0)
                    $cicloAtual = $ciclos[0]["CD_CICLO"];

                foreach($ciclos as $ciclo){
                    $selecionado = "";
                    if($cicloAtual == $ciclo["CD_CICLO"]){
                        $selecionado = " class=\"selected\"";
                    }

                    echo "<li $selecionado onclick=\"javascript:trocaAba(this,'$complementoCiclo', {$ciclo["CD_CICLO"]});\"><a>{$ciclo["NM_CICLO"]}<br />&nbsp;</a></li>";
                }

            ?>

        </ul>

        <div class="boxR1">

            <div class="boxR12">

                <div class="boxR13">

                    <div class="boxR14">

                        <div class="activeTabs">

                            <div class="contTabs" id="<?php echo "tabs$complementoCiclo"; ?>" style=" margin-left:10px; margin-right:10px">


<?php

                                foreach($ciclos as $ciclo){

                                    $exibir = "";
                                    if($cicloAtual != $ciclo["CD_CICLO"]){
                                       $exibir = "style=\"display:none;\"";
                                    }

                                    echo "<div id=\"$complementoCiclo{$ciclo["CD_CICLO"]}\" $exibir class=\"conteudo\">";

                                    //echo $ciclo["CD_CICLO"];

                                    mysql_data_seek($resultado, 0);

                                    $possuiTeste = false;
                                    while ($linha = mysql_fetch_array($resultado)){
                                        if ($linha["CD_CICLO"] == $ciclo["CD_CICLO"]){
                                            $possuiTeste = true;
                                        }
                                    }

                                    mysql_data_seek($resultado, 0);

                                    //if(mysql_num_rows($resultado) == 0){
                                    if(!$possuiTeste){
                                        if ($listaSimulados)
                                            echo "<div class='textblk' style='width:100%;text-align:center'><b>Nenhum simulado dispon�vel.</b></div>";
                                        else
                                            echo "<div class='textblk' style='width:100%;text-align:center'><b>Nenhum teste dispon�vel.</b></div>";

                                        echo "</div>";
                                        continue;
                                    }

                                    echo '                        <TABLE cellpadding="0" cellspacing="0" border="0" width="98%">
                                            <TR class="tarjaItens">
                                                <TD class="title">T�tulo</TD>
                                                <TD class="title" align="center">In�cio</TD>
                                                <TD class="title" align="center">T�rmino</TD>
                                                <TD class="title" align="center">Hora In�cio</TD>
                                                <TD class="title" align="center">Hora T�rmino</TD>
                                                <TD class="title" align="center">Quest�es</TD>
                                                <TD class="title" align="center">Tempo</TD>
                                                <TD class="title">&nbsp;</TD>
                                            </TR>';

                                    while ($linha = mysql_fetch_array($resultado)){

                                        if ($linha["CD_CICLO"] != $ciclo["CD_CICLO"])
                                            continue;

                                        $data = page::formataPropriedade($linha["DT_INICIO_REALIZACAO"], DaoEngine::DATA);
                                        $dataFim = page::formataPropriedade($linha["DT_TERMINO_REALIZACAO"], DaoEngine::DATA);
                                        $inicio = page::formataPropriedade($linha["HR_INICIO_REALIZACAO"], DaoEngine::DATA_HORA_MINUTO);
                                        $fim = page::formataPropriedade($linha["HR_TERMINO_REALIZACAO"], DaoEngine::DATA_HORA_MINUTO);
                                        $duracao = $linha["DURACAO_PROVA"];

                                        $sqlQuestoes = "SELECT SUM(NR_QTD_PERGUNTAS_NIVEL_1) + SUM(NR_QTD_PERGUNTAS_NIVEL_2) + SUM(NR_QTD_PERGUNTAS_NIVEL_3) as Total
                                    FROM col_prova_disciplina WHERE CD_PROVA = {$linha["CD_PROVA"]}";

                                        $resultadoQuestoes = DaoEngine::getInstance()->executeQuery($sqlQuestoes);

                                        $quest�es = 0;

                                        if ($linhaQuestoes = mysql_fetch_row($resultadoQuestoes))
                                        {
                                            $quest�es = $linhaQuestoes[0];
                                        }

                                        $linkProva = "<a href='certificacao/prova.php?id={$linha["CD_PROVA"]}'>Iniciar</a>";
                                        $hrefProva = "<a href='certificacao/prova.php?id={$linha["CD_PROVA"]}'>{$linha["DS_PROVA"]}</a>";
                                        if ($linha["IN_SENHA_MESTRA"] == 1)
                                        {
                                            $linkProva = "<a href='javascript:iniciaAvaliacao({$linha["CD_PROVA"]})'>Iniciar</a>";
                                            $hrefProva = "<a href='javascript:iniciaAvaliacao({$linha["CD_PROVA"]})'>{$linha["DS_PROVA"]}</a>";

                                        }

                                        echo "<TR style='height: 20px;'>
                        <TD class='textblk' style='text-align:left;'>$hrefProva</TD>
                        <TD class='textblk' style='text-align:center;'>{$data}</TD>
                        <TD class='textblk' style='text-align:center;'>{$dataFim}</TD>
                        <TD class='textblk' style='text-align:center;'>{$inicio}</TD>
                        <TD class='textblk' style='text-align:center;'>{$fim}</TD>
                        <TD class='textblk' style='text-align:center;'>{$quest�es}</TD>
                        <TD class='textblk' style='text-align:center;'>{$duracao}</TD>
                        <TD class='textblk' style='text-align:left;'>$linkProva</TD>
                        </TR>";

                                    }


                                    echo '						</TABLE>';

                                    echo "</div>";

                                }


?>


                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>



    </div>



<?php


	}





?>

