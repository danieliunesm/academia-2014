<?php
include "../include/security.php";
include "../include/defines.php";
include "../include/dbconnection.php";
include "../include/genericfunctions.php";
include "../include/accesscounter.php";
include('../admin/framework/crud.php');
include('../admin/controles.php');


obterSessaoEmpresa();

$empresaID = isset($_SESSION["empresaID"])? $_SESSION["empresaID"] : 0;

$codigoPesquisa = $_REQUEST["cp"];

?> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Canal de Comunica��o Colabor&aelig;</title>
<meta http-equiv=pragma content=no-cache>
<link type="text/css" rel="stylesheet" media="all" href="abas.css">
<!--[if lte IE 8]><link type="text/css" rel="stylesheet" media="all" href="abasIE.css"><![endif]-->
<link rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">

<style type="text/css">
body{background:url(images/empresas/logo<?php echo $empresaID; ?>.png) no-repeat fixed center; margin-top: 20px;}
.conteudo, div, td {
	font-size: 11px;
	font-family: tahoma,arial,helvetica,sans-serif;
	color: #333;

}
.conteudo .text{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.conteudo .textbold{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:bold;color:#000000}
.conteudo .textblk{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.conteudo .rodape{font-family:"tahoma","arial","helvetica",sans-serif;font-size:10px;font-weight:normal;color:#666666}
.conteudo .legenda{font-family:"arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#666666}
.conteudo .legendablk{font-family:"arial","helvetica",sans-serif;font-size:11px;font-weight:normal;color:#000000}
.conteudo .assinatura{font-family:"tahoma","arial","helvetica",sans-serif;font-size:11px;font-weight:normal;font-style:italic;color:#000000}
.conteudo .title{font-family:"tahoma","arial","helvetica",sans-serif;font-size:9pt;font-weight:bold;color:#000000}

.conteudo a{font-family:tahoma,arial,helvetica,sans-serif;font-size:11px;font-weight:normal;color:#000066;text-decoration:none}
.conteudo a:hover{color:#000066}

</style>
<script type="text/javascript" src="include/js/functions.js"></script>
<script language="JavaScript" src="../include/js/ranking.js"></script>

</head>

<body>
<div width="100%" align="center">
<form name="frmPesquisa" method="post" action="respondePesquisa.php">
<table cellpadding="2" cellspacing="2" border="0" width="500">
	<tr class="textblk">
		<td align="left">
<?php
obterTextoComentario(basename("/pesquisa",".php"));
?>
		</td>
	</tr>
<?php 

$codigoEmpresa = $_SESSION["empresaID"];
$codigoUsuario = $_SESSION["cd_usu"];

$sql = "
		SELECT
			p.CD_PESQUISA, CD_PERGUNTA_PESQUISA, DS_PERGUNTA,
			IF(EXISTS(SELECT 1 FROM col_usuario_pesquisa up WHERE up.CD_PESQUISA = p.CD_PESQUISA AND up.CD_EMPRESA = p.CD_EMPRESA AND up.CD_USUARIO = $codigoUsuario),1,0) JA_RESPONDEU
		FROM
			col_pesquisa p
			INNER JOIN col_pergunta_pesquisa pp ON pp.CD_PESQUISA = p.CD_PESQUISA AND pp.CD_EMPRESA = p.CD_EMPRESA
		WHERE
			p.CD_EMPRESA = $codigoEmpresa
			AND   p.CD_PESQUISA = $codigoPesquisa
		ORDER BY
			pp.NR_ORDEM";

$resultado = DaoEngine::getInstance()->executeQuery($sql, true);

$alerta = "";

?>

<?php

if (mysql_num_rows($resultado) == 0){
	$alerta = "N�o existe pesquisa formulada para o programa.";
}
else 
{
	
	
?>

	<tr class="textblk">
		<td colspan="2"><b>Avalie as seguintes quest�es:</b></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>

<?php 
	$codigoPesquisa = 0;
	$jaParticipou = false;
	while($linha = mysql_fetch_array($resultado))
	{
		
		if ($linha["JA_RESPONDEU"] > 0)
		{
			$alerta = "Obrigado por ter participado da nossa pesquisa.";
			$jaParticipou = true;
			break;
		}
		
		$codigoPesquisa = $linha["CD_PESQUISA"];
		
		?>
		
		
			<tr class="textblk">
				<td style="text-align:justify"><?php echo $linha["DS_PERGUNTA"];?></td>
				<?php $nomeControle = "cboResposta_{$linha["CD_PERGUNTA_PESQUISA"]}"?>
				<td width="50" align="right">
					<select id="<?php echo $nomeControle;?>" name="<?php echo $nomeControle;?>">
						<option value="0"></option>
						<option value="5">5</option>
						<option value="4">4</option>
						<option value="3">3</option>
						<option value="2">2</option>
						<option value="1">1</option>
					</select>
				</td>
			
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		
		
		<?php
		
	}
	if (!$jaParticipou)
	{
?>
	
	<tr class="textblk">
		<td colspan="2">
			<input type="hidden" name="hdnPesquisa" value="<?php echo $codigoPesquisa;?>" />
            Comente a sua resposta abaixo (em at� 150 caracteres):
		</td>
	</tr>
	<tr class="textblk">
		<td colspan="2">
			<input type="text" name="txtComentario" maxlength="150" style="width: 100%" />
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="submit" value="Enviar" id="btnEnviar" name="btnEnviar" class="buttonsty" />
		</td>
	</tr>
	
<?php
	} 
}

if ($alerta != "")
{
	echo "<tr class=\"textblk\" align=\"center\"><td>$alerta</td></tr>";
}

?>

</table>
</form>
</div>

<div id="footer" style="position:relative;top:0px;margin:20px auto 20px auto">
<div class="hrblue"></div>
<p>&copy; Colabor&aelig; <?php echo date("Y");?></p>
</div>

</body>
</html>

<?php

acertarSessaoEmpresa();

?>