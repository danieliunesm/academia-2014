<script type="text/javascript">

        function getElementsByName(name){

        var elementos = document.getElementsByName(name);

        if(elementos.length == 0){

            var temp = document.all;

            var matches = [];
            for(var i=0;i<temp.length;i++){
                if (temp[i].getAttribute("name") == name) {
                    matches.push(temp[i]);
                }
            }

            elementos = matches;

        }

        return elementos;
        
    }

    function escondeMostraCurso(curso){
        
        var elementos = getElementsByName("trcurso" + curso);
        var qtdElementos = elementos.length;
        
        for(var i = 0; i < qtdElementos; i++){
            if(elementos[i].style.display == "none")
                elementos[i].style.display = "";
            else
                elementos[i].style.display = "none";
        }

    }

</script>
<p style="font-size: 13px; font-weight: bold">Clique na �rea de conhecimento e curso de prefer�ncia.</p>
<?php

$empresaID = $_SESSION["empresaID"];
/*
$sql = "SELECT
            pi.ID, pi.Titulo, p.CD_PROVA, p.DS_PROVA, pa.DT_TERMINO, ce.TX_COR, ae.TX_COR AS COR_AREA, ae.CD_AREA_CONHECIMENTO, ac.NM_AREA_CONHECIMENTO, pi.IN_PA
        FROM
            tb_pastasindividuais pi
            INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pi.ID
            INNER JOIN col_area_conhecimento_empresa ae ON ae.CD_EMPRESA = ce.CD_EMPRESA AND ae.CD_AREA_CONHECIMENTO = pi.CD_AREA_CONHECIMENTO
            INNER JOIN col_area_conhecimento ac ON ac.CD_AREA_CONHECIMENTO = ae.CD_AREA_CONHECIMENTO
            INNER JOIN col_prova p ON p.ID_PASTA_INDIVIDUAL = pi.ID
            INNER JOIN col_assessment a ON a.CD_PROVA = p.CD_PROVA
            LEFT JOIN col_prova_aplicada pa ON pa.CD_PROVA = p.CD_PROVA AND pa.CD_USUARIO = {$_SESSION['cd_usu']}
        WHERE
            ce.CD_EMPRESA = $empresaID
        AND pi.Status = 1
        ORDER BY
          IF(pa.DT_TERMINO IS NOT NULL, ce.NR_ORDEM, 99999),
          ae.NR_ORDEM,
          ce.NR_ORDEM,
          pi.Titulo";
*/
$sql = "SELECT
            pi.ID,
            pi.Titulo,
            p.CD_PROVA,
            p.DS_PROVA,
            pa.DT_TERMINO,
            ce.TX_COR,
            ae.TX_COR AS COR_AREA,
            ae.CD_AREA_CONHECIMENTO,
            ac.NM_AREA_CONHECIMENTO,
            pi.IN_PA,
            ae.NR_ORDEM,
            ce.NR_ORDEM AS NR_ORDEM_CURSO,
            0 AS ORDENACAO,
            pa.VL_MEDIA,
            (SELECT AVG(pa1.VL_MEDIA) FROM col_prova_aplicada pa1 INNER JOIN col_prova p1 ON p1.CD_PROVA = pa1.CD_PROVA WHERE pa1.CD_USUARIO = pa.CD_USUARIO AND p1.IN_PROVA = 1 AND p1.ID_PASTA_INDIVIDUAL = pi.ID AND p1.CD_PROVA <> p.CD_PROVA) AS VL_MEDIA_AVALIACAO
        FROM
            tb_pastasindividuais pi
        INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pi.ID
        INNER JOIN col_area_conhecimento_empresa ae ON ae.CD_EMPRESA = ce.CD_EMPRESA
        AND ae.CD_AREA_CONHECIMENTO = pi.CD_AREA_CONHECIMENTO
        INNER JOIN col_area_conhecimento ac ON ac.CD_AREA_CONHECIMENTO = ae.CD_AREA_CONHECIMENTO
        INNER JOIN col_prova p ON p.ID_PASTA_INDIVIDUAL = pi.ID
        INNER JOIN col_assessment a ON a.CD_PROVA = p.CD_PROVA
        LEFT JOIN col_prova_aplicada pa ON pa.CD_PROVA = p.CD_PROVA
        AND pa.CD_USUARIO = {$_SESSION['cd_usu']}
        WHERE
            ce.CD_EMPRESA = $empresaID
            AND pa.DT_TERMINO IS NOT NULL
        AND pi. STATUS = 1

        UNION ALL

        SELECT
            pi.ID,
            pi.Titulo,
            p.CD_PROVA,
            p.DS_PROVA,
            pa.DT_TERMINO,
            ce.TX_COR,
            ae.TX_COR AS COR_AREA,
            ae.CD_AREA_CONHECIMENTO,
            ac.NM_AREA_CONHECIMENTO,
            pi.IN_PA,
            ae.NR_ORDEM,
            ce.NR_ORDEM AS NR_ORDEM_CURSO,
            1 AS ORDENACAO,
            pa.VL_MEDIA,
            (SELECT AVG(pa1.VL_MEDIA) FROM col_prova_aplicada pa1 INNER JOIN col_prova p1 ON p1.CD_PROVA = pa1.CD_PROVA WHERE pa1.CD_USUARIO = pa.CD_USUARIO AND p1.IN_PROVA = 1 AND p1.ID_PASTA_INDIVIDUAL = pi.ID AND p1.CD_PROVA <> p.CD_PROVA) AS VL_MEDIA_AVALIACAO
        FROM
            tb_pastasindividuais pi
        INNER JOIN col_curso_empresa ce ON ce.ID_PASTA_INDIVIDUAL = pi.ID
        INNER JOIN col_area_conhecimento_empresa ae ON ae.CD_EMPRESA = ce.CD_EMPRESA
        AND ae.CD_AREA_CONHECIMENTO = pi.CD_AREA_CONHECIMENTO
        INNER JOIN col_area_conhecimento ac ON ac.CD_AREA_CONHECIMENTO = ae.CD_AREA_CONHECIMENTO
        INNER JOIN col_prova p ON p.ID_PASTA_INDIVIDUAL = pi.ID
        INNER JOIN col_assessment a ON a.CD_PROVA = p.CD_PROVA
        LEFT JOIN col_prova_aplicada pa ON pa.CD_PROVA = p.CD_PROVA
        AND pa.CD_USUARIO = {$_SESSION['cd_usu']}
        WHERE
            ce.CD_EMPRESA = $empresaID
        AND pi. STATUS = 1
        ORDER BY

            ORDENACAO,
         NR_ORDEM,
         NR_ORDEM_CURSO,
         Titulo";

$resultado = DaoEngine::getInstance()->executeQuery($sql);

?>
    <table cellpadding="3" cellspacing="2" border="0" width="100%" align="center">
        <tr class="tarjaItens" style='height: 30px;'>
            <td class='textblk' style='text-align:left; font-weight: bold; width: 350px;'>�rea de Conhecimento<a class='fancy' href='../exerciciosdecampo/saibamais/info/visaogeral.JPG' target='areaconhecimento'><img height='14px' style='vertical-align: middle; float: right' src='../exerciciosdecampo/buttons/botao_saibamais.png'></a></td>
            <td class="textblk" style='text-align:left; font-weight: bold; width: 330px;'>Curso</td>
            <!-- <td class="textblk" style='text-align:left; font-weight: bold;'>Resumo</td> -->
            <td class="textblk" style='text-align:left; font-weight: bold;'>Teste Inicial</td>
            <td class="textblk" style='text-align:center; font-weight: bold;'>PA</td>
            <td class="textblk" style='text-align:center; font-weight: bold;'>AA</td>
        </tr>

<?php

$area_conhecimentoid = -1;

$cursos = "";
$qtdCursos = 0;
$notaPACursos = null;
$notaAACursos = null;
$qtdAACursos = 0;

while($linhaCurso=mysql_fetch_array($resultado)){

    $cor = "";

    if($linhaCurso["ORDENACAO"] == 0){

        if($area_conhecimentoid == -1){

            $area_conhecimentoid = 0;
            $cor = "background: #ffe4a0;";

            $cursos = "$cursos<tr style='cursor: default; height: 25px; $cor' onclick='javascript:escondeMostraCurso($area_conhecimentoid)'>";
            $cursos = "$cursos<td class='textblk' style='cursor: default; text-align:left; $cor' >Minha Trilha</td>";
            $cursos = "$cursos<td colspan='2' class='textblk' style='cursor: default; text-align:left; $cor'>&nbsp;</td>";
            $cursos = "$cursos<td class='textblk' style='cursor: default; text-align:center; $cor'>[PA$area_conhecimentoid]</td>";
            $cursos = "$cursos<td class='textblk' style='cursor: default; text-align:center; $cor'>[AA$area_conhecimentoid]</td>";
            $cursos = "$cursos</tr>";

        }

    }elseif($area_conhecimentoid != $linhaCurso["CD_AREA_CONHECIMENTO"]){

        if(!$notaPACursos === null){
            $notaPACursos = $notaPACursos / $qtdCursos;
        }
        $cursos = str_replace("[PA$area_conhecimentoid]", formatarValores($notaPACursos), $cursos);
        $notaPACursos = null;
        $qtdCursos = 0;

        if(!$notaAACursos === null){
            $notaAACursos = $notaAACursos / $qtdAACursos;
        }
        $cursos = str_replace("[AA$area_conhecimentoid]", formatarValores($notaAACursos), $cursos);
        $notaAACursos = null;
        $qtdAACursos = 0;

        $cor = "background: " . $linhaCurso["COR_AREA"] . ";";
        $area_conhecimentoid = $linhaCurso["CD_AREA_CONHECIMENTO"];

        $cursos = "$cursos<tr style='cursor: default; height: 25px; $cor' onclick='javascript:escondeMostraCurso($area_conhecimentoid)'>";
        $cursos = "$cursos<td class='textblk' style='cursor: default; text-align:left; $cor' > {$linhaCurso["NM_AREA_CONHECIMENTO"]}<a class='fancy' href='saibamais.php?id=$area_conhecimentoid' target='areaconhecimento'><img height='14px' style='vertical-align: middle; float: right' src='../exerciciosdecampo/buttons/botao_saibamais.png'></a></td>";
        $cursos = "$cursos<td colspan='2' class='textblk' style='cursor: default; text-align:left; $cor'></td>";
        $cursos = "$cursos<td class='textblk' style='cursor: default; text-align:center; $cor'>[PA$area_conhecimentoid]</td>";
        $cursos = "$cursos<td class='textblk' style='cursor: default; text-align:center; $cor'>[AA$area_conhecimentoid]</td>";
        $cursos = "$cursos</tr>";


    }

    if($linhaCurso["DT_TERMINO"] != ""){
        $qtdCursos++;
        if($notaPACursos === null){
            $notaPACursos = $linhaCurso["VL_MEDIA"];
        }else{
            $notaPACursos = $notaPACursos + $linhaCurso["VL_MEDIA"];
        }
    }

    if($linhaCurso["VL_MEDIA_AVALIACAO"] != null){
        $qtdAACursos++;
        if($notaAACursos === null){
            $notaAACursos = $linhaCurso["VL_MEDIA_AVALIACAO"];
        }else{
            $notaAACursos = $notaAACursos + $linhaCurso["VL_MEDIA_AVALIACAO"];
        }
    }

    $target = "target='_top'";
    $multi = "&multi=1";
    $cor = "";

    if($linhaCurso["TX_COR"] != ""){
        $cor = "background: {$linhaCurso["TX_COR"]};";

    }

    $linkProva = "<a href='certificacao/prova.php?id={$linhaCurso["CD_PROVA"]}&all=1$multi&mode=1' $target $cor>Fazer Teste Inicial</a>";

    if($linhaCurso["DT_TERMINO"] != ""){
        $linkProva = "<span $cor>Curso j� iniciado</span>";
    }elseif($linhaCurso["IN_PA"] == 0){
        $linkProva = "&nbsp;";
    }

    $display = "display: none;";
    $nomeArea = "&nbsp;";
    $corLink = "";
    if($area_conhecimentoid == 0){
        $display = "";
        $nomeArea = $linhaCurso["NM_AREA_CONHECIMENTO"];
    }elseif($area_conhecimentoid != 0 && $linhaCurso["DT_TERMINO"] != ""){
        $corLink = "style = 'color: #636356';";
    }

    $cursos = "$cursos<tr name='trcurso$area_conhecimentoid' style='height: 20px; $display'>";

    $cursos = "$cursos<td class='textblk' style='text-align:left; $cor'>$nomeArea</td>";

    $cursos = "$cursos<td class='textblk' style='text-align:left; $cor' ><a href='../paginaindividualprivada.php?pasta={$linhaCurso["ID"]}' onclick='javascript:mudaAba(\"tabCont08\");' target='iconteudo' $corLink>{$linhaCurso["Titulo"]}</a></td>";

    // echo "<td class='textblk' style='text-align:left; $cor' ><a href='../paginaindividualprivada.php?pasta={$linhaCurso["ID"]}' onclick='javascript:mudaAba(\"tabCont08\");' target='iconteudo' $corLink>Resumo do Curso</a></td>";

    $cursos = "$cursos<td class='textblk' style='text-align:left; $cor' >$linkProva</td>";

    $cursos = "$cursos<td class='textblk' style='text-align:center; $cor' >" . formatarValores($linhaCurso["VL_MEDIA"]) . "</td>";

    $cursos = "$cursos<td class='textblk' style='text-align:center; $cor' >" . formatarValores($linhaCurso["VL_MEDIA_AVALIACAO"]) . "</td>";

    $cursos = "$cursos</tr>";

}

if(!$notaPACursos === null){
    $notaPACursos = $notaPACursos / $qtdCursos;
}
$cursos = str_replace("[PA$area_conhecimentoid]", formatarValores($notaPACursos), $cursos);

if(!$notaAACursos === null){
    $notaAACursos = $notaAACursos / $qtdAACursos;
}
$cursos = str_replace("[AA$area_conhecimentoid]", formatarValores($notaAACursos), $cursos);

echo $cursos;

?>

    </table>

<?php

function formatarValores($valor, $decimal = 2, $seVazio = "-"){

    if($valor === null || $valor === ""){
        return $seVazio;
    }

    return number_format(round($valor,$decimal), $decimal, ",", "");


}


?>