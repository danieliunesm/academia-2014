<?php
include "../include/security.php";
include "../include/genericfunctions.php";
include("../include/defines.php");
include('../admin/framework/crud.php');
include('../admin/controles.php');
include('../admin/page.php');
include "../include/accesscounter.php";

if (!($_SESSION["tipo"] >=4))
	header("Location:gestao_programa_participante.php");

$empresa = $_SESSION["empresaID"];

$nomeParticipante = (isset($_REQUEST["txtNome"])) ? $_REQUEST["txtNome"] : "";
$loginParticipante = (isset($_REQUEST["txtLogin"])) ? $_REQUEST["txtLogin"] : "";

$sql = "SELECT CD_USUARIO, login, NM_USUARIO, NM_SOBRENOME FROM col_usuario WHERE empresa = $empresa
AND
	(NM_USUARIO LIKE '$nomeParticipante%' OR NM_SOBRENOME LIKE '$nomeParticipante%' OR '$nomeParticipante' = '')
AND
	(login like '$loginParticipante%' OR '$loginParticipante' = '')
ORDER BY NM_USUARIO, NM_SOBRENOME, login";

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Colabor� - Consultoria e Treinamento em Telecom</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META http-equiv="pragma" content="no-cache">
		<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">
		<script type="text/javascript" src="include/js/functions.js"></script>
		<script language="JavaScript" src="../include/js/ranking.js"></script>
		<script language="javascript">
			function abreObs(id){
				
				var url = 'observacao.php?id=' +id;
				newWin=null;
				var w=400;
				var h=300;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				newWin=window.open(url,'menueditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=0,resizable=0');
				if(newWin!=null)setTimeout('newWin.focus()',100);
			
				
			}
			
			function abreRelatorio(rel, dataInicio, dataTermino, filtro, args)
			{
				
				url = rel + "?txtDe=" + dataInicio + "&txtAte=" + dataTermino + "&id=" + filtro + args;
				
				newWin=null;
				var w= 900;
				var h= 600;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				
				newWin=window.open(url,'relatorio','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
				
			}
		
		</script>
	</HEAD>
	
	<BODY>
	<!-- Inicio do T�tulo -->
		<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
			<TR>
				<TD><IMG height="2" src="images/blank.gif" width="100%"/></TD>
			</TR>
			<TR>
				<TD background="images/bg_logo_admin.png">
					<TABLE cellpadding="0" cellspacing="0" width="663" border="0"><!-- background="../images/logo_prova_simulado.png" -->
						<TR>
							<TD><IMG src="images/blank.gif" height="32" width="1"/></TD>
							<TD class="data" align="right"><?php echo getServerDate(); ?></TD>
						</TR>
					
					</TABLE>
				
				</TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" width="100%" height="2" /></TD>
			</TR>
			<TR>
				<TD bgcolor="#cccccc"><IMG src="images/blank.gif" height="3" width="100%"/></TD>
			</TR>
		</TABLE>
		<!-- Fim do T�tulo -->
		
		<!-- In�cio Cabe�alho -->
		<TABLE cellspacing="0" cellpadding="0" width="756" align="center" border="0">
			<TR>
				<TD width="1%"><IMG height="20" src="images/blank.gif" width="289"/></TD>
				<TD></TD>
				<TD></TD>
			</TR>
			<TR valign="top">
				<td class="textblk">Usu�rio &raquo; <strong><?php echo strtoupper($_SESSION['alias']); ?></strong></td>
				<TD width="1%"><IMG height="20" src="images/blank.gif" width="150"/></TD>
				<TD align="right" width="98%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href = '/canal/index.php'"
							type="button" value="Voltar"></TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" height="4" width="1" /></TD>
			</TR>
		</TABLE>
		<BR>
		<!-- Fim Cabe�alho -->
		
		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="GET">
					
			<TABLE cellpadding="0" cellspacing="0" width="756" align="center" border="0">
				<TR class="tarjaTitulo">
						<TD align="middle" height="20">Radar do Programa - <?php echo "Sele��o de Participante para Visualiza��o"; ?></TD>
				</TR>
				<TR>
					<TD><IMG src="images/blank.gif" height="1" width="10"/></TD>
				</TR>
				<TR>
					<TD width="100%">

					<?php
						obterTextoComentario(basename($PHP_SELF,".php"));
					?>
						
						<p>
						<table cellpadding="0" cellspacing="0" border="0" >
							<tr>
								<td class="textblk" width="220">
									Nome do Participante:
								</td>
								<td class="textblk" width="170">
									Login do Participante:
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									<?php textbox("txtNome", "100", $nomeParticipante, "200", "textbox3"); ?>
								</td>
								<td>
									<?php textbox("txtLogin", "50", $loginParticipante, "150", "textbox3"); ?>
								</td>
								<td align="center">
									<input type='submit' name='btnPesquisa' class='buttonsty' value='Filtrar'>
								</td>
							</tr>
						</table>
						</p>
						<?php
						
						
						
						echo '       <TABLE cellpadding="5" cellspacing="0" border="0" width="100%">
						                <TR class="tarjaItens">
						                    <TD class="title">Participantes</TD>
						                </TR>';
						
						$bgcolor = "#f1f1f1";
						
						while ($linha = mysql_fetch_array($resultado)) {

							if ($bgcolor == "#f1f1f1")
								$bgcolor = "#ffffff";
							else 
								$bgcolor = "#f1f1f1";
								
							echo "	<tr align=\"left\" class=\"textblk\" bgcolor=\"$bgcolor\">
											<td>
											<a href=\"javascript:document.location.href='gestao_programa_participante.php?hdnParticipante={$linha["CD_USUARIO"]}'\">
												{$linha["login"]} / {$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}
											</a>
											</td>
										</tr>";

						}
								
							
							
										
							?>
						
						</table>
					
					</TD>
				</TR>

				
			</TABLE>
			</p>
		</FORM>
		
<div id="footer" style="position:relative;top:0px;margin:20px auto 20px auto">
<div class="hrblue"></div>
<p>&copy; Colabor&aelig; <?php echo date("Y");?></p>
</div>
		
	</BODY>
	
</HTML>


