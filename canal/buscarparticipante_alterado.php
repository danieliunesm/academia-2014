<?php 
include "../include/security.php";
include "../include/defines.php";
include "../include/genericfunctions.php";
include "../include/accesscounter.php";
include "../include/cripto.php";
include('../admin/controles.php');
include('../admin/framework/crud.php');

if (count($_POST) == 0) exit();
$tipo = $_SESSION["tipo"];

function ehExcel()
{
	return $_POST["rdoExcel"] == 2;
}

function exibeChecks()
{
	global $tipo;
	if ($tipo == -1 || $tipo > 4) {
		return (!ehExcel());
	}
	
	return false;
	
}

if (ehExcel())
{
	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=Cadastro.xls");
	
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<?php 
if (!(ehExcel()))
{
?>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<style type="text/css">
body{
	SCROLLBAR-FACE-COLOR: #fff;
	SCROLLBAR-HIGHLIGHT-COLOR: #fff;
	SCROLLBAR-SHADOW-COLOR: #ccc;
	SCROLLBAR-3DLIGHT-COLOR: #ccc;
	SCROLLBAR-ARROW-COLOR: #000;
	SCROLLBAR-TRACK-COLOR: #ccc;
	SCROLLBAR-DARKSHADOW-COLOR: #ccc;
	SCROLLBAR-BASE-COLOR: #fff;
}
</style>
<style type="text/css">
@import "include/css/tablepage.css";
@import "include/css/table.css";
@import "include/css/tablecustom.css";
</style>
<?php 
}
else
{
?>
<style type="text/css">
#tabelaCadastro{
	border-collapse:collapse;
}

#tabelaCadastro th{
	background-color:#ccc;
	border: 1px solid #666;
	padding:0 10px 0 10px;
}

#tabelaCadastro td{
	border: 1px solid #ccc;
	padding:0 20px 0 10px;
}
</style>
<?php
}
?>
<script type="text/javascript" language="javascript" src="include/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="include/js/jquery.dataTables.js"></script>
<script type="text/javascript">
<?php 
if (!(ehExcel()))
{
?>
	$(document).ready(function() {
		parent.document.getElementById('ifrUsuarios').style.display = 'block';
		$('#tabelaCadastro').dataTable( {
			"sScrollY": 300,
			"sScrollX": "100%",
			"sScrollXInner": "110%",
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"aoColumns": [null,null,null,null,null,null,null,null,{"bSortable":false}]
		} );
	} );
<?php 
}
?>
</script>
</head>
<body id="dt_example" style="background-color:#ffffff">
	<?php 
	if (!(ehExcel()))
	{
	?>
	<form name="bloqueioForm" enctype="multipart/form-data" method="post" action="bloqueio.php" target="ifrBloqueio">
	<input type="hidden" id="hdnBloqueio" name="hdnBloqueio" />
	<?php 
	}
	?>
<div id="container">
<div class="demo_jui">	
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tabelaCadastro">
	<thead>
		<tr>
			<th>Diretor Business</th>
			<th>Gerente</th>
			<th>Coordenador</th>
			<th>Trader</th>
			<th>Raz�o Social do TBP</th>
			<th>Nome Completo</th>
			<th>Cargo</th>
			<th>CPF</th>
			<?php
			if (exibeChecks())
			{
			?>
			<th style="width:30px;padding:0 0 0 10px">&nbsp;</th>
			<?php 
			}
			?>
		</tr>
	</thead>
	<tbody>
<?php 

$empresa = $_SESSION["empresaID"];
$tipo = $_SESSION["tipo"];
$lotacaoUsuario = $_SESSION["lotacaoID"];
$nome = $_POST["nome"];
$cpf = $_POST["cpf"];
$lotacao = $_POST["cboLotacao"];
$cargo = $_POST["cargofuncao"];
$tipoParticipante = $_POST["rdoParticipantes"];

$sql = "SELECT CD_LOTACAO, DS_LOTACAO, CD_LOTACAO_PAI, CD_NIVEL_HIERARQUICO FROM col_lotacao WHERE CD_EMPRESA = $empresa";
$resultadoLotacao = DaoEngine::getInstance()->executeQuery($sql,true);


$sqlWhere = "";
if ($tipoParticipante == 2) // Est� associado a prova
{
	$sqlWhere = " AND u.CD_USUARIO IN (SELECT pa.CD_USUARIO FROM col_prova_aplicada pa INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA WHERE p.IN_PROVA = 1)";
}

if ($tipoParticipante == 3) // N�o est� associado a prova
{
	$sqlWhere = " AND u.CD_USUARIO NOT IN (SELECT pa.CD_USUARIO FROM col_prova_aplicada pa INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA WHERE p.IN_PROVA = 1)";
}

$lotacoes = array();
while($linha = mysql_fetch_array($resultadoLotacao))
{
	$lotacoes[$linha["CD_LOTACAO"]] = $linha;	
}


$sql = "SELECT login, CONCAT(NM_USUARIO, ' ', COALESCE(NM_SOBRENOME,'')) AS NM_USUARIO, cargofuncao, lotacao, u.CD_USUARIO FROM col_usuario u LEFT OUTER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
 
WHERE empresa = $empresa
AND (cargofuncao like '%$cargo%')
AND ((lotacao = '$lotacao' OR '$lotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$lotacao%') AND (DS_LOTACAO_HIERARQUIA LIKE '%$lotacaoUsuario%' OR $tipo IN (-1,5,4)))
AND (NM_USUARIO LIKE '$nome%' OR NM_SOBRENOME LIKE '$nome%' OR '$nome' = '')
AND (login = '$cpf' OR '$cpf' = '')
$sqlWhere
ORDER BY NM_USUARIO, NM_SOBRENOME, login";

//echo $sql;
//exit();

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

while($linha=mysql_fetch_array($resultado))
{
	$cargofuncao = $linha["cargofuncao"];
	if ($cargofuncao == "") $cargofuncao = "&nbsp;";
	
	$lotacaoUsuario = $linha["lotacao"];
	
	$linhaLotacao = $lotacoes[$lotacaoUsuario];

	$tbp = "&nbsp;";
	$trader = "&nbsp;";
	$coordenador = "&nbsp;";
	$gerente = "&nbsp;";
	$diretor = "&nbsp;";
	
	atribuiLotacao($tbp, $linhaLotacao, $lotacoes, 5);
	atribuiLotacao($trader, $linhaLotacao, $lotacoes, 4);
	atribuiLotacao($coordenador, $linhaLotacao, $lotacoes, 3);
	atribuiLotacao($gerente, $linhaLotacao, $lotacoes, 2);
	atribuiLotacao($diretor, $linhaLotacao, $lotacoes, 1);
	
	$check = "";
	
	if (exibeChecks())
	{
		$check = "<td style=\"width:30px;padding:0 0 0 10px\">
				<input type=\"checkbox\" name=\"chkSelecao[]\" value=\"{$linha["CD_USUARIO"]}\" />
			</td>";
	}
	
	echo "	<tr>
			<td>$diretor</td>
			<td>$gerente</td>
			<td>$coordenador</td>
			<td>$trader</td>
			<td>$tbp</td>
			<td>{$linha["NM_USUARIO"]}</td>
			<td>$cargofuncao</td>
			<td>{$linha["login"]}</td>
			$check
			</tr>
	";
}

function atribuiLotacao(&$string, &$linhaLotacao, &$lotacoes, $nivel)
{
	if ($linhaLotacao["CD_NIVEL_HIERARQUICO"] == $nivel)
	{
		$string = $linhaLotacao["DS_LOTACAO"];
		$lotacaoUsuario = $linhaLotacao["CD_LOTACAO_PAI"];
		$linhaLotacao = $lotacoes[$lotacaoUsuario];
	}
}

?>
	</tbody>	
	</table>
	</div>
	</div>
	</form>
</body>
</html>