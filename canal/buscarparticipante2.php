<?php 
include "../include/security.php";
include "../include/defines.php";
include "../include/genericfunctions.php";
include "../include/accesscounter.php";
include "../include/cripto.php";
include('../admin/controles.php');
include('../admin/framework/crud.php');

if (count($_POST) == 0) exit();
$tipo = $_SESSION["tipo"];

function ehExcel()
{
	return $_POST["rdoExcel"] == 2;
}

function exibeChecks()
{
	global $tipo;
	if ($tipo == -1 || $tipo > 4 || $_SESSION["nivelHierarquico"] == 1) {
		return (!ehExcel());
	}
	
	return false;
	
}

if (ehExcel())
{
	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=Cadastro.xls");
	
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<?php 
if (!(ehExcel()))
{
?>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<?php 
}
?>
<style type="text/css">
#tabelaCadastro td
{
	font-size:11px;
	font-family:tahoma,arial,helvetica,sans-serif;
	border: 1px solid #999;
	padding:2px 10px 2px 10px;
}

#tabelaCadastro th
{
	font-size:11px;
	font-family:tahoma,arial,helvetica,sans-serif;
	background-color:#ccc;
	border: 1px solid #999;
}
</style>
<script type="text/javascript">
window.onload = function(){parent.document.getElementById('ifrUsuarios').style.display = 'block';}
</script>
</head>
<body style="background-color:#ffffff">
	<?php 
	if (!(ehExcel()))
	{
	?>
	<form name="bloqueioForm" enctype="multipart/form-data" method="post" action="bloqueio.php" target="ifrBloqueio">
	<input type="hidden" id="hdnBloqueio" name="hdnBloqueio" />
	<?php 
	}
	?>
	<table id="tabelaCadastro" border="0" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse">
		<tr class="textblk">
			<th>Diretor Business</th>
			<th>Gerente</th>
			<th>Coordenador</th>
			<th>Trader</th>
			<th>Raz�o Social do TBP</th>
			<th>Nome Completo</th>
			<th>Cargo</th>
			<th>CPF</th>
			<?php
			if (exibeChecks())
			{
			?>
			<th style="width:20px;padding:0 5px 0 5px">&nbsp;</th>
			<?php 
			}
			?>
		</tr>

<?php 

$empresa = $_SESSION["empresaID"];
$tipo = $_SESSION["tipo"];
$lotacaoUsuario = $_SESSION["lotacaoID"];
$nome = $_POST["nome"];
$cpf = $_POST["cpf"];
$lotacao = $_POST["cboLotacao"];
$cargo = $_POST["cargofuncao"];
$tipoParticipante = $_POST["rdoParticipantes"];
$ciclo = $_POST["cboCiclo"];

$sql = "SELECT CD_LOTACAO, DS_LOTACAO, CD_LOTACAO_PAI, CD_NIVEL_HIERARQUICO FROM col_lotacao WHERE CD_EMPRESA = $empresa";
$resultadoLotacao = DaoEngine::getInstance()->executeQuery($sql,true);


$sqlWhere = "";
if ($tipoParticipante == 2) // Est� associado a prova
{
	$sqlWhere = " AND u.CD_USUARIO IN (SELECT pa.CD_USUARIO FROM col_prova_aplicada pa INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA WHERE p.IN_PROVA = 1 AND p.CD_CICLO = $ciclo) AND u.tipo = 1";
}

if ($tipoParticipante == 3) // N�o est� associado a prova
{
	$sqlWhere = " AND u.CD_USUARIO NOT IN (SELECT pa.CD_USUARIO FROM col_prova_aplicada pa INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA WHERE p.IN_PROVA = 1 AND p.CD_CICLO = $ciclo)";
}

$lotacoes = array();
while($linha = mysql_fetch_array($resultadoLotacao))
{
	$lotacoes[$linha["CD_LOTACAO"]] = $linha;	
}


$sql = "SELECT login, CONCAT(NM_USUARIO, ' ', COALESCE(NM_SOBRENOME,'')) AS NM_USUARIO, cargofuncao, lotacao, u.CD_USUARIO FROM col_usuario u LEFT OUTER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
 
WHERE empresa = $empresa
AND (cargofuncao like '%$cargo%')
AND ((lotacao = '$lotacao' OR '$lotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$lotacao%') AND (DS_LOTACAO_HIERARQUIA LIKE '%$lotacaoUsuario%' OR $tipo IN (-1,5,4)))
AND (NM_USUARIO LIKE '$nome%' OR NM_SOBRENOME LIKE '$nome%' OR '$nome' = '')
AND (login = '$cpf' OR '$cpf' = '')
$sqlWhere
ORDER BY NM_USUARIO, NM_SOBRENOME, login";

//echo $sql;
//exit();

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
$iCount	   = 0;

while($linha=mysql_fetch_array($resultado))
{
	$cargofuncao = $linha["cargofuncao"];
	if ($cargofuncao == "") $cargofuncao = "&nbsp;";
	
	$lotacaoUsuario = $linha["lotacao"];
	
	$linhaLotacao = $lotacoes[$lotacaoUsuario];

	$tbp = "&nbsp;";
	$trader = "&nbsp;";
	$coordenador = "&nbsp;";
	$gerente = "&nbsp;";
	$diretor = "&nbsp;";
	
	atribuiLotacao($tbp, $linhaLotacao, $lotacoes, 5);
	atribuiLotacao($trader, $linhaLotacao, $lotacoes, 4);
	atribuiLotacao($coordenador, $linhaLotacao, $lotacoes, 3);
	atribuiLotacao($gerente, $linhaLotacao, $lotacoes, 2);
	atribuiLotacao($diretor, $linhaLotacao, $lotacoes, 1);
	
	$check = "";
	
	if($iCount % 2 == 0) $bgcolor = "#fff"; else $bgcolor = "#f1f1f1";
	
	if (exibeChecks())
	{
		$check = "<td style=\"width:20px;padding:0 5px 0 5px\">
				<input type=\"checkbox\" name=\"chkSelecao[]\" value=\"{$linha["CD_USUARIO"]}\" />
			</td>";
	}
	
	echo "	<tr class=\"textblk\" style=\"background-color:$bgcolor\">
			<td>
				$diretor
			</td>
			<td>
				$gerente
			</td>
			<td>
				$coordenador
			</td>
			<td>
				$trader
			</td>
			<td>
				$tbp
			</td>
			<td>
				{$linha["NM_USUARIO"]}
			</td>
			<td>
				$cargofuncao
			</td>
			<td>
				{$linha["login"]}
			</td>
				$check
			</tr>
	";
	$iCount++;
}

function atribuiLotacao(&$string, &$linhaLotacao, &$lotacoes, $nivel)
{
	if ($linhaLotacao["CD_NIVEL_HIERARQUICO"] == $nivel)
	{
		$string = $linhaLotacao["DS_LOTACAO"];
		$lotacaoUsuario = $linhaLotacao["CD_LOTACAO_PAI"];
		$linhaLotacao = $lotacoes[$lotacaoUsuario];
	}
}

?>
		
	</table>
	</form>
</body>
</html>