<?php

	function obterTotaisUsuariosAcumuladoCiclo()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		$sql = "SELECT
					NM_CICLO, SUM(QT_PARTICIPANTE) AS QD_PARTICIPANTE, SUM(QT_CADASTRADO) AS QD_CADASTRADO, DT_INICIO, CICLO_VALIDO
				FROM
					(
						SELECT
  							c.NM_CICLO, COUNT(DISTINCT u.CD_USUARIO) AS QT_PARTICIPANTE, NULL AS QT_CADASTRADO, DT_INICIO, 1 AS CICLO_VALIDO
						FROM
  							col_acesso a
  							INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
							INNER JOIN col_ciclo c ON DATE_FORMAT(c.DT_INICIO,'%Y%m%d') <= DATE_FORMAT(DT_ACESSO,'%Y%m%d') AND DATE_FORMAT(DT_TERMINO,'%Y%m%d') >= DATE_FORMAT(DT_ACESSO,'%Y%m%d') AND c.CD_EMPRESA = u.empresa
						WHERE
	    					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
						  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
						 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
						  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  			AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
						GROUP BY
						    c.CD_CICLO, c.NM_CICLO, DT_INICIO
						
						UNION ALL
						
						SELECT
							c.NM_CICLO,
							NULL AS TOTAL,
							COUNT(1),
							DT_INICIO,
							NULL AS CICLO_VALIDO
						FROM
							col_ciclo c
							INNER JOIN col_usuario u ON u.empresa = c.CD_EMPRESA
						WHERE
						    (u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
						    
						    AND DATE_FORMAT(u.datacadastro,'%Y%m%d') <= DATE_FORMAT(DT_TERMINO,'%Y%m%d')
			
						  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
						 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
						  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
						  	
						GROUP BY
							c.NM_CICLO, DT_INICIO
						) AS TABELA
						GROUP BY
  							NM_CICLO, DT_INICIO
						ORDER BY
  							DT_INICIO
						    ";
		
		//echo $sql;
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			if ($linha["CICLO_VALIDO"] == 1)
			{
				$resultados[$linha["NM_CICLO"]]["QD_PARTICIPANTE"] = $linha["QD_PARTICIPANTE"];
				$resultados[$linha["NM_CICLO"]]["QD_CADASTRADO"] = $linha["QD_CADASTRADO"];
			}
			
			
		}
		
		return $resultados;
		
		
	}
	
	
	function obterTotalAcessosCiclo()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		//DATE_FORMAT(c.DT_INICIO,'%Y%m%d') <= DATE_FORMAT(pr.DT_INICIO,'%Y%m%d') AND DATE_FORMAT(c.DT_TERMINO,'%Y%m%d') >= DATE_FORMAT(pr.DT_INICIO,'%Y%m%d') AND u.empresa = c.CD_EMPRESA
		
		$sql = "SELECT
				    IN_ORDENAR, NM_CICLO, TOTAL
				FROM
				(
				SELECT
				    IN_ORDENAR, NM_CICLO, COUNT(DISTINCT DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'), a.CD_USUARIO) AS TOTAL
				FROM
				    col_acesso a
				    INNER JOIN col_pagina_acesso pa ON pa.CD_PAGINA_ACESSO = a.CD_PAGINA_ACESSO
				    INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
            		INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND DATE_FORMAT(c.DT_INICIO,'%Y%m%d') <= DATE_FORMAT(DT_ACESSO,'%Y%m%d') AND DATE_FORMAT(c.DT_TERMINO,'%Y%m%d') >= DATE_FORMAT(DT_ACESSO,'%Y%m%d') AND u.empresa = c.CD_EMPRESA
				WHERE
				    IN_ORDENAR < 999  AND IN_ORDENAR NOT IN (1,6,7)
					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
				  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
				  	AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    IN_ORDENAR,  NM_CICLO
				    
				UNION ALL
				
					SELECT
				    5 AS IN_ORDENAR, NM_CICLO, COUNT(1) AS TOTAL
				FROM
				    tb_forum f
				    INNER JOIN col_usuario u ON u.empresa = f.cd_empresa and u.login = f.name
				    INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND DATE_FORMAT(c.DT_INICIO,'%Y%m%d') <= DATE_FORMAT(f.data,'%Y%m%d') AND DATE_FORMAT(c.DT_TERMINO,'%Y%m%d') >= DATE_FORMAT(f.data,'%Y%m%d') AND u.empresa = c.CD_EMPRESA
				WHERE
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(f.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(f.data, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    NM_CICLO
				    
				UNION ALL

				SELECT
          			6 AS IN_ORDENAR, NM_CICLO, COUNT(1) AS TOTAL
        		FROM
          			col_prova_realizada pr
          			INNER JOIN col_prova p ON p.CD_PROVA = pr.CD_PROVA
  		    		  INNER JOIN col_usuario u ON pr.CD_USUARIO = u.CD_USUARIO
    				    INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND c.CD_CICLO = p.CD_CICLO
				WHERE
          			p.IN_PROVA = 0 AND
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(c.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(c.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				  	
				  	GROUP BY
				      NM_CICLO
				    
				UNION ALL

				SELECT
          			7 AS IN_ORDENAR, NM_CICLO, COUNT(1) AS TOTAL
        		FROM
          				col_prova_realizada pr
          				INNER JOIN col_prova p ON p.CD_PROVA = pr.CD_PROVA
    		    		INNER JOIN col_usuario u ON pr.CD_USUARIO = u.CD_USUARIO
    				    INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND c.CD_CICLO = p.CD_CICLO
				WHERE
          			p.IN_PROVA = 0 AND pr.VL_MEDIA >= 7 AND
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(c.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(c.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				  	
				  	GROUP BY
				      NM_CICLO

				)
				AS
				    TABELA
				GROUP BY
				    IN_ORDENAR, NM_CICLO
				ORDER BY NM_CICLO";
		
		//echo "<!-- $sql -->";
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$indice = "{$linha["IN_ORDENAR"]}{$linha["NM_CICLO"]}";
			$resultados[$indice] = $linha["TOTAL"];
			
		}
		
		return $resultados;
		
	}
	
	function obterProvasMediaCiclo()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery, $codigoCiclo;
		
		if ($codigoCiclo == "")
			$codigoCiclo = -1;
		
		$sql = "SELECT
        			SUM(QTD_AVALIACAO) AS QTD_AVALIACAO,
        			SUM(QTD_AVALIACAO_SETE) AS QTD_AVALIACAO_SETE,
        			AVG(VL_MEDIA) AS VL_MEDIA,
        			NM_CICLO
				FROM
			(
			
			      SELECT
			          u.CD_USUARIO,
					COUNT(pr.DT_INICIO) AS QTD_AVALIACAO,
					COUNT(IF(pr.VL_MEDIA >= 7,1,NULL)) AS QTD_AVALIACAO_SETE,
					AVG(pr.VL_MEDIA) AS VL_MEDIA,
					NM_CICLO
				FROM
					col_prova_realizada pr
					INNER JOIN col_prova p ON pr.CD_PROVA = p.CD_PROVA
					INNER JOIN col_usuario u ON u.CD_USUARIO = pr.CD_USUARIO
          			INNER JOIN col_ciclo c ON c.CD_EMPRESA = u.empresa AND p.CD_CICLO = c.CD_CICLO
				WHERE
 					p.IN_PROVA = 1
 					
					AND pr.DT_INICIO IS NOT NULL AND pr.VL_MEDIA IS NOT NULL

					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
				  	
				  	AND (p.CD_CICLO = $codigoCiclo OR ($codigoCiclo = -1 
				  	AND	(DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'))
				  		)
				  	
				GROUP BY
				        u.CD_USUARIO, NM_CICLO
				) AS TABELA
				GROUP BY NM_CICLO
				ORDER BY
				    NM_CICLO";
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		global $mediaGeralProvasCalculada, $quantidadeTotalAvaliacoesMaisSegundaChamada, $quantidadeTotalAvaliacoeAcimaSeteSegundaChamada;
		
		$ciclos = 0;
		
		while ($linha = mysql_fetch_array($resultado))
		{
			$indice = "{$linha["NM_CICLO"]}";
			$resultados[$indice]["VL_MEDIA"] = str_ireplace(".",",",round($linha["VL_MEDIA"] * 10, 2));
			//$resultados[$indice]["VL_MEDIA"] = $linha["VL_MEDIA"] * 10;
			$resultados[$indice]["QTD_AVALIACAO"] = $linha["QTD_AVALIACAO"];
			$resultados[$indice]["QTD_AVALIACAO_SETE"] = $linha["QTD_AVALIACAO_SETE"];
			
			$mediaGeralProvasCalculada = $mediaGeralProvasCalculada + $linha["VL_MEDIA"];
			$quantidadeTotalAvaliacoesMaisSegundaChamada = $quantidadeTotalAvaliacoesMaisSegundaChamada + $linha["QTD_AVALIACAO"];
			$quantidadeTotalAvaliacoeAcimaSeteSegundaChamada = $quantidadeTotalAvaliacoeAcimaSeteSegundaChamada + $linha["QTD_AVALIACAO_SETE"];
			$ciclos++;
			
		}
		
		if ($ciclos > 0)
		{
			$mediaGeralProvasCalculada = $mediaGeralProvasCalculada / $ciclos;
			$mediaGeralProvasCalculada = str_ireplace(".",",",round($mediaGeralProvasCalculada * 10, 2));
		}
			
		
		return $resultados;
		
	}

	
	function obterTotaisUsuariosDia()
	{
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		$sql = "SELECT
    				DT_ACESSO, COUNT(1) AS QTD_PARTICIPANTE, NULL AS QTD_CADASTRADO
				FROM
				(
					SELECT
    					a.CD_USUARIO,  MIN(DATE_FORMAT(DT_ACESSO,'%Y%m%d'))  AS DT_ACESSO
					FROM
    					col_acesso a
    					INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
    				WHERE
    				
	    				(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
    				
					GROUP BY
    					CD_USUARIO
				)
				AS
    				TABELA
				GROUP BY
    				DT_ACESSO
    				
    				
    			UNION
    			
				SELECT
					DATE_FORMAT(datacadastro,'%Y%m%d')  AS DT_ACESSO, NULL, COUNT(1)
				FROM
  					col_usuario u
				WHERE
    				(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				GROUP BY DT_ACESSO
    				
				ORDER BY
    				DT_ACESSO";
		
		//echo $sql;
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			if ($linha["QTD_PARTICIPANTE"] != null)
				$resultados[$linha["DT_ACESSO"]]["QTD_PARTICIPANTE"] = $linha["QTD_PARTICIPANTE"];
				
			if ($linha["QTD_CADASTRADO"] != null)
				$resultados[$linha["DT_ACESSO"]]["QTD_CADASTRADO"] = $linha["QTD_CADASTRADO"];
			
		}
		
		return $resultados;
		
		
	}
	
	

	
	function obterTotaisUsuariosAcumuladoDia()
	{
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		
		$sql = "SELECT
    				DT_ACESSO, COUNT(1) AS TOTAL
				FROM
				(
					SELECT
    					a.CD_USUARIO,  MIN(DATE_FORMAT(DT_ACESSO,'%Y%m%d'))  AS DT_ACESSO
					FROM
    					col_acesso a
    					INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
    				WHERE
    				
	    				(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
    				
					GROUP BY
    					CD_USUARIO
				)
				AS
    				TABELA
				GROUP BY
    				DT_ACESSO
				ORDER BY
    				DT_ACESSO";
		
		
		//echo $sql;
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$resultados[$linha["DT_ACESSO"]] = $linha["TOTAL"];
			
		}
		
		return $resultados;
		
	}


	function obterTotalAcessosDia()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		$sql = "SELECT
				    IN_ORDENAR, DT_ACESSO, TOTAL
				FROM
				(
				SELECT
				    IN_ORDENAR, DATE_FORMAT(DT_ACESSO, '%Y%m%d') AS DT_ACESSO, COUNT(DISTINCT DATE_FORMAT(a.DT_ACESSO, '%Y%m%d'), a.CD_USUARIO) AS TOTAL
				FROM
				    col_acesso a
				    INNER JOIN col_pagina_acesso pa ON pa.CD_PAGINA_ACESSO = a.CD_PAGINA_ACESSO
				    INNER JOIN col_usuario u ON u.CD_USUARIO = a.CD_USUARIO
				WHERE
				    IN_ORDENAR < 999  AND IN_ORDENAR NOT IN (1,6,7)
					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
				  	AND	(a.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
				  	AND	(DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(a.DT_ACESSO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    IN_ORDENAR,  DATE_FORMAT(DT_ACESSO, '%Y%m%d')
				    
				UNION ALL
				
				SELECT
				    5 AS IN_ORDENAR, DATE_FORMAT(f.data, '%Y%m%d')  AS DT_ACESSO, COUNT(1) AS TOTAL
				FROM
				    tb_forum f
				    INNER JOIN col_usuario u ON u.empresa = f.cd_empresa and u.login = f.name
				    
				WHERE
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(f.data, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(f.data, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    DATE_FORMAT(data, '%Y%m%d') 
				    
				UNION ALL

				SELECT
          			6 AS IN_ORDENAR, DATE_FORMAT(pr.DT_INICIO, '%Y%m%d')  AS DT_ACESSO, COUNT(1) AS TOTAL
        		FROM
          			col_prova_realizada pr
          			INNER JOIN col_prova p ON p.CD_PROVA = pr.CD_PROVA
  		    		INNER JOIN col_usuario u ON pr.CD_USUARIO = u.CD_USUARIO
				WHERE
          			p.IN_PROVA = 0 AND
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				  	
				  	GROUP BY
				    DATE_FORMAT(pr.DT_INICIO, '%Y%m%d')
				    
				UNION ALL

				SELECT
          			7 AS IN_ORDENAR, DATE_FORMAT(pr.DT_INICIO, '%Y%m%d')  AS DT_ACESSO, COUNT(1) AS TOTAL
        		FROM
          			col_prova_realizada pr
          			INNER JOIN col_prova p ON p.CD_PROVA = pr.CD_PROVA
  		    		INNER JOIN col_usuario u ON pr.CD_USUARIO = u.CD_USUARIO
				WHERE
          			p.IN_PROVA = 0 AND pr.VL_MEDIA >= 7 AND
					(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				  	
				  	GROUP BY
				    DATE_FORMAT(pr.DT_INICIO, '%Y%m%d')
				
				)
				AS
				    TABELA
				GROUP BY
				    IN_ORDENAR, DT_ACESSO
				ORDER BY DT_ACESSO";
		
		
		//echo $sql;
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$indice = "{$linha["IN_ORDENAR"]}{$linha["DT_ACESSO"]}";
			$resultados[$indice] = $linha["TOTAL"];
			
		}
		
		return $resultados;
		
	}
	
	function obterProvasDia()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		$sql = "SELECT
					COUNT(pr.DT_INICIO) AS QTD_AVALIACAO,
					COUNT(IF(pr.VL_MEDIA >= 7,1,NULL)) AS QTD_AVALIACAO_SETE,
					AVG(pr.VL_MEDIA) AS VL_MEDIA,
					DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') AS DT_INICIO
				FROM
					col_prova_realizada pr
					INNER JOIN col_prova p ON pr.CD_PROVA = p.CD_PROVA
					INNER JOIN col_usuario u ON u.CD_USUARIO = pr.CD_USUARIO
				WHERE
 					p.IN_PROVA = 1
 					
					AND pr.DT_INICIO IS NOT NULL AND pr.VL_MEDIA IS NOT NULL

					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    DATE_FORMAT(pr.DT_INICIO, '%Y%m%d') 
				ORDER BY
				    pr.DT_INICIO";
		
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			$indice = "{$linha["DT_INICIO"]}";
			$resultados[$indice]["VL_MEDIA"] = str_ireplace(".",",",round($linha["VL_MEDIA"] * 10, 2));
			$resultados[$indice]["QTD_AVALIACAO"] = $linha["QTD_AVALIACAO"];
			$resultados[$indice]["QTD_AVALIACAO_SETE"] = $linha["QTD_AVALIACAO_SETE"];
		}
		
		return $resultados;
		
	}
	
	
	function obterProvasMediasDia()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		$sql = "SELECT
				    IN_PROVA, DT_INICIO, SUM(SOMA_MEDIA) / SUM(QD_PROVA) AS VL_MEDIA
				FROM
				(
				SELECT
				    p.IN_PROVA ,DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') AS DT_INICIO, SUM(pa.VL_MEDIA) AS SOMA_MEDIA, count(pa.CD_PROVA) AS QD_PROVA
				FROM
				    col_prova_aplicada pa
				    INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
				    INNER JOIN col_usuario u ON u.CD_USUARIO = pa.CD_USUARIO
				WHERE
				    pa.VL_MEDIA IS NOT NULL
				    AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    p.IN_PROVA ,DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') 
				
				UNION ALL
				
				SELECT
				    p.IN_PROVA ,DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') AS DT_INICIO, SUM(pa.VL_MEDIA) AS SOMA_MEDIA, count(pa.CD_PROVA) AS QD_PROVA
				FROM
				    col_prova_aplicada_hist pa
				    INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
				    INNER JOIN col_usuario u ON u.CD_USUARIO = pa.CD_USUARIO
				WHERE
					pa.VL_MEDIA IS NOT NULL
					
					AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
		
				  	AND	(DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery')
				GROUP BY
				    p.IN_PROVA ,DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') 
				)
				AS TABELA
				GROUP BY
				    IN_PROVA, DT_INICIO
				ORDER BY
				    DT_INICIO";
		
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$indice = "{$linha["IN_PROVA"]}{$linha["DT_INICIO"]}";
			$resultados[$indice] = str_ireplace(".",",",round($linha["VL_MEDIA"], 2));
			
		}
		
		return $resultados;
		
		
	}
	
	
	function obterProvasMediasGeral()
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery, $codigoCiclo;
		
		if ($codigoCiclo == "")
			$codigoCiclo = -1;
		
		$sql = "SELECT
				    p.IN_PROVA, SUM(pa.VL_MEDIA) / count(pa.CD_PROVA) AS VL_MEDIA
				FROM
				    col_prova_realizada pa
				    INNER JOIN col_prova p ON p.CD_PROVA = pa.CD_PROVA
				    INNER JOIN col_usuario u ON u.CD_USUARIO = pa.CD_USUARIO
				WHERE
				    pa.VL_MEDIA IS NOT NULL
				    AND	(u.empresa = $codigoEmpresa OR $codigoEmpresa = -1)
			
				  	AND	(u.CD_USUARIO in ($codigoUsuario) OR '$codigoUsuario' = '-1')
		
				 	AND	(u.lotacao in ($codigoLotacao) OR '$codigoLotacao' = '-1')
		
				  	AND (u.cargofuncao in ('$cargo') OR '-1' IN ('$cargo'))
				  	
				  	AND (p.CD_CICLO = $codigoCiclo OR ($codigoCiclo = -1 
				  		AND	(DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') >= '$dataInicialQuery' AND DATE_FORMAT(pa.DT_INICIO, '%Y%m%d') <= '$dataFinalQuery'))
				  		)
				GROUP BY
				    p.IN_PROVA
				";
		
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$indice = "{$linha["IN_PROVA"]}";
			$resultados[$indice] = str_ireplace(".",",",round($linha["VL_MEDIA"] * 10, 2));
			
		}
		
		return $resultados;
		
		
	}
	
	
	
	function obterNomePaginas()
	{
		
		global $codigoEmpresa;
		
		$paginas = array();
		
		$paginas[1] = "Boas Vindas";
		$paginas[2] = "Senha";
		$paginas[3] = "Download";
		
		
		if ($codigoEmpresa != 29)
		{
			$paginas[4] = "Visita ao F�rum";
			$paginas[5] = "Contribui��o&nbsp;ao&nbsp;F�rum";
		}
		$paginas[6] = "Simulados";
		//$paginas[7] = "Certifica��es";
		$paginas[7] = "Simulados com Aprova��o";
		
		
		return $paginas;
		
	}
	
	
	function obterNomeCiclos($filtrarData = false)
	{
		
		global $codigoEmpresa, $codigoUsuario, $codigoLotacao, $cargo, $dataInicialQuery, $dataFinalQuery;
		
		$whereCiclo = "";
		if ($filtrarData)
		{
			$whereCiclo = "AND DATE_FORMAT(DT_INICIO, '%Y%m%d') <= $dataFinalQuery";
		}
		
		$sql = "SELECT
				    CD_CICLO, NM_CICLO
				FROM
				    col_ciclo
				WHERE
				    CD_EMPRESA = $codigoEmpresa
				    $whereCiclo
				ORDER BY
					DT_INICIO";
		
		
		$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

		$resultados = array();
		
		while ($linha = mysql_fetch_array($resultado))
		{
			
			$resultados[] = $linha["NM_CICLO"];
			
		}
		
		return $resultados;
		
		
	}
	
	

?>