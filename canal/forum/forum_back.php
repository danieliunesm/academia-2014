<?php
$QUERY_STRING = '';

$querybrancharray = isset($_GET["openbranch"])?$_GET["openbranch"]:'';
$QUERY_STRING = '&openbranch=' . $querybrancharray;

//function FmtDataTempo($oDate){
//	return strftime("%d/%m/%Y %H:%M:%S", strtotime($oDate));
//}

class ThreadProcessor{
    var $db;
	var $getthreadCode;
    var $threadCode;
    var $threadSubmitted;
    var $user;
    var $email;
    var $title;
    var $message;
	var $data;
	var $status;
	var $action;
	var $anonym;
	var $olink;
	
	var $alias;
	var $cd_usu;
	
	var $firsttime = true;
	var $previousmessage = '';
	var $tb_forum = 'tb_forum';
	
    function ThreadProcessor(&$db){
        $this->db=&$db;
		$this->getthreadCode=isset($_GET['threadcode'])?$_GET['threadcode']:'0';
        $this->threadCode=isset($_GET['threadcode'])?$_GET['threadcode']:'0';
        $this->threadSubmitted=isset($_POST['send'])?$_POST['send']:'';
		
//		$this->forum=isset($_GET['f'])?$_GET['f']:'1';
		$this->forum=$_SESSION['forum'];
		$this->empresa=$_SESSION['empresaID'];
		
//		$this->user=$_SESSION['valid_user'];
        $this->email=isset($_POST['email'])?$_POST['email']:$_SESSION['email'];
		$this->alias=$_SESSION['alias'];
		
		$this->cd_usu=$_SESSION['cd_usu'];
		
        $this->title=isset($_POST['title'])?$_POST['title']:'';
        $this->message=isset($_POST['message'])?$_POST['message']:'';
		$this->data=isset($_POST['data'])?$_POST['data']:date('%d/%m/%Y %H:%M:%S');
		$this->status=isset($_POST['status'])?$_POST['status']:'1';
		$this->anonym=isset($_POST['anonym'])?($_POST['anonym']?'1':'0'):'0';
		
		$this->olink=isset($_POST['link'])?$_POST['link']:'';
    }

    function displayThreads(){
        if($this->threadSubmitted){
            $this->addThread();
        }
        ob_start();
		echo '<div id="threadslayer">';
		echo '<h2>T�picos recentes';
		if($this->getthreadCode=='0'){
			if(strtolower($this->alias) == strtolower($_SESSION["mediador"])) echo '<img src="../images/ic_black_bullet.gif" width="12" height="9" hspace="5"><a href="#formspot" onclick="setTimeout(\'document.forumform.title.focus()\',100)">Inserir t�pico</a>';
		}
		else echo '<img src="../images/ic_black_bullet.gif" width="12" height="9" hspace="5"><a href="#formspot" onclick="setTimeout(\'document.forumform.title.focus()\',100)">Inserir contribui��o</a>';
		echo '</h2><span style="float: right; margin-top: -34px" class="textblk"><input type="text" class="textbox" style="width: 100px" name="txtLocalizar" value="' . $_GET["pesq"] . '"> <input type="button" value="Localizar" class="formbutton" style="width: 60px" onclick="localizar()"/></span>';
		$this->threadCode=='0'?$this->fetchTitles():$this->fetchMessages();
		echo '</div>';
		$this->createThreadForm();
        $contents=ob_get_contents();
        ob_end_clean();
        return $contents;
    }

	function obterCodigoPai($filho){
    	
    	$sql = "SELECT parent_id from ".$this->tb_forum." WHERE id = $filho AND CD_FORUM = '$this->forum'";
    	
    	$result = mysql_query($sql);
    	
    	$linha = mysql_fetch_array($result);
    	
    	if ($linha["parent_id"] == 0){
    		return $filho;
    	}else{
    		return $linha["parent_id"];
    	}
    	
    }
    
    
    function buscarItens($pesquisa, $dao){
    	
    	
    	$sql = "SELECT f.parent_id FROM ".$this->tb_forum." f, tb_status s WHERE f.CD_FORUM='$this->forum' AND f.parent_id <> 0 AND f.status = s.id $pesquisa ";
    	
    	$result = mysql_query($sql);
    	    	
    	$codigosPais = "";
    	
    	while ($linha = mysql_fetch_array($result)) {
    		
    		$codigoAtual = $this->obterCodigoPai($linha["parent_id"]);
    		
    		if ($codigosPais == ""){
    			$codigosPais = $codigoAtual;
    		}else{
    			$codigosPais = "$codigosPais, $codigoAtual";
    		}
    		
    	}
    	
    	if ($codigosPais != ""){
    		
    		$codigosPais = "($codigosPais)";
    		
    	}
    	
    	return $codigosPais;
    	
    }
    
    function fetchTitles(){
		$firstthread = false;
		$listyle = '';
		
		
		if ($this->threadCode == 0){
			
			if(isset($_GET["pesq"])){
				$pesquisa = $_GET["pesq"];
				$pesquisa = str_ireplace("\\","\\\\\\",$pesquisa);
				$pesquisa = str_ireplace("'","\\'",$pesquisa);
				$pesquisa = " AND f.title like '%$pesquisa%' OR f.message like '%$pesquisa%' ";
				
				$itensPais = $this->buscarItens($pesquisa, $this->db);
				
				$pesquisa = $_GET["pesq"];
				$pesquisa = str_ireplace("\\","\\\\\\",$pesquisa);
				$pesquisa = str_ireplace("'","\\'",$pesquisa);
				
				$pesquisa = " AND (f.title like '%$pesquisa%' OR f.message like '%$pesquisa%' ";
				
				if ($itensPais != ""){
					$pesquisa = "$pesquisa OR f.id IN $itensPais ";
				}
				
				$pesquisa = "$pesquisa) ";
				
			}
		}

		
        $result=$this->db->query("SELECT f.id, f.parent_id, f.name, f.email, f.title, f.data, f.message, f.anonym, f.link, s.status, s.statusimg FROM ".$this->tb_forum." f, tb_status s WHERE f.CD_FORUM='$this->forum' AND parent_id='$this->threadCode' AND f.status = s.id $pesquisa ORDER BY f.data DESC");
		$nrows = $result->countRows();
		if($nrows > 0){
			if($this->firsttime){
				$this->firsttime = false;
				echo '<ul>';
			}
			else{
				echo '<a href="javascript:void(0)" onclick="opencloseBranch(this,'.$this->threadCode.');return false"><img id="img'.$this->threadCode.'" src="../images/ic_plus3.gif" alt="   expandir sub-�rvores   " style="float:left;width:16px;height:16px;margin-left:-25px;margin-top:-18px" /></a><ul id="'.$this->threadCode.'" style="display:none">';
			}
		}
        while($row=$result->fetchRow()){
			$result3  = $this->db->query("SELECT id FROM ".$this->tb_forum." WHERE parent_id='".$row['id']."'");
			$nsubrows = (int)$result3->countRows();
			if($row['parent_id'] == 0){
				$listyle  = 'topico';
				$bgstyle  = 'bgtopico';
				$msgstyle = '0" style="display:none"';
			}
			else{
				$listyle  = 'linha';
				$bgstyle  = 'bglinha';
				$msgstyle = '" style="display:block"';
			}
			echo '<li><div class="'.$listyle.'"><a name="a'.$row['id'].'"></a><div class="'.$bgstyle.'"><img class="icostatus" src="../images/'.$row['statusimg'].'" alt="   '.$row['status'].'   " /><a class="threadlink" href="javascript:void(0)" onclick="reloadPage('.$row['id'].');return false">'.$row['title'].'</a> - Postado por <span class="nome">'.($row['anonym']?'An�nimo':$row['name']).'</span>  em '.FmtDataTempo($row['data']).'<span class="'.($nsubrows > 0?'comment':'nocomment').'"> &raquo; '.$nsubrows.' '.($nsubrows != 1?'coment�rios':'coment�rio').'</span></div>';
			echo '<span id="message'.$row['id'].'" class="messageinline'.$msgstyle.'>' .$row['message'];
			
			if($row['link'] != ''){
				echo '<br /><br />' . '<a href="'.$row['link'].'" target="_blank" style="font-weight:normal">'.$row['link'].'</a>';
			}
			
			echo '</span></div></li>';
			if($row['parent_id'] == 0){
				$result2=$this->db->query("SELECT id FROM $this->tb_forum WHERE parent_id='".$row['id']."'");
				$nrows2 = $result2->countRows();
				if($nrows2 < 1){
					echo '<div><a href="javascript:void(0)" onclick="opencloseMessage(this, \'message'.$row['id'].'\');return false"><img id="img'.$row['id'].'" src="../images/ic_plus3.gif" alt="   expandir sub-�rvores   " style="float:left;width:16px;height:16px;margin-left:-25px;margin-top:-18px" /></a></div>';
				}
			}
            $this->threadCode=$row['id'];
            $this->fetchTitles();
        }
		if($nrows > 0)
	        echo '</ul>';
    }
    function fetchMessages(){
		global $QUERY_STRING;
        $result=$this->db->query("SELECT name,title,message,anonym FROM $this->tb_forum WHERE id='$this->threadCode'");
        if($result->countRows()==0){
            echo 'Nenhuma mensagem foi encontrada!';
            return;              
        }
        $row=$result->fetchRow();
        echo '<h2>'.$row['title'].'</h2><p><span class="nome">'.($row['anonym']?'An�nimo':$row['name']).'</span> escreveu: <div class="message">'.$row['message'].'</div></p><a class="threadlink" href="'.$_SERVER['PHP_SELF'].'?threadcode=0'.$QUERY_STRING.'|'.$this->threadCode.'">Voltar ao F�rum</a>';
    }

    function createThreadForm(){
		global $QUERY_STRING;
		$this->threadCode=intval(isset($_GET['threadcode'])?$_GET['threadcode']:'0');
		echo '<div id="formlayer">';
		if(($this->getthreadCode=='0' && strtolower($this->alias) == strtolower($_SESSION["mediador"])) || $this->getthreadCode!='0'){
			echo '<a name="formspot"></a>';
			
			echo '<form name="forumform" method="post" action="'.$_SERVER['PHP_SELF'].'?threadcode='.$this->threadCode.$QUERY_STRING.'" onsubmit="return submitForm(this)">';
			
			echo '<h2 style="float:left;width:150px">Inserir contribui��o</h2>';
			
			echo '<p><select class="combobox" style="width:200px" name="status">';
			echo '<option value="0">Selecione um tipo de contribui��o</option>';
			$result=$this->db->query("SELECT * FROM tb_status ORDER BY statusorder");
			while($row=$result->fetchRow()){
				echo '<option value="'.$row['id'].'">'.$row['status'].'</option>';
			}
			echo '</select></p>';
			
			echo '<p class="label" style="margin-top:40px"><input type="checkbox" id="anonym" name="anonym" onfocus="if(this.blur)this.blur()"><span>Usu�rio an�nimo (* Escolhendo esta op��o, seus dados n�o ser�o exibidos no f�rum.)</span></p>';
			echo '<p class="label">E.mail: <span>(opcional)</span><br /><input class="textbox" type="text" name="email" size="30" onfocus="this.style.borderColor=\'#000\';this.style.backgroundColor = \'#f8f8f8\'" onblur="this.style.borderColor=\'#999\';this.style.backgroundColor = \'#fff\'" value="'.$this->email.'" maxlength="50" /></p>';
			echo '<p class="label">T�tulo:<br /><input class="textbox" type="text" name="title" size="30" onfocus="this.style.borderColor=\'#000\';this.style.backgroundColor = \'#f8f8f8\'" onblur="this.style.borderColor=\'#999\';this.style.backgroundColor = \'#fff\'"  maxlength="50" /></p>';
			echo '<p class="label">Mensagem:<br /><textarea class="textarea" style="background:transparent" name="message" rows="10" cols="30"  onfocus="this.style.borderColor=\'#000\';this.style.backgroundColor = \'#f1f1f1\'" onblur="this.style.borderColor=\'#999\';this.style.backgroundColor = \'transparent\'"></textarea></p>';

			echo '<p class="label">Link: <span style="font-weight:normal">(para urls externas, digite http://)</span><br /><input class="textbox" type="text" name="link" size="30" onfocus="this.style.borderColor=\'#000\';this.style.backgroundColor = \'#f8f8f8\'" onblur="this.style.borderColor=\'#999\';this.style.backgroundColor = \'#fff\'"  maxlength="255" /><!-- <a href="" title="   procurar link no Sum�rio Executivo correspondente  "><img src="/admin/images/viewfile2.gif" align="absmiddle" hspace="10" /></a> --></p>';

			echo '<p><input class="formbutton" type="submit" name="send" value=" enviar   " /><input class="formbutton" type="reset" name="reset" value=" limpar   " /></p>';
			echo '</form>';
			echo '<script type="text/javascript">document.forumform.title.focus()</script>';
		}
		echo '<a href="';
		if($this->getthreadCode!='0')
			echo $_SERVER['PHP_SELF'].'?threadcode=0'.$QUERY_STRING;
		else
			echo 'javascript:history.back()';
		echo '"><img src="../images/bt_back.gif" align="absmiddle" alt="   voltar   " hspace="5">Voltar</a>&nbsp;&nbsp;&nbsp;<a href="javascript:self.scrollTo(0,0)"><img src="../images/bt_top.gif" align="absmiddle" alt="   voltar a topo   " hspace="5">Topo</a>';
		echo '</div>';
    }

    function addThread(){
		global $QUERY_STRING;
        $this->db->query("INSERT INTO ".$this->tb_forum." (cd_forum,id,parent_id,name,email,title,message,data,status,anonym,cd_empresa,link) VALUES ($this->forum,NULL,'$this->threadCode','$this->alias','$this->email','".$this->replaceSingleQuotationMark($this->title)."','".$this->replaceSingleQuotationMark($this->message)."',NOW(),$this->status,$this->anonym,$this->empresa,'$this->olink')");
//        header('location:'.$_SERVER['PHP_SELF']);
		echo "<meta http-equiv=\"refresh\" content=\"0;URL=".$_SERVER['PHP_SELF']."?threadcode=0".$QUERY_STRING."|".$this->threadCode."\">";
    }
	
	function replaceSingleQuotationMark($str){
//		$newstr = str_replace("'", "&lsquo;", $str);
//		$newstr = htmlentities($str,ENT_QUOTES);
		$newstr = addslashes($str);
		return $newstr;
	}
}

class MySQL {
    var $conId;
    var $host;
    var $user;
    var $password;
    var $database;

    function MySQL($options=array()){
        if(count($options)>0){
            foreach($options as $parameter=>$value){
//              if(empty($value)){
//                  trigger_error('Par�metro inv�lido '.$parameter,E_USER_ERROR);
//              }
                $this->{$parameter}=$value;
            }
            $this->connectDB();
        }
        else {
            trigger_error('Nenhum par�metro de conex�o foi fornecido',E_USER_ERROR);
        }
    }

    function connectDB(){
        if(!$this->conId=mysql_connect($this->host,$this->user,$this->password)){
            trigger_error('Erro ao tentar conectar com o servidor',E_USER_ERROR);
        }
        if(!mysql_select_db($this->database,$this->conId)){
            trigger_error('Erro ao tentar selecionar o banco de dados',E_USER_ERROR);
        }
    }

    function query($query){
        if(!$this->result=mysql_query($query,$this->conId)){
            trigger_error('Erro ao tentar executar a consulta '.$query,E_USER_ERROR);
        }
        return new Result($this,$this->result); 
    }
}

class Result{
    var $mysql;
    var $result;
    function Result(&$mysql,$result){
        $this->mysql=&$mysql;
        $this->result=$result;
    }

    function fetchRow(){
        return mysql_fetch_array($this->result,MYSQL_ASSOC);
    }

    function countRows(){
        if(!$rows=mysql_num_rows($this->result)){
            return false;
        }
        return $rows;
    }

    function countAffectedRows(){
        if(!$rows=mysql_affected_rows($this->mysql->conId)){
            trigger_error('Erro ao tentar contar o n�mero de linhas afetadas',E_USER_ERROR);
        }
        return $rows;
    }

    function getInsertID(){
        if(!$id=mysql_insert_id($this->mysql->conId)){
            trigger_error('Erro ao tentar obter o ID',E_USER_ERROR);
        }
        return $id;
    }

    function seekRow($row=0){
        if(!mysql_data_seek($this->result,$row)){
            trigger_error('Erro ao pesquisar os dados',E_USER_ERROR);
        }
    }

    function getQueryResource(){
        return $this->result;
    }
}
?>