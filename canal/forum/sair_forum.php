<?php
session_start();

include("../../include/defines.php");
include("../../include/contadorforum.php");
excluiControleSessaoUsuario($_SESSION['cd_usu']);

unset($_SESSION["forum"]);
unset($_SESSION["mediador"]);

header("Location: /canal/index.php");
?>