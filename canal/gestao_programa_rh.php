<?php
//include "../include/security.php";
//include "../include/genericfunctions.php";
//include("../include/defines.php");
//include('../admin/framework/crud.php');
//include('../admin/controles.php');
//include('../admin/page.php');
//include "../include/accesscounter.php";

$dataAtual = date("Ymd");
$dataFinal = date("d/m/Y");
$empresa = $_SESSION["empresaID"];
$dataInicio = "";
$dataInicial = $dataInicio;
$cicloSelecionado = (isset($_GET["id"])? $_GET["id"]:"");
$dataInicioTodosCiclos = "";
$dataTerminoTodosCiclos = "";

if ($cicloSelecionado == "")
	$cicloSelecionado = -1;

?>
		<script language="javascript">
			function abreRelatorioRH()
			{
				
				cboCiclo = document.getElementById("cboCiclo");
				lotacao = "";

				var cboLocalidade = document.getElementById("cboLocalidade");
				var opcoesLocalidade = cboLocalidade.options;
				
				var quantidadeLocalidade = cboLocalidade.options.length;

				var localidades = "";


				for(i=0; i < quantidadeLocalidade; i++){

					if (cboLocalidade.options[i].selected){
						if(localidades != ""){
							localidades = localidades + ","
						}
						localidades = localidades + cboLocalidade.options[i].value;

					}
				}

				if(localidades == ""){
					localidades = "-1";
					
				}

				localidades = "&cboLocalidade=" + localidades;

				filtro = "-1";
				cargo = "-1";
				hdnCiclo = cboCiclo.value;
				dataInicio = cboCiclo.options[cboCiclo.selectedIndex].getAttribute("dataInicial");
				dataTermino = cboCiclo.options[cboCiclo.selectedIndex].getAttribute("dataFinal");
				if (document.getElementById("cboFiltro") != null) filtro = document.getElementById("cboFiltro").value;
				if (document.getElementById("cboCargo") != null) cargo = document.getElementById("cboCargo").value;
				if (document.getElementById("hdnLotacao") != null) lotacao = "&hdnLotacao=" + document.getElementById("hdnLotacao").value;
				cboEmpresa = <?php echo $empresa; ?>;
				rel = document.getElementById("cboTipoRelatorio").value;
				
				url = "relatorios/" + rel + "&txtDe=" + dataInicio + "&txtAte=" + dataTermino + "&id=" + filtro + "&hdnCiclo=" + hdnCiclo + "&cboEmpresa=" + cboEmpresa + lotacao + localidades + "&cboCargo=" + cargo + "&cboCurso=" + document.getElementById("cboCurso").value;
				
				newWin=null;
				var w= 900;
				var h= 600;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				
				newWin=window.open(url,'relatorio','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
				
			}
		
		</script>
			
		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="post">
					
			<TABLE cellpadding="0" cellspacing="0" width="100%" align="center" border="0">
				<TR>
					<TD width="100%">

					<?php
						obterTextoComentario(basename("/gestao_programa_rh",".php"));
					
					?>
					<table cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" style="padding-top: 25px;">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                    <td>
                                        Ciclos do Programa:
                                    </td>
                                </tr>
                                    <tr>
                                        <td>
                                            <?php
                                            comboboxCiclo("cboCiclo", -1, $empresa)
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Curso:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php
                                            comboboxPastaEmpresa("cboCurso", -1, $empresa)
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Grupos de Gest�o:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php
                                                //if ($empresa == 34)
                                                //{
                                                    comboboxLotacaoHierarquia("hdnLotacao",'', $empresa, $_SESSION['cd_usu'], $tipoUsuario);
                                                //} else
                                                //{
                                                //	comboboxFiltro("cboFiltro", -1, $empresa, false, false, false, array("CD_FILTRO" => "", "NM_FILTRO" => "Empresa"), true);
                                                //}

                                            ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cargo:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php
                                                //if ($empresa == 34)
                                                //{
                                                    comboboxCargoFuncao("cboCargo",'', $empresa);
                                                //} else
                                                //{
                                                //	comboboxFiltro("cboFiltro", -1, $empresa, false, false, false, array("CD_FILTRO" => "", "NM_FILTRO" => "Empresa"), true);
                                                //}

                                            ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Localidade:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php
                                                //if ($empresa == 34)
                                                //{
                                                    comboboxLocalidade("cboLocalidade",'', $empresa, false, false);
                                                //} else
                                                //{
                                                //	comboboxFiltro("cboFiltro", -1, $empresa, false, false, false, array("CD_FILTRO" => "", "NM_FILTRO" => "Empresa"), true);
                                                //}

                                            ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tipos de Relat�rios:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php
                                                //if ($empresa == 34)
                                                //{
                                                //	comboboxTipoRelatorioTim("cboTipoRelatorio", "","");
                                                //} else
                                                //{
                                                    comboboxTipoRelatorio("cboTipoRelatorio", "", $somenteAssessment);
                                                //}

                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="button" class="buttonsty" value="Visualizar Relat�rio" onclick="javascript:abreRelatorioRH();" />
                                        </td>
                                    </tr>
                                </table>

                            </td>
                            <td>
                                <!--<img src="relatorios/imageconfig.php?parametro=;;6000;4800;250&texto=Cadastrados;Eleg�veis;Participantes&ytitle=&xtitle&ymax=6500&tickincrement=500" width="500" />-->
                                <? graficoCadastro($empresa); ?>
                                <div style="width:100%; text-align: center; margin-top: -100px;">
                                    <input type="button" class="buttonsty" value="Cadastro R�pido" onclick="javascript:document.location.href='cadastro.php';" />
                                </div>
                            </td>

                        </tr>

                    </table>
					
					</TD>
				</TR>

				
			</TABLE>
		</FORM>
		
