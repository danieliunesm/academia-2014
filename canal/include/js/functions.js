//FUNÇÃO PARA VALIDAR A DATA
function ValidarData(p_oObjeto) {	
	try {
		if (p_oObjeto.value != "")
			if (ExecutarValidacaoData(p_oObjeto.value) == null)
				throw "Data inválida.";
	}
	catch(e) {
		alert(e);
		p_oObjeto.value = "";
		p_oObjeto.focus();
	}
}

//SE FOR UMA DATA INV�?LIDA, ENTÃO A FUNÇÃO RETORNAR�? NULL
function ExecutarValidacaoData(p_sData) {	
	return CompararDatas(p_sData, "01/01/1753");
}

//FUNÇÃO QUE FAZ A COMPARAÇÃO ENTRA AS DATAS PASSADAS.
//SE A DATA MAIOR ESTIVER MENOR, ENTÃO A FUNÇÃO RETORNAR�? NULO.
function CompararDatas(p_sDataMaior, p_sDataMenor) {
	try {
		var v_sData = new String(p_sDataMaior);
		var v_sDataFormatada = parseInt(v_sData.substr(3,2), 10) + "/" + parseInt(v_sData.substr(0,2), 10) + "/" + parseInt(v_sData.substr(6,4), 10);
		var v_oObjData = new Date(v_sDataFormatada);

		//SE p_sDataMenor FOR PASSADO, ENTÃO VERIFICAR SE A DATA MAIOR REALMENTE É A MAIOR,
		//SE NÃO FOR, ENTÃO RETORNAR�? NULL
		if (p_sDataMenor != "" && p_sDataMenor != null) {
			var v_sDataMenor = new String(p_sDataMenor);
			var v_sDataMenorFormatada = parseInt(v_sDataMenor.substr(3,2), 10) + "/" + parseInt(v_sDataMenor.substr(0,2), 10) + "/" + parseInt(v_sDataMenor.substr(6,4), 10);
			var v_oObjDataMenor = new Date(v_sDataMenorFormatada);

			if (v_oObjData < v_oObjDataMenor)
				return null;
		}
		if (((v_oObjData.getUTCMonth() + 1) + "/" + v_oObjData.getUTCDate() + "/" + v_oObjData.getUTCFullYear()) != v_sDataFormatada)
			return null;
		else 
			return v_oObjData;
	}
	catch(e) {
		return null;
	}
}

function RetirarMascara(p_sValor){

	var v_sNovoValor = '';
	for(i=0;i<p_sValor.length;i++){
		if(p_sValor.charCodeAt(i) > 47 && p_sValor.charCodeAt(i) < 58){
			v_sNovoValor += p_sValor.substring(i, i+1);
		}
	}
	return v_sNovoValor;
}


function MascaraTelefone(p_oObj, p_bDDD){
	
	var v_sMascara;
	var v_iMudarMascara = 8; //9
	if(p_bDDD)
		v_iMudarMascara += 2; //5
		
	if(event.type=="blur")
		v_iMudarMascara +=1;
	
		
	if(RetirarMascara(p_oObj.value).length + 1 < v_iMudarMascara){
		if(p_bDDD)
			v_sMascara = '(##) ###-####';
		else
			v_sMascara = '###-####';
	}else{
		if(p_bDDD)
			v_sMascara = '(##) ####-####';
		else
			v_sMascara = '#####-####';
	}
	
	if(event.keyCode==0)
		ColocarMascara(p_oObj,v_sMascara,p_oObj.value);
	else
		MascaraDinamica(p_oObj,v_sMascara,"N");
		
	if(p_oObj.value.length==1)
		p_oObj.value=""
}


function MascaraDinamica(p_oObj, p_sMascara){

	if(event.type=="blur"){
		ColocarMascara(p_oObj,p_sMascara,p_oObj.value)
		return;
	}

	if (CheckDigits('N')) {
		ColocarMascara(p_oObj,p_sMascara,p_oObj.value + String.fromCharCode(event.keyCode))
		event.keyCode = null;
	}
}

function ColocarMascara(p_oObj,p_sMascara,p_sValor){
	
		var v_sValor = RetirarMascara(p_sValor);
		var v_iIndice = 0;
		var v_sNovoValor = ''
		for(i=0; i<p_sMascara.length;i++){
		
			if(p_sMascara.substring(i, i+1)=='#'){
				if(v_iIndice < v_sValor.length){
					v_sNovoValor += v_sValor.substring(v_iIndice,v_iIndice+1)
					v_iIndice++;
				}else{
					break;
				}
			}else{
				
				v_sNovoValor += p_sMascara.substring(i, i+1)
							
			}
		}
		p_oObj.value = v_sNovoValor

}

//FUNÇÃO RECEBE O OBJETO E A M�?SCARA DESEJADA
//DEVER�? SER USADA JUNTO COM O MAXLENGTH PARA LIMITAR O TAMANHO DO CAMPO
function DinamicMasks(v_oObj, v_sMask, v_sOnlyDigits) {
	if (CheckDigits(v_sOnlyDigits)) {
		for (i = 0; i < v_sMask.length; i++) {
			//MONTAR QUALQUER M�?SCARA COM #
			if ((v_sMask.substring(i, i + 1) != "#") && (v_oObj.value.length == i)) {
				v_oObj.value = v_oObj.value + v_sMask.substring(i, i + 1);
			}
		}
	}
}

//FUNÇÃO PARA PERMITIR DIGITAÇÃO DE SOMENTE NÚMEROS
function OnlyNumbersDot() {
	var v_eKeyPress = window.event.keyCode;

	if (!(v_eKeyPress > 45 && v_eKeyPress < 58 || v_eKeyPress == 13)) {
		window.event.keyCode = null;
		return false;
	}
	else {
		return true;
	}
}

//FUNÇÃO PARA PERMITIR DIGITAÇÃO DE SOMENTE NÚMEROS
function OnlyNumbers() {
	var v_eKeyPress = window.event.keyCode;

	if (!(v_eKeyPress > 47 && v_eKeyPress < 58 || v_eKeyPress == 13)) {
		window.event.keyCode = null;
		return false;
	}
	else {
		return true;
	}
}

//FUNÇÃO PARA PERMITIR DIGITAÇÃO DE SOMENTE LETRAS
function OnlyLetters() {
	var v_eKeyPress = window.event.keyCode;

	if (!((v_eKeyPress > 64 && v_eKeyPress < 91) || (v_eKeyPress > 96 && v_eKeyPress < 123))) {
		window.event.keyCode = null;
		return false;
	}
	else {
		return true;
	}
}

//FUNÇÃO PARA VERIFICAR QUAIS OS FILTROS DE DIGITAÇÃO SERÃO USADOS
function CheckDigits(v_sOnlyDigits) {
	if (v_sOnlyDigits == "N") {
		return OnlyNumbers();
	}
	if (v_sOnlyDigits == "L") {
		return OnlyLetters();
	}

	return true;
}

//FUNÇÃO PARA CONFIRMAR
function Confirmar(p_sTexto) {
	if (confirm(p_sTexto)) {
		return true;
	}
	else {
		return false;
	}
}

//FUNÇÃO PARA SETAR O FOCO EM OBJETOS
function SetFocus(p_oObj) {
	if (ObjExist(p_oObj)) {
		p_oObj.focus();
	}
}

//FUNÇÃO PARA VERIFICAR SE O OBJECT EXISTE
function ObjExist(p_oObj) {
	try {
		if ((p_oObj.name != "") || (p_oObj.id != "")) {
			return true;
		}
	}
	catch(e) {
		return false;
	}
}

//FUNÇÃO QUE RETIRA TODOS OS ESPAÇOS DA STRING
function Trim(p_sString){ 
	return(p_sString.replace( /^\s+|\s+$/gi, "").replace( /\s{2,}/gi, " " ))
}

//FUNÇÃO PARA VERIFICAR PREENCHIMENTO OBRIGATÓRIO
function VerificarPreenchimentoObrigatorio() {
	if (ObjExist(document.forms[0].hdnCamposObrigatorios)) {
		return VerificarPreenchimentoObrigatorioCampo();
	}
	else {
		return VerificarPreenchimentoObrigatorioTag();
	}
}

//FUNÇÃO QUE VERIFICA O PREENCHIMENTO OBRIGATÓRIO DOS CAMPOS QUE 
//POSSUEM A TAG: "TagMensagemObrigatorio"
function VerificarPreenchimentoObrigatorioTag() {
	var v_oFormulario = document.forms[0];

	for (i = 0; i < v_oFormulario.elements.length; i++) {
		//VERIFICO SE O OBJETO POSSUI A TAG PARA DIZER SE É OBRIGATÓRIO
		if (ObjExist(v_oFormulario.elements[i].TagMensagemObrigatorio)) {
			//OBJETOS DESABILITADOS NÃO SÃO VALIDADOS
			if (!v_oFormulario.elements[i].disabled) {
				//SE FOR OBRIGATÓRIO, ENTÃO VERIFICAR TIPO DO OBJETO E FAZER A VALIDAÇÃO
				if ((v_oFormulario.elements[i].type == "text") || (v_oFormulario.elements[i].type == "textarea") || (v_oFormulario.elements[i].type == "file") || (v_oFormulario.elements[i].type == "password")) {
					if (Trim(v_oFormulario.elements[i].value) == "") {
						v_oFormulario.elements[i].focus();
						alert("O campo " + v_oFormulario.elements[i].TagMensagemObrigatorio + " deve ser preenchido.");
						return false;
					}
				}

				if (v_oFormulario.elements[i].type == "select-one") {
					if (Trim(v_oFormulario.elements[i].value) == "-1") {
						v_oFormulario.elements[i].focus();
						alert("O campo " + v_oFormulario.elements[i].TagMensagemObrigatorio + " deve ser preenchido.");
						return false;
					}
				}
				
				if(v_oFormulario.elements[i].type == "hidden") {
					if (Trim(v_oFormulario.elements[i].value) == "") {
						document.getElementById(v_oFormulario.elements[i].TagCampoFoco).focus();
						alert("O campo " + v_oFormulario.elements[i].TagMensagemObrigatorio + " deve ser preenchido.");
						return false;
					
					}
				}
			}
		}
	}

	return true;
}

var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
function encode64(input) {
	var output = new StringMaker();
	var chr1, chr2, chr3;
	var enc1, enc2, enc3, enc4;
	var i = 0;
 
	while (i < input.length) {
		chr1 = input.charCodeAt(i++);
		chr2 = input.charCodeAt(i++);
		chr3 = input.charCodeAt(i++);
 
		enc1 = chr1 >> 2;
		enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		enc4 = chr3 & 63;
 
		if (isNaN(chr2)) {
			enc3 = enc4 = 64;
		} else if (isNaN(chr3)) {
			enc4 = 64;
		}
 
		output.append(keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4));
   }
   
   return output.toString();
}
 
function decode64(input) {
	var output = new StringMaker();
	var chr1, chr2, chr3;
	var enc1, enc2, enc3, enc4;
	var i = 0;
 
	// remove all characters that are not A-Z, a-z, 0-9, +, /, or =
	input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
	while (i < input.length) {
		enc1 = keyStr.indexOf(input.charAt(i++));
		enc2 = keyStr.indexOf(input.charAt(i++));
		enc3 = keyStr.indexOf(input.charAt(i++));
		enc4 = keyStr.indexOf(input.charAt(i++));
 
		chr1 = (enc1 << 2) | (enc2 >> 4);
		chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
		chr3 = ((enc3 & 3) << 6) | enc4;
 
		output.append(String.fromCharCode(chr1));
 
		if (enc3 != 64) {
			output.append(String.fromCharCode(chr2));
		}
		if (enc4 != 64) {
			output.append(String.fromCharCode(chr3));
		}
	}
 
	return output.toString();
}

function noFocus(obj){if(obj.blur())obj.blur()}