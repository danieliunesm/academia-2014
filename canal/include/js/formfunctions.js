function Trim(TRIM_VALUE){if(TRIM_VALUE.length<1)return "";TRIM_VALUE=RTrim(TRIM_VALUE);TRIM_VALUE=LTrim(TRIM_VALUE);if(TRIM_VALUE=="")return"";else return TRIM_VALUE;}

function RTrim(VALUE){var w_space=String.fromCharCode(32);var v_length=VALUE.length;var strTemp="";if(v_length<0)return"";var iTemp=v_length-1;while(iTemp>-1){if(VALUE.charAt(iTemp)!=w_space){strTemp=VALUE.substring(0,iTemp+1);break;};iTemp=iTemp-1;};return strTemp;}

function LTrim(VALUE){var w_space=String.fromCharCode(32);if(v_length<1)return"";var v_length=VALUE.length;var strTemp="";var iTemp=0;while(iTemp<v_length){if(VALUE.charAt(iTemp)!=w_space){strTemp=VALUE.substring(iTemp,v_length);break;};iTemp=iTemp+1;};return strTemp;}

function validaEmail(emailStr){
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")	;
	var matchArray=emailStr.match(emailPat);
	if(matchArray==null)return false;
	var user=matchArray[1];
	var domain=matchArray[2];
	if(user.match(userPat)==null)return false;
	var IPArray=domain.match(ipDomainPat);
	if(IPArray!=null){
		for(var i=1;i<=4;i++){
			if(IPArray[i]>255)return false;
		}
		return true;
	}	
	var domainArray=domain.match(domainPat);
	if(domainArray==null)return false;
	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;
	if(domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>4)return false;
	if(len<2)return false;
	return true;
}