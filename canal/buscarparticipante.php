<?php 
include "../include/security.php";
include "../include/defines.php";
include "../include/genericfunctions.php";
include "../include/accesscounter.php";
include "../include/cripto.php";
include('../admin/controles.php');
include('../admin/framework/crud.php');

if (count($_POST) == 0) exit();
$tipo = $_SESSION["tipo"];

function ehExcel()
{
	return $_POST["rdoExcel"] == 2;
}

function exibeChecks()
{
	global $tipo;
	//if ($tipo == -1 || $tipo > 4 || $_SESSION["nivelHierarquico"] == 1) {
	return (!ehExcel());
	//}
	
	//return false;
	
}

if (ehExcel())
{
	header("Content-Type: application/vnd.ms-excel; name='excel'");

	header("Content-disposition:  attachment; filename=Cadastro.xls");
	
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Colabor&aelig; - Consultoria e Educa&ccedil;&atilde;o Corporativa</title>
<meta http-equiv=pragma content=no-cache>
<?php 
if (!(ehExcel()))
{
?>
<link rel="stylesheet" type="text/css" href="/include/css/admincolaborae.css">
<?php 
}
?>
<style type="text/css">
#tabelaCadastro td
{
	font-size:10px;
	font-family:tahoma,arial,helvetica,sans-serif;
	border: 1px solid #999;
	padding:2px 5px 2px 5px;
}

#tabelaCadastro th
{
	font-size:11px;
	font-family:tahoma,arial,helvetica,sans-serif;
	background-color:#ccc;
	border: 1px solid #999;
}
</style>
<script type="text/javascript">
window.onload = function(){parent.document.getElementById('ifrUsuarios').style.display = 'block';}

function marcarDesmarcarTodos(obj){

    var marcar = obj.checked;
    var inputs = document.getElementsByTagName("input");
    var qtdInputs = inputs.length;

    for(var i = 0; i < qtdInputs; i++){

        if (inputs[i].type == "checkbox") {
            inputs[i].checked = marcar;
        }
    }

}


</script>
</head>
<body style="background-color:#ffffff">
	<?php 
	if (!(ehExcel()))
	{
	?>
	<form name="bloqueioForm" enctype="multipart/form-data" method="post" action="bloqueio.php" target="ifrBloqueio">
	<input type="hidden" id="hdnBloqueio" name="hdnBloqueio" />
	<?php 
	}

$empresa = $_SESSION["empresaID"];
$tipo = $_SESSION["tipo"];
$lotacaoUsuario = $_SESSION["lotacaoID"];
$nome = $_POST["nome"];
$cpf = $_POST["cpf"];
$lotacao = $_POST["cboLotacao"];
$cargo = $_POST["cargofuncao"];
$tipoParticipante = $_POST["rdoParticipantes"];
$ciclo = $_POST["cboCiclo"];

if($cargo == "-1") $cargo = "";

$sql = "SELECT CD_LOTACAO, DS_LOTACAO, CD_LOTACAO_PAI, CD_NIVEL_HIERARQUICO, DS_LOTACAO_HIERARQUIA, CD_IDENTIFICADOR_LOTACAO FROM col_lotacao WHERE CD_EMPRESA = $empresa ORDER BY DS_LOTACAO_HIERARQUIA";
$resultadoLotacao = DaoEngine::getInstance()->executeQuery( $sql, true );

$sqlWhere = "";
if ($tipoParticipante == "2") // Est� associado a prova
{
	$sqlWhere = " AND IN_ELEGIVEL = 1 ";
}

if ($tipoParticipante == "3") // N�o est� associado a prova
{	$sqlWhere = " AND IN_ELEGIVEL = 0 ";
}
$lotacoes = array();
$maxNivel = 0;
while($linha = mysql_fetch_array($resultadoLotacao))
{
	$auxNivel = count(explode('.', $linha["DS_LOTACAO_HIERARQUIA"]));
	$maxNivel = $auxNivel > $maxNivel ? $auxNivel : $maxNivel;
	$lotacoes[$linha["CD_LOTACAO"]] = $linha;
}

$sql = "

SELECT login, CONCAT(NM_USUARIO, ' ', COALESCE(NM_SOBRENOME,'')) AS NM_USUARIO, cargofuncao, lotacao, u.CD_USUARIO, DS_LOTACAO_HIERARQUIA, u.email,
(SELECT COUNT(1) FROM col_prova_aplicada pa INNER JOIN col_prova p ON pa.CD_PROVA = p.CD_PROVA WHERE pa.CD_USUARIO = u.CD_USUARIO AND p.IN_PROVA = 1) AS ELEGIVEL
FROM col_usuario u LEFT OUTER JOIN col_lotacao l on l.CD_LOTACAO = u.lotacao
 
WHERE empresa = $empresa
AND (cargofuncao like '%$cargo%')
AND ((lotacao = '$lotacao' OR '$lotacao' = '-1' OR DS_LOTACAO_HIERARQUIA LIKE '%$lotacao%') AND (DS_LOTACAO_HIERARQUIA LIKE '%$lotacaoUsuario%' OR $tipo IN (-1,5,4)))
AND (NM_USUARIO LIKE '$nome%' OR NM_SOBRENOME LIKE '$nome%' OR '$nome' = '')
AND (login like '%$cpf%' OR '$cpf' = '')
AND (Status=1)
$sqlWhere
ORDER BY NM_USUARIO, NM_SOBRENOME, login
";
echo "<!-- $sql $tipoParticipante -->";
//echo $sql;
//exit();

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);

?>
	<div id="" style="width: 95%; text-align: left; font-size:11px;
	font-family:tahoma,arial,helvetica,sans-serif;;">
		Total de Registros: <?php echo mysql_num_rows($resultado); ?>
	</div>

    <table id="tabelaCadastro"  border="0" cellpadding="0" cellspacing="0"
            style="table-layout:fixed; width:100%; border-collapse:collapse;">
        <tr class="textblk">
            <th>Grupo N1</th>
            <th>Grupo N2</th>
            <th>Grupo N3</th>
            <th>Grupo N4</th>
            <th>Grupo N5</th>
            <th>Nome Completo</th>
            <th>Cargo</th>
            <th>Login</th>
            <th>Email</th>
            <th style="width: 37px; padding:0px; text-align: left;"><input title="Marcar/Desmarcar Todos" type="checkbox" id="chkSelecionaTodos" onclick="marcarDesmarcarTodos(this);" /></th>
        </tr>
    </table>

<div style="width:100%; height: 350px; position: static; overflow-y: scroll;">
	<table id="tabelaCadastro" border="0" cellpadding="0" cellspacing="0" 
		style="table-layout:fixed; width:100%; border-collapse:collapse;">
		<tr class="textblk" style="display: none;">
<?php 
	for($j = 0; $j < $maxNivel; $j++){
?>
		<th>Grupo N<?php echo $j + 1;?></th>
<?php
	}
?>
		<th>Nome Completo</th>
		<th>Cargo</th>
		<th>Login</th>
		<th>Email</th>
<?php
	if (exibeChecks()) {
?>
		<th style="width: 20px; padding:0px;">&nbsp;</th>
<?php
	}
?>
		</tr>

<?php 

$iCount	   = 0;

while($linha=mysql_fetch_array($resultado))
{
	$cargofuncao = $linha["cargofuncao"];
	if ($cargofuncao == "") $cargofuncao = "&nbsp;";
	
	$lotacaoUsuario = $linha["lotacao"];
	
	$linhaLotacao = $lotacoes[$lotacaoUsuario];
	
	$check = "";
	
	if($iCount % 2 == 0) $bgcolor = "#fff"; else $bgcolor = "#f1f1f1";
	
	if (exibeChecks())
	{
        $checked = "";
        //if($linha["ELEGIVEL"] > 0)  $checked = "checked=\"checked\"";

		$check = "<td style=\"width:20px;padding:0px;text-align:center;\">
				<input type=\"checkbox\" name=\"chkSelecao[]\" value=\"{$linha["CD_USUARIO"]}\" />
			</td>";
	}
	
	echo "	<tr class=\"textblk\" style=\"background-color:$bgcolor\"> ";
	
	$str = $linha["DS_LOTACAO_HIERARQUIA"];
	for($i = 0; $i < $maxNivel; $i++) {
		atribuiLotacao2($lotacoes, $i, $str);
	}
	
	echo "	<td>				{$linha["NM_USUARIO"]}			</td>
			<td>				$cargofuncao			</td>
			<td>				{$linha["login"]}			</td>
			<td>				{$linha["email"]}			</td>				$check			</tr>	";
	$iCount++;
}

function atribuiLotacao(&$string, &$linhaLotacao, &$lotacoes, $nivel)
{
	if ($linhaLotacao["CD_NIVEL_HIERARQUICO"] == $nivel)
	{
		$string = $linhaLotacao["DS_LOTACAO"];
		$lotacaoUsuario = $linhaLotacao["CD_LOTACAO_PAI"];
		$linhaLotacao = $lotacoes[$lotacaoUsuario];
	}
}

function atribuiLotacao2(&$lotacoes, $nivel, $str) 
{
	$string = "&nbsp;";
	$arrayLotacao = explode( '.', $str );
	
	$campoExibir = "DS_LOTACAO";
	
	if ($_REQUEST["rdoLotacao"] == "S") {
		$campoExibir = "CD_IDENTIFICADOR_LOTACAO";
	}
	
	if(count($arrayLotacao) > $nivel)
	{
		$linhaLotacaoAux = $lotacoes[$arrayLotacao[$nivel]];
		$string = $linhaLotacaoAux[$campoExibir];
//		$lotacaoUsuario = $linhaLotacaoAux["CD_LOTACAO_PAI"];
	}
	echo "<td> $string </td>";
}

?>	</table></div>	</form>
</body>
</html>