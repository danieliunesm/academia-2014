<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>RSS - Termos de uso</title>
<style type="text/css">
body{padding:20px;font-family:verdana,arial,helvetica,sans-serif;font-size:11px;color:#666;line-height:1.6em;}
strong, span{color:#000;}
</style>
<script type="text/javascript">
top.window.moveTo(screen.availWidth*.1,screen.availHeight*.1);
top.window.resizeTo(screen.availWidth*.8, screen.availHeight*.8);
</script>
</head>
<body>

<p><strong>Nota importante!</strong></p>

<p>Este Banco de Not�cias re�ne assuntos de seu interesse e de profissionais de servi�os de informa��o e comunica��o. S�o informa��es de v�rias fontes � nacionais e internacionais. O objetivo � facilitar o acesso a not�cias do setor TIC e mant�-lo atualizado.</p> 

<p>As seguintes palavras chaves est�o configuradas para pesquisa:</p>

<ol>
<li>Acelera��o de Aplica��es</li>
<li>Anti V�rus</li>
<li>BI/Business Inteligence</li>
<li>BPM � Business Process Management</li>
<li>Cloud Computing</li>
<li>Content Filter/Filtro de Conte�do</li>
<li>Converg�ncia de Tecnologias</li>
<li>Converg�ncia IP/IP Convergence</li>
<li>Datacenter</li>
<li>DLP</li>
<li>Firewall</li>
<li>Internet</li>
<li>IPS</li>
<li>LAN</li>
<li>Mercado de Tecnologia da Informa��o</li>
<li>Mercado de Telecom Brasil</li>
<li>Mobilidade/Aplica��es M�veis/Mobility/Mobility Application</li>
<li>NAC</li>
<li>Next Generation Communication</li>
<li>Operadoras de Telecom</li>
<li>Redes de Computadores</li>
<li>SAN</li>
<li>Seguran�a</li>
<li>SIP TRUNCKING</li>
<li>Smart phones</li>
<li>Telecom � Servi�os e Produtos</li>
<li>Telefonia Fixa</li>
<li>Telefonia IP/ IP Telephony</li>
<li>Telefonia M�vel</li>
<li>Telepresen�a</li>
<li>TI /Tecnologia da Informa��o</li>
<li>Unified Communications</li>
<li>Virtualiza��o/Virtualization</li>
<li>VoIP</li>
<li>Web Application/Aplica��es Web</li>
<li>Wireless</li>
</ol>  

<p><strong>Observa��o:</strong><br />
Procuramos selecionar sites que n�o contam com propagandas de terceiros para acesso.<br />
A Colaborae n�o tem nenhum v�nculo com qualquer fornecedor.<br />
Eventualmente, tais tipos de propaganda podem ocorrer � ambientes web s�o muito din�micos. Caso identifiquem situa��es dessa natureza ou alguma outra quest�o que mere�a tratamento, contamos com a sua ajuda via <span>"fale conosco"</span>.</p>

<p><strong>Bom Programa!</strong></p> 

</body>
</html>
