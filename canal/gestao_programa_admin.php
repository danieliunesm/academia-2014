<?php
include "../include/security.php";
include "../include/genericfunctions.php";
include("../include/defines.php");
include('../admin/framework/crud.php');
include('../admin/controles.php');
include('../admin/page.php');
include "../include/accesscounter.php";

if (isset($_SESSION["empresaID"]) && $_SESSION["empresaID"] == 34)
{
	$labelAvaliacao = "Certifica��es";
}
else
{
	$labelAvaliacao = "Avalia��es";
}

$dataAtual = date("Ymd");
$dataFinal = date("d/m/Y");
$empresa = $_SESSION["empresaID"];
$dataInicio = "";
$dataInicial = $dataInicio;
$cicloSelecionado = (isset($_GET["id"])? $_GET["id"]:"");
$dataInicioTodosCiclos = "";
$dataTerminoTodosCiclos = "";


$sqlCiclo = "SELECT * FROM col_ciclo WHERE CD_EMPRESA = $empresa ORDER BY DT_INICIO";
$resultadoCiclo = DaoEngine::getInstance()->executeQuery($sqlCiclo,true);

$linksOutrosCiclos = "";

if (mysql_num_rows($resultadoCiclo) > 1)
{
	$linksOutrosCiclos = "$linksOutrosCiclos<p class=\"textblk\"><span class=\"tarjaTitulo\" style=\"background: #ffffff; color: #000000;\"><b>Selecione aqui um ciclo ou todos os ciclos:</b></span><br />";
}


if ($cicloSelecionado == "")
	$cicloSelecionado = -1;

$nomeCicloSelecionado = "Todos os Ciclos";
$codigoCicloSelecionado = -1;
while ($linhaCiclo = mysql_fetch_array($resultadoCiclo)) {
	
	$dataInicioCiclo = formataDataBancoYmd($linhaCiclo["DT_INICIO"]);
	$dataTerminoCiclo = formataDataBancoYmd($linhaCiclo["DT_TERMINO"]);
	
	if ($dataInicioTodosCiclos == "")
		$dataInicioTodosCiclos = formataDataBancodmY($linhaCiclo["DT_INICIO"]);
	
	$dataTerminoTodosCiclos = formataDataBancodmY($linhaCiclo["DT_TERMINO"]);
	
	if ($dataAtual < $dataInicioCiclo)
	{
		continue;
	}
		
	if ($cicloSelecionado == "")
	{
		if ($dataAtual >= $dataInicioCiclo && $dataAtual <= $dataTerminoCiclo)
		{
			$dataInicio = formataDataBancodmY($linhaCiclo["DT_INICIO"]);
			$dataInicial = $dataInicio;
			$dataFinal = formataDataBancodmY($linhaCiclo["DT_TERMINO"]);
			$nomeCicloSelecionado = $linhaCiclo["NM_CICLO"];
			$codigoCicloSelecionado = $linhaCiclo["CD_CICLO"];
			continue;
		}
	}
	else 
	{
		if ($cicloSelecionado == $linhaCiclo["CD_CICLO"])
		{
			$dataInicio = formataDataBancodmY($linhaCiclo["DT_INICIO"]);
			$dataInicial = $dataInicio;
			$dataFinal = formataDataBancodmY($linhaCiclo["DT_TERMINO"]);
			$nomeCicloSelecionado = $linhaCiclo["NM_CICLO"];
			$codigoCicloSelecionado = $linhaCiclo["CD_CICLO"];
			continue;
		}
	}
	
	$dataInicioTexto = formataDataBancodmY($linhaCiclo["DT_INICIO"]);
	$dataTerminoTexto = formataDataBancodmY($linhaCiclo["DT_TERMINO"]);
	
	$linksOutrosCiclos = "$linksOutrosCiclos<a href='$PHP_SELF?id={$linhaCiclo["CD_CICLO"]}'>{$linhaCiclo["NM_CICLO"]} - $dataInicioTexto a $dataTerminoTexto</a><br />";
	
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Colabor� - Consultoria e Treinamento em Telecom</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META http-equiv="pragma" content="no-cache">
		<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">
		<script type="text/javascript" src="include/js/functions.js"></script>
		<script language="JavaScript" src="../include/js/ranking.js"></script>
		<script language="javascript">
			function abreObs(id){
				
				var url = 'observacao.php?id=' +id;
				newWin=null;
				var w=400;
				var h=300;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				newWin=window.open(url,'menueditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=0,resizable=0');
				if(newWin!=null)setTimeout('newWin.focus()',100);
			
				
			}
			
			function abreRelatorio(rel, dataInicio, dataTermino, filtro, args)
			{
				
				url = rel + "?txtDe=" + dataInicio + "&txtAte=" + dataTermino + "&id=" + filtro + args;
				
				newWin=null;
				var w= 900;
				var h= 600;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				
				newWin=window.open(url,'relatorio','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
				
			}
		
		</script>
	</HEAD>
	
	<BODY>
	<!-- Inicio do T�tulo -->
		<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
			<TR>
				<TD><IMG height="2" src="images/blank.gif" width="100%"/></TD>
			</TR>
			<TR>
				<TD background="images/bg_logo_admin.png">
					<TABLE cellpadding="0" cellspacing="0" width="663" border="0"><!-- background="../images/logo_prova_simulado.png" -->
						<TR>
							<TD><IMG src="images/blank.gif" height="32" width="1"/></TD>
							<TD class="data" align="right"><?php echo getServerDate(); ?></TD>
						</TR>
					
					</TABLE>
				
				</TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" width="100%" height="2" /></TD>
			</TR>
			<TR>
				<TD bgcolor="#cccccc"><IMG src="images/blank.gif" height="3" width="100%"/></TD>
			</TR>
		</TABLE>
		<!-- Fim do T�tulo -->
		
		<!-- In�cio Cabe�alho -->
		<TABLE cellspacing="0" cellpadding="0" width="90%" align="center" border="0">
			<TR>
				<TD width="1%"><IMG height="20" src="images/blank.gif" width="289"/></TD>
				<TD></TD>
				<TD></TD>
			</TR>
			<TR valign="top">
				<td class="textblk">Usu�rio &raquo; <strong><?php echo strtoupper($_SESSION['alias']); ?></strong></td>
				<TD width="1%"><IMG height="20" src="images/blank.gif" width="150"/></TD>
				<TD align="right" width="98%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href = '/canal/index.php'"
							type="button" value="Voltar"></TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" height="4" width="1" /></TD>
			</TR>
		</TABLE>
		<BR>
		<!-- Fim Cabe�alho -->
		
		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="post">
					
			<TABLE cellpadding="0" cellspacing="0" width="90%" align="center" border="0">
				<TR class="tarjaTitulo">
						<TD align="middle" height="20">Radar do Administrador - <?php echo $nomeCicloSelecionado; ?></TD>
				</TR>
				<TR>
					<TD><IMG src="images/blank.gif" height="1" width="10"/></TD>
				</TR>
				<TR>
					<TD width="100%">

					<?php
						obterTextoComentario(basename($PHP_SELF,".php"));
					
						echo $linksOutrosCiclos;
						
						if ($codigoCicloSelecionado != -1)
						{
							echo "<a href='$PHP_SELF?id=-1'>Todos os Ciclos - $dataInicioTodosCiclos a $dataTerminoTodosCiclos</a><br />";
						}else 
						{
							$dataFinal = $dataTerminoTodosCiclos;
							$dataInicial = $dataInicioTodosCiclos;
						}
						
						$dataTerminoTexto = $dataFinal;
						
						if (formataDataStringYmd($dataFinal) > $dataAtual)
						{
							$dataFinal = date("d/m/Y");
						}
						

						
						echo "<p class=\"tarjaTitulo\" style=\"margin-bottom:-32px;background: #fff; color:#000;\"><b>$nomeCicloSelecionado - $dataInicial a $dataTerminoTexto</b>";
						echo "<p class=\"tarjaTitulo\" style=\"background: transparent; color:#000;\" align=\"right\"><b>Data do Ciclo: $dataFinal</b>";
						
					
						
						$sql = "SELECT CD_FILTRO, NM_FILTRO FROM col_filtro WHERE CD_EMPRESA = $empresa AND IN_ATIVO = 1 ORDER BY NM_FILTRO";
						$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
						
						$tipo = $_SESSION["tipo"];
						
						$possuiPermissao = false;
						
						if ($tipo > 4 || $tipo == -1)
							$possuiPermissao = true;
						
						
						echo '       <TABLE cellpadding="5" cellspacing="0" border="0" width="100%">
						                <TR class="tarjaItens">
						                    <TD class="title" width="20%">Grupo de Gest�o</TD>
						                    <TD class="title">Participantes</TD>
						                    <TD class="title">Acessos</TD>
						                    <TD class="title">Simulados</TD>
						                    <TD class="title">' . $labelAvaliacao . '</TD>
						                    <TD class="title">Ranking</TD>
								    <TD class="title">Ranking Novo</TD>
						                    <TD class="title">Fator</TD>
						                    <TD class="title">Absoluto</TD>
						                </TR>';
						
							
								if ($possuiPermissao)
								{
						
									
									
									
									$bgcolor = "#f1f1f1";
									
									$arDias = array();
									
									
									//for ($i=count($arDias) - 1; $i > -1; $i--)
									//{
										
										$dataInicio = $dataInicial;
										
										if ($bgcolor == "#f1f1f1")
											$bgcolor = "#ffffff";
										else 
											$bgcolor = "#f1f1f1";
										
										echo "	<tr align=\"left\" class=\"textblk\" bgcolor=\"$bgcolor\">
													<td align=\"left\">Vis�o Empresa</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_acesso_consolidado.php','$dataInicial','$dataFinal','', '&hdnTipoRelatorio=D&cboEmpresa=$empresa')\">
														Consolidado da Empresa
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_acesso.php','$dataInicial','$dataFinal','', '&hdnTipoRelatorio=D&cboEmpresa=$empresa')\">
														Detalhado da Empresa
													</a>
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_acesso_consolidado.php','$dataInicial','$dataFinal','', '&hdnTipoRelatorio=S&cboEmpresa=$empresa')\">
														Consolidado da Empresa
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_acesso.php','$dataInicial','$dataFinal','', '&hdnTipoRelatorio=S&cboEmpresa=$empresa')\">
														Detalhado da Empresa
													</a>
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_prova.php','$dataInicial','$dataFinal','','&cboEmpresa=$empresa&tipo=0')\">
														Detalhado da Empresa
													</a>
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_prova.php','$dataInicial','$dataFinal','','&cboEmpresa=$empresa&tipo=1')\">
														Detalhado da Empresa
													</a>
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking.php','$dataInicial','$dataFinal','','&hdnTipoRelatorio=U&cboEmpresa=$empresa&hdnCiclo=$codigoCicloSelecionado')\">
														Participantes
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking.php','$dataInicial','$dataFinal','','&hdnTipoRelatorio=L&cboEmpresa=$empresa&hdnCiclo=$codigoCicloSelecionado')\">
														Lota��es
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking.php','$dataInicial','$dataFinal','','&hdnTipoRelatorio=G&cboEmpresa=$empresa&hdnCiclo=$codigoCicloSelecionado')\">
														Grupos
													</a>
													<br />
													</td>
													<td >
														-----------
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_fator.php','$dataInicial','$dataFinal','','&hdnTipoRelatorio=U&cboEmpresa=$empresa&hdnCiclo=$codigoCicloSelecionado')\">
														Participantes
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_fator.php','$dataInicial','$dataFinal','','&hdnTipoRelatorio=L&cboEmpresa=$empresa&hdnCiclo=$codigoCicloSelecionado')\">
														Lota��es
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_fator.php','$dataInicial','$dataFinal','','&hdnTipoRelatorio=G&cboEmpresa=$empresa&hdnCiclo=$codigoCicloSelecionado')\">
														Grupos
													</a>
													<br />
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_absoluto.php','$dataInicial','$dataFinal','','&hdnTipoRelatorio=U&cboEmpresa=$empresa&hdnCiclo=$codigoCicloSelecionado')\">
														Participantes
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_absoluto.php','$dataInicial','$dataFinal','','&hdnTipoRelatorio=L&cboEmpresa=$empresa&hdnCiclo=$codigoCicloSelecionado')\">
														Lota��es
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_absoluto.php','$dataInicial','$dataFinal','','&hdnTipoRelatorio=G&cboEmpresa=$empresa&hdnCiclo=$codigoCicloSelecionado')\">
														Grupos
													</a>
													<br />
													</td>
												</tr>";
										
		
										
									//	}
									
									while ($linha = mysql_fetch_array($resultado)) {
										
										if ($bgcolor == "#f1f1f1")
											$bgcolor = "#ffffff";
										else 
											$bgcolor = "#f1f1f1";
											
										$filtro = $linha["CD_FILTRO"];
										
										echo "	<tr align=\"left\" class=\"textblk\" bgcolor=\"$bgcolor\">
													<td align=\"left\">{$linha["NM_FILTRO"]}</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_acesso_consolidado.php','$dataInicial','$dataFinal','$filtro', '&hdnTipoRelatorio=D')\">
														Consolidado do Grupo
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_acesso.php','$dataInicial','$dataFinal','$filtro', '&hdnTipoRelatorio=D')\">
														Detalhado do Grupo
													</a>
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_acesso_consolidado.php','$dataInicial','$dataFinal','$filtro', '&hdnTipoRelatorio=S')\">
														Consolidado do Grupo
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_acesso.php','$dataInicial','$dataFinal','$filtro', '&hdnTipoRelatorio=S')\">
														Detalhado do Grupo
													</a>
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_prova.php','$dataInicial','$dataFinal','$filtro','&tipo=0')\">
														Detalhado do Grupo
													</a>
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_prova.php','$dataInicial','$dataFinal','$filtro','&tipo=1')\">
														Detalhado do Grupo
													</a>
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking.php','$dataInicial','$dataFinal','$filtro','&hdnTipoRelatorio=U&hdnCiclo=$codigoCicloSelecionado')\">
														Participantes
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking.php','$dataInicial','$dataFinal','$filtro','&hdnTipoRelatorio=L&hdnCiclo=$codigoCicloSelecionado')\">
														Lota��es
													</a>
													</td>
													<td>
													<a title=\"Exibe o relat�rio de ranking normal, por�m com mais uma coluna para mostrar a posi��o do indiv�duo dentro da sua diretoria\" href=\"javascript:abreRelatorio('relatorios/relatorio_rankingb.php','$dataInicial','$dataFinal','$filtro','&hdnTipoRelatorio=U&hdnCiclo=$codigoCicloSelecionado&hdnModeloRelatorio=1')\">
														Part. por Diretoria
													</a>
													<br />
													<a title=\"Exibe o ranking tendo como universo utilizado apenas a diretoria do indiv�duo\" href=\"javascript:abreRelatorio('relatorios/relatorio_rankingb.php','$dataInicial','$dataFinal','$filtro','&hdnTipoRelatorio=U&hdnCiclo=$codigoCicloSelecionado&hdnModeloRelatorio=2')\">
														Part. por Diretoria2
													</a>

													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_fator.php','$dataInicial','$dataFinal','$filtro','&hdnTipoRelatorio=U&hdnCiclo=$codigoCicloSelecionado')\">
														Participantes
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_fator.php','$dataInicial','$dataFinal','$filtro','&hdnTipoRelatorio=L&hdnCiclo=$codigoCicloSelecionado')\">
														Lota��es
													</a>
													</td>
													<td>
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_absoluto.php','$dataInicial','$dataFinal','$filtro','&hdnTipoRelatorio=U&hdnCiclo=$codigoCicloSelecionado')\">
														Participantes
													</a>
													<br />
													<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_absoluto.php','$dataInicial','$dataFinal','$filtro','&hdnTipoRelatorio=L&hdnCiclo=$codigoCicloSelecionado')\">
														Lota��es
													</a>
													</td>
												</tr>";
									}
									
								}
								else
								{
									?>
										<tr>
											<td colspan="5" class="textblk" align="center">Voc� n�o possui permiss�o para visualizar estas informa��es.<br />Entre em contato com o administrador do sistema para mais informa��es.</td>
										</tr>
									<?php
								}
							
							?>
						
						</table>
					
					</TD>
				</TR>

				
			</TABLE>
			</p>
		</FORM>
		
<div id="footer" style="position:relative;top:0px;margin:20px auto 20px auto">
<div class="hrblue"></div>
<p>&copy; Colabor&aelig; <?php echo date("Y");?></p>
</div>
		
	</BODY>
	
</HTML>


<?php

	function conveterDataParaYMD($data)
	{
		$aData = split("/", $data);
		$data = "{$aData[2]}{$aData[1]}{$aData[0]}";
		
		return $data;
		
	}
	
	
	
	function somarUmDia($data){
		$aData = split("/", $data);
		$data = date("d/m/Y", strtotime("{$aData[1]}/{$aData[0]}/{$aData[2]}") + 90000);
		
		return  $data;
	}

	function formataDataBancoYmd($data)
	{
		
		$data = split("-", $data);
		$data = "{$data[0]}{$data[1]}{$data[2]}";
		
		return $data;
		
	}
	
	function formataDataBancodmY($data)
	{
		
		$data = split("-", $data);
		$data = "{$data[2]}/{$data[1]}/{$data[0]}";
		
		return $data;
		
	}
	
	function formataDataStringYmd($data)
	{
		
		$data = split("/", $data);
		$data = "{$data[2]}{$data[1]}{$data[0]}";
		
		return $data;
		
	}

	

?>