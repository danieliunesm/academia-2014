<?php
include "../include/security.php";
include "../include/genericfunctions.php";
include("../include/defines.php");
include('../admin/framework/crud.php');
include('../admin/controles.php');
include('../admin/page.php');
include "../include/accesscounter.php";

if ($_SESSION["tipo"] <4 && $_SESSION["tipo"] > 0)
	header("Location:gestao_programa_participante.php");

$empresa = $_SESSION["empresaID"];
$codigosUsuarios = "-1";
$nomesCargos = "-1";
$codigosLotacoes = "-1";
$codigolotacoesinferior = "''";

if ($_SESSION["tipo"] == -2)
{
	
	$codigoFiltro = $_SESSION["CodigoFiltro"];
	
	$filtro = new col_filtro();
	$filtro->CD_FILTRO = $codigoFiltro;
	$filtro = $filtro->obter();

	$codigosUsuarios = tranformaArrayVazioMenos1(obterValoresUsuario($filtro->usuarios));
	$nomesCargos = tranformaArrayVazioMenos1(obterValoresCargo($filtro->cargos));
	$codigosLotacoes = tranformaArrayVazioMenos1(obterValoresLotacao($filtro->lotacoes));
	
	$codigosUsuarios = joinArray($codigosUsuarios,",");
	$nomesCargos = joinArray($nomesCargos,"','");
	$codigosLotacoes = joinArray($codigosLotacoes,",");
}elseif ($_SESSION["tipo"] == -3)
{
	$codigosLotacoes = $_SESSION["lotacaoID"];
	$codigolotacoesinferior = $codigosLotacoes;
}

function tranformaArrayVazioMenos1($array)
{
	if (count($array) == 0)
	{
		return "-1";
	}
	
	return $array;

}

function joinArray($array, $separador = ",")
{
	if (is_array($array))
	{
		return join($separador, $array);
	}
	else
	{
		return "-1";
	}
	
}

$participante = (isset($_REQUEST["txtParticipante"])) ? $_REQUEST["txtParticipante"] : "";

$sql = "SELECT CD_USUARIO, login, NM_USUARIO, NM_SOBRENOME FROM col_usuario u inner join col_lotacao l on l.CD_LOTACAO = u.lotacao WHERE empresa = $empresa
AND (u.Status = 1)
AND (CD_USUARIO IN ($codigosUsuarios) or '$codigosUsuarios' = '-1')
AND (cargofuncao IN ('$nomesCargos') or '$nomesCargos' = '-1')
AND (lotacao IN ($codigosLotacoes) or '$codigosLotacoes' = '-1' or DS_LOTACAO_HIERARQUIA LIKE '%$codigolotacoesinferior.%')
AND
(
	(NM_USUARIO LIKE '$participante%' OR NM_SOBRENOME LIKE '$participante%' OR '$participante' = '')
OR
	(login like '$participante%' OR '$participante' = '')
)
ORDER BY NM_USUARIO, NM_SOBRENOME, login";

$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Colabor� - Consultoria e Treinamento em Telecom</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META http-equiv="pragma" content="no-cache">
		<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">
		
		<style>
			.textblk {
					

			}

			tr {
					cursor: hand;
			}
	
		</style>
		
		
		<script type="text/javascript" language="javascript">
		
			var trAnterior;
		
			function selecionarLinha(linha, idParticipante)
			{
				
				if (trAnterior != null)
				{
					trAnterior.style.background = trAnterior.bgColor;
					trAnterior.style.color = "#000000";
					
				}
				
				parent.document.getElementById("hdnParticipante").value = idParticipante;
				
				linha.style.background = "#0065ce";
				linha.style.color = "#ffffff";
				
				trAnterior = linha;
				
			}
		
		
		</script>
		
	</HEAD>
	
	<BODY>

		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="GET">
						<?php
						
						echo '       <TABLE cellpadding="1" cellspacing="0" border="0" width="100%">
						                ';
						
						$bgcolor = "#ffffff";
						
						while ($linha = mysql_fetch_array($resultado)) {

							if ($bgcolor == "#f1f1f1")
								$bgcolor = "#ffffff";
							else 
								$bgcolor = "#f1f1f1";
								
							echo "	<tr align=\"left\" class=\"textblk\" bgcolor=\"$bgcolor\" onclick='javascript:selecionarLinha(this, {$linha["CD_USUARIO"]})'>
											<td>
												{$linha["login"]} / {$linha["NM_USUARIO"]} {$linha["NM_SOBRENOME"]}
											</td>
										</tr>";

						}
								
							
							
										
							?>
						
						</table>
			
		</FORM>
	</BODY>
	
</HTML>


