<?php
include "../include/security.php";
include "../include/genericfunctions.php";
include("../include/defines.php");
include('../admin/framework/crud.php');
include('../admin/controles.php');
include('../admin/page.php');
include "../include/accesscounter.php";

$dataAtual = date("Ymd");
$dataFinal = date("d/m/Y");
$empresa = $_SESSION["empresaID"];
$dataInicio = "";
$dataInicial = $dataInicio;
$cicloSelecionado = (isset($_GET["id"])? $_GET["id"]:"");
$dataInicioTodosCiclos = "";
$dataTerminoTodosCiclos = "";

//S� permite sele��o do usu�rio se for administrador
$codigoUsuario = 0;
$usuarioSelecionado = "";
$linkVoltar = "/canal/selecao_participante.php";
if (isset($_GET["hdnParticipante"]) && $_SESSION["tipo"] >=4)
{
	$codigoUsuario = $_GET["hdnParticipante"];
	$usuarioSelecionado = "&hdnParticipante=$codigoUsuario";
}
else
{
	$codigoUsuario = $_SESSION["cd_usu"];
	$linkVoltar = "/canal/index.php";
}

$sqlCiclo = "SELECT * FROM col_ciclo WHERE CD_EMPRESA = $empresa ORDER BY DT_INICIO";
$resultadoCiclo = DaoEngine::getInstance()->executeQuery($sqlCiclo,true);

$linksOutrosCiclos = "";

if (mysql_num_rows($resultadoCiclo) > 1)
{
	$linksOutrosCiclos = "$linksOutrosCiclos<p class=\"textblk\"><span class=\"tarjaTitulo\" style=\"background: #ffffff; color: #000000;\"><b>Selecione aqui um ciclo ou todos os ciclos:</b></span><br />";
}

if ($cicloSelecionado == "")
	$cicloSelecionado = -1;

$nomeCicloSelecionado = "Todos os Ciclos";
$codigoCicloSelecionado = -1;



while ($linhaCiclo = mysql_fetch_array($resultadoCiclo)) {
	
	$dataInicioCiclo = formataDataBancoYmd($linhaCiclo["DT_INICIO"]);
	$dataTerminoCiclo = formataDataBancoYmd($linhaCiclo["DT_TERMINO"]);
	
	if ($dataInicioTodosCiclos == "")
		$dataInicioTodosCiclos = formataDataBancodmY($linhaCiclo["DT_INICIO"]);
	
	$dataTerminoTodosCiclos = formataDataBancodmY($linhaCiclo["DT_TERMINO"]);
	
	if ($dataAtual < $dataInicioCiclo)
	{
		continue;
	}
		
	if ($cicloSelecionado == "")
	{
		if ($dataAtual >= $dataInicioCiclo && $dataAtual <= $dataTerminoCiclo)
		{
			$dataInicio = formataDataBancodmY($linhaCiclo["DT_INICIO"]);
			$dataInicial = $dataInicio;
			$dataFinal = formataDataBancodmY($linhaCiclo["DT_TERMINO"]);
			$nomeCicloSelecionado = $linhaCiclo["NM_CICLO"];
			$codigoCicloSelecionado = $linhaCiclo["CD_CICLO"];
			continue;
		}
	}
	else 
	{
		if ($cicloSelecionado == $linhaCiclo["CD_CICLO"])
		{
			$dataInicio = formataDataBancodmY($linhaCiclo["DT_INICIO"]);
			$dataInicial = $dataInicio;
			$dataFinal = formataDataBancodmY($linhaCiclo["DT_TERMINO"]);
			$nomeCicloSelecionado = $linhaCiclo["NM_CICLO"];
			$codigoCicloSelecionado = $linhaCiclo["CD_CICLO"];
			continue;
		}
	}
	
	$dataInicioTexto = formataDataBancodmY($linhaCiclo["DT_INICIO"]);
	$dataTerminoTexto = formataDataBancodmY($linhaCiclo["DT_TERMINO"]);
	
	$linksOutrosCiclos = "$linksOutrosCiclos<a href='$PHP_SELF?id={$linhaCiclo["CD_CICLO"]}$usuarioSelecionado'>{$linhaCiclo["NM_CICLO"]} - $dataInicioTexto a $dataTerminoTexto</a><br />";
	
	$sql = "SELECT
  				u.NM_USUARIO, u.NM_SOBRENOME, f.NM_FILTRO
			FROM
  				col_usuario u
  				INNER JOIN col_filtro_lotacao fl ON fl.CD_LOTACAO = u.lotacao
  				INNER JOIN col_filtro f ON f.CD_FILTRO = fl.CD_FILTRO
			WHERE
  				u.CD_USUARIO = $codigoUsuario
			AND  f.IN_ATIVO = 1
			AND f.IN_FILTRO_RANKING = 1";
	
	$resultado = DaoEngine::getInstance()->executeQuery($sql,true);
	
	$linha = mysql_fetch_row($resultado);
	$nomeUsuario = "{$linha[0]} {$linha[1]}";
	$nomeFiltro = $linha[2];
	
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Colabor� - Consultoria e Treinamento em Telecom</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META http-equiv="pragma" content="no-cache">
		<LINK rel="stylesheet" type="text/css" href="include/css/admincolaborae.css">
		<script type="text/javascript" src="include/js/functions.js"></script>
		<script language="JavaScript" src="../include/js/ranking.js"></script>
		<script language="javascript">
			function abreObs(id){
				
				var url = 'observacao.php?id=' +id;
				newWin=null;
				var w=400;
				var h=300;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				newWin=window.open(url,'menueditor','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=0,resizable=0');
				if(newWin!=null)setTimeout('newWin.focus()',100);
			
				
			}
			
			function abreRelatorio(rel, dataInicio, dataTermino, filtro, args)
			{
				
				url = rel + "?txtDe=" + dataInicio + "&txtAte=" + dataTermino + "&id=" + filtro + args;
				
				newWin=null;
				var w= 900;
				var h= 600;
				var l=(screen.width-w)/2;
				var t=(screen.height-h)/2;
				
				newWin=window.open(url,'relatorio','left='+l+',top='+t+',width='+w+',height='+h+',scrollbars=1,resizable=1');
				
			}
		
		</script>
	</HEAD>
	
	<BODY>
	<!-- Inicio do T�tulo -->
		<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
			<TR>
				<TD><IMG height="2" src="images/blank.gif" width="100%"/></TD>
			</TR>
			<TR>
				<TD background="images/bg_logo_admin.png">
					<TABLE cellpadding="0" cellspacing="0" width="663" border="0"><!-- background="../images/logo_prova_simulado.png" -->
						<TR>
							<TD><IMG src="images/blank.gif" height="32" width="1"/></TD>
							<TD class="data" align="right"><?php echo getServerDate(); ?></TD>
						</TR>
					
					</TABLE>
				
				</TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" width="100%" height="2" /></TD>
			</TR>
			<TR>
				<TD bgcolor="#cccccc"><IMG src="images/blank.gif" height="3" width="100%"/></TD>
			</TR>
		</TABLE>
		<!-- Fim do T�tulo -->
		
		<!-- In�cio Cabe�alho -->
		<TABLE cellspacing="0" cellpadding="0" width="756" align="center" border="0">
			<TR>
				<TD width="1%"><IMG height="20" src="images/blank.gif" width="289"/></TD>
				<TD></TD>
				<TD></TD>
			</TR>
			<TR valign="top">
				<td class="textblk">Usu�rio &raquo; <strong><?php echo strtoupper($_SESSION['alias']); ?></strong></td>
				<TD width="1%"><IMG height="20" src="images/blank.gif" width="150"/></TD>
				<TD align="right" width="98%"><INPUT class="buttonsty" onfocus="noFocus(this)" onclick="document.location.href = '<?php echo $linkVoltar; ?>'"
							type="button" value="Voltar"></TD>
			</TR>
			<TR>
				<TD><IMG src="images/blank.gif" height="4" width="1" /></TD>
			</TR>
		</TABLE>
		<BR>
		<!-- Fim Cabe�alho -->
		
		<!-- inicio do form -->
		
		
		<FORM name="dadosForm" action="" method="post">
					
			<TABLE cellpadding="0" cellspacing="0" width="756" align="center" border="0">
				<TR class="tarjaTitulo">
						<TD align="middle" height="20">Radar do Participante - <?php echo "$nomeUsuario - $nomeCicloSelecionado"; ?></TD>
				</TR>
				<TR>
					<TD><IMG src="images/blank.gif" height="1" width="10"/></TD>
				</TR>
				<TR>
					<TD width="100%">

					<?php
						obterTextoComentario(basename($PHP_SELF,".php"));
					
						echo $linksOutrosCiclos;
						
						if ($codigoCicloSelecionado != -1)
						{
							echo "<a href='$PHP_SELF?id=-1$usuarioSelecionado'>Todos os Ciclos - $dataInicioTodosCiclos a $dataTerminoTodosCiclos</a><br />";
						}else 
						{
							$dataFinal = $dataTerminoTodosCiclos;
							$dataInicial = $dataInicioTodosCiclos;
						}
						
						$dataTerminoTexto = $dataFinal;
						
						if (formataDataStringYmd($dataFinal) > $dataAtual)
						{
							$dataFinal = date("d/m/Y");
						}

						
						echo "<p class=\"tarjaTitulo\" style=\"margin-bottom:-32px;background: #fff; color:#000;\"><b>$nomeCicloSelecionado - $dataInicial a $dataTerminoTexto</b>";
						echo "<p class=\"tarjaTitulo\" style=\"background: transparent; color:#000;\" align=\"right\"><b>Data do Ciclo: $dataFinal</b>";

						
						echo '       <TABLE cellpadding="5" cellspacing="0" border="0" width="100%">
						                <TR class="tarjaItens">
						                    <TD class="title" align="center">Ranking</TD>
						                    <TD class="title" align="center">Simulados</TD>
						                    <TD class="title" align="center">Certifica��es</TD>
						                </TR>';
						
							
							$bgcolor = "#f1f1f1";
							
							$arDias = array();
							
							if ($bgcolor == "#f1f1f1")
								$bgcolor = "#ffffff";
							else 
								$bgcolor = "#f1f1f1";
								
							$forum = "";
								
							$empresa = $_SESSION["empresaID"];
									
							echo "	<tr align=\"center\" class=\"textblk\" bgcolor=\"$bgcolor\">
											<td>
											<a href=\"javascript:abreRelatorio('relatorios/relatorio_ranking_individual.php','$dataInicial','$dataFinal','','&cboUsuario=$codigoUsuario&hdnTipoRelatorio=G&hdnCiclo=$codigoCicloSelecionado')\">
												Individual
											</a>
											</td>
											<td>
												<a href=\"javascript:abreRelatorio('relatorios/relatorio_prova_individual.php','$dataInicial','$dataFinal','','&cboEmpresa=$empresa&tipo=0&cboUsuario=$codigoUsuario&hdnCiclo=$codigoCicloSelecionado')\">
													Individual
												</a>
												</td>
												<td>
												<a href=\"javascript:abreRelatorio('relatorios/relatorio_prova_individual.php','$dataInicial','$dataFinal','','&cboEmpresa=$empresa&tipo=1&cboUsuario=$codigoUsuario&hdnCiclo=$codigoCicloSelecionado')\">
													Individual
												</a>
											</td>
										</tr>";
										
							?>
						
						</table>
					
					</TD>
				</TR>

				
			</TABLE>
			</p>
		</FORM>
		
<div id="footer" style="position:relative;top:0px;margin:20px auto 20px auto">
<div class="hrblue"></div>
<p>&copy; Colabor&aelig; <?php echo date("Y");?></p>
</div>
		
	</BODY>
	
</HTML>

